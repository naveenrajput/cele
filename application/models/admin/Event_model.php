<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event_model extends CI_Model {

    public function __construct()
    {
        parent:: __construct();
    }

  


    public function geteventedetails($event_id) 
    {
        $today_date = date('Y-m-d H:i:s');
        $where="events.id !=0 and events.is_deleted=0 and events.id=".$event_id; 

        $this->db->select("events.* ,services.service_title,countries.name as country_name,states.name as state_name,cities.name as city_name, users.fullname,if(events.is_completed=1,'Completed',if(events.event_date>'".$today_date."','Not Started','On Going')) as event_status ,sp.fullname as sp_name");
        $this->db->from('events');
        $this->db->join('users','users.user_id=events.user_id');
        $this->db->join('users sp','sp.user_id=events.sp_id'); 
        $this->db->join('services','services.id=events.service_id','left');
        $this->db->join('countries','countries.id=events.country_id','left');
        $this->db->join('states','states.id=events.state_id','left');
        $this->db->join('cities','cities.id=events.city_id','left');
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        }  
        $query = $this->db->get();     
        // echo $this->db->last_query();die;   
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;       
       
    }     


    public function getOrderItem($event_id) 
    {
        $today_date = date('Y-m-d H:i:s');
        $where="order_details.id !=0 and order_details.is_deleted=0 and order_details.event_id=".$event_id; 
        $this->db->select("order_details.*,services.service_title "); 
        $this->db->join('services','services.id=order_details.service_id','left'); 
        $this->db->from('order_details'); 
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        }  
        $query = $this->db->get();        
           // echo $this->db->last_query();die; 
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;       
       
    }     


  
 

    public function getEvent($offset=0,$limit=0) 
    {
        $where="events.id !=0 and events.is_deleted=0";
        $today_date = date('Y-m-d H:i:s');
        if($this->input->get('event_title')){
            $event_title=$this->input->get('event_title');
            $where.=" and events.event_title LIKE '%".$this->db->escape_like_str(trim($event_title))."%' ";
        }   
        if($this->input->get('event_number')){
            $event_number=$this->input->get('event_number');
            $where.=" and events.event_number LIKE '%".$this->db->escape_like_str(trim($event_number))."%' ";
        }   
        if($this->input->get('username')){
            $username=$this->input->get('username');
            $where.=" and users.fullname LIKE '%".$this->db->escape_like_str(trim($username))."%' ";
        }   
        
        if($this->input->get('date_rang')){
            $date_rang=$this->input->get('date_rang');
            $split_date=explode(" - ",$date_rang);
            $start_date= date('Y-m-d',strtotime($split_date[0]));
            $end_date=date('Y-m-d',strtotime($split_date[1])); 
            $where.=" and date(events.event_date) >='".$start_date."' and date(events.event_date) <='".$end_date."' ";
        } 

        
        if($this->input->get('status')){
            $status=$this->input->get('status');
            $where.=" and events.status='".$this->db->escape_like_str(trim($status))."'";
        } 
        $having ='';
        if($this->input->get('event_status')){
            $event_status=$this->input->get('event_status');
            $having.="event_status='".(trim($event_status))."'";
        } 
        
        $this->db->select("events.* ,users.fullname,if(events.is_completed=1,'Completed',if(events.event_date>'".$today_date."','Not Started','On Going')) as event_status ");
        $this->db->join('users','users.user_id=events.user_id');
        $this->db->from('events');
      
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        } 
        if($having !="" && $having != NULL) {
            $this->db->having($having);
        } 
        $this->db->order_by('events.id','DESC');
        if ($limit!=0) {
            $this->db->limit($limit,$offset);
            $query = $this->db->get(); 
                
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }else{
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->num_rows();
            } else return false;
        }        
       
    }    
    
}//model end

?>