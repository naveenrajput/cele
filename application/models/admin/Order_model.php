<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_model extends CI_Model {

    public function __construct()
    {
        parent:: __construct();
    }

  


    public function getOrderdetails($order_id) 
    {
        $today_date = date('Y-m-d H:i:s');
        $where="events.id !=0 and events.is_deleted=0 and events.type='Actual' and user_address.status='primary' and  orders.id=".$order_id; 

        $this->db->select("  orders.id as order_id,
            orders.order_number ,
            orders.total_amount as order_total_amount ,
            orders.payment_date ,
            orders.order_status ,
            orders.cancel_date ,
            orders.cancel_reason ,
            orders.canceled_by ,
            orders.ip_address ,
            orders.fullname as ev_name ,
            orders.email , 
            orders.mobile , 
            orders.payment_status , 
            events.* ,services.service_title,countries.name as country_name,states.name as state_name,cities.name as city_name, users.fullname,if(events.is_completed=1,'Completed',if(events.event_date>'".$today_date."','Not Started','On Going')) as event_status ,sp.fullname as sp_name,cancel.fullname as cancel_name,user_address.address as sp_address");
        $this->db->from('orders');
        $this->db->join('events','events.id=orders.event_id');
        $this->db->join('users','users.user_id=events.user_id');
        $this->db->join('users sp','sp.user_id=events.sp_id'); 
        $this->db->join('services','services.id=events.service_id','left');
        $this->db->join('countries','countries.id=events.country_id','left');
        $this->db->join('states','states.id=events.state_id','left');
        $this->db->join('cities','cities.id=events.city_id','left');
        $this->db->join('user_address','events.sp_id=user_address.user_id','left');
        $this->db->join('users cancel','cancel.user_id=orders.canceled_by','left');
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        }  
        $query = $this->db->get();     
    // echo $this->db->last_query();die;   
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;       
       
    }     


    public function getOrderItem($order_id) 
    {
        $today_date = date('Y-m-d H:i:s');
        $where="order_details.id !=0 and order_details.is_deleted=0 and order_details.order_id=".$order_id; 
        $this->db->select("order_details.*,services.service_title "); 
        $this->db->join('services','services.id=order_details.service_id','left'); 
        $this->db->from('order_details'); 
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        }  
        $query = $this->db->get();        
           // echo $this->db->last_query();die; 
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;       
       
    }     


  
 

    public function getOrder($offset=0,$limit=0) 
    {
        $where="events.id !=0 and events.is_deleted=0 and events.type='Actual'";
        $today_date = date('Y-m-d H:i:s');
        if($this->input->get('event_title')){
            $event_title=$this->input->get('event_title');
            $where.=" and events.event_title LIKE '%".$this->db->escape_like_str(trim($event_title))."%' ";
        }   
        if($this->input->get('event_number')){
            $event_number=$this->input->get('event_number');
            $where.=" and events.event_number LIKE '%".$this->db->escape_like_str(trim($event_number))."%' ";
        }  
        if($this->input->get('order_number')){
            $order_number=$this->input->get('order_number');
            $where.=" and orders.order_number LIKE '%".$this->db->escape_like_str(trim($order_number))."%' ";
        }   
        if($this->input->get('username')){
            $username=$this->input->get('username');
            $where.=" and users.fullname LIKE '%".$this->db->escape_like_str(trim($username))."%' ";
        }   
        
        if($this->input->get('date_rang')){
            $date_rang=$this->input->get('date_rang');
            $split_date=explode(" - ",$date_rang);
            $start_date= date('Y-m-d',strtotime($split_date[0]));
            $end_date=date('Y-m-d',strtotime($split_date[1])); 
            $where.=" and date(events.event_date) >='".$start_date."' and date(events.event_date) <='".$end_date."' ";
        } 

        
        if($this->input->get('order_status')){
            $order_status=$this->input->get('order_status');
            $where.=" and orders.order_status='".$this->db->escape_like_str(trim($order_status))."'";
        } 

        $having ='';
        if($this->input->get('event_status')){
            $event_status=$this->input->get('event_status');
            $having.="event_status='".(trim($event_status))."'";
        } 
        
        $this->db->select("
            orders.id as order_id,
            orders.order_number ,
            orders.total_amount as order_total_amount ,
            orders.payment_date ,
            orders.order_status ,
            orders.cancel_date ,
            orders.cancel_reason ,
            orders.canceled_by ,
            orders.ip_address , 

            ,events.* ,users.fullname,if(events.is_completed=1,'Completed',if(events.event_date>'".$today_date."','Not Started','On Going')) as event_status ");
        $this->db->from('orders');
        $this->db->join('events','events.id=orders.event_id');
        $this->db->join('users','users.user_id=events.user_id');
      
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        } 
        if($having !="" && $having != NULL) {
            $this->db->having($having);
        } 
        $this->db->order_by('events.id','DESC');
        if ($limit!=0) {
            $this->db->limit($limit,$offset);
            $query = $this->db->get(); 
                
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }else{
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->num_rows();
            } else return false;
        }        
       
    }    
    

    public function getAllPayment($offset=0,$limit=0) 
    {
        $where="events.id !=0 and events.is_deleted=0 ";
        $today_date = date('Y-m-d H:i:s');
         
        if($this->input->get('order_number')){
            $order_number=$this->input->get('order_number');
            $where.=" and orders.order_number LIKE '%".$this->db->escape_like_str(trim($order_number))."%' ";
        }   
        if($this->input->get('sender_name')){
            $sender_name=$this->input->get('sender_name');
            $where.=" and sender.fullname LIKE '%".$this->db->escape_like_str(trim($sender_name))."%' ";
        }   
        if($this->input->get('receiver_name')){
            $receiver_name=$this->input->get('receiver_name');
            $where.=" and receiver.fullname LIKE '%".$this->db->escape_like_str(trim($receiver_name))."%' ";
        }   
              
        if($this->input->get('date_rang')){
            $date_rang=$this->input->get('date_rang');
            $split_date=explode(" - ",$date_rang);
            $start_date= date('Y-m-d',strtotime($split_date[0]));
            $end_date=date('Y-m-d',strtotime($split_date[1])); 
            $where.=" and date(payment_history.created) >='".$start_date."' and date(payment_history.created) <='".$end_date."' ";
        } 
 
        if($this->input->get('payment_type')){
            $payment_type=$this->input->get('payment_type');
            $where.=" and payment_history.payment_type='".$this->db->escape_like_str(trim($payment_type))."'";
        } 
 
        if($this->input->get('transfer_type')){
            $transfer_type=$this->input->get('transfer_type');
            $where.=" and payment_history.transfer_type='".(trim($transfer_type))."'";
        } 

        $having ='';
        if($this->input->get('event_status')){
            $event_status=$this->input->get('event_status');
            $having.="event_status='".(trim($event_status))."'";
        } 
        
        $this->db->select(" 
            payment_history.id,
            payment_history.transaction_id,
            payment_history.transfer_type,
            payment_history.order_id,
            orders.order_number ,
            events.event_number ,
            events.event_title ,
            if(events.is_completed=1,'Completed',if(events.event_date>'".$today_date."','Not Started','On Going')) as event_status,
            payment_history.total_order_amount,
            payment_history.payment_status,
            payment_history.admin_commission_amount,
            payment_history.paid_amount ,
            payment_history.created ,
            payment_history.payment_type ,
            sender.fullname as sender_fullname,
            receiver.fullname as receiver_fullname,
            events.event_date 
            ");
        $this->db->from('payment_history'); 
        $this->db->join('orders','orders.id=payment_history.order_id');
        $this->db->join('events','events.id=orders.event_id');
        $this->db->join('users sender','sender.user_id=payment_history.customer_id');
        $this->db->join('users receiver','receiver.user_id=payment_history.service_provider_id');
      
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        } 
        if($having !="" && $having != NULL) {
            $this->db->having($having);
        } 
        $this->db->order_by('events.id','DESC');
        if ($limit!=0) {
            $this->db->limit($limit,$offset);
            $query = $this->db->get(); 
                
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }else{
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->num_rows();
            } else return false;
        }        
       
    }    
 



    public function getPaymentHistory($offset=0,$limit=0) 
    {
        $where="events.id !=0 and events.is_deleted=0 and  payment_history.payment_type='Order'";
        $today_date = date('Y-m-d H:i:s');
         
        if($this->input->get('order_number')){
            $order_number=$this->input->get('order_number');
            $where.=" and orders.order_number LIKE '%".$this->db->escape_like_str(trim($order_number))."%' ";
        }   
        if($this->input->get('sender_name')){
            $sender_name=$this->input->get('sender_name');
            $where.=" and sender.fullname LIKE '%".$this->db->escape_like_str(trim($sender_name))."%' ";
        }   
        if($this->input->get('receiver_name')){
            $receiver_name=$this->input->get('receiver_name');
            $where.=" and receiver.fullname LIKE '%".$this->db->escape_like_str(trim($receiver_name))."%' ";
        }   
              
        if($this->input->get('date_rang')){
            $date_rang=$this->input->get('date_rang');
            $split_date=explode(" - ",$date_rang);
            $start_date= date('Y-m-d',strtotime($split_date[0]));
            $end_date=date('Y-m-d',strtotime($split_date[1])); 
            $where.=" and date(payment_history.created) >='".$start_date."' and date(payment_history.created) <='".$end_date."' ";
        } 
 
        if($this->input->get('payment_type')){
            $payment_type=$this->input->get('payment_type');
            $where.=" and payment_history.payment_type='".$this->db->escape_like_str(trim($payment_type))."'";
        } 

        $having ='';
        if($this->input->get('event_status')){
            $event_status=$this->input->get('event_status');
            $having.="event_status='".(trim($event_status))."'";
        } 
        
        $this->db->select(" 
            payment_history.id,
            payment_history.order_id,
            orders.order_number ,
            payment_history.total_order_amount,
            payment_history.paid_amount ,
            payment_history.created ,
            payment_history.payment_type ,
            sender.fullname as sender_fullname,
            receiver.fullname as receiver_fullname 
            ");
        $this->db->from('payment_history'); 
        $this->db->join('orders','orders.id=payment_history.order_id');
        $this->db->join('events','events.id=orders.event_id');
        $this->db->join('users sender','sender.user_id=payment_history.customer_id');
        $this->db->join('users receiver','receiver.user_id=payment_history.service_provider_id');
      
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        } 
        if($having !="" && $having != NULL) {
            $this->db->having($having);
        } 
        $this->db->order_by('events.id','DESC');
        if ($limit!=0) {
            $this->db->limit($limit,$offset);
            $query = $this->db->get(); 
                
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }else{
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->num_rows();
            } else return false;
        }        
       
    }    

    public function getPaymentRefundHistory($offset=0,$limit=0) 
    {
        $where="events.id !=0 and events.is_deleted=0  and  payment_history.payment_type='Return'";
        $today_date = date('Y-m-d H:i:s');
         
        if($this->input->get('order_number')){
            $order_number=$this->input->get('order_number');
            $where.=" and orders.order_number LIKE '%".$this->db->escape_like_str(trim($order_number))."%' ";
        }   
        if($this->input->get('sender_name')){
            $sender_name=$this->input->get('sender_name');
            $where.=" and sender.fullname LIKE '%".$this->db->escape_like_str(trim($sender_name))."%' ";
        }   
        if($this->input->get('receiver_name')){
            $receiver_name=$this->input->get('receiver_name');
            $where.=" and receiver.fullname LIKE '%".$this->db->escape_like_str(trim($receiver_name))."%' ";
        }   
              
        if($this->input->get('date_rang')){
            $date_rang=$this->input->get('date_rang');
            $split_date=explode(" - ",$date_rang);
            $start_date= date('Y-m-d',strtotime($split_date[0]));
            $end_date=date('Y-m-d',strtotime($split_date[1])); 
            $where.=" and date(payment_history.created) >='".$start_date."' and date(payment_history.created) <='".$end_date."' ";
        } 
 
        if($this->input->get('payment_type')){
            $payment_type=$this->input->get('payment_type');
            $where.=" and payment_history.payment_type='".$this->db->escape_like_str(trim($payment_type))."'";
        } 

        $having ='';
        if($this->input->get('event_status')){
            $event_status=$this->input->get('event_status');
            $having.="event_status='".(trim($event_status))."'";
        } 
        
        $this->db->select(" 
            payment_history.id,
            payment_history.transfer_type,
            payment_history.order_id,
            orders.order_number ,
            payment_history.total_order_amount,
            payment_history.paid_amount ,
            payment_history.created ,
            payment_history.payment_type ,
            sender.fullname as sender_fullname,
            receiver.fullname as receiver_fullname 
            ");
        $this->db->from('payment_history'); 
        $this->db->join('orders','orders.id=payment_history.order_id');
        $this->db->join('events','events.id=orders.event_id');
        $this->db->join('users sender','sender.user_id=payment_history.customer_id');
        $this->db->join('users receiver','receiver.user_id=payment_history.service_provider_id');
      
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        } 
        if($having !="" && $having != NULL) {
            $this->db->having($having);
        } 
        $this->db->order_by('events.id','DESC');
        if ($limit!=0) {
            $this->db->limit($limit,$offset);
            $query = $this->db->get(); 
                
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }else{
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->num_rows();
            } else return false;
        }        
       
    }    

 
 

    public function getUsreSubscription($offset=0,$limit=0) 
    {
        $where="user_subscription_history.id !=0 ";
        $today_date = date('Y-m-d H:i:s');
         
      
        if($this->input->get('username')){
            $username=$this->input->get('username');
            $where.=" and users.fullname LIKE '%".$this->db->escape_like_str(trim($username))."%' ";
        }   
        if($this->input->get('title')){
            $title=$this->input->get('title');
            $where.=" and user_subscription_history.title LIKE '%".$this->db->escape_like_str(trim($title))."%' ";
        }   
        if($this->input->get('user_id')){
            $user_id=$this->input->get('user_id');
            $where.=" and user_subscription_history.user_id = '".$this->db->escape_like_str(trim($user_id))."' ";
        }   
       
              
        if($this->input->get('date_rang')){
            $date_rang=$this->input->get('date_rang');
            $split_date=explode(" - ",$date_rang);
            $start_date= date('Y-m-d',strtotime($split_date[0]));
            $end_date=date('Y-m-d',strtotime($split_date[1])); 
            $where.=" and date(payment_history.created) >='".$start_date."' and date(payment_history.created) <='".$end_date."' ";
        } 
  
        $this->db->select("users.fullname,user_subscription_history.*");
        $this->db->from('user_subscription_history');   
        $this->db->join('users','users.user_id=user_subscription_history.user_id');
      
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        } 
       
        $this->db->order_by('user_subscription_history.id','DESC');
        if ($limit!=0) {
            $this->db->limit($limit,$offset);
            $query = $this->db->get(); 
                
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }else{
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->num_rows();
            } else return false;
        }        
       
    }    




    public function getPaymentdetails($payment_id) 
    {
        $where="events.id !=0 and events.is_deleted=0 and payment_history.id=".$payment_id;
        $today_date = date('Y-m-d H:i:s'); 
        
        $this->db->select(" 
            payment_history.id,
            payment_history.order_id,
            orders.order_number ,
            payment_history.total_order_amount,
            payment_history.paid_amount ,
            payment_history.transaction_id ,
            payment_history.created ,
            payment_history.payment_type ,
            
            sender.fullname as sender_fullname,
            sender.mobile as sender_mobile,
            receiver.fullname as receiver_fullname,
            receiver.mobile as receiver_mobile,
            orders.payment_date ,
            orders.order_status ,
            orders.cancel_date ,
            orders.cancel_reason ,
            orders.canceled_by ,
            orders.ip_address ,
            orders.email , 
            orders.mobile , 
            orders.payment_status , 
            events.* ,countries.name as country_name,states.name as state_name,cities.name as city_name,if(events.is_completed=1,'Completed',if(events.event_date>'".$today_date."','Not Started','On Going')) as event_status ,sp.fullname as sp_name,cancel.fullname as cancel_name
            ");
        $this->db->from('payment_history'); 
        $this->db->join('orders','orders.id=payment_history.order_id');
        $this->db->join('events','events.id=orders.event_id');
        $this->db->join('countries','countries.id=events.country_id','left');
        $this->db->join('states','states.id=events.state_id','left');
        $this->db->join('cities','cities.id=events.city_id','left');
        $this->db->join('users sender','sender.user_id=payment_history.customer_id');
        $this->db->join('users receiver','receiver.user_id=payment_history.service_provider_id');
        $this->db->join('users sp','sp.user_id=events.sp_id'); 
        $this->db->join('users cancel','cancel.user_id=orders.canceled_by','left');
      
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        } 
    
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->row_array();
            } else return false; 
    }    
    
    


    public function getPayment($offset=0,$limit=0) 
    {
        $where="events.id !=0 and events.is_deleted=0";
        $today_date = date('Y-m-d H:i:s');
        if($this->input->get('event_title')){
            $event_title=$this->input->get('event_title');
            $where.=" and events.event_title LIKE '%".$this->db->escape_like_str(trim($event_title))."%' ";
        }   
        if($this->input->get('event_number')){
            $event_number=$this->input->get('event_number');
            $where.=" and events.event_number LIKE '%".$this->db->escape_like_str(trim($event_number))."%' ";
        }  
        if($this->input->get('order_number')){
            $order_number=$this->input->get('order_number');
            $where.=" and orders.order_number LIKE '%".$this->db->escape_like_str(trim($order_number))."%' ";
        }   
        if($this->input->get('username')){
            $username=$this->input->get('username');
            $where.=" and users.fullname LIKE '%".$this->db->escape_like_str(trim($username))."%' ";
        }   
        
        if($this->input->get('date_rang')){
            $date_rang=$this->input->get('date_rang');
            $split_date=explode(" - ",$date_rang);
            $start_date= date('Y-m-d',strtotime($split_date[0]));
            $end_date=date('Y-m-d',strtotime($split_date[1])); 
            $where.=" and date(events.event_date) >='".$start_date."' and date(events.event_date) <='".$end_date."' ";
        } 

        
        if($this->input->get('order_status')){
            $order_status=$this->input->get('order_status');
            $where.=" and orders.order_status='".$this->db->escape_like_str(trim($order_status))."'";
        } 

        $having ='';
        if($this->input->get('event_status')){
            $event_status=$this->input->get('event_status');
            $having.="event_status='".(trim($event_status))."'";
        } 
        
        $this->db->select("
            orders.id as order_id,
            orders.order_number ,
            orders.total_amount as order_total_amount ,
            orders.payment_date ,
            orders.order_status ,
            orders.cancel_date ,
            orders.cancel_reason ,
            orders.canceled_by ,
            orders.ip_address ,  
            ,events.* ,users.fullname,if(events.is_completed=1,'Completed',if(events.event_date>'".$today_date."','Not Started','On Going')) as event_status ");
        $this->db->from('orders');
        $this->db->join('events','events.id=orders.event_id');
        $this->db->join('users','users.user_id=events.user_id');
      
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        } 
        if($having !="" && $having != NULL) {
            $this->db->having($having);
        } 
        $this->db->order_by('events.id','DESC');
        if ($limit!=0) {
            $this->db->limit($limit,$offset);
            $query = $this->db->get(); 
                
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }else{
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->num_rows();
            } else return false;
        }        
       
    }    



}//model end

?>