<?php 

class Chat_model extends CI_Model 
{
	function __construct() {
     	parent::__construct();
    }
	
 
 
	public function getFriendList($user_id,$count=false,$limit='',$offset='') {
 	
 		if($this->input->get('userfullname')){
            $userfullname=$this->input->get('userfullname');
            $this->db->like("users.fullname", trim($userfullname));
        } 
		$this->db->select('users.user_id,friends.id,users.fullname,users.staging_id,users.created as join_date,users.status,friends.modified');
		$this->db->from('friends');
		$this->db->join('users','users.user_id = if(friends.sender_id='.$user_id.',friends.receiver_id,friends.sender_id)');    
		$this->db->group_start(); 
			$this->db->or_where('friends.sender_id',$user_id); 
			$this->db->or_where('friends.receiver_id',$user_id);  
		$this->db->group_end(); 
		$this->db->where('users.is_deleted',0); 
		$this->db->where('friends.status','Accepted'); 
		$this->db->order_by('friends.id','DESC');
		   $this->db->limit($limit, $offset);
		$query = $this->db->get();

		$result= $query->result_array();
 
	     if($count==true){
              return $query->num_rows();
        }
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        } 
    }
 
 
	public function chatHistory($user_id,$room_id,$limit='',$offset='') {
 	
 		 $sql = "SELECT chat.original_file_name,chat.id,chat.file_format,chat.sender_id,chat.receiver_id,sender.fullname as user_name,if(sender.profile_picture='','resources/default_image.png',sender.profile_picture) as profile_picture,chat.room_id,chat.type,chat.job_id,chat.message,chat.created FROM `chat` join `users`sender on sender.user_id = chat.sender_id   WHERE chat.room_id=".$room_id." AND chat.message!='' And (if(chat.sender_id=".$user_id.",chat.sender_id_delete=0,chat.receiver_id_delete=0)) ORDER BY chat.id DESC limit ".$offset." ,".$limit;
		
		$query=$this->db->query($sql);
		
		if ($query->num_rows() > 0) {
             return $query->result_array();
        } else return false;
        
    }
  

	public function findRoomId($sender_id,$receiver_id) {
 	
		$sql = "select * FROM `chat_room` where (`first_user`='".$sender_id."' and `second_user`='".$receiver_id."') or (`second_user`='".$sender_id."' and `first_user`='".$receiver_id."') ";  
		$query=$this->db->query($sql);
		
		if ($query->num_rows() > 0) {
             return $query->row_array();
        } else return false;
        
    }
  

}

