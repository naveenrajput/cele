<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

    public function __construct()
    {
        parent:: __construct();
    }
    public function get_user_list($limit,$offset) {
        if($this->input->get('fullname')){
            $fullname=$this->input->get('fullname');
            $this->db->like("fullname", trim($fullname));
        }
        if($this->input->get('staging_id')){
            $staging_id=$this->input->get('staging_id');
            $this->db->like("staging_id", trim($staging_id));
        }
        if($this->input->get('mobile')){
            $mobile=$this->input->get('mobile');
            $this->db->like("mobile", trim($mobile));
        }
        if($this->input->get('user_type')){
            $user_type=$this->input->get('user_type');
            $this->db->like("user_type", trim($user_type));
        }
        if($this->input->get('status')){
            $status=$this->input->get('status');
            $this->db->where('status',$status);
        }
        if($this->input->get('user_id')){
            $user_id=$this->input->get('user_id');
            $this->db->where('users.user_id',$user_id);
        }
        $this->db->select('users.*');
        $this->db->from('users');
        $this->db->where('is_deleted','0');
        $this->db->order_by('user_id','Desc');
        $this->db->limit($limit, $offset);
        $query=$this->db->get();

        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }



    public function getReportPostAll($count=false,$limit='',$offset='') { 
 
        if($this->input->get('post_number')){
            $post_number=$this->input->get('post_number');
            $this->db->like("post.post_number", trim($post_number));
        }
 
        if($this->input->get('userfullname')){
            $userfullname=$this->input->get('userfullname');
            $this->db->like("users.fullname", trim($userfullname));
        }
  
        $this->db->select('post_report.*,users.fullname,users.staging_id,users.profile_picture,post.post_number,post.post_content,post.status,post_user.fullname as postusername,post_user.user_id as post_user_user_id,post_user.status as post_user_status,post_user.staging_id as user_staging_id');
        $this->db->from('post_report');
        $this->db->join('post','post.id=post_report.post_id');
        $this->db->join('users','users.user_id=post_report.user_id');
        $this->db->join('users post_user','post_user.user_id=post.user_id');
        $this->db->where('users.is_deleted','0'); 
        $this->db->where('post.is_deleted','0'); 
        $this->db->order_by('post_report.id','Desc');
        $this->db->limit($limit, $offset);
        $query=$this->db->get();

        if($count==true){
              return $query->num_rows();
        }
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }

    } 
 

    public function getReportPost($user_id,$count=false,$limit='',$offset='') { 
 
        if($this->input->get('post_number')){
            $post_number=$this->input->get('post_number');
            $this->db->like("post.post_number", trim($post_number));
        }
 
        if($this->input->get('userfullname')){
            $userfullname=$this->input->get('userfullname');
            $this->db->like("users.fullname", trim($userfullname));
        }
  
        $this->db->select('post_report.*,users.fullname,users.staging_id,users.profile_picture,post.post_number,post.post_content,post.status');
        $this->db->from('post_report');
        $this->db->join('post','post.id=post_report.post_id');
        $this->db->join('users','users.user_id=post_report.user_id');
        $this->db->where('users.is_deleted','0'); 
        $this->db->where('post.is_deleted','0'); 
        $this->db->where('post.user_id',$user_id); 
        $this->db->order_by('post_report.id','Desc');
          $this->db->limit($limit, $offset);
        $query=$this->db->get();

        if($count==true){
              return $query->num_rows();
        }
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        } 
    }




    public function getreportDetail($report_id) {   
          $this->db->select('post_report.*,users.fullname,users.staging_id,users.profile_picture,post.post_number,post.post_content,post.status,post_user.fullname as postusername,post_user.user_id as post_user_user_id,post_user.status as post_user_status,post_user.staging_id as user_staging_id');
        $this->db->from('post_report');
        $this->db->join('post','post.id=post_report.post_id');
        $this->db->join('users','users.user_id=post_report.user_id');
        $this->db->join('users post_user','post_user.user_id=post.user_id');
        $this->db->where('users.is_deleted','0'); 
        $this->db->where('post.is_deleted','0');  
        $this->db->where('post_report.id',$report_id);  
        $query=$this->db->get();  
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;  
    }





  public function getPostLike($user_id,$count=false,$limit='',$offset=''){  

        if($this->input->get('post_number')){
            $post_number=$this->input->get('post_number');
            $this->db->like("post.post_number", trim($post_number));
        }
 
        if($this->input->get('userfullname')){
            $userfullname=$this->input->get('userfullname');
            $this->db->like("users.fullname", trim($userfullname));
        }
  
        $this->db->select('like_post.*,users.fullname,users.staging_id,users.profile_picture,post.post_number,post.post_content,post.status');
        $this->db->from('like_post');
        $this->db->join('post','post.id=like_post.post_id');
        $this->db->join('users','users.user_id=post.user_id');
        $this->db->where('users.is_deleted','0'); 
        $this->db->where('post.is_deleted','0'); 
        $this->db->where('like_post.user_id',$user_id); 
        $this->db->order_by('like_post.id','Desc');
          $this->db->limit($limit, $offset);
        $query=$this->db->get();

        if($count==true){
              return $query->num_rows();
        }
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }

    }




  public function getPostshared($user_id,$count=false,$limit='',$offset=''){  

        if($this->input->get('post_number')){
            $post_number=$this->input->get('post_number');
            $this->db->like("post.post_number", trim($post_number));
        }
 
        if($this->input->get('userfullname')){
            $userfullname=$this->input->get('userfullname');
            $this->db->like("users.fullname", trim($userfullname));
        }
  
        $this->db->select('main_post.post_content,main_post.post_number,main_post.id,users.fullname,users.staging_id,users.profile_picture,post.created,main_post.status');
        $this->db->from('post'); 
        $this->db->join('post main_post','main_post.id=post.shared_post_id');
        $this->db->join('users','users.user_id=main_post.user_id');
        $this->db->where('users.is_deleted','0'); 
        $this->db->where('post.is_deleted','0');  
        $this->db->where('post.user_id',$user_id); 
        $this->db->where('post.shared_post_id!=',''); 
        $this->db->order_by('post.id','Desc');
        $this->db->limit($limit, $offset);
        $query=$this->db->get();

        if($count==true){
              return $query->num_rows();
        }
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }

    }







}//model end

?>