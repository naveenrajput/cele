<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post_model extends CI_Model {

    public function __construct()
    {
        parent:: __construct();
    }

    public function get_post_list($limit,$offset) {
    	if($this->input->get('userfullname')){
            $fullname=$this->input->get('userfullname');
            $this->db->like("users.fullname", trim($fullname));
        }
        if($this->input->get('post_number')){
            $post_number=$this->input->get('post_number');
            $this->db->like("pd.post_number", trim($post_number));
        }
        if($this->input->get('post_kind')){
            $post_kind=$this->input->get('post_kind');
            $this->db->like("pd.post_type", trim($post_kind));
        }
        if($this->input->get('post_type')){
            $post_type=$this->input->get('post_type');
            if($post_type==1){
                $this->db->where("post_media.id!=","");
            }else{
                $this->db->where("post_media.id",NULL);
            }
        }
        if($this->input->get('post_id')){
            $post_id=$this->input->get('post_id');
            $this->db->where("pd.id", ($post_id));
        }

        if($this->input->get('user_id')){
            $user_id=$this->input->get('user_id');
           $this->db->where('pd.user_id',$user_id);
        }
        if($this->input->get('status')){
            $status=$this->input->get('status');
            $this->db->where('pd.status',$status);
        }
    	$this->db->select('pd.*,post_media.id as media_id');
    	// $this->db->select('post_media.media_path, post_media.video_thumbnail');
    	$this->db->select('users.fullname');
        $this->db->from('post pd');
        $this->db->join('users','users.user_id=pd.user_id','left');
        $this->db->join('post_media','post_media.post_id=pd.id','left');
        $this->db->where('pd.is_deleted','0');
        $this->db->group_by('pd.id');
        $this->db->order_by('pd.id','Desc');
        $this->db->limit($limit, $offset);
        $query=$this->db->get();
       // echo $this->db->last_query();exit;
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }


     public function getReportusers($post_id,$count=false,$limit='',$offset='') { 
 
        if($this->input->get('post_number')){
            $post_number=$this->input->get('post_number');
            $this->db->like("post.post_number", trim($post_number));
        }
 
        if($this->input->get('userfullname')){
            $userfullname=$this->input->get('userfullname');
            $this->db->like("users.fullname", trim($userfullname));
        }
  
        $this->db->select('post_report.*,users.fullname,users.staging_id,users.profile_picture');
        $this->db->from('post_report');
        $this->db->join('post','post.id=post_report.post_id');
        $this->db->join('users','users.user_id=post_report.user_id');
        $this->db->where('users.is_deleted','0'); 
        $this->db->where('post.is_deleted','0'); 
        $this->db->where('post.id',$post_id); 
        $this->db->order_by('post_report.id','Desc');
          $this->db->limit($limit, $offset);
        $query=$this->db->get();

        if($count==true){
              return $query->num_rows();
        }
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }

    }

  
    public function getLikedusers($post_id,$count=false,$limit='',$offset='',$filter=''){  
        if($filter!=1){
            if($this->input->get('staging_id')){
                $staging_id=$this->input->get('staging_id');
                $this->db->like("users.staging_id", trim($staging_id));
            }
            
            if($this->input->get('userfullname')){
                $userfullname=$this->input->get('userfullname');
                $this->db->like("users.fullname", trim($userfullname));
            }
        }
        
        $this->db->select('like_post.*,users.fullname,users.staging_id,users.profile_picture,users.status');
        $this->db->from('like_post');
        $this->db->join('post','post.id=like_post.post_id');
        $this->db->join('users','users.user_id=like_post.user_id');
        $this->db->where('users.is_deleted','0'); 
        $this->db->where('post.is_deleted','0'); 
        $this->db->where('like_post.post_id',$post_id); 
        $this->db->order_by('like_post.id','Desc');
          $this->db->limit($limit, $offset);
        $query=$this->db->get();

        if($count==true){
              return $query->num_rows();
        }
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }

    }

  
    public function getpostcomment($post_id,$parent_id){  

       
        $this->db->select('comments.*, (select count(comment_id) from comment_like WHERE comment_like.comment_id =comments.id) as comment_like ,users.fullname,users.staging_id,users.profile_picture,users.status,comments.created');
        $this->db->from('comments');
        $this->db->join('post','post.id=comments.post_id');
        $this->db->join('users','users.user_id=comments.user_id');
        $this->db->where('users.is_deleted','0'); 
        $this->db->where('post.is_deleted','0'); 
        $this->db->where('comments.parent_id',$parent_id); 
        $this->db->where('comments.post_id',$post_id); 
        $this->db->order_by('comments.id','Desc');
       
        $query=$this->db->get(); 
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
       

    }



  public function getPostshared($post_id,$count=false,$limit='',$offset=''){  

        if($this->input->get('staging_id')){
            $staging_id=$this->input->get('staging_id');
            $this->db->like("users.staging_id", trim($staging_id));
        }
 
        if($this->input->get('userfullname')){
            $userfullname=$this->input->get('userfullname');
            $this->db->like("users.fullname", trim($userfullname));
        }
  
        $this->db->select('main_post.id,users.fullname,users.staging_id,users.profile_picture,post.created,users.status');
        $this->db->from('post'); 
        $this->db->join('post main_post','main_post.id=post.shared_post_id');
        $this->db->join('users','users.user_id=post.user_id');
        $this->db->where('users.is_deleted','0'); 
        $this->db->where('post.is_deleted','0');  
        $this->db->where('post.shared_post_id',$post_id); 
        $this->db->order_by('post.id','Desc');
        $this->db->limit($limit, $offset);
        $query=$this->db->get();

        if($count==true){
              return $query->num_rows();
        }
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }

    }


      public function getReportPost($post_id,$count=false,$limit='',$offset='') { 
 
        if($this->input->get('staging_id')){
            $staging_id=$this->input->get('staging_id');
            $this->db->like("users.staging_id", trim($staging_id));
        }
 
        if($this->input->get('userfullname')){
            $userfullname=$this->input->get('userfullname');
            $this->db->like("users.fullname", trim($userfullname));
        }
  
        $this->db->select('post_report.*,users.fullname,users.staging_id,users.profile_picture,post.post_number,post.post_content,post.status');
        $this->db->from('post_report');
        $this->db->join('post','post.id=post_report.post_id');
        $this->db->join('users','users.user_id=post_report.user_id');
        $this->db->where('users.is_deleted','0'); 
        $this->db->where('post.is_deleted','0'); 
        $this->db->where('post_report.post_id',$post_id); 
        $this->db->order_by('post_report.id','Desc');
          $this->db->limit($limit, $offset);
        $query=$this->db->get();

        if($count==true){
              return $query->num_rows();
        }
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }

    }





}