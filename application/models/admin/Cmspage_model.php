<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cmspage_model extends CI_Model {

    public function __construct()
    {
    	parent:: __construct();
    }

    			
    public function insert_cmspage($data)
    {

    	$cmspage_data=array(
    		'title' =>$data['title'] ,
    		'description' =>$data['description'] ,
    		'content'=>$data['content'],
    		'seo_title'=>$data['seo_title'],
    		'seo_description'=>$data['seo_description'],
    		'seo_keywords'=>$data['seo_keywords'],
    		'status'=>$data['status']
    	);

    return $this->db->insert('cms_pages',$cmspage_data);

    }/* insert product end*/


    public function update_cmspage($data)
    {
    	$cmspage_data=array(
    		'title' =>$data['title'] ,
    		'description' =>$data['description'] ,
    		'content'=>$data['content'],
    		'seo_title'=>$data['seo_title'],
    		'seo_description'=>$data['seo_description'],
    		'seo_keywords'=>$data['seo_keywords'],
    		'status'=>$data['status']
    	);
    					
    	$this->db->where('cmspage_id',$data['cmsid']);
    	return $this->db->update('cms_pages',$cmspage_data);
    }/* update product end*/



    public function show_cmspages($limit,$start)
    {
    	$this->db->limit($limit, $start);
    	$query= $result=$this->db->get('cms_pages');

    	if ($query->num_rows() > 0) {
            return $query;
        }
        return false;

    }/* Show_products*/

    public function get_cmspage($id)
    {
    	$this->db->where('cmspage_id', $id);
    	return $this->db->get('cms_pages');
    }/* Edit Product*/

    public function total_cmspages()
    {
    	$result=$this->db->get('cms_pages');
    	return $result->num_rows();
    }

    public function delete_cmspage($id)
    {
    	$this->db->where('cmspage_id', $id);
    	$this->db->delete('cms_pages'); 
    	
    }
    public function get_all_pages($limit,$offset)
    {
    	if($this->input->get()) {
            //  echo "<pre>"; print_r($_GET['orderby']); exit;
            if($this->input->get('title')!='') {
                $this->db->like('title', $this->input->get('title'));
            } 
            if($this->input->get('status')!='') {
                $this->db->where('status', $this->input->get('status'));
            } 
            
            if($this->input->get('orderby') !='') {
                if($this->input->get('orderby') =='ASC')
                    $this->db->order_by('title', 'ASC');
                if($this->input->get('orderby') =='DESC')
                    $this->db->order_by('title', 'DESC');
            } else {
               $this->db->order_by('title','DESC');
            }
        } else {
            $this->db->order_by('title','DESC');
        }
            
    	$this->db->limit($limit, $offset);
    	$query=$this->db->get('pages');

    	if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }


    public function get_pages_total() 
    {
    	$where="page_id !=0";
     	
    	if($this->input->get('title')){
    		$title=$this->input->get('title');
    		$where.=" and pages.title LIKE '%".$this->db->escape_like_str(trim($title))."%' ";
    	}
        if($this->input->get('status')){
            $status=$this->input->get('status');
            $where.=" and pages.status='".$this->db->escape_like_str(trim($status))."'";
        }

    	$limit=ADMIN_LIMIT;
     	$sql="SELECT *
    	FROM  `pages` 
    	WHERE $where
    	";
    	$query=$this->db->query($sql);
    	
    	if ($query->num_rows() > 0) {
            return $query->num_rows();
        } else return false;
        
    }	

    public function get_pages_list($offset=0) 
    {
    	$where="page_id !=0";
     	
    	if($this->input->get('title')){
    		$title=$this->input->get('title');
    		$where.=" and pages.title LIKE '%".$this->db->escape_like_str(trim($title))."%' ";
    	}
        if($this->input->get('status')){
            $status=$this->input->get('status');
            $where.=" and pages.status='".$this->db->escape_like_str(trim($status))."'";
        }
    	$limit=ADMIN_LIMIT;
     	$sql="SELECT *
    	FROM  `pages` 
    	WHERE $where
    	ORDER BY page_id DESC
    	limit $offset,$limit";
    	$query=$this->db->query($sql);

    	if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
        
    }	

    public function get_advertisement_list($offset=0) 
    {
        $where="advertisement_id !=0";
        
        if($this->input->get('expiry_date')){
            $expiry_date=$this->input->get('expiry_date');
            $where.=" and advertisement.expiry_date LIKE '%".$this->db->escape_like_str(trim($expiry_date))."%' ";
        }

        $limit=ADMIN_LIMIT;
        $sql="SELECT *
        FROM  `advertisement` 
        WHERE $where
        ORDER BY advertisement_id DESC
        limit $offset,$limit";
        $query=$this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
        
    }   


    public function get_all_admin_contacts($limit,$offset)
    {
        $this->db->select('contact_id,ticket_id,name,email,subject,created');
        if($this->input->get()) {
            
            if($this->input->get('ticket_id')!='') {
                $this->db->where('ticket_id', $this->input->get('ticket_id'));
            }
           
            if($this->input->get('name')!='') {
                $this->db->like('name', $this->input->get('name'));
            } 

            if($this->input->get('email')!='') {
                $this->db->like('email', $this->input->get('email'));
            } 

             if($this->input->get('subject')!='') {
                $this->db->like('subject', $this->input->get('subject'));
            } 

            if($this->input->get('status')!='') {
                $this->db->where('status', $this->input->get('status'));
            } 
            
            if($this->input->get('orderby') !='') {
                if($this->input->get('orderby') =='ASC')
                    $this->db->order_by('contact_id', 'ASC');
                if($this->input->get('orderby') =='DESC')
                    $this->db->order_by('contact_id', 'DESC');
            } else {
               $this->db->order_by('contact_id','DESC');
            }
        } else {
            $this->db->order_by('contact_id','DESC');
        }
            
        $this->db->limit($limit, $offset);
        
        $query=$this->db->get('contact');
        // echo $this->db->last_query();die;
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }

    public function getContactDetail($contact_id) {
        if($contact_id) {
            $this->db->select('contact.*,admin.fullname,admin.parent_id as adminparent');
            $this->db->from('contact');
            $this->db->join('admin','admin.admin_id = contact.reply_by','left');
            $this->db->where('contact.contact_id', $contact_id);
            $this->db->or_where('contact.parent_id', $contact_id);
            $query=$this->db->get();
            return $query->result_array();
        }
    }

    public function get_banner_list($limit,$offset)
    {
        if($this->input->get()) {
            if($this->input->get('title')!='') {
                $this->db->like('title', $this->input->get('title'));
            } 
        }
        $this->db->order_by('id','DESC');
        $this->db->limit($limit, $offset);
        $query=$this->db->get('banner');

        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }
   





}//model end

?>