<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front_common_model extends CI_Model {

    public function __construct()
    {
        parent:: __construct();
    }
    public function getpostcomment($limit,$offset,$post_id,$parent_id){ 

        $this->db->select('if((select id from comment_like where comments.id =comment_like.comment_id and user_id=121),1,0) as is_liked_by_me,comments.*,users.fullname,users.staging_id,users.profile_picture,users.status,users.gender,comments.created,post.post_number,(select count(comment_id) from comment_like inner join users U ON U.user_id=comment_like.user_id WHERE comment_like.comment_id =comments.id and U.is_deleted=0 and U.status="Active") as comment_like,(select count(id) from comments C inner join users U ON U.user_id=C.user_id WHERE comments.id =C.parent_id and U.is_deleted=0 and U.status="Active") as reply_count ');
        $this->db->from('comments');
        $this->db->join('post','post.id=comments.post_id');
        $this->db->join('users','users.user_id=comments.user_id');
        $this->db->where('users.is_deleted','0'); 
        $this->db->where('post.is_deleted','0'); 
        $this->db->where('comments.is_deleted','0'); 
        $this->db->where('comments.parent_id',$parent_id); 
        $this->db->where('comments.post_id',$post_id); 
        $this->db->order_by('comments.id','Desc');
        $this->db->limit($limit,$offset);
        $query=$this->db->get();
       // echo $this->db->last_query();
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    } 
    public function getpostcomment_bkp($limit,$offset,$post_id,$parent_id){ 

        $this->db->select('comments.*,users.fullname,users.staging_id,users.profile_picture,users.status,users.gender,comments.created,(select count(comment_id) from comment_like inner join users U ON U.user_id=comment_like.user_id WHERE comment_like.comment_id =comments.id and U.is_deleted=0 and U.status="Active") as comment_like,(select count(id) from comments C inner join users U ON U.user_id=C.user_id WHERE comments.id =C.parent_id and U.is_deleted=0 and U.status="Active") as reply_count ');
        $this->db->from('comments');
        $this->db->join('post','post.id=comments.post_id');
        $this->db->join('users','users.user_id=comments.user_id');
        $this->db->where('users.is_deleted','0'); 
        $this->db->where('post.is_deleted','0'); 
        $this->db->where('comments.is_deleted','0'); 
        $this->db->where('comments.parent_id',$parent_id); 
        $this->db->where('comments.post_id',$post_id); 
        $this->db->order_by('comments.id','Desc');
        
        $query=$this->db->get(); 
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
    } 
    public function free_trial_subscription($user_id) 
    {
        if(APP_PUBLISH){
            $start_date=APP_PUBLISH_DATE;
            $end_date= date('Y-m-d', strtotime(APP_PUBLISH_DATE. ' + '.FREE_TRIAL_DAYS.' days'));
            $current=date('Y-m-d');
            $subscription_end_date=date('Y-m-d', strtotime($current. ' + '.FREE_TRIAL_DAYS.' days'));
            if(strtotime($end_date) > strtotime($current)){
                $subscription_array=array(
                    'user_id'=>$user_id,
                    'plan_id'=>1000,
                    'start_date'=>$current,
                    'end_date'=>$subscription_end_date,
                    'plan_orders'=>'Unlimited',
                    'time_type'=>'Month',
                    'time_value'=>3,
                    'total_days'=>FREE_TRIAL_DAYS,
                    'amount'=>0,
                    'title'=>'Free Trial',
                    'description'=>'Free Trial',
                    'transaction_id'=>'',
                    'transaction_fees'=>'',
                    'total_used_order'=>0,
                    'previous_remaining_order'=>0,
                    'total_remaining_order'=>0,
                    'created'=>date('Y-m-d H:i:s'),
                );
                $this->Common_model->addEditRecords('user_subscription_history',$subscription_array);
            }
        }
    }
    public function cancel_by_sp_payment($user_id,$order_id,$reason=''){
        
        $order=$this->Common_model->getRecords('orders','*',array('id'=>$order_id),'id DESC',true);
        $services =  $this->Common_model->getRecords('services','user_id',array('id'=>$order['service_id']),'id DESC',true);
        $payment_detail=$this->Common_model->getRecords('payment_history','*',array('order_id'=>$order_id,'transfer_type'=>'Customer_To_Admin'),'id DESC',true);
        $payment_for_cele = $payment_detail['user_will_get'];

        $cele_detail=$this->Common_model->getRecords('users','email,paypal_id',array('user_id'=>$order['user_id']),'user_id DESC',true);
        $data_for_paypal['email_subject']= 'Cancel Service Amount';
        $data_for_paypal['email_message']='Cancel Service Amount';
        $paypal_id = $cele_detail['paypal_id'];
        if(empty($paypal_id)){
                $response =  array('status'=>0,'msg'=>'Event owner\'s paypal id not found.');  
                echo json_encode($response); exit;
        }

        $responce =  paypal_amount_send($payment_for_cele,ADMIN_CURRENCY_CODE,$paypal_id,$data_for_paypal);

        $paypal_fees = ($payment_for_cele*2.90/100)+0.30;  // paypal fee

        $user_will_get = number_format((float)$payment_for_cele-$paypal_fees , 2, '.', ''); 
        if(!isset($responce->batch_header->payout_batch_id) || empty($responce->batch_header->payout_batch_id)){
            $error = str_replace("_"," ",$responce->error);
            $details = array('status'=>0,'msg'=>"Paypal ".$error);
            echo json_encode($details);exit; 
        }

        $payout_batch_id = $responce->batch_header->payout_batch_id;
        if(empty($payout_batch_id)){

            $error = str_replace("_"," ",$responce->error);
            $details = array('status'=>0,'msg'=>"Paypal ".$error);
            echo json_encode($details);exit; 
         }

        $array_insert = array(
            'order_id'=>$order['id'],
            'transaction_id'=>'',
            'total_order_amount'=>$order['total_amount'],
            'paid_amount'=>$payment_for_cele,
            'user_will_get'=>$user_will_get,
            'paypal_fee_amount'=>$paypal_fees,
            'payout_batch_id'=>$payout_batch_id,
            'customer_id'=>$payment_detail['customer_id'],
            'service_provider_id'=>$services['user_id'],
            'payment_type'=>'Return',
            'payment_status'=>'Pending',
            'transfer_type'=>'Admin_To_Customer', 
            'created'=>date('Y-m-d H:i:s'),
            'ip_address'=>$this->input->ip_address(), 
        ); 
        $this->db->trans_begin();
        $this->Common_model->addEditRecords('payment_history',$array_insert);
        $this->Common_model->addEditRecords('events',array('is_canceled'=>'1','canceled_by'=>$user_id,'modified'=>date('Y-m-d H:i:s')),array('id'=>$order['event_id'])); 
        $this->Common_model->addEditRecords('orders',array('order_status'=>'Canceled','cancel_date'=>date('Y-m-d'),'canceled_by'=>$user_id,'cancel_reason'=>$reason,'modified'=>date('Y-m-d H:i:s')),array('id'=>$order['id'])); 
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return 0;
        } else {
            $this->db->trans_commit();
            $this->notify_event_attendee($order['user_id'],$order['event_id']);
            return 1;
        }

    }
    public function notify_event_attendee($user_id,$event_id){
      
        $this->db->select('events.event_title,event_number,user_id,sp_id');
        $this->db->from('events');
        $this->db->where('events.id',$event_id);
        $query1 = $this->db->get();        
        if ($query1->num_rows() > 0) {
            $event_data=$query1->row_array();
        } else return false;

        if($event_data['user_id']!=$user_id){
            $title1='#'.$event_data['event_number'].' '.ucwords($event_data['event_title']).' has been canceled by celebrant.';
            $content1='#'.$event_data['event_number'].' '.ucwords($event_data['event_title']).' has been canceled by celebrant.';
            $notdata1['type']='event_canceled_by_celebrant';
            $this->Common_model->push_notification_send($event_data['sp_id'],$notdata1,$event_id,$title1,$content1,'',$user_id);
        }else{
            $title1='#'.$event_data['event_number'].' '.ucwords($event_data['event_title']).' has been canceled by service provider.';
            $content1='#'.$event_data['event_number'].' '.ucwords($event_data['event_title']).' has been canceled by service provider.';
            $notdata1['type']='event_canceled_by_sp';
            $this->Common_model->push_notification_send($event_data['user_id'],$notdata1,$event_id,$title1,$content1,'',$user_id);
        }
        $this->db->select('users.user_id');
        $this->db->from('event_attendee');
        $this->db->join('users','users.user_id=if(sender_id='.$user_id.',receiver_id,sender_id)','left');
        $this->db->where('event_attendee.status','Accepted');
        $this->db->where('event_attendee.event_id',$event_id);
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            $user_data=$query->result_array();
        } else return false;
        if(!empty($user_data)){
            $title='#'.$event_data['event_number'].' '.ucwords($event_data['event_title']).' has been canceled.';
            $content='#'.$event_data['event_number'].' '.ucwords($event_data['event_title']).' has been canceled.';
            $notdata['type']='event_canceled';
            foreach ($user_data as $key => $value) {
               $this->Common_model->push_notification_send($value['user_id'],$notdata,$event_id,$title,$content,'',$user_id);
            }
        }
    }
    public function cancel_by_cele_payment($user_id,$order_id){
        
       
        $order=$this->Common_model->getRecords('orders','*',array('id'=>$order_id),'id DESC',true);
        // echo "<pre>";print_r($order);die;
        $services =  $this->Common_model->getRecords('services','user_id',array('id'=>$order['service_id']),'id DESC',true);

        $payment_detail=$this->Common_model->getRecords('payment_history','*',array('order_id'=>$order_id,'transfer_type'=>'Customer_To_Admin'),'id DESC',true);
    

         // Cancel Condition 

        $cancel_condition =  $this->Common_model->getRecords('settings','before_cancellation,after_cancellation','','id DESC',true);
        $today_date = date('Y-m-d');
        $cancel_date = $order['sp_order_cancel_date'];

        // echo "<pre>";print_r($services['user_id']);die;

        if(strtotime($today_date) <= strtotime($cancel_date)){

            $cancel_per = $cancel_condition['before_cancellation'];

        }else{
            $cancel_per = $cancel_condition['after_cancellation'];
        }

        /// For Service Provide 

        $admin_get = $payment_detail['user_will_get'];
        $total_amount_for_sp = number_format((float)(($admin_get*$cancel_per)/100) , 2, '.', '');
        $amount_for_cele =number_format((float)($admin_get-$total_amount_for_sp), 2, '.', '');

        //Remove Admin Commisstion

        $getAdmincommission =  $this->Common_model->getFieldValue('settings','commission');


        $admincommissionvalue = ($total_amount_for_sp*$getAdmincommission)/100;
 
        $payment_for_service_provider =number_format((float) $total_amount_for_sp-$admincommissionvalue  , 2, '.', '');



        // Amount For Cele 
        $amount_for_cele = $amount_for_cele; 

         
        $cele_detail=$this->Common_model->getRecords('users','email,paypal_id',array('user_id'=>$order['user_id']),'user_id DESC',true); 
        
        $data_for_paypal['email_subject']= 'Cancel Service Amount';
        $data_for_paypal['email_message']='Cancel Service Amount';
        $paypal_id = $cele_detail['paypal_id'];
        if(empty($paypal_id)){
                $response =  array('status'=>0,'msg'=>'Event owner paypal id not found.');  
                echo json_encode($response); exit;
        }

        $responce =  paypal_amount_send($amount_for_cele,ADMIN_CURRENCY_CODE,$paypal_id,$data_for_paypal);

        $paypal_fees = ($amount_for_cele*2.90/100)+0.30;  // paypal fee

        $user_will_get = number_format((float)$amount_for_cele-$paypal_fees , 2, '.', ''); 
        if(!isset($responce->batch_header->payout_batch_id) || empty($responce->batch_header->payout_batch_id)){
            $error = str_replace("_"," ",$responce->error);
            $details = array('status'=>0,'msg'=>"Paypal ".$error);
            echo json_encode($details);exit; 
        }

        $payout_batch_id = $responce->batch_header->payout_batch_id;
        if(empty($payout_batch_id)){

            $error = str_replace("_"," ",$responce->error);
            $details = array('status'=>0,'msg'=>"Paypal ".$error);
            echo json_encode($details);exit; 
         }
 
        $array_insert = array(
            'order_id'=>$order['id'],
            'transaction_id'=>'',
            'total_order_amount'=>$order['total_amount'],
            'paid_amount'=>$amount_for_cele,
            'user_will_get'=>$user_will_get,
            'paypal_fee_amount'=>$paypal_fees,
            'payout_batch_id'=>$payout_batch_id,
            'customer_id'=>$payment_detail['customer_id'],
            'service_provider_id'=>$services['user_id'],
            'payment_type'=>'Return',
            'payment_status'=>'Pending',
            'transfer_type'=>'Admin_To_Customer', 
            'created'=>date('Y-m-d H:i:s'), 
            'ip_address'=>$this->input->ip_address(),
        ); 
        $this->db->trans_begin();
        $this->Common_model->addEditRecords('payment_history',$array_insert);

        $amount_for_sp = $payment_for_service_provider;


        $service_provider_detail=$this->Common_model->getRecords('users','email,paypal_id',array('user_id'=>$services['user_id']),'user_id DESC',true);

        
        $data_for_paypal['email_subject']= 'Cancel Services Amount';
        $data_for_paypal['email_message']='Cancel Services Amount';
        $paypal_id = $service_provider_detail['paypal_id'];
        if(empty($paypal_id)){
                $response =  array('status'=>0,'msg'=>'Service provider paypal id not found.');  
                echo json_encode($response); exit;
        }

        $responce =  paypal_amount_send($amount_for_sp,ADMIN_CURRENCY_CODE,$paypal_id,$data_for_paypal);
        if(!isset($responce->batch_header->payout_batch_id) || empty($responce->batch_header->payout_batch_id)){
            $error = str_replace("_"," ",$responce->error);
            $details = array('status'=>0,'msg'=>"Paypal ".$error);
            echo json_encode($details);exit; 
        }
        $payout_batch_id = $responce->batch_header->payout_batch_id;
        if(empty($payout_batch_id)){

            $error = str_replace("_"," ",$responce->error);
            $details = array('status'=>0,'msg'=>"Paypal ".$error);
            echo json_encode($details);exit; 
        }

        $paypal_fees = ($amount_for_sp*2.90/100)+0.30;  // paypal fee

        $user_will_get = number_format((float)$amount_for_sp-$paypal_fees , 2, '.', ''); 


        $array_insert = array(
            'order_id'=>$order['id'],
            'transaction_id'=>'',
            'total_order_amount'=>$order['total_amount'],
            'paid_amount'=>$amount_for_sp,
            'user_will_get'=>$user_will_get,
            'paypal_fee_amount'=>$paypal_fees,
            'payout_batch_id'=>$payout_batch_id,
            'admin_commission_amount'=>$admincommissionvalue,
            'customer_id'=>$payment_detail['customer_id'],
            'service_provider_id'=>$services['user_id'],
            'payment_type'=>'Order',
            'payment_status'=>'Pending',
            'transfer_type'=>'Admin_To_Service_Provider', 
            'created'=>date('Y-m-d H:i:s'),
            'ip_address'=>$this->input->ip_address(), 
        ); 
        $this->Common_model->addEditRecords('payment_history',$array_insert); 

        $this->Common_model->addEditRecords('events',array('is_canceled'=>'1','canceled_by'=>$user_id),array('id'=>$order['event_id'])); 
        $this->Common_model->addEditRecords('orders',array('order_status'=>'Canceled','cancel_date'=>date('Y-m-d'),'canceled_by'=>$user_id,'modified'=>date('Y-m-d H:i:s')),array('id'=>$order['id'])); 
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return 0;
        } else {
            $this->db->trans_commit();
            $this->notify_event_attendee($order['user_id'],$order['event_id']);
            return 1;
        }
    }

    public function complete_payment($order_id){
       
        $order=$this->Common_model->getRecords('orders','*',array('id'=>$order_id),'id DESC',true);
        // echo "<pre>";print_r($order);die;
        $services =  $this->Common_model->getRecords('services','user_id',array('id'=>$order['service_id']),'id DESC',true);
        // echo "<pre>";print_r($services);die;

        $payment_detail=$this->Common_model->getRecords('payment_history','*',array('order_id'=>$order_id,'transfer_type'=>'Customer_To_Admin'),'id DESC',true);
        
// echo "<pre>";print_r($payment_detail);die;
        // Get Admin Commistion
        $getAdmincommission =  $this->Common_model->getFieldValue('settings','commission');
        $admincommissionvalue = ($payment_detail['user_will_get']*$getAdmincommission)/100;
        $admincommissionvalue = number_format((float)$admincommissionvalue  , 2, '.', '');
        $payment_for_service_provider =number_format((float) $payment_detail['user_will_get']-$admincommissionvalue  , 2, '.', '');
        // Send Amount To  Service Provider 
        // echo $services['user_id'];die;
        $service_provider_detail=$this->Common_model->getRecords('users','email,paypal_id',array('user_id'=>$services['user_id']),'user_id DESC',true);

       // echo $this->db->last_query();exit;
        $data_for_paypal['email_subject']= 'Services Amount';
        $data_for_paypal['email_message']='Services Amount';
        $paypal_id = $service_provider_detail['paypal_id'];
        if(empty($paypal_id)){
                $response =  array('status'=>0,'msg'=>'Service provider paypal id not found.');  
                echo json_encode($response); exit;
        }

        $responce =  paypal_amount_send($payment_for_service_provider,ADMIN_CURRENCY_CODE,$paypal_id,$data_for_paypal);
        //echo '<pre>';print_r($responce);exit;
        if(!isset($responce->batch_header->payout_batch_id) || empty($responce->batch_header->payout_batch_id)){
            $error = str_replace("_"," ",$responce->error);
            $details = array('status'=>0,'msg'=>"Paypal ".$error);
            echo json_encode($details);exit; 
        }
          $payout_batch_id = $responce->batch_header->payout_batch_id;
             if(empty($payout_batch_id)){

                $error = str_replace("_"," ",$responce->error);
                $details = array('status'=>0,'msg'=>"Paypal ".$error);
                echo json_encode($details);exit; 
             }

        $paypal_fees = ($payment_for_service_provider*2.90/100)+0.30;  // paypal fee

        $user_will_get = number_format((float)$payment_for_service_provider-$paypal_fees , 2, '.', ''); 

        $array_insert = array(
            'order_id'=>$order['id'],
            'transaction_id'=>'',
            'total_order_amount'=>$order['total_amount'],
            'paid_amount'=>$payment_for_service_provider,
            'user_will_get'=>$user_will_get,
            'paypal_fee_amount'=>$paypal_fees,
            'payout_batch_id'=>$payout_batch_id,
            'admin_commission_amount'=>$admincommissionvalue,
            'customer_id'=>$payment_detail['customer_id'],
            'service_provider_id'=>$services['user_id'],
            'payment_type'=>'Order',
            'payment_status'=>'Pending',
            'transfer_type'=>'Admin_To_Service_Provider', 
            'created'=>date('Y-m-d H:i:s'),
            'ip_address'=>$this->input->ip_address(), 
        );
        $this->db->trans_begin();
        $this->Common_model->addEditRecords('payment_history',$array_insert);
        $this->Common_model->addEditRecords('events',array('is_completed'=>'1'),array('id'=>$order['event_id'])); 
        $this->Common_model->addEditRecords('orders',array('order_status'=>'Completed'),array('id'=>$order['id']));
        $this->db->trans_complete();
        if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            return 0;
        } else {
            $this->db->trans_commit();
            return 1;
        } 
    }
    public function get_user_by_id($id,$user_type) 
    {

        $sel="U.*,C.name as country,S.name as state,UA.country as country_id,UA.state as state_id,UA.city as city_id,CI.name as city,UA.address,UA.latitude,UA.longitude"; 
        if($user_type!=1){
        $sel.=",ifnull(CAST((select if(avg(rating) is null,'0',avg(rating)) from reviews where receiver_id='".$id."' and status='Active' and is_deleted=0) AS DECIMAL(4,1)),0) as rating";
        }
        $this->db->select($sel);
        $this->db->from('users U');
        $this->db->join('user_address UA','UA.user_id=U.user_id','inner');
        $this->db->join('countries C','UA.country=C.id','inner');
        $this->db->join('states S','UA.state=S.id','inner');
        $this->db->join('cities CI','UA.city=CI.id','inner');
        $this->db->where('U.status','Active');
        $this->db->where('U.is_deleted',0);
        $this->db->where('U.user_id',$id);
        $query=$this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    }
    public function getOrderItem($order_id) 
    {
        $today_date = date('Y-m-d H:i:s');
        $where="order_details.id !=0 and order_details.is_deleted=0 and order_details.order_id=".$order_id; 
        $this->db->select("item_name,price,qty,total_qty,customer_qty,unit,total_price,sell_price,tax,discount"); 
        $this->db->from('order_details'); 
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        }  
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;       
       
    }     
    public function getPaymentdetails($order_id,$type) 
    {
        $where="events.id !=0 and events.is_deleted=0 and payment_history.transfer_type= '".$type."' and payment_history.order_id=".$order_id;
        $today_date = date('Y-m-d H:i:s'); 
        
        $this->db->select(" 
            payment_history.id,
            payment_history.order_id,
            orders.order_number ,
            payment_history.total_order_amount,
            payment_history.paid_amount ,
            payment_history.transaction_id ,
            payment_history.created ,
            payment_history.payment_type ,
            sender.fullname as sender_fullname,
            sender.mobile as sender_mobile,
            receiver.fullname as receiver_fullname,
            receiver.mobile as receiver_mobile,
            orders.payment_date ,
            orders.order_status ,
            orders.cancel_date ,
            orders.cancel_reason ,
            orders.canceled_by ,
            orders.ip_address ,
            orders.email , 
            orders.mobile , 
            orders.payment_status , 
            events.event_title,events.event_address,events.event_number,events.members,events.note,events.event_date,event_type,countries.name as country_name,states.name as state_name,cities.name as city_name,if(events.is_completed=1,'Completed',if(events.is_canceled=1,'Canceled',if(events.event_date>'".$today_date."','Not started','On Going'))) as event_status,sp.fullname as sp_name,cancel.fullname as cancel_name,(select service_title from services where services.id =events.service_id) as service_title
            ");
        $this->db->from('payment_history'); 
        $this->db->join('orders','orders.id=payment_history.order_id');
        $this->db->join('events','events.id=orders.event_id');
        $this->db->join('countries','countries.id=events.country_id','left');
        $this->db->join('states','states.id=events.state_id','left');
        $this->db->join('cities','cities.id=events.city_id','left');
        $this->db->join('users sender','sender.user_id=payment_history.customer_id');
        $this->db->join('users receiver','receiver.user_id=payment_history.service_provider_id');
        $this->db->join('users sp','sp.user_id=events.sp_id'); 
        $this->db->join('users cancel','cancel.user_id=orders.canceled_by','left');
      
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        } 
    
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->row_array();
            } else return false; 
    }    
    public function send_order_invoice($order_id,$type='',$timezone='')
    {
        
        $data['order']=$this->getPaymentdetails($order_id,$type);
       // echo $this->db->last_query();exit;
        $data['order_details'] = $this->getOrderItem($order_id);
        $data['order_total'] = $this->Common_model->getRecords('order_total','*',array('order_id'=>$order_id),'',false);
        $data['timezone']=$timezone;
        //print_r($data);exit;
        $body=$this->load->view('template/event_order', $data, true);

        $from_email =getNotificationEmail();
        $to_email = $data['order']['email']; 
        if(!empty($to_email))
        {
            $subject= WEBSITE_NAME." :  Order Confirmed Order Number #".$data['order']['order_number']; 
            $this->Common_model->sendEmail($to_email,$subject,$body,$from_email);
                
        }
    }
     
}//model end

?>