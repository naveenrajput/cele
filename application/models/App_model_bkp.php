<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App_model extends CI_Model {
     
    public function __construct()
    {
        // Call the CI_Model constructor
        parent::__construct();
    }

    
    public function check_user_status() 
    {
        $user_id  =  $this->input->post('user_id');
        $mobile_auth_token  =  $this->input->post('mobile_auth_token');
        if(empty($user_id)){
            $this->display_output('0','Please enter user id.');
        }
        if(empty($mobile_auth_token)){
            $this->display_output('0','Please enter mobile auth token .');
        }

        $where['user_id'] = $user_id;
        if($user_data = $this->Common_model->getRecords('users','staging_id,mobile,mobile_auth_token,status,is_deleted',$where,'',true)) {
            
            if($user_data['status']=='Unverified'){
                $this->display_output('4','Your account is unverified!');
            }
            if($user_data['is_deleted']==1 || $user_data['status']=='Inactive'){

                $this->display_output('4','Your account has been deactivated!');
            }else{
                if(empty($user_data['mobile_auth_token'])) {
                      $this->display_output('4','You logged out from this device.');  
                } else {
                    
                    if($user_data['mobile_auth_token']!=$mobile_auth_token)
                    {
                        $this->display_output('4','You logged out from this device.'); 
                    }
                }
            }

        }else{
            $this->display_output('4','Details not found. Please contact to admin.');
        }
    }
    public function check_user_document_status() 
    {
        $user_id  =  $this->input->post('user_id');
        $where['user_id'] = $user_id;
        $user_data = $this->Common_model->getRecords('users','user_type,document_status',$where,'',true);
        if($user_data['user_type']==2 || $user_data['user_type']==3){
            if($user_data['document_status']!='Accepted'){
                $this->display_output('2','Your document is '.$user_data['document_status'].'!');
            }
        }
        
    }
    public function check_subscription_status() 
    {
        $user_id  =  $this->input->post('user_id');
        $where['user_id'] = $user_id;
        $current=date('Y-m-d');
        $subscription_data = $this->Common_model->getRecords('user_subscription_history','title,total_remaining_order,start_date,end_date,plan_orders',$where,'id desc',true);

        if($subscription_data['end_date'] < $current){
            $this->display_output('0','Your subscription plan has been expired.');
        }
        if($subscription_data['total_remaining_order']=='0'){
            $this->display_output('0','Your all subscribed orders are finished.');
        }
    }
    public function free_trial_subscription($user_id) 
    {
        if(APP_PUBLISH){
            $start_date=APP_PUBLISH_DATE;
            $end_date= date('Y-m-d', strtotime(APP_PUBLISH_DATE. ' + '.FREE_TRIAL_DAYS.' days'));
            $current=date('Y-m-d');
            $subscription_end_date=date('Y-m-d', strtotime($current. ' + '.FREE_TRIAL_DAYS.' days'));
            if(strtotime($end_date) > strtotime($current)){
                $subscription_array=array(
                    'user_id'=>$user_id,
                    'plan_id'=>1000,
                    'start_date'=>$current,
                    'end_date'=>$subscription_end_date,
                    'plan_orders'=>'Unlimited',
                    'time_type'=>'Month',
                    'time_value'=>3,
                    'total_days'=>FREE_TRIAL_DAYS,
                    'amount'=>0,
                    'title'=>'Free Trial',
                    'description'=>'Free Trial',
                    'transaction_id'=>'',
                    'transaction_fees'=>'',
                    'total_used_order'=>0,
                    'previous_remaining_order'=>0,
                    'total_remaining_order'=>0,
                    'created'=>date('Y-m-d H:i:s'),
                );
                $this->Common_model->addEditRecords('user_subscription_history',$subscription_array);
            }
        }
    }

    /////////////////////////////Service provider Section///////////////////////////
    public function get_sp_service_list($user_id,$keyword='') 
    {
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $keyword  = trim($this->input->post('keyword'));// only for category
        if($keyword){
           // $this->db->like("SC.title", trim($keyword));
            $this->db->like("S.service_title", trim($keyword));
        }
        $offset=$limit*$page;
        $sel="S.id as service_id,S.service_title,S.description,S.img1,SC.title as category,S.status,(select substring_index(group_concat(SI.item_name SEPARATOR ','), ',', 3) from services_item SI where S.id=SI.service_id and SI.status='Active' and SI.is_deleted=0) as item"; 
        $this->db->select($sel);
        $this->db->from('services S');
        $this->db->join('service_category SC','SC.id=S.category_id','inner');
        $this->db->where('S.is_deleted','0');
        $this->db->where('SC.is_deleted','0');
        //$this->db->where('SC.status','Active');
       // $this->db->where('S.status','Active');
        $this->db->where('S.user_id',$user_id);
        $this->db->order_by('S.id','desc');
        $this->db->limit($limit, $offset);
        
        $query=$this->db->get();
        // echo $this->db->last_query();die;

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
    }
    public function sp_service_details($service_id) 
    {
        $sel="S.id as service_id,S.service_title,S.category_id,S.description,S.img1,S.img2,S.img3,SC.title as category,S.status as service_status,S.user_id as sp_id, UA.country,UA.state,UA.city,UA.address,UA.latitude,UA.longitude"; 
        $this->db->select($sel);
        $this->db->from('services S');
        $this->db->join('service_category SC','SC.id=S.category_id','inner');
        $this->db->join('user_address UA','UA.user_id=S.user_id','left');
        $this->db->where('S.is_deleted','0');
        $this->db->where('SC.is_deleted','0');
        $this->db->where('S.id',$service_id);
        $this->db->where('UA.status','Primary');
        $query=$this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    }
    public function service_item_details($service_id) 
    {
        $sel="SI.id,SI.item_name,SI.price,SI.qty,U.id as unit_id,U.title as unit,SI.status as item_status"; 
        $this->db->select($sel);
        $this->db->from('services_item SI');
        $this->db->join('measurement_unit U','U.id=SI.unit','inner');
        $this->db->where('SI.is_deleted','0');
        $this->db->where('SI.service_id',$service_id);
        $query=$this->db->get();
        return $query->result_array();
    }

    public function get_sp_offer_list($user_id,$keyword='') 
    {
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $keyword  = trim($this->input->post('keyword'));// only for 
        if($keyword){
             $this->db->like("O.offer_name", trim($keyword));
             //$this->db->or_like("S.service_title", trim($keyword));
        }
        $offset=$limit*$page;
        $sel="O.id as offer_id,O.offer_name,O.description,O.start_date,O.end_date,O.discount,O.img1,O.status,S.service_title"; 
        $this->db->select($sel);
        $this->db->from('offer O');
        $this->db->join('services S','S.id=O.service_id','inner');
        $this->db->where('S.is_deleted','0');
        $this->db->where('O.is_deleted','0');
        $this->db->where('S.user_id',$user_id);
        $this->db->order_by('O.id','desc');
        $this->db->limit($limit, $offset);
        
        $query=$this->db->get();
        // echo $this->db->last_query();die;

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
    }
    public function sp_offer_details($offer_id) 
    {
        
        $sel="O.id as offer_id,O.offer_name,O.description,date(O.start_date) as start_date,date(O.end_date) as end_date,O.discount,O.img1,O.status,S.service_title,service_id"; 
        $this->db->select($sel);
        $this->db->from('offer O');
        $this->db->join('services S','S.id=O.service_id','inner');
        $this->db->where('S.is_deleted','0');
        $this->db->where('O.is_deleted','0');
        $this->db->where('O.id',$offer_id);
        $query=$this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    }
    public function check_offer($service_id,$offer_st_date,$offer_end_date,$offer_id='')
    {
        $offer_where="service_id=$service_id  and ((`start_date` between '".$offer_st_date."' and '".$offer_end_date."') OR (`end_date` between '".$offer_st_date."' and '".$offer_end_date."') OR (`start_date` <= '".$offer_st_date."' AND `end_date` >= '".$offer_end_date."')) and is_deleted=0";
        if(!empty($offer_id)){
            $offer_where.=" and id!=$offer_id";
        }
       return  $check_record=$this->Common_model->getRecords('offer', 'start_date,end_date', $offer_where, '', true);
    }


    /////////////////////////////Celebrant Section///////////////////////////
     public function celebrant_get_service_provider_list($user_id,$user_country,$user_state,$user_city){
        $today=date('Y-m-d');
        $offer  = trim($this->input->post('offer'));
        $rating  = trim($this->input->post('rating'));
        $country  = trim($this->input->post('country'));
        $state  = trim($this->input->post('state'));
        $city  = trim($this->input->post('city'));
        $service_cat  = trim($this->input->post('service_cat'));
        $where="S.id!=0";
        $csc_where="";
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $keyword  = trim($this->input->post('keyword'));// only for category
        if($keyword){
            $this->db->like("U.company_name", trim($keyword));
        }
        if(isset($service_cat) && !empty($service_cat)){
            $this->db->where('S.category_id',$service_cat);
        }
        $offset=$limit*$page;
        $sel="S.id as service_id,S.user_id as service_provider_id,S.service_title,S.img1,S.status,(SELECT substring_index(group_concat(SC.`title` SEPARATOR ', '), ', ', 3) from service_category SC inner join services Ser ON Ser.category_id= SC.id where Ser.user_id=S.user_id and  SC.status='Active' AND SC.is_deleted=0) as category,U.fullname,U.profile_picture,U.company_name";
        $sel.=",ifnull(CAST((select if(avg(rating) is null,'0',avg(rating)) from reviews where receiver_id=U.user_id and status='Active' and is_deleted=0) AS DECIMAL(4,1)),0) as rating"; 
        $this->db->select($sel);
        $this->db->from('services S');
        $this->db->join('users U','U.user_id=S.user_id','inner');
        $this->db->join('user_address UA','UA.user_id=S.user_id','left');
        $this->db->join('offer O','O.service_id=S.id','left');
        $this->db->where('S.is_deleted','0');
        $this->db->where('U.is_deleted','0');
        $this->db->where('U.status','Active');
        $this->db->where('S.user_id!=',$user_id);
        if(isset($offer) && !empty($offer)){
            $range=explode('-', $offer);
            $offer_from=$range[0];
            $offer_to=$range[1];
            $where.= " AND O.discount between '".$offer_from."' and '".$offer_to."' and date(O.end_date) > '".$today."'"; 
        }
        $this->db->where($where);

        if(isset($country) && !empty($country)){
            $countrys=explode(',', $country);
            $this->db->group_start();
            for($i=0;$i<count($countrys);$i++){

                if($i==0){
                    $this->db->where('UA.country',$countrys[$i]);
                }else{
                   $this->db->or_where('UA.country',$countrys[$i]);
                }
            } 
            $this->db->group_end();
        }
        if(isset($state) && !empty($state)){
            $states=explode(',', $state);
            $this->db->group_start();
            for($i=0;$i<count($states);$i++){
                if($i==0){
                   // $csc_where.="UA.state ='".$states[$i]."'";
                    $this->db->where('UA.state',$states[$i]);
                 }else{
                    //$csc_where.=" OR UA.state >='".$states[$i]."'";
                    $this->db->or_where('UA.state',$states[$i]);
                 }
            } 
           $this->db->group_end();
        }
        if(isset($city) && !empty($city)){
            $cities=explode(',', $city);
            $this->db->group_start();
            for($i=0;$i<count($cities);$i++){
                if($i==0){
                    $this->db->where('UA.city',$cities[$i]);
                    //$csc_where.="UA.city ='".$cities[$i]."'";
                 }else{
                    $this->db->or_where('UA.city',$cities[$i]);
                    //$csc_where.=" OR UA.city >='".$cities[$i]."'";
                 }
            } 
           $this->db->group_end();
        }


        $having ='';
        if(isset($rating) && !empty($rating)){
            $ratings=explode(',', $rating);
            for($i=0;$i<count($ratings);$i++){
                
                if($i==0){
                    $having.="rating >='".$ratings[$i]."'";
                 }else{
                    $having.=" OR rating >='".$ratings[$i]."'";
                 }
            }
             $this->db->having($having);
        }
        $this->db->group_by('S.user_id');
        $order_by="FIELD(UA.city, $user_city) DESC,FIELD(UA.state, $user_state) DESC,FIELD(UA.country, $user_country) DESC";
        $this ->db->order_by($order_by);
        $this->db->limit($limit, $offset);
        $query=$this->db->get();
    // echo $this->db->last_query();die;

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;

    }
    public function celebrant_get_service_provider_list_old3($user_id,$user_country,$user_state,$user_city){
        $today=date('Y-m-d');
        $offer  = trim($this->input->post('offer'));
        $rating  = trim($this->input->post('rating'));
        $country  = trim($this->input->post('country'));
        $state  = trim($this->input->post('state'));
        $city  = trim($this->input->post('city'));
        $where="S.id!=0";
        $csc_where="";
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $keyword  = trim($this->input->post('keyword'));// only for category
        if($keyword){
            $this->db->like("S.service_title", trim($keyword));
        }
        $offset=$limit*$page;
        $sel="S.id as service_id,S.user_id as service_provider_id,substring_index(group_concat(distinct(`S`.service_title) SEPARATOR ', '), ', ', 3) as service_title,S.img1,S.status,U.fullname,U.profile_picture,U.company_name";
        $sel.=",ifnull(CAST((select if(avg(rating) is null,'0',avg(rating)) from reviews where receiver_id=U.user_id and status='Active' and is_deleted=0) AS DECIMAL(4,1)),0) as rating"; 
        $this->db->select($sel);
        $this->db->from('services S');
        $this->db->join('users U','U.user_id=S.user_id','inner');
        $this->db->join('user_address UA','UA.user_id=S.user_id','left');
        $this->db->join('offer O','O.service_id=S.id','left');
        $this->db->where('S.is_deleted','0');
        $this->db->where('U.is_deleted','0');
        $this->db->where('U.status','Active');
        if(isset($offer) && !empty($offer)){
            $range=explode('-', $offer);
            $offer_from=$range[0];
            $offer_to=$range[1];
            $where.= " AND O.discount between '".$offer_from."' and '".$offer_to."' and date(O.end_date) > '".$today."'"; 
        }
        $this->db->where($where);

        if(isset($country) && !empty($country)){
            $countrys=explode(',', $country);
            $this->db->group_start();
            for($i=0;$i<count($countrys);$i++){

                if($i==0){
                    $this->db->where('UA.country',$countrys[$i]);
                }else{
                   $this->db->or_where('UA.country',$countrys[$i]);
                }
            } 
            $this->db->group_end();
        }
        if(isset($state) && !empty($state)){
            $states=explode(',', $state);
            $this->db->group_start();
            for($i=0;$i<count($states);$i++){
                if($i==0){
                   // $csc_where.="UA.state ='".$states[$i]."'";
                    $this->db->where('UA.state',$states[$i]);
                 }else{
                    //$csc_where.=" OR UA.state >='".$states[$i]."'";
                    $this->db->or_where('UA.state',$states[$i]);
                 }
            } 
           $this->db->group_end();
        }
        if(isset($city) && !empty($city)){
            $cities=explode(',', $city);
            $this->db->group_start();
            for($i=0;$i<count($cities);$i++){
                if($i==0){
                    $this->db->where('UA.city',$cities[$i]);
                    //$csc_where.="UA.city ='".$cities[$i]."'";
                 }else{
                    $this->db->or_where('UA.city',$cities[$i]);
                    //$csc_where.=" OR UA.city >='".$cities[$i]."'";
                 }
            } 
           $this->db->group_end();
        }


        $having ='';
        if(isset($rating) && !empty($rating)){
            $ratings=explode(',', $rating);
            for($i=0;$i<count($ratings);$i++){
                
                if($i==0){
                    $having.="rating >='".$ratings[$i]."'";
                 }else{
                    $having.=" OR rating >='".$ratings[$i]."'";
                 }
            }
             $this->db->having($having);
        }
        $this->db->group_by('S.user_id');
        $order_by="FIELD(UA.city, $user_city) DESC,FIELD(UA.state, $user_state) DESC,FIELD(UA.country, $user_country) DESC";
        $this ->db->order_by($order_by);
        $this->db->limit($limit, $offset);
        $query=$this->db->get();
    // echo $this->db->last_query();die;

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;

    }
     public function celebrant_get_service_provider_list_old2($user_id,$user_country,$user_state,$user_city){
        $today=date('Y-m-d');
        $offer  = trim($this->input->post('offer'));
        $rating  = trim($this->input->post('rating'));
        $country  = trim($this->input->post('country'));
        $state  = trim($this->input->post('state'));
        $city  = trim($this->input->post('city'));
        $where="S.id!=0";
        $csc_where="";
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $keyword  = trim($this->input->post('keyword'));// only for category
        if($keyword){
            $this->db->like("S.service_title", trim($keyword));
        }
        $offset=$limit*$page;
        $sel="S.id as service_id,S.user_id as service_provider_id,S.service_title,S.img1,SC.title as category,S.status,(select substring_index(group_concat(SI.item_name SEPARATOR ','), ',', 3) from services_item SI where S.id=SI.service_id and SI.status='Active' and SI.is_deleted=0) as item,U.fullname,U.profile_picture,U.company_name";
        $sel.=",ifnull(CAST((select if(avg(rating) is null,'0',avg(rating)) from reviews where receiver_id=U.user_id and status='Active' and is_deleted=0) AS DECIMAL(4,1)),0) as rating"; 
        $this->db->select($sel);
        $this->db->from('services S');
        $this->db->join('service_category SC','SC.id=S.category_id','inner');
        $this->db->join('users U','U.user_id=S.user_id','inner');
        $this->db->join('user_address UA','UA.user_id=S.user_id','left');
        $this->db->join('offer O','O.service_id=S.id','left');
        $this->db->where('S.is_deleted','0');
        $this->db->where('SC.is_deleted','0');
        $this->db->where('U.is_deleted','0');
        $this->db->where('U.status','Active');
        if(isset($offer) && !empty($offer)){
            $range=explode('-', $offer);
            $offer_from=$range[0];
            $offer_to=$range[1];
            $where.= " AND O.discount between '".$offer_from."' and '".$offer_to."' and date(O.end_date) > '".$today."'"; 
        }
        $this->db->where($where);

        if(isset($country) && !empty($country)){
            $countrys=explode(',', $country);
            $this->db->group_start();
            for($i=0;$i<count($countrys);$i++){

                if($i==0){
                    $this->db->where('UA.country',$countrys[$i]);
                }else{
                   $this->db->or_where('UA.country',$countrys[$i]);
                }
            } 
            $this->db->group_end();
        }
        if(isset($state) && !empty($state)){
            $states=explode(',', $state);
            $this->db->group_start();
            for($i=0;$i<count($states);$i++){
                if($i==0){
                   // $csc_where.="UA.state ='".$states[$i]."'";
                    $this->db->where('UA.state',$states[$i]);
                 }else{
                    //$csc_where.=" OR UA.state >='".$states[$i]."'";
                    $this->db->or_where('UA.state',$states[$i]);
                 }
            } 
           $this->db->group_end();
        }
        if(isset($city) && !empty($city)){
            $cities=explode(',', $city);
            $this->db->group_start();
            for($i=0;$i<count($cities);$i++){
                if($i==0){
                    $this->db->where('UA.city',$cities[$i]);
                    //$csc_where.="UA.city ='".$cities[$i]."'";
                 }else{
                    $this->db->or_where('UA.city',$cities[$i]);
                    //$csc_where.=" OR UA.city >='".$cities[$i]."'";
                 }
            } 
           $this->db->group_end();
        }


        $having ='';
        if(isset($rating) && !empty($rating)){
            $ratings=explode(',', $rating);
            for($i=0;$i<count($ratings);$i++){
                
                if($i==0){
                    $having.="rating >='".$ratings[$i]."'";
                 }else{
                    $having.=" OR rating >='".$ratings[$i]."'";
                 }
            }
             $this->db->having($having);
        }
        $this->db->group_by('S.id');
        $order_by="FIELD(UA.city, $user_city) DESC,FIELD(UA.state, $user_state) DESC,FIELD(UA.country, $user_country) DESC";
        $this ->db->order_by($order_by);
        $this->db->limit($limit, $offset);
        $query=$this->db->get();
    // echo $this->db->last_query();die;

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;

    }
    public function celebrant_get_service_provider_list_bkp($user_id,$country,$state,$city){
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $keyword  = trim($this->input->post('keyword'));// only for category
        if($keyword){
           // $this->db->like("SC.title", trim($keyword));
            $this->db->like("S.service_title", trim($keyword));
        }
        $offset=$limit*$page;
        $sel="S.id as service_id,S.user_id as service_provider_id,S.service_title,S.description,S.img1,SC.title as category,S.status,(select substring_index(group_concat(SI.item_name SEPARATOR ','), ',', 3) from services_item SI where S.id=SI.service_id and SI.status='Active' and SI.is_deleted=0) as item,U.fullname,U.profile_picture";
        $sel.=",ifnull(CAST((select if(avg(rating) is null,'0',avg(rating)) from reviews where receiver_id=U.user_id and status='Active' and is_deleted=0) AS DECIMAL(4,1)),0) as rating"; 
        $this->db->select($sel);
        $this->db->from('services S');
        $this->db->join('service_category SC','SC.id=S.category_id','inner');
        $this->db->join('users U','U.user_id=S.user_id','inner');
        $this->db->join('user_address UA','UA.user_id=S.user_id','left');
        $this->db->where('S.is_deleted','0');
        $this->db->where('SC.is_deleted','0');
        $this->db->where('U.is_deleted','0');
        $this->db->where('U.status','Active');
        $order_by="FIELD(UA.city, $city) DESC,FIELD(UA.state, $state) DESC,FIELD(UA.country, $country) DESC";
        $this ->db->order_by($order_by);
        $this->db->limit($limit, $offset);
        $query=$this->db->get();
        // echo $this->db->last_query();die;

        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;

    }

    public function cp_get_sp_details($id) 
    {
        $sel="U.fullname,U.company_name,U.about_description,U.profile_picture,UA.country,UA.state,UA.city,UA.address,UA.latitude,UA.longitude";
        $sel.=",ifnull(CAST((select if(avg(rating) is null,'0',avg(rating)) from reviews where receiver_id='".$id."' and status='Active' and is_deleted=0) AS DECIMAL(4,1)),0) as rating"; 
        $this->db->select($sel);
        $this->db->from('users U');
        $this->db->join('user_address UA','UA.user_id=U.user_id','left');
        $this->db->where('U.is_deleted','0');
        $this->db->where('U.status','Active');
        $this->db->where('U.document_status','Accepted');
        $this->db->where('U.user_id',$id);
        $this->db->where('UA.status','Primary');
        $query=$this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    }
    public function get_servies_offer($id,$local_time) 
    {
        
        $sel="S.id , S.service_title,SC.title as category,S.description,ifnull((select discount from offer O where O.status='Active' and O.is_deleted=0 AND  (`O`.`start_date` <= '".$local_time."' and `O`.`end_date` >= '".$local_time."') and O.service_id =S.id ),'')as offer_discount";
        $this->db->select($sel);
        $this->db->from('services  S');
        $this->db->join('service_category  SC','SC.id=S.category_id','left');
        $this->db->where('S.is_deleted','0');
        $this->db->where('SC.is_deleted','0');
        $this->db->where('S.status','Active');
        $this->db->where('SC.status','Active');
        $this->db->where('S.user_id',$id);
        $query=$this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
    }
    public function get_service_items($service_id) 
    {
        
        $this->db->select('SI.id as item_id,SI.item_name,SI.price,SI.qty,MU.title as unit,MU.id as unit_id');
        $this->db->from('services_item  SI');
        $this->db->join('measurement_unit  MU','MU.id=SI.unit','left');
        $this->db->where('SI.is_deleted','0');
        $this->db->where('SI.status','Active');
        $this->db->where('SI.service_id',$service_id);
        $query=$this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
    }
    public function my_bookings($user_id,$user_type,$keyword='',$status='',$is_event='') 
    {
        $today_date = date('Y-m-d H:i:s');
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $keyword  = trim($this->input->post('keyword'));// only for category
        if($keyword){
            $this->db->like("event_title", trim($keyword));
        }
        $having ='';
        if($status){
            // $having.="event_status='".(trim($status))."'";
            $all=explode(',', $status);
            if(!empty($all)){
                for($i=0;$i<count($all);$i++){
                    if($i==0){
                        $having.="event_status='".(trim($all[$i]))."'";
                     }else{
                        $having.=" OR event_status='".(trim($all[$i]))."'";
                     }
                }
                
            }
           
        }
        
        $offset=$limit*$page;
        $this->db->select("events.id as event_id,events.event_title,event_date,DATE_FORMAT(cast(events.event_date as date),'%M %d, %Y ') as event_date,O.id as order_id,O.order_status, DATE_FORMAT(cast(events.event_date as time),'%H:%i %p') as event_time,if(events.is_completed=1,'Completed',if(events.is_canceled=1,'Canceled',if(events.event_date>'".$today_date."','Not started','On Going'))) as event_status ,(SELECT image from service_category  where services.category_id=service_category.id) as image,if(events.user_id='".$user_id."',(select users.fullname from users where users.user_id=events.sp_id),(select users.fullname from users where users.user_id=events.user_id))as fullname,if(events.user_id='".$user_id."',1,0)as created_by_me");
        $this->db->from('events');
        $this->db->join('services','events.service_id=services.id','left');
        $this->db->join('orders O','O.event_id=events.id','left');
        if($user_type==1){
            $this->db->where('events.user_id',$user_id);
        }
        if($user_type==2){
            $this->db->where('events.sp_id',$user_id);
        }
        if($user_type==3){
            if(isset($is_event) && $is_event==1){
                $this->db->where('events.user_id',$user_id);
            }else{
                 $this->db->where('events.sp_id',$user_id);
            }
            
           
        }
        $this->db->where('events.type','Actual');
        if($having !="" && $having != NULL) {
            $this->db->having($having);
        } 
        $this->db->order_by('events.id','DESC');
        $this->db->limit($limit, $offset);
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
    }
    public function booking_details($event_id,$user_id,$call_type) 
    {
        
        $today_date = date('Y-m-d H:i:s');
        $this->db->select("events.id as event_id,events.user_id,events.sp_id,events.deal_id,events.is_deal_done,events.event_title,event_date,members as total_memeber,note,event_address,events.canceled_by,events.latitude,events.longitude,events.event_number,DATE_FORMAT(cast(events.event_date as date),'%M %d,%Y ') as event_date,DATE_FORMAT(cast(events.event_date as time),'%H:%i %p') as event_time,if(events.is_completed=1,'Completed',if(events.is_canceled=1,'Canceled',if(events.event_date>'".$today_date."','Not started','On Going'))) as event_status ,(SELECT image from service_category  where services.category_id=service_category.id) as image,U.fullname, U.profile_picture");
        $this->db->from('events');
        $this->db->join('users U','U.user_id=events.user_id','inner');
        $this->db->join('services','events.service_id=services.id','left');
        if($call_type=='booking'){
            $this->db->where('events.sp_id',$user_id);
        }
        $this->db->where('events.id',$event_id);
        $this->db->where('events.type','Actual');
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    }
    public function get_event_attendee($user_id,$event_id,$get_attendee){
        if($get_attendee=='all'){
            $page=trim($this->input->post('page'));
            $limit=trim($this->input->post('limit'));
            if(!$page){
                $page=0;
            }
            if(!$limit){
                $limit=10;
            }
            $offset=$limit*$page;
           $this->db->limit($limit, $offset);
        }
        $this->db->select('users.fullname,users.profile_picture,users.user_id,users.user_type');
        $this->db->from('event_attendee');
        $this->db->join('users','users.user_id=if(sender_id='.$user_id.',receiver_id,sender_id)','left');
        $this->db->where('event_attendee.status','Accepted');
        $this->db->where('event_attendee.event_id',$event_id);
        if($get_attendee=='limit'){
            $this->db->limit(3);
        }
        $query = $this->db->get(); 
        return $query->result_array();       
        /*if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;*/

    }
    public function booking_detail_order($event_id) 
    {
        $this->db->select("o.id as order_id,OSH.status,OSH.id as order_status_id,order_number");
        $this->db->from('orders o');
        $this->db->join('order_status_history OSH','OSH.order_id=o.id','inner');
        $this->db->where('o.id',$event_id);
       // $this->db->where('OSH.status','Pending');
        $this->db->order_by('OSH.order_batch_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    }
    public function celebrant_order_history_list($user_id) 
    {
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $offset=$limit*$page;
        $status  = trim($this->input->post('status'));// only for category
        if($status){
            $this->db->like("O.order_status", trim($status));
        }
        $this->db->select("P.id as booking_history_id,P.order_id,P.transaction_id,P.paid_amount,P.payment_type,P.created as transaction_date,P.payment_type,P.payment_status,U.fullname,U.profile_picture,O.id as order_id,O.order_status,(select event_title from events where events.id=O.id) as event_title");
        $this->db->from('payment_history P');
        $this->db->join('users U','U.user_id=P.service_provider_id','left');
        $this->db->join('orders O','O.id=P.order_id','left');
        $this->db->where('P.customer_id',$user_id);
        $this->db->where('P.transfer_type','Customer_To_Admin');
        $this->db->where('P.payment_status','Completed');
        $this->db->order_by('P.id','desc');
        $this->db->group_by('P.order_id');
        $this->db->limit($limit, $offset);
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
    }
    public function celebrant_order_history_details($user_id,$booking_history_id) 
    {
        $this->db->select("P.id as booking_history_id,P.order_id,P.transaction_id,P.paid_amount,P.payment_type,P.created as transaction_date,P.user_will_get,P.paypal_fee_amount,P.payment_type,U.fullname,U.profile_picture,O.order_number,O.created as order_date,O.payment_status,O.order_status,E.event_title,E.status as event_status,E.event_date,O.total_amount as order_total_amount");
        $this->db->from('payment_history P');
        $this->db->join('users U','U.user_id=P.service_provider_id','left');
        $this->db->join('orders O','O.id=P.order_id','left');
        $this->db->join('events E','E.id=O.event_id','left');
        $this->db->where('P.customer_id',$user_id);
        $this->db->where('P.transfer_type','Customer_To_Admin');
        $this->db->where('P.id',$booking_history_id);
        $this->db->order_by('P.id','desc');
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    }
     public function csp_order_history_list($user_id) 
    {
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $offset=$limit*$page;
        $status  = trim($this->input->post('status'));// only for category
        if($status){
            $this->db->like("O.order_status", trim($status));
        }
        $where="if(P.customer_id=$user_id,P.transfer_type='Customer_To_Admin',P.transfer_type='Admin_To_Service_Provider' and P.service_provider_id=$user_id)";
        $this->db->select("P.id as booking_history_id,P.order_id,P.transaction_id,P.paid_amount,P.payment_type,P.created as transaction_date,P.payment_type,P.payment_status,U.fullname,U.profile_picture,O.id as order_id,O.order_status,(select event_title from events where events.id=O.id) as event_title,if(P.customer_id=$user_id,1,0) as created_by_me");
        $this->db->from('payment_history P');
        $this->db->join('users U','U.user_id=if(P.customer_id='.$user_id.',P.service_provider_id,P.customer_id)','left');
        $this->db->join('orders O','O.id=P.order_id','left');
        $this->db->where('P.payment_status','Completed');
        $this->db->where($where);
        $this->db->order_by('P.id','desc');
        $this->db->group_by('P.order_id');
        $this->db->limit($limit, $offset);
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
    }
    public function csp_order_history_details($user_id,$booking_history_id,$created_by_me) 
    {
        $this->db->select("P.id as booking_history_id,P.order_id,P.transaction_id,P.paid_amount,P.payment_type,P.created as transaction_date,P.user_will_get,P.paypal_fee_amount,P.payment_type,U.fullname,U.profile_picture,O.order_number,O.created as order_date,O.payment_status,O.order_status,E.event_title,E.status as event_status,E.event_date,O.total_amount as order_total_amount");
        $this->db->from('payment_history P');
        $this->db->join('users U','U.user_id=if('.$created_by_me.'=0,P.customer_id,P.service_provider_id)','left');
        $this->db->join('orders O','O.id=P.order_id','left');
        $this->db->join('events E','E.id=O.event_id','left');
        if($created_by_me==0){
            $this->db->where('P.service_provider_id',$user_id);
            $this->db->where('P.transfer_type','Admin_To_Service_Provider');
        }else{
            $this->db->where('P.customer_id',$user_id);
            $this->db->where('P.transfer_type','Customer_To_Admin');
        }
       
        $this->db->where('P.id',$booking_history_id);
        $this->db->order_by('P.id','desc');
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    }
    public function sp_booking_history_list($user_id) 
    {
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $offset=$limit*$page;
        $status  = trim($this->input->post('status'));
        if($status){
            $this->db->like("O.order_status", trim($status));
        }
        $this->db->select("P.id as booking_history_id,P.order_id,P.transaction_id,P.paid_amount,P.payment_type,P.created as transaction_date,P.payment_type,U.fullname,U.profile_picture,O.id as order_id,O.order_status,(select event_title from events where events.id=O.id) as event_title");
        $this->db->from('payment_history P');
        $this->db->join('users U','U.user_id=P.customer_id','left');
        $this->db->join('orders O','O.id=P.order_id','left');
        $this->db->where('P.service_provider_id',$user_id);
        $this->db->where('P.transfer_type','Admin_To_Service_Provider');
        $this->db->where('P.payment_status','Completed');
        $this->db->order_by('P.id','desc');
        $this->db->limit($limit, $offset);
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
    }
    public function sp_booking_history_details($user_id,$user_type,$booking_history_id) 
    {
        $this->db->select("P.id as booking_history_id,P.order_id,P.transaction_id,P.paid_amount,P.payment_type,P.created as transaction_date,P.user_will_get,P.paypal_fee_amount,U.fullname,U.profile_picture,O.order_number,O.created as order_date,O.payment_status,O.order_status,E.event_title,E.status as event_status,E.event_date,O.total_amount");
        $this->db->from('payment_history P');
        $this->db->join('users U','U.user_id=P.customer_id','left');
        $this->db->join('orders O','O.id=P.order_id','left');
        $this->db->join('events E','E.id=O.event_id','left');
        $this->db->where('P.service_provider_id',$user_id);
        $this->db->where('P.transfer_type','Admin_To_Service_Provider');
        $this->db->where('P.id',$booking_history_id);
        $this->db->order_by('P.id','desc');
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    }
   
    public function get_upcoming_public_list_bkp($user_id,$city_id,$keyword='') 
    {
        $today_date = date('Y-m-d H:i:s');
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $offset=$limit*$page;
        $keyword  = trim($this->input->post('keyword'));// only for category
        if($keyword){
            $this->db->like("E.event_title", trim($keyword));
        }
        $this->db->select("E.id as event_id,E.`event_title`,E.`event_date`,E.note,U.fullname,(SELECT image from service_category  where S.category_id=service_category.id) as image,if((select EA.status from event_attendee EA where EA.event_id = E.id and EA.receiver_id='".$user_id."') IS NULL ,'',(select EA.status from event_attendee EA where EA.event_id = E.id and EA.receiver_id='".$user_id."')) as request_status");

        $this->db->from('events E');
        $this->db->join('users U','U.user_id=E.user_id','inner');
        $this->db->join('services S','E.service_id=S.id','left');
        $this->db->where('U.status','Active');
        $this->db->where('U.is_deleted',0);
       // $this->db->where('E.city_id',$city_id);
        $this->db->where('E.event_type','Public');
        $this->db->where('E.event_date >',$today_date);
        $this->db->where('E.user_id!=',$user_id);
        $this->db->where('E.type','Actual');
        $this->db->order_by('E.event_date','DESC');
        $this->db->limit($limit, $offset);
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
    }
    public function get_upcoming_public_list($user_id) 
    {
        $today_date = date('Y-m-d H:i:s');
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $offset=$limit*$page;
        $country  = $this->input->post('country');
        $state  = $this->input->post('state');
        $city  = $this->input->post('city');
        $keyword  = $this->input->post('keyword');// only for category
        if(!empty($keyword)){
            $this->db->like("E.event_title", trim($keyword));
        }
        $this->db->select("E.id as event_id,E.`event_title`,E.`event_date`,E.event_address,U.fullname,U.profile_picture,(SELECT image from service_category  where S.category_id=service_category.id) as image,EA.status as request_status");
        $this->db->from('events E');
        $this->db->join('users U','U.user_id=E.user_id','inner');
        $this->db->join('services S','E.service_id=S.id','left');
        $this->db->join('event_attendee EA','EA.event_id=E.id','inner');
        $this->db->where('U.status','Active');
        $this->db->where('U.is_deleted',0);
        $this->db->where('E.event_type','Public');
        $this->db->where('E.event_date >',$today_date);
        $this->db->where('E.user_id!=',$user_id);
        $this->db->where('E.type','Actual');
        $this->db->where('EA.sender_id',$user_id);
        if(isset($country) && !empty($country)){
            $countrys=explode(',', $country);
            $this->db->group_start();
            for($i=0;$i<count($countrys);$i++){
                if($i==0){
                    $this->db->where('E.country_id',$countrys[$i]);
                }else{
                   $this->db->or_where('E.country_id',$countrys[$i]);
                }
            } 
            $this->db->group_end();
        }
        if(isset($state) && !empty($state)){
            $states=explode(',', $state);
            $this->db->group_start();
            for($i=0;$i<count($states);$i++){
                if($i==0){
                    $this->db->where('E.state_id',$states[$i]);
                 }else{
                    $this->db->or_where('E.state_id',$states[$i]);
                 }
            } 
           $this->db->group_end();
        }
        if(isset($city) && !empty($city)){
            $cities=explode(',', $city);
            $this->db->group_start();
            for($i=0;$i<count($cities);$i++){
                if($i==0){
                    $this->db->where('E.city_id',$cities[$i]);
                 }else{
                    $this->db->or_where('E.city_id',$cities[$i]);
                 }
            } 
           $this->db->group_end();
        }
        $this->db->order_by('E.event_date','DESC');
        $this->db->limit($limit, $offset);
        $query=$this->db->get();
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }
    public function get_public_events($user_id,$user_country,$user_state,$user_city){
        $today_date = date('Y-m-d H:i:s');
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }

        $offset=$limit*$page;
        $country  = $this->input->post('country');
        $state  = $this->input->post('state');
        $city  = $this->input->post('city');
        $keyword  = $this->input->post('keyword');
        if(!empty($keyword)){
            $this->db->like("E.event_title", trim($keyword));
        }
        $this->db->select("E.id as event_id,E.`event_title`,E.`event_date`,E.event_address,U.fullname,U.profile_picture,(SELECT image from service_category  where S.category_id=service_category.id) as image,if(EA.status IS NULL,'', EA.status) as request_status");
        $where="if(EA.status IS NULL,E.is_canceled!='1','1')";
        $this->db->from('events E');
        $this->db->join('users U','U.user_id=E.user_id','inner');
        $this->db->join('services S','E.service_id=S.id','left');
        $this->db->join('event_attendee EA','EA.event_id=E.id and (EA.receiver_id='.$user_id.' OR  EA.sender_id='.$user_id.')','left',false);
        $this->db->where($where);
        $this->db->where('U.status','Active');
        $this->db->where('U.is_deleted',0);
        $this->db->where('E.event_type','Public');
        $this->db->where('E.event_date >',$today_date);
        $this->db->where('E.user_id!=',$user_id);
        $this->db->where('E.type','Actual');

        if(isset($country) && !empty($country)){
            $countrys=explode(',', $country);
            $this->db->group_start();
            for($i=0;$i<count($countrys);$i++){
                if($i==0){
                    $this->db->where('E.country_id',$countrys[$i]);
                }else{
                   $this->db->or_where('E.country_id',$countrys[$i]);
                }
            } 
            $this->db->group_end();
        }
        if(isset($state) && !empty($state)){
            $states=explode(',', $state);
            $this->db->group_start();
            for($i=0;$i<count($states);$i++){
                if($i==0){
                    $this->db->where('E.state_id',$states[$i]);
                 }else{
                    $this->db->or_where('E.state_id',$states[$i]);
                 }
            } 
           $this->db->group_end();
        }
        if(isset($city) && !empty($city)){
            $cities=explode(',', $city);
            $this->db->group_start();
            for($i=0;$i<count($cities);$i++){
                if($i==0){
                    $this->db->where('E.city_id',$cities[$i]);
                 }else{
                    $this->db->or_where('E.city_id',$cities[$i]);
                 }
            } 
           $this->db->group_end();
        }
        $order_by="E.event_date desc, FIELD(E.city_id, $user_city) DESC,FIELD(E.state_id, $user_state) DESC,FIELD(E.country_id, $user_country) DESC";
        $this ->db->order_by($order_by);
        
        $this->db->limit($limit,$offset);
        
        $query=$this->db->get();
       // echo $this->db->last_query();exit;
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }

    public function booking_summary($event_id) 
    {
        $sel="E.event_title,E.members,E.event_address,E.country_id,E.state_id,E.city_id,E.service_id,E.note,DATE_FORMAT(cast(E.event_date as date),'%M %d, %Y ') as event_date, DATE_FORMAT(cast(E.event_date as time),'%H:%i %p') as event_time, U.company_name,S.service_title,O.id as order_id"; 
        $this->db->select($sel);
        $this->db->from('events E');
        $this->db->join('services S','E.service_id=S.id','left');
        $this->db->join('users U','U.user_id=S.id','left');
        $this->db->join('orders O','O.event_id=E.id','left');
        $this->db->where('E.id',$event_id);
        $query=$this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    } 
    public function get_order_items($order_id) 
    {
        $sel="E.event_title,E.members,E.event_address,DATE_FORMAT(cast(E.event_date as date),'%M %d, %Y ') as event_date, DATE_FORMAT(cast(E.event_date as time),'%H:%i %p') as event_time, U.company_name,S.service_title,O.id as order_id"; 
        $this->db->select($sel);
        $this->db->from('events E');
        $this->db->join('services S','E.service_id=S.id','left');
        $this->db->join('users U','U.user_id=S.id','left');
        $this->db->join('orders O','O.event_id=E.id','left');
        $this->db->where('O.id',$order_id);
        $query=$this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    }    

    ////////////////////////////////////social media///////////////////////////////////////
    public function get_post_by_id($id) 
    {
       
        $sel="P.id as post_id,post_content,media_path,video_thumbnail,video_large_thumbnail,media_type,format"; 
        $this->db->select($sel);
        $this->db->from('post P');
        $this->db->join('post_media M','M.post_id=P.id','left');
        $this->db->where('P.id',$id);
        $this->db->where('P.is_deleted',0);
        $query=$this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
    }
    public function post_detail($post_id,$user_id){  
        $this->db->select('post.id as post_id,post.post_number,post.post_content,post.created,post.shared_post_id,if(post.shared_post_id>0,(select G.post_content from post G where G.id =post.shared_post_id),"") as shared_post_content,U.fullname,U.profile_picture,U.gender,if((select id from like_post where like_post.post_id=post.id and like_post.user_id="'.$user_id.'"),1,0) as is_like_by_me');
        $this->db->from('post');
        $this->db->join('users U','U.user_id=post.user_id','left');
        $this->db->where('post.id',$post_id); 
        $this->db->where('post.is_deleted','0'); 
        $query=$this->db->get();
        //echo $this->db->last_query();exit;
        return $query->row_array();
           
    } 
    public function get_friend_request_list($id,$keyword='') 
    {
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $keyword  = trim($this->input->post('keyword'));// only for category
        if($keyword){
            $this->db->like("U.fullname", trim($keyword));
        }
        $offset=$limit*$page;
        $sel="F.id as request_id,U.user_id as sent_by,U.fullname, U.profile_picture, U.gender, F.created"; 
        $this->db->select($sel);
        $this->db->from('friends F');
        $this->db->join('users U','U.user_id=F.sender_id','left');
        $this->db->where('F.receiver_id',$id);
        $this->db->where('F.status','Pending');
        $this->db->where('U.is_deleted',0);
        $this->db->where('U.status','Active');
        $this->db->order_by('F.id','desc');
        $this->db->limit($limit, $offset);
        $query=$this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
    } 
    public function get_friend_list($id,$keyword='') 
    {
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $keyword  = trim($this->input->post('keyword'));// only for category
        if($keyword){
            $this->db->like("U.fullname", trim($keyword));
        }
        $offset=$limit*$page;
      
        $sel="F.id as request_id,U.user_id as sent_by,U.fullname, U.profile_picture, U.gender, F.created,if(YEAR(CURDATE())-YEAR(dob)=YEAR(CURDATE()), '',YEAR(CURDATE()) - YEAR(dob)) AS age"; 
        $this->db->select($sel);
        $this->db->from('friends F');
        $this->db->join('users U','U.user_id=if(F.sender_id='.$id.',F.receiver_id,F.sender_id)','left');
        $this->db->where('U.is_deleted',0);
        $this->db->where('U.status','Active');
        $this->db->where('F.status','Accepted');
        $this->db->group_start();
        $this->db->where('F.receiver_id',$id);
        $this->db->or_where('F.sender_id',$id);
        $this->db->group_end();
        $this->db->order_by('U.fullname','desc');
        $this->db->limit($limit, $offset);
        $query=$this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
    }
    public function get_invite_friend_list($id,$event_id,$keyword='') 
    {
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $keyword  = trim($this->input->post('keyword'));// only for category
        if($keyword){
            $this->db->like("U.fullname", trim($keyword));
        }
        $offset=$limit*$page;
      
        $sel="F.id as request_id,U.user_id as sent_by,U.fullname, U.profile_picture, U.gender, F.created,if(E.status is null,'',E.status) as event_invitation_status"; 
        $this->db->select($sel);
        $this->db->from('friends F');
        $this->db->join('users U','U.user_id=if(F.sender_id='.$id.',F.receiver_id,F.sender_id)','left');
        $this->db->join('event_attendee E','U.user_id IN(E.sender_id,E.receiver_id) and E.event_id ='.$event_id.'','left');
        $this->db->where('U.is_deleted',0);
        $this->db->where('U.status','Active');
        $this->db->where('F.status','Accepted');
        $this->db->group_start();
        $this->db->where('F.receiver_id',$id);
        $this->db->or_where('F.sender_id',$id);
        $this->db->group_end();
        $this->db->order_by('F.id','desc');
        $this->db->limit($limit, $offset);
        $query=$this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
    }
    public function getpostcomment($post_id,$parent_id){  
        $this->db->select('comments.*,users.fullname,users.staging_id,users.profile_picture,users.status,users.gender,comments.created,(select count(comment_id) from comment_like inner join users U ON U.user_id=comment_like.user_id WHERE comment_like.comment_id =comments.id and U.is_deleted=0 and U.status="Active") as comment_like,(select count(id) from comments C inner join users U ON U.user_id=C.user_id WHERE comments.id =C.parent_id and U.is_deleted=0 and U.status="Active") as reply_count ');
        $this->db->from('comments');
        $this->db->join('post','post.id=comments.post_id');
        $this->db->join('users','users.user_id=comments.user_id');
        $this->db->where('users.is_deleted','0'); 
        $this->db->where('post.is_deleted','0'); 
        $this->db->where('comments.is_deleted','0'); 
        $this->db->where('comments.parent_id',$parent_id); 
        $this->db->where('comments.post_id',$post_id); 
        $this->db->order_by('comments.id','Desc');
       
        $query=$this->db->get(); 
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
    } 
    
    public function save_post_list($user_id){  
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $offset=$limit*$page;
        $this->db->select('save_post.id as save_post_id,post.id as post_id,post.shared_post_id,post.user_id,post.post_content,post.created,save_post.created as save_post_time,if(post.shared_post_id>0,(select G.post_content from post G where G.id =post.shared_post_id),"") as shared_post_content,U.fullname,U.profile_picture,U.gender,if((select id from like_post where like_post.post_id=post.id and like_post.user_id="'.$user_id.'"),1,0) as is_like_by_me');
        $this->db->from('save_post');
        $this->db->join('post','post.id=save_post.post_id','left');
        $this->db->join('users U','U.user_id=post.user_id','left');
        $this->db->where('save_post.user_id',$user_id); 
        $this->db->where('post.is_deleted','0'); 
        $this->db->order_by('save_post.id','Desc');
        $this->db->limit($limit, $offset);
        $query=$this->db->get(); 
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
    }
    public function get_banner($user_country,$user_state,$user_city){
        $limit=5;
        //$sel='U.user_id,U.fullname,U.profile_picture,U.company_name,S.img1,(select CAST(avg(rating) AS DECIMAL(4,1)) from reviews where reviews.receiver_id = U.user_id and status="Active") as rating';
        $sel='U.user_id,U.user_type,U.fullname,U.profile_picture,U.company_name,S.img1';
        $sel.= ',ifnull(CAST((select if(avg(rating) is null,"0",avg(rating)) from reviews where reviews.receiver_id = U.user_id and status="Active" and is_deleted=0) AS DECIMAL(4,1)),0) as rating';
        $this->db->select($sel);
        $this->db->from('users U');
        $this->db->join('user_address UA','UA.user_id=U.user_id','inner');
        $this->db->join('services S','S.user_id=U.user_id','inner');
        $this->db->group_start();
        $this->db->where('U.user_type',2);
        $this->db->or_where('U.user_type',3);
        $this->db->group_end(); 
        //$this->db->where('UA.city',$city_id); 
        $this->db->where('U.status','Active'); 
        $this->db->where('U.is_deleted',0); 
        //$this->db->order_by('rating','Desc');
        $order_by="rating Desc ,FIELD(UA.city, $user_city) DESC,FIELD(UA.state, $user_state) DESC,FIELD(UA.country, $user_country) DESC";
        $this ->db->order_by($order_by);
        $this ->db->group_by('U.user_id');
        $this->db->limit($limit);
        $query=$this->db->get(); 
        return $query->result_array();
        /*if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;*/
       

    } 
    public function get_feeds_before_sp_follow($user_id){
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $offset=$limit*$page;
       /* $sql="SELECT post.id as post_id,post.post_content,post.shared_post_id,post.user_id,post.created,friends.id as request_id,if(post.shared_post_id>0,(select G.post_content from post G where G.id =post.shared_post_id),'') as shared_post_content,users.fullname,users.profile_picture,users.gender from post left join friends ON (post.user_id=if(friends.sender_id='".$user_id."',friends.receiver_id,friends.sender_id) OR post.user_id='".$user_id."') left join users ON users.user_id = post.user_id left join hide_post H ON H.post_id= post.id and H.user_id='".$user_id."' where (friends.sender_id='".$user_id."' OR friends.receiver_id ='".$user_id."' and friends.status='Accepted') and post.is_deleted=0 and post.status='Active' and H.id IS NULL group by post.id order by post.created desc  limit ".$offset.", ".$limit." ";
        $query=$this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;*/
        $friend_where="(sender_id=$user_id OR receiver_id =$user_id) and status='Accepted'";
        $friend_check=$this->Common_model->getRecords('friends','id',$friend_where,'',true);
        if($friend_check){ 
            $sql="SELECT post.id as post_id,post.post_content,post.shared_post_id,post.user_id,post.event_id,post.post_type,post.created,friends.id as request_id,if(post.shared_post_id>0,(select G.post_content from post G where G.id =post.shared_post_id),'') as shared_post_content,(select id from like_post where like_post.post_id=post.id and like_post.user_id='".$user_id."')as is_like_by_me,users.fullname,users.profile_picture,users.gender from post left join friends ON (post.user_id=if(friends.sender_id='".$user_id."',friends.receiver_id,friends.sender_id) OR post.user_id='".$user_id."') left join users ON users.user_id = post.user_id left join hide_post H ON H.post_id= post.id and H.user_id='".$user_id."' where (friends.sender_id='".$user_id."' OR friends.receiver_id ='".$user_id."' and friends.status='Accepted') and post.is_deleted=0 and post.status='Active' and H.id IS NULL group by post.id order by post.created desc  limit ".$offset.", ".$limit." ";
                $query=$this->db->query($sql);
                if ($query->num_rows() > 0) {
                    return $query->result_array();
                } else return false;
        }else{
            $sql="SELECT post.id as post_id,post.post_content,post.shared_post_id,post.event_id,post.post_number,post.post_type,post.user_id,post.created,'0'as request_id,if(post.shared_post_id>0,(select G.post_content from post G where G.id =post.shared_post_id),'') as shared_post_content,(select id from like_post where like_post.post_id=post.id and like_post.user_id='".$user_id."') as is_like_by_me,users.fullname,users.profile_picture,users.gender from post left join users ON users.user_id = post.user_id left join hide_post H ON H.post_id= post.id and H.user_id='".$user_id."' where post.user_id= ".$user_id." and post.is_deleted=0 and post.status='Active' and H.id IS NULL group by post.id order by post.created desc  limit ".$offset.", ".$limit." ";
                $query=$this->db->query($sql);
                if ($query->num_rows() > 0) {
                    return $query->result_array();
                } else return false;
        }

    }
    public function get_feeds($user_id){
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $offset=$limit*$page;
       /* $sql="SELECT post.id as post_id,post.post_content,post.shared_post_id,post.user_id,post.created,friends.id as request_id,if(post.shared_post_id>0,(select G.post_content from post G where G.id =post.shared_post_id),'') as shared_post_content,users.fullname,users.profile_picture,users.gender from post left join friends ON (post.user_id=if(friends.sender_id='".$user_id."',friends.receiver_id,friends.sender_id) OR post.user_id='".$user_id."') left join users ON users.user_id = post.user_id left join hide_post H ON H.post_id= post.id and H.user_id='".$user_id."' where (friends.sender_id='".$user_id."' OR friends.receiver_id ='".$user_id."' and friends.status='Accepted') and post.is_deleted=0 and post.status='Active' and H.id IS NULL group by post.id order by post.created desc  limit ".$offset.", ".$limit." ";
        $query=$this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;*/
        $friend_where="(sender_id=$user_id OR receiver_id =$user_id) and status='Accepted'";
        $friend_check=$this->Common_model->getRecords('friends','id',$friend_where,'',true);
        if($friend_check){ 
            $sql="SELECT post.id as post_id,post.post_content,post.shared_post_id,post.user_id,post.event_id,post.post_type,post.created,post.sp_id,F.id as follower_id,friends.id as request_id,if(post.shared_post_id>0,(select G.post_content from post G where G.id =post.shared_post_id),'') as shared_post_content,(select id from like_post where like_post.post_id=post.id and like_post.user_id='".$user_id."')as is_like_by_me,if(F.id>0,users.company_name,users.fullname) as fullname,users.profile_picture,users.gender from post 

                left join friends ON (post.user_id=if(friends.sender_id='".$user_id."',friends.receiver_id,friends.sender_id) OR post.user_id='".$user_id."') 
                left join follow_users F ON
                if((select is_completed from events E where E.id = post.event_id)=1, post.sp_id = F.follow_to and F.followed_by ='".$user_id."', post.sp_id = 0)

                left join users ON users.user_id = post.user_id 

                left join hide_post H ON H.post_id= post.id and H.user_id='".$user_id."' 
                where ((friends.sender_id='".$user_id."' OR friends.receiver_id ='".$user_id."') and friends.status='Accepted') and post.is_deleted=0 and post.status='Active' and H.id IS NULL   OR (F.followed_by='".$user_id."' OR (post_type=2 and is_private=0) ) and H.id IS NULL group by post.id order by post.created desc limit ".$offset.", ".$limit." ";
                $query=$this->db->query($sql);
                if ($query->num_rows() > 0) {
                    return $query->result_array();
                } else return false;
        }else{
            $sql="SELECT post.id as post_id,post.post_content,post.shared_post_id,post.event_id,post.post_number,post.post_type,post.user_id,post.created,post.sp_id,F.id as follower_id,'0'as request_id,if(post.shared_post_id>0,(select G.post_content from post G where G.id =post.shared_post_id),'') as shared_post_content,(select id from like_post where like_post.post_id=post.id and like_post.user_id='".$user_id."') as is_like_by_me,if(F.id>0,users.company_name,users.fullname) as fullname,users.profile_picture,users.gender from post 

                left join follow_users F ON
                if((select is_completed from events E where E.id = post.event_idleft)=1,  post.sp_id = F.follow_to and F.followed_by ='".$user_id."', post.sp_id = 0)                                                                                                           
                left join users ON users.user_id = post.user_id 
                left join hide_post H ON H.post_id= post.id and H.user_id='".$user_id."'

                where post.user_id= ".$user_id." and post.is_deleted=0 and post.status='Active' and H.id IS NULL OR (F.followed_by='".$user_id."' OR (post_type=2 and is_private=0) ) and H.id IS NULL group by post.id order by post.created desc  limit ".$offset.", ".$limit." ";
                $query=$this->db->query($sql);
                if ($query->num_rows() > 0) {
                    return $query->result_array();
                } else return false;
        }

    }
    public function my_post($user_id){
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $offset=$limit*$page;
        $sql="SELECT post.id as post_id,post.post_content,post.shared_post_id,post.user_id,post.created,if(post.shared_post_id>0,(select G.post_content from post G where G.id =post.shared_post_id),'') as shared_post_content,users.fullname,users.profile_picture,users.gender,(select id from like_post where like_post.post_id=post.id and like_post.user_id='".$user_id."')as is_like_by_me from post 
            left join users ON users.user_id = post.user_id where  post.is_deleted=0  and post.user_id =$user_id group by post.id order by post.created desc  limit ".$offset.", ".$limit." ";
        $query=$this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;

    }
    public function get_count($table,$where){
        $this->db->select('count(id) as count');
        $this->db->from($table);
        $this->db->join('users U','U.user_id='.$table.'.user_id','left');
        $this->db->where('U.status','Active'); 
        $this->db->where('U.is_deleted','0'); 
        if($table=='comments'){
            $this->db->where('comments.is_deleted','0'); 
        }
        $this->db->where($where); 
        $query=$this->db->get(); 
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    }
    public function get_like_users($table,$where){
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $offset=$limit*$page;
        $this->db->select('U.fullname,U.user_id,U.gender,U.profile_picture');
        $this->db->from($table);
        $this->db->join('users U','U.user_id='.$table.'.user_id','left');
        $this->db->where('U.status','Active'); 
        $this->db->where('U.is_deleted','0'); 
        $this->db->where($where);
        $this->db->limit($limit, $offset); 
        $query=$this->db->get(); 
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
    }
    public function event_request_received_list($id,$keyword='') 
    {
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $keyword  = trim($this->input->post('keyword'));// only for user fullname
        if($keyword){
            $this->db->like("U.fullname", trim($keyword));
        }
        $offset=$limit*$page;
        $sel="EA.id as request_id,EA.event_id,E.event_title,EA.status,U.user_id as sent_by,U.fullname, U.profile_picture, U.gender,EA.event_owner,EA.created";
        $this->db->select($sel);        
        $this->db->from('event_attendee EA');
        $this->db->join('events E','E.id=EA.event_id','inner');
        $this->db->join('users U','U.user_id=EA.sender_id','inner');
        $this->db->group_start();
        $this->db->where('EA.receiver_id',$id);
        //$this->db->or_where('EA.sender_id',$id);
        $this->db->group_end();
        $this->db->where('U.is_deleted',0);
        $this->db->where('E.status','Active');
        $this->db->where('E.is_deleted',0);
        $this->db->where('U.status','Active');
        //$this->db->where('EA.event_owner','Self');
        $this->db->order_by('EA.id','desc');
        $this->db->limit($limit, $offset);
        $query=$this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
    }
    public function get_user_by_id($id,$user_type) 
    {

        $sel="U.*,C.name as country,S.name as state,UA.country as country_id,UA.state as state_id,UA.city as city_id,CI.name as city,UA.address,UA.latitude,UA.longitude"; 
        if($user_type!=1){
        $sel.=",ifnull(CAST((select if(avg(rating) is null,'0',avg(rating)) from reviews where receiver_id='".$id."' and status='Active' and is_deleted=0) AS DECIMAL(4,1)),0) as rating";
        }
        $this->db->select($sel);
        $this->db->from('users U');
        $this->db->join('user_address UA','UA.user_id=U.user_id','inner');
        $this->db->join('countries C','UA.country=C.id','inner');
        $this->db->join('states S','UA.state=S.id','inner');
        $this->db->join('cities CI','UA.city=CI.id','inner');
        $this->db->where('U.status','Active');
        $this->db->where('U.is_deleted',0);
        $this->db->where('U.user_id',$id);
        $query=$this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    }
    public function get_current_subscription($id) 
    {

        $sel="H.plan_id,S.title as subscription_plan"; 
        $this->db->select($sel);
        $this->db->from('user_subscription_history H');
        $this->db->join('subscriptions S','H.plan_id=S.id','inner');
        $this->db->where('H.user_id',$id);
        $this->db->order_by('H.id','desc');
        $this->db->limit(1);
        $query=$this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    }
    public function be_my_date_list($user_id,$name,$age,$address,$gender) 
    {
        $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);
        $user_country=$user_address['country'];
        $user_state=$user_address['state'];
        $user_city=$user_address['city'];
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $offset=$limit*$page;
        /*if($keyword){
            $this->db->like("U.fullname", trim($keyword));
        }
        $this->db->select('U.user_id,U.fullname,U.user_id,U.gender,UA.address,if(YEAR(CURDATE()) - YEAR(dob)=YEAR(CURDATE()),"",YEAR(CURDATE()) - YEAR(dob)) AS age');
        $this->db->from('users U');
        $this->db->join('user_address UA','UA.user_id=U.user_id','inner');
        $this->db->where('U.status','Active'); 
        $this->db->where('U.is_deleted','0'); 
        $this->db->where('U.user_id!=',$user_id); 
        $this->db->where('U.user_type!=','2'); 
        $this->db->where('UA.status','Primary'); 
        $this->db->group_by('U.user_id');
        $this->db->limit($limit, $offset); 
        $query=$this->db->get(); 
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;*/


        $where="(b.id IS NULL OR b.status <> 'Accepted') and
            D.id IS NULL and
            U.user_id <> $user_id and
            U.user_type!=2 
             AND UA.status='Primary'";

        if($name){
            $where.=" and U.fullname LIKE '%".$this->db->escape_like_str(trim($name))."%' ";
        }
        if($age){
             $where.=" and (YEAR(CURDATE()) - YEAR(dob))= $age";
        }
        if($address){
            $where.=" and UA.address LIKE '%".$this->db->escape_like_str(trim($address))."%' ";
        }if($gender){
            $where.=" and U.gender='".trim($gender)."'";
        }
        $where.=" and U.status='Active' and U.is_deleted=0";
        $sql="SELECT U.user_id,U.fullname,U.profile_picture,U.gender,UA.address,if(YEAR(CURDATE())-YEAR(dob)=YEAR(CURDATE()), '',YEAR(CURDATE()) - YEAR(dob)) AS age,if(b.id ,1,0) as is_friend_req_sent
            FROM users U 
            left join  be_my_date_hide D ON (U.user_id = D.hide_to  and D.hide_by=$user_id)
            left join user_address UA ON UA.user_id= U.user_id LEFT JOIN friends b ON 
            U.user_id IN (b.sender_id, b.receiver_id) AND
            $user_id IN (b.sender_id, b.receiver_id)
            WHERE $where
            group by U.user_id
            order by U.fullname asc , FIELD(UA.city, $user_city) DESC,FIELD(UA.state, $user_state) DESC,FIELD(UA.country, $user_country) DESC
            
            limit $offset,$limit";
            $query=$this->db->query($sql);
            //echo $this->db->last_query();exit;
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        
    } 
    /////////////////////////////////////////ORDER/////////////////////////////////////////
    public function getPaymentdetails_bkp($order_id,$type) 
    {
        $where="events.id !=0 and events.is_deleted=0 and payment_history.transfer_type= '".$type."' and payment_history.order_id=".$order_id;
        $today_date = date('Y-m-d H:i:s'); 
        
        $this->db->select(" 
            payment_history.id,
            payment_history.order_id,
            orders.order_number ,
            payment_history.total_order_amount,
            payment_history.paid_amount ,
            payment_history.transaction_id ,
            payment_history.created ,
            payment_history.payment_type ,
            sender.fullname as sender_fullname,
            sender.mobile as sender_mobile,
            receiver.fullname as receiver_fullname,
            receiver.mobile as receiver_mobile,
            orders.payment_date ,
            orders.order_status ,
            orders.cancel_date ,
            orders.cancel_reason ,
            orders.canceled_by ,
            orders.ip_address ,
            orders.email , 
            orders.mobile , 
            orders.payment_status , 
            events.event_title,events.event_address,events.event_number,events.members,events.note,events.event_date,event_type,countries.name as country_name,states.name as state_name,cities.name as city_name,if(events.is_completed=1,'Completed',if(events.is_canceled=1,'Canceled',if(events.event_date>'".$today_date."','Not started','On Going'))) as event_status,sp.fullname as sp_name,cancel.fullname as cancel_name,(select service_title from services where services.id =events.service_id) as service_title
            ");
        $this->db->from('payment_history'); 
        $this->db->join('orders','orders.id=payment_history.order_id');
        $this->db->join('events','events.id=orders.event_id');
        $this->db->join('countries','countries.id=events.country_id','left');
        $this->db->join('states','states.id=events.state_id','left');
        $this->db->join('cities','cities.id=events.city_id','left');
        $this->db->join('users sender','sender.user_id=payment_history.customer_id');
        $this->db->join('users receiver','receiver.user_id=payment_history.service_provider_id');
        $this->db->join('users sp','sp.user_id=events.sp_id'); 
        $this->db->join('users cancel','cancel.user_id=orders.canceled_by','left');
      
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        } 
    
            $query = $this->db->get();        
            if ($query->num_rows() > 0) {
                return $query->row_array();
            } else return false; 
    }    
    public function getOrderItem_bkp($order_id) 
    {
        $today_date = date('Y-m-d H:i:s');
        $where="order_details.id !=0 and order_details.is_deleted=0 and order_details.order_id=".$order_id; 
        $this->db->select("item_name,price,qty,total_qty,customer_qty,unit,total_price,sell_price,tax,discount"); 
        $this->db->from('order_details'); 
        if($where !="" && $where != NULL) {
            $this->db->where($where);
        }  
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;       
       
    }     
    public function send_order_invoice_bkp($order_id,$type='',$timezone='')
    {
        
        $data['order']=$this->getPaymentdetails($order_id,$type);
       // echo $this->db->last_query();exit;
        $data['order_details'] = $this->getOrderItem($order_id);
        $data['order_total'] = $this->Common_model->getRecords('order_total','*',array('order_id'=>$order_id),'',false);
        $data['timezone']=$timezone;
        //print_r($data);exit;
        $body=$this->load->view('template/event_order', $data, true);

        $from_email =getNotificationEmail();
        $to_email = $data['order']['email']; 
        if(!empty($to_email))
        {
            $subject= WEBSITE_NAME." :  Order Confirmed Order Number #".$data['order']['order_number']; 
            $this->Common_model->sendEmail($to_email,$subject,$body,$from_email);
                
        }
    }
    /*****************************************Deals *************************************/
    public function get_deals($user_id,$user_type,$keyword='') 
    {
        $today_date = date('Y-m-d H:i:s');
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        if($keyword){
            $this->db->like("event_title", trim($keyword));
        }
        //echo '<pre>';print_r($status);
       
        $offset=$limit*$page;
        $this->db->select("U.fullname,U.profile_picture,events.id as event_id,events.sp_id,events.user_id,event_title,event_date,O.id as order_id,O.order_status,events.is_deal_done,(SELECT image from service_category  where services.category_id=service_category.id) as image");
        $this->db->from('events');
        $this->db->join('services','events.service_id=services.id','left');
        $this->db->join('orders O','O.event_id=deal_id','left');
        $this->db->join('users U','U.user_id=if(events.user_id="'.$user_id.'",events.sp_id,events.user_id)','left');
        if($user_type==2){
            $this->db->where('events.sp_id',$user_id);
        }
        $this->db->where('events.deal_id!=',0);
       /* if($having !="" && $having != NULL) {
            $this->db->having($having);
        } */
        $this->db->order_by('events.id','DESC');
        $this->db->limit($limit,$offset);
        $query=$this->db->get();
       // echo $this->db->last_query();exit;
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            return $query->result_array();
        }
    }
    public function deals_details($event_id,$user_id,$call_type) 
    {
        $user_type= $this->input->post('user_type');

        $today_date = date('Y-m-d H:i:s');
        $this->db->select("events.id as event_id,events.user_id,events.event_title,event_date,members as total_memeber,note,event_address,events.latitude,events.longitude,events.event_number,events.canceled_by,events.sp_id,events.is_deal_done,DATE_FORMAT(cast(events.event_date as date),'%M %d,%Y ') as event_date,DATE_FORMAT(cast(events.event_date as time),'%H:%i %p') as event_time,if(events.is_completed=1,'Completed',if(events.is_canceled=1,'Canceled',if(events.event_date>'".$today_date."','Not started','On Going'))) as event_status ,(SELECT image from service_category  where services.category_id=service_category.id) as image,U.fullname, U.profile_picture");
        $this->db->from('events');
        $this->db->join('users U','U.user_id=events.user_id','inner');
        $this->db->join('services','events.service_id=services.id','left');
        if($user_type==2){
            if($call_type=='booking'){
                $this->db->where('events.sp_id',$user_id);
            }
        }
        $this->db->where('events.id',$event_id);
        //$this->db->where('events.type','Actual');
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    }
    /////////////////////////////////////////////////////////////////////////////////////// 
    ///////////////////////////////////////////////////////////////////////////////////////
    /*================================ Generate Random String ============================*/
    public function generateRandomString($length = 12) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    public function display_output($status,$msg,$data=array()){

        $response =  array('status'=>$status,'msg'=>$msg);
        if(!empty($data)){
           $response= array_merge($response,$data);
        }
        echo json_encode($response); 
        exit;
    }

}





