<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Explore_model extends CI_Model {

    public function __construct()
    {
        parent:: __construct();
    }
    
    public function celebrant_get_service_provider_list($limit,$offset,$user_id,$user_country,$user_state,$user_city,$service_cat=""){
        $today=date('Y-m-d');
        $offer  = $this->input->post('offer');
        $rating  = $this->input->post('ratings');
        $countrys  = $this->input->post('country_id');
        $states  = $this->input->post('state_id');
        $cities  = $this->input->post('city_id');
        $service_cat  = $this->input->post('service_cat');
        $where="S.id!=0";
        $csc_where="";
        $keyword  = $this->input->post('keyword');// only for category
        if(!empty($keyword)){
            $this->db->like("U.company_name", trim($keyword));
        }
        $sel="S.id as service_id,S.user_id as service_provider_id,S.service_title,S.img1,S.status,(SELECT substring_index(group_concat(SC.`title` SEPARATOR ', '), ', ', 3) from service_category SC inner join services Ser ON Ser.category_id= SC.id where Ser.user_id=S.user_id and  SC.status='Active' AND SC.is_deleted=0) as category,U.fullname,U.profile_picture,U.company_name";
        $sel.=",ifnull(CAST((select if(avg(rating) is null,'0',avg(rating)) from reviews where receiver_id=U.user_id and status='Active' and is_deleted=0) AS DECIMAL(4,1)),0) as rating"; 
        $this->db->select($sel);
        $this->db->from('services S');
        $this->db->join('users U','U.user_id=S.user_id','inner');
        $this->db->join('user_address UA','UA.user_id=S.user_id','left');
        $this->db->join('offer O','O.service_id=S.id','left');
        $this->db->where('S.is_deleted','0');
        
        $this->db->where('U.is_deleted','0');
        $this->db->where('U.status','Active');
        $this->db->where('S.user_id!=',$user_id);
        if(isset($offer) && !empty($offer)){
            $range=explode('-', $offer);
            $offer_from=$range[0];
            $offer_to=$range[1];
            $where.= " AND O.discount between '".$offer_from."' and '".$offer_to."' and date(O.end_date) > '".$today."'"; 
        }
        $this->db->where($where);
        if(isset($service_cat) && !empty($service_cat)){
            $this->db->where('S.category_id',base64_decode($service_cat));
        }
        if(isset($countrys) && !empty($countrys)){
            $this->db->group_start();
            for($i=0;$i<count($countrys);$i++){
                if($i==0){
                    $this->db->where('UA.country',$countrys[$i]);
                }else{
                   $this->db->or_where('UA.country',$countrys[$i]);
                }
            } 
            $this->db->group_end();
        }
        if(isset($states) && !empty($states)){
            $this->db->group_start();
            for($i=0;$i<count($states);$i++){
                if($i==0){
                    $this->db->where('UA.state',$states[$i]);
                 }else{
                    $this->db->or_where('UA.state',$states[$i]);
                 }
            } 
           $this->db->group_end();
        }
        if(isset($cities) && !empty($cities)){
            $this->db->group_start();
            for($i=0;$i<count($cities);$i++){
                if($i==0){
                    $this->db->where('UA.city',$cities[$i]);
                 }else{
                    $this->db->or_where('UA.city',$cities[$i]);
                 }
            } 
           $this->db->group_end();
        }

        $having ='';
       
        if(isset($rating) && !empty($rating)){
            $ratings=explode(',', $rating);
            for($i=0;$i<count($ratings);$i++){
                
                if($i==0){
                   //$having.="rating >='".$ratings[$i]."'";
                    $having.="rating ='".$ratings[$i]."'";
                 }else{
                    $having.=" OR rating >='".$ratings[$i]."'";
                 }
            }
             $this->db->having($having);
        }
        $this->db->group_by('S.user_id');
        $order_by="FIELD(UA.city, $user_city) DESC,FIELD(UA.state, $user_state) DESC,FIELD(UA.country, $user_country) DESC";
        $this ->db->order_by($order_by);
        $this->db->limit($offset,$limit);
        $query=$this->db->get();
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }

    }

    /////////////////////////////////////////SP details///////////////////////////////////////
    
    public function cp_get_sp_details($id) 
    {
        $sel="U.fullname,U.company_name,U.about_description,U.profile_picture,UA.country,UA.state,UA.city,UA.address,UA.latitude,UA.longitude";
        $sel.=",ifnull(CAST((select if(avg(rating) is null,'0',avg(rating)) from reviews where receiver_id='".$id."' and status='Active' and is_deleted=0) AS DECIMAL(4,1)),0) as rating"; 
        $this->db->select($sel);
        $this->db->from('users U');
        $this->db->join('user_address UA','UA.user_id=U.user_id','left');
        $this->db->where('U.is_deleted','0');
        $this->db->where('U.status','Active');
        $this->db->where('U.document_status','Accepted');
        $this->db->where('U.user_id',$id);
        $this->db->where('UA.status','Primary');
        $query=$this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    }
    public function get_servies_offer($id,$local_time) 
    {
        
        $sel="S.id ,S.img1,S.description,S.service_title,SC.title as category,ifnull((select discount from offer O where O.status='Active' and O.is_deleted=0 AND  (`O`.`start_date` <= '".$local_time."' and `O`.`end_date` >= '".$local_time."') and O.service_id =S.id ),'')as offer_discount";
        $this->db->select($sel);
        $this->db->from('services  S');
        $this->db->join('service_category  SC','SC.id=S.category_id','left');
        $this->db->where('S.is_deleted','0');
        $this->db->where('SC.is_deleted','0');
        $this->db->where('S.status','Active');
        $this->db->where('SC.status','Active');
        $this->db->where('S.user_id',$id);
        $query=$this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
    }

    public function get_service_items($service_id) 
    {
        
        $this->db->select('SI.id as item_id,SI.item_name,SI.price,SI.qty,MU.title as unit,MU.id as unit_id');
        $this->db->from('services_item  SI');
        $this->db->join('measurement_unit  MU','MU.id=SI.unit','left');
        $this->db->where('SI.is_deleted','0');
        $this->db->where('SI.status','Active');
        $this->db->where('SI.service_id',$service_id);
        $query=$this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
    }
    public function sp_info($id) 
    {
        $sel="S.service_title,S.description,S.img1,U.company_name,S.user_id";
        $this->db->select($sel);
        $this->db->from('services S');
        $this->db->join('users U','U.user_id=S.user_id','left');
        /*$this->db->where('U.is_deleted','0');
        $this->db->where('U.status','Active');*/
        $this->db->where('S.id',$id);
        $query=$this->db->get();
        return $query->row_array();
    }
    //////////////////////////////////Events//////////////////////////////////////////

    public function get_upcoming_public_list_bkp($limit,$offset,$user_id,$user_country,$user_state,$user_city,$call_type) 
    {
        $today_date = date('Y-m-d H:i:s');
      
        $this->db->select("E.id as event_id,E.`event_title`,E.`event_date`,E.event_address,U.fullname,U.profile_picture,(SELECT image from service_category  where S.category_id=service_category.id) as image,if((select EA.status from event_attendee EA where EA.event_id = E.id and EA.receiver_id='".$user_id."') IS NULL ,'',(select EA.status from event_attendee EA where EA.event_id = E.id and EA.receiver_id='".$user_id."')) as request_status");

        $this->db->from('events E');
        $this->db->join('users U','U.user_id=E.user_id','inner');
        $this->db->join('services S','E.service_id=S.id','left');
        $this->db->where('U.status','Active');
        $this->db->where('U.is_deleted',0);
        //$this->db->where('E.city_id',$city_id);
        $this->db->where('E.event_type','Public');
        $this->db->where('E.event_date >',$today_date);
        $this->db->where('E.user_id!=',$user_id);
        $this->db->where('E.type','Actual');
        $this->db->order_by('E.event_date','DESC');

        /*$order_by="FIELD(UA.city, $user_city) DESC,FIELD(UA.state, $user_state) DESC,FIELD(UA.country, $user_country) DESC";
        $this ->db->order_by($order_by);*/
        if($call_type=='limit'){

            $this->db->limit(5,0);
        }else{
            $this->db->limit($offset,$limit);
        }
        
        $query=$this->db->get();
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }
    public function get_upcoming_public_list($limit,$offset,$user_id,$user_country,$user_state,$user_city,$call_type) 
    {
        $today_date = date('Y-m-d H:i:s');
        $countrys  = $this->input->post('country_id');
        $states  = $this->input->post('state_id');
        $cities  = $this->input->post('city_id');
        $keyword  = $this->input->post('keyword');// only for category
        if(!empty($keyword)){
            $this->db->like("E.event_title", trim($keyword));
        }
        $this->db->select("E.id as event_id,E.`event_title`,E.`event_date`,E.event_address,U.fullname,U.profile_picture,U.user_id,(SELECT image from service_category  where S.category_id=service_category.id) as image,EA.status as request_status");
        $this->db->from('events E');
        $this->db->join('users U','U.user_id=E.user_id','inner');
        $this->db->join('services S','E.service_id=S.id','left');
        $this->db->join('event_attendee EA','EA.event_id=E.id','inner');
        $this->db->where('U.status','Active');
        $this->db->where('U.is_deleted',0);
        $this->db->where('E.event_type','Public');
        $this->db->where('E.event_date >',$today_date);
        $this->db->where('E.user_id!=',$user_id);
        $this->db->where('E.type','Actual');
        $this->db->where('EA.sender_id',$user_id);
        if(isset($countrys) && !empty($countrys)){
            $this->db->group_start();
            for($i=0;$i<count($countrys);$i++){
                if($i==0){
                    $this->db->where('E.country_id',$countrys[$i]);
                }else{
                   $this->db->or_where('E.country_id',$countrys[$i]);
                }
            } 
            $this->db->group_end();
        }
        if(isset($states) && !empty($states)){
            $this->db->group_start();
            for($i=0;$i<count($states);$i++){
                if($i==0){
                    $this->db->where('E.state_id',$states[$i]);
                 }else{
                    $this->db->or_where('E.state_id',$states[$i]);
                 }
            } 
           $this->db->group_end();
        }
        if(isset($cities) && !empty($cities)){
            $this->db->group_start();
            for($i=0;$i<count($cities);$i++){
                if($i==0){
                    $this->db->where('E.city_id',$cities[$i]);
                 }else{
                    $this->db->or_where('E.city_id',$cities[$i]);
                 }
            } 
           $this->db->group_end();
        }
        $this->db->order_by('E.event_date','DESC');
        if($call_type=='limit'){

            $this->db->limit(5,0);
        }else{
            $this->db->limit($offset,$limit);
        }
        
        $query=$this->db->get();
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }
    public function get_public_events($limit,$offset,$user_id,$user_country,$user_state,$user_city) 
    {
        $today_date = date('Y-m-d H:i:s');
        $countrys  = $this->input->post('country_id');
        $states  = $this->input->post('state_id');
        $cities  = $this->input->post('city_id');
        $keyword  = $this->input->post('keyword');// only for category
        if(!empty($keyword)){
            $this->db->like("E.event_title", trim($keyword));
        }
        $this->db->select("E.id as event_id,E.`event_title`,E.`event_date`,E.event_address,U.fullname,U.profile_picture,U.user_id,(SELECT image from service_category  where S.category_id=service_category.id) as image,if(EA.status IS NULL,'', EA.status) as request_status");
        $where="if(EA.status IS NULL,E.is_canceled!='1','1')";
        $this->db->from('events E');
        $this->db->join('users U','U.user_id=E.user_id','inner');
        $this->db->join('services S','E.service_id=S.id','left');
        $this->db->join('event_attendee EA','EA.event_id=E.id and (EA.receiver_id='.$user_id.' OR  EA.sender_id='.$user_id.')','left',false);
        $this->db->where($where);
        $this->db->where('U.status','Active');
        $this->db->where('U.is_deleted',0);
        $this->db->where('E.event_type','Public');
        $this->db->where('E.event_date >',$today_date);
        $this->db->where('E.user_id!=',$user_id);
        $this->db->where('E.type','Actual');

        if(isset($countrys) && !empty($countrys)){
            $this->db->group_start();
            for($i=0;$i<count($countrys);$i++){
                if($i==0){
                    $this->db->where('E.country_id',$countrys[$i]);
                }else{
                   $this->db->or_where('E.country_id',$countrys[$i]);
                }
            } 
            $this->db->group_end();
        }
        if(isset($states) && !empty($states)){
            $this->db->group_start();
            for($i=0;$i<count($states);$i++){
                if($i==0){
                    $this->db->where('E.state_id',$states[$i]);
                 }else{
                    $this->db->or_where('E.state_id',$states[$i]);
                 }
            } 
           $this->db->group_end();
        }
        if(isset($cities) && !empty($cities)){
            $this->db->group_start();
            for($i=0;$i<count($cities);$i++){
                if($i==0){
                    $this->db->where('E.city_id',$cities[$i]);
                 }else{
                    $this->db->or_where('E.city_id',$cities[$i]);
                 }
            } 
           $this->db->group_end();
        }
        $order_by="E.event_date desc, FIELD(E.city_id, $user_city) DESC,FIELD(E.state_id, $user_state) DESC,FIELD(E.country_id, $user_country) DESC";
        $this ->db->order_by($order_by);
        
        $this->db->limit($offset,$limit);
        
        
        $query=$this->db->get();
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }



}//model end

?>