<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Store_model extends CI_Model {

    public function __construct()
    {
        parent:: __construct();
    }
    public function get_sp_service_list($limit,$offset) 
    {

        if(isset($_POST['name']) && !empty($_POST['name'])){
            $this->db->like('S.service_title',$_POST['name']);
        }
        $user_id= $this->session->userdata('user_id');
        $sel="S.id as service_id,S.service_title,S.description,S.img1,SC.title as category,S.status,(select substring_index(group_concat(SI.item_name SEPARATOR ','), ',', 3) from services_item SI where S.id=SI.service_id and SI.status='Active' and SI.is_deleted=0) as item"; 
        $this->db->select($sel);
        $this->db->from('services S');
        $this->db->join('service_category SC','SC.id=S.category_id','inner');
        $this->db->where('S.is_deleted','0');
        $this->db->where('SC.is_deleted','0');
        $this->db->where('S.user_id',$user_id);
        $this->db->order_by('S.id','desc');
        $this->db->limit($offset,$limit);
        $query=$this->db->get();
        //echo $this->db->last_query();
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }
    public function get_sp_offer_list($limit,$offset) 
    {
        $user_id= $this->session->userdata('user_id');
        if(isset($_POST['name']) && !empty($_POST['name'])){
            $this->db->like('S.service_title',$_POST['name']);
        }
        $sel="O.id as offer_id,O.offer_name,O.description,O.start_date,O.end_date,O.discount,O.img1,O.status,S.service_title"; 
        $this->db->select($sel);
        $this->db->from('offer O');
        $this->db->join('services S','S.id=O.service_id','inner');
        $this->db->where('S.is_deleted','0');
        $this->db->where('O.is_deleted','0');
        $this->db->where('S.user_id',$user_id);
        $this->db->order_by('O.id','desc');
        $this->db->limit($offset, $limit);
        $query=$this->db->get();
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }
    public function sp_service_details($service_id) 
    {
        $user_id= $this->session->userdata('user_id');
        $sel="S.id as service_id,S.service_title,S.category_id,S.description,S.img1,S.img2,S.img3,SC.title as category,S.status as service_status,UA.country,UA.state,UA.city,UA.address,UA.latitude,UA.longitude"; 
        $this->db->select($sel);
        $this->db->from('services S');
        $this->db->join('service_category SC','SC.id=S.category_id','inner');
        $this->db->join('user_address UA','UA.user_id=S.user_id','left');
        $this->db->where('S.is_deleted','0');
        $this->db->where('SC.is_deleted','0');
        $this->db->where('S.id',$service_id);
        $this->db->where('S.user_id',$user_id);
        $this->db->where('UA.status','Primary');
        $query=$this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    }
    public function service_item_details($service_id) 
    {
        $sel="SI.id,SI.item_name,SI.price,SI.qty,U.id as unit_id,U.title as unit,SI.status as item_status"; 
        $this->db->select($sel);
        $this->db->from('services_item SI');
        $this->db->join('measurement_unit U','U.id=SI.unit','inner');
        $this->db->where('SI.is_deleted','0');
        $this->db->where('SI.service_id',$service_id);
        $query=$this->db->get();
        return $query->result_array();
    }
    public function service_details_my_booking() 
    {
        $user_id= $this->session->userdata('user_id');
        $today_date = date('Y-m-d H:i:s');
        $having ='';
        $this->db->select("events.id as event_id,events.event_address,events.event_title,event_date,DATE_FORMAT(cast(events.event_date as date),'%M %d, %Y ') as event_date,O.id as order_id,O.order_status, DATE_FORMAT(cast(events.event_date as time),'%H:%i %p') as event_time,if(events.is_completed=1,'Completed',if(events.is_canceled=1,'Canceled',if(events.event_date>'".$today_date."','Not started','On Going'))) as event_status ,(SELECT image from service_category  where services.category_id=service_category.id) as image");
        $this->db->from('events');
        $this->db->join('services','events.service_id=services.id','left');
        $this->db->join('orders O','O.event_id=events.id','left');
        $this->db->where('events.sp_id',$user_id);
        
        $this->db->where('events.type','Actual');
        $having.="event_status='Not started'";
        if($having !="" && $having != NULL) {
            
            $this->db->having($having);
        } 
        $this->db->order_by('events.event_date','DESC');
        $this->db->limit(0, 5);
        $query = $this->db->get();  
        return $query->result_array();      
    }
    public function sp_offer_details($offer_id) 
    {
        $user_id= $this->session->userdata('user_id');
        $sel="O.id as offer_id,O.offer_name,O.description,date(O.start_date) as start_date,date(O.end_date) as end_date,O.discount,O.img1,O.status,S.service_title,service_id"; 
        $this->db->select($sel);
        $this->db->from('offer O');
        $this->db->join('services S','S.id=O.service_id','inner');
        $this->db->where('S.is_deleted','0');
        $this->db->where('O.is_deleted','0');
        $this->db->where('O.id',$offer_id);
        $this->db->where('S.user_id',$user_id);
        $query=$this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    }
    public function check_offer($service_id,$offer_st_date,$offer_end_date,$offer_id='')
    {
        $offer_where="service_id=$service_id  and ((`start_date` between '".$offer_st_date."' and '".$offer_end_date."') OR (`end_date` between '".$offer_st_date."' and '".$offer_end_date."') OR (`start_date` <= '".$offer_st_date."' AND `end_date` >= '".$offer_end_date."')) and is_deleted =0";
        if(!empty($offer_id)){
            $offer_where.=" and id!=$offer_id";
        }
       return  $check_record=$this->Common_model->getRecords('offer', 'start_date,end_date', $offer_where, '', true);
    }

     
}//model end

?>