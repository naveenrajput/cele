<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Booking_model extends CI_Model {

    public function __construct()
    {
        parent:: __construct();
    }
    public function my_bookings($limit,$offset,$keyword='',$status='',$is_event='') 
    {
        $user_id= $this->session->userdata('user_id');
        $user_type= $this->session->userdata('front_user_type');
        $today_date = date('Y-m-d H:i:s');
        if($keyword){
            $this->db->like("event_title", trim($keyword));
        }
        //echo '<pre>';print_r($status);
        $having ='';
        if(!empty($status)){
            // $having.="event_status='".(trim($status))."'";

                for($i=0;$i<count($status);$i++){
                    if($i==0){
                        $having.="event_status='".(trim($status[$i]))."'";
                     }else{
                        $having.=" OR event_status='".(trim($status[$i]))."'";
                     }
                }
                
           
           
        }
        
        $this->db->select("U.fullname,U.profile_picture,events.id as event_id,events.event_title,event_date,O.id as order_id,O.order_status, if(events.is_completed=1,'Completed',if(events.is_canceled=1,'Canceled',if(events.event_date>'".$today_date."','Not started','On Going'))) as event_status ,(SELECT image from service_category  where services.category_id=service_category.id) as image");
        $this->db->from('events');
        $this->db->join('services','events.service_id=services.id','left');
        $this->db->join('orders O','O.event_id=events.id','left');
        $this->db->join('users U','U.user_id=if(events.user_id="'.$user_id.'",events.sp_id,events.user_id)','left');
        if($user_type==1){
            $this->db->where('events.user_id',$user_id);
        }
        if($user_type==2){
            $this->db->where('events.sp_id',$user_id);
        }
        if($user_type==3){
            if(isset($is_event) && $is_event==1){
                $this->db->where('events.user_id',$user_id);
            }else{
                 $this->db->where('events.sp_id',$user_id);
            }
            
           
        }
        $this->db->where('events.type','Actual');
        if($having !="" && $having != NULL) {
            $this->db->having($having);
        } 
        $this->db->order_by('events.id','DESC');
        $this->db->limit($offset,$limit);
        $query=$this->db->get();
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }
    public function booking_details($event_id,$user_id,$call_type) 
    {
        $today_date = date('Y-m-d H:i:s');
        $this->db->select("events.id as event_id,events.user_id,events.event_title,event_date,members as total_memeber,note,event_address,events.latitude,events.longitude,events.event_number,events.canceled_by,events.sp_id,events.is_deal_done,if(events.is_completed=1,'Completed',if(events.is_canceled=1,'Canceled',if(events.event_date>'".$today_date."','Not started','On Going'))) as event_status ,(SELECT image from service_category  where services.category_id=service_category.id) as image,U.fullname, U.profile_picture");
        $this->db->from('events');
        $this->db->join('users U','U.user_id=events.user_id','inner');
        $this->db->join('services','events.service_id=services.id','left');
        if($call_type=='booking'){
            $this->db->where('events.sp_id',$user_id);
        }
        $this->db->where('events.id',$event_id);
        $this->db->where('events.type','Actual');
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    }
    public function booking_detail_order($event_id) 
    {
        $this->db->select("o.id as order_id,OSH.status,OSH.id as order_status_id,order_number,o.total_amount,o.cancel_date,o.sp_order_cancel_date,o.order_status,o.cancel_reason");
        $this->db->from('orders o');
        $this->db->join('order_status_history OSH','OSH.order_id=o.id','inner');
        $this->db->where('o.id',$event_id);
       // $this->db->where('OSH.status','Pending');
        $this->db->order_by('OSH.order_batch_id','desc');
        $this->db->limit(1);
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    }
    public function get_event_attendee($event_id,$status,$user_id){
        $this->db->select('users.fullname,users.profile_picture,users.user_id');
        $this->db->from('event_attendee');
        $this->db->join('users','users.user_id=if(sender_id='.$user_id.',receiver_id,sender_id)','left');
        $this->db->where('event_attendee.status',$status);
        $this->db->where('event_attendee.event_id',$event_id);
        $query = $this->db->get(); 
        return $query->result_array();       
    }
    public function check_subscription_status() 
    {
        $user_id= $this->session->userdata('user_id');
        $where['user_id'] = $user_id;
        $current=date('Y-m-d');
        $subscription_data = $this->Common_model->getRecords('user_subscription_history','title,total_remaining_order,start_date,end_date,plan_orders',$where,'id desc',true);
        //echo $this->db->last_query();exit;
        if($subscription_data['end_date'] < $current){
           display_output('0','Your subscription plan has been expired.');
        }
        if($subscription_data['total_remaining_order']=='0'){
            display_output('0','Your all subscribed orders are finished.');
        }
    }
    public function sp_booking_history_list($limit,$offset,$order_id) 
    {
        $user_id= $this->session->userdata('user_id');
        if($order_id){
            $this->db->like("O.order_number", trim($order_id));
        }
        $this->db->select("P.id as booking_history_id,P.order_id,P.transaction_id,P.paid_amount,P.payment_type,P.created as transaction_date,P.payment_type,U.fullname,U.profile_picture,O.id as order_id,O.order_number,O.order_status,(select event_title from events where events.id=O.id) as event_title");
        $this->db->from('payment_history P');
        $this->db->join('users U','U.user_id=P.customer_id','left');
        $this->db->join('orders O','O.id=P.order_id','left');
        $this->db->where('P.service_provider_id',$user_id);
        $this->db->where('P.transfer_type','Admin_To_Service_Provider');
        $this->db->where('P.payment_status','Completed');
        $this->db->order_by('P.id','desc');
        $this->db->limit($offset,$limit);
        $query=$this->db->get();
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }
    public function event_request_received_list($limit,$offset,$event_id,$user_id) 
    {
        $sel="EA.id as request_id,EA.event_id,EA.status,U.user_id as sent_by,U.fullname,U.profile_picture,U.gender,EA.event_owner,";
        $this->db->select($sel);        
        $this->db->from('event_attendee EA');
        $this->db->join('users U','U.user_id=EA.sender_id','inner');
        $this->db->where('U.is_deleted',0);
        $this->db->where('U.status','Active');
        $this->db->where('EA.receiver_id',$user_id);
        $this->db->where('EA.event_id',$event_id);
        $this->db->order_by('EA.id','desc');
        //$this->db->limit($offset,$limit);
        $query=$this->db->get();
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }
    public function get_invite_friend_list($limit,$offset,$event_id) 
    {
       
        $id= $this->session->userdata('user_id');
        $keyword  = trim($this->input->post('keyword'));// only for category
        if($keyword){
            $this->db->like("U.fullname", trim($keyword));
        }
       
        $sel="F.id as request_id,U.user_id as sent_by,U.fullname, U.profile_picture, U.gender, F.created,if(E.status is null,'',E.status) as event_invitation_status"; 
        $this->db->select($sel);
        $this->db->from('friends F');
        $this->db->join('users U','U.user_id=if(F.sender_id='.$id.',F.receiver_id,F.sender_id)','left');
        $this->db->join('event_attendee E','U.user_id IN(E.sender_id,E.receiver_id) and E.event_id ='.$event_id.'','left');
        $this->db->where('U.is_deleted',0);
        $this->db->where('U.status','Active');
        $this->db->where('F.status','Accepted');
        $this->db->group_start();
        $this->db->where('F.receiver_id',$id);
        $this->db->or_where('F.sender_id',$id);
        $this->db->group_end();
        $this->db->order_by('F.id','desc');
        $this->db->limit($offset,$limit);
        $query=$this->db->get();
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }
    public function celebrant_order_history_list($limit,$offset,$order_id) 
    {
        $user_id= $this->session->userdata('user_id');
        if($order_id){
            $this->db->like("O.order_number", trim($order_id));
        }
        $this->db->select("P.id as booking_history_id,P.order_id,P.transaction_id,P.paid_amount,P.payment_type,P.created as transaction_date,P.payment_type,P.payment_status,U.fullname,U.profile_picture,O.id as order_id,O.order_status,O.order_number,(select event_title from events where events.id=O.id) as event_title");
        $this->db->from('payment_history P');
        $this->db->join('users U','U.user_id=P.service_provider_id','left');
        $this->db->join('orders O','O.id=P.order_id','left');
        $this->db->where('P.customer_id',$user_id);
        $this->db->where('P.transfer_type','Customer_To_Admin');
        $this->db->where('P.payment_status','Completed');
        $this->db->order_by('P.id','desc');
        $this->db->group_by('P.order_id');
        $this->db->limit($offset,$limit);
        $query=$this->db->get();
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }
    public function get_deals($limit,$offset,$keyword='',$status='',$is_event='') 
    {
        $user_id= $this->session->userdata('user_id');
        $user_type= $this->session->userdata('front_user_type');
        $today_date = date('Y-m-d H:i:s');
        if($keyword){
            $this->db->like("event_title", trim($keyword));
        }
        //echo '<pre>';print_r($status);
        $having ='';
        if(!empty($status)){
            // $having.="event_status='".(trim($status))."'";

                for($i=0;$i<count($status);$i++){
                    if($i==0){
                        $having.="event_status='".(trim($status[$i]))."'";
                     }else{
                        $having.=" OR event_status='".(trim($status[$i]))."'";
                     }
                }
                
           
           
        }
        
        $this->db->select("U.fullname,U.profile_picture,U.user_id,events.id as event_id,events.event_title,event_date,O.id as order_id,O.order_status, if(events.is_completed=1,'Completed',if(events.is_canceled=1,'Canceled',if(events.event_date>'".$today_date."','Not started','On Going'))) as event_status,events.is_deal_done,(SELECT image from service_category  where services.category_id=service_category.id) as image");
        $this->db->from('events');
        $this->db->join('services','events.service_id=services.id','left');
        $this->db->join('orders O','O.event_id=deal_id','left');
        $this->db->join('users U','U.user_id=if(events.user_id="'.$user_id.'",events.sp_id,events.user_id)','left');
       /* if($user_type==1){
            $this->db->where('events.user_id',$user_id);
        }*/
        if($user_type==2){
            $this->db->where('events.sp_id',$user_id);
        }
        /*if($user_type==3){
            if(isset($is_event) && $is_event==1){
                $this->db->where('events.user_id',$user_id);
            }else{
                 $this->db->where('events.sp_id',$user_id);
            }
            
           
        }*/
        //$this->db->where('events.type','Actual');
        $this->db->where('events.deal_id!=',0);
        if($having !="" && $having != NULL) {
            $this->db->having($having);
        } 
        $this->db->order_by('events.id','DESC');
        $this->db->limit($offset,$limit);
        $query=$this->db->get();
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }
    public function deals_details($event_id,$user_id,$call_type) 
    {
        $user_type= $this->session->userdata('front_user_type');
        $today_date = date('Y-m-d H:i:s');
        $this->db->select("events.id as event_id,events.user_id,events.event_title,event_date,members as total_memeber,note,event_address,events.latitude,events.longitude,events.event_number,events.canceled_by,events.sp_id,events.is_deal_done,if(events.is_completed=1,'Completed',if(events.is_canceled=1,'Canceled',if(events.event_date>'".$today_date."','Not started','On Going'))) as event_status ,(SELECT image from service_category  where services.category_id=service_category.id) as image,U.fullname, U.profile_picture");
        $this->db->from('events');
        $this->db->join('users U','U.user_id=events.user_id','inner');
        $this->db->join('services','events.service_id=services.id','left');
        if($user_type==2){
            if($call_type=='booking'){
                $this->db->where('events.sp_id',$user_id);
            }
        }
        $this->db->where('events.id',$event_id);
        //$this->db->where('events.type','Actual');
        $query = $this->db->get();        
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    }
     
}//model end

?>