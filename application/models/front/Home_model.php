<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model {

    public function __construct()
    {
        parent:: __construct();
    }
    

    public function be_my_date_list($offset,$limit,$user_id,$type="") 
    {
        $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);
        $user_country=$user_address['country'];
        $user_state=$user_address['state'];
        $user_city=$user_address['city'];
        $name  = $this->input->post('keyword');
        $age  = $this->input->post('age');
        $address  = $this->input->post('address');
        $gender  = $this->input->post('gender');
        $page=trim($this->input->post('page'));
        $countrys  = $this->input->post('country_id');
        $states  = $this->input->post('state_id');
        $cities  = $this->input->post('city_id');
        //$limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        /*if(!$limit){
            $limit=10;
        }
        $offset=$limit*$page;
        if($type=='limit'){
            $offset=0;
            $limit=3;
        }*/
      
        $where="(b.id IS NULL OR b.status <> 'Accepted') and
            D.id IS NULL and
            U.user_id <> $user_id and
            U.user_type!=2 
             AND UA.status='Primary'";

        if($name){
            $where.=" and U.fullname LIKE '%".$this->db->escape_like_str(trim($name))."%' ";
        }
        if($age){
            $where.=" and (YEAR(CURDATE()) - YEAR(dob)) BETWEEN $age";
        }
        if($address){
            $where.=" and UA.address LIKE '%".$this->db->escape_like_str(trim($address))."%' ";
        }if($gender){
            $where.=" and U.gender='".trim($gender)."'";
        }
        $where.=" and U.status='Active' and U.is_deleted=0";
        if(isset($countrys) && !empty($countrys)){
            $where.=' and (';
            //$this->db->group_start();
            for($i=0;$i<count($countrys);$i++){
                
                if($i==0){
                 //   $this->db->where('UA.country',$countrys[$i]);
                    $where.="UA.country=$countrys[$i]";
                }else{
                  // $this->db->or_where('UA.country',$countrys[$i]);
                   $where.=" OR UA.country=$countrys[$i]";
                }
                
            } 
            $where.=')';
            //$this->db->group_end();
        }
        if(isset($states) && !empty($states)){
            $where.=' and (';
            //$this->db->group_start();
            for($i=0;$i<count($states);$i++){
                if($i==0){
                   // $this->db->where('UA.state',$states[$i]);
                    $where.="UA.state=$states[$i]";
                 }else{
                    //$this->db->or_where('UA.state',$states[$i]);
                    $where.=" OR UA.state=$states[$i]";
                 }
            } 
            $where.=')';
           //$this->db->group_end();
        }
        if(isset($cities) && !empty($cities)){
            $where.=' and (';
            //$this->db->group_start();
            for($i=0;$i<count($cities);$i++){
                if($i==0){
                   // $this->db->where('UA.city',$cities[$i]);
                    $where.="UA.city=$cities[$i]";
                 }else{
                    //$this->db->or_where('UA.city',$cities[$i]);
                    $where.=" OR UA.city=$cities[$i]";
                 }
            } 
           //$this->db->group_end();
            $where.=')';
        }

        $sql="SELECT U.user_id,U.profile_picture,b.status, U.fullname,U.gender,UA.address,if(YEAR(CURDATE())-YEAR(dob)=YEAR(CURDATE()), '',YEAR(CURDATE()) - YEAR(dob)) AS age
            FROM users U 
            left join  be_my_date_hide D ON (U.user_id = D.hide_to  and D.hide_by=$user_id)
            left join user_address UA ON UA.user_id= U.user_id LEFT JOIN friends b ON 
            U.user_id IN (b.sender_id, b.receiver_id) AND
            $user_id IN (b.sender_id, b.receiver_id)
            WHERE $where
            group by U.user_id
            order by U.fullname ASC , UA.city DESC,UA.state DESC,UA.country DESC
            ";

            
            if($limit===0 && $offset===0){     
                $query=$this->db->query($sql);
                return $query->num_rows();
            } else {
                if($type=='limit'){ 
                    $newsql = $sql.'LIMIT 0,3';
                    $query=$this->db->query($newsql);
                }else{
                    //$l = $this->db->limit($offset,$limit);
                    $newsql = $sql.'LIMIT '.$offset.','.$limit;
                    $query=$this->db->query($newsql);
                }
                if ($query->num_rows() > 0) {
                    return $query->result_array();
                } else return false;
            }
        
    } 


    public function find_friend_suggestion_list($limit,$offset,$user_country,$user_state,$user_city) 
    {
        $id= $this->session->userdata('user_id');
        $keyword  = $this->input->post('keyword');
        if($keyword){
            $this->db->like("U.fullname", trim($keyword));
        }
      
        $sel="U.user_id as sent_by,U.fullname,U.profile_picture, U.gender,if(YEAR(CURDATE())-YEAR(dob)=YEAR(CURDATE()), '',YEAR(CURDATE()) - YEAR(dob)) AS age,(select if(friends.status='Accepted',id,status) as req_id from friends where (sender_id =$id and receiver_id=U.user_id) OR (sender_id =U.user_id and receiver_id=$id)) as request_id"; 
        $this->db->select($sel);
        $this->db->from('users U');
        $this->db->join('user_address UA','UA.user_id=U.user_id','left');
        $this->db->where('U.is_deleted',0);
        $this->db->where('U.status','Active');
        $this->db->where('U.user_type!=',2);
        $this->db->where('U.user_id!=',$id);
        $order_by="FIELD(UA.city, $user_city) DESC,FIELD(UA.state, $user_state) DESC,FIELD(UA.country, $user_country) DESC";
        $this ->db->order_by($order_by);
        //$this->db->order_by('U.fullname','ASC');
        $this->db->limit($offset,$limit);
        $query=$this->db->get();
        //echo $this->db->last_query();
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }
     public function get_friend_list($limit,$offset) 
    {
        $id= $this->session->userdata('user_id');
        $keyword  = $this->input->post('keyword');
        if($keyword){
            $this->db->like("U.fullname", trim($keyword));
        }
      
        $sel="F.id as request_id,U.user_id as sent_by,U.fullname,U.profile_picture, U.gender, F.created,if(YEAR(CURDATE())-YEAR(dob)=YEAR(CURDATE()), '',YEAR(CURDATE()) - YEAR(dob)) AS age,U.user_type"; 
        $this->db->select($sel);
        $this->db->from('friends F');
        $this->db->join('users U','U.user_id=if(F.sender_id='.$id.',F.receiver_id,F.sender_id)','left');
        $this->db->where('U.is_deleted',0);
        $this->db->where('U.status','Active');
        $this->db->where('F.status','Accepted');
        $this->db->group_start();
        $this->db->where('F.receiver_id',$id);
        $this->db->or_where('F.sender_id',$id);
        $this->db->group_end();
        $this->db->order_by('U.fullname','desc');
        $this->db->limit($offset,$limit);
        $query=$this->db->get();
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }

    public function get_friend_request_list($limit,$offset,$id,$keyword='',$call='') 
    {
        
        $keyword  = trim($this->input->post('keyword'));// only for category
        if($keyword){
            $this->db->like("U.fullname", trim($keyword));
        }
        
        $sel="F.id as request_id,U.user_id as sent_by,U.fullname, U.profile_picture, U.gender, F.created"; 
        $this->db->select($sel);
        $this->db->from('friends F');
        $this->db->join('users U','U.user_id=F.sender_id','left');
        $this->db->where('F.receiver_id',$id);
        $this->db->where('F.status','Pending');
        $this->db->where('U.is_deleted',0);
        $this->db->where('U.status','Active');
        $this->db->order_by('F.id','desc');
        if ($call == 'limit') {
            $this->db->limit(0,5);
        }else{
            $this->db->limit($offset,$limit);    
        }
        
        $query=$this->db->get();
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    } 
    public function top_rated_sp($user_country,$user_state,$user_city){
        $limit=5;
        $sel='U.user_id,U.user_type,U.fullname,U.profile_picture,U.company_name,S.img1';
        $sel.= ',ifnull(CAST((select if(avg(rating) is null,"0",avg(rating)) from reviews where reviews.receiver_id = U.user_id and status="Active" and is_deleted=0) AS DECIMAL(4,1)),0) as rating';
        $this->db->select($sel);
        $this->db->from('users U');
        $this->db->join('user_address UA','UA.user_id=U.user_id','inner');
        $this->db->join('services S','S.user_id=U.user_id','inner');
        $this->db->group_start();
        $this->db->where('U.user_type',2);
        $this->db->or_where('U.user_type',3);
        $this->db->group_end(); 
        $this->db->where('U.status','Active'); 
        $this->db->where('U.is_deleted',0); 
        $order_by="rating Desc";
        if(!empty($user_country) && !empty($user_state) && !empty($user_city)){
            $order_by.=", FIELD(UA.city, $user_city) DESC,FIELD(UA.state, $user_state) DESC,FIELD(UA.country, $user_country) DESC";
        }
        
        $this ->db->order_by($order_by);
        $this ->db->group_by('U.user_id');
        $this->db->limit($limit);
        $query=$this->db->get(); 
        return $query->result_array();
    }
    public function get_feeds_bkp_before_follow_query($limit,$offset,$user_id){
        $friend_where="(sender_id=$user_id OR receiver_id =$user_id) and status='Accepted'";
        $friend_check=$this->Common_model->getRecords('friends','id',$friend_where,'',true);
        if($friend_check){ 
            $sql="SELECT post.id as post_id,post.shared_post_id,post.post_number,post.post_content,post.shared_post_id,post.event_id,post.post_type,post.user_id,post.created,friends.id as request_id,if(post.shared_post_id>0,(select G.post_content from post G where G.id =post.shared_post_id),'') as shared_post_content,(select id from like_post where like_post.post_id=post.id and like_post.user_id='".$user_id."') as is_like_by_me,users.fullname,users.profile_picture,users.gender from post left join friends ON (post.user_id=if(friends.sender_id='".$user_id."',friends.receiver_id,friends.sender_id) OR post.user_id='".$user_id."') left join users ON users.user_id = post.user_id left join hide_post H ON H.post_id= post.id and H.user_id='".$user_id."' where (friends.sender_id='".$user_id."' OR friends.receiver_id ='".$user_id."' and friends.status='Accepted') and post.is_deleted=0 and post.status='Active' and H.id IS NULL group by post.id order by post.created desc";
                //OR (post.post_type=2)
                //OR clause with type  2 added for showing publix events irrespective of friends
            if($limit==0 && $offset==0){ 
                $query=$this->db->query($sql);
                return $query->num_rows();
            } else {
                $sql.=" limit ".$limit.", ".$offset."";
                $query=$this->db->query($sql);
                echo $this->db->last_query();exit;
                return $query->result_array();
            }
        }else{
            $sql="SELECT post.id as post_id,post.shared_post_id,post.post_number,post.post_content,post.shared_post_id,post.event_id,post.post_type,post.user_id,post.created,'0'as request_id,if(post.shared_post_id>0,(select G.post_content from post G where G.id =post.shared_post_id),'') as shared_post_content,(select id from like_post where like_post.post_id=post.id and like_post.user_id='".$user_id."') as is_like_by_me,users.fullname,users.profile_picture,users.gender from post left join users ON users.user_id = post.user_id left join hide_post H ON H.post_id= post.id and H.user_id='".$user_id."' where post.user_id= ".$user_id." and post.is_deleted=0 and post.status='Active' and H.id IS NULL group by post.id order by post.created desc";
                //OR clause with type  2 added for showing publix events irrespective of friends
            if($limit==0 && $offset==0){ 
                $query=$this->db->query($sql);
                return $query->num_rows();
            } else {
                $sql.=" limit ".$limit.", ".$offset."";
                $query=$this->db->query($sql);
               return $query->result_array();
            }
        }

    }
    //important only create bkp for shared profile image
    public function get_feeds_bk_for_app($limit,$offset,$user_id){
        $friend_where="(sender_id=$user_id OR receiver_id =$user_id) and status='Accepted'";
        $friend_check=$this->Common_model->getRecords('friends','id',$friend_where,'',true);
        if($friend_check){ 
            $sql="SELECT post.id as post_id,post.post_number,post.post_content,post.shared_post_id,post.event_id,post.post_type,post.user_id,post.created,post.sp_id,F.id as follower_id,friends.id as request_id,if(post.shared_post_id>0,(select G.post_content from post G where G.id =post.shared_post_id),'') as shared_post_content,(select id from like_post where like_post.post_id=post.id and like_post.user_id='".$user_id."') as is_like_by_me, if(F.id>0,users.company_name,users.fullname) as fullname,users.company_name,users.profile_picture,users.gender from post 
                left join friends ON (post.user_id=if(friends.sender_id='".$user_id."',friends.receiver_id,friends.sender_id) OR post.user_id='".$user_id."') 
                left join follow_users F ON
                if((select is_completed from events E where E.id = post.event_id)=1, post.sp_id = F.follow_to and F.followed_by ='".$user_id."', post.sp_id = 0)

                left join users ON if(F.id>0,users.user_id = post.sp_id,users.user_id = post.user_id and users.is_deleted=0)
                left join hide_post H ON H.post_id= post.id and H.user_id='".$user_id."'
                where ((friends.sender_id='".$user_id."' OR friends.receiver_id ='".$user_id."') and friends.status='Accepted') and post.is_deleted=0 and post.status='Active' and H.id IS NULL   OR (F.followed_by='".$user_id."' OR (post_type=2 and is_private=0) ) and H.id IS NULL group by post.id order by post.created desc";
                //OR (post.post_type=2)
                //OR clause with type  2 added for showing publix events irrespective of friends
            if($limit==0 && $offset==0){ 
                $query=$this->db->query($sql);
                return $query->num_rows();
            } else {
                $sql.=" limit ".$limit.", ".$offset."";
                $query=$this->db->query($sql);
                //echo $this->db->last_query();exit;
                return $query->result_array();
            }
        }else{
            $sql="SELECT post.id as post_id,post.post_number,post.post_content,post.shared_post_id,post.event_id,post.post_type,post.user_id,post.created,post.sp_id,F.id as follower_id,'0'as request_id,if(post.shared_post_id>0,(select G.post_content from post G where G.id =post.shared_post_id),'') as shared_post_content,(select id from like_post where like_post.post_id=post.id and like_post.user_id='".$user_id."') as is_like_by_me,if(F.id>0,users.company_name,users.fullname) as fullname,users.company_name,users.profile_picture,users.gender from post 
                left join follow_users F ON
                if((select is_completed from events E where E.id = post.event_id)=1,  post.sp_id = F.follow_to and F.followed_by ='".$user_id."', post.sp_id = 0)
                left join users ON if(F.id>0,users.user_id = post.sp_id,users.user_id = post.user_id and users.is_deleted=0)
                left join hide_post H ON H.post_id= post.id and H.user_id='".$user_id."' 
                where post.user_id= ".$user_id." and post.is_deleted=0 and post.status='Active' and H.id IS NULL OR (F.followed_by='".$user_id."' OR (post_type=2 and is_private=0) ) and H.id IS NULL group by post.id order by post.created desc";
                //OR clause with type  2 added for showing publix events irrespective of friends
            if($limit==0 && $offset==0){ 
                $query=$this->db->query($sql);
                return $query->num_rows();
            } else {
                $sql.=" limit ".$limit.", ".$offset."";
                $query=$this->db->query($sql);
               return $query->result_array();
            }
        }

    }
    public function get_feeds($limit,$offset,$user_id){
        $friend_where="(sender_id=$user_id OR receiver_id =$user_id) and status='Accepted'";
        $friend_check=$this->Common_model->getRecords('friends','id',$friend_where,'',true);
        if($friend_check){
            $sql="SELECT post.id as post_id,post.post_number,post.post_content,post.shared_post_id,post.user_id,post.event_id,post.post_type,post.created,post.sp_id,F.id as follower_id,friends.id as request_id,if(post.shared_post_id>0,(select G.post_content from post G where G.id =post.shared_post_id),'') as shared_post_content,(select id from like_post where like_post.post_id=post.id and like_post.user_id='".$user_id."')as is_like_by_me,if(F.id>0,users.company_name,users.fullname) as fullname,users.profile_picture,users.gender from post 

                left join friends ON (post.user_id=if(friends.sender_id='".$user_id."',friends.receiver_id,friends.sender_id) OR post.user_id='".$user_id."') 
                left join follow_users F ON
                if((select is_completed from events E where E.id = post.event_id)=1, post.sp_id = F.follow_to and F.followed_by ='".$user_id."', post.sp_id = 0)

                left join users ON users.user_id = post.user_id 

                left join hide_post H ON H.post_id= post.id and H.user_id='".$user_id."' 
                where ((friends.sender_id='".$user_id."' OR friends.receiver_id ='".$user_id."') and friends.status='Accepted') and post.is_deleted=0 and post.status='Active' and H.id IS NULL   OR (F.followed_by='".$user_id."' OR (post_type=2 and is_private=0) ) and H.id IS NULL group by post.id order by post.created desc"; 
           
            if($limit==0 && $offset==0){ 
                $query=$this->db->query($sql);
                return $query->num_rows();
            } else {
                $sql.=" limit ".$limit.", ".$offset."";
                $query=$this->db->query($sql);
                //echo $this->db->last_query();exit;
                return $query->result_array();
            }
        }else{
            $sql="SELECT post.id as post_id,post.post_content,post.shared_post_id,post.event_id,post.post_number,post.post_type,post.user_id,post.created,post.sp_id,F.id as follower_id,'0'as request_id,if(post.shared_post_id>0,(select G.post_content from post G where G.id =post.shared_post_id),'') as shared_post_content,(select id from like_post where like_post.post_id=post.id and like_post.user_id='".$user_id."') as is_like_by_me,if(F.id>0,users.company_name,users.fullname) as fullname,users.profile_picture,users.gender from post 

                left join follow_users F ON
                if((select is_completed from events E where E.id = post.event_id)=1,  post.sp_id = F.follow_to and F.followed_by ='".$user_id."', post.sp_id = 0)                                                                                                           
                left join users ON users.user_id = post.user_id 
                left join hide_post H ON H.post_id= post.id and H.user_id='".$user_id."'

                where post.user_id= ".$user_id." and post.is_deleted=0 and post.status='Active' and H.id IS NULL OR (F.followed_by='".$user_id."' OR (post_type=2 and is_private=0) ) and H.id IS NULL group by post.id order by post.created desc";

           
                //OR clause with type  2 added for showing publix events irrespective of friends
            if($limit==0 && $offset==0){ 
                $query=$this->db->query($sql);
                return $query->num_rows();
            } else {
                $sql.=" limit ".$limit.", ".$offset."";
                $query=$this->db->query($sql);
               return $query->result_array();
            }
        }

    }
    public function get_count($table,$where){
        $this->db->select('count(id) as count');
        $this->db->from($table);
        $this->db->join('users U','U.user_id='.$table.'.user_id','left');
        $this->db->where('U.status','Active'); 
        $this->db->where('U.is_deleted','0'); 
        if($table=='comments'){
            $this->db->where('comments.is_deleted','0'); 
        }
        $this->db->where($where); 
        $query=$this->db->get(); 
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else return false;
    }
    public function get_like_users($table,$where){
        $page=trim($this->input->post('page'));
        $limit=trim($this->input->post('limit'));
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $offset=$limit*$page;
        $this->db->select('U.fullname,U.user_id,U.gender,U.profile_picture');
        $this->db->from($table);
        $this->db->join('users U','U.user_id='.$table.'.user_id','left');
        $this->db->where('U.status','Active'); 
        $this->db->where('U.is_deleted','0'); 
        $this->db->where($where);
        $this->db->limit($limit, $offset); 
        $query=$this->db->get(); 
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else return false;
    }
    public function save_post_list($limit,$offset,$user_id){  
        $this->db->select('save_post.id as save_post_id,post.id as post_id,post.post_number,post.post_content,post.created,post.shared_post_id,save_post.created as save_post_time,if(post.shared_post_id>0,(select G.post_content from post G where G.id =post.shared_post_id),"") as shared_post_content,U.fullname,U.user_id,U.profile_picture,U.gender,(select id from like_post where like_post.post_id=post.id and like_post.user_id="'.$user_id.'") as is_like_by_me');
        $this->db->from('save_post');
        $this->db->join('post','post.id=save_post.post_id','left');
        $this->db->join('users U','U.user_id=post.user_id','left');
        $this->db->where('save_post.user_id',$user_id); 
        $this->db->where('post.is_deleted','0'); 
        $this->db->order_by('save_post.id','Desc');
        $this->db->limit($offset,$limit);
        $query=$this->db->get();
        if($limit==0 && $offset==0){ 
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else return false;
        }
    }
    public function post_detail($post_number){  
        $user_id= $this->session->userdata('user_id');
        $this->db->select('post.id as post_id,post.post_number,post.post_content,post.created,post.shared_post_id,post.user_id,if(post.shared_post_id>0,(select G.post_content from post G where G.id =post.shared_post_id),"") as shared_post_content,U.fullname,U.profile_picture,U.gender,(select id from like_post where like_post.post_id=post.id and like_post.user_id="'.$user_id.'") as is_like_by_me');
        $this->db->from('post');
        $this->db->join('users U','U.user_id=post.user_id','left');
        $this->db->where('post.post_number',$post_number); 
        $this->db->where('post.is_deleted','0'); 
        $query=$this->db->get();
        return $query->result_array();
           
    }
    public function my_post($limit,$offset,$user_id){
        $sql="SELECT post.id as post_id,post.post_number,post.post_content,post.shared_post_id,post.user_id,post.created,if(post.shared_post_id>0,(select G.post_content from post G where G.id =post.shared_post_id),'') as shared_post_content,users.fullname,users.profile_picture,users.gender,(select id from like_post where like_post.post_id=post.id and like_post.user_id=".$user_id.") as is_like_by_me from post 
            left join users ON users.user_id = post.user_id where  post.is_deleted=0  and post.user_id =$user_id group by post.id order by post.created desc";
            if($limit==0 && $offset==0){ 
                $query=$this->db->query($sql);
                return $query->num_rows();
            } else {
                $sql.=" limit ".$limit.", ".$offset."";
                $query=$this->db->query($sql);
               return $query->result_array();
            }

    }
    public function notification_list($limit,$offset){
        $user_id  = $this->session->userdata('user_id');
        $this->db->select('U.gender,U.profile_picture,N.*');
        $this->db->from('notifications N');
        $this->db->join('users U','U.user_id=N.created_by','inner');
        $this->db->where('U.status','Active'); 
        $this->db->where('U.is_deleted','0'); 
        $this->db->where('N.user_id',$user_id); 
        //$this->db->where('N.is_read','0'); 
        $this->db->order_by('N.creation_datetime','desc');
        $this->db->limit($offset,$limit); 
        $query=$this->db->get(); 
        return $query->result_array();
    }
}//model end

?>