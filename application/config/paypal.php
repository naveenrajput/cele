<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------
// Paypal library configuration
// ------------------------------------------------------------------------

// PayPal environment, Sandbox or Live
$config['sandbox'] = TRUE; // FALSE for live environment

// PayPal business email
//$config['business'] = 'rajput25@gmail.com';
//$config['business'] = 'business_neha@gmail.com';
//client personal ac sendbox
//$config['business'] = 'sb-qqfsh1455223@business.example.com';
//client business ac sandbox
$config['business'] = 'sb-6npae1485918@business.example.com';

// What is the default currency?
$config['paypal_lib_currency_code'] = 'USD';

// Where is the button located at?

// If (and where) to log ipn response in a file
$config['paypal_lib_ipn_log'] = TRUE;
$config['paypal_lib_ipn_log_file'] = BASEPATH . 'logs/paypal_ipn.log';