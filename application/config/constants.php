<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESCTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code 
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code 

define('GOOGLE_PLACE_KEY', 'AIzaSyAO2J4teAnyP8v6gPXlS4nsHF0bQTUvwSo');
define('SERVICE_CATEGORY_PATH', 'resources/images/service_category/');
define('BANNER_PATH', 'resources/images/banner/');
define('USER_DOCUMNET_PATH', 'resources/user/document/');
define('SERVICE_BANNER_PATH', 'resources/services/banner/');
define('OFFER_BANNER_PATH', 'resources/services/offer_banner/');
define('USER_PROFILE_PATH', 'resources/images/profile/');


define('ADMIN_LIMIT', 10);
define('FRONT_LIMIT', 30);
define('COMMENT_LIMIT',5);
define('CHILD_COMMENT_LIMIT',5);
define('INNER_CHILD_COMMENT_LIMIT',2);


define('ADMIN_CURRENCY','$');
define('ADMIN_CURRENCY_CODE','USD');

define('DATETIMEFORMATE',"M d, Y");

/*define('APP_PUBLISH',false);
define('APP_PUBLISH_DATE','2020-10-20');
define('FREE_TRIAL_DAYS',90);*/
define('APP_PUBLISH',true);
define('APP_PUBLISH_DATE','2020-01-20');
define('FREE_TRIAL_DAYS',90);
define('NODEURL','https://www.techopium.com:3714/');
//define('NODEURL','http://192.168.2.87:3720/');

define('SMTP_HOST','smtp.gmail.com');
/*define('SMTP_USER', 'hypdev01@gmail.com');
define('SMTP_PASS', 'Test@123');*/
define('SMTP_USER', 'celeprofile@gmail.com');
define('SMTP_PASS', 'Babanidigi1Iyaniwura2Ola');
define('SMTP_PORT', '465'); 
define('PROTOCOL', 'smtp');
define('MAILPATH', '/usr/sbin/sendmail');
define('MAILTYPE', 'html');
define('CHARSET', 'utf-8');
define('WORD_WRAP', TRUE);
define('SMTP_TIMEOUT',300);
define('enc_value','1345');

define('FROM_NAME','Cele');
define('WEBSITE_NAME','Cele');
define('SITE_TITLE', 'Cele');

define('APP_KEY','HmTGlpmOucnXebDayeJi5PpxIvcUVO5L');

# use it
define('DEV_MODE',true);

define('GOOGLE_API_KEY','AIzaSyBLV-ZJ-ZcSVG4z63Ohe0n4yFDkxrbHvBo');
define('GOOGLE_API_KEY_LATLONG','AIzaSyB_jfNvLIu7C6yw_LdYzWRKOf4lvDiMWXg');


/*define('AMAZON_BUCKET','thetruthdebate');
define('AMAZON_ID','AKIAJXQ2BHBZRYF5KHQQ');
define('AMAZON_KEY','vo3Se5wjK1jvY7NgcGBAokYYApahR0o/L2UZEqUU');
define('AMAZON_PATH',"https://".AMAZON_BUCKET.".s3.amazonaws.com/");*/


define('AMAZON_BUCKET','celebucket');
define('AMAZON_ID','AKIAUJFHHHADDFYQ2VX3');
define('AMAZON_KEY','ZmJAOR8mnkyd/4kt0Q3ZYkacLxsdkmZQbVs99KK+');
define('AMAZON_PATH',"https://".AMAZON_BUCKET.".s3.amazonaws.com/");

define('MEDIA_PATH','resources/media/');
define('MEDIA_THUMB_PATH','resources/media/media_thumbs/');
define('MEDIA_THUMB_TEMP_PATH','resources/media/temp/');

define('GOOGLE_PLAY','https://play.google.com');
define('APPLE_ITUNE','https://play.google.com');



/*define('PAYPAL_CLIENT_ID', 'ATEAJmqmgv-ioyZG4-IuVi2dUi0BVx6g0SBqkv14wOUMO0OTgyQz3UGOyOmkk4WlEuCNSxM-EvZdLEfz');
define('PAYPAL_SECRATE_KEY', 'EBuf-AN_Q2tdhGlWEM8sgFfK3NCgUVfnv7hrUWMulwppYkY9LaQjE2FswXVE6mHKqVfcsZlJTpkVaCLt');*/

//client personal account credential//
/*define('PAYPAL_CLIENT_ID', 'AWQOEFE0MTInGK1JeYos0GgMoKewKuP1Ri3kbx7BK0Y-9leQKtc1g43qABNYt7pQZSgixwGGVworMWp4');
define('PAYPAL_SECRATE_KEY', 'EF_GjK1QUs2lIctL5cfQOccVfBCBFLCzGDVmKcW-bwo2voGVv33BeEG4P-qiwXlEnMbBnTYLMnxJ0mK9');*/

//client bussiness account credentails ///need to check

/*define('PAYPAL_CLIENT_ID', 'AWQOEFE0MTInGK1JeYos0GgMoKewKuP1Ri3kbx7BK0Y-9leQKtc1g43qABNYt7pQZSgixwGGVworMWp4');
define('PAYPAL_SECRATE_KEY', 'EF_GjK1QUs2lIctL5cfQOccVfBCBFLCzGDVmKcW-bwo2voGVv33BeEG4P-qiwXlEnMbBnTYLMnxJ0mK9');*/
define('PAYPAL_CLIENT_ID', 'AYXHJUrvsNQIJC-ZqNvma69H9S7a9_2lCrFM3d0odWweGGmoOa-zPL38H_9LoA0mOLoTL3y5sAjgLtjV
');
define('PAYPAL_SECRATE_KEY', 'EG8YWp8pbQpaqSi9uD3Mk2l9upRvHwoc-1zWLSs3Z9e12DOkZSkzs89-Btei78FPuHOV4z25J6HfFESK');

