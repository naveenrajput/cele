<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html 
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Welcome'; //admin login view    
$route['404_override'] = 'page_not_found';
$route['translate_uri_dashes'] = FALSE;

$route['app/terms_and_conditions']	= 	'Static_pages/terms_and_conditions';
$route['app/privacy_policy'] 		= 	'Static_pages/privacy_policy';
$route['app/get_in_touch'] 		= 	'Static_pages/get_in_touch';
$route['app/about-us'] 		= 	'Static_pages/about_us';
$route['app/cancellation_and_refund_policy'] 		= 	'Static_pages/cancellation_and_refund_policy';
$route['app/contact_us'] 		= 	'Static_pages/contact_us';

$route['admin'] 		 		= 'admin/admin';
$route['admin/login'] 		 		= 'admin/admin';
$route['admin/logout'] 				= 'admin/admin/logout';
$route['admin/forgot_password'] 	= 'admin/admin/forgot_password';
$route['admin/reset_password'] 		= 'admin/admin/reset_password';

$route['admin/edit_profile'] 		= 'admin/admin/edit_profile';
$route['admin/change_password'] 	= 'admin/admin/change_password';

$route['admin/dashboard'] 			= 'admin/Dashboard';
$route['admin/dashboard/dashboard_calender']= 'admin/Dashboard/dashboard_calender';
//$route['admin/dashboard/prohibit_dashboard']= 'admin/Dashboard/prohibit_dashboard';

$route['admin/subadmin/list'] 			= 'admin/admin/admin_list';
$route['admin/subadmin/list/:num'] 		= 'admin/admin/admin_list/:num';
$route['admin/subadmin/add'] 			= 'admin/admin/add_admin';
$route['admin/subadmin/edit/(:num)'] 	= 'admin/admin/edit_admin/$1';
$route['admin/subadmin/edit'] 			= 'admin/admin/edit_admin';

$route['admin/role_list'] 			= 'admin/admin/role_list';
$route['admin/role_list/:num'] 		= 'admin/admin/role_list/:num';
$route['admin/add_role'] 			= 'admin/admin/add_role';
$route['admin/edit_role/(:num)'] 	= 'admin/admin/edit_role/$1';
$route['admin/edit_role'] 			= 'admin/admin/edit_role';

$route['admin/contact/list'] 		= 'admin/Support/contact_list';
$route['admin/contact/list/(:num)'] = 'admin/Support/contact_list/$1';
$route['admin/contact/add_contact'] = 'admin/Support/add_contact';
$route['admin/contact/reply'] 		= 'admin/Support/contact_reply';
$route['admin/contact/reply/(:num)']= 'admin/Support/contact_reply/$1';



$route['admin/pages/list'] 				= 'admin/Cmscontent/pages_list';
$route['admin/pages/list/:num'] 		= 'admin/Cmscontent/pages_list/:num';
$route['admin/pages/add'] 				= 'admin/Cmscontent/add_page';
$route['admin/pages/edit/(:num)']   	= 'admin/Cmscontent/edit_page/$1';
$route['admin/pages/edit'] 				= 'admin/Cmscontent/edit_page';

$route['admin/states/list'] 			= 'admin/Cmscontent/states_list';
$route['admin/states/list/:num'] 		= 'admin/Cmscontent/states_list/:num';
$route['admin/states/add'] 				= 'admin/Cmscontent/add_states';
$route['admin/states/edit/(:num)']   	= 'admin/Cmscontent/edit_states/$1';
$route['admin/states/edit'] 			= 'admin/Cmscontent/edit_states';

$route['admin/cities/list'] 			= 'admin/Cmscontent/cities_list';
$route['admin/cities/list/:num'] 		= 'admin/Cmscontent/cities_list/:num';
$route['admin/cities/add'] 				= 'admin/Cmscontent/add_cities';
$route['admin/cities/edit/(:num)']   	= 'admin/Cmscontent/edit_cities/$1';
$route['admin/cities/edit'] 			= 'admin/Cmscontent/edit_cities';


$route['admin/contact_us/list'] 			= 'admin/Cmscontent/contact_us_form';
$route['admin/contact_us/list/:num'] 		= 'admin/Cmscontent/contact_us_form/:num';
$route['admin/contact_us/detail/(:num)'] 		= 'admin/Cmscontent/contact_details/$1';

$route['admin/banner/list'] 			= 'admin/Cmscontent/banner_list';
$route['admin/banner/list/:num'] 		= 'admin/Cmscontent/banner_list/:num';
$route['admin/banner/add'] 				= 'admin/Cmscontent/add_banner';
$route['admin/banner/edit/(:num)']   	= 'admin/Cmscontent/edit_banner/$1';
$route['admin/banner/edit'] 			= 'admin/Cmscontent/edit_banner';


$route['admin/service_category/list']   	 = 'admin/Service/service_list';
$route['admin/service_category/list/:num']   = 'admin/Service/service_list/:num';
$route['admin/service_category/add'] 			= 'admin/Service/add_service';
$route['admin/service_category/edit/(:num)']   	= 'admin/Service/edit_service/$1';
$route['admin/service_category/edit'] 			= 'admin/Service/edit_service';

$route['admin/measurement_unit/list'] 			= 'admin/Service/measurement_unit_list';
$route['admin/measurement_unit/list/:num'] 		= 'admin/Service/measurement_unit_list/:num';
$route['admin/measurement_unit/add'] 				= 'admin/Service/add_measurement_unit';
$route['admin/measurement_unit/edit/(:num)']   	= 'admin/Service/edit_measurement_unit/$1';
$route['admin/measurement_unit/edit'] 			= 'admin/Service/edit_measurement_unit';

$route['admin/user/list']    		= 'admin/User/users_list';
$route['admin/user/list/:num'] 		= 'admin/User/users_list/:num';
$route['admin/user/add'] 			= 'admin/User/add_user';
$route['admin/user/edit/(:num)']   	= 'admin/User/edit_user/$1';
$route['admin/user/edit'] 			= 'admin/User/edit_user';
 
$route['admin/user/post_report'] 		= 'admin/User/report_port_list';
$route['admin/user/post_report/(:num)'] 		= 'admin/User/report_port_list/$1';
$route['admin/user/post_report/(:num)/(:num)'] 		= 'admin/User/report_port_list/$1/$i';



 
$route['admin/user/post_like'] 		= 'admin/User/post_like_list';
$route['admin/user/post_like/(:num)'] 		= 'admin/User/post_like_list/$1';
$route['admin/user/post_like/(:num)/(:num)'] 		= 'admin/User/post_like_list/$1/$i';

 
$route['admin/user/post_shared'] 		= 'admin/User/post_shared_list';
$route['admin/user/post_shared/(:num)'] 		= 'admin/User/post_shared_list/$1';
$route['admin/user/post_shared/(:num)/(:num)'] 		= 'admin/User/post_shared_list/$1/$i';


 
$route['admin/user/friend'] 		= 'admin/User/friend_list';
$route['admin/user/friend/(:num)'] 		= 'admin/User/friend_list/$1';
$route['admin/user/friend/(:num)/(:num)'] 		= 'admin/User/friend_list/$1/$i';
 



$route['admin/post/post_like'] 		= 'admin/post/post_like_list';
$route['admin/post/post_like/(:num)'] 		= 'admin/post/post_like_list/$1';
$route['admin/post/post_like/(:num)/(:num)'] 		= 'admin/post/post_like_list/$1/$i';



$route['admin/post/post_shared'] 		= 'admin/post/post_shared_list';
$route['admin/post/post_shared/(:num)'] 		= 'admin/post/post_shared_list/$1';
$route['admin/post/post_shared/(:num)/(:num)'] 		= 'admin/post/post_shared_list/$1/$i';
 
$route['admin/post/post_report'] 		= 'admin/post/report_port_list';
$route['admin/post/post_report/(:num)'] 		= 'admin/post/report_port_list/$1';
$route['admin/post/post_report/(:num)/(:num)'] 		= 'admin/post/report_port_list/$1/$i';
 


$route['admin/user/chat_history/(:num)/(:num)'] 		= 'admin/User/chat_history/$1/$2';
$route['admin/user/chat_history/(:num)/(:num)/(:num)'] 		= 'admin/User/chat_history/$1/$2/$i';



$route['admin/user/post'] 		   = 'admin/post/post_list';
$route['admin/user/post/:num'] 	   = 'admin/post/post_list/:num';






$route['admin/broadcast_notification/list']   		= 		'admin/Notification/index';
$route['admin/broadcast_notification/list/:num'] 	= 		'admin/Notification/index/:num';
$route['admin/broadcast_notification/send'] 		= 		'admin/Notification/broadcast_notification_send';

$route['admin/settings/edit/(:num)'] = 'admin/Setting/edit_setting/$1';


$route['admin/not_authorized'] 	= 'admin/Admin/not_authorized';
$route['admin/prohibit_dashboard'] 	= 'admin/Dashboard/prohibit_dashboard';
 

$route['admin/rating/list'] 		   = 'admin/Other/reviews_list';
$route['admin/rating/list/:num'] 	   = 'admin/Other/reviews_list/:num';
$route['admin/rating/add'] 		   = 'admin/Other/add_reviews';
$route['admin/rating/edit/(:num)']    = 'admin/Other/edit_reviews/$1';
$route['admin/rating/edit'] 		   = 'admin/Other/edit_reviews';

$route['admin/rating/view/(:num)']    = 'admin/Other/view_reviews/$1';
$route['admin/rating/view'] 		   = 'admin/Other/view_reviews';

$route['admin/post/list'] 		   = 'admin/Post/post_list';
$route['admin/post/list/:num'] 	   = 'admin/Post/post_list/:num';
$route['admin/post/add'] 		   = 'admin/Post/add_post';
$route['admin/post/edit/(:num)']   = 'admin/Post/edit_post/$1';
$route['admin/post/edit'] 		   = 'admin/Post/edit_post';



$route['admin/subscription/list'] 		   = 'admin/Other/subscription_list';
$route['admin/subscription/list/:num'] 	   = 'admin/Other/subscription_list/:num';
$route['admin/subscription/add'] 			   = 'admin/Other/add_subscription';
$route['admin/subscription/edit/(:num)']     = 'admin/Other/edit_subscription/$1';
$route['admin/subscription/edit'] 		   = 'admin/Other/edit_subscription';

$route['admin/payment/subscription'] 		   = 'admin/Order/user_subscription_list';
$route['admin/payment/subscription/:num'] 	   = 'admin/Order/user_subscription_list/:num'; 


$route['admin/report/list'] 		   = 'admin/Report/report_list';
$route['admin/report/list/:num'] 	   = 'admin/Report/report_list/:num'; 



$route['admin/report/reply'] 		= 'admin/Report/report_reply';
$route['admin/report/reply/(:num)']= 'admin/Report/report_reply/$1';

  


$route['admin/user/subscription'] 		   = 'admin/Order/user_subscription_list';
$route['admin/user/subscription/:num'] 	   = 'admin/Order/user_subscription_list/:num'; 


$route['admin/services/list'] 		   = 'admin/Service/user_services_list';
$route['admin/services/list/:num'] 	   = 'admin/Service/user_services_list/:num';
$route['admin/services/view/(:num)']     = 'admin/Service/view_user_service/$1';
$route['admin/services/view'] 		   = 'admin/Service/view_user_service';


$route['admin/user/services_list'] 		   = 'admin/Service/user_services_list';
$route['admin/user/services_list/:num'] 	   = 'admin/Service/user_services_list/:num';
$route['admin/user/services_view/(:num)']     = 'admin/Service/view_user_service/$1';
$route['admin/user/services_view'] 		   = 'admin/Service/view_user_service';


$route['admin/events/list'] 		   = 'admin/Event/events_list';
$route['admin/events/list/:num'] 	   = 'admin/Event/events_list/:num';
$route['admin/event/view/(:num)']     = 'admin/Event/view_event/$1';
// $route['admin/events/view'] 		   = 'admin/Event/events';


$route['admin/orders/list'] 		   = 'admin/Order/orders_list';
$route['admin/orders/list/:num'] 	   = 'admin/Order/orders_list/:num';
$route['admin/orders/view/(:num)']     = 'admin/Order/view_order/$1';
// $route['admin/orders/view'] 		   = 'admin/Event/orders';



$route['admin/payment/list'] 		   = 'admin/Order/payment_list';
$route['admin/payment/list/:num'] 	   = 'admin/Order/payment_list/:num';
$route['admin/payment/view/(:num)']     = 'admin/Order/view_order/$1'; 


$route['admin/report_export/list'] 	   = 'admin/Order/report_excel'; 
$route['admin/report_export/list/:num'] 	   = 'admin/Order/report_excel/:num'; 


$route['admin/payment/refund'] 		   = 'admin/Order/refund_list';
$route['admin/payment/refund/:num']   = 'admin/Order/refund_list/:num';
//$route['admin/payment/list/:num'] 	   = 'admin/Order/payment_list/:num';
$route['admin/payment_refund/view/(:num)']     = 'admin/Order/view_order/$1'; 


$route['admin/payment/createInvoice/(:num)']     = 'admin/Order/createInvoice/$1'; 
$route['admin/payment/createInvoice/(:num)/(:num)']     = 'admin/Order/createInvoice/$1/$1'; 


$route['admin/services/offer/(:num)/(:num)']     = 'admin/Service/service_offer_list/$1/(:num)';
$route['admin/services/offer/(:num)']     = 'admin/Service/service_offer_list/$1';
$route['admin/services/offer'] 		   = 'admin/Service/service_offer_list';
// $route['admin/services/add'] 			   = 'admin/Other/add_services';
$route['admin/action_log/list'] 			   = 'admin/Other/action_log_list';
$route['admin/action_log/list/(:num)'] 			   = 'admin/Other/action_log_list/$1';

//////////////////////////////cron//////////////////////////////
$route['complete_cron'] 		   = 'Cron/complete_cron';
$route['temp_table_delete'] 	   = 'Cron/delete_temp_table';
$route['delete_draft_event'] 	   = 'Cron/delete_draft_event';
$route['broadcast_notification_send'] = 'Cron/broadcast_notification';
$route['deal_back'] = 'Cron/without_payment_deal_back';


////////////////////////////////////////////////////////////////

///////////////////////////////////FRONT ROUTES////////////////////////

$route['sign-up'] 		 		= 'front/Login/sign_up';
$route['login'] 		 		= 'front/Login';
$route['login-ajax'] 		 	= 'front/Login/login_ajax';
$route['forgot-password'] 		= 'front/Login/forgot_password';
$route['reset-password'] 		= 'front/Login/reset_password';
$route['change-password'] 		= 'front/Login/change_password';
$route['profile'] 		 		= 'front/Login/profile';
$route['profile/(:any)'] 		= 'front/Login/profile/$1';
$route['edit-profile'] 		 	= 'front/Login/edit_profile';
$route['upgrade-profile'] 		= 'front/Login/edit_profile';
$route['logout'] 				= 'front/Login/logout';

$route['manage-service'] 		= 'front/Mystore/manage_service_list';
$route['manage-offers'] 		= 'front/Mystore/manage_offer_list';
$route['add-service'] 			= 'front/Mystore/add_service';
$route['edit-service/(:any)'] 	= 'front/Mystore/edit_service/$1';
$route['edit-service'] 			= 'front/Mystore/edit_service';
$route['service-details/(:any)'] = 'front/Mystore/service_details/$1';
$route['service-details'] 		= 'front/Mystore/service_details';
$route['offer-details/(:any)'] = 'front/Mystore/offer_details/$1';
$route['offer-details'] 		= 'front/Mystore/offer_details';
$route['add-offer'] 			= 'front/Mystore/add_offer';
$route['edit-offer/(:any)'] 	= 'front/Mystore/edit_offer/$1';
$route['edit-offer'] 			= 'front/Mystore/edit_offer';

$route['subscription'] 			= 'front/Mystore/subscription';
$route['plan-subscription'] 	= 'front/Mystore/plan_subscription';
$route['plan-subscription/(:any)'] 	= 'front/Mystore/plan_subscription/$1';
$route['subscription-notify-payment'] 	= 'front/Mystore/subscription_notify_payment';
$route['cancel-subscription-plan/(:any)'] 	= 'front/Mystore/cancel_subscription_plan/$1';

$route['my-bookings'] 			= 'front/Booking/sp_my_booking';
$route['my-booking-details'] 	= 'front/Booking/booking_details';
$route['my-booking-details/(:any)'] = 'front/Booking/booking_details/$1';
$route['my-booking-filter'] 	= 'front/Booking/sp_my_booking_filter';

$route['sp-order-history'] 			= 'front/Booking/sp_order_history';

/////////////////////////////cele & celesp/////////////////////////////////


$route['home'] 				= 'front/Home';
$route['post'] 				= 'front/Home/post_content';
$route['update-post'] 		= 'front/Home/update_post_content';
$route['share-post'] 		= 'front/Home/share_post';
$route['post-like'] 		= 'front/Home/post_like';
$route['post-hide'] 		= 'front/Home/hide_post';
$route['post-delete'] 		= 'front/Home/delete_post';
$route['post-report'] 		= 'front/Home/report_post';
$route['post-save'] 		= 'front/Home/save_post';
$route['post-comment'] 		= 'front/Home/post_comment';
$route['delete-comment'] 	= 'front/Home/delete_comment';
$route['comment-like'] 		= 'front/Home/comment_like_dislike';
$route['saved-post'] 		= 'front/Home/save_post_list';
$route['post-unsave'] 		= 'front/Home/unsave_post';
$route['post-details/(:any)']= 'front/Home/post_details/$1';

$route['service-providers'] = 'front/Explore/manage_service_providers_list';
$route['service-providers/(:any)'] = 'front/Explore/manage_service_providers_list/$1';
$route['service-provider-detail'] 	= 'front/Explore/service_provider_details';
$route['service-provider-detail/(:any)'] = 'front/Explore/service_provider_details/$1';
$route['service-provider-filter'] 	= 'front/Explore/service_provider_filter';

$route['book-service'] = 'front/Explore/book_service';
$route['book-event-service'] = 'front/Explore/boook_event_service';
$route['create-event'] = 'front/Explore/create_event';
$route['make-payment/(:any)'] = 'front/Explore/make_payment/$1';
$route['notify-payment'] = 'front/Explore/notify_payment';


$route['upcoming-events'] = 'front/Explore/manage_upcoming_event_list';
$route['upcoming-events-detail/(:any)'] = 'front/Explore/upcoming_event_details/$1';
$route['upcoming-events-filter'] 	= 'front/Explore/upcoming_event_filter';

$route['my-events'] 			= 'front/Booking/my_events';
$route['my-event-filter'] 	= 'front/Booking/my_event_filter';
$route['my-event-details/(:any)'] = 'front/Booking/my_event_details/$1';
$route['complete-event'] = 'front/Booking/celebrant_completed_booking';
$route['rating-review'] = 'front/Booking/add_rating_review';

$route['csp-my-bookings'] 			= 'front/Booking/csp_my_booking';
$route['csp-booking-filter'] 	= 'front/Booking/csp_my_booking_filter';
$route['csp-booking-details'] 	= 'front/Booking/booking_details';
$route['csp-booking-details/(:any)'] = 'front/Booking/booking_details/$1';


$route['public-events'] = 'front/Explore/public_event_list';
$route['public-events-filter'] 	= 'front/Explore/public_event_filter';
$route['public-event-details/(:any)'] = 'front/Explore/public_event_details/$1';

$route['invite-friend'] = 'front/Explore/invite_friend';
$route['accept-reject-event-status'] = 'front/Booking/accept_reject_event_request';

$route['invite-friend-list'] = 'front/Booking/invite_friend_list';
$route['invite-friend-list/(:any)'] = 'front/Booking/invite_friend_list/$1';
/////////////////////////////csp order received/////////////////////////////////
$route['order-received-history'] 			= 'front/Booking/order_history';
//////////////////////////////csp & cele order placed///////////////////////
$route['order-placed-history'] 			= 'front/Booking/order_placed_history';

/*footer route*/
$route['about-us'] = 'front/Footer/about_us';
$route['privacy-policy'] = 'front/Footer/privacy_policy';
$route['terms-and-conditions'] = 'front/Footer/terms_and_conditions';
$route['cancellation-refund-policy'] = 'front/Footer/cancellation_refund_policy';
$route['contact-us'] = 'front/Footer/contact_us';

/* Home route */
$route['friends-list'] = 'front/Home/my_friends_list';
$route['friends-request-list'] = 'front/Home/my_friend_requests';
$route['find-friends-list'] = 'front/Home/my_friend_suggestion';

$route['be-my-date']='front/Home/my_date';
$route['be-my-date-filter']='front/Home/be_my_date_filter';


$route['friends'] = 'front/Home/my_friends_list';

$route['friends-list'] = 'front/Home/my_friends_list';
$route['friends-request-list'] = 'front/Home/my_friend_requests';

$route['be-my-date']='front/Home/my_date';

/*Follow user*/
$route['follow-user'] = 'front/Explore/follow_user';

/*Add to deal*/
$route['add_to_deal'] = 'front/Explore/event_add_to_deal';


$route['manage-deals'] = 'front/Deal/manage_deal_list';

$route['deals-details/(:any)'] = 'front/Deal/deals_details/$1';

$route['grab-deal'] = 'front/Deal/grab_deal';

$route['deal-notify-payment'] = 'front/Deal/deal_notify_payment';

$route['deal-make-payment/(:any)'] = 'front/Deal/deal_make_payment/$1';

$route['deals-filter'] = 'front/Deal/deals_filter';

