<section class="section-div py-5 aboutus-section border">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="aboutus">
                    <h2 class="aboutus-title"><?php echo $page_detail['title'];?></h2>
                    <?php echo $page_detail['content']; ?>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="aboutus-banner">
                    <!-- <img src="http://themeinnovation.com/demo2/html/build-up/img/home1/about1.jpg" alt=""> -->
                </div>
            </div>
            <div class="col-md-5 col-sm-6 col-xs-12">
                <div class="feature">
                    <div class="feature-box">
                        <div class="clearfix">
                            <div class="iconset">
                                <i class="fa fa-cog icon" aria-hidden="true"></i>
                            </div>
                            <div class="feature-content">
                                <h4>Work with heart</h4>
                                <p>Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in.</p>
                            </div>
                        </div>
                    </div>
                    <div class="feature-box">
                        <div class="clearfix">
                            <div class="iconset">
                                <i class="fa fa-cog icon" aria-hidden="true"></i>
                            </div>
                            <div class="feature-content">
                                <h4>Reliable services</h4>
                                <p>Donec vitae sapien ut libero venenatis faucibu. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt</p>
                            </div>
                        </div>
                    </div>
                    <div class="feature-box">
                        <div class="clearfix">
                            <div class="iconset">
                                <i class="fa fa-cog icon" aria-hidden="true"></i>
                            </div>
                            <div class="feature-content">
                                <h4>Great support</h4>
                                <p>Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>