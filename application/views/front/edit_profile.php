<div class="data_reload">
<section class="section-div pt-4 add-service-section edit-profile-next">
   <div class="container">
      <div class="card add-service-card">
        <div class="message_box" style="display:none;"></div>
        <div id="status_box">
        </div>
         <form class="add-service-form signup-form" action="" id="edit_profile" enctype="multipart/form-data" data-parsley-validate>
            <div class="row">
               <div class="col-md-8 service-details">
                  <div class="card-banner service-slider-container">
                     <h2 class="py-4 main-heading">Edit Profile</h2>
                  </div>
                  <div class="accordion book-service">
                     <div class="row">
                        <div class="form-group col-md-6">
                           <img src="assets/front/images/form_icons/ic_full_name.png" class="form-input-icon" alt="mob-img">
                           <input type="text" class="form-control" placeholder="Full Name" name="fullname" id= "fullname" data-parsley-required data-parsley-required-message="Please enter full name." maxlength="100"  value="<?php if(!empty($user_data['fullname'])) echo $user_data['fullname'];?>"/>
                        </div>
                        <div class="form-group col-md-6">
                             <img src="assets/front/images/form_icons/ic_staging.png" class="form-input-icon" alt="mob-img" />
                             <input type="text" class="form-control" placeholder="Login ID" name="staging_id" id="staging_id" data-parsley-required data-parsley-required-message="Please enter login ID." maxlength="20" oninput="this.value = this.value.replace(/[^A-Za-z0-9._]/g,'');" value="<?php if(!empty($user_data['staging_id'])) echo $user_data['staging_id'];?>" readonly />
                        </div>
                        <div class="form-group col-md-6">
                            <img src="assets/front/images/form_icons/ic_email.png" class="form-input-icon" alt="mob-img" />
                            <input type="email" class="form-control" placeholder="Email ID" name="email" id="email" data-parsley-required data-parsley-required-message="Please enter email ID." data-parsley-type="email" data-parsley-type-message="Please enter valid email." value="<?php if(!empty($user_data['email'])) echo $user_data['email'];?>" />
                            <span class="error" id="access_email_error" style="display:none;">Email already used.</span>
                        </div>
                        <div class="form-group col-md-6">
                           <img src="assets/front/images/form_icons/ic_mobile.png" class="form-input-icon" alt="mob-img">
                           <input type="text" class="form-control" placeholder="Mobile Number" id="mobile"  name="mobile" oninput="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" data-parsley-required data-parsley-required-message="Please enter mobile no." maxlength="50" value="<?php if(!empty($user_data['mobile'])) echo $user_data['mobile'];?>" readonly />
                        </div>
                        <div class="form-group col-md-6">
                           <img src="assets/front/images/form_icons/ic_email.png" class="form-input-icon" alt="mob-img">
                           <input type="email" class="form-control" placeholder="Paypal ID" name="paypal_id" id="paypal_id" data-parsley-required data-parsley-required-message="Please enter paypal ID." data-parsley-type="email" data-parsley-type-message="Please enter valid paypal ID." value="<?php if(!empty($user_data['paypal_id'])) echo $user_data['paypal_id'];?>" />
                        </div>
                        <div class="form-group col-md-6">
                           <img src="assets/front/images/form_icons/ic_country.png" class="form-input-icon" alt="mob-img">
                          <select class="form-control" name="country" id="country" data-parsley-required data-parsley-required-message="Please select country." onchange="get_country()">
                            <option value="" >Select Country</option>
                            <?php if(isset($countries)) {
                                foreach($countries as $country) { ?>
                                    <option value=<?php echo $country['id'];?>  <?php if(!empty($user_data['country']) && ($user_data['country_id']==$country['id'])) {echo "selected";} ?>><?php echo $country['name'];?></option>";
                                <?php }
                            } ?>
                         </select>
                        </div>
                        <div class="form-group col-md-6">
                            <img src="assets/front/images/form_icons/ic_state.png" class="form-input-icon" alt="mob-img" />
                             <select class="form-control"  name="state" id="state" data-parsley-required data-parsley-required-message="Please select state." onchange="get_states()">
                            <option id="first" value="">Select State</option>
                             <?php if(!empty($user_data['country_id'])) {
                                    if($states = getStatesList($user_data['country_id'])) {
                                        foreach($states as $state) { ?>
                                            <option value=<?php echo $state['id'];?> <?php if(!empty($user_data['state_id']) && ($user_data['state_id']==$state['id'])) {echo "selected";} ?>><?php echo $state['name'];?></option>
                                        <?php }
                                    } 
                                }?>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                           <img src="assets/front/images/form_icons/ic_city.png" class="form-input-icon" alt="mob-img">
                           <select class="form-control" id='city' name="city" data-parsley-required data-parsley-required-message="Please select city.">
                            <option id='city_first' value="">Select City</option>
                                 <?php if(!empty($user_data['state'])) {
                                    $cities = getCitiesList($user_data['state_id']);
                                    if($cities) {
                                        foreach($cities as $city) { ?>
                                            <option value=<?php echo $city['id'];?> <?php if(!empty($user_data['city_id']) && ($user_data['city_id']==$city['id'])) {echo "selected";} ?>><?php echo $city['name'];?></option>
                                        <?php }
                                    } 
                                }?>
                            </select>
                        </div>
                         <div class="form-group col-md-12">
                           <img src="assets/front/images/form_icons/ic_address.png" class="form-input-icon" alt="mob-img">
                           <input type="text" class="form-control google_address" id ="address" name="address" placeholder="Address" data-parsley-required data-parsley-required-message="Please enter address." oninput="clear_address()" value="<?php if(!empty($user_data['address'])) echo $user_data['address'];?>"/>
                            <p class="error" id="latlong" style="display:none;">Please enter valid address.</p>
                            <input type="hidden" id="lat" class="lat" name="latitude" value="<?php if(!empty($user_data['latitude'])) echo $user_data['latitude'];?>">
                            <input type="hidden" id="lng" class="lng" name="longitude" value="<?php if(!empty($user_data['longitude'])) echo $user_data['longitude'];?>">
                        </div>
                     </div>
                    <?php if(!empty($user_data['user_type'])){
                        if($user_data['user_type']==2 || $user_data['user_type']==3 || (isset($show_sp_data) && !empty($show_sp_data))){ ?>
                     <div class="row">
                        <div class="form-group col-md-6">
                             <img src="assets/front/images/form_icons/ic_company.png" class="form-input-icon" alt="mob-img" />
                             <input type="text" class="form-control" placeholder="Company Name" name="company_name" id= "company_name" data-parsley-required data-parsley-required-message="Please enter company name." maxlength="150" value="<?php if(!empty($user_data['company_name'])) echo $user_data['company_name'];?>" />
                        </div>
                        <div class="form-group col-md-6">
                             <img src="assets/front/images/form_icons/ic_phone.png" class="form-input-icon" alt="mob-img" />
                             <input type="text" class="form-control" placeholder="Business Phone Number (Optional)" id="business_no"  name="business_no" oninput="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" maxlength="50" value="<?php if(!empty($user_data['business_no'])) echo $user_data['business_no'];?>"/>
                        </div>
                        <div class="form-group col-md-6">
                             <img src="assets/front/images/form_icons/ic_year.png" class="form-input-icon" alt="mob-img" />
                             <select class="form-control" name="establishment_year" id ="establishment_year" data-parsley-required data-parsley-required-message="Please select establishment year.">
                             <option value="">Year of Establishment</option>
                                <?php for($i=date('Y');$i>=1950;$i--){ ?>
                                    <option value="<?php echo $i;?>" <?php if(!empty($user_data['establishment_year'])) { if($user_data['establishment_year']==$i)echo 'selected';}?>><?php echo $i;?></option>
                                <?php } ?>
                             </select>
                        </div>
                        <div class="form-group col-md-6">
                             <img src="assets/front/images/form_icons/ic_proof.png" class="form-input-icon" alt="mob-img" />
                             <select class="form-control"  name ="document_type" id="document_type" data-parsley-required data-parsley-required-message="Please select document.">
                                <option value="">Select Document</option>
                                <option value="Certificate of Operation" <?php if(!empty($user_data['document_type'])){if($user_data['document_type']=='Certificate of Operation') echo 'selected';}?>>Certificate of Operation</option>
                                <option value="Govt ID Proof" <?php if(!empty($user_data['document_type'])){if($user_data['document_type']=='Govt ID Proof') echo 'selected';}?>>Govt ID</option>
                                <option value="SSN Number" <?php if(!empty($user_data['document_type'])){if($user_data['document_type']=='SSN Number') echo 'selected';}?>>SSN Number</option>
                             </select>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="form-group col-md-6">
                                <div class="preview-section">
                                    <div class="document_proof_image">
                                    <?php if(!empty($user_data['document_proof'])){
                                            $type=explode('.',explode('/', $user_data['document_proof'])[3])[1];
                                            if(strtolower($type)=='doc' || strtolower($type)=='docx' || strtolower($type)=='txt' || strtolower($type)=='rtf'){ ?>
                                                 <a href="<?php echo $user_data['document_proof'];?>"><img src="assets/images/Doc-icon.png" alt="Document proof"></a>
                                            <?php }else if(strtolower($type)=='pdf'){ ?>
                                               <a href="<?php echo $user_data['document_proof'];?>" target="_blank"><img src="assets/images/pdf_default.jpg" alt="Document proof"></a>
                                            <?php }else if(strtolower($type)=='png' || strtolower($type)=='jpg' || strtolower($type)=='jpeg'){ ?>
                                               <a href="<?php echo $user_data['document_proof'];?>" target="_blank"><img src="<?php echo $user_data['document_proof'];?>" alt="Document proof"></a>
                                           <?php  } ?>
                                    <?php }else{ ?>
                                        <img src="resources/default_image.png" alt="Document proof">
                                    <?php } ?>
                                   </div>
                                </div>
                            </div>
                            <div class="form-group col-md-6 colors">
                                <input type="file" name ="document_proof" id="document_proof" class="user_document"  style="padding: 0px;" <?php if(isset($show_sp_data) && !empty($show_sp_data)){ echo 'data-parsley-required data-parsley-required-message="Please upload document."';}?>/>
                                <div id="document_error_image" class="error"></div>
                            </div>
                        </div>
                     </div>
                    <?php } if($user_data['user_type']==1 || $user_data['user_type']==3){ ?>
                        <div class="row">
                            <div class="form-group col-md-6">
                                 <img src="assets/front/images/icons/cake-with-candles.svg" class="form-input-icon" style="width: 20px;" alt="">
                                <input type="text" class="form-control datepicker" name ="dob" id="dob" placeholder="Date of Birth" value="<?php if($user_data['dob']!='0000-00-00'){echo date('m/d/Y',strtotime($user_data['dob']));}?>" oninput="this.value = this.value.replace(/[^]/g, '').replace(/(\..*)\./g, '$1');"/>
                                <div class="input-group-addon">
                                    <span class="glyphicon glyphicon-th"></span>

                                </div>
                           </div>
                            <div class="form-group col-md-6">
                                 <img src="assets/front/images/form_icons/ic_proof.png" class="form-input-icon" alt="mob-img" />
                                 <select class="form-control"  name ="relationship_status" id="relationship_status">
                                    <option value="">Select Relationship Status</option>
                                    <option value="Single" <?php if(!empty($user_data['relationship_status'])){if($user_data['relationship_status']=='Single') echo 'selected';}?>>Single</option>
                                    <option value="Married" <?php if(!empty($user_data['relationship_status'])){if($user_data['relationship_status']=='Married') echo 'selected';}?>>Married</option>
                                 </select>
                            </div>
                            <div class="form-group col-md-6">
                                 <img src="assets/front/images/form_icons/ic_proof.png" class="form-input-icon" alt="mob-img" />
                                 <select class="form-control"  name ="gender" id="gender">
                                    <option value="">Select Gender</option>
                                    <option value="Male" <?php if(!empty($user_data['gender'])){if($user_data['gender']=='Male') echo 'selected';}?>>Male</option>
                                    <option value="Female" <?php if(!empty($user_data['gender'])){if($user_data['gender']=='Female') echo 'selected';}?>>Female</option>
                                    <option value="Other" <?php if(!empty($user_data['Other'])){if($user_data['gender']=='Other') echo 'selected';}?>>Other</option>
                                 </select>
                            </div>
                            <div class="form-group col-md-6">
                               <img src="assets/front/images/icons/cake-with-candles.svg" class="form-input-icon" style="width: 20px;" alt="">
                               <input maxlength="150" type="text" class="form-control" name ="education" id="education" placeholder="Education" value="<?php if(!empty($user_data['education'])){echo $user_data['education'];}?>" />
                            </div>
                            <div class="form-group col-md-6">
                               <img src="assets/front/images/icons/cake-with-candles.svg" class="form-input-icon" style="width: 20px;" alt="">
                               <input  maxlength="200" type="text" class="form-control" name ="hobbies" id="hobbies" placeholder="Hobbies" value="<?php if(!empty($user_data['hobbies'])){echo $user_data['hobbies'];}?>" />
                            </div>
                            <div class="form-group col-md-6">
                               <img src="assets/front/images/icons/cake-with-candles.svg" class="form-input-icon" style="width: 20px;" alt="">
                               <input type="text" maxlength="200" class="form-control" name ="interests" id="interests" placeholder="Interests" value="<?php if(!empty($user_data['interests'])){echo $user_data['interests'];}?>" />
                            </div>
                            <div class="form-group col-md-12">
                               <textarea class="form-control" maxlength="250" name="about_description" id="about_description" placeholder="About Description" style="height: 100px;"><?php if(!empty($user_data['about_description'])){echo $user_data['about_description'];}?></textarea>
                            </div>
                        </div>
                    <?php }  }?>
                    
                  </div>
               </div>
               <div class="col-md-4 payment-card">
                  <h6 class="py-4 main-heading">Upload Photo</h6>
                     <div class="accordion book-service">
                        <div data-provides="fileupload" class="fileupload fileupload-new">
                            <div class="fileupload-new thumbnail">
                                <img alt="No Image" src="<?php echo !empty($user_data['profile_picture'])?$user_data['profile_picture']:'assets/front/images/profile0.png';?>">
                            </div>
                            <div class="fileupload-preview fileupload-exists thumbnail"></div>
                            <div>
                                 <div class="drop-zone">
                                    <div class="area">
                                        <p>Only jpg, jpeg, png files are allowed to upload.</p>
                                        <input type="file" name="profile_picture" id="image" class="default user_profile_pic" accept="image/*">
                                        <div id="eeror_image" class="error"></div>
                                     </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php if($this->session->userdata('front_user_type')==3 || $this->session->userdata('front_user_type')==2){?>
                   <div class="text-center">
                      <a class="btn btn-primary" href="subscription"> My Subscription </a>
                   </div>
                <?php } ?>
               </div>
            </div>
            <div class="d-flex mt-4">
               <button type="button" onclick ="return form_submit('edit_profile');" class="btn btn-primary px-4">Update</button>
               <a href="change-password" class="change-pass-link"> Change Password </a>
            </div>
         </form>
      </div>
   </div>
</section>
</div>
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo GOOGLE_API_KEY; ?>"></script>
<script type="text/javascript">
    $( document ).ready(function() {
       var user_type='<?php if(!empty($user_data['user_type'])) echo $user_data['user_type'];?>';
       var document_status='<?php if(!empty($user_data['document_status'])) echo $user_data['document_status'];?>';
       if(user_type!=1 && document_status !='Accepted'){
            var str='';
            if(document_status=='Rejected'){
                str='. Please upload valid documents.';
            }
            $("#status_box").html('<div class="alert alert-block alert-danger">Your document is '+document_status+str+'.</div>');
       }
    });
    var options = {
        componentRestrictions: {
            country: 'us'
        }
    };
    var autocompletes = new google.maps.places.Autocomplete(document.getElementsByClassName('google_address')[0],options);

    google.maps.event.addListener(autocompletes, 'place_changed', function () {
        var place = autocompletes.getPlace();
        var lat = place.geometry.location.lat();
        var long = place.geometry.location.lng();
        $("#latlong").hide();
        $('.lat').val(lat);
        $('.lng').val(long);
    });
    function clear_address(){ 
        $(".lat").val('');
        $(".lng").val('');
    }
    $(".user_profile_pic").change(function() {
        $('#success').html('');
        var val = $(this).val();
        if(val!=''){
        switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase()){
            case 'jpg': case 'png':  case 'jpeg':
               $("#eeror_image").html('');
                break;
            default:
                //$(this).val('');
                $("#eeror_image").html('Only jpg, jpeg, png files are allowed to upload.');
                break;
        }
        }
    });
    $('.datepicker').datepicker({
        format: 'mm/dd/yyyy',
        todayHighlight: true,
        endDate: '+0d',
        autoclose: true,
        orientation: "bottom auto",
    });

    var is_email_valid=1;
    function email_check(){
       return new Promise(function(resolve, reject){
        error_msg_email="";
        $("#access_email_error").hide();
        var matched_value = $("#email").val().trim();
        var matched_column = 'email';
        var matched_id='user_id';
        var id = "<?php echo $user_data['user_id']; ?>";
        var table="users";
        if(matched_value!='') {
            $.ajax({
                type:'POST',
                url: "<?php echo base_url(); ?>admin/ajax/check_unique/",
                data: {matched_value:matched_value,matched_column:matched_column,table:table,matched_id:matched_id,id:id},
                success:function(data)
                {
                    if(data==1) {
                        is_email_valid=0;alert(is_email_valid);
                        error_msg_email = "Email ID already used." ;
                        $("#access_email_error").show();
                        reject();
                        
                    } else {
                        $("#access_email_error").hide();
                        is_email_valid=1;
                        resolve();
                       
                    }
                }
            });
        } else {
            is_email_valid=1;
            error_msg_email = "" ;
            $("#access_email_error").hide();
            resolve();
        }
    });
        
    }
  
    function form_submit(id)
    {
     
          email_check().then(function(){
                submitDetailsForm(id);
          }).catch(function(){
            return false;
          });
    }  

    $(".user_document").change(function() {
        $('#success').html('');
        var val = $(this).val();
        if(val!=''){
            switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase()){
            case 'jpg': case 'png':  case 'jpeg': case 'pdf': case 'doc': case 'docx': 
               $("#document_error_image").html('');
                break;
            default:
               // $(this).val('');
                $("#document_error_image").html('Only jpg, jpeg, png, pdf, doc, docx files are allowed to upload.');
                break;
            }
        }
    }); 
</script>