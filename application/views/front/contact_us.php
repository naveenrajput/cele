<link rel="stylesheet" href="<?php echo base_url();?>assets/front/css/styles.css">
<section class="section-div py-5 aboutus-section border">
   <section class="Material-contact-section section-padding section-dark">
      <div class="container">
         
         <div class="row">

            <!-- Section Titile -->
            <div class="col-md-6">
               <h2 class="aboutus-title"> <?php echo !empty($page_detail['title'])?$page_detail['title']:'N/A';?></h2>
               <?php echo !empty($page_detail['content'])?$page_detail['content']:'N/A';?>
            </div>
            <!-- contact form -->
            <div class="col-md-6">
               <div class="card add-service-card">
                  <div class="row">
                     <div class="col-md-12 service-details-div">
                        <div class="message_box" style="display:none;"></div>
                        <div class="card-banner service-slider-container">
                           <h2 class="py-4 main-heading">Get In Touch</h2>
                        </div>
                        <div class="accordion book-service">
                           <form class="add-service-form" id="contact_form" action="" data-parsley-validate>
                              <div class="row">
                                 <div class="form-group col-md-12">
                                    <input type="text" class="form-control" placeholder="eg. John Doe" name="name" maxlength="50" data-parsley-required data-parsley-required-message="Please enter name.">
                                 </div>
                                 <div class="form-group col-md-12">
                                    <input type="text" class="form-control" placeholder="test@mail.com" name="email" data-parsley-required data-parsley-required-message="Please enter email." data-parsley-type="email" data-parsley-type-message="Please enter valid email.">
                                 </div>
                                 <div class="form-group col-md-12">
                                    <input type="text" class="form-control" placeholder="Subject" maxlength="150" name="subject" data-parsley-required data-parsley-required-message="Please enter subject.">
                                 </div>
                                 <div class="form-group col-md-12">
                                    <textarea class="form-control" rows="4" cols="50"
                                       placeholder="Type message here..." maxlength="500" name="message" data-parsley-required data-parsley-required-message="Please enter message."></textarea>
                                 </div>

                              </div>
                              <div class="d-flex justify-content-center mt-4">
                                 <button type="submit" class="btn btn-success px-4" onclick="return form_submit('contact_form');">Submit</button>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</section>
<script type="text/javascript">
    function form_submit(id)
    {
        submitDetailsForm(id);
        return false;
    }
</script>