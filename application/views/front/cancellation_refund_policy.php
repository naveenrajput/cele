<section class="section-div py-5 aboutus-section border">
    <div class="container">
        <h2 class="aboutus-title"><?php echo $page_detail['title']; ?></h2>
        <?php echo $page_detail['content']; ?>
    </div>
</section>