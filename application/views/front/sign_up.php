<section class="signup-section">
   <div class="slider-container">
      <div class="login-image">
         <div class="hero-text">
            <h1 style="font-size:50px"><?php echo $page_title;?></h1>
         </div>
      </div>
   </div>
   <div class="main-login-box">
      <div class="slider-4 login-left-banner">
         <div class="overlay-color">
            <div class="login-left-banner-logo">
               <img src="assets/front/images/Group9.png" alt="logo">
               <h5 class="text-center">Celebrate your life </h5>
            </div>
            <div class="text-box">
               <h4>Simplify your search</h4>
               <h5>Save your favorite Services and share them with your colleagues.</h5>
            </div>
            <div class="partition-line"></div>
            <div class="text-below-line">
               <p>Have an account?  <a href="login"> Sign In Here! </a></p>
            </div>
         </div>
      </div>
      <div class="slider-8 form-container signuponly">
         <div class="form-heading text-left">
            <h5>Create your free account by filling in a few details!</h5>
            <div class="message_box" style="display:none;"></div>
         </div>
         <div class="form-option-block">
            <div class="form-check-inline" onclick="show1();">
               <label class="radio-container">
               <input type="radio" value ="1" name="optradio" checked="checked" >Celebrant
               <span class="checkmark"></span>
               </label>
            </div>
            <div class="form-check-inline" onclick="show3();">
               <label class="radio-container">
               <input type="radio"  value ="3" name="optradio">Celebrant + Service Provider
               <span class="checkmark"></span>
               </label>
            </div>
            <div class="form-check-inline" onclick="show3();">
               <label class="radio-container">
               <input type="radio"  value ="2" name="optradio">Only Service Provider?
               <span class="checkmark"></span>
               </label>
            </div>
         </div>
         <div class="" id="celebrant">
            <form class="signup-form" id="celebrant_sign_up_form" action="" enctype="multipart/form-data" data-parsley-validate>
               <div class="row">
                  <div class="form-group col-md-12">
                     <img src="assets/front/images/form_icons/ic_full_name.png" class="form-input-icon" alt="mob-img" />
                     <input type="text" class="form-control" placeholder="Full Name" name="fullname" id= "fullname" data-parsley-required data-parsley-required-message="Please enter full name." maxlength="100" />
                     <input type="hidden" name ="user_type" id="user_type" class="user_type" value="1">
                  </div>
                  <div class="form-group col-md-12">
                     <img src="assets/front/images/form_icons/ic_staging.png" class="form-input-icon" alt="mob-img" />
                     <input type="text" class="form-control" placeholder="Login ID" name="staging_id" id="staging_id" data-parsley-required data-parsley-required-message="Please enter login ID." maxlength="20" oninput="this.value = this.value.replace(/[^A-Za-z0-9._]/g,'');" />
                     <p class="error" id="un_error_title" style="display:none;">Login ID already used.</p>
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-md-12">
                     <img src="assets/front/images/form_icons/ic_email.png" class="form-input-icon" alt="mob-img" />
                     <input type="email" class="form-control" placeholder="Email ID" name="email" id="email" data-parsley-required data-parsley-required-message="Please enter email ID." data-parsley-type="email" data-parsley-type-message="Please enter valid email."/>
                     <p class="error" id="access_email_error" style="display:none;">Email already used.</p>
                  </div>
                  <div class="form-group col-md-12">
                     <img src="assets/front/images/form_icons/ic_mobile.png" class="form-input-icon" alt="mob-img" />
                     <input type="text" class="form-control" placeholder="Mobile Number" id="mobile"  name="mobile" oninput="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" data-parsley-required data-parsley-required-message="Please enter mobile no." maxlength="50" />
                     <p class="error" id="access_mobile_error" style="display:none;">Mobile no. already used.</p>
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-md-12">
                     <img src="assets/front/images/form_icons/ic_password.png" class="form-input-icon" alt="mob-img" />
                     <input type="password" class="form-control" placeholder="Password"  id="password" name="password" value="" data-parsley-required data-parsley-required-message="Please enter password." data-parsley-minlength="8" maxlength="12" autocomplete="off" data-parsley-number="1" data-parsley-alphabet="1" data-parsley-maxlength-message="Password must be at maximum 12 digits long." data-parsley-special="1" data-parsley-minlength-message="Password must be at least 8 characters long."/>
                  </div>
                  <div class="form-group col-md-12">
                     <img src="assets/front/images/form_icons/ic_password.png" class="form-input-icon" alt="mob-img" />
                     <input type="password" class="form-control" placeholder="Confirm Password" id="confpassword" name="confpassword" placeholder="Confirm Password" value="" data-parsley-required data-parsley-required-message="Please enter confirm password."  data-parsley-equalto="#password" data-parsley-equalto-message="Confirm password must be same as password." autocomplete="off"/>
                  </div>
               </div>
               <div class="row">
                    <div class="form-group col-md-12">
                         <img src="assets/front/images/form_icons/ic_country.png" class="form-input-icon" alt="mob-img" />
                         <select class="form-control" name="country" id="country" data-parsley-required data-parsley-required-message="Please select country." onchange="get_country()">
                            <option value="" >Select Country</option>
                            <?php if(isset($countries)) {
                                foreach($countries as $country) { ?>
                                    <option value=<?php echo $country['id'];?>><?php echo $country['name'];?></option>";
                                <?php }
                            } ?>
                         </select>
                    </div>
                    <div class="form-group col-md-12">
                          <img src="assets/front/images/form_icons/ic_state.png" class="form-input-icon" alt="mob-img" />
                         <select class="form-control"  name="state" id="state" data-parsley-required data-parsley-required-message="Please select state." onchange="get_states()">
                            <option id="first" value="">Select State</option>
                         </select>
                    </div>
               </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        <img src="assets/front/images/form_icons/ic_state.png" class="form-input-icon" alt="mob-img" />
                        <select class="form-control" id='city' name="city" data-parsley-required data-parsley-required-message="Please select city.">
                            <option id='city_first' value="">Select City</option>

                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <img src="assets/front/images/form_icons/ic_address.png" class="form-input-icon" alt="mob-img" />
                        <input type="text" class="form-control google_address" id ="address" name="address" placeholder="Address" data-parsley-required data-parsley-required-message="Please enter address." oninput="clear_address()"/>
                        <p class="error" id="latlong" style="display:none;">Please enter valid address.</p>
                        <input type="hidden" id="lat" class="lat" name="latitude" value="">
                        <input type="hidden" id="lng" class="lng" name="longitude" value="">
                    </div>
               </div>
               <div class="text-center mt-3">
                  <button type="button" id ="celebrant_btn" class="btn btn-primary px-4" onclick="return form_submit('celebrant_sign_up_form','celebrant');">Sign Up</button>
               </div>
            </form>
         </div>

         <div class="" id="onlySP">
            <form class="signup-form" id="sp_sign_up_form" action="" enctype="multipart/form-data" data-parsley-validate>
               <div class="row">
                  <div class="form-group col-md-6">
                     <img src="assets/front/images/form_icons/ic_full_name.png" class="form-input-icon" alt="mob-img" />
                     <input type="text" class="form-control" placeholder="Full Name" name="fullname" id= "fullname" data-parsley-required data-parsley-required-message="Please enter full name." maxlength="100" />
                     <input type="hidden" name ="user_type" id="user_type" class="user_type" value="1">
                  </div>
                  <div class="form-group col-md-6">
                     <img src="assets/front/images/form_icons/ic_staging.png" class="form-input-icon" alt="mob-img" />
                     <input type="text" class="form-control" placeholder="Login ID" name="staging_id" id="staging_id" data-parsley-required data-parsley-required-message="Please enter login ID." maxlength="20" oninput="this.value = this.value.replace(/[^A-Za-z0-9._]/g,'');" />
                     <p class="error" id="un_error_title" style="display:none;">Login ID already used.</p>
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-md-6">
                     <img src="assets/front/images/form_icons/ic_email.png" class="form-input-icon" alt="mob-img" />
                       <input type="email" class="form-control" placeholder="Email ID" name="email" id="email" data-parsley-required data-parsley-required-message="Please enter email ID." data-parsley-type="email" data-parsley-type-message="Please enter valid email."/>
                     <p class="error" id="access_email_error" style="display:none;">Email already used.</p>
                  </div>
                  <div class="form-group col-md-6">
                     <img src="assets/front/images/form_icons/ic_mobile.png" class="form-input-icon" alt="mob-img" />
                     <input type="text" class="form-control" placeholder="Mobile Number" id="mobile"  name="mobile" oninput="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" data-parsley-required data-parsley-required-message="Please enter mobile no." maxlength="50"/>
                     <p class="error" id="access_mobile_error" style="display:none;">Mobile no. already used.</p>
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-md-6">
                     <img src="assets/front/images/form_icons/ic_password.png" class="form-input-icon" alt="mob-img" />
                     <input type="password" class="form-control" placeholder="Password"  id="sp_password" name="password" value="" data-parsley-required data-parsley-required-message="Please enter password." data-parsley-minlength="8" maxlength="12" autocomplete="off" data-parsley-number="1" data-parsley-alphabet="1" data-parsley-maxlength-message="Password must be at maximum 12 digits long." data-parsley-special="1" data-parsley-minlength-message="Password must be at least 8 characters long."/>
                  </div>
                  <div class="form-group col-md-6">
                     <img src="assets/front/images/form_icons/ic_password.png" class="form-input-icon" alt="mob-img" />
                     <input type="password" class="form-control" placeholder="Confirm Password" id="confpassword" name="confpassword" placeholder="Confirm Password" value="" data-parsley-required data-parsley-required-message="Please enter confirm password."  data-parsley-equalto="#sp_password" data-parsley-equalto-message="Confirm password must be same as password." autocomplete="off"/>
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-md-6">
                     <img src="assets/front/images/form_icons/ic_company.png" class="form-input-icon" alt="mob-img" />
                     <input type="text" class="form-control" placeholder="Company Name" name="company_name" id= "company_name" data-parsley-required data-parsley-required-message="Please enter company name." maxlength="150" />
                  </div>
                  <div class="form-group col-md-6">
                     <img src="assets/front/images/form_icons/ic_country.png" class="form-input-icon" alt="mob-img" />
                     <select class="form-control" name="country" id="sp_country" data-parsley-required data-parsley-required-message="Please select country.">
                            <option value="">Select Country</option>
                            <?php if(isset($countries)) {
                                foreach($countries as $country) { ?>
                                    <option value=<?php echo $country['id'];?>><?php echo $country['name'];?></option>";
                                <?php }
                            } ?>
                         </select>
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-md-6">
                     <img src="assets/front/images/form_icons/ic_state.png" class="form-input-icon" alt="mob-img" />
                    <select class="form-control"  name="state" id="sp_state" data-parsley-required data-parsley-required-message="Please select state.">
                        <option id="sp_first" value="" >Select State</option>
                     </select>
                  </div>
                  <div class="form-group col-md-6">
                     <img src="assets/front/images/form_icons/ic_state.png" class="form-input-icon" alt="mob-img" />
                    <select class="form-control" id='sp_city' name="city" data-parsley-required data-parsley-required-message="Please select city.">
                        <option id='sp_city_first' value="" >Select City</option>
                    </select>
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-md-6">
                     <img src="assets/front/images/form_icons/ic_address.png" class="form-input-icon" alt="mob-img" />
                      <input type="text" class="form-control sp_google_address" id ="address" name="address" placeholder="Address" data-parsley-required data-parsley-required-message="Please enter address." oninput="clear_address()" />
                        <p class="error" id="sp_latlong" style="display:none;">Please enter valid address.</p>
                        <input type="hidden" id="lat" class="sp_lat" name="latitude" value="">
                        <input type="hidden" id="lng" class="sp_lng" name="longitude" value="">
                  </div>
                  <div class="form-group col-md-6">
                     <img src="assets/front/images/form_icons/ic_phone.png" class="form-input-icon" alt="mob-img" />
                     <input type="text" class="form-control" placeholder="Business Phone Number (Optional)" id="business_no"  name="business_no" oninput="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" maxlength="50" />
                  </div>
               </div>
               <div class="row">
                  <div class="form-group col-md-6">
                     <img src="assets/front/images/form_icons/ic_year.png" class="form-input-icon" alt="mob-img" />
                     <select class="form-control" name="establishment_year" id ="establishment_year" data-parsley-required data-parsley-required-message="Please select establishment year.">
                     <option value="">Year of Establishment</option>
                        <?php for($i=date('Y');$i>=1950;$i--){ ?>
                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                        <?php } ?>
                     </select>
                  </div>
                  <div class="form-group col-md-6">
                     <img src="assets/front/images/form_icons/ic_proof.png" class="form-input-icon" alt="mob-img" />
                     <select class="form-control"  name ="document_type" id="document_type" data-parsley-required data-parsley-required-message="Please select document.">
                        <option value="">Select Document</option>
                        <option value="Certificate of Operation">Certificate of Operation</option>
                        <option value="Govt ID Proof">Govt ID</option>
                        <option value="SSN Number">SSN Number</option>
                     </select>
                  </div>
                  <div class="form-group col-md-6 colors" id="license">
                     <input type="file" name ="document_proof" id="document_proof" class="user_document" data-parsley-required data-parsley-required-message="Please select document proof." style="padding: 0px;" />
                     <div id="document_error_image" class="error"></div>
                  </div>
               </div>
               <div class="text-center mt-3">
                <button type="submit" id ="sp_btn" class="btn btn-primary px-4" onclick="return form_submit('sp_sign_up_form','sp');">Sign Up</button>
               </div>
            </form>
         </div>
      </div>
   </div>
</section>
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo GOOGLE_API_KEY; ?>"></script>
<Script> 
  //////////////////////////////////////////////////celebrant/////////////////////////////////////////
   
    var options = {
        componentRestrictions: {
            country: 'us'
        }
    };
    var autocompletes = new google.maps.places.Autocomplete(document.getElementsByClassName('google_address')[0], options);

    google.maps.event.addListener(autocompletes, 'place_changed', function () {
        var place = autocompletes.getPlace();
        var lat = place.geometry.location.lat();
        var long = place.geometry.location.lng();
        $("#latlong").hide();
        $('.lat').val(lat);
        $('.lng').val(long);
    });
    
    /////////////////////////////////////////////////////////sp , c+sp/////////////////////////////////////
    var autocomplete = new google.maps.places.Autocomplete(document.getElementsByClassName('sp_google_address')[0]);

    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        var lat = place.geometry.location.lat();
        var long = place.geometry.location.lng();
        $("#sp_latlong").hide();
        $('.sp_lat').val(lat);
        $('.sp_lng').val(long);
    });
    function clear_address(){ 
        $(".sp_lat").val('');
        $(".sp_lng").val('');
        $(".lat").val('');
        $(".lng").val('');
    }
    $('#sp_country').change(function(e) {
        var id = $("#sp_country").val();
        if(id!='') {
            $('#sp_state option').slice(1).remove();

            $.ajax({
                type:'POST',
                url: "<?php echo base_url(); ?>admin/ajax/get_states/",
                data: {id:id},
                
                success:function(data)
                {
                    if(data) {
                        var states="";
                        data = JSON.parse(data);
                        
                        //sub_outlets += "<option value=''>Select Sub Outlet</option>";
                        for(var i=0;i<data.length;i++) {
                            states += "<option value="+data[i].id+">"+data[i].name+"</option>";
                        }
                        $("#sp_state").find("#sp_first").after(states);
                        $("#sp_city").html('');
                        $("#sp_city").html('<option id="sp_city_first" value="" >Select City</option>');
                    } else {
                        $("#sp_state").find("#sp_first").after('<option value="">No Record</option>');
                    }
                    //console.log(data[0].sub_outlet_id);
                }
            });
        }
    });

    $('#sp_state').change(function(e) {
        var id = $("#sp_state").val();
        if(id!='') {
            $('#sp_city option').slice(1).remove();

            $.ajax({
                type:'POST',
                url: "<?php echo base_url(); ?>admin/ajax/get_cities/",
                data: {id:id},
                
                success:function(data)
                {
                    if(data) {
                        var cities="";
                        data = JSON.parse(data);
                        
                        //sub_outlets += "<option value=''>Select Sub Outlet</option>";
                        for(var i=0;i<data.length;i++) {
                            cities += "<option value="+data[i].id+">"+data[i].name+"</option>";
                        }
                        $("#sp_city").find("#sp_city_first").after(cities);
                    } else {
                        $("#sp_city").find("#sp_city_first").after('<option value="">No Record</option>');
                    }
                }
            });
        }
    }); 
    var is_staging_id_valid=1;
    function check_staging_id(id) {
        return new Promise(function(resolve, reject){
            error_msg="";
            $("#"+id+"  #un_error_title").hide();
            var check_value=$("#"+id+" input[name=staging_id]").val().trim();
            var matched_value = check_value;
            var matched_column = 'staging_id';
            var table="users";
            if(matched_value!='') {
                $.ajax({
                    type:'POST',
                    url: "<?php echo base_url(); ?>admin/ajax/check_unique/",
                    data: {matched_value:matched_value,matched_column:matched_column,table:table},
                    
                    success:function(data)
                    {
                        if(data==1) {
                            is_staging_id_valid=0;
                            error_msg = "Login ID already used." ;
                            $("#"+id+" #un_error_title").show();
                            reject();
                        } else {
                            $("#"+id+" #un_error_title").hide();
                            is_staging_id_valid=1;
                            resolve();
                        }
                        
                    }
                });
            } else {
                is_staging_id_valid=1;
                error_msg = "" ;
                $("#"+id+"  #un_error_title").hide();
                resolve();
            }
        });
    }

    var is_mobile_valid=1;
    function mobile_check(id){
        return new Promise(function(resolve, reject){
            error_msg_mobile="";
            $("#"+id+" #access_mobile_error").hide();
           // var matched_value = $("#mobile").val().trim();
            var matched_value = $("#"+id+" input[name=mobile]").val().trim();
            var matched_column = 'mobile';
            var table="users";
            if(matched_value!='') {
                $.ajax({
                    type:'POST',
                    url: "<?php echo base_url(); ?>admin/ajax/check_unique/",
                    data: {matched_value:matched_value,matched_column:matched_column,table:table},
                    
                    success:function(data)
                    {
                        if(data==1) {
                            is_mobile_valid=0;
                            error_msg_mobile = "Mobile number already used." ;
                            $("#"+id+"  #access_mobile_error").show();
                            reject();
                        } else {
                            $("#"+id+"  #access_mobile_error").hide();
                            is_mobile_valid=1;
                            resolve();
                        }
                        
                    }
                });
            } else {
                is_mobile_valid=1;
                error_msg_mobile = "" ;
                $("#"+id+" #access_mobile_error").hide();
                resolve();
            }
        });
    }
    var is_email_valid=1;
    function email_check(id){
        return new Promise(function(resolve, reject){
            error_msg_email="";
            $("#"+id+"  #access_email_error").hide();
            //var matched_value = $("#email").val().trim();
            var matched_value = $("#"+id+" input[name=email]").val().trim();
            var matched_column = 'email';
            var table="users";
            if(matched_value!='') {
                $.ajax({
                    type:'POST',
                    url: "<?php echo base_url(); ?>admin/ajax/check_unique/",
                    data: {matched_value:matched_value,matched_column:matched_column,table:table},
                    success:function(data)
                    {
                        if(data==1) {
                            is_email_valid=0;
                            error_msg_email = "Email ID already used." ;
                            $("#"+id+"  #access_email_error").show();
                            reject();
                        } else {
                            $("#"+id+"  #access_email_error").hide();
                            is_email_valid=1;
                            resolve();
                        }
                    }
                });
            } else {
                is_email_valid=1;
                error_msg_email = "" ;
                $("#"+id+"  #access_email_error").hide();
                resolve();
            }
        });
    }
    function submitSignupForm(id,form_type) {
        
        var form_action= "<?php if(!empty($form_action))echo $form_action;?>";
        $("#"+id).parsley().validate();
        var form = $('#'+id)[0];
        if($("#"+id).parsley().isValid()){
          var formData = new FormData(form);
          $("#loader").show(); 
          if(form_type=='celebrant'){
            $("#celebrant_btn").attr('disabled',true);
          }else{
            $("#sp_btn").attr('disabled',true);
          }
          
          $.ajax({
            url: form_action,
            type: 'POST',
            data: formData,
            dataType: 'json',
            // async: false,
            cache: false,
            contentType: false,
            processData: false,
        
            success:function(resp){
              $("#loader").hide();  
              $("html, body").animate({ scrollTop: 0 }, "slow");
              $(".message_box").show(); 
              $("#celebrant_btn").attr('disabled',false);
              $("#sp_btn").attr('disabled',false);
                if(resp.status==0){
                    $(".message_box").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                    setTimeout(function() {
                     $(".message_box").html('');
                      $(".message_box").hide();
                    }, 7000); 
                  }else{
                    $(".message_box").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                    $("#"+id).parsley().reset();
                    $("#"+id)[0].reset();
                    setTimeout(function() {
                    $(".message_box").html('');
                    $(".message_box").hide();
                    $('#otp_popup').modal('show');
                    $("#otp_mobile").val(resp.mobile);
                    $("#otp_type").val('signup');
                    return false;
                    }, 7000); 
                }
            },
            error:function(err){
                $("#celebrant_btn").attr('disabled',false);
                $("#sp_btn").attr('disabled',false);
              $("#loader").hide();
            }
          });
        }
    }
    function form_submit(id,type)
    {
        /*check_staging_id(id);
        mobile_check(id);
        email_check(id);
        $("#document_error_image").html('');
        var user_type_value = $("input[name='optradio']:checked").val();
        $(".user_type").val('');
        $(".user_type").val(user_type_value);
        if(is_staging_id_valid==1 && is_mobile_valid ==1 && is_email_valid==1){
            submitSignupForm(id,type);
            return false;
        }else{
            return false;
        }*/
        
        $("#document_error_image").html('');
        var user_type_value = $("input[name='optradio']:checked").val();
        $(".user_type").val('');
        $(".user_type").val(user_type_value);
        check_staging_id(id).then(function(){
                return email_check(id);
          }).then(function(){
                return  mobile_check(id);
          }).then(function(){
               submitSignupForm(id,type);
          }).catch(function(){
            
            return false;
          });
        return false;

    }   

    $(".user_document").change(function() {
        $('#success').html('');
        var val = $(this).val();
        if(val!=''){
            switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase()){
            case 'jpg': case 'png':  case 'jpeg': case 'pdf': case 'doc': case 'docx': 
               $("#document_error_image").html('');
                break;
            default:
                $(this).val('');
                $("#document_error_image").html('Only jpg, jpeg, png, pdf, doc, docx files are allowed to upload.');
                break;
            }
        }
    });
</Script>