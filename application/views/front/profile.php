<section class="section-div pt-5 home-section">
   <div class="container">
      <div class="row">
         <div class="col-md-8 service-details friend-profile">
            <h4 class="main-heading">Activity</h4>
            <div class="message_box" style="display:none;"></div>
            <div class="flex">
               <div class="content" id="feed_box">
                  
               </div>
            </div>
            <div class="text-center pt-4"><a href="javascript:void(0)" id="loadMore">
                  <img class="spinner-icon" src="assets/front/images/home/icon/spinner-of-dots.svg" alt="">
                  <span>Load More</span></a>
            </div>
         </div>
         <div class="col-md-4 no-background">
            <h4 class="main-heading">About 
            <?php if(empty($edit_icon_hide)){ ?>
            <span class="btn btn-primary edit-profile">
            <a href="edit-profile" style="color: #fff;">
               <i class="fa fa-pencil" aria-hidden="true"></i>
            </a>
            </span>
            <?php } ?>
            </h4>
            <div class="biography-style">
               <div class="profile-box">
                  <div class="img-sq-rounded"><img src="<?php echo !empty($user_data['profile_picture'])?$user_data['profile_picture']:'assets/front/images/profile0.png';?>" alt=""  /></div>
                  <h3><?php if(!empty($user_data['fullname'])) {echo $user_data['fullname'];}else{ echo 'N/A';}?><strong><?php if(!empty($user_data['user_type'])){
                  					if($user_data['user_type']==1){
                  						echo 'Celebrant';
                  					}if($user_data['user_type']==2){
                  						echo 'Service Provider';
                  					}if($user_data['user_type']==3){
                  						echo 'Celebrant + Service Provider';
                  					}
              					}?></strong></h3>
               </div>
               <ul class="address-listing-style">
                  <li> <img src="assets/front/images/icons/location.svg" alt="" /><span><?php if(!empty($user_data['address'])) {echo $user_data['address'];}else{ echo 'N/A';}?></span></li>
                  <li> <img src="assets/front/images/icons/calendar.svg" alt="" /><span>Joined <?php if(!empty($user_data['created'])) echo convertGMTToLocalTimezone($user_data['created']); ?></span></li>
                  <li> <img src="assets/front/images/icons/cake-with-candles.svg" alt="" /><span><?php if(!empty($user_data['dob']) && $user_data['dob']!='0000-00-00'){echo convertGMTToLocalTimezone($user_data['dob']);}else{ echo 'N/A';} ?></span></li>
               </ul>
               <div class="bio-detail">
                  <h3>Bio</h3>
                  <p><?php if(!empty($user_data['about_description'])){echo $user_data['about_description'];}else{echo 'N/A';}?></p>
               </div>
               <?php if($this->session->userdata('front_user_type')==3){
                if($edit_icon_hide==0){ ?>
               <div class="text-center">
                  <a class="btn btn-primary" href="subscription"> My Subscription </a>
               </div>
               <?php } } ?>
            </div>
         </div>
      </div>
   </div>
</section>
<script>
$(document).ready(function () {
      $(".content").slice(0, 4).show();
      $("#loadMore").on("click", function (e) {
         e.preventDefault();
         $(".content:hidden").slice(0, 2).slideDown();
         if ($(".content:hidden").length == 0) {
            $("#loadMore").text("No Content").addClass("noContent");
         }
      });
   });
$("#loader-wrapper").show();
    var totalrecords='<?php echo $total_records;?>';
    var track_page = 0;
     //track user scroll as page number, right now page number is 1
    load_contents(track_page);
    $(window).scroll(function() { //detect page scroll
      
        if($(window).scrollTop() + $(window).height() >= $(document).height()-600) { //if user scrolled to bottom of the page
         
            track_page++;
            var current_page=<?php echo FRONT_LIMIT; ?>*track_page;
            if(current_page<totalrecords){
            load_contents(track_page);
          }
        }
    }); 

    //Ajax load function
    function load_contents(track_page){
        $('#loader-wrapper').show(); 
        var check=$("#feed_box").html(); 
        var section2='<?php echo !empty($this->uri->segment(2))?$this->uri->segment(2):"";?>';
        $.ajax({
            type:'POST',
            data:{ 
                page:track_page,
                get_id:section2,
            },
            url: "<?php echo base_url();?>front/Login/my_post_data", 
           success:function(data) {
                 
                if(data.trim().length == 0){
                  if(check.trim().length == 0){
                    $("#loadMore").text("No Content").addClass("noContent");
                    $('#loader-wrapper').hide();
                    return;
                  }
                }
                if(track_page==0){
                    $("#feed_box").html('');
                }
                $('#loader-wrapper').hide(); 
                $("#feed_box").append(data);
            }
        });
    }
</script>