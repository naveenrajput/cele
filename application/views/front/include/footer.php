<footer>
    <div class="modal" id="deleteOption">
       <div class="modal-dialog">
          <div class="modal-content">
             <div class="modal-header">
                <h5>Delete Notification</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
             </div>
             <!-- Modal body -->
             <div class="modal-body">
                <p>Are you sure you want to delete ?</p>
             </div>
             <!-- Modal footer -->
             <div class="modal-footer">
                <button type="button" class="btn btn-primary" >Yes</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">NO</button>
             </div>
          </div>
       </div>
    </div>
    <div class="modal fade" id="otp_popup" style="display: none;">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
          
            <!-- Modal Header -->
            <div class="modal-header justify-content-center">
              <h4 class="modal-title">OTP Verification</h4>
            </div>
            
            <!-- Modal body -->
            <div class="modal-body text-center">
              <p class="alert_message" id="otp_msg" style='display:none;'></p>
              <p>Enter the OTP which we have sent to your registered mobile number.</p>
              <form class="add-service-form" id="otp_form" action="" data-parsley-validate>
                <div class="row">
                   <div class="form-group col-md-8 offset-md-2">
                      <input type="text" name="otp" id="otp"  class="form-control" placeholder="Enter 4 Digit OTP here" data-parsley-required data-parsley-required-message="Please enter OTP." maxlength="4">
                      <input type="hidden" name="otp_mobile" id ="otp_mobile" value="">
                      <input type="hidden" name="otp_type" id ="otp_type" value="">
                      <div class="pull-right pt-2">
                       <a href="JavaScript:void(0);" onclick="resend_otp()" style="color: #3354A5;">Resend OTP</a>
                     </div>
                   </div>
                  </div>
             </form>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer justify-content-center">
              <button type="button" id="otp_button" class="btn btn-success" onclick="return otp_submit()" >Submit</button>
            </div>
            
          </div>
        </div>
    </div> 
    <div class="upsection">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-3 col-md-4">
                    <a href="<?php echo site_url();?>" class="logo-footer"><img src="assets/front/images/Group8.png" > </a>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour </p>
                </div>
                <div class="col-xs-12 col-md-3 col-md-8">
                    <div class="row">
                        <?php if($this->session->userdata('user_id')) { ?>
                        <?php if($this->session->userdata('user_type')==2) { ?>
                        <!-- <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 testfooter">
                            <div class="footer-witget">
                                <h2> Useful Link  </h2>
                                <ul>
                                    <li><a href="#"> My Store  </a></li>
                                    <li><a href="#"> My Orders   </a></li>
                                    <li><a href="#"> My Orders Explore  </a></li>
                                </ul>
                            </div>
                        </div>  -->
                        <?php } }?>
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 testfooter">  
                            <div class="footer-witget">
                                <h2> Information  </h2>
                                <ul>
                                    <li><a href="<?php echo base_url().'about-us' ?>"> About Us </a></li>
                                    <li><a href="<?php echo base_url().'privacy-policy' ?>"> Privacy Policy   </a></li>
                                    <li><a href="<?php echo base_url().'terms-and-conditions' ?>"> Terms of Use  </a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 testfooter">
                            <div class="footer-witget">
                                <h2> Support  </h2>
                                <ul>
                                    <li><a href="cancellation-refund-policy"> Cancellation & Refund Policy</a></li>
                                    <li><a href="contact-us"> Contact Us  </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <input type="hidden" id="user_ID" value="<?php echo $this->session->userdata('user_id'); ?>" />
<a href="<?php echo base_url();?>" class="copyright" data-check="<?php echo base_url();?>">  © <?php echo date('Y');?> <?php echo SITE_TITLE;?>. All Rights Reserved. </a>

    <!--------------------- Chat model start ----------------->
    <?php
    if ($this->session->userdata('user_id')) { ?>
    
        <script src="<?php echo NODEURL; ?>assets/js/chat_system.js" authKey="U2FsdGVkX1+g3Od8NrmQqU52Q0w8EsFRmAfcF0rqfEfbW5NecUZQRy6RsmdqE8SEG7i46mKyFRuTC0UOem984Q+BFwYJbNbkNzY9bRrCO3Q=" gcs_userId="<?php echo $this->session->userdata('user_id'); ?>"></script>
    <?php  } ?>




<!----------------------Chat model End ------------------->

<!-- <div class="chat-room" style="display: none;">
    <a href="#"><img src="assets/front/images/form_icons/ic_chat_white.png"> <span class="chat-text">Chat test</span></a>
</div> -->

</footer>

<script src="assets/front/js/custom.js"></script>
<script src="assets/js/moment.js"></script>
<script>
$(document).ready(function(){
  $(".dropdown").click(function(){ 
    $(this).children('.dropmenuContainer1').toggle();
  });
  $(".form-control").each(function(e){ 
        var attr = $(this).attr('autocomplete'); 
        if (typeof attr !== typeof undefined && attr !== false) { 
        }else{      
            $(this).attr('autocomplete','sdfsd'); 
        }
    })
$( ".dropdown" ).click(function() {
  $( this ).dropdown.toggle();
});


});

// accept reject request HEADER
function accept_reject_header(sent_by,request_id,status,i){
    //$("#loader-wrapper").show(); 
    $.ajax({
        type: 'POST',
        url: "front/Home/accept_reject_request",
        data: {sent_by:sent_by,request_id:request_id,status:status},
        dataType: 'json',
        success:function(resp){
            $("#loader-wrapper").hide(); 
            console.log(resp);
            if(resp.status==0){
                /*$(".message_box").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                    setTimeout(function() {
                     $(".message_box").html('');
                     $(".message_box").hide();
                    }, 5000);*/ 
                    $("#sp"+i).html('<span> has reject your request </span>')
                    $("#acc"+i).hide();
                    $("#rej"+i).replaceWith('<button class="btn btn-reject" >Some error occured!</button>')
            }else{
               /*$(".message_box").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                    setTimeout(function() { 
                     $(".message_box").html('');
                     $(".message_box").hide();
                     location.reload();
                    }, 5000);*/
                    if(status=='Accepted'){
                      $("#sp"+i).html('<span> is now your friend </span>')
                    }
                    $("#rej"+i).hide();
                    $("#acc"+i).replaceWith('<button class="btn btn-accept">'+resp.rq_status+'</button>')
            }
            
        }
    });     
}

function otp_submit(){
    var mobile=$("#otp_mobile").val();
    var otp=$("#otp").val();
    var otp_type=$("#otp_type").val();
       $("#loader-wrapper").show();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>front/Login/verify_otp",
            data: {mobile:mobile,otp:otp,otp_type:otp_type},
            success: function(response) { 
                $("#loader-wrapper").hide();
                var res = JSON.parse(response);
                if(res.status==1) {
                    document.getElementById("otp_form").reset(); 
                    $('#otp_msg').html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+res.msg+'</div>');
                    $('#otp_msg').css('display','block');
                    setTimeout(function() {
                        $('#otp_msg').fadeOut('slow');
                        $('#otp_popup').modal('hide');
                        if(otp_type!='login'){
                            location.href="<?php echo base_url();?>login";
                            return false;
                        }
                        var user_type=res.details;
                        switch(user_type) {
                            case '1':
                                location.href="<?php echo base_url();?>home";
                                break;
                            case '2':
                                location.href="<?php echo base_url();?>manage-service";
                                break;
                            case '3':
                                location.href="<?php echo base_url();?>home";
                                break;
                          default:
                          break;
                        }    
                    }, 2000);
                    
                } else {
                    $('#otp_msg').html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+res.msg+'</div>');
                    $('#otp_msg').css('display','block');
                    setTimeout(function() {
                        $('#otp_msg').fadeOut('slow');
                    }, 10000); 
                }
                
            },
        });
    return false;
}
function resend_otp(){
   var mobile=$("#otp_mobile").val();
   $("#loader-wrapper").show();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>front/Login/resend_otp",
            data: {mobile:mobile},
            success: function(response) { 
                $("#loader-wrapper").hide();
                var res = JSON.parse(response);
                if(res.status==1) {
                    document.getElementById("otp_form").reset(); 
                    $('#otp_msg').html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+res.msg+'</div>');
                    $('#otp_msg').css('display','block');
                    setTimeout(function() {
                        $('#otp_msg').fadeOut('slow');
                    }, 2000);
                    
                } else {
                    $('#otp_msg').html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+res.msg+'</div>');
                    $('#otp_msg').css('display','block');
                    setTimeout(function() {
                        $('#otp_msg').fadeOut('slow');
                    }, 10000); 
                }
                
            },
        });
    return false;
}
function get_country(){
     var id = $("#country").val();
    if(id!='') {
        $('#state option').slice(1).remove();

        $.ajax({
            type:'POST',
            url: "<?php echo base_url(); ?>admin/ajax/get_states/",
            data: {id:id},
            
            success:function(data)
            {
                if(data) {
                    var states="";
                    data = JSON.parse(data);
                    
                    //sub_outlets += "<option value=''>Select Sub Outlet</option>";

                    for(var i=0;i<data.length;i++) {
                        states += "<option value="+data[i].id+">"+data[i].name+"</option>";
                    }
                    $("#state").find("#first").after(states);
                    $("#city").html('');
                    $("#city").html('<option id="city_first" value="" >Select City</option>');
                } else {
                    $("#state").find("#first").after('<option value="">No Record</option>');
                }
                //console.log(data[0].sub_outlet_id);
            }
        });
    }
}
function get_states(){
     var id = $("#state").val();
    if(id!='') {
        $('#city option').slice(1).remove();

        $.ajax({
            type:'POST',
            url: "<?php echo base_url(); ?>admin/ajax/get_cities/",
            data: {id:id},
            
            success:function(data)
            {
                if(data) {
                    var cities="";
                    data = JSON.parse(data);
                    
                    //sub_outlets += "<option value=''>Select Sub Outlet</option>";
                    for(var i=0;i<data.length;i++) {
                        cities += "<option value="+data[i].id+">"+data[i].name+"</option>";
                    }
                    $("#city").find("#city_first").after(cities);
                } else {
                    $("#city").find("#city_first").after('<option value="">No Record</option>');
                }
            }
        });
    }
}
function submitDetailsForm(id,seturl='') {
    if(seturl)
    {
        var form_action=seturl;
    }else
    {
        var form_action= "<?php if(!empty($form_action))echo $form_action;?>";
    }
    var last_element = form_action.split("/").pop(-1);
    //alert(form_action);
    $("#"+id).parsley().validate();
    var form = $('#'+id)[0];
    if($("#"+id).parsley().isValid()){
      var formData = new FormData(form);
      $("#loader-wrapper").show();
      $("#submit_form").attr('disabled',true);
      $.ajax({
        url: form_action,
        type: 'POST',
        data: formData,
        dataType: 'json',
        // async: false,
        cache: false,
        contentType: false,
        processData: false,
    
        success:function(resp){
          $("#loader-wrapper").hide();  
          $("html, body").animate({ scrollTop: 0 }, "slow");
          $(".message_box").show(); 
          $("#submit_form").attr('disabled',false);
            ajax_check_session(resp);
            if(resp.status==0){
                $(".message_box").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                setTimeout(function() {
                 $(".message_box").html('');
                  $(".message_box").hide();
                }, 5000); 
              }else{
                $(".message_box").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                $("#"+id).parsley().reset();
                $("#"+id)[0].reset();
                setTimeout(function() { 
                 $(".message_box").html('');
                  $(".message_box").hide();
                  if(last_element=='upgrade-profile'){
                        //alert('ok');
                        location.href="home";
                    }else{
                      get_page_by_ajax(form_action);
                    }
                }, 2000); 
            }
        },
        error:function(err){
          $("#submit_form").attr('disabled',false);
          $("#loader-wrapper").hide();
        }
      });
    }
}

function get_page_by_ajax(form_action){
    //$("#loader-wrapper").show(); 

    $.ajax({
        url: form_action,
        type: 'GET',
        data: {id:1},
        success:function(resp){
         // $("#loader-wrapper").hide();  
            if(resp){
               $(".data_reload").html(resp);
              }else{
                location.reload();
            }
        },
        error:function(err){
          location.reload();
          //$("#loader-wrapper").hide();
        }
      });
}

$('#otp').keydown(function (e){
    if(e.keyCode == 13){
        $("#otp_button").click();
         return false; 
        //otp_submit();
    }
});
function delete_front_records(table,field,id) {
    swal({
        title: "Confirmation",
        text: "Are you sure you want to delete ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                type: 'POST',
                url: "<?php echo base_url('admin/Ajax/front_soft_delete_record'); ?>",
                data: {table:table,field:field,id:id},
                dataType: 'json',
                success:function(resp){
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    $(".message_box").html(resp.msg);
                    $(".message_box").show();
                    if(resp.status==0){
                        $(".message_box").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                            setTimeout(function() {
                             $(".message_box").html('');
                             $(".message_box").hide();
                            }, 5000); 
                    }else{
                        $(".message_box").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                            setTimeout(function() {
                                $(".message_box").html('');
                                $(".message_box").hide();
                                location.reload();
                            }, 2000); 
                    }
                    
                }
            });
        }
    });
}
function hard_delete_front_records(table,field,id) {
    swal({
        title: "Confirmation",
        text: "Are you sure you want to delete ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                type: 'POST',
                url: "<?php echo base_url('admin/Ajax/front_hard_delete_record'); ?>",
                data: {table:table,field:field,id:id},
                dataType: 'json',
                success:function(resp){
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    $(".message_box").html(resp.msg);
                    $(".message_box").show();
                    if(resp.status==0){
                        $(".message_box").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                            setTimeout(function() {
                             $(".message_box").html('');
                             $(".message_box").hide();
                            }, 5000); 
                    }else{
                        $(".message_box").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                            setTimeout(function() {
                                $(".message_box").html('');
                                $(".message_box").hide();
                                location.reload();
                            }, 1000); 
                    }
                    
                }
            });
        }
    });
}
function get_event_attendee(status,id,user_id) {
    $("#loader-wrapper").show();
    var tab='attendee';
    var box="";
    var url=""; 
    switch(tab){
        case 'attendee':
             box="#attendee_box";
             url='front/Booking/get_event_attendee/'+status+'/'+id+'/'+user_id;
        break;  
    } 
    $(box).load(url,function() { 
        $("#loader-wrapper").hide(); 
    }); 
}
function event_req_accept_reject(status,request_id){
    var text='';
    if(status=='Accepted'){
        text='accept';
    }else{text='reject';}
    swal({
        title: "Confirmation",
        text: "Are you sure you want to "+text+" this request ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $("#loader-wrapper").show(); 
            $.ajax({
                type: 'POST',
                url: "accept-reject-event-status",
                data: {status:status,request_id:request_id},
                dataType: 'json',
                success:function(resp){
                    $("#loader-wrapper").hide();
                    $("#rq_btn").attr('disabled',false);
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    $(".message_box").html(resp.msg);
                    $(".message_box").show();
                    if(resp.status==0){
                        $(".message_box").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                            setTimeout(function() {
                             $(".message_box").html('');
                             $(".message_box").hide();
                            }, 5000); 
                    }else{
                        $(".message_box").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                            setTimeout(function() {
                                $(".message_box").html('');
                                $(".message_box").hide();
                            location.reload();
                            }, 1000); 
                    }
                    
                }
            });
        }
    });
}
function upgrade_sp(){
    swal({
        title: "Confirmation",
        text: "Are you sure you want to upgrade as celebrant SP ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $("#loader-wrapper").show(); 
            $.ajax({
                type: 'POST',
                url: "front/Login/upgrade_sp_as_csp",
                data: {},
                dataType: 'json',
                success:function(resp){
                    $("#loader-wrapper").hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    $(".message_box").html(resp.msg);
                    $(".message_box").show();
                    if(resp.status==0){
                       alert(resp.msg); 
                    }else{
                        window.location.href="home";
                    }
                    
                }
            });
        }
    });
}
function get_order_items(order_id) {
    $("#loader-wrapper").show();
    var tab='items';
    var box="";
    var url=""; 
    switch(tab){
        case 'items':
             box="#item_box";
             url='front/Booking/get_order_items/'+order_id;
        break;  
    } 
    $(box).load(url,function() { 
        $("#loader-wrapper").hide(); 
    }); 
} 


function get_notification() {
    $("#loader-wrapper").show();
    var tab='item';
    var box='notification_box';
    var url=""; 
    switch(tab){
        case 'item':
             box=".notification_box";
             url='front/Login/notification_list';
        break;  
    } 
    $(box).load(url,function() { 
        $("#loader-wrapper").hide(); 
    }); 
} 
function ajax_check_session(resp,is_data=''){
    if(is_data){
        if(resp.includes('{"status":"4","msg":"Session expired"}')){
            location.reload();
            return false;
        }
    }else{
        if(resp.status==4){
          location.reload();
          return false;
        }
    }
    
}
 window.Parsley.addValidator('number', {
    requirementType: 'number',
    validateString: function(value, requirement) {
        var numbers = value.match(/[0-9]/g) || [];
        return numbers.length >= requirement;
    },
    messages: {
        en: 'Password must contain at least (%s) number.'
    }
});

window.Parsley.addValidator('alphabet', {
    requirementType: 'alphabet',
    validateString: function(value, requirement) {
        var upercase = value.match(/[a-zA-Z]/g) || [];
        return upercase.length >= requirement;
    },
    messages: {
        en: 'Password must contain at least (%s) alphabet.'
    }
});
window.Parsley.addValidator('special', {
  requirementType: 'number',
  validateString: function(value, requirement) {
    var specials = value.match(/[^a-zA-Z0-9]/g) || [];
    return specials.length >= requirement;
  },
  messages: {
    en: 'Password must contain at least (%s) special characters.'
  }
});
/*$(document).ajaxComplete(function() {alert('l');
    var chk_session='<?php //echo $this->session->userdata('user_id');?>';
    alert(chk_session);
        if(chk_session==''){
            var current_url=window.location.href;
            var last_element = current_url.split("/").pop(-1);
            if(last_element!='login'){
                window.location.href='login';
            }
            return false;
        }
        return false;
});*/

</script>