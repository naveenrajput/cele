<div id="notification_list">
<?php 
    if (isset($notification_list) && !empty($notification_list)) {
   
      foreach ($notification_list as $noti) { ?>
           <?php if($noti["notification_type"]=='Broadcast' || $noti["notification_type"]=='document_status_update'){ ?>
            <li class="dropdown-item">
                <a href="javascript:void(0)" onclick="get_update('<?php echo $noti["notification_type"];?>','<?php echo base64_encode($noti["record_id"]);?>','<?php echo base64_encode($noti["notification_id"]);?>')"> 
                    <?php if($noti['is_read']==0){
                        $noti_block_color='dark_color';
                    }else{$noti_block_color='';}?>
                   <div class="single-request-block <?php echo $noti_block_color;?>">
                      <div class="request-profile-image">
                         <img src="<?php 
                                    $admin_noti_profile_pic=getUserInfo($noti['created_by'],'admin','admin_id','profile_pic');
                         echo !empty($admin_noti_profile_pic['profile_pic'])?$admin_noti_profile_pic['profile_pic']:'assets/front/images/profile0.png'?>">
                      </div>
                      <div class="request-profile-option">
                         <h4><span><?php echo $noti['notification_description']; ?></span></h4>
                         <span><?php print_r(get_timeago(strtotime($noti['creation_datetime']))); ?></span>
                      </div>
                   </div>
                </a>
            </li>
            <?php }else{ ?>
            <li class="dropdown-item">
                <a href="javascript:void(0)" onclick="get_update('<?php echo $noti["notification_type"];?>','<?php echo base64_encode($noti["record_id"]);?>','<?php echo base64_encode($noti["notification_id"]);?>')"> 
                    <?php if($noti['is_read']==0){
                        $noti_block_color='dark_color';
                    }else{$noti_block_color='';}?>
                   <div class="single-request-block <?php echo $noti_block_color;?>">
                      <div class="request-profile-image">
                         <img src="<?php echo !empty($noti['profile_picture'])?$noti['profile_picture']:'assets/front/images/profile0.png'?>">
                      </div>
                      <div class="request-profile-option">
                         <h4><span><?php echo $noti['notification_description']; ?></span></h4>
                         <span><?php print_r(get_timeago(strtotime($noti['creation_datetime']))); ?></span>
                      </div>
                   </div>
                </a>
            </li>
        <?php } ?>
        <?php } ?>
        <?php if((FRONT_LIMIT*($track_page+1)) < countNotification()) { ?>
            <li class="dropdown-item text-center"><a href="javascript:void(0)" onclick="load_more_notification(this,'<?php echo $track_page;?>')">View More</a></li>
        <?php } ?>

    <?php }else{
     echo '<li class="dropdown-item text-center">No record available.</li>';
  } ?>
  </div>
  <script>
  $(document).ready(function() {
        var chk_session='<?php echo $this->session->userdata('user_id');?>';
        if(chk_session==''){
            window.location.href='login';
        }
    });
    //Ajax load function
    function load_more_notification(ele,track_page){
        $('#loader-wrapper').show();
        $(ele).closest('.dropdown-item').remove();
        track_page++;
          $.ajax({
              type:'POST',
              data:{ 
                  page:track_page
              },
              url: "<?php echo base_url();?>front/Login/notification_list", 
             success:function(data) {
                  if(data.trim().length == 0){
                      $('#loader-wrapper').hide();
                      return;
                  }
                  $('#loader-wrapper').hide();
                  $("#notification_list").append(data);
              }
          });
    }
    function get_update(type,record_id,notification_id){
        $('#loader-wrapper').show();
        $.ajax({
              type:'POST',
              data:{ 
                  type:type,
                  record_id:record_id,
                  notification_id:notification_id,
              },
              url: "<?php echo base_url();?>front/Login/update_notification", 
             success:function(data) {
                  location.href=data;
              }
        });
    }

  </script>