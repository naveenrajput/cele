<div id="demo" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
        <li data-target="#demo" data-slide-to="0" class="active"></li>
        <li data-target="#demo" data-slide-to="1"></li>
        <li data-target="#demo" data-slide-to="2"></li>
    </ul>

    <!-- The slideshow -->
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="assets/front/images/slider2.png" alt="Los Angeles">
            <div class="carousel-caption">
                <p class="text-warning mb-3"><span class="round"></span> Eden Events Planner</p>
                <h2 class="mb-3">Is your wedding <br/> comming up?</h2>
                <span class="stw-bg ">WE WILL HANDLE YOUR BIG DAY !!</span>
            </div>
        </div>
        <div class="carousel-item">
            <img src="assets/front/images/slider2.png" alt="Los Angeles">
            <div class="carousel-caption">
                <p class="text-warning mb-3"><span class="round"></span> Eden Events Planner</p>
                <h2 class="mb-3">Is your wedding <br/> comming up?</h2>
                <span class="stw-bg ">WE WILL HANDLE YOUR BIG DAY !!</span>
            </div>
        </div>
        <div class="carousel-item">
            <img src="assets/front/images/slider2.png" alt="Los Angeles">
            <div class="carousel-caption">
                <p class="text-warning mb-3"><span class="round"></span> Eden Events Planner</p>
                <h2 class="mb-3">Is your wedding <br/> comming up?</h2>
                <span class="stw-bg ">WE WILL HANDLE YOUR BIG DAY !!</span>
            </div>
        </div>
    </div>
</div>