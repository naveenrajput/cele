<div class="service-provider">
	<div class="service">
		<div class="service-img cele-logo">
            <img src="assets/front/images/Group8.png" alt="cake">
        </div>
		<div class="service-details">
			<h6 class="userName">Cele Mobile app</h6>
			<p>Download Cele App.It's free,it's fast and has amazing features.</p>
			<ul class="list-inline app-store mt-2">
				<li class="list-inline-item">
					<a href="<?php echo GOOGLE_PLAY;?>" target="_blank">
						<img src="assets/front/images/google-pay.png" alt="cake">
					</a>
				</li>
				<li class="list-inline-item">
					<a href="<?php echo APPLE_ITUNE;?>" target="_blank">
						<img src="assets/front/images/app-store.png" alt="cake">
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>