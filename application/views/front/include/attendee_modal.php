<div class="modal" id="attendee_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal body -->
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Attendee</h5>
				<div class="card add-service-card forpop">
					<?php if(!empty($details)){ $i=0;
						foreach ($details as $key => $value) { $i++;?>
							<div class="eventDetails-option ">
								<div class="profile-image">
									<div class="request-profile-image">
										<img src="<?php echo !empty($value['profile_picture'])?$value['profile_picture']:'assets/front/images/profile0.png'; ?>" />
									</div>
									<div class="request-profile-option">
										<div class="row">
											<div class="col-md-6 pt-2">
												<a href="profile/<?php echo !empty($value['user_id'])?base64_encode($value['user_id']):'N/A';?>">
													<h4 class="mb-0 pt-2" style="color: #000;"><?php echo !empty($value['fullname'])?$value['fullname']:'N/A';?></h4>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php if($total!=$i){ ?>
								<hr>
							<?php }?>
							
					<?php } }else{ ?>
							<div class="eventDetails-option ">
								<p class="text-center">No attendee available.</p>
							</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function() { 
        $('#attendee_modal').modal('show');
    });
</script>