
<div class="modal" id="item_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal body -->
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="card add-service-card forpop">
          <div class="tab-content" id="singleService">
            <div class="tab-pane show active" id="tab1" role="tabpanel" aria-labelledby="tab1">
              <div class="accordion" id="accordion-tab-1">

                <div class="show" id="accordion-tab-1-content-1">
                  <div class="card-body pd-0">
                    <div class="table-responsive">
                      <table class="table border-none">
                        <thead>
                          <tr>
                            <th width="40%">List Items</th>
                            <th width="10%">Unit</th>
                            <th width="10%">Price</th>

                          </tr>
                        </thead>
                        <tbody>
                         <?php if(!empty($details)){//echo '<pre>';print_r($details);exit;
                            foreach ($details as $key => $value) { ?>
                             <tr>
                                <td>
                                  <div class="imgWDtext">
                                    <strong><?php echo !empty($value['item_name'])?$value['item_name']:'N/A'; ?></strong>
                                  </div>
                                </td>
                                <td><span class="textblue"><?php echo !empty($value['customer_qty'])?$value['customer_qty']:'N/A'; ?></span></td>
                                <td><span class="textblue"><?php echo ADMIN_CURRENCY; ?><?php echo !empty($value['total_price'])?$value['total_price']:'N/A'; ?></span></td>
                            </tr>
                         <?php  } }?>
                         <tr>
                            <td colspan='4'>
                                <hr style="margin:0;">
                            </td>
                        </tr>
                        <?php if(!empty($order_total)){
                                foreach ($order_total as $key => $value) { ?>
                                <tr>
                                    <td><span class="textblue"><strong><?php if(!empty($value['title'])){echo $value['title'];}else{echo 'N/A';}?></strong></span></td>
                                    <td><span class="textblue"></span></td>
                                    <td align="center"><span class="textblue"></span></td>
                                    <td><span> <?php if($value['code']=='tax'){ ?>
                                        <strong><?php if(!empty($value['value'])){ echo $value['value'].' %';} else {echo 'N/A';}?></strong>
                                    <?php }else { ?>
                                            <strong><?php echo ADMIN_CURRENCY;?><?php if(!empty($value['value'])){ echo $value['value'];} else {echo 'N/A';}?></strong>
                                    <?php } ?>
                                    
                                    </span></td>
                                </tr>
                        <?php } }?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() { 
    $('#item_modal').modal('show');
});
</script>