
<div class="slider-container">
    <div class="hero-image">
        <div class="hero-text">
            <h1 style="font-size:50px"><?php if(!empty($page_title)) echo $page_title;?></h1>
            <p><?php if(!empty($page_sub_title)) echo $page_sub_title;?></p>
        </div>
        <div class="breadcrumb-container">
            <div class="container">
                <ul class="breadcrumb">
                    <?php foreach ($breadcrumbs as  $breadcrumb) { 
                            if(!empty($breadcrumb['link'])) { ?>
                                <li><a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['title'];?></a></li>
                            <?php }else{ ?>
                                <li><span><?php echo $breadcrumb['title'];?></span></li>
                            <?php } ?>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div>