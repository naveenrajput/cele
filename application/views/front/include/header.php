<!DOCTYPE html>
<html lang="en">
   <head>
      <title><?php echo $title?$title:'Home';?></title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <base href="<?php echo base_url();?>">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="assets/front/css/bootstrap.min.css">
      <link href="https://fonts.googleapis.com/css?family=Rubik:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
      <link rel="stylesheet" href="assets/front/css/styles.css">
      <link rel="stylesheet" href="assets/front/css/responsive.css">
      <script src="assets/front/js/jquery.min.js"></script> 
      <script src="assets/front/js/popper.min.js"></script>
      <script src="assets/front/js/bootstrap.min.js"></script>
      <script src="assets/js/parsley-min.js"></script>
      <link href="assets/front/css/front_developer.css" rel="stylesheet" />
      <link href="assets/admin/css/bootstrap-fileupload.css" rel="stylesheet" />
      <script type="text/javascript" src="assets/admin/js/bootstrap-fileupload.js"></script>
      <script src="assets/admin/plugins/datepicker/bootstrap-datepicker.js"></script>
      <link href="assets/admin/css/bootstrap-datepicker.css" rel="stylesheet" />
      <script src="assets/admin/plugins/timepicker/bootstrap-timepicker.min.js"></script>
      <link href="assets/admin/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet" />  
      <script src="assets/front/js/sweetalert.min.js"></script>
      <link rel="stylesheet" href="assets/front/bootstrap-slider/slider.css">
      <script src="assets/front/bootstrap-slider/bootstrap-slider.js"></script>
      <script type="text/javascript">
            $(document).ready(function(){
                var timezone = Intl.DateTimeFormat().resolvedOptions().timeZone; //'Asia/Kolhata' for Indian Time.
                $.post("admin/Ajax/getFrontTimeZone", {timezone: timezone}, function(data) {
                });
            });
        </script>
   </head>
   <body id="body" class="border-top">
   <div id="loader-wrapper" style="display:none;">
   <div id="loader"></div>
    </div>
    <?php $doc_status=check_user_document_status();
          $nav_url_segment = $this->uri->segment(1);?>
    <?php if($this->session->userdata('user_id')) { ?>
    
      <div id="myNav" class="overlay">
         <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
         <div class="overlay-content">
            <ul class="navbar-nav mx-auto">
                <?php if($this->session->userdata('front_user_type')==2) { ?>
                <li class="nav-item">
                    <?php if($nav_url_segment=='manage-service') { ?>
                        <a class="nav-link active" href="manage-service">My Store</a>
                    <?php }else{ ?>
                         <a class="nav-link" href="manage-service">My Store</a>
                    <?php } ?>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="my-bookings">My Booking</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="sp-order-history">Order History</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="manage-deals">Deals</a>
                </li>
                <?php }else{ ?>
                        <li class="nav-item">
                        <?php if($nav_url_segment=='home') { ?>
                          <a class="nav-link active" href="home">Home</a>
                        <?php }else{ ?>
                            <a class="nav-link" href="home">Home</a>
                        <?php } ?>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="service-providers">Explore</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="my-events">My Events</a>
                        </li>
                        <?php if($this->session->userdata('front_user_type')==3) { 
                                if($doc_status=='Accepted'){ ?>
                            <li class="nav-item">
                               <a class="nav-link" href="manage-service">My Store</a>
                            </li>
                        <?php } }?>
                        <li class="nav-item">
                          <a class="nav-link" href="manage-deals">Deals</a>
                        </li>
                <?php } ?>
            </ul>
         </div>
      </div>
    <?php }?>
      <div class="bg-white">
         <div class="container menu-container">
            <nav class="navbar navbar-expand-md navbar-light nav-container px-4 " id="main-navigation">
               <a class="navbar-brand" href="<?php echo base_url();?>"><img src="assets/front/images/Group8.png" alt="logo">
               </a>
           
                <?php if($this->session->userdata('user_id')) { ?>
                      <span class="show-toggle" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>
                    <div class="collapse navbar-collapse" id="navb2">
                    <?php if($this->session->userdata('front_user_type')==2) {
                        if($doc_status=='Accepted'){
                     ?>
                        <ul class="navbar-nav main-navigation nav">
                            <li class="nav-item">
                                <?php if($nav_url_segment=='manage-service') { ?>
                                    <a class="nav-link active" href="manage-service">My Store</a>
                                <?php } else{ ?>
                                    <a class="nav-link" href="manage-service">My Store</a>
                                <?php } ?>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="my-bookings">My Booking</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="sp-order-history">Order History</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="manage-deals">Deals</a>
                            </li>
                        </ul>
                    <?php } } else{ ?>
                        <ul class="navbar-nav main-navigation">
                            <li class="nav-item">
                            <?php if($nav_url_segment=='home') { ?>
                              <a class="nav-link active" href="home">Home</a>
                            <?php }else{ ?>
                                <a class="nav-link" href="home">Home</a>
                            <?php } ?>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="service-providers">Explore</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="my-events">My Events</a>
                            </li>
                            <?php if($this->session->userdata('front_user_type')==3) { 
                                    if($doc_status=='Accepted'){ ?>
                                <li class="nav-item">
                                   <a class="nav-link" href="manage-service">My Store</a>
                                </li>
                            <?php } }?>
                            <li class="nav-item">
                              <a class="nav-link" href="manage-deals">Deals</a>
                            </li>
                        </ul>
                    <?php } ?>
                    </div>
                <div class="collapse navbar-collapse justify-content-end for-desktop" id="navb3">
                  <ul class="navbar-nav ml-3 fa-ul">
                    <?php if($this->session->userdata('front_user_type')!=2) { ?>
                     <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown"><img src="assets/front/images/friends.svg" alt="logo"><?php if (getTotalCount('friends', 'id', array('receiver_id'=>$this->session->userdata('user_id'),'status'=>'Pending')) > 0) { ?>
                          <span class="label"><?php echo getTotalCount('friends', 'id', array('receiver_id'=>$this->session->userdata('user_id'),'status'=>'Pending')); ?></span>

                         <?php } ?> </a>
                        <div class="notification-menu request-menu">
                           <ul class="dropdown-menu">
                              <li class="dropdown-item only-head">Friend Request</li>
                              <?php 
                                  $request_list = getFriendRequest($this->session->userdata('user_id'));
                                  if (isset($request_list) && !empty($request_list)) {
                                    $i=0;
                                      foreach ($request_list as $request) {
                                        $i++;
                              ?>
                              <li class="dropdown-item">
                                 <!-- <a href="javascript:void(0)"> -->
                                    <div class="single-request-block">
                                       <div class="request-profile-image">
                                       <a href="profile/<?php echo !empty($request['sent_by'])?base64_encode($request['sent_by']):'N/A';?>">
                                          <img src="<?php  
                                                      if($request['profile_picture']) { 
                                                          echo base_url().$request['profile_picture'] ; 
                                                      }else
                                                      { 
                                                          if($request['gender'] == 'Male'){ echo base_url().'assets/front/images/profile0.png' ; } 
                                                          if($request['gender'] == 'Female') { echo base_url().'assets/front/images/profile00.jpg' ; } 
                                                          if($request['gender'] == '') { echo base_url().'assets/front/images/profile000.jpeg' ;}
                                                      } ?>"
                                          >
                                          </a>
                                       </div>
                                       <div class="request-profile-option">
                                          <h4><?php echo $request['fullname']; ?><span id="sp<?php echo $i ?>"> wants to be your friend.</span></h4>
                                          <button class="btn btn-accept" id="acc<?php echo $i; ?>" onclick="accept_reject_header('<?php echo base64_encode($request['sent_by']); ?>', '<?php echo base64_encode($request['request_id']); ?>','Accepted', '<?php echo $i; ?>')">Accept</button>
                                          <button class="btn btn-reject" id="rej<?php echo $i; ?>" onclick="accept_reject_header('<?php echo base64_encode($request['sent_by']); ?>', '<?php echo base64_encode($request['request_id']); ?>','Rejected', '<?php echo $i; ?>')">Reject</button>
                                       </div>
                                    </div>
                                 <!-- </a> -->
                              </li>
                              <?php            
                                      }
                                  }
                              ?>
                              <?php if (getTotalCount('friends', 'id', array('receiver_id'=>$this->session->userdata('user_id'),'status'=>'Pending')) > 5) { ?>
                              <li class="dropdown-item text-center"><a href="friends">View all</a></li>
                              <?php 
                              }elseif(empty($request_list)){ ?>
                                <li class="dropdown-item text-center"><a href="javascript:void(0)">No request yet!</a></li>
                              <?php  
                              }
                              ?>
                           </ul>
                        </div>
                     </li>
                     <?php } ?>
                     <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown" onclick="get_notification()"><img src="assets/front/images/renewable.svg" alt="logo"><?php if(countNotification()){ echo '<span class="label">'.countNotification().'</span>';}?></a>
                        <div class="notification-menu request-menu onlynotification">
                           <ul class="dropdown-menu head-noti">
                              <li class="dropdown-item only-head">Notification</li>
                              <div id="notification_box" class="notification_box"></div>
                           </ul>
                        </div>
                     </li>
                     <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle pt-1" href="javascript:void(0)" data-toggle="dropdown" aria-expanded="true">

                           <div class="user-image"><img src="<?php echo getProfilePicture(); ?>" alt="logo"></div>
                        </a>
                        <div class="profile-menu">
                           <ul class="dropdown-menu">
                              <li class="dropdown-item"><a href="<?php if($this->session->userdata('front_user_type')!=2) { echo 'profile' ;}else { echo 'edit-profile';}?>"><img src="assets/front/images/form_icons/ic_full_name.png"><span class="ml-1">Profile</span></a></li>
                              <?php if($this->session->userdata('front_user_type')!=2) { ?>

                              <li class="dropdown-item"><a href="friends-list"><img src="assets/front/images/form_icons/menu_my_friends.png"><span class="ml-1">Friends</span></a></li>
                              <li class="dropdown-item"><a href="be-my-date"><img src="assets/front/images/form_icons/menu_be_my_date.png"><span class="ml-1">Be My Date</span></a></li>

                              <li class="dropdown-item"><a href="saved-post"><img src="assets/front/images/form_icons/menu_save_post.png"><span class="ml-1">Saved Post</span></a></li>
                              <li class="dropdown-item"><a href="<?php if($this->session->userdata('front_user_type')==1) { echo 'order-placed-history';}else{echo 'order-received-history';}?>"><img src="assets/front/images/form_icons/menu_payment.png" /><span class="ml-1">Order History</span></a></li>
                              <?php if($this->session->userdata('front_user_type')==1) { ?>
                                <li class="dropdown-item"><a href="upgrade-profile"><img src="assets/front/images/form_icons/menu_upgrade.png"><span class="ml-1">Upgrade as celebrant SP</span></a></li>
                              <?php } ?>
                              <?php } ?>
                              <?php if($this->session->userdata('front_user_type')==2) { ?>
                                <li class="dropdown-item"><a href="javascript:void(0)" onclick="upgrade_sp()"><img src="assets/front/images/form_icons/menu_upgrade.png"><span class="ml-1">Upgrade as celebrant SP</span></a></li>
                              <?php } ?>
                              <li class="dropdown-item"><a href="logout"><img src="assets/front/images/form_icons/menu_logout.png"><span class="ml-1">Logout </span></a></li>
                           </ul>
                        </div>
                     </li>
                  </ul>
               </div>
               <?php } ?>
               <?php if(!$this->session->userdata('user_id')) { ?>
               <div class="collapse navbar-collapse justify-content-end for-desktop" id="navb13">
                  <ul class="navbar-nav ml-3 fa-ul">
                    <?php $pg_chk = $this->uri->segment(1);
                        if($pg_chk=='login'){ ?>
                        <li class="nav-item">
                            <a class="nav-link" href="<?php echo base_url().'sign-up';?>">Sign Up <img src="assets/front/images/form_icons/login_signup.png" style="width: 20px;"></a>
                         </li>
                    <?php }else{ ?>
                            <li class="nav-item">
                                <a class="nav-link" href="<?php echo base_url().'login';?>">Sign In <img src="assets/front/images/form_icons/login_signup.png" style="width: 20px;"></a>
                             </li>
                       <?php } ?>                     
                  </ul>
               </div>
               <?php } ?>
            </nav>
            <?php if($this->session->userdata('user_id')) {  ?> 
            <div class="navbar navbar-collapse for-mobile" id="navb3">
               <ul class="list-inline">
                <?php if($this->session->userdata('front_user_type')!=2) { ?>

                  <li class="list-inline-item dropdown">
                     <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown"><img src="assets/front/images/friends.svg" alt="logo"><?php if (getTotalCount('friends', 'id', array('receiver_id'=>$this->session->userdata('user_id'),'status'=>'Pending')) > 0) { ?>
                          <span class="label"><?php echo getTotalCount('friends', 'id', array('receiver_id'=>$this->session->userdata('user_id'),'status'=>'Pending')); ?></span>

                         <?php } ?></a>
                     <div class="notification-menu request-menu">
                        <ul class="dropdown-menu">
                            <li class="dropdown-item only-head">Friend Request</li>
                              <?php 
                                  $request_list = getFriendRequest($this->session->userdata('user_id'));
                                  if (isset($request_list) && !empty($request_list)) {
                                    $i=0;
                                      foreach ($request_list as $request) {
                                        $i++;
                              ?>
                              <li class="dropdown-item">
                                 <!-- <a href="javascript:void(0)"> -->
                                    <div class="single-request-block">
                                       <div class="request-profile-image">
                                       <a href="profile/<?php echo !empty($request['sent_by'])?base64_encode($request['sent_by']):'N/A';?>">
                                          <img src="<?php  
                                                      if($request['profile_picture']) { 
                                                          echo base_url().$request['profile_picture'] ; 
                                                      }else
                                                      { 
                                                          if($request['gender'] == 'Male'){ echo base_url().'assets/front/images/profile0.png' ; } 
                                                          if($request['gender'] == 'Female') { echo base_url().'assets/front/images/profile00.jpg' ; } 
                                                          if($request['gender'] == '') { echo base_url().'assets/front/images/profile000.jpeg' ;}
                                                      } ?>"
                                          >
                                          </a>
                                       </div>
                                       <div class="request-profile-option">
                                          <h4><?php echo $request['fullname']; ?><span id="sp<?php echo $i ?>"> wants to be your friend.</span></h4>
                                          <button class="btn btn-accept" id="acc<?php echo $i; ?>" onclick="accept_reject_header('<?php echo base64_encode($request['sent_by']); ?>', '<?php echo base64_encode($request['request_id']); ?>','Accepted', '<?php echo $i; ?>')">Accept</button>
                                          <button class="btn btn-reject" id="rej<?php echo $i; ?>" onclick="accept_reject_header('<?php echo base64_encode($request['sent_by']); ?>', '<?php echo base64_encode($request['request_id']); ?>','Rejected', '<?php echo $i; ?>')">Reject</button>
                                       </div>
                                    </div>
                                 <!-- </a> -->
                              </li>
                              <?php            
                                      }
                                  }
                              ?>
                              <?php if (getTotalCount('friends', 'id', array('receiver_id'=>$this->session->userdata('user_id'),'status'=>'Pending')) > 5) { ?>
                              <li class="dropdown-item text-center"><a href="friends">View all</a></li>
                              <?php 
                              }elseif(empty($request_list)){ ?>
                                <li class="dropdown-item text-center"><a href="javascript:void(0)">No request yet!</a></li>
                              <?php  
                              }
                              ?>
                        </ul>
                     </div>
                  </li>
                  <?php } ?>

                  <li class="list-inline-item dropdown">
                     <a class="nav-link dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown" onclick="get_notification()"><img src="assets/front/images/renewable.svg" alt="logo"><?php if(countNotification()){ echo '<span class="label">'.countNotification().'</span>';}?></a>
                     <div class="notification-menu request-menu onlynotification">
                       <ul class="dropdown-menu head-noti">
                          <li class="dropdown-item only-head">Notification</li>
                          <div id="notification_box" class="notification_box"></div>
                       </ul>
                    </div>
                  </li>
                  <li class="list-inline-item dropdown">
                     <a class="nav-link dropdown-toggle pt-1" href="javascript:void(0)" data-toggle="dropdown">
                        <div class="user-image"><img src="<?php echo getProfilePicture(); ?>" alt="logo"></div>
                     </a>
                     <div class="profile-menu">
                        <ul class="dropdown-menu">
                           <li class="dropdown-item"><a href="<?php if($this->session->userdata('front_user_type')!=2) { echo 'profile' ;}else { echo 'edit-profile';}?>"><img src="assets/front/images/form_icons/ic_full_name.png"><span class="ml-1">Profile</span></a></li>
                              <?php if($this->session->userdata('front_user_type')!=2) { ?>

                              <li class="dropdown-item"><a href="friends-list"><img src="assets/front/images/form_icons/menu_my_friends.png"><span class="ml-1">Friends</span></a></li>
                              <li class="dropdown-item"><a href="be-my-date"><img src="assets/front/images/form_icons/menu_be_my_date.png"><span class="ml-1">Be My Date</span></a></li>

                              <li class="dropdown-item"><a href="saved-post"><img src="assets/front/images/form_icons/menu_save_post.png"><span class="ml-1">Saved Post</span></a></li>
                              <li class="dropdown-item"><a href="<?php if($this->session->userdata('front_user_type')==1) { echo 'order-placed-history';}else{echo 'order-received-history';}?>"><img src="assets/front/images/form_icons/menu_payment.png" /><span class="ml-1">Order History</span></a></li>
                              <?php if($this->session->userdata('front_user_type')==1) { ?>
                                <li class="dropdown-item"><a href="upgrade-profile"><img src="assets/front/images/form_icons/menu_upgrade.png"><span class="ml-1">Upgrade as celebrant SP</span></a></li>
                              <?php } ?>
                              <?php } ?>
                              <?php if($this->session->userdata('front_user_type')==2) { ?>
                                <li class="dropdown-item"><a href="javascript:void(0)" onclick="upgrade_sp()"><img src="assets/front/images/form_icons/menu_upgrade.png"><span class="ml-1">Upgrade as celebrant SP</span></a></li>
                              <?php } ?>
                              <li class="dropdown-item"><a href="logout"><img src="assets/front/images/form_icons/menu_logout.png"><span class="ml-1">Logout</span></a></li>
                        </ul>
                     </div>
                  </li>
               </ul>
            </div>
            <?php } ?>
         </div>
      </div>
      <script>
         /* responsive navigation */
         function openNav() {
           document.getElementById("myNav").style.width = "100%";
         }
         
         function closeNav() {
           document.getElementById("myNav").style.width = "0%";
         }
         
         
         
         /* loader jquery */
         $(document).ready(function() {
         
           // Fakes the loading setting a timeout
           setTimeout(function() {
             $('body').addClass('loaded');
           }, 1000);
         
         });
         
         /* search on enter */
         $(function() {
         
           $('input[name="Submit"]').click(function() {
             alert('You clicked me...!');
           });
         
           //press enter on text area..
           $('#searchInput').keypress(function(e) {
             var key = e.which;
             if (key == 13) // the enter key code
             {
               $('input[name = Submit]').click();
               return false;
             }
           });
         
         });
         
         /********sticky header ******/
           $(window).scroll(function() {
           if ($(this).scrollTop() > 1){  
               $('.bg-white').addClass("sticky");
             }
             else{
               $('.bg-white').removeClass("sticky");
             }
           });
           $(".nav li a").on("click", function() { 
              $(".nav li a").removeClass("active");
              $(this).addClass("active");
            });
      </script>
      <script>
    $(function(){
        $('a').each(function(){ 
            if ($(this).prop('href') == window.location.href) {
                $(this).addClass('active'); $(this).parents('li').addClass('active');
            }
        });
    });
</script>
   </body>
</html>