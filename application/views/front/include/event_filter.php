<div class="service-provider filters">
	<h4 class="heading">FILTERS</h4>
	<!-- <form  action ="my-bookings" method="POST" id="event_filter"> -->
	<div class="card searchFilter mt-3">
		<h4 class="filter-heading">Search Within</h4>
		<div class="form-inline my-2 my-lg-0 posrel" >
			<input class="form-control" type="search" placeholder="Search" id="name" name="name" value="<?php if(isset($_POST['name']) && !empty($_POST['name'])){ echo $_POST['name'];}?>">
			 <button type="button" onclick="search_product();"> <i class="fa fa-search search-icon" aria-hidden="true"></i></button>
		</div>
	</div>
	<div class="card selectFilter mt-4">
		<h4 class="filter-heading">Filter By</h4>
		<form class="form-inline my-2 my-lg-0 posrel style-2">
			<div class="form-group">
				<input type="checkbox" id="one" name="event_status" value="Not started" onclick="search_product();" <?php
                      if(isset($_POST['event_status']) && !empty($_POST['event_status'])){
                              if(in_array('Not started', $_POST['event_status'])){echo 'checked';}else{echo '';}
                    }?>>
				<label for="one">Not Started</label>
			</div>
			<div class="form-group">
				<input type="checkbox" id="two" name="event_status" value="On Going" onclick="search_product();" <?php
                      if(isset($_POST['event_status']) && !empty($_POST['event_status'])){if(in_array('On Going', $_POST['event_status'])){echo 'checked';}else{echo '';}
                    }?>>
				<label for="two">On Going</label>
			</div>
			<div class="form-group">
				<input type="checkbox" id="three" name="event_status" value="Completed" onclick="search_product();" <?php
                      if(isset($_POST['event_status']) && !empty($_POST['event_status'])){ if(in_array('Completed', $_POST['event_status'])){echo 'checked';}else{echo '';}
                    }?>>
				<label for="three">Completed</label>
			</div>
			<div class="form-group">
				<input type="checkbox" id="four" name="event_status" value="Canceled" onclick="search_product();" <?php
                      if(isset($_POST['event_status']) && !empty($_POST['event_status'])){ if(in_array('Canceled', $_POST['event_status'])){echo 'checked';}else{echo '';}
                    }?>>
				<label for="four">Canceled</label>
			</div>
    </div>
   <!--  </form> -->
</div>
<script>
	$('#name').keydown(function (e){
        if(e.keyCode == 13){
           search_product();
        }
    });
</script>