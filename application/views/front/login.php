
<section class="signup-section">
<div class="slider-container">
 <div class="login-image">
    <div class="hero-text">
        <h1 style="font-size:50px"><?php echo $page_title;?></h1> 
    </div>  
 </div>
</div>
<div class="main-login-box">
      <div class="slider-4 login-left-banner">
        <div class="overlay-color">
        <div class="login-left-banner-logo">
          <img src="assets/front/images/Group9.png" alt="logo">
          <h5 class="text-center">Celebrate your life </h5>
        </div>

        <div class="text-box">
          <h4>Simplify your search</h4><h5>Save your favorite Services and share them with your colleagues.</h5>
        </div>
        <div class="partition-line"></div>
        <div class="text-below-line">
          <p>Don't have an account?  <a href="sign-up"> Sign Up Here! </a></p>
        </div>
      </div>
      </div>
      <div class="slider-8 form-container login-part">
        <div class="form-heading">
            <h5>LOG IN WITH Mobile or Login ID</h5>
            <p class="alert_message" id="msg" style='display:none;'></p>
            <?php if ($this->session->flashdata('error')) { ?>
              <div class="alert alert-danger">
                  <button data-dismiss="alert" class="close" type="button">×</button>
                  <?php echo $this->session->flashdata('error') ?>
              </div>
            <?php } ?>
            <?php if ($this->session->flashdata('success')) { ?>
              <div class="alert alert-success">
                  <button data-dismiss="alert" class="close" type="button">×</button>
                  <?php echo $this->session->flashdata('success') ?>
              </div>
            <?php } ?>
        </div>
       <form class="login-form" id="login_form" action="" role="form" data-parsley-validate>
                <div class="form-group">
                  <img src="assets/front/images/form_icons/ic_mobile.png" class="form-input-icon" alt="mob-img" />
                  <input type="text" name ="mobile" class="form-control" id="mobile" placeholder="+1 123 456 7890" data-parsley-required data-parsley-required-message="Please enter mobile." data-parsley-errors-container="#mobile_error" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"  oninput="check_inputs('mobile')"/>
                  <div id="mobile_error"></div>
                </div>
                <div class="d-flex justify-content-center">
                  <div class="partition-line">
                    <div class="line-lext">OR</div>
                  </div>
                </div>
                <div class="form-group">
                  <img src="assets/front/images/form_icons/ic_staging.png" class="form-input-icon" alt="mob-img" />
                  <input type="text" name ="staging_id" class="form-control" id="sid" placeholder="Login ID" data-parsley-required data-parsley-required-message="Please enter login ID." data-parsley-errors-container="#sid_error" oninput="check_inputs('staging_id')"/>
                  <div id="sid_error"></div>
                </div>
                <div class="form-group">
                <img src="assets/front/images/form_icons/ic_password.png" class="form-input-icon" alt="mob-img">
                   <input type="password" name="password" id="login_password" class="form-control" placeholder="Password" data-parsley-required data-parsley-required-message="Please enter password." data-parsley-errors-container="#password_error"  oninput="check_inputs('password')"/>
                   <div id="password_error"></div>
                </div>
                <div class="d-flex justify-content-end mt-4">
                  <a href="forgot-password" style="color: #3354A5;">Forgot Password ?</a>
                </div>
                <div class="d-flex justify-content-center mt-4">
                  <button type="button" id="login_submit" class="btn btn-primary px-4">Login</button>
                </div>
              </form>
      </div>
</div>
</section>
<script>
    function check_inputs(input_type){
        
        if(input_type=='mobile'){
            $("#sid").val('');
            $("#login_password").val('');
            $("#sid").attr('data-parsley-required',false);
            $("#login_password").attr('data-parsley-required',false);
            $("#mobile").attr('data-parsley-required',true);
            $("#sid_error li").text('');
            $("#password_error li").text('');
            
        }else{
            $("#mobile").val('');
            $("#mobile").attr('data-parsley-required',false);
            $("#sid").attr('data-parsley-required',true);
            $("#login_password").attr('data-parsley-required',true);
            $("#mobile_error li").text('');
        }
    }
    $("#login_submit").on('click', function() {
        var sid=$("#sid").val();
        var mobile=$("#mobile").val();
        var login_password=$("#login_password").val();
        if(mobile=='' && sid=='' && login_password ==''){
            $("#mobile").attr('data-parsley-required',true);
            $("#sid").attr('data-parsley-required',true);
            $("#login_password").attr('data-parsley-required',true);
        }else{
             if(mobile!=''){
                $("#mobile").attr('data-parsley-required',true);
                $("#sid").attr('data-parsley-required',false);
                $("#login_password").attr('data-parsley-required',false);
            }else{
                $("#sid").attr('data-parsley-required',true);
                $("#login_password").attr('data-parsley-required',true);
                $("#mobile").attr('data-parsley-required',false);
            }
        }
       
        $('#login_form').parsley().validate();
        if ($('#login_form').parsley().isValid()) {
            $("#loader-wrapper").show();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>login-ajax",
                data: {staging_id:sid,password:login_password,mobile:mobile},

                success: function(response) { 
                    $("#loader-wrapper").hide();
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    var res = JSON.parse(response);
                    if(res.status==1) {
                        document.getElementById("login_form").reset(); 
                        $('#msg').html('<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+res.msg+'</div>');
                        $('#msg').css('display','block');
                        setTimeout(function() {
                            $('#msg').fadeOut('slow');
                            var user_type=res.details;
                            var login_by=res.login_by;
                            if(login_by=='mobile'){
                                $('#otp_popup').modal('show');
                                $("#otp_mobile").val(mobile);
                                $("#otp_type").val('login');
                                return false;
                            }
                            switch(user_type) {
                                case '1':
                                    location.href="<?php echo base_url();?>home";
                                    break;
                                case '2':
                                    location.href="<?php echo base_url();?>manage-service";
                                    break;
                                case '3':
                                    location.href="<?php echo base_url();?>home";
                                    break;
                              default:
                              break;
                            }    
                        }, 2000);
                        
                    } else {
                        $('#msg').html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+res.msg+'</div>');
                        $('#msg').css('display','block');
                        setTimeout(function() {
                            $('#msg').fadeOut('slow');
                        }, 5000); 
                    }
                    
                },
            });
        } else {
            return false;
        }
    });
    $('#mobile').keydown(function (e){
        if(e.keyCode == 13){
            $("#login_submit").click();
             return false; 
        }
    });
    $('#sid').keydown(function (e){
        if(e.keyCode == 13){
            $("#login_submit").click();
             return false; 
        }
    });
    $('#login_password').keydown(function (e){
        if(e.keyCode == 13){
            $("#login_submit").click();
             return false; 
        }
    });
    
</script>
