<div class="data_reload">
<section class="section-div pt-4 add-service-section">
   <div class="container">
      <div class="card add-service-card">
        <form class="add-service-form" action="" id="add_service" enctype="multipart/form-data" data-parsley-validate>
         <div class="row">
            <div class="col-md-8 service-details-div">
               <div class="message_box" style="display:none;"></div>
               <div class="card-banner service-slider-container">
                  <h2 class="py-4 main-heading">Edit Service Details</h2>
               </div>
               <div class="accordion book-service">
                     <div class="row">
                        <div class="form-group col-md-6">
                           <input type="text" class="form-control" placeholder="Service Title" name="service_title"  maxlength="150" data-parsley-required data-parsley-required-message="Please enter service title." value="<?php if(!empty($service['service_title'])) echo $service['service_title'];?>"/>
                        </div>
                        <div class="form-group col-md-6">
                           <select class="form-control" name="category_id" data-parsley-required data-parsley-required-message="Please select service category.">
                              <option value="">Service Category </option>
                                <?php if(!empty($category_list)){
                                    foreach($category_list as $row){ ?>
                                        <option value="<?php if(!empty($row['id']))echo $row['id'];?>" <?php if(!empty($service['category_id'])){ if($service['category_id']==$row['id']){echo 'selected';}else{echo '';}}?> ><?php if(!empty($row['title']))echo $row['title'];?></option>
                                <?php } }?>
                           </select>
                        </div>
                     </div>
                     <div class="row">
                        <div class="form-group col-md-12">
                           <textarea class="form-control" rows="4" cols="50" placeholder="Description…." name="description" maxlength="250" data-parsley-required data-parsley-required-message="Please enter description."><?php if(!empty($service['description'])) echo $service['description'];?></textarea>
                        </div>
                        <div class="form-group col-md-12">
                           <h4 class="main-heading my-3">Add Items <i class="fa fa-plus-circle add-btn" aria-hidden="true" data-toggle="collapse" data-target="#demo"></i></h4>
                           <input type="text" name="service_item" id="service_item" value="" data-parsley-required data-parsley-required-message="Please select items." data-parsley-errors-container="#service_item_err" style="visibility: hidden;" >
                           <div id="service_item_err"></div>
                        </div>
                     </div>
                     <div class="add-item-form collapse show" id="demo">
                        <div class="row">
                           <div class="col-md-6 add-item-form-col">
                              <div class="form-group">
                                 <input type="text" class="form-control" placeholder="Item Name"  id ="item_name" maxlength="100" />
                                 <p class="error" id="item_name_err"></p>
                              </div>
                           </div>
                           <div class="col-md-3 add-item-form-col">
                              <div class="form-group">
                                <input type="text" class="form-control" id ="price" placeholder="Enter Price" maxlength="10" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"/>
                                <p class="error" id="price_err"></p>
                              </div>
                           </div>
                           <div class="col-md-3 add-item-form-col">
                              <div class="form-group">
                                <input type="text" class="form-control" id ="qty"  placeholder="Enter Quantity" maxlength="10" oninput="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" />
                                <p class="error" id="qty_err"></p>
                              </div>
                           </div>
                           <div class="col-md-3 add-item-form-col">
                              <div class="form-group">
                                 <select class="form-control" id ="unit">
                                    <option value="">Select Unit</option>
                                    <?php if(!empty($category_list)){
                                        foreach($measurement_unit as $row){ ?>
                                        <option value="<?php if(!empty($row['id']))echo $row['id'];?>"><?php if(!empty($row['title']))echo $row['title'];?></option>
                                    <?php } }?>
                                </select>
                                <p class="error" id="unit_err"></p>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="form-group text-right">
                                 <button type ="button" class="btn btn-primary add-item" onclick="return add_items();"> ADD </button>
                              </div>
                           </div>
                        </div>
                        <div class="table-container style-2">
                           <div class="table-responsive">
                              <table class="table table-item-list add-service-table">
                                 <tbody>
                                    <?php if(!empty($items)){ 
                                        foreach($items as $row){ ?>
                                        <input type="hidden" name="old_form_id[]" value="<?php if(!empty($row['id'])) echo base64_encode($row['id']);?>">
                                        <tr>
                                          <td><?php if(!empty($row['item_name']))echo $row['item_name'];?></td>
                                          <td align="right">
                                            <span class="unit">$<?php if(!empty($row['price']))echo $row['price'];?>/<?php if(!empty($row['qty']))echo $row['qty'];?>  <?php if(!empty($row['unit']))echo $row['unit'];?></span><input type="hidden" name="form_id[]" value="<?php if(!empty($row['id']))echo base64_encode($row['id']);?>"><span class="delete-btn"><a href="javascript:void(0);" onClick="$(this).closest('tr').remove();"><i class="fa fa-trash text-danger"
                                                  aria-hidden="true"></i></a></span>
                                          </td>
                                        </tr>
                                    <?php } }?>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
               </div>
            </div>
            <div class="col-md-4 payment-card">
               <h6 class="py-4 main-heading">Upload Photos</h6>
                <div class="accordion book-service fileupload fileupload-new" data-provides="fileupload">
                  <div class="drop-zone">
                     <div class="area">
                        <div class="wrap-custom-file">
                           <input type="file" name="img_1" id="image1" data-id="1" class="service_profile_pic" accept="image/*" style="display: none" />
                           <div class="fileupload-new thumbnail">
                               <?php if(!empty($service['img1'])){ ?>
                                    <img alt="No Image" src="<?php echo !empty($service['img1'])? $service['img1']:'';?>">
                                <?php } ?>
                           </div>
                           <div class="fileupload-preview fileupload-exists thumbnail"></div>
                           <label for="image1">
                              <i class="fa fa-plus add-btn"></i>
                              <p class="pt-4">Drop files here to upload</p>
                           </label>
                            <div id="eeror_image_1" class="error"></div>
                            <div id="service_image_1"></div>
                        </div>
                     </div>
                  </div>
                </div>
                <div class="mt-2">
                   <div class="accordion book-service fileupload fileupload-new" data-provides="fileupload">
                      <div class="drop-zone">
                         <div class="area">
                            <div class="wrap-custom-file">
                               <input type="file" name="img_2" id="image2" data-id="2"  class="service_profile_pic" accept="image/*" style="display: none" />
                                <div class="fileupload-new thumbnail"><?php if(!empty($service['img2'])){ ?>
                                    <img alt="No Image" src="<?php echo !empty($service['img2'])? $service['img2']:'';?>">
                                <?php } ?></div>
                                <div class="fileupload-preview fileupload-exists thumbnail"></div>
                               <label for="image2">
                                  <i class="fa fa-plus add-btn"></i>
                                  <p class="pt-4">Drop files here to upload</p>
                               </label>
                                <div id="eeror_image_2" class="error"></div>
                            </div>
                         </div>
                      </div>
                   </div>
                </div>
                <div class="mt-2">
                    <div class="accordion book-service fileupload fileupload-new" data-provides="fileupload">
                      <div class="drop-zone">
                         <div class="area">
                            <div class="wrap-custom-file">
                               <input type="file" name="img_3" id="image3"  data-id="3" class="service_profile_pic" accept="image/*"  style="display: none" />
                                <div class="fileupload-new thumbnail"><?php if(!empty($service['img3'])){ ?>
                                    <img alt="No Image" src="<?php echo !empty($service['img3'])? $service['img3']:'';?>">
                                <?php } ?></div>
                                <div class="fileupload-preview fileupload-exists thumbnail" id="thumbnail_3"></div>
                               <label for="image3">
                                  <i class="fa fa-plus add-btn"></i>
                                  <p class="pt-4">Drop files here to upload</p>
                               </label>
                               <div id="eeror_image_3" class="error"></div>
                            </div>
                         </div>
                      </div>
                   </div>
                </div>
            </div>
         </div>
         <div class="d-flex justify-content-center mt-4">
                <button type="submit" class="btn btn-primary px-4" onclick="return form_submit('add_service');">Update Service</button>
         </div>
      </form>
      </div>
   </div>
</section>
</div>
<script>
    $(".service_profile_pic").change(function() {
        var attr= $(this).attr('data-id');
        var val = $(this).val();
        if(val!=''){
        switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase()){
            case 'jpg': case 'png':  case 'jpeg':
               $("#eeror_image_"+attr+"").html('');
                break;
            default:
                //$(this).val('');
                //$("#thumbnail_"+attr+"").attr('src', '');
               // $("#thumbnail_"+attr+"").attr('src')=="";
                $(this).val('');
              
                // error message here
                 $("#eeror_image_"+attr+"").html('Only jpg, jpeg, png files are allowed to upload.');
                break;
        }
        }
    });
    function add_items(){
        var is_err=1;
        var item_name=$("#item_name").val();
        var price=$("#price").val();
        var qty=$("#qty").val();
        var unit=$("#unit").val();
        if(item_name==''){
            is_err=0;
            $("#item_name_err").html('Please enter item name.');
        }else{
            is_err=1;
            $("#item_name_err").html('');
        }
        if(price==''){
            is_err=0;            
            $("#price_err").html('Please enter price.');
        }else{
            is_err=1;
            $("#price_err").html('');
        }
        if(qty==''){
            is_err=0;  
            $("#qty_err").html('Please enter quntity.');
        }else{
            is_err=1;
            $("#qty_err").html('');
        }
        if(unit==''){
            is_err=0;  
           $("#unit_err").html('Please select unit.');
        }else{
            is_err=1;
            $("#unit_err").html('');
            var unit_text=$("#unit option:selected").text();
        }
        if(is_err==1){
            var html='';
                html+='<tr>';
                    html+='<td>'+item_name+'</td>';
                        html+='<td align="right">';
                            html+='<span class="unit">$'+price+'/'+qty+' '+unit_text+'</span><input type="hidden" name="item_name[]" value="'+item_name+'"><input type="hidden" name="price[]" value='+price+'><input type="hidden" name="qty[]" value='+qty+'><input type="hidden" name="unit[]" value='+unit+'>';
                            html+='<span class="delete-btn"><a href="javascript:void(0);" onClick="$(this).closest(\'tr\').remove();"><i class="fa fa-trash text-danger" aria-hidden="true"></i></a></span>';
                        html+='</td>';
                    html+='</tr>';
            $('table tbody').append(html);
            var item_name=$("#item_name").val('');
            var price=$("#price").val('');
            var qty=$("#qty").val('');
            var unit=$("#unit").val('');
            $("#service_item_err li").text('');
        }
        return  is_err;
    }    

    function form_submit(id)
    {
        $("#eeror_image_1").text('');
        var rowCount = $('table tr').length;
        if(rowCount!=0){
             $("#service_item").val(rowCount);
        }else{
             $("#service_item").val('');
        }
        submitDetailsForm(id);
        return false;
    }
</script>

