<?php if(isset($services) && !empty($services)) { 
    foreach($services as $row){ ?>
            <div class="col-xs-12 col-md-6 col-lg-6">
                <a href="service-details/<?php if(!empty($row['service_id'])) echo base64_encode($row['service_id']);?>">
                    <div class="card">
                        <div class="card-banner">
                            <img class="card-img-top" src="<?php if(!empty($row['img1'])){ echo $row['img1'];}else{ echo 'assets/images/phl_service_banner.png';}?>" alt="Card image cap">
                        </div>
                        <div class="card-body">
                             <div class="card-title-box">
                                <h5 class="card-title"><?php if(!empty($row['service_title'])){echo mb_strimwidth($row['service_title'],0, 20,'...');}else{ echo 'N/A';}?></h5>
                            </div>
                            <div class="ctn inner-profile-title">
                                <div class="user-profile">
                                    <img src="assets/front/images/profile.png" alt="logo">
                                </div>
                                <div class="post-text">
                                     <div class="recentPostDetails">
                                        <h6 class="userName"><?php if(!empty($row['item'])){echo $row['item'];}else{ echo 'N/A';}?></h6> 
                                    </div>
                                </div>
                            </div>
                            <div class="functions-list icon-content-box">
                                <span><?php if(!empty($row['description'])){echo mb_strimwidth($row['description'],0, 50,'...');}else{ echo 'N/A';}?></span>
                                <div class="post-btn relPos">
                                    <div class="dropdown">
                                        <a href="javscript:void(0);" data-toggle="dropdown">
                                            <img src="assets/front/images/option.svg" alt="like">
                                        </a>
                                        <div class="dropmenuContainer">
                                            <div class="dropdown-menu"> 
                                                <a class="dropdown-item" href="edit-service/<?php if(!empty($row['service_id']))  echo base64_encode($row['service_id']);?>"><img src="assets/front/images/icons/edit.svg" alt="" /> Edit Service</a>
                                                <a class="dropdown-item" href="javascript:void(0)" onclick="delete_front_records('services','id','<?php if(!empty($row["service_id"]))  echo base64_encode($row["service_id"]);?>')"><img  src="assets/front/images/icons/garbage.svg" alt="" /> Delete Service</a> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
    <?php } }?>
