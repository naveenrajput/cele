<?php if(!empty($bookings)){ 
	foreach($bookings as $row) { ?>
<div class="col-xs-12 col-md-6 col-lg-6">
	<a href="my-booking-details/<?php if(!empty($row['event_id'])){ echo base64_encode($row['event_id']);}?>">
		<div class="card">
			<div class="card-banner">
				<img class="card-img-top" src="<?php echo !empty($row['image'])?$row['image']:'assets/images/phl_service_banner.png'?>" alt="Card image
                        cap">
			</div>
			<div class="card-body">
				<div class="card-title-box">
					<ul class="list-inline">
						<li class="list-inline-item">
							<span class="date"><?php if(!empty($row['event_date'])) echo convertGMTToLocalTimezone($row['event_date']);?> </span>|<span class="time"> <?php if(!empty($row['event_date'])) echo convertGMTToLocalTimezone($row['event_date'],'',true);?></span></li>
							<?php if(!empty($row['event_status'])){
									if($row['event_status']=='Not started'){
										$text_cls='text-danger';
										$bg_cls='bg-danger';
									}if($row['event_status']=='On Going'){
										$text_cls='text-warning';
										$bg_cls='bg-warning';
									}if($row['event_status']=='Completed'){
										$text_cls='text-success';
										$bg_cls='bg-success';
									}if($row['event_status']=='Canceled'){
										$text_cls='text-danger';
										$bg_cls='bg-danger';
									}
								}?>
						<li class="list-inline-item float-right <?php echo $text_cls;?>">
						<span class="dots <?php echo $bg_cls;?> "></span>
						<?php if(!empty($row['event_status'])){echo $row['event_status'];}else{ echo 'N/A';}?></li>
					</ul>
					<h5 class="card-title"><?php if(!empty($row['event_title'])){echo $row['event_title'];}else{ echo 'N/A';}?></h5>
				</div>
				<div class="ctn inner-profile-title">
					<div class="user-profile">
						<img src="<?php echo !empty($row['profile_picture'])?$row['profile_picture']:'assets/front/images/profile0.png'?>" alt="logo">
					</div>
					<div class="post-text">
						<div class="recentPostDetails">
							<h6 class="userName"><?php if(!empty($row['fullname'])){echo $row['fullname'];}else{ echo 'N/A';}?></h6> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</a>
</div>	
<?php }  } ?>
