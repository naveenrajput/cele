<div class="filter-body">
<section class="section-div pt-4 booking-section">
	<div class="container">
		<div class="row">
			<?php if(empty($filter)){?>
			<div class="col-md-4 no-background">
				<?php include APPPATH.'views/front/include/event_filter.php';  ?>
				<?php include APPPATH.'views/front/include/play_store_link.php';  ?>
			</div>
			<?php } ?>
			<div class="col-md-8">
				<div class="tab-container">
					<?php if(empty($filter)){?>
					<ul class="nav nav-tabs tab-btn" role="tablist">
						<li class="nav-item"> <a class="nav-link active" href="#MyBooking" role="tab" data-toggle="tab">My Events</a>
						</li>
					</ul>
					<?php } ?>
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="MyBooking">
							<div class="flex">
								<div class="content cards-listing-boxes">
									<div class="row" id="booking_id">	
											
									</div>
								</div>
							</div>
							<?php if(empty($filter)){?>
							<div class="text-center pt-4"><a href="javascript:void(0)" id="loadMore">
			           			<img class="spinner-icon" src="assets/front/images/home/icon/spinner-of-dots.svg" alt="">
			           			<span>Load More</span></a>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>
</div>
<script>
	/* for load more post */
	  $("#loader-wrapper").show();
            var totalrecords='<?php echo $total_records;?>';
          // alert(totalrecords);
            //alert(totalrecords);
            var track_page = 0;
             //track user scroll as page number, right now page number is 1
            load_contents(track_page);
            $(window).scroll(function() { //detect page scroll
                if($(window).scrollTop() + $(window).height() >= $(document).height()-600) { //if user scrolled to bottom of the page
                    track_page++;
                    var current_page=<?php echo FRONT_LIMIT; ?>*track_page;
                   
                    if(current_page<totalrecords){
                    load_contents(track_page);
                  }
                }
            }); 

        //Ajax load function
        function load_contents(track_page){

            $('#loader-wrapper').show(); 
            var check=$("#booking_id").html(); 
            var event_status=[];
			$.each($("input[name='event_status']:checked"), function(){            
				event_status.push($(this).val());
			}); 
			var name=$("#name").val();
            $.ajax({
                type:'POST',
                data:{ 
                	name:name,event_status:event_status,
                    page:track_page,
                },
                url: "<?php echo base_url();?>front/Booking/sp_my_booking_data", 
               success:function(data) {
               		 ajax_check_session(data,1);
                    if(data.trim().length == 0){
                      if(check.trim().length == 0){
                        $("#loadMore").text("No Content").addClass("noContent");
                        $('#loader-wrapper').hide();
                        return;
                      }
                    }
                    if(track_page==0){
                        $("#booking_id").html('');
                    }
                    $('#loader-wrapper').hide(); 
                    $("#booking_id").append(data);

                }
            });
        }
	  $(document).ready(function() {
	  	
	    $(".content").slice(0, 4).show();
	    $("#loadMore,#loadMore2").on("click", function(e) {
	      e.preventDefault();
	      $(".content:hidden").slice(0, 1).slideDown();
	      if ($(".content:hidden").length == 0) {
	        $("#loadMore").text("No Content").addClass("noContent");
	      }
	    });
	    $(".content1").slice(0, 4).show();
	    $("#loadMore2").on("click", function(e) {
	      e.preventDefault();
	      $(".content1:hidden").slice(0, 1).slideDown();
	      if ($(".content1:hidden").length == 0) {
	        $("#loadMore2").text("No Content").addClass("noContent");
	      }
	    });
	  });

		function search_product()
		{  
			$("#loader-wrapper").show();
			var check=$("#booking_id").html(); 
			//$("#booking_id").html('');
			var event_status=[];
			$.each($("input[name='event_status']:checked"), function(){            
				event_status.push($(this).val());
			}); 
			var name=$("#name").val();
			$.ajax({
	            type:'POST',
	            url: "my-booking-filter",
	            data: {name:name,event_status:event_status},
	            
	            success:function(data)
	            {
	            	ajax_check_session(data,1);
	               if(data.trim().length == 0){//alert();
                      if(check.trim().length == 0){
                        $("#loadMore").text("No Content").addClass("noContent");
                        $('#loader-wrapper').hide();
                        return;
                      }
                    }
                    if(track_page==0){
                        $("#booking_id").html('');
                       // alert();
                    }
                    $("#booking_id").html('');
                    $('#loader-wrapper').hide(); 
                    $("#booking_id").append(data);
	            }
	        });
		}

	/*$.fn.WBslider = function() {
		return this.each(function() {
			var $_this = $(this),
				$_date = $('input', $_this),
				$_title = $('.setyear', $_this),
				thumbwidth = 50, // set this to the pixel width of the thumb
				yrnow = 5.0;
	
			// set range max to current year
			$_date.attr('max', yrnow);
			$('.endyear', $_this).text( yrnow );
			$_date.val(yrnow - 10); // -10 years, just because...
	
			$_date.on('input change keyup', function() {
				var $_this = $(this),
					val = parseInt($_date.val(), 10);
	
				// if (val < 1988) {
				// 	val = '< 1987';
				// }
				// if (val === '') { // Stop IE8 displaying NaN
				// 	val = 0;
				// }
	
				$_title.text( val );
	
				var pos = (val - $_date.attr('min'))/($_date.attr('max') - $_date.attr('min'));
	
				// position the title with the thumb
				var thumbCorrect = thumbwidth * (pos - 0.5) * -1,
					titlepos = Math.round( ( pos * $_date.width() ) - thumbwidth/4 + thumbCorrect );
	
				$_title.css({'left': titlepos});
	
				// show "progress" on the track
				pos = Math.round( pos * 99 ); // to hide stuff behide the thumb
				var grad = 'linear-gradient(90deg, #3354a5 ' + pos + '%,#fff ' + (pos+1) + '%)';
				$_date.css({'background': grad});
	
			}).on('focus', function() {
				if ( isNaN( $(this).val() ) ) {
					$(this).val(0);
				}
			}).trigger('change');
			
			$(window).on('resize', function() {
				$_date.trigger('change');
			});
		});
	};
	
	$(function() {
	
		$('.slider').WBslider();
	
	});*/
</script>
