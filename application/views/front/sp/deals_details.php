
<div id="attendee_box"></div>
<section class="section-div pt-5">
	<div class="container">
		<div class="message_box" style="display:none;"></div>
		<div class="row">
			<div class="col-md-8 service-details f-width">
				<div class="card">
					<div class="card-banner service-slider-container">
						<div id="demo" class="carousel slide" data-ride="carousel">
						    <!-- Indicators -->
						    <ul class="carousel-indicators">
						        <li data-target="#demo" data-slide-to="0" class="active"></li>
						        <?php if(!empty($service['img2'])) { ?>
						        <li data-target="#demo" data-slide-to="1"></li>
						        <?php }  if(!empty($service['img3'])) { ?>
						        <li data-target="#demo" data-slide-to="2"></li>
						        <?php } ?>
						    </ul>

						    <!-- The slideshow -->
						    <div class="carousel-inner">
						        <div class="carousel-item active">
						            <img src="<?php if(!empty($booking_details['image'])){echo $booking_details['image'];} else{echo 'assets/images/phl_service_banner.png';}?>" alt="Service Image">
						        </div>
						       <?php if(!empty($service['img2'])) { ?>
						        <div class="carousel-item">
						            <img src="<?php if(!empty($service['img2'])) {echo $service['img2']; }else { echo 'assets/images/phl_service_banner.png';} ?>" alt="Service Image">
						        </div>
						        <?php } if(!empty($service['img3'])) { ?>
						        <div class="carousel-item">
						            <img src="<?php if(!empty($service['img3'])) { echo $service['img3'];} else { echo 'assets/images/phl_service_banner.png';}?>" alt="Service Image">
						        </div>
						        <?php } ?>
						    </div>
						</div>
						<div class="d-Service detail-profile-content white-text-style">
							<div class="row">
								<div class="col-sm-8">
									<h5 class="card-title"><?php if(!empty($booking_details['event_title'])){ echo ucfirst($booking_details['event_title']);} else {echo 'N/A';}?></h5>
								</div>
								<div class="col-sm-4 text-right img-msg">
									<?php if(!empty($booking_details['is_deal_done'])){
										if($booking_details['is_deal_done']=='1'){
					                        $sts_cls='completed';
					                    }
					                    ?>
									<span class="btn-sm-style <?php echo $sts_cls;?> "><?php if(!empty($booking_details['is_deal_done'])){ echo 'Sold Out';}?></span>
									<?php } ?>
								</div>
								<div class="col-md-12">
									<div class="eventVanue">
										<div class="address">
											<span><?php if(!empty($booking_details['event_address'])){ echo $booking_details['event_address'];} else {echo 'N/A';}?></span>
											<a target="_blank" href="https://www.google.com/maps/search/?api=1&query=<?php if(!empty($booking_details['event_address'])) echo $booking_details['event_address'];?>"><img class="map-icon-style" src="assets/front/images/icons/GoogleMaps_logo@2x.png"
												alt="" /></a>
										</div>
										<div class="text-right">
											<!-- <span>Order Number #<?php /*if(!empty($order_record['order_number'])){ echo $order_record['order_number'];} else {echo 'N/A';}*/ ?></span> -->
										</div>
									</div>
								</div>
							</div>

							<div class="row ">
								<div class="col-md-7 eventDetails">
									<ul class="list-inline mb-1">
										
									</ul>
								</div>
								<div class="col-md-5 text-right">
									<div class="eventDetails dateTMStyle">
										<ul class="list-inline mb-1">
											<li class="list-inline-item dateTextStyle">
												<img src="assets/front/images/icons/event.svg" alt="">
												<span><?php if(!empty($booking_details['event_date'])){ echo convertGMTToLocalTimezone($booking_details['event_date']);} else {echo 'N/A';}?></span>
											</li>
											<li class="list-inline-item timeTextStyle">
												<img src="assets/front/images/icons/clock.svg" alt="">
												<span><?php if(!empty($booking_details['event_date'])){ echo convertGMTToLocalTimezone($booking_details['event_date'],'',true);} else {echo 'N/A';}?></span>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>



						<div class="d-Service detail-profile-content ">

							<div class="row">
								<div class="col-sm-8">
									<div class="profileWDtext">
										<div class="img-circle-style">
											<img src="<?php echo !empty($booking_details['profile_picture'])?$booking_details['profile_picture']:'assets/front/images/profile0.png'?>" alt="">
										</div>
										<span><?php if(!empty($booking_details['fullname'])){ echo $booking_details['fullname'];} else {echo 'N/A';}?></span>
									</div>
								</div>
								<div class="col-sm-4 text-right img-msg">
									<?php if($user_type==1 || $user_type==3){
									if(empty($booking_details['is_deal_done'])){
									if($booking_details['sp_id']!=$user_id){ ?>
										<button class="btn continue-step" id="payment_btn" type="button" onclick="event_submit()">Payment <i class="fa fa-long-arrow-right pl-2" aria-hidden="true"></i></button>
									<?php } } } ?>
								</div>
								
							</div>
						</div>
						<div class="card-body profile-detail-style">
							<form class="add-service-form" action="">
								<div class="row">
									<div class="form-group col-md-12">
										<textarea class="form-control" placeholder="Additional Note -" rows="5"
											readonly><?php if(!empty($booking_details['note'])){ echo $booking_details['note'];} else {echo 'N/A';}?></textarea>
									</div>
									<?php if($booking_details['event_status']=='Canceled'){
										if($booking_details['sp_id']==$booking_details['canceled_by']){ ?>
										<div class="form-group col-md-12">
											<h6>Cancel Reason</h6>
											<textarea class="form-control" placeholder="Additional Note -" rows="5"
												readonly><?php if(!empty($order_record['cancel_reason'])){ echo $order_record['cancel_reason'];} else {echo 'N/A';}?></textarea>
										</div>
									<?php } } ?>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="card mt-3">
					<table class="table border-none">
						<thead>
							<tr>
								<th width="40%">Items</th>
								<th width="10%">Price/Unit</th>
								<th width="10%">Quantity</th>
								<th width="10%">Price</th>

							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan='4'>
									<hr style="margin:0; padding:0">
								</td>
							</tr>
							<?php if(!empty($items)){ 
								foreach($items as $row){ ?>
								<tr>
									<td><span><?php if(!empty($row['item_name'])){ echo $row['item_name'];} else {echo 'N/A';}?></span></td>
									<td><span class="textblue"><?php echo ADMIN_CURRENCY;?><?php if(!empty($row['price'])){ echo $row['price'];} else {echo 'N/A';}?>/<?php if(!empty($row['qty'])){ echo $row['qty'];} else {echo 'N/A';}?> <?php if(!empty($row['unit'])){ echo $row['unit'];} else {echo 'N/A';}?> </span></td>
									<td align="center"><span class="textblue"><?php if(!empty($row['customer_qty'])){ echo $row['customer_qty'];} else {echo 'N/A';}?></span></td>
									<td><span class="textblue"><?php echo ADMIN_CURRENCY;?><?php if(!empty($row['total_price'])){ echo $row['total_price'];} else {echo 'N/A';}?></span></td>

								</tr>
							<?php 	} } ?>
							
							<tr>
								<td colspan='4'>
									<hr style="margin:0;">
								</td>
							</tr>
							<?php if(!empty($order_total)){
									foreach ($order_total as $key => $value) { ?>
									<tr>
										<td><span class="textblue"><strong><?php if(!empty($value['title'])){echo $value['title'];}else{echo 'N/A';}?></strong></span></td>
										<td><span class="textblue"></span></td>
										<td align="center"><span class="textblue"></span></td>
										<td><span> <?php if($value['code']=='tax'){ ?>
											<strong><?php if(!empty($value['value'])){ echo $value['value'].' %';} else {echo 'N/A';}?></strong>
										<?php }else { ?>
												<strong><?php echo ADMIN_CURRENCY;?><?php if(!empty($value['value'])){ echo $value['value'];} else {echo 'N/A';}?></strong>
										<?php } ?>
										
										</span></td>
									</tr>
							<?php } }?>
							
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-md-4 no-background">
				<?php include APPPATH.'views/front/include/play_store_link.php';  ?>
			</div>
		</div>
	</div>
</section>
<div class="modal" id="cancelBooking">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5>Cancellation Reason</h5>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<form class="add-service-form" id="cancel_form" action="" data-parsley-validate>
				<div class="modal-body">
					<div class="row">
						<div class="form-group col-md-12">
							<textarea class="form-control" placeholder="Reason For Cancellation..." name ="cancel_reason" id="cancel_reason" data-parsley-required data-parsley-required-message="Please enter cancel reason." maxlength="250"></textarea>
						</div>
					</div>
				</div>
				<!-- Modal footer -->
				<div class="modal-footer">
					<button type="button" id ="cancel_btn" class="btn btn-primary" onclick="cancel_booking()">Submit</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
 
$('.datepicker_cancel').datepicker({
    format: 'mm/dd/yyyy',
    todayHighlight: true,
    startDate: '+0d',
    endDate: '<?php if(!empty($booking_details['event_date'])){ echo date('m/d/Y',strtotime(convertGMTToLocalTimezone($booking_details['event_date'])));} ?>',
    autoclose: true,
    //orientation: "bottom auto",


});
function event_submit(){
	var event_id ='<?php if(!empty($booking_details['event_id'])){ echo base64_encode($booking_details['event_id']);} ?>';
    $("#loader-wrapper").show();
    $("#payment_btn").attr('disabled',true);
     $.ajax({
        url: 'grab-deal',
        type: 'POST',
        data: {event_id:event_id},
        dataType: 'json',
        // async: false,
        success:function(resp){
          $("#loader-wrapper").hide();  
          $("html, body").animate({ scrollTop: 0 }, "slow");
          $(".message_box").show();
           ajax_check_session(resp); 
            if(resp.status==0){
                $("#payment_btn").attr('disabled',false);
                $(".message_box").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                setTimeout(function() {
                  $(".message_box").html('');
                  $(".message_box").hide();
                }, 5000); 
            }else{
                location.href="deal-make-payment/"+resp.msg;
            }
        },
        error:function(err){
          $("#loader-wrapper").hide();
        }
    });
    
}

</script>