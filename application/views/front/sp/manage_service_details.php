<section class="section-div  pt-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 service-details-div">
				<div class="card mrgB-20">
					<div class="card-banner service-slider-container slider-transe-overlay rounded-border-box">
						<?php require_once('service_slider.php'); ?>
						<div class="d-Service detail-profile-content whiteBGStyle">
							<div class="row">
								<div class="col-sm-12">
									<h5 class="card-title mrgT-10"><?php if(!empty($service['service_title'])){echo ucfirst($service['service_title']);}else{ echo 'N/A';}?> </h5>
									<div class="eventVanue">
										<div class="address">
											<a target="_blank" href="https://www.google.com/maps/search/?api=1&query=<?php if(!empty($service['address'])) echo $service['address'];?>"> <span><?php if(!empty($service['address'])){echo ucfirst($service['address']);}else{ echo 'N/A';}?></span>
												<img class="map-icon-style" src="assets/front/images/icons/GoogleMaps_logo@2x.png" alt="" />
											</a>
										</div>
										<?php if($service['service_status']=='Inactive'){ ?>
											<div class="pull-right">Service Status -<?php echo $service['service_status'];?></div>
										<?php } ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 detail-content">
									<p><?php if(!empty($service['description'])){echo ucfirst($service['description']);}else{ echo 'N/A';}?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-body">
						<div class="tab-content" id="singleService">
							<div class="tab-pane show active" id="tab1" role="tabpanel" aria-labelledby="tab1">
								<div class="accordion" id="accordion-tab-1">
									<div class="card">
										<div class="show" id="accordion-tab-1-content-1" >
											<div class="card-body pd-0">
                                                <div class="table-responsive">
                                                    <table class="table border-none">
                                                        <thead>
                                                            <tr>
                                                                <th width="40%">Items</th>
                                                                <th width="10%">Unit</th>
                                                                <th width="10%">Price</th>
                                                        
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php if(!empty($items)){ 
                                                            	foreach($items as $row){ ?>
                                                        		<tr>
	                                                            	<td>   
	                                                       			 	<div class="imgWDtext">
	                                                                		<strong><?php if(!empty($row['item_name']))echo $row['item_name'];?><?php if($row['item_status']=='Inactive'){echo ' '.'('.$row['item_status'].')';}?></strong>
	                                                                	</div> 
	                                                                </td>
	                                                                <td><span class="textblue"><?php if(!empty($row['qty']))echo $row['qty'];?>  <?php if(!empty($row['unit']))echo $row['unit'];?></span></td>
	                                                                <td><span class="textblue">$<?php if(!empty($row['price']))echo $row['price'];?></span></td>
	                                                            </tr>
                                                            	<?php } } ?>
                                                        </tbody>
                                                    </table>
                                                </div> 
                                            </div>
                                           
										</div>
									</div>
									
								
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 no-background">
				<?php require_once('service_details_my_booking.php'); ?>
				<?php include APPPATH.'views/front/include/play_store_link.php';  ?>
			</div>
		</div>
	</div>
</section>