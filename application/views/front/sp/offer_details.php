<section class="section-div  pt-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-8 service-details-div">
				<div class="card mrgB-20">
					<div class="card-banner service-slider-container slider-transe-overlay rounded-border-box">
						<img class="offerimg" src="<?php if(!empty($offer['img1'])){echo $offer['img1'];} else { echo 'assets/images/phl_service_banner.png';}?> ">
						<div class="d-Service detail-profile-content whiteBGStyle">
							<div class="row">
								<div class="col-sm-12">
									<h5 class="card-title mrgT-10"><?php if(!empty($offer['offer_name'])){echo ucfirst($offer['offer_name']);}else{ echo 'N/A';}?> </h5>
								
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 detail-content offerdetailspage">
									<p>Offer: <span class="btn-sm-style completed mr-2"><?php if(!empty($offer['discount'])){echo $offer['discount'].'%'.' OFF';}else{ echo 'N/A';}?></span></p>
										</div>
								
							</div>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-body">
						<div class="tab-content" id="singleService">
							<div class="tab-pane show active" id="tab1" role="tabpanel" aria-labelledby="tab1">
								<div class="accordion" id="accordion-tab-1">
									<div class="card">
										<div class="show" id="accordion-tab-1-content-1">
											<div class="card-body pd-10 offerdetl">
											<p><span> Service Title: </span> <?php if(!empty($offer['service_title'])){echo ucfirst($offer['service_title']);}else{ echo 'N/A';}?></p>
											<p> <span> Valid From: </span> <?php if(!empty($offer['start_date'])){//echo convertGMTToLocalTimezone($offer['start_date']);}else{ echo 'N/A';} 
											echo date(DATETIMEFORMATE,strtotime($offer['start_date']));}else{echo 'N/A';}?></p>
											<p> <span> Valid TIll : </span> <?php if(!empty($offer['end_date'])){//echo convertGMTToLocalTimezone($offer['end_date']);}else{ echo 'N/A';}
											echo date(DATETIMEFORMATE,strtotime($offer['end_date']));}else{echo 'N/A';}?></p>
											<p> <span> Status : </span> <?php if(!empty($offer['status'])){echo ucfirst($offer['status']);}else{ echo 'N/A';}?></p>
											<p><span> Description : </span> <?php if(!empty($offer['description'])){echo ucfirst($offer['description']);}else{ echo 'N/A';}?></p>
								
											

                                            </div>
                                           
										</div>
									</div>
									
								
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 no-background">
				<?php require_once('service_details_my_booking.php'); ?>
				<?php include APPPATH.'views/front/include/play_store_link.php';  ?>
			</div>
		</div>
	</div>
</section>