<div class ="data_reload">
<section class="section-div pt-4 add-service-section">
    <div class="container">
      <div class="card add-service-card">
        <form class="add-service-form" action="" id="edit_offer" enctype="multipart/form-data" data-parsley-validate>
        <div class="row">
            <div class="col-md-8 service-details-div">
                <div class="message_box" style="display:none;"></div>
                <div class="card-banner service-slider-container">
                    <h2 class="py-4 main-heading">Edit Offer Details</h2>
                </div>
                <div class="accordion book-service">
                    <div class="row">
                      <div class="form-group col-md-6">
                           <input type="text" class="form-control" placeholder="Offer Name" name="offer_name"  maxlength="150" data-parsley-required data-parsley-required-message="Please enter offer name." value="<?php if(!empty($offer['offer_name']))echo $offer['offer_name']; ?>"/>
                      </div>
                      <div class="form-group col-md-6">
                        <select class="form-control" name="service_id" id ="service_id" data-parsley-required data-parsley-required-message="Please select service title.">
                          <option value="">Select Service</option>
                          <?php if(!empty($services)){
                                foreach($services as $row){ ?>
                                    <option value="<?php if(!empty($row['id']))echo $row['id'];?>" <?php if(!empty($offer['service_id'])){ if($offer['service_id']==$row['id']){echo 'selected';}else{echo '';}}?> ><?php if(!empty($row['service_title']))echo $row['service_title'];?></option>
                            <?php } }?>
                        </select>
                      </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-md-12">
                        <textarea rows="4" cols="50" class="form-control" placeholder="Description…." name="description" maxlength="250" data-parsley-required data-parsley-required-message="Please enter description."><?php if(!empty($offer['description']))echo $offer['description']; ?></textarea>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="form-group col-md-6">
                        <span class="px-2">Start Date</span>
                         <?php if(strtotime($offer['start_date'])>strtotime(date('m/d/Y'))){
                                $mindate=date('m/d/Y');
                            }else{
                                $mindate=date('m/d/Y',strtotime($offer['start_date']));
                        }?>
                        <input  data-mindate="<?php echo $mindate;?>" class="form-control datepicker" placeholder ="Start Date" name ="start_date" id= "startdate"  data-parsley-required data-parsley-required-message="Please select start date." value="<?php if(!empty($offer['start_date'])){ echo date('m/d/Y',strtotime($offer['start_date']));}?>" oninput="this.value = this.value.replace(/[^]/g, '').replace(/(\..*)\./g, '$1');" data-parsley-errors-container="#st_date_err" />
                        <div id ="st_date_err"></div>
                      </div>
                      <div class="form-group col-md-6">
                        <span class="px-2">End Date</span>
                        <input class="form-control datepicker" placeholder ="End Date" name ="end_date" id= "enddate" data-parsley-required data-parsley-required-message="Please select end date." value="<?php if(!empty($offer['end_date'])){ echo date('m/d/Y',strtotime($offer['end_date']));}?>" oninput="this.value = this.value.replace(/[^]/g, '').replace(/(\..*)\./g, '$1');"  data-parsley-errors-container="#end_date_err"/>
                        <div id ="end_date_err"></div>
                      </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="percentage">
                      <div class="form-group">
                        <span class="px-2">Discount Type (%)</span>
                        <input type="text" class="form-control" placeholder="Enter Discount Value" name="discount" id="discount" data-parsley-required data-parsley-required-message="Please enter discount." maxlength="3" min="1" max ="100"  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" value="<?php if(!empty($offer['discount'])){ echo $offer['discount'];}?>" />
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 payment-card">
                <h6 class="py-4 main-heading">Upload Photos</h6>
                <div class="accordion book-service fileupload fileupload-new" data-provides="fileupload">
                  <div class="drop-zone">
                     <div class="area">
                        <div class="wrap-custom-file">
                           <input type="file" name="img_1" id="image1" data-id="1" class="offer_profile_pic" accept="image/*" style="display: none" />
                           <div class="fileupload-new thumbnail">
                               <?php if(!empty($offer['img1'])){ ?>
                                    <img alt="No Image" src="<?php echo !empty($offer['img1'])? $offer['img1']:'';?>">
                                <?php } ?>
                           </div>
                           <div class="fileupload-preview fileupload-exists thumbnail"></div>
                           <label for="image1">
                              <i class="fa fa-plus add-btn"></i>
                              <p class="pt-4">Drop files here to upload</p>
                           </label>
                            <div id="eeror_image_1" class="error"></div>
                        </div>
                     </div>
                  </div>
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-center mt-4">
            <button type="submit" class="btn btn-primary px-4" onclick="return form_submit('edit_offer');">Submit</button>
        </div>
        </form>
      </div>
    </div>
</section>
</div>
<script>
    $(document).ready(function(){
        var end_set_date=$("#startdate").val();
        $('#enddate').datepicker({orientation: "bottom auto"});
         $('#enddate').datepicker('setStartDate', end_set_date);
        $("#startdate").datepicker({
            todayHighlight: true,
            autoclose: true,
            startDate: '+0d',
            orientation: "bottom auto",
        }).on('changeDate', function (selected) {
            $("#enddate").val('');
            var minDate = new Date(selected.date.valueOf());
            $('#enddate').datepicker('setStartDate', minDate);
            $("#st_date_err  li").text('');
        });
        
        $("#enddate").datepicker({
             orientation: "bottom auto",
        }).on('changeDate', function (selected) {
            var maxDate = new Date(selected.date.valueOf());
            console.log(maxDate);
            $('#startdate').datepicker('setEndDate', maxDate);
            $("#end_date_err  li").text('');
        });

    });

    $(".offer_profile_pic").change(function() {
        var attr= $(this).attr('data-id');
        var val = $(this).val();
        if(val!=''){
        switch(val.substring(val.lastIndexOf('.') + 1).toLowerCase()){
            case 'jpg': case 'png':  case 'jpeg':
               $("#eeror_image_"+attr+"").html('');
                break;
            default:
                //$(this).val('');
                //$("#thumbnail_"+attr+"").attr('src', '');
               // $("#thumbnail_"+attr+"").attr('src')=="";
                $(this).val('');
                //$(".fa-plus").css('display','block');
                // error message here
                 $("#eeror_image_"+attr+"").html('Only jpg, jpeg, png files are allowed to upload.');
                break;
        }
        }
    });
    function form_submit(id)
    {
        $("#eeror_image_1").text('');
        editofferForm(id);
        return false;
    }
   function editofferForm(id,seturl='') {
        if(seturl)
        {
            var form_action=seturl;
        }else
        {
            var form_action= "<?php if(!empty($form_action))echo $form_action;?>";
        }
        //alert(last_element);
        $("#"+id).parsley().validate();
        var form = $('#'+id)[0];
        if($("#"+id).parsley().isValid()){
          var formData = new FormData(form);
          $("#loader-wrapper").show();
          $("#submit_form").attr('disabled',true);
          $.ajax({
            url: form_action,
            type: 'POST',
            data: formData,
            dataType: 'json',
            // async: false,
            cache: false,
            contentType: false,
            processData: false,
        
            success:function(resp){
              $("#loader-wrapper").hide();  
              $("html, body").animate({ scrollTop: 0 }, "slow");
              $(".message_box").show(); 
              $("#submit_form").attr('disabled',false);
                if(resp.status==0){
                    $(".message_box").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                    setTimeout(function() {
                        $(".message_box").html('');
                        $(".message_box").hide();
                        if(resp.msg=='You already added offer for this time duration.'){
                            $("#startdate").datepicker('remove'); //detach
                            $("#enddate").datepicker('remove'); //detach
                            $('#startdate').datepicker({orientation: "bottom auto"});
                            $('#enddate').datepicker({orientation: "bottom auto"});
                            var end_set_date='<?php if(!empty($offer['start_date'])){ echo date('m/d/Y',strtotime($offer['start_date']));}?>';
                            $('#enddate').datepicker('setStartDate', end_set_date);
                            $("#startdate").val('<?php if(!empty($offer['start_date'])){ echo date('m/d/Y',strtotime($offer['start_date']));}?>');
                            $("#enddate").val('<?php if(!empty($offer['end_date'])){ echo date('m/d/Y',strtotime($offer['end_date']));}?>');
                            $("#startdate").datepicker({
                                todayHighlight: true,
                                autoclose: true,
                                startDate: '+0d',
                                orientation: "bottom auto",
                            }).on('changeDate', function (selected) {
                                $("#enddate").val('');
                                var minDate = new Date(selected.date.valueOf());
                                $('#enddate').datepicker('setStartDate', minDate);
                                $("#st_date_err  li").text('');
                            });
                            
                            $("#enddate").datepicker({
                                 orientation: "bottom auto",
                            }).on('changeDate', function (selected) {
                                var maxDate = new Date(selected.date.valueOf());
                                $("#end_date_err  li").text('');
                            });
                        }
                    }, 2000); 
                  }else{
                    $(".message_box").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                    $("#"+id).parsley().reset();
                    $("#"+id)[0].reset();
                    setTimeout(function() { 
                        $(".message_box").html('');
                        $(".message_box").hide();
                        get_page_by_ajax(form_action);
                    }, 2000); 
                }
            },
            error:function(err){
              $("#submit_form").attr('disabled',false);
              $("#loader-wrapper").hide();
            }
          });
        }
    }

</script>

