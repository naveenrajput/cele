
<div id="attendee_box"></div>
<div class="modal" id="accept_booking">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5>Accept Booking</h5>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<form class="add-service-form" id="accept_form" action="" data-parsley-validate>
				<!-- Modal body -->
				<div class="modal-body">
					<div class="row">
						<div class="form-group col-md-12">
							<p>Cancelation charges depend on this date.</p>
							<input type="text" class="form-control datepicker_cancel" name ="sp_order_cancel_date" id="sp_order_cancel_date" placeholder="Cancel Date" value="" oninput="this.value = this.value.replace(/[^]/g, '').replace(/(\..*)\./g, '$1');" data-parsley-required data-parsley-required-message="Please select date." />
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                            </div>
						</div>
					</div>
				</div>
				<!-- Modal footer -->
				<div class="modal-footer">
					<button type="button" id="accept_btn" class="btn btn-primary" onclick="accept_booking()">Submit</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>
<section class="section-div pt-5">
	<div class="container">
		<div class="message_box" style="display:none;"></div>
		<div class="row">
			<div class="col-md-8 service-details f-width">
				<div class="card">
					<div class="card-banner service-slider-container">
						<div id="demo" class="carousel slide" data-ride="carousel">
						    <!-- Indicators -->
						    <ul class="carousel-indicators">
						        <li data-target="#demo" data-slide-to="0" class="active"></li>
						        <?php if(!empty($service['img2'])) { ?>
						        <li data-target="#demo" data-slide-to="1"></li>
						        <?php }  if(!empty($service['img3'])) { ?>
						        <li data-target="#demo" data-slide-to="2"></li>
						        <?php } ?>
						    </ul>

						    <!-- The slideshow -->
						    <div class="carousel-inner">
						        <div class="carousel-item active">
						            <img src="<?php if(!empty($booking_details['image'])){echo $booking_details['image'];} else{echo 'assets/images/phl_service_banner.png';}?>" alt="Service Image">
						        </div>
						       <?php if(!empty($service['img2'])) { ?>
						        <div class="carousel-item">
						            <img src="<?php if(!empty($service['img2'])) {echo $service['img2']; }else { echo 'assets/images/phl_service_banner.png';} ?>" alt="Service Image">
						        </div>
						        <?php } if(!empty($service['img3'])) { ?>
						        <div class="carousel-item">
						            <img src="<?php if(!empty($service['img3'])) { echo $service['img3'];} else { echo 'assets/images/phl_service_banner.png';}?>" alt="Service Image">
						        </div>
						        <?php } ?>
						    </div>
						</div>
						<div class="d-Service detail-profile-content white-text-style">
							<div class="row">
								<div class="col-sm-8">
									<h5 class="card-title"><?php if(!empty($booking_details['event_title'])){ echo ucfirst($booking_details['event_title']);} else {echo 'N/A';}?></h5>
								</div>
								<div class="col-sm-4 text-right img-msg">
									<?php if(!empty($booking_details['event_status'])){
					                    if($booking_details['event_status']=='Not started'){
					                        $sts_cls='not-started';
					                    }if($booking_details['event_status']=='On Going'){
					                        $sts_cls='on-going';
					                    }if($booking_details['event_status']=='Completed'){
					                        $sts_cls='completed';
					                    }if($booking_details['event_status']=='Canceled'){
					                        $sts_cls='not-started';
					                    }?>
									<span class="btn-sm-style <?php echo $sts_cls;?> "><?php if(!empty($booking_details['event_status'])){ echo $booking_details['event_status'];} else {echo 'N/A';} if($booking_details['is_deal_done']==1){ echo '/Deal Event';}?></span>
									<?php } ?>
								</div>
								<div class="col-md-12">
									<div class="eventVanue">
										<div class="address">
											<span><?php if(!empty($booking_details['event_address'])){ echo $booking_details['event_address'];} else {echo 'N/A';}?></span>
											<a target="_blank" href="https://www.google.com/maps/search/?api=1&query=<?php if(!empty($booking_details['event_address'])) echo $booking_details['event_address'];?>"><img class="map-icon-style" src="assets/front/images/icons/GoogleMaps_logo@2x.png"
												alt="" /></a>
										</div>
										<div class="text-right">
											<span>Order Number #<?php if(!empty($order_record['order_number'])){ echo $order_record['order_number'];} else {echo 'N/A';}?></span>
										</div>
										
									</div>
								</div>
							</div>

							<div class="row ">
								<div class="col-md-7 eventDetails">
									<ul class="list-inline mb-1">
										<li class="list-inline-item  accept-icon-style" onclick="get_event_attendee('Accepted','<?php echo base64_encode($booking_details['event_id']);?>','<?php echo base64_encode($booking_details['user_id']);?>')">
											<img src="assets/front/images/icons/ic_accept.svg" alt="">
											<span><?php if(isset($accepted)){ echo $accepted;} else {echo 'N/A';}?> Accepted</span>
										</li>
										<li class="list-inline-item pending-icon-style" onclick="get_event_attendee('Pending','<?php echo base64_encode($booking_details['event_id']);?>','<?php echo base64_encode($booking_details['user_id']);?>')">
											<img src="assets/front/images/icons/sand-clock.svg" alt="">
											<span><?php if(isset($pending)){ echo $pending;} else {echo 'N/A';}?> pending</span>
										</li>
										<li class="list-inline-item rejected-icon-style" onclick="get_event_attendee('Rejected','<?php echo base64_encode($booking_details['event_id']);?>','<?php echo base64_encode($booking_details['user_id']);?>')">
											<img src="assets/front/images/icons/dislike.svg" alt="">
											<span><?php if(isset($rejected)){ echo $rejected;} else {echo 'N/A';}?> Rejected</span>
										</li>
									</ul>
								</div>
								<div class="col-md-5 text-right">
									<div class="eventDetails dateTMStyle">
										<ul class="list-inline mb-1">
											<li class="list-inline-item dateTextStyle">
												<img src="assets/front/images/icons/event.svg" alt="">
												<span><?php if(!empty($booking_details['event_date'])){ echo convertGMTToLocalTimezone($booking_details['event_date']);} else {echo 'N/A';}?></span>
											</li>
											<li class="list-inline-item timeTextStyle">
												<img src="assets/front/images/icons/clock.svg" alt="">
												<span><?php if(!empty($booking_details['event_date'])){ echo convertGMTToLocalTimezone($booking_details['event_date'],'',true);} else {echo 'N/A';}?></span>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>



						<div class="d-Service detail-profile-content ">

							<div class="row">
								<div class="col-sm-8">
									<div class="profileWDtext">
										<div class="img-circle-style">
											<img src="<?php echo !empty($booking_details['profile_picture'])?$booking_details['profile_picture']:'assets/front/images/profile0.png'?>" alt="">
										</div>
										<span><?php if(!empty($booking_details['fullname'])){ echo $booking_details['fullname'];} else {echo 'N/A';}?></span>
									</div>
								</div>
								<div class="col-sm-4 text-right img-msg">
									<?php 
									
										$current_date=convertGMTToLocalTimezone(date('Y-m-d H:i:s'));
										$current_time= convertGMTToLocalTimezone(date('Y-m-d H:i:s'),'',true);
										$curr_d=strtotime($current_date.' '.$current_time);
										$ev_date=convertGMTToLocalTimezone($booking_details['event_date']);
										$ev_time= convertGMTToLocalTimezone($booking_details['event_date'],'',true);
										$ev=strtotime($ev_date.' '.$ev_time);
									if(isset($order_record['status']) && !empty($order_record['status'])){ //if($order_record['status']=='Pending'){ 
									
									if($order_record['status']=='Pending' && $booking_details['event_status']!='Canceled'){ 
										if($curr_d<$ev){ ?>
										
											<a class="btn-lg-style yellow-btn mrgT-6" href="javascript:void(0);"
												data-toggle="modal" data-target="#accept_booking">Accept Booking</a>
												<a class="btn-lg-style yellow-btn mrgT-6" href="javascript:void(0);"
												onclick="reject_booking()">Reject Booking</a>
												<?php }
												else{echo '<span>Event time has been passed.</span>';} 
											}if($order_record['status']=='Accepted' && ($booking_details['event_status']!='Completed' && $booking_details['event_status']!='Canceled')){ ?>
												<div class="col-sm-8 text-right img-msg">
													<a class="btn-lg-style yellow-btn mrgT-6" href="javascript:void(0);"
														data-toggle="modal" data-target="#cancelBooking">Cancel Booking</a>
												</div>
												
												<?php if(isset($order_record['sp_order_cancel_date']) && !empty($order_record['sp_order_cancel_date']) && $order_record['sp_order_cancel_date']!='0000-00-00'){ ?>
												<span>Cancelation charges applied after this date: <?php echo date(DATETIMEFORMATE,strtotime($order_record['sp_order_cancel_date']));?></span>
												<?php } ?>
											<?php } 
									} 
									if($booking_details['event_status']=='Canceled'){ 
										if($booking_details['sp_id']!=$booking_details['canceled_by']){ ?>
 											
											<span id="deal_span">
											<?php 
											//if(!empty($booking_details['event_id'])){ echo base64_encode($booking_details['event_id']);}

											if(checkDealStatus($booking_details['event_id']?base64_encode($booking_details['event_id']):"")){ 
												if($curr_d<$ev){ ?>
												<button class="btn-lg-style yellow-btn mrgT-6 add_deal"
												onclick="add_deal()">Add to Deal</button>
											<?php }  }else{ ?>
												<button class="btn-lg-style yellow-btn mrgT-6 add_deal"
												>Deal Added</button>
												<?php } ?>

											</span>
										<?php }
									}
									?>
								</div>
								
							</div>
						</div>
						<div class="card-body profile-detail-style">
							<form class="add-service-form" action="">
								<div class="row">
									<div class="form-group col-md-12">
										<textarea class="form-control" placeholder="Additional Note -" rows="5"
											readonly><?php if(!empty($booking_details['note'])){ echo $booking_details['note'];} else {echo 'N/A';}?></textarea>
									</div>
									<?php if($booking_details['event_status']=='Canceled'){
										if($booking_details['sp_id']==$booking_details['canceled_by']){ ?>
										<div class="form-group col-md-12">
											<h6>Cancel Reason</h6>
											<textarea class="form-control" placeholder="Additional Note -" rows="5"
												readonly><?php if(!empty($order_record['cancel_reason'])){ echo $order_record['cancel_reason'];} else {echo 'N/A';}?></textarea>
										</div>
									<?php } } ?>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="card mt-3">
					<table class="table border-none">
						<thead>
							<tr>
								<th width="40%">Items</th>
								<th width="10%">Price/Unit</th>
								<th width="10%">Quantity</th>
								<th width="10%">Price</th>

							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan='4'>
									<hr style="margin:0; padding:0">
								</td>
							</tr>
							<?php if(!empty($items)){ 
								foreach($items as $row){ ?>
								<tr>
									<td><span><?php if(!empty($row['item_name'])){ echo $row['item_name'];} else {echo 'N/A';}?></span></td>
									<td><span class="textblue"><?php echo ADMIN_CURRENCY;?><?php if(!empty($row['price'])){ echo $row['price'];} else {echo 'N/A';}?>/<?php if(!empty($row['qty'])){ echo $row['qty'];} else {echo 'N/A';}?> <?php if(!empty($row['unit'])){ echo $row['unit'];} else {echo 'N/A';}?> </span></td>
									<td align="center"><span class="textblue"><?php if(!empty($row['customer_qty'])){ echo $row['customer_qty'];} else {echo 'N/A';}?></span></td>
									<td><span class="textblue"><?php echo ADMIN_CURRENCY;?><?php if(!empty($row['total_price'])){ echo $row['total_price'];} else {echo 'N/A';}?></span></td>

								</tr>
							<?php 	} } ?>
							
							<tr>
								<td colspan='4'>
									<hr style="margin:0;">
								</td>
							</tr>
							<?php if(!empty($order_total)){
									foreach ($order_total as $key => $value) { ?>
									<tr>
										<td><span class="textblue"><strong><?php if(!empty($value['title'])){echo $value['title'];}else{echo 'N/A';}?></strong></span></td>
										<td><span class="textblue"></span></td>
										<td align="center"><span class="textblue"></span></td>
										<td><span> <?php if($value['code']=='tax'){ ?>
											<strong><?php if(!empty($value['value'])){ echo $value['value'].' %';} else {echo 'N/A';}?></strong>
										<?php }else { ?>
												<strong><?php echo ADMIN_CURRENCY;?><?php if(!empty($value['value'])){ echo $value['value'];} else {echo 'N/A';}?></strong>
										<?php } ?>
										
										</span></td>
									</tr>
							<?php } }?>
							
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-md-4 no-background">
				<?php include APPPATH.'views/front/include/play_store_link.php';  ?>
			</div>
		</div>
	</div>
</section>
<div class="modal" id="cancelBooking">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5>Cancellation Reason</h5>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<form class="add-service-form" id="cancel_form" action="" data-parsley-validate>
				<div class="modal-body">
					<div class="row">
						<div class="form-group col-md-12">
							<textarea class="form-control" placeholder="Reason For Cancellation..." name ="cancel_reason" id="cancel_reason" data-parsley-required data-parsley-required-message="Please enter cancel reason." maxlength="250"></textarea>
						</div>
					</div>
				</div>
				<!-- Modal footer -->
				<div class="modal-footer">
					<button type="button" id ="cancel_btn" class="btn btn-primary" onclick="cancel_booking()">Submit</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>

<script>
 
$('.datepicker_cancel').datepicker({
    format: 'mm/dd/yyyy',
    todayHighlight: true,
    startDate: '+0d',
    endDate: '<?php if(!empty($booking_details['event_date'])){ echo date('m/d/Y',strtotime(convertGMTToLocalTimezone($booking_details['event_date'])));} ?>',
    autoclose: true,
    //orientation: "bottom auto",


});
function cancel_booking(){
	$("#cancel_form").parsley().validate();
    if($("#cancel_form").parsley().isValid()){
    	$('#cancelBooking').modal('hide');
    	$("#cancel_btn").attr('disabled',true);
    	$("#loader-wrapper").show();
    	var order_id ='<?php if(!empty($order_record['order_id'])){ echo base64_encode($order_record['order_id']);} ?>'; 
    	var cancel_reason=$("#cancel_reason").val();
		if(cancel_reason==''){
			alert('Please enter cancel reason.');
			return false;
		}
    	$.ajax({
	        url: 'front/Booking/sp_canceled_booking',
	        type: 'POST',
	        data: {order_id:order_id,cancel_reason:cancel_reason},
	       	dataType: 'json',
	        success:function(resp){
			  $("#cancel_form").parsley().reset();
	          $("#cancel_form")[0].reset();
	          $("#loader-wrapper").hide();  
	          $("html, body").animate({ scrollTop: 0 }, "slow");
	          $(".message_box").show(); 
	          $("#cancel_btn").attr('disabled',false);
	            if(resp.status==0){
	                $(".message_box").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
	                setTimeout(function() {
	                 $(".message_box").html('');
	                  $(".message_box").hide();
	                }, 5000); 
	              }else{
	                $(".message_box").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
	                setTimeout(function() { 
	                 $(".message_box").html('');
	                 $(".message_box").hide();
	                 location.reload();
	                }, 2000); 
	            }
	        },
	        error:function(err){
	          $("#loader-wrapper").hide();
	        }
      	});
    }
}
function accept_booking(){
	$("#accept_form").parsley().validate();
    if($("#accept_form").parsley().isValid()){
    	$('#accept_booking').modal('hide');
    	$("#accept_btn").attr('disabled',true);
    	accept_reject_booking('Accepted');
    }
}
function reject_booking(){
	 swal({
        title: "Confirmation",
        text: "Are you sure you want to reject booking ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
        	accept_reject_booking('Rejected');
        }
    });
}
function accept_reject_booking(status){
	$("#loader-wrapper").show();
	var event_id ='<?php if(!empty($booking_details['event_id'])){ echo base64_encode($booking_details['event_id']);} ?>';
	var user_id ='<?php if(!empty($booking_details['user_id'])){ echo base64_encode($booking_details['user_id']);} ?>';
	var order_status_id ='<?php if(!empty($order_record['order_status_id'])){ echo base64_encode($order_record['order_status_id']);} ?>';

	var sp_order_cancel_date=$("#sp_order_cancel_date").val();
	if(status=='Accepted'){
		if(sp_order_cancel_date==''){
			alert('Please select cancel date.');
			return false;
		}
	}  	
	$.ajax({
        url: 'front/Booking/sp_accept_reject_booking',
        type: 'POST',
        data: {event_id:event_id,order_status_id:order_status_id,status:status,sp_order_cancel_date:sp_order_cancel_date},
       	dataType: 'json',
        success:function(resp){
		  $("#accept_form").parsley().reset();
          $("#accept_form")[0].reset();
          $("#loader-wrapper").hide();  
          $("html, body").animate({ scrollTop: 0 }, "slow");
          $(".message_box").show(); 
          $("#accept_btn").attr('disabled',false);
            ajax_check_session(resp);
            if(resp.status==0){
                $(".message_box").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                setTimeout(function() {
                 $(".message_box").html('');
                  $(".message_box").hide();
                }, 5000); 
              }else{
                $(".message_box").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                setTimeout(function() { 
                 $(".message_box").html('');
                 $(".message_box").hide();
                 location.reload();
                }, 2000); 
            }
        },
        error:function(err){
          $("#loader-wrapper").hide();
        }
  	});
}
function add_deal(){
	swal({
        title: "Confirmation",
        text: "Are you sure you want to add this event in deal ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
    		$("#loader-wrapper").show();
			var event_id ='<?php if(!empty($booking_details['event_id'])){ echo base64_encode($booking_details['event_id']);} ?>';
			$(".add_deal").attr('disabled',true);
			$.ajax({
		        url: 'front/Deal/event_add_to_deal',
		        type: 'POST',
		        data: {event_id:event_id},
		       	dataType: 'json',
		        success:function(resp){
		            
					$("#loader-wrapper").hide();  
					$(".message_box").show(); 
					$(".add_deal").attr('disabled',false);
		            ajax_check_session(resp);
		            if(resp.status==0){
		            	$("html, body").animate({ scrollTop: 0 }, "slow");
		                $(".message_box").html('<div class="alert alert-danger add_deal"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
		                setTimeout(function() {
		                 	$(".message_box").html('');
		                  	$(".message_box").hide();
		                }, 5000); 
		              }else{
		                var html='<button class="btn-lg-style yellow-btn mrgT-6 add_deal">Deal Added</button>';
						$('#deal_span').empty().append(html);
		                setTimeout(function() { 
		                 $(".message_box").html('');
		                 $(".message_box").hide();
		                 location.reload();
		                }, 2000); 
            		}
		        },error : function (resp){ 
		            $("#loader-wrapper").hide();
		            $(".add_deal").attr('disabled',false);
		        }
		  	});
        }
    });
}
function remove_deal(){
	swal({
        title: "Confirmation",
        text: "Are you sure you want to remove event from deal ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
    		$("#loader-wrapper").show();
			var event_id ='<?php if(!empty($booking_details['event_id'])){ echo base64_encode($booking_details['event_id']);} ?>';
			$(".add_deal").attr('disabled',true);
			$.ajax({
		        url: 'front/Explore/remove_from_deal',
		        type: 'POST',
		        data: {event_id:event_id},
		       	dataType: 'json',
		        success:function(resp){
		            
					$("#loader-wrapper").hide();  
					$("html, body").animate({ scrollTop: 0 }, "slow");
					$(".message_box").show(); 
					$(".add_deal").attr('disabled',false);
		            ajax_check_session(resp);
		            if(resp.status==0){
		                $(".message_box").html('<div class="alert alert-danger add_deal"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
		                setTimeout(function() {
		                 	$(".message_box").html('');
		                  	$(".message_box").hide();
		                }, 5000); 
		              }else{
		                var html='<button class="btn-lg-style yellow-btn mrgT-6 add_deal" onclick="add_deal()">Add to Deal</button>';
						$('#deal_span').empty().append(html);
		                setTimeout(function() { 
		                 $(".message_box").html('');
		                 $(".message_box").hide();
		                 location.reload();
		                }, 2000); 
            		}
		        },error : function (resp){ 
		            $("#loader-wrapper").hide();
		            $(".add_deal").attr('disabled',false);
		        }
		  	});
        }
    });
}
</script>