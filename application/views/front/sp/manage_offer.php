
<section class="section-div pt-4 my-store">
  <div class="container">
    <div class="row">
    <div class="col-md-4 no-background">
        <div class="service-provider filters">
            <h4 class="heading">FILTERS</h4>
            <div class="card searchFilter mt-3">
                <h4 class="filter-heading">Search Within</h4>
                <form class="form-inline my-2 my-lg-0 posrel" action="manage-offers" method="POST">
                    <input class="form-control" id ="name"  name="name" value='<?php if(isset($_POST["name"]) && !empty($_POST["name"])){echo $_POST["name"];}?>' placeholder="Search" type="search" id="searchInput2">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
        </div>
        <?php include APPPATH.'views/front/include/play_store_link.php';  ?>
     </div>
      <div class="col-md-8">
        <div class="message_box" style="display:none;"></div>
        <div class="tab-container">
          <ul class="nav nav-tabs tab-btn w-75 float-left" role="tablist">

            <li class="nav-item"> <a class="nav-link" href="JavaScript:Void(0);" data-target="#ManageProvider" onclick="get_list_data('service');" role="tab" data-toggle="tab">Manage service</a>
            </li>
            <li class="nav-item"> <a class="nav-link active" href="JavaScript:Void(0);" data-target="#ManageOffers" onclick="get_list_data('offer');" role="tab" data-toggle="tab">Manage Offer</a>

            </li>
          </ul>
          <div class="service-btn-container w-25 float-right text-right" id="store_btn_box">
            <a href="add-offer" class="btn service-offer-btn">Add Offer <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
          </div>
          <div class="tab-content" style="padding-top: 60px;">
            <div role="tabpanel" class="tab-pane" id="ManageProvider">
                <div class="flex">
                    <div class="content cards-listing-boxes">
                        <div class="row" id ="service_box">
                        </div>
                    </div>
                </div>
               <!--  <div class="text-center pt-4"> <a href="javascript:void(0)" id="loadMore">Load More</a></div> -->
            </div>
            <div role="tabpanel" class="tab-pane active" id="ManageOffers">
                <div class="flex">
                    <div class="content1">
                        <div class="row" id="offer_box">
                        </div>
                    </div>
                </div>
                <div class="text-center pt-4"> <a href="javascript:void(0)" id="loadMore2">Load More</a></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<script>
    $( document ).ready(function() {
        get_list_data('offer');
    });
    function get_list_data(tab) {
       
        switch(tab){
           
            case 'service':
                 location.href='manage-service';
            break;
            default:
                $("#loader").show();
                    var totalrecords='<?php echo $offer_total_records;?>';
                    var track_page = 0;
                    load_contentsu(track_page);
                    $(window).scroll(function() { //detect page scroll
                        if($(window).scrollTop() + $(window).height() >= $(document).height()-600) { //if user scrolled to bottom of the page
                            track_page++;
                            var current_page=<?php echo FRONT_LIMIT; ?>*track_page;
                           
                            if(current_page<totalrecords){
                            load_contentsu(track_page);
                          }
                        }
                    }); 
                    function load_contentsu(track_page){ 
                        $('#loader').show(); 
                        var check=$("#offer_box").html(); 
                        var name=$("#name").val(); 
                        $.ajax({
                            type:'POST',
                            data:{ 
                                page:track_page,
                                name:name
                            },
                            url: "<?php echo base_url();?>front/Mystore/manage_offer_data", 
                           success:function(data) {
                                ajax_check_session(data,1);
                                if(data.trim().length == 0){
                                  if(check.trim().length == 0){
                                    $("#loadMore2").text("No Content").addClass("noContent");
                                    $('#loader').hide();
                                    return;
                                  }
                                }
                                if(track_page==0){
                                    $("#offer_box").html('');
                                }
                                $('#loader').hide(); 
                               // $(".load_text").html('');
                                $("#offer_box").append(data);

                            }
                        });
                    }

            break;
        } 
    }

    $(document).ready(function() {
     
      $(".content").slice(0, 4).show();
      $("#loadMore,#loadMore2").on("click", function(e) {
        e.preventDefault();
        $(".content:hidden").slice(0, 1).slideDown();
        if ($(".content:hidden").length == 0) {
          $("#loadMore").text("No Content").addClass("noContent");
        }
      });
      $(".content1").slice(0, 4).show();
      $("#loadMore2").on("click", function(e) {
        e.preventDefault();
        $(".content1:hidden").slice(0, 1).slideDown();
        if ($(".content1:hidden").length == 0) {
          $("#loadMore2").text("No Content").addClass("noContent");
        }
      });
    });
  
  
  /**************************************/
</script>