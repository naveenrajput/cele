<div class="service-provider smilarEvents   pdT-0">
	<h4 class="heading">My Bookings</h4>
	<?php if(!empty($my_bookings)){
		foreach($my_bookings as $row){ ?>
		<div class="service">
			<div class="service-img">
				<img src="<?php if(!empty($row['image'])){echo $row['image'];}else{ echo 'assets/front/images/Group9.png';}?>" alt="Event Image">
			</div>
			<div class="service-details">
				<h6 class="userName"><?php if(!empty($row['event_title'])){echo $row['event_title'];}?></h6>
				<p><?php if(!empty($row['event_date'])){echo $row['event_date'];}?></p>
                <p><?php if(!empty($row['event_address'])){echo $row['event_address'];}?></p>
                 <a href="my-booking-details/<?php if(!empty($row['event_id'])){ echo base64_encode($row['event_id']);}?>" class="btn btn-primary join-btn">View Details</a>
			</div>
		</div>
	<?php } }else { echo 'No bookings are available.';}?>
</div>