<div id="demo" class="carousel slide" data-ride="carousel">

    <!-- Indicators -->
    <ul class="carousel-indicators">
        <li data-target="#demo" data-slide-to="0" class="active"></li>
        <?php if(!empty($service['img2'])) { ?>
        <li data-target="#demo" data-slide-to="1"></li>
        <?php }  if(!empty($service['img3'])) { ?>
        <li data-target="#demo" data-slide-to="2"></li>
        <?php } ?>
    </ul>

    <!-- The slideshow -->
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="<?php if(!empty($service['img1'])){echo $service['img1'];} else{echo 'assets/images/phl_service_banner.png';}?>" alt="Service Image">
        </div>
       <?php if(!empty($service['img2'])) { ?>
        <div class="carousel-item">
            <img src="<?php if(!empty($service['img2'])) {echo $service['img2']; }else { echo 'assets/images/phl_service_banner.png';} ?>" alt="Service Image">
        </div>
        <?php } if(!empty($service['img3'])) { ?>
        <div class="carousel-item">
            <img src="<?php if(!empty($service['img3'])) { echo $service['img3'];} else { echo 'assets/images/phl_service_banner.png';}?>" alt="Service Image">
        </div>
        <?php } ?>
    </div>
</div>