
 <script src="https://www.paypal.com/sdk/js?client-id=AYXHJUrvsNQIJC-ZqNvma69H9S7a9_2lCrFM3d0odWweGGmoOa-zPL38H_9LoA0mOLoTL3y5sAjgLtjV&vault=true&disable-funding=credit,card" data-sdk-integration-source="button-factory"></script>
<section class="section-div pt-4 subscription-section">
   <div class="container">
      <div class="message_box" style="display:none;"></div>
       <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger">
              <button data-dismiss="alert" class="close" type="button">×</button>
              <?php echo $this->session->flashdata('error') ?>
          </div>
        <?php } ?>
        <?php if ($this->session->flashdata('success')) { ?>
          <div class="alert alert-success">
              <button data-dismiss="alert" class="close" type="button">×</button>
              <?php echo $this->session->flashdata('success') ?>
          </div>
        <?php } ?>
        <?php 
            $is_perv=0;
            if(empty($previous_plan)){
                $is_perv=1;    
            }
            if(isset($previous_plan) && !empty($previous_plan)){
            $current_date=convertGMTToLocalTimezone(date('Y-m-d H:i:s'));
            $current_time= convertGMTToLocalTimezone(date('Y-m-d H:i:s'),'',true);
            $curr_d=strtotime($current_date.' '.$current_time);
            $ev_date=convertGMTToLocalTimezone($previous_plan['end_date']);
            $ev_time= convertGMTToLocalTimezone($previous_plan['end_date'].$previous_plan['end_time'],'',true);
            $ev=strtotime($ev_date.' '.$ev_time);
            if($curr_d>$ev || ($previous_plan['is_current']==0)){ 
                $is_perv=1;    
            } ?>
        <div class="form-option-block">
            <div class="form-check-inline">
                <label class="radio-container">
                   <div class="card">
                      <label>Recent Plan: <span><?php echo !empty($previous_plan['title'])?ucfirst($previous_plan['title']):''; ?></span></label>
                      <label>Plan start date: <span><?php if(!empty($previous_plan['start_date'])){ echo convertGMTToLocalTimezone($previous_plan['start_date'].' '.$previous_plan['start_time']);} else {echo 'N/A';}?></span></label>
                      <label>Plan end date: <span><?php if(!empty($previous_plan['end_date'])){ echo convertGMTToLocalTimezone($previous_plan['end_date'].' '.$previous_plan['end_time']);} else {echo 'N/A';}?></span></label>
                      <label>No. of Orders: <span><?php echo !empty($previous_plan['total_remaining_order'])?$previous_plan['total_remaining_order']:''; ?></span></label>
                      <?php if($curr_d<$ev && ($previous_plan['is_current']==1)){  ?>
          
                        <button type="button" class ='btn btn-danger px-4'  value="" onclick="window.location.href='cancel-subscription-plan/<?php echo base64_encode($previous_plan["subscriptionID"]);?>'">Cancel Subscription</button>
                      <?php } ?>
                   </div>

                </label>
             </div>
         </div>
         <?php } ?>
        <div class="form-option-block">
            <?php if(!empty($plan)){ $i=0;
                $class='';
            foreach ($plan as $key => $value) { $i++; 
                if($i==1){ ?>
                 <div class="form-check-inline" onclick="show_description('<?php echo $i;?>');">
                    <label class="radio-container sub_plan con-one">
                       <input type="radio" name="optradio" <?php if(!empty($previous_plan['plan_id'])){if($previous_plan['plan_id']==$value['plan_id']){echo "checked=checked" ;$class='newactive';} }else{ echo "checked=checked" ;$class='newactive';}?> >
                       <span class="checkmark"></span>
                       <div class="card <?php echo $class;?>">
                          <h6 class="text-warning"><?php if(!empty($value['title'])){echo ucfirst($value['title']);}?></h6>
                          <p class="mb-0">Basic features are available for users</p>

                       </div>
                    </label>
                 </div>
                 <?php } else { ?>
                <div class="form-check-inline" onclick="show_description('<?php echo $i;?>');">
                    <label class="radio-container sub_plan con-two">
                       <input type="radio" name="optradio" <?php if(!empty($previous_plan['plan_id'])){if($previous_plan['plan_id']==$value['plan_id']){echo "checked=checked" ;$class='newactive';}else{$class='';} }?>>
                       <span class="checkmark"></span>
                       <div class="card <?php echo $class;?>">
                          <h6 class="text-warning"><?php if(!empty($value['title'])){echo ucfirst($value['title']);}?></h6>
                          <p class="mb-0">Basic features are available for users</p>
                       </div>
                    </label>
                 </div>
            <?php } } }?>
        </div>
        <?php if(!empty($plan)){ $i=0;
            
            foreach ($plan as $key => $value) { $i++; 
                if($i==1){ ?>
                <div class="card details-card blocks" id="block_<?php echo  $i;?>" <?php if(!empty($previous_plan['plan_id'])){if($previous_plan['plan_id']==$value['plan_id']){echo "style=display:block";}else{echo "style=display:none";} }else{ echo "style=display:block";}?>>
                    <?php if(!empty($value['description'])){echo ucfirst($value['description']);}?>
                    <div class="d-flex justify-content-center mt-4">
                       <!--  <button type="submit" class="btn btn-primary px-4" onclick="payment('<?php //echo base64_encode($value["plan_id"]);?>')">Subscribe</button> -->
                        <!-- <button type="button" class ='btn btn-primary px-4'  value="Pay to subscribe" onclick="window.location.href='plan-subscription/<?php //echo base64_encode($value["plan_id"]);?>'">Subscribe</button> -->
                        <!-- <a href="https://www.sandbox.paypal.com/webapps/billing/plans/subscribe?plan_id=P-6U749705PA291951KL54CKLQ" class="btn btn-primary"> Subscribe</a> -->
                        <?php if($is_perv==1){ ?>
                            <div id="paypal-button-container"></div>
                            <script>
                              paypal.Buttons({
                                  style: {
                                      shape: 'rect',
                                      color: 'gold',
                                      layout: 'vertical',
                                      label: 'subscribe'
                                  },
                                  createSubscription: function(data, actions) {
                                    return actions.subscription.create({
                                      'plan_id': '<?php echo $value["paypal_plan_id"];?>'
                                    });
                                  },
                                  onApprove: function(data, actions) {
                                    paypal_subscription_payment(data.orderID,data.paymentID,data.billingToken,data.subscriptionID,'<?php echo $value["paypal_plan_id"];?>')
                                  },
                                  onCancel: function (data) {
                                    window.location.href="<?php echo base_url();?>subscription";
                                  },
                                  onError: function (err) {
                                    alert(err);
                                  }
                              }).render('#paypal-button-container');
                            </script>
                        <?php } ?>
                    </div>

                </div>
        <?php } else { ?>
            <div class="card details-card blocks" id="block_<?php echo $i;?>" <?php if(!empty($previous_plan['plan_id'])){if($previous_plan['plan_id']==$value['plan_id']){echo "style=display:block";}else{echo "style=display:none";} }else{echo "style=display:none";}?>>
                <?php if(!empty($value['description'])){echo ucfirst($value['description']);}?>
                 <div class="d-flex justify-content-center mt-4">
                    <!-- <button type="button" class ='btn btn-primary px-4'  value="Pay to subscribe" onclick="window.location.href='plan-subscription/<?php //echo base64_encode($value["plan_id"]);?>'">Subscribe</button> -->
                   <?php if($i==2 && $is_perv==1){  ?>
                    <div class="paypal-button-containers"></div>
                    <script>
                      paypal.Buttons({
                          style: {
                              shape: 'rect',
                              color: 'gold',
                              layout: 'vertical',
                              label: 'subscribe'
                          },
                          createSubscription: function(data, actions) {
                            return actions.subscription.create({
                              'plan_id': '<?php echo $value["paypal_plan_id"];?>'
                            });
                          },
                          onApprove: function(data, actions) {
                            paypal_subscription_payment(data.orderID,data.paymentID,data.billingToken,data.subscriptionID,'<?php echo $value["paypal_plan_id"];?>')
                          },
                          onCancel: function (data) {
                            window.location.href="<?php echo base_url();?>subscription";
                          },
                          onError: function (err) {
                            alert(err);
                          }

                      }).render('.paypal-button-containers');
                    </script>
                    <?php }elseif($i==3 && $is_perv==1) { ?>
                        <div id="paypal-button-containerss"></div>
                        <script>
                          paypal.Buttons({
                              style: {
                                  shape: 'rect',
                                  color: 'gold',
                                  layout: 'vertical',
                                  label: 'subscribe'
                              },
                              createSubscription: function(data, actions) {
                                return actions.subscription.create({
                                  'plan_id': '<?php echo $value["paypal_plan_id"];?>'
                                });
                              },
                              onApprove: function(data, actions) {
                                 paypal_subscription_payment(data.orderID,data.paymentID,data.billingToken,data.subscriptionID,'<?php echo $value["paypal_plan_id"];?>')
                              },
                              onCancel: function (data) {
                                window.location.href="<?php echo base_url();?>subscription";
                              },
                              onError: function (err) {
                                alert(err);
                              }
                          }).render('#paypal-button-containerss');
                        </script>
                    <?php } ?>
                 </div>
            </div>
        <?php } } } ?>
   </div>
</section>
<script>
function show_description(id){
    $(".blocks").css("display", 'none');
    $("#block_"+id+"").css("display", 'block');
    
}
$(document).ready(function() {
  /*$(".con-one").click(function() { 
      $('.con-one .card').addClass("newactive");
       $('.con-two .card').removeClass("newactive");
  });
    $(".con-two").click(function() { 
      $('.con-two .card').addClass("newactive");
      $('.con-one .card').removeClass("newactive");
  });*/
$(".sub_plan").click(function() {
     $('.sub_plan .card').removeClass("newactive");
     $(this).find('.card').addClass("newactive");
});

});
function paypal_subscription_payment(orderID,paymentID,billingToken,subscriptionID,planId){
    $("#loader-wrapper").show(); 
    $.ajax({
        type: 'POST',
        url: "front/Mystore/paypal_subscription_payment",
        data: {order_id:orderID,payment_id:paymentID,billing_token:billingToken,subscription_id:subscriptionID,plan_id:planId},
        dataType: 'json',
        success:function(resp){
            $("#loader-wrapper").hide();  
              $("html, body").animate({ scrollTop: 0 }, "slow");
              $(".message_box").show();
             if(resp.status==0){
                $(".message_box").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                setTimeout(function() {
                 $(".message_box").html('');
                  $(".message_box").hide();
                }, 5000); 
              }else{
                $(".message_box").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                setTimeout(function() { 
                    location.reload();
                  
                }, 2000); 
            }
        },error: function(err){
             $("#loader-wrapper").hide(); 
        }
    }); 
}
</script>
