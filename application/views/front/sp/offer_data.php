<?php if(isset($offers) && !empty($offers)) {// echo '<pre>';print_r($offers);exit;
    foreach($offers as $row){?>
         <div class="col-xs-12 col-md-6  col-lg-6 mb-6">
            <a href="offer-details/<?php if(!empty($row['offer_id'])) echo base64_encode($row['offer_id']);?>"">
               <div class="card">
                  <div class="card-banner">
                     <img class="card-img-top" src="<?php if(!empty($row['img1'])){ echo $row['img1'];}else{ echo 'assets/images/phl_service_banner.png';}?>" alt="Offer image">
                  </div>
                  <div class="card-body">
                     <h5 class="card-title"><?php if(!empty($row['offer_name'])){echo mb_strimwidth($row['offer_name'],0, 20,'...');}else{ echo 'N/A';}?>
                     </h5>
                     <div class="ctn inner-profile-title">
                        <div class="user-profile">
                           <img src="assets/front/images/profile.png" alt="logo">
                        </div>
                        <div class="post-text">
                           <div class="recentPostDetails">
                              <h6 class="userName"><span class="btn-sm-style completed mr-2"><?php if(!empty($row['discount'])){echo $row['discount'];}else{ echo 'N/A';}?> % OFF</span>on <?php if(!empty($row['service_title'])){echo $row['service_title'];}else{'N/A';}?></h6>
                           </div>
                        </div>
                     </div>
                     <div class="functions-list icon-content-box">
                                <span><?php if(!empty($row['description'])){echo mb_strimwidth($row['description'],0, 50,'...');}else{ echo 'N/A';}?></span>
                                <div class="post-btn relPos">
                                    <div class="dropdown">
                                        <a href="javscript:void(0);" data-toggle="dropdown">
                                            <img src="assets/front/images/option.svg" alt="like">
                                        </a>
                                        <div class="dropmenuContainer">
                                            <div class="dropdown-menu"> 
                                                <a class="dropdown-item" href="edit-offer/<?php if(!empty($row['offer_id']))  echo base64_encode($row['offer_id']);?>"><img src="assets/front/images/icons/edit.svg" alt="" /> Edit Offer</a>
                                                <a class="dropdown-item" href="javascript:void(0)" onclick="delete_front_records('offer','id','<?php if(!empty($row["offer_id"]))  echo base64_encode($row["offer_id"]);?>')"><img  src="assets/front/images/icons/garbage.svg" alt="" /> Delete Offer</a> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                  </div>
               </div>
            </a>
         </div>
<?php } }?>
