
<section class="signup-section">
<div class="slider-container">
<div class="hero-image">
    <div class="hero-text">
        <h1 style="font-size:50px"><?php echo $page_title;?></h1> 
    </div>  
</div>
</div>
<div class="main-login-box forgot-password">
    <div class="form-container">
        <div class="form-heading">
          <h5><?php echo $page_title;?></h5>
        </div>
        <div class="message_box" style="display:none;"></div>
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger">
              <button data-dismiss="alert" class="close" type="button">×</button>
              <?php echo $this->session->flashdata('error') ?>
          </div>
        <?php } ?>
        <?php if ($this->session->flashdata('success')) { ?>
          <div class="alert alert-success">
              <button data-dismiss="alert" class="close" type="button">×</button>
              <?php echo $this->session->flashdata('success') ?>
          </div>
        <?php } ?>
        <form class="login-form"  method="POST" action="<?php echo base_url(); ?>reset-password?token=<?php echo $this->input->get('token'); ?>" id="forgot_password" data-parsley-validate>
                <div class="form-group">
                   <input type="password" class="form-control" id="new_password" name="new_password" placeholder="New Password" data-parsley-minlength="8" maxlength="12" data-parsley-required data-parsley-required-message="Please enter new password." data-parsley-number="1" data-parsley-alphabet="1" data-parsley-maxlength-message="Password must be at maximum 12 digits long." data-parsley-special="1" data-parsley-minlength-message="Password must be at least 8 characters long.">
                    <?php echo form_error('new_password'); ?> 
                </div>
                <div class="form-group">
                   <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm Password" data-parsley-equalto="#new_password" required data-parsley-required data-parsley-required-message="Please enter confirm password." data-parsley-equalto-message="New password and confirm password not match.">
                    <?php echo form_error('confirm_password'); ?> 
                </div>
                <div class="d-flex justify-content-center mt-4">
                  <button type="submit" class="btn btn-primary px-4" id="submit_form">Submit</button>
                </div>
        </form>
    </div>
</div>
</section>
<script>
    function form_submit(id)
    {
        submitDetailsForm(id);
        return false;
    }
</script>
