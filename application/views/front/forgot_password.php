
<section class="signup-section">
<div class="slider-container">
<div class="hero-image">
    <div class="hero-text">
        <h1 style="font-size:50px"><?php echo $page_title;?></h1> 
    </div>  
</div>
</div>
<div class="main-login-box forgot-password">
    <div class="form-container">
        <div class="form-heading">
          <h5><?php echo $page_title;?></h5>
        </div>
        <div class="message_box" style="display:none;"></div>
        <?php if ($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger">
              <button data-dismiss="alert" class="close" type="button">×</button>
              <?php echo $this->session->flashdata('error') ?>
          </div>
        <?php } ?>
        <?php if ($this->session->flashdata('success')) { ?>
          <div class="alert alert-success">
              <button data-dismiss="alert" class="close" type="button">×</button>
              <?php echo $this->session->flashdata('success') ?>
          </div>
        <?php } ?>
        <form class="login-form" action="" id="forgot_password" data-parsley-validate>
                <div class="form-group">
                  <input type="text" class="form-control" name ="email" id="email" placeholder="Email/Login ID" style="padding: 15px 20px;" data-parsley-required data-parsley-required-message="Please enter email/login ID ."/>
                </div>
                <div class="d-flex justify-content-center mt-4">
                  <button type="submit" class="btn btn-primary px-4" id="submit_form" onclick="return form_submit('forgot_password');">Submit</button>
                </div>
        </form>
    </div>
</div>
</section>
<script>
    function form_submit(id)
    {
        submitDetailsForm(id);
        return false;
    }
</script>
