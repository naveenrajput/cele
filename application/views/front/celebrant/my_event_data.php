<?php if(!empty($bookings)){ 
    foreach($bookings as $row) { ?>
<div class="col-xs-12 col-md-6 col-lg-6">
    <a href="<?php echo $detail_action;?>/<?php if(!empty($row['event_id'])){ echo base64_encode($row['event_id']);}?>">
        <div class="card">
            <div class="card-banner">
               <img class="card-img-top"
                  src="<?php echo !empty($row['image'])?$row['image']:'assets/images/phl_service_banner.png'?>"
                  alt="Card image cap" />
                  <?php if(!empty($row['event_status'])){
                    if($row['event_status']=='Not started'){
                        $sts_cls='not-started';
                    }if($row['event_status']=='On Going'){
                        $sts_cls='on-going';
                    }if($row['event_status']=='Completed'){
                        $sts_cls='completed';
                    }if($row['event_status']=='Canceled'){
                        $sts_cls='not-started';
                    }?>
                    <div class="top-ribbon <?php echo $sts_cls;?>"><?php echo $row['event_status'];?></div>
                <?php }?>
               
            </div>
            <div class="card-body">
               <div class="card-title-box">
                    <h5 class="card-title"><?php if(!empty($row['event_title'])){echo $row['event_title'];}else{ echo 'N/A';}?></h5>
                    <div class="latest-sec-2 sec-3">
                        <?php if(!empty($row['event_status']) && !empty($row['order_status'])){ 
                            if($row['event_status']=='Not started' && $row['order_status']!='Canceled'){ ?>
                            <div class="dropdown">
                               <a href="javascript:void(0);" class="zigzag" data-toggle="dropdown" aria-expanded="false">
                                <img src="assets/front/images/option.svg" alt="like">
                               </a>
                                <div class="dropmenuContainer1">
                                    <div class="dropdown-menu1 dropdown-menu">
                                       <a class="dropdown-item"
                                          href="javascript:void(0);" onclick="celebrant_canceled_event('<?php echo !empty($row["event_id"])?base64_encode($row["event_id"]):"";?>','<?php echo !empty($row["order_id"])?base64_encode($row["order_id"]):"";?>');">
                                       Cancel Booking</a>
                                    </div>
                                </div>
                            </div>
                        <?php } }?>
                    </div>
                </div>
                <div class="functions-list"><span><?php if(!empty($row['event_date'])) echo convertGMTToLocalTimezone($row['event_date']);?>  | <?php if(!empty($row['event_date'])) echo convertGMTToLocalTimezone($row['event_date'],'',true);?></span>
                </div>
                <div class="ctn inner-profile-title">
                    <div class="user-profile">
                        <img src="<?php echo !empty($row['profile_picture'])?$row['profile_picture']:'assets/front/images/profile0.png'?>" alt="logo">
                    </div>
                    <div class="post-text">
                        <div class="recentPostDetails">
                            <h6 class="userName"><?php if(!empty($row['fullname'])){echo $row['fullname'];}else{ echo 'N/A';}?></h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>
<?php }  } ?>

<script type="text/javascript">

function celebrant_canceled_event(event_id,order_id){
    
    swal({
        title: "Confirmation",
        text: "Are you sure you want to cancel this event ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            $("#loader-wrapper").show(); 
            $.ajax({
                type: 'POST',
                url: "front/Booking/celebrant_canceled_event",
                data: {event_id:event_id,order_id:order_id},
                dataType: 'json',
                success:function(resp){
                    $("#loader-wrapper").hide();
                    ajax_check_session(resp); 
                    if(resp.status==0){
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                        $(".message_box").html(resp.msg);
                        $(".message_box").show();
                        $(".message_box").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                            setTimeout(function() {
                             $(".message_box").html('');
                             $(".message_box").hide();
                            }, 5000); 
                    }else{
                       $(".message_box").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                            setTimeout(function() { 
                             $(".message_box").html('');
                             $(".message_box").hide();
                             location.reload();
                            }, 2000); 
                    }
                    
                }
            });
        }
    });
    
}
</script>