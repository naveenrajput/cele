<?php 
if (isset($friends_list) && !empty($friends_list)) {
    foreach ($friends_list as $friends) { 
?>        
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
            <div class="friends-section">
                <div class="image-container">
                    <img src="<?php  
                                if(!empty($friends['profile_picture'])) { 
                                    echo base_url().$friends['profile_picture'] ; 
                                }else
                                { 
                                    echo base_url().'assets/front/images/profile0.png' ; 
                                   /* if($friends['gender'] == 'Male'){ echo base_url().'assets/front/images/profile0.png' ; } 
                                    if($friends['gender'] == 'Female') { echo base_url().'assets/front/images/profile00.jpg' ; } 
                                    if($friends['gender'] == '') { echo base_url().'assets/front/images/profile000.png' ;}*/
                                } ?>" 
                    />
                </div>
                <div class="image-text">
                    <div class="content-text w-100">
                    <div class="text-part">
                       <a href="profile/<?php echo !empty($friends['sent_by'])? base64_encode($friends['sent_by']):'N/A';?>"><p><?php echo !empty($friends['fullname'])?ucfirst($friends['fullname']):'N/A'; ?></p></a>
                       <p><?php if(!empty($friends['age'])){ 
                                    echo $friends['age']; 
                                    if($friends['age']>1){
                                    echo ' Years,';}else{echo ' Year,';}
                                }
                            ?> <?php if(!empty($friends['gender'])){ echo $friends['gender'];}?></p>
                    </div>
                    <div class="menu-part dropdown">
                        <img src="assets/front/images/option.svg" class="menu-option" alt="dots" data-toggle="dropdown" />
                        <div class="drop-content">
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="javascript:void(0);" onclick="accept_reject('<?php echo base64_encode($friends['sent_by']); ?>', '<?php echo base64_encode($friends['request_id']); ?>','unfriend')" ><img src="assets/front/images/icons/delete.svg" class="unfriend-icon" alt="unfriend" /> Unfriend</a>
                               
                            </div>
                        </div>
                    </div>
                </div>
                </div>
           </div>
        </div>
<?php  } } ?>

