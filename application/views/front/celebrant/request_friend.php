<section class="section-div pt-4 beMyDate-div">
	<div class="container">
	<ul class="nav nav-tabs tab-btn" role="tablist">
      <li class="nav-item"> <a class="nav-link" href="friends-list">Friends</a>
      </li>
      <li class="nav-item"> <a class="nav-link active" href="javascript:void(0)">All Request</a>
      </li>
      <li class="nav-item"> <a class="nav-link" href="find-friends-list">Find Friends</a>
      </li>
    </ul>
    
		<div class="row">
			<div class="col-md-4 no-background">
				<div class="service-provider filters">
					<h4 class="heading">FILTERS</h4>
					<div class="card searchFilter mt-3">
						<h4 class="filter-heading">Search Within</h4>
						<form class="form-inline my-2 my-lg-0 posrel" action="friends-request-list" method="POST">
							<input class="form-control" name="name" value='<?php if(isset($_POST["name"]) && !empty($_POST["name"])){echo $_POST["name"];}?>' placeholder="Search" type="search" id="searchInput2">
							<button type="submit"><i class="fa fa-search"></i></button>
						</form>
					</div>
				</div>
				<?php include APPPATH.'views/front/include/play_store_link.php';  ?>
			</div>
			<div class="col-md-8">

				<h4 class="main-heading for-heading">Friends Request</h4>
				<div class="message_box" style="display:none;"></div>
				<div class="row mt-4" id="request_id">
				</div>
				<div class="text-center pt-4"><a href="javascript:void(0)" id="loadMore">
           			<img class="spinner-icon" src="assets/front/images/home/icon/spinner-of-dots.svg" alt="">
           			<span>Load More</span></a>
				</div>
			</div>
	</div>
</section>
<script type="text/javascript">

	/* for load more post */
    $("#loader-wrapper").show();

    
    var totalrequest='<?php echo $total_request;?>';
    
    var track_page = 0;
    //track user scroll as page number, right now page number is 1
    
    load_request_contents(track_page);
    $(window).scroll(function() { //detect page scroll
        if($(window).scrollTop() + $(window).height() >= $(document).height()-400) { //if user scrolled to bottom of the page 

            track_page++;
            
            var current_page=<?php echo FRONT_LIMIT; ?>*track_page;
            
            if(current_page<totalrequest){
                
                load_request_contents(track_page);
            }
        }
    }); 

    //Ajax load function request
    function load_request_contents(track_page){

        $('#loader-wrapper').show(); 
        
        var checkRequest=$("#request_id").html(); 
        var keyword='<?php if(isset($_POST["name"]) && !empty($_POST["name"])){echo $_POST["name"];}?>';
        
        $.ajax({
            type:'POST',
            data:{ 
                 name:keyword,page:track_page,page_name:'Request List'
            },
            url: "<?php echo base_url();?>front/Home/my_friends_request_list_data", 
           success:function(data) {
                ajax_check_session(data,1);
                if(data.trim().length == 0){
                
                  if(checkRequest.trim().length == 0){
                    $("#requestMore").text("No Request").addClass("noRequest");
                    $('#loader-wrapper').hide();
                    return;
                  }
                }
                if(track_page==0){
                    
                    $("#request_id").html('');
                }
                $('#loader-wrapper').hide(); 
                $("#request_id").append(data);

            }
        });
    }

    // accept reject request
    function accept_reject(sent_by,request_id,status){
        if (status == 'Accepted') { st = 'accept'; warn = 'success' ; danger = false };
        if (status == 'Rejected') { st = 'reject'; warn = 'warning' ; danger = true};
        swal({
            
            title: "Confirmation",
            text: "Are you sure you want to "+st+" request ?",
            icon: warn,
            buttons: true,
            dangerMode: danger,
        })
        .then((willDelete) => {
            if (willDelete) {
                $("#loader-wrapper").show(); 
                $.ajax({
                    type: 'POST',
                    url: "front/Home/accept_reject_request",
                    data: {sent_by:sent_by,request_id:request_id,status:status},
                    dataType: 'json',
                    success:function(resp){
                        ajax_check_session(resp);
                        $("#loader-wrapper").hide(); 
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                        $(".message_box").html(resp.msg);
                        $(".message_box").show();
                        if(resp.status==0){
                            $(".message_box").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                                setTimeout(function() {
                                 $(".message_box").html('');
                                 $(".message_box").hide();
                                 location.reload();
                                }, 5000); 
                        }else{
                           $(".message_box").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                                setTimeout(function() { 
                                 $(".message_box").html('');
                                 $(".message_box").hide();
                                 location.reload();
                                }, 5000); 
                        }
                        
                    }
                });
            }
        });
    }
    $(document).ready(function () {
        $(".content").slice(0, 4).show();
        $("#loadMore,#loadMore2").on("click", function (e) {
          e.preventDefault();
          $(".content:hidden").slice(0, 1).slideDown();
          if ($(".content:hidden").length == 0) {
            $("#loadMore").text("No Content").addClass("noContent");
          }
        });
        $(".content1").slice(0, 4).show();
        $("#loadMore2").on("click", function (e) {
          e.preventDefault();
          $(".content1:hidden").slice(0, 1).slideDown();
          if ($(".content1:hidden").length == 0) {
            $("#loadMore2").text("No Content").addClass("noContent");
          }
        });
    });
</script>