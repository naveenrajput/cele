<?php if(isset($bookings) && !empty($bookings)){
    $i=0;
    foreach ($bookings as $key => $value) {$i++; ?>
    
<div class="order-history-list px-4">
   <div class="single-order-list">
      <div class="row">
         <div class="col-sm-6">
            <p>Order ID : <span>#<?php echo !empty($value['order_number'])?$value['order_number']:'N/A'; ?></span>
            <?php if(!empty($value['order_status'])){ 
                if($value['order_status']=='Completed'){
                    $st_class='completed';
                }if($value['order_status']=='Canceled'){
                    $st_class='not-started';
                }if($value['order_status']=='Pending'){
                    $st_class='warning';
                }if($value['order_status']=='Confirmed'){
                    $st_class='warning';
                }?>
            <span class="btn-sm-style <?php echo $st_class;?> mr-2"><?php echo !empty($value['order_status'])?$value['order_status']:'N/A'; ?></span>
            <?php } ?>
            </p>

         </div>
         <div class="col-sm-6 text-right">
            <p><span><?php if(!empty($value['transaction_date'])) echo convertGMTToLocalTimezone($value['transaction_date']);?> | <?php if(!empty($value['transaction_date'])) echo convertGMTToLocalTimezone($value['transaction_date'], '',true);?></span></p>
         </div>
      </div>
      <div class="profile-image">
         <div class="request-profile-image">
            <img src= <?php echo !empty($value['profile_picture'])?$value['profile_picture']:'assets/front/images/profile0.png'; ?> />
         </div>
         <div class="request-profile-option">
            <div class="row">
               <div class="col-sm-6">
                  <h4 class="mb-1 pt-2" style="color: #000;"><?php echo !empty($value['fullname'])?$value['fullname']:'N/A'; ?></h4>
               </div>
               <div class="col-sm-6 text-right">
                  <h4 class="mb-1 pt-2 text-success"><?php echo ADMIN_CURRENCY;?><?php echo !empty($value['paid_amount'])?$value['paid_amount']:'N/A'; ?> Paid</h4>
               </div>
            </div>
            <div class="row">
               <div class="col-sm-6">
                  <p class="text-primary"><?php echo !empty($value['event_title'])?$value['event_title']:'N/A'; ?></p>
               </div>
               <div class="col-sm-6 text-right">
                  <a href="javascript:void(0)" class="v-prof" onclick="get_order_items('<?php echo !empty($value['order_id'])?base64_encode($value['order_id']):'N/A'; ?>')"> Item Details </a>
                  <?php if(!empty($value['order_status'])){ 
                    if($value['order_status']=='Canceled'){ ?>
                        <a href="javascript:void(0)" class="v-prof" onclick="get_cancel_order_details('<?php echo !empty($value['order_id'])?base64_encode($value['order_id']):'N/A'; ?>')"> Cancel order Details </a>
                    <?php } } ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php if($total!=$i){?>
<hr>
<?php } } } else{ ?>
    <div class="order-history-list px-4">
          <div class="single-order-list">
                <p class='text-center'>No Record Found</p>
          </div>
    </div>
<?php } ?>
<script>
function get_cancel_order_details(order_id) {
    $("#loader-wrapper").show();
    var tab='items';
    var box="";
    var url=""; 
    switch(tab){
        case 'items':
             box="#item_box";
             url='front/Booking/get_cancel_order_details/'+order_id;
        break;  
    } 
    $(box).load(url,function() { 
        $("#loader-wrapper").hide(); 
    }); 
}
</script>