<?php if(!empty($details)){
    //echo "<pre>";print_r($details);die;
    foreach($details as $row){?>
    <div class="col-xs-12 col-md-6 col-lg-6">
         <a href="service-provider-detail/<?php  echo !empty($row['service_provider_id'])?base64_encode($row['service_provider_id']):'';?>">
            <div class="card">
                <div class="card-banner">
                    <img class="card-img-top" src="<?php if(!empty($row['img1'])){ echo $row['img1'];}else{ echo 'assets/images/phl_service_banner.png';}?>" alt="Card image
                     cap">
                </div>
                <div class="card-body">
                        <div class="card-title-box">
                            <h5 class="card-title"><?php if(!empty($row['company_name'])){ echo $row['company_name'];}else{ echo 'N/A';}?></h5>
                            <div class="post-btn">
                                <a href="javscript:void(0);"> <i class="fa fa-star" aria-hidden="true"></i>     <span><?php if(!empty($row['rating'])){ echo $row['rating'];}else{ echo 'N/A';}?></span>
                                </a>
                            </div>
                         </div>
                         <div class="ctn inner-profile-title">
                            <div class="user-profile">
                                <a href="profile/<?php echo !empty($row['service_provider_id'])? base64_encode($row['service_provider_id']):'N/A';?>">
                                <img src="<?php if(!empty($row['profile_picture'])){ echo $row['profile_picture'];}else{ echo 'assets/front/images/profile0.png';}?>" alt="logo"></a>
                            </div>
                            <div class="post-text">
                                <div class="recentPostDetails">
                                    <h6 class="userName"><?php if(!empty($row['fullname'])){ echo $row['fullname'];}else{ echo 'N/A';}?></h6> 
                                </div>
                            </div>
                        </div>
                        <div class="functions-list">
                            <?php if(!empty($row['category'])){ echo $row['category'];}else{ echo 'N/A';}?>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
<?php } } ?>