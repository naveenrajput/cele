<div class="table-responsive">
<form action="book-service" method ="POST">
     <input type="hidden" name="s_id" value="<?php echo !empty($service_id)?base64_encode($service_id):'';?>">
    <table class="table border-none">
        <thead>
            <tr>
                <th width="40%">Add Items</th>
                <th width="20%">Unit</th>
                <th width="20%">Price</th>
                <th  width="10%"></th>
            </tr>
        </thead>
        <tbody>
        <?php if(!empty($item_details) && isset($item_details)){$p=0;
                foreach($item_details as $details){ $p++;?>
            <tr>
                <td>
                   <div class="imgWDtext">
                      <strong><?php echo !empty($details['item_name'])?$details['item_name']:'';?></strong>
                      <input type="hidden" name="item_name[<?php echo $details['item_id'];?>]" value="<?php echo !empty($details['item_name'])?$details['item_name']:'';?>">
                   </div>
                </td>
                <td><span class="textblue"><?php echo !empty($details['qty'])?$details['qty']:'';?> <?php echo !empty($details['unit'])?$details['unit']:'';?></span>
                    <input type="hidden" name="qty[<?php echo $details['item_id'];?>]" value="<?php echo !empty($details['qty'])?$details['qty']:'';?>">
                    <input type="hidden" name="unit[<?php echo $details['item_id'];?>]" value="<?php echo !empty($details['unit'])?$details['unit']:'';?>">
                </td>
                <td><span class="textblue"><?php echo ADMIN_CURRENCY;?><?php echo !empty($details['price'])?$details['price']:'';?></span>
                    <input type="hidden" name="price[<?php echo $details['item_id'];?>]" value="<?php echo !empty($details['price'])?$details['price']:'';?>">
                </td>
                <td>
                   <button class="btn btn-link btn-add float-right" type="button">ADD</button>
                   <div class="qty">
                      <div class="qty_inc_dec"> <i class="decrement" onclick="decrementQty('<?php echo $p;?>')">-</i>
                      </div>
                      <input type="text" id="qty_<?php echo $p;?>" name="customer_qty[<?php echo $details['item_id'];?>]" maxlength="100" value="" title="" class="input-text qty_num" readonly />
                      <div class="qty_inc_dec"> <i class="increment" onclick="incrementQty('<?php echo $p;?>')">+</i>
                      </div>
                   </div>
                </td>
            </tr>
         <?php } } else{ ?>
            <tr>
                <td><span class="textblue">No items available.</span></td>
            </tr>
        <?php } ?>
      </tbody>
    </table>
    <?php if(!empty($item_details) && isset($item_details)){ ?>
    <div class="grayWDcontinoue">
        <button type="submit" class="btn-lg-style completed" >Continue & Next</button>
   </div>
   <?php } ?>
</form>
</div>
<script>
  $(document).ready(function() {
        var chk_session='<?php echo $this->session->userdata('user_id');?>';
        if(chk_session==''){
            window.location.href='login';
        }
    });
    $('.qty').hide();
    $('.btn-add').click(function(){
         var $this= $(this);
         $this.hide();
         $this.next('.qty').show();
         $this.next('.qty').find('.qty_num').val('1');
     });
</script>