<section class="section-div pt-5 sd-section">
	<div class="container">
		<?php if ($this->session->flashdata('error')) { ?>
		      <div class="alert alert-danger">
		          <button data-dismiss="alert" class="close" type="button">×</button>
		          <?php echo $this->session->flashdata('error') ?>
		      </div>
		    <?php } ?>
		    <?php if ($this->session->flashdata('success')) { ?>
		      <div class="alert alert-success">
		          <button data-dismiss="alert" class="close" type="button">×</button>
		          <?php echo $this->session->flashdata('success') ?>
		      </div>
		    <?php } ?>
		<div class="row">
			<div class="col-lg-8 service-details-div">
				<div class="card mrgB-20">
					<div class="card-banner service-slider-container slider-transe-overlay rounded-border-box">
						<div id="demo" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <?php //echo '<pre>';print_r($service_image);?>
                            <ul class="carousel-indicators">
                                <?php if(!empty($service_image)){$i=0; 
                                    foreach($service_image as $row){
                                		if(!empty($row)){ ?>
                                    	<li data-target="#demo" data-slide-to="<?php echo $i;?>" class="<?php echo $i==0?'active':'';?>"></li> 
                                <?php $i++;} } } ?>
                            </ul>

                            <!-- The slideshow -->
                            <div class="carousel-inner">
                                <?php if(!empty($service_image)){$i=0; 
                                    foreach($service_image as $row){
                                    if(!empty($row)){  ?>
                                     <div class="carousel-item <?php echo $i==0?'active':'';?>">
                                        <img src="<?php if(!empty($row)){echo $row;} else{echo 'assets/images/phl_service_banner.png';}?>" alt="Service Image">
                                    </div>
                                <?php $i++;} } }?>
                              
                            </div>
                        </div>
						<div class="profileOverlayBanner">
							<div class="InnerProfileOverlay">
								<div class="img-circle-style">
									<img src="<?php echo !empty($sp_details['profile_picture'])?$sp_details['profile_picture']:'assets/front/images/profile0.png';?>" alt="">
								</div>
								<div class="profileTLrating"> <strong class="titleStyle"><?php echo !empty($sp_details['fullname'])?$sp_details['fullname']:'N/A';?></strong>
									<div><span class="starWHbg"><i class="fa fa-star" aria-hidden="true"></i> </span>  <span><?php echo !empty($sp_details['rating'])?$sp_details['rating']:'N/A';?></span>
									</div>
								</div>
							</div>
						</div>
						<div class="d-Service detail-profile-content whiteBGStyle">
							<div class="row">
								<div class="col-sm-12">
									<h5 class="card-title mrgT-20"><?php echo !empty($sp_details['company_name'])?$sp_details['company_name']:'N/A';?> </h5>
									<div class="eventVanue">
										<div class="address">
                                            <a target="_blank" href="https://www.google.com/maps/search/?api=1&query=<?php if(!empty($service['address'])) echo $service['address'];?>"> <span><?php echo !empty($sp_details['address'])?$sp_details['address']:'N/A';?></span>
                                                <img class="map-icon-style" src="assets/front/images/icons/GoogleMaps_logo@2x.png" alt="" />
                                            </a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-body">
                        <div class="d-flex flex-wrap justify-content-between align-self-center align-items-center mb-3">
    						<h3 class="second-title">Services</h3> 
    						<?php 
                                if(checkFollowStatus($service_provider_id)){
                                        $f_cls='btn btn-danger';
                                        $f_text='Unfollow';
                                }else{
                                        $f_cls='btn btn-primary';
                                        $f_text='Follow';
                            }?>
    						<button  onclick="follow_user('<?php echo $service_provider_id?$service_provider_id:'';?>');" class="<?php echo $f_cls;?>" id="follow_btn" type="button"><?php echo $f_text;?></button>
                        </div>
						<div class="tab-content" id="singleService">
							<div class="tab-pane show active" id="tab1" role="tabpanel" aria-labelledby="tab1">
								<div class="accordion" id="accordion-tab-1">
                                    <?php if(!empty($service_offer)){ 
                                        $k=0;
                                        foreach($service_offer as $row){ $k++;?>
                                            <div class="<?php echo $k==1?'card':'card mt-4'?>">
                                                <div class="card-header" id="accordion-tab-1-heading-<?php echo $k;?>">
                                                    <div class="service">
                                                        <div class="service-img">
                                                            <img src="<?php if(!empty($row['img1'])){echo $row['img1'];} else{echo 'assets/images/phl_service_banner.png';}?>" alt="cake">
                                                        </div>
                                                        <div class="service-details pr-0">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <h6 class="userName"><?php echo !empty($row['service_title'])?$row['service_title']:'N/A';?>
                                                                    <?php if(!empty($row['offer_discount'])){ ?>
                                                                         <span class="btn-sm-style completed ml-2"><?php echo $row['offer_discount'].'% OFF';?></span>
                                                                     <?php } ?>
                                                                    </h6>
                                                                    <p><?php echo !empty($row['description'])?$row['description']:'N/A';?></p>
                                                                </div>
                                                                <div class="col-sm-6 text-right">
                                                                    <button  onclick="get_items('<?php if(!empty($row["id"])) echo $row["id"];?>');" class="btn btn-link btn-booknow" type="button" data-toggle="collapse" data-target="#accordion-tab-1-content-<?php echo $k;?>" aria-expanded="false" aria-controls="accordion-tab-1-content-<?php echo $k;?>">Book Now</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="collapse" id="accordion-tab-1-content-<?php echo $k;?>" aria-labelledby="accordion-tab-1-heading-<?php echo $k;?>" data-parent="#accordion-tab-1">
                                                    <div class="card-body pd-0 get_items" id="item_<?php echo !empty($row['id'])?$row['id']:'N/A';?>">
                                                    </div>
                                                   
                                                </div>
                                            </div>
                                    <?php } }?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 no-background">
				<div class="service-provider smilarEvents   pdT-0">
					<?php require_once('similar_events.php'); ?>
				</div>
				<?php include APPPATH.'views/front/include/play_store_link.php';  ?>
			</div>
		</div>
	</div>
</section>
<script>

function get_items(id) {
    $("#loader-wrapper").show();
    var tab='item';
    var box='item_'+id;
    var url=""; 
    switch(tab){
        case 'item':
             box="#"+box;
             url='front/Explore/get_items/'+id;
        break;  
    } 
    $(".get_items").html('');
    $(box).load(url,function() { 
        $("#loader-wrapper").hide(); 
    }); 
}
function follow_user(id) {
    $("#loader-wrapper").show(); 
    $.ajax({
        type: 'POST',
        url: "follow-user",
        data: {'id':id},
        dataType: 'json',
        success:function(resp){
            $("#loader-wrapper").hide();
            if(resp.status==0){
               alert(resp.msg); 
            }else{
                $("#follow_btn").html(resp.msg);
                if(resp.msg=='Unfollow'){
                    $("#follow_btn").removeClass('btn-primary');
                    $("#follow_btn").addClass('btn-danger');
                }else{
                    $("#follow_btn").removeClass('btn-danger');
                    $("#follow_btn").addClass('btn-primary');
                }
            }
            
        },error : function (resp){
            $("#loader-wrapper").hide();
        }
    });																								      
} 
</script>