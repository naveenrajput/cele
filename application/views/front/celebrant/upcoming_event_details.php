
<section class="section-div pt-5">
	<div class="container">
		<div class="message_box" style="display:none;"></div>
		<div class="row">
			<div class="col-lg-8 service-details-div">
				<div class="card">
					<div class="card-banner service-slider-container">
						<div id="demo" class="carousel slide" data-ride="carousel">
						    <!-- Indicators -->
						    <ul class="carousel-indicators">
						        <li data-target="#demo" data-slide-to="0" class="active"></li>
						    </ul>

						    <!-- The slideshow -->
						    <div class="carousel-inner">
						        <div class="carousel-item active">
						            <img src="<?php if(!empty($booking_details['image'])){echo $booking_details['image'];} else{echo 'assets/images/phl_service_banner.png';}?>" alt="Event Image">
						        </div>
						    </div>
						</div>
						<div class="d-Service detail-profile-content white-text-style">
							<div class="row">
								<div class="col-sm-8">
									<h5 class="card-title"><?php if(!empty($booking_details['event_title'])){ echo ucfirst($booking_details['event_title']);} else {echo 'N/A';}?> </h5>
									<div class="eventVanue">
										<!-- <div class="ev-icon">
											<img src="assets/front/images/icons/address-white-icon.svg" alt="" />
										</div> -->
										<div class="address">
											<span><?php if(!empty($booking_details['event_address'])){ echo $booking_details['event_address'];} else {echo 'N/A';}?></span>
											<a target="_blank" href="https://www.google.com/maps/search/?api=1&query=<?php if(!empty($booking_details['event_address'])) echo $booking_details['event_address'];?>"><img class="map-icon-style" src="assets/front/images/icons/GoogleMaps_logo@2x.png"
												alt="" /></a>
										</div>
									</div> 
								</div>
								<div class="col-sm-4 text-right img-msg">
									<?php if(!empty($booking_details['event_status'])){
					                    if($booking_details['event_status']=='Not started'){
					                        $sts_cls='not-started';
					                    }if($booking_details['event_status']=='On Going'){
					                        $sts_cls='on-going';
					                    }if($booking_details['event_status']=='Completed'){
					                        $sts_cls='completed';
					                    }if($booking_details['event_status']=='Canceled'){
					                        $sts_cls='not-started';
					                    }?>
                                	<a class="btn-sm-style <?php echo $sts_cls;?>" href="#"><?php  echo $booking_details['event_status'];?></a>
                                	<?php } ?>
								</div>
							</div>
							<!-- <div class="row">
								<div class="col-md-12">
									<?php if(!empty($accepted)){ ?>
									<div class="peoples">
										<ul class="user-profile-list">
											<?php if($accepted==1){ ?>
											<li>
												<img src="assets/front/images/home/profile/96d99888663a0c871e459e1c687b92de-1.png" alt="">
											</li>
											<?php } if($accepted==2){ ?>
											<li>
												<img src="assets/front/images/home/profile/96d99888663a0c871e459e1c687b92de-1.png" alt="">
											</li>
											<li>
												<img src="assets/front/images/home/profile/96d99888663a0c871e459e1c687b92de-2.png" alt="">
											</li>
											<?php } if($accepted>=3){ ?>
											<li>
												<img src="assets/front/images/home/profile/96d99888663a0c871e459e1c687b92de-1.png" alt="">
											</li>
											<li>
												<img src="assets/front/images/home/profile/96d99888663a0c871e459e1c687b92de-2.png" alt="">
											</li>
											<li>
												<img src="assets/front/images/home/profile/96d99888663a0c871e459e1c687b92de-3.png" alt="">
											</li>
											<?php } ?>
										</ul>
										
										<span> &nbsp;<?php if(!empty($total_attendee)){ echo '+'.$total_attendee.' members going';}?></span>
									</div>
									<?php } ?>
								 </div>
							</div> -->
                            
                            <div class="row ">
                             	<div class="col-md-12 text-left">
                                    <div class="eventDetails dateTMStyle">
                                        <ul class="list-inline mb-1">
                                            <li class="list-inline-item dateTextStyle">
                                                <img src="assets/front/images/icons/event.svg" alt="">	
                                                <span><?php if(!empty($booking_details['event_date'])){ echo convertGMTToLocalTimezone($booking_details['event_date']);} else {echo 'N/A';}?></span>
                                            </li>
                                            <li class="list-inline-item timeTextStyle">
                                                <img src="assets/front/images/icons/clock.svg" alt="">
                                                    <span><?php if(!empty($booking_details['event_date'])){ echo convertGMTToLocalTimezone($booking_details['event_date'],'',true);} else {echo 'N/A';}?></span>
                                            </li>
                                        </ul>
							        </div>
                                 </div>
                          </div>  
                        </div>
                        
                      

						<div class="d-Service detail-profile-content ">

						<div class="row">
								<div class="col-sm-6"> 
								    <div class="profileWDtext">
		                               <div class="img-circle-style">
										   <img src="<?php echo !empty($booking_details['profile_picture'])?$booking_details['profile_picture']:'assets/front/images/profile0.png'?>" alt="">
									   </div>
									   <span><?php if(!empty($booking_details['fullname'])){ echo $booking_details['fullname'];} else {echo 'N/A';}?></span>
								    </div> 
								</div>
								<div class="col-sm-6 text-right img-msg">
									<?php if(!empty($my_request_status['status'])){
											if($my_request_status['status']=='Pending'){
												$btn_cls='btn-warning';
											}elseif($my_request_status['status']=='Accepted'){
												$btn_cls='btn-primary';
											}else{
												$btn_cls='btn-danger';
											} ?>
											Request Status: <button class="btn <?php echo $btn_cls;?> mrgT-6"><?php echo $my_request_status['status'];?></button>
									<?php }else{ ?>
											<a class="btn btn-primary mrgT-6" href="javascript:void(0);" id="rq_btn" onclick="invite_friend('request_to_join');">Request to Join</a>
									<?php } ?>
								</div>
								<?php 
								if(!empty($my_request_status['status'])){
										if($my_request_status['status']=='Pending' && $my_request_status['event_owner']=='Self' ){ 
											if(($booking_details['event_status']=='Not started')|| ($booking_details['event_status']=='On Going')){ ?>
									<div class="col-sm-6 text-right img-msg">
										<button class="btn btn-accept" onclick="event_req_accept_reject('Accepted','<?php echo !empty($my_request_status["request_id"])?base64_encode($my_request_status["request_id"]):"";?>')">Accept</button>
										<button class="btn btn-reject" onclick="event_req_accept_reject('Rejected','<?php echo !empty($my_request_status["request_id"])?base64_encode($my_request_status["request_id"]):"";?>')">Reject</button>
									</div>
								<?php } } } ?>
							</div>  
						</div>
						<div class="card-body profile-detail-style">
							<p><?php if(!empty($booking_details['note'])){ echo $booking_details['note'];} else {echo 'N/A';}?></p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 no-background">
			<!-- 	<div class="service-provider pdT-0 not-50">
					<h4 class="heading">UPCOMING EVENT NEAR YOU</h4>
					<div class="service singlePost">
						<div class="service-img full-width">
							<?php //include 'slider2.php'; ?>
						</div>
						<div class="service-details fWidth">
							<h6 class="userName">Andrey Kim</h6>
							<p>49 Female</p>
							<p class="address">724 S Mission St. Sapulpa, OK 74066, UK</p>
						</div>
					</div>
				</div> -->
				<div class="service-provider">
					<?php include APPPATH.'views/front/include/be_my_date.php';  ?>
				</div>
				<?php include APPPATH.'views/front/include/play_store_link.php';  ?>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">

function invite_friend(type){
	var event_id='<?php if(!empty($booking_details['event_id'])){ echo base64_encode($booking_details['event_id']);}?>';
	swal({
        title: "Confirmation",
        text: "Are you sure you want to join this event ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
        	$("#rq_btn").attr('disabled',true);
        	$("#loader-wrapper").show(); 
            $.ajax({
                type: 'POST',
                url: "invite-friend",
                data: {request_type:type,event_id:event_id},
                dataType: 'json',
                success:function(resp){
                	$("#rq_btn").attr('disabled',false);
                	$("#loader-wrapper").hide(); 
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    $(".message_box").html(resp.msg);
                    $(".message_box").show();
                    ajax_check_session(resp);
                    if(resp.status==0){
                        $(".message_box").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                            setTimeout(function() {
                             $(".message_box").html('');
                             $(".message_box").hide();
                            }, 5000); 
                    }else{
                        $(".message_box").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                            setTimeout(function() {
                                $(".message_box").html('');
                                $(".message_box").hide();
                            $("#rq_btn").text('Requested');    
                            }, 1000); 
                    }
                    
                }
            });
        }
    });
    
}
</script>