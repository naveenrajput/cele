<?php if(isset($request_received) && !empty($request_received)){ 
	$i=0;
	foreach($request_received as $row){  ?>
<div class="col-md-6 <?php if($i>=2)echo 'mt-3';?>" data-toggle="modal" data-target="#myModal">
	
		<div class="single-request-block">
			<a href="profile/<?php echo !empty($row['sent_by'])?base64_encode($row['sent_by']):'N/A';?>">
				<div class="request-profile-image">
					<img src="<?php echo !empty($booking_details['profile_picture'])?$booking_details['profile_picture']:'assets/front/images/profile0.png'?>">
				</div>
			</a>
			<div class="request-profile-option">
				<h4><?php echo !empty($row['fullname'])?$row['fullname']:'N/A';?></h4>
				<?php if(!empty($row['status'])){
				    if($row['status']=='Pending'){ ?>
					<button class="btn btn-accept" onclick="event_req_accept_reject('Accepted','<?php echo !empty($row["request_id"])?base64_encode($row["request_id"]):"";?>')">Accept</button>
					<button class="btn btn-reject" onclick="event_req_accept_reject('Rejected','<?php echo !empty($row["request_id"])?base64_encode($row["request_id"]):"";?>')">Reject</button>
				<?php } else{ 
						if($row['status']=='Accepted'){
							$btn_cls='accept';
							}else{$btn_cls='danger';}?>
						<button class="btn btn-<?php echo $btn_cls;?>"><?php echo $row['status'];?></button>
				<?php } }?>
			</div>
		</div>
	
</div>
<?php $i++;} } ?>
