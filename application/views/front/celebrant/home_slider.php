<div class="modal" id="home_slider_modal">
   <div class="modal-dialog">
      <div class="modal-content">
         <!-- Modal body -->
         <div class="modal-body p-0">
            <button type="button" class="close"
               data-dismiss="modal">&times;</button>
            <div id="demo2" class="carousel slide" data-ride="carousel"
               data-interval="false">
               <!-- Indicators -->
               <ul class="carousel-indicators">
                  <?php if(!empty($details) && isset($details)){ 
                     $i=0;
                     foreach($details as $slides){ ?>
                     <li data-target="#demo2" data-slide-to="<?php echo $i;?>" class="<?php echo $i==0?'active':'';?>"></li>
                  <?php $i++;} } ?>
               </ul>
               <!-- The slideshow -->
               <div class="carousel-inner" style=" width:100%;">
                  <?php if(!empty($details) && isset($details)){ 
                     $i=0;
                     foreach($details as $slides){ 
                        if($slides['media_type']=='video'){ ?>
                           <div class="carousel-item <?php echo $i==0?'active':'';?>">
                              <video autoplay controls loop>
                                 <source src="<?php echo !empty($slides['media_path'])?$slides['media_path'] : 'assets/front/images/video/video1.mp4';?>">
                                 Your browser does not support the video tag.
                              </video>
                           </div>
                     <?php }else{ ?>
                        <div class="carousel-item <?php echo $i==0?'active':'';?>">
                           <img src="<?php echo !empty($slides['media_path'])?$slides['media_path'] : 'assets/images/phl_service_banner.png';?>"
                              alt="Los Angeles">
                        </div>
                     <?php } ?>
                   
                  <?php $i++;} } ?>
               </div>
               <!-- Left and right controls -->
               <a class="carousel-control-prev" href="#demo2" data-slide="prev">
               <span class="carousel-control-prev-icon"></span>
               </a>
               <a class="carousel-control-next" href="#demo2" data-slide="next">
               <span class="carousel-control-next-icon"></span>
               </a>
            </div>
         </div>
      </div>
   </div>
</div>
<script>
$(document).ready(function() { 
    $('#home_slider_modal').modal('show');
});
</script>