<section class="section-div pt-4 book-service-section">
   <div class="container">
      <div class="row">
         <div class="col-md-8 service-details">
         <p class="alert_message message_box" id="msg" style='display:none;'></p>
            <div class="card-banner service-slider-container">
               <h2 class="py-4 main-heading">Services</h2>
               <div class="accordion book-service">
                  <div class="card">
                     <div class="card-header">
                        <div class="service">
                           <div class="service-img">
                              <img src="<?php if(!empty($sp_info['img1'])){echo $sp_info['img1'];} else{echo 'assets/images/phl_service_banner.png';}?>" alt="cake">
                           </div>
                           <div class="service-details pr-0">
                              <div class="row">
                                 <div class="col-sm-12">
                                    <h6 class="userName"><?php echo !empty($sp_info['service_title'])?$sp_info['service_title']:'N/A';?></h6>
                                    <p><?php echo !empty($sp_info['description'])?$sp_info['description']:'N/A';?></p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="table-responsive">
                        <table class="table">
                           <thead>
                              <tr>
                                 <th>Items</th>
                                 <th>Quantity</th>
                                 <th>Price</th>
                                 <th>Total</th>
                                 <th>Action</th>
                              </tr>
                           </thead>
                           <tbody>
                            <?php 
                                $total_price=0;
                                if(isset($item_details) && !empty($item_details)){ $p=0;
                                foreach($item_details as $row){$p++; ?>
                              <tr>
                                <td>
                                    <span class="table-list-text"><?php echo !empty($row['item_name'])?$row['item_name']:'N/A';?></span>
                                </td>
                                <td>

                                    <div class="qty">
                                       <div class="qty_inc_dec">
                                          <i class="decrement" onclick="book_decrementQty('<?php echo $p;?>','<?php echo !empty($row["id"])?base64_encode($row["id"]):""?>')">-</i>
                                       </div>
                                       <input type="text" name="qty" id ="qty_<?php echo $p;?>" maxlength="100" value="<?php echo !empty($row['customer_qty'])?$row['customer_qty']:'N/A';?>" title="" class="input-text" readonly/>
                                       <div class="qty_inc_dec">
                                          <i class="increment" onclick="book_incrementQty('<?php echo $p;?>','<?php echo !empty($row["id"])?base64_encode($row["id"]):""?>')">+</i>
                                       </div>
                                    </div>
                                </td>
                                <td class="text-muted"><?php echo ADMIN_CURRENCY;?><?php echo !empty($row['price'])?$row['price']:'';?>/<?php echo !empty($row['qty'])?$row['qty']:'';?> <?php echo !empty($row['unit'])?$row['unit']:'';?></td>
                                <td><?php echo ADMIN_CURRENCY;?><?php echo !empty($row['total_price'])?$row['total_price']:'';?></td>
                                <td>
                                <a  href="javascript:void(0)" onclick="hard_delete_front_records('temp_table','id','<?php if(!empty($row["id"]))  echo base64_encode($row["id"]);?>')"><img  src="assets/front/images/form_icons/ic_delete_red.png" alt="" style="width: 20px;margin-left:15px;" /></a> 
                                </td>
                              </tr>
                            <?php $total_price+=$row['total_price']; } } ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-md-4 payment-card">
            <h6 class="py-4 main-heading">Summary</h6>
            <?php if(isset($item_details) && !empty($item_details)){ ?>
            <div class="accordion book-service">
               <div class="card payment-card-text">
                  <h6><?php echo !empty($sp_info['company_name'])?$sp_info['company_name']:'N/A';?></h6>
                  <p class="pt-2"></p>
                  <table class="table for-dot mb-0">
                     <tbody>
                        <tr>
                           <td class="text-muted">Sub-Total</td>
                           <td class="text-gray"><?php echo ADMIN_CURRENCY.$total_price;?></td>
                        </tr>
                        <tr>
                           <td class="text-muted">Tax</td>
                           <td class="text-warning"><?php echo !empty($item_details[0]['tax'])?$item_details[0]['tax'].'%':'';?></td>
                        </tr>
                        <?php if(!empty($item_details[0]['discount'])){?>
                        <tr>
                           <td class="text-muted">Discount-<?php echo !empty($item_details[0]['discount'])?$item_details[0]['discount'].'% OFF':'';?></td>
                           <td class="text-gray"><?php echo $deduct_discount=ADMIN_CURRENCY.(($total_price*$item_details[0]['discount'])/100)?></td>
                        </tr>
                        <?php } ?>
                     </tbody>
                     <tfoot>
                        <tr>
                           <th class="text-primary">Total</th>
                           <th class="text-gray"><?php 
                           if(!empty($item_details[0]['discount'])){
                                 $discount=$item_details[0]['discount'];
                                 $discounted_sell_price=number_format((($total_price*$discount)/100),2,'.','');
                                 $discount_price=$total_price-$discounted_sell_price;
                                 if($item_details[0]['tax']!='0.00'){
                                    $tax=$item_details[0]['tax'];
                                    $tax_sell_price=number_format((($discount_price*$tax)/100),2,'.','');
                                    $sell_price=$discount_price+$tax_sell_price;
                                 }else{
                                    $sell_price=$discount_price;
                                 }
                            }else{
                                $sell_price=$total_price;
                                if($item_details[0]['tax']!='0.00'){
                                    $tax=$item_details[0]['tax'];
                                    $tax_sell_price=number_format((($total_price*$tax)/100),2,'.','');
                                    $sell_price=$total_price+$tax_sell_price;
                                }
                            }

                           echo ADMIN_CURRENCY.$sell_price;?></th>
                        </tr>
                        <tr>
                           <td colspan="2" class="text-center">
                              <a href="book-event-service" class="btn continue-step">Continue <i class="fa fa-long-arrow-right pl-2" aria-hidden="true"></i></a>
                           </td>
                        </tr>
                     </tfoot>
                  </table>
               </div>
            </div>
            <?php } ?>
         </div>
         
      </div>
   </div>
</section>
<script>
    function book_incrementQty(id,temp_id) {
        var value = document.querySelector('#qty_'+id).value;
        value = isNaN(value) ? 1 : value;
        value++;
        document.querySelector('#qty_'+id).value = value;
         update_items(value,temp_id);
    }
    function book_decrementQty(id,temp_id) {
        var value = document.querySelector('#qty_'+id).value;
        value = isNaN(value) ? 1 : value;
        value > 1 ? value-- : value;
        document.querySelector('#qty_'+id).value = value;
        if(value >=1){
            update_items(value,temp_id);
        }
        
    }

    function update_items(value,temp_id){
        $("#loader-wrapper").show();
       $.ajax({
           url: 'front/Explore/update_book_service',
           type: 'POST',
           data: {'qty':value,'temp_id':temp_id},
           dataType: 'json',
           success:function(resp){
                $("#loader-wrapper").hide();
                if(resp.status==0){
                    $(".message_box").show();
                    $(".message_box").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                    setTimeout(function() {
                        $(".message_box").html('');
                        $(".message_box").hide();
                    }, 5000); 
                }else{
                    location.reload();
                }
           },
           error:function(err){
             $("#loader-wrapper").hide();
           }
        });

    }
</script>