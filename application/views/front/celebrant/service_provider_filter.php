
   <div class="service-provider filters">
      <h4 class="heading">FILTERS</h4>
      <div class="card searchFilter mt-3">
         <h4 class="filter-heading">Search Within</h4>
         <div class="form-inline my-2 my-lg-0 posrel">
            <input class="form-control" type="search" placeholder="Search" id="name" value="<?php if(isset($_POST['keyword']) && !empty($_POST['keyword'])){ echo $_POST['keyword'];}?>" >
             <button type="button" onclick="search_product();"> <i class="fa fa-search search-icon" aria-hidden="true"></i></button>
         </div>
      </div>
      <div class="card selectFilter mt-4" >
     
         <h4 class="filter-heading">Country</h4>
         <form class="form-inline my-2 my-lg-0 posrel style-2" id="country_div">
            <?php 
                //this query added for temprary because of country resctricted to US, multi country managed in system by controller
                $countries=$this->Common_model->getRecords('countries', 'id,name', array('id'=>231), 'name', false);
                if(!empty($countries)){ 
                $country=0; 
               foreach($countries as $row){$country++; ?>
                  <div class="form-group" id="country_div_<?php echo $country;?>">
                     <input type="checkbox" id="<?php echo !empty($row['id'])?$row['id']:''; ?>" name="country_id" value="<?php echo !empty($row['id'])?$row['id']:''; ?>" onclick="search_product()" <?php
                         if(isset($_POST['country_id']) && !empty($_POST['country_id'])){
                              if(in_array($row['id'], $_POST['country_id']))
                                 {echo 'checked';}else{echo '';}
                         }?>>
                     <label for="<?php echo !empty($row['id'])?$row['id']:''; ?>"><?php echo !empty($row['name'])?$row['name']:''; ?></label>
                  </div>
            <?php } } ?>
            
         </form>
      </div>
      <div class="card selectFilter mt-4">
         <h4 class="filter-heading">State</h4>
         <form class="form-inline my-2 my-lg-0 posrel style-2" id="state_div">
            <div class="form-group" id="state_box">
               <?php 
                 //this query added for temprary because of country resctricted to US, multi country managed in system by controller
               $states=$this->Common_model->getRecords('states', 'id,name', array('country_id'=>231), 'name', false);
               if(!empty($states)){
                  $state=0;  
                  foreach($states as $row){ $state++;?>
                     <div class="form-group" id="state_div_<?php echo $state;?>">
                        <input type="checkbox" id="check<?php echo !empty($row['id'])?$row['id']:''; ?>" name="state_id" value="<?php echo !empty($row['id'])?$row['id']:''; ?>" onclick="search_product()" <?php
                         if(isset($_POST['state_id']) && !empty($_POST['state_id'])){
                              if(in_array($row['id'], $_POST['state_id']))
                                 {echo 'checked';}else{echo '';}
                         }?>>
                        <label for="check<?php echo !empty($row['id'])?$row['id']:''; ?>"><?php echo !empty($row['name'])?$row['name']:''; ?></label>
                     </div>
               <?php } }?>
            </div>
         </form>
      </div>
      <div class="card selectFilter mt-4">
         <h4 class="filter-heading">City</h4>
         <form class="form-inline my-2 my-lg-0 posrel style-2" id="city_div">
            <?php if(!empty($cities)){ 
                $city=0;
               foreach($cities as $row){ $city++;?>
                  <div class="form-group" id="city_div_<?php echo $city;?>">
                     <input type="checkbox" id="checking<?php echo !empty($row['id'])?$row['id']:''; ?>" name="city_id" value="<?php echo !empty($row['id'])?$row['id']:''; ?>" onclick="search_product()" <?php
                         if(isset($_POST['city_id']) && !empty($_POST['city_id'])){
                              if(in_array($row['id'], $_POST['city_id']))
                                 {echo 'checked';}else{echo '';}
                         }?>>
                     <label for="checking<?php echo !empty($row['id'])?$row['id']:''; ?>"><?php echo !empty($row['name'])?$row['name']:''; ?></label>
                  </div>
            <?php } }?>
         </form>
      </div>
      <?php if(!isset($filter_hide)){?>
      <div class="card selectFilter mt-4">
         <h4 class="filter-heading">Offer</h4>
         <form class="my-2 my-lg-0 posrel style-2">
            <label class="radio-container">Upto - 20%
            <input type="radio"  name="offer" value="0-20" onclick="search_product()" <?php
             if(isset($_POST['offer']) && !empty($_POST['offer'])){
                  if($_POST['offer']=='0-20')
                     {echo 'checked';}else{echo '';}
             }?>>
            <span class="checkmark"></span>
            </label>
            <label class="radio-container">20% - 40%
            <input type="radio" name="offer" value="20-40" onclick="search_product()" <?php
             if(isset($_POST['offer']) && !empty($_POST['offer'])){
                  if($_POST['offer']=='20-40')
                     {echo 'checked';}else{echo '';}
             }?> >
            <span class="checkmark"></span>
            </label>
            <label class="radio-container">40% - 60%
            <input type="radio" name="offer" value="40-60" onclick="search_product()" <?php
             if(isset($_POST['offer']) && !empty($_POST['offer'])){
                  if($_POST['offer']=='40-60')
                     {echo 'checked';}else{echo '';}
             }?> >
            <span class="checkmark"></span>
            </label>
            <label class="radio-container">60% - 80%
            <input type="radio" name="offer" value="60-80" onclick="search_product()" <?php
             if(isset($_POST['offer']) && !empty($_POST['offer'])){
                  if($_POST['offer']=='60-80')
                     {echo 'checked';}else{echo '';}
             }?>>
            <span class="checkmark"></span>
            </label>
         </form>
      </div>
       <div class="card priceFilter mt-4">
         <h4 class="filter-heading">Rating</h4>
         <div class="slidecontainer">
             <form>
               <div class="slider" >
                  <div class="range">
                    <input id="ex19" type="text"
                    data-provide="slider"
                    data-slider-ticks="[0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5]"
                    data-slider-ticks-labels='["0", "0.5", "1", "1.5" ,"2", "2.5", "3", "3.5", "4", "4.5", "5"]'
                    data-slider-min="0"
                    data-slider-max="5"
                    data-slider-step="0.5"
                    data-slider-value="<?php echo !empty($_POST['ratings'])? $_POST['ratings']:'0';?>"
                    data-slider-tooltip="show" class="slider_role form-control"  data-slider-id="blue" name="rating" id="rating" value=""/>
                  </div>
               </div>
            </form>
         </div>
      </div>
      <?php } ?>
   </div>

   <?php  
   if (isset($gender_hide)) {
   ?>
  <div class="card selectFilter mt-4">
    <h4 class="filter-heading">Gender</h4>
    <form class="my-2 my-lg-0 posrel style-2">
      <label class="radio-container">Male
        <input type="radio" name="gender" value="Male" onclick="search_product()" <?php  if(isset($_POST['gender']) && !empty($_POST['gender'])){ echo ($_POST['gender'] == 'Male') ? 'checked' : '' ; } ?> >
        <span class="checkmark"></span>
      </label>
      <label class="radio-container">Female
        <input type="radio" name="gender" value="Female" onclick="search_product()" <?php  if(isset($_POST['gender']) && !empty($_POST['gender'])){ echo ($_POST['gender'] == 'Female') ? 'checked' : '' ; } ?> >
        <span class="checkmark"></span>
      </label>
      <label class="radio-container">Others
        <input type="radio" name="gender" value="Other" onclick="search_product()" <?php  if(isset($_POST['gender']) && !empty($_POST['gender'])){ echo ($_POST['gender'] == 'Other') ? 'checked' : '' ; } ?> >
        <span class="checkmark"></span>
      </label>
    </form>
  </div>
   <?php  
   }
   if (isset($age_hide)) {
  ?>
  <div class="card selectFilter mt-4">
    <h4 class="filter-heading">Age</h4>
    <form class="my-2 my-lg-0 posrel style-2">
      <label class="radio-container">18-25
        <input type="radio"  name="age" value="18 AND 25" onclick="search_product()" <?php  if(isset($_POST['age']) && !empty($_POST['age'])){ echo ($_POST['age'] == '18 AND 25') ? 'checked' : '' ; } ?> >
        <span class="checkmark"></span>
      </label>
      <label class="radio-container">25-40
        <input type="radio" name="age" value="25 AND 40" onclick="search_product()" <?php  if(isset($_POST['age']) && !empty($_POST['age'])){ echo ($_POST['age'] == '25 AND 40') ? 'checked' : '' ; } ?> >
        <span class="checkmark"></span>
      </label>
      <label class="radio-container">40-60
        <input type="radio" name="age" value="40 AND 60" onclick="search_product()" <?php  if(isset($_POST['age']) && !empty($_POST['age'])){ echo ($_POST['age'] == '40 AND 60') ? 'checked' : '' ; } ?> >
        <span class="checkmark"></span>
      </label>
    </form>
  </div>
   <?php  
   }
   ?>

   <script>
   $('#name').keydown(function (e){
        if(e.keyCode == 13){
           search_product();
        }
    });
   <?php 
    if (!isset($filter_hide)) { ?>
      $('.slider_role').slider().on('slideStop', function(ev){
        var newVal = $('.slider_role').data('slider').getValue();
        $("#rating").val(newVal);
        search_product();
           
    });
    <?php  
    }
   ?>

    var c_n=0;
    $("input[name='country_id']").each(function(){
    c_n++;
        if ($(this).is(':checked')) {
            var el = document.getElementById("country_div_"+c_n+"");
            $("#country_div").animate({
                scrollTop: el.offsetTop
            });
            return false;
        }                
    });
    var s_n=0;
    $("input[name='state_id']").each(function(){
    s_n++;
        if ($(this).is(':checked')) {
            var el = document.getElementById("state_div_"+s_n+"");
            console.log(el);
            $("#state_div").animate({
                scrollTop: el.offsetTop
            });
            return false;
        }                
    });
    var ci_n=0;
    $("input[name='city_id']").each(function(){
    ci_n++;

        if ($(this).is(':checked')) {
            var c_el = document.getElementById("city_div_"+ci_n+"");
            $("#city_div").animate({
                scrollTop: c_el.offsetTop
            });
            return false;
        }                
    });
</script>
