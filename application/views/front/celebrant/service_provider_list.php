<section class="section-div pt-4 explore">
   <div class="container">
      <div class="row" id="page_box">
         <div class="col-md-4 no-background">
            <?php require_once('service_provider_filter.php'); ?>
            <div class="service-provider smilarEvents hide-res">
                <?php require_once('similar_events.php'); ?>
            </div>
            <?php include APPPATH.'views/front/include/play_store_link.php';  ?>
         </div>
         <div class="col-md-8">
            <div class="tab-container">
              <?php if ($this->session->flashdata('error')) { ?>
                  <div class="alert alert-danger">
                      <button data-dismiss="alert" class="close" type="button">×</button>
                      <?php echo $this->session->flashdata('error') ?>
                  </div>
                <?php } ?>
                <?php if ($this->session->flashdata('success')) { ?>
                  <div class="alert alert-success">
                      <button data-dismiss="alert" class="close" type="button">×</button>
                      <?php echo $this->session->flashdata('success') ?>
                  </div>
                <?php } ?>
               <ul class="nav nav-tabs tab-btn" role="tablist">
                  <li class="nav-item"> <a class="nav-link active" href="service-providers" >Service Provider</a>
                  </li>
                  <li class="nav-item"> <a class="nav-link" href="upcoming-events">Upcoming
                     Events</a>
                  </li>
               </ul>
               <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active" id="ServiceProvider">
                     <div class="flex">
                        <div class="content cards-listing-boxes">
                           <input type="hidden" id="service_cat" value="<?php if(!empty($service_category)) echo $service_category;?>">
                           <div class="row" id="sp_box">
                              
                           </div>
                        </div>
                     </div>
                     <div class="text-center pt-4"><a href="javascript:void(0)" id="loadMore">
                        <img class="spinner-icon" src="assets/front/images/home/icon/spinner-of-dots.svg" alt="">
                        <span>Load More</span></a>
                     </div>
                  </div>
                  <div role="tabpanel" class="tab-pane" id="UpcomingEvents">
                     <div class="flex">
                        <div class="content1 cards-listing-boxes">
                           <div class="row" id="events_box">
                              
                           </div>
                        </div>
                     </div>
                     <div class="text-center pt-4"> <a href="javascript:void(0)" id="loadMore2">Load More</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   </div>
</section>
<script>
   /* for load more post */
        $("#loader-wrapper").show();
        var totalrecords='<?php echo $total_records;?>';
        var track_page = 0;
         //track user scroll as page number, right now page number is 1
        load_contents(track_page);
        $(window).scroll(function() { //detect page scroll
            if($(window).scrollTop() + $(window).height() >= $(document).height()-600) { //if user scrolled to bottom of the page
                track_page++;
                var current_page=<?php echo FRONT_LIMIT; ?>*track_page;
               
                if(current_page<totalrecords){
                load_contents(track_page);
              }
            }
        }); 

        //Ajax load function
        function load_contents(track_page){

            $('#loader-wrapper').show(); 
            var check=$("#sp_box").html(); 
            var country_id=[];
            var state_id=[];
            var city_id=[];
            var offer;
            var ratings;
            var keyword;
            offer=$("input[name='offer']:checked").val();
            ratings=$("input[name='rating']").val();

            $.each($("input[name='country_id']:checked"), function(){  
                country_id.push($(this).val());
            }); 
            $.each($("input[name='state_id']:checked"), function(){  
                state_id.push($(this).val());
            }); 
            $.each($("input[name='city_id']:checked"), function(){  
                city_id.push($(this).val());
            }); 
            $.each($("input[name='country_id']:checked"), function(){  
                country_id.push($(this).val());
            }); 
            keyword=$("#name").val();
            service_cat=$("#service_cat").val();
            //var name=$("#name").val();
            $.ajax({
                type:'POST',
                data:{ 
                    country_id:country_id,state_id:state_id,city_id:city_id,offer:offer,ratings:ratings,keyword:keyword,service_cat:service_cat,
                    page:track_page,
                },
                url: "<?php echo base_url();?>front/Explore/manage_service_providers_data", 
               success:function(data) {
                    ajax_check_session(data,1);
                    if(data.trim().length == 0){
                      if(check.trim().length == 0){
                        $("#loadMore").text("No Content").addClass("noContent");
                        $('#loader-wrapper').hide();
                        return;
                      }
                    }
                    if(track_page==0){
                        $("#sp_box").html('');
                    }
                    $('#loader-wrapper').hide(); 
                    $("#sp_box").append(data);

                },error:function(data) {
                    $('#loader-wrapper').hide(); 
                }
            });
        }
        function search_product()
        {  
            $("#loader-wrapper").show();
            var check=$("#page_box").html(); 
            var country_id=[];
            var state_id=[];
            var city_id=[];
            var offer;
            var ratings;
            var keyword;
            offer=$("input[name='offer']:checked").val();
            ratings=$("input[name='rating']").val();
            $.each($("input[name='country_id']:checked"), function(){  
                country_id.push($(this).val());
            }); 
            $.each($("input[name='state_id']:checked"), function(){  
                state_id.push($(this).val());
            }); 
            $.each($("input[name='city_id']:checked"), function(){  
                city_id.push($(this).val());
            }); 
            keyword=$("#name").val();
            service_cat=$("#service_cat").val();
            $.ajax({
                type:'POST',
                url: "service-provider-filter",
                data: {country_id:country_id,state_id:state_id,city_id:city_id,offer:offer,ratings:ratings,keyword:keyword,service_cat:service_cat},
                
                success:function(data)
                {
                    ajax_check_session(data,1);
                   if(data.trim().length == 0){//alert();
                      if(check.trim().length == 0){
                        $("#loadMore").text("No Content").addClass("noContent");
                        $('#loader-wrapper').hide();
                        return;
                      }
                    }
                    if(track_page==0){
                        $("#page_box").html('');
                       // alert();
                    }
                    $("#page_box").html('');
                    $('#loader-wrapper').hide(); 
                    $("#page_box").append(data);
                }
            });
        }
    
     $(document).ready(function() {
       $(".content").slice(0, 4).show();
       $("#loadMore,#loadMore2").on("click", function(e) {
         e.preventDefault();
         $(".content:hidden").slice(0, 1).slideDown();
         if ($(".content:hidden").length == 0) {
           $("#loadMore").text("No Content").addClass("noContent");
         }
       });
       $(".content1").slice(0, 4).show();
       $("#loadMore2").on("click", function(e) {
         e.preventDefault();
         $(".content1:hidden").slice(0, 1).slideDown();
         if ($(".content1:hidden").length == 0) {
           $("#loadMore2").text("No Content").addClass("noContent");
         }
       });
     });
</script>

