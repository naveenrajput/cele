
   <h4 class="heading">Upcoming Events Near You</h4>
    <?php if(!empty($upcoming_events)){ 
        foreach($upcoming_events as $row){ ?>
       <div class="service">
          <div class="service-img">
             <img src="<?php if(!empty($row['image'])){ echo $row['image'];}else{ echo 'assets/images/phl_service_banner.png';}?>" alt="cake">
          </div>
          <div class="service-details">
             <h6 class="userName"><?php if(!empty($row['event_title'])){ echo $row['event_title'];}else{ echo 'N/A';}?></h6>
             <p><?php if(!empty($row['event_address'])){ echo $row['event_address'];}else{ echo 'N/A';}?></p>
             <a href="upcoming-events-detail/<?php if(!empty($row['event_id'])){ echo base64_encode($row['event_id']);}else{ echo 'N/A';}?>" class="btn btn-primary join-btn">View Detail</a>
             <!-- <a href="upcoming-events-detail/<>" >Join</a> -->
          </div>
       </div>
    <?php } }else{ echo 'No events available.';} ?>
