<section class="section-div pt-4 beMyDate-div">
	<div class="container">
		<div class="row">
			<div class="col-md-4 no-background">
				<div class="service-provider filters">
					<h4 class="heading">FILTERS</h4>
					<div class="card searchFilter mt-3">
						<h4 class="filter-heading">Search Within</h4>
						<form class="form-inline my-2 my-lg-0 posrel" action="invite-friend-list/<?php echo $event_id;?>" method="POST">
							<input class="form-control" name="name" value='<?php if(isset($_POST["name"]) && !empty($_POST["name"])){echo $_POST["name"];}?>' placeholder="Search" type="search" id="searchInput2">
							<button type="submit"><i class="fa fa-search"></i></button>
						</form>
					</div>
				</div>
				<?php include APPPATH.'views/front/include/play_store_link.php';  ?>
			</div>
			<div class="col-md-8">

				<h4 class="main-heading for-heading">Invite Friends</h4>
				<div class="message_box" style="display:none;"></div>
				<div class="row mt-4" id="friend_box">
				</div>
				<div class="text-center pt-4"><a href="javascript:void(0)" id="loadMore">
           			<img class="spinner-icon" src="assets/front/images/home/icon/spinner-of-dots.svg" alt="">
           			<span>Load More</span></a>
				</div>
				<input id ="event_id" type ="hidden" name="event_id" value="<?php if(!empty($event_id)){ echo $event_id;}?>">
			</div>
	</div>
</section>

<script type="text/javascript">
	$("#loader-wrapper").show();
        var totalrecords='<?php echo $total_records;?>';
        var track_page = 0;
         //track user scroll as page number, right now page number is 1
        load_contents(track_page);
        $(window).scroll(function() { //detect page scroll
            if($(window).scrollTop() + $(window).height() >= $(document).height()-600) { //if user scrolled to bottom of the page
                track_page++;
                var current_page=<?php echo FRONT_LIMIT; ?>*track_page;
               
                if(current_page<totalrecords){
                load_contents(track_page);
              }
            }
        }); 

        //Ajax load function
        function load_contents(track_page){

            $('#loader-wrapper').show(); 
            var check=$("#friend_box").html(); 
            var keyword='<?php if(isset($_POST["name"]) && !empty($_POST["name"])){echo $_POST["name"];}?>';
           
            var event_id=$("#event_id").val();
            $.ajax({
                type:'POST',
                data:{ 
                	event_id:event_id,
                    keyword:keyword,
                    page:track_page,
                },
                url: "<?php echo base_url();?>front/Booking/invite_friend_list_data", 
               success:function(data) {
                     ajax_check_session(data,1);
                    if(data.trim().length == 0){
                      if(check.trim().length == 0){
                        $("#loadMore").text("No Content").addClass("noContent");
                        $('#loader-wrapper').hide();
                        return;
                      }
                    }
                    if(track_page==0){
                        $("#friend_box").html('');
                    }
                    $('#loader-wrapper').hide(); 
                    $("#friend_box").append(data);
                },error:function(data) {
                    location.reload();
                }
            });
        }
       if ( window.history.replaceState ) {
		  window.history.replaceState( null, null, window.location.href );
		}
		
</script>