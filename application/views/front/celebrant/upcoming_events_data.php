<?php if(!empty($details)){
  //echo "<pre>";print_r($details);die;
    foreach($details as $row){ ?>
    <div class="col-xs-12 col-md-6  col-lg-6">
        <a href="upcoming-events-detail/<?php if(!empty($row['event_id'])){ echo base64_encode($row['event_id']);}else{ echo 'N/A';}?>">
            <div class="card">
               <div class="card-banner">
                  <img class="card-img-top" src="<?php if(!empty($row['image'])){ echo $row['image'];}else{ echo 'assets/images/phl_service_banner.png';}?>" alt="Card image cap"> 
               </div>
               <div class="card-body">
                  <h5 class="card-title"><?php if(!empty($row['event_title'])){ echo $row['event_title'];}else{ echo 'N/A';}?>
                  </h5>
                  <div class="ctn inner-profile-title">
                     <div class="user-profile">
                        <a href="profile/<?php echo !empty($row['user_id'])? base64_encode($row['user_id']):'N/A';?>"> <img src="assets/front/images/profile.png" alt="logo"></a>
                     </div>
                     <div class="post-text">
                        <div class="recentPostDetails">
                           <h6 class="userName"><?php if(!empty($row['fullname'])){ echo $row['fullname'];}else{ echo 'assets/images/phl_service_banner.png';}?></h6>
                        </div>
                     </div>
                  </div>
                  <div class="eventDetails">
                     <ul class="list-inline
                        mb-1">
                        <li class="list-inline-item">
                           <img src="assets/front/images/icons/event.svg" alt=""> 
                           <span><?php if(!empty($row['event_date'])){ echo convertGMTToLocalTimezone($row['event_date']);} else {echo 'N/A';}?></span>
                        </li>
                        <li class="list-inline-item"> 
                           <img src="assets/front/images/icons/clock.svg" alt=""> 
                           <span><?php if(!empty($row['event_date'])){ echo convertGMTToLocalTimezone($row['event_date'],'',true);} else {echo 'N/A';}?></span>
                        </li>
                     </ul>
                  </div>
                  <div class="eventVanue">
                     <div class="ev-icon">
                        <img src="assets/front/images/icons/address.svg" alt="">
                     </div>
                     <div class="address">
                        <p><?php if(!empty($row['event_address'])){ echo $row['event_address'];}else{ echo 'N/A';}?></p>
                     </div>
                  </div>
               </div>
            </div>
        </a>
    </div>
<?php } }  ?>                             