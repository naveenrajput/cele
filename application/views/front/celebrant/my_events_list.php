
<section class="section-div pt-4">
	<div class="container">
		<div class="row" id="page_box">
			<div class="col-md-4 no-background">
				<?php include APPPATH.'views/front/include/event_filter.php';  ?>
				<!-- <div class="service-provider not-50 hide-res">
					<h4 class="heading">UPCOMING EVENT NEAR YOU</h4>
					<div class="service singlePost">
						<div class="service-img full-width">
							<?php //include 'slider2.php'; ?>
						</div>
						<div class="service-details fWidth">
							<h6 class="userName">Andrey Kim</h6>
							<p>49 Female</p>
							<p class="address">724 S Mission St. Sapulpa, OK 74066, UK</p>
						</div>
					</div>
				</div> -->
				<div class="service-provider hiddenmobile hide-res">
					<?php include APPPATH.'views/front/include/play_store_link.php';  ?>
				</div>
			</div>
			<div class="col-md-8">
			   <div class="tab-container">
		      	<ul class="nav nav-tabs tab-btn" role="tablist">
			         <li class="nav-item"> <a class="nav-link active" href="my-events">My Event</a> </li>
			         <?php if($this->session->userdata('front_user_type')==3){ ?>
			         <li class="nav-item"> <a class="nav-link" href="csp-my-bookings">My Bookings</a> </li>
			         <?php } ?>
			         <li class="nav-item"> <a class="nav-link" href="public-events">Public Events</a> </li>
		      	</ul>
  			   	<div class="message_box" style="display:none;"></div>
  			   	<?php if ($this->session->flashdata('error')) { ?>
	                  <div class="alert alert-danger">
	                      <button data-dismiss="alert" class="close" type="button">×</button>
	                      <?php echo $this->session->flashdata('error') ?>
	                  </div>
	                <?php } ?>
	                <?php if ($this->session->flashdata('success')) { ?>
	                  <div class="alert alert-success">
	                      <button data-dismiss="alert" class="close" type="button">×</button>
	                      <?php echo $this->session->flashdata('success') ?>
	                  </div>
	                <?php } ?>
			      <div class="tab-content">
			         <div role="tabpanel" class="tab-pane active" id="event">
			            <div class="flex">
			               <div class="content cards-listing-boxes">
			                	<div class="row" id="my_events">
			                	</div>
			               </div>
			            </div>
			            <div class="text-center pt-4"><a href="javascript:void(0)" id="loadMore">
			               <img class="spinner-icon" src="assets/front/images/home/icon/spinner-of-dots.svg" alt="">
			               <span>Load More</span></a>
			            </div>
			         </div>
			      </div>
			   </div>
			   <div class="service-provider hidedesktop">
			  		<?php include APPPATH.'views/front/include/play_store_link.php';  ?>
			   </div>
			</div>

</section>


<script>
	/* for load more post */
	 $("#loader-wrapper").show();
            var totalrecords='<?php echo $total_records;?>';
            var track_page = 0;
             //track user scroll as page number, right now page number is 1
            load_contents(track_page);
            $(window).scroll(function() { //detect page scroll
                if($(window).scrollTop() + $(window).height() >= $(document).height()-600) { //if user scrolled to bottom of the page
                    track_page++;
                    var current_page=<?php echo FRONT_LIMIT; ?>*track_page;
                   
                    if(current_page<totalrecords){
                    load_contents(track_page);
                  }
                }
            }); 

        //Ajax load function
        function load_contents(track_page){

            $('#loader-wrapper').show(); 
            var check=$("#my_events").html(); 
            var event_status=[];
			$.each($("input[name='event_status']:checked"), function(){            
				event_status.push($(this).val());
			}); 
			var name=$("#name").val();
            $.ajax({
                type:'POST',
                data:{ 
                	name:name,event_status:event_status,
                    page:track_page,
                },
                url: "<?php echo base_url();?>front/Booking/my_events_data", 
               success:function(data) {
               		 ajax_check_session(data,1);
                    if(data.trim().length == 0){
                      if(check.trim().length == 0){
                        $("#loadMore").text("No Content").addClass("noContent");
                        $('#loader-wrapper').hide();
                        return;
                      }
                    }
                    if(track_page==0){
                        $("#my_events").html('');
                    }
                    $('#loader-wrapper').hide(); 
                    $("#my_events").append(data);

                }
            });
        }
		function search_product()
		{  
			$("#loader-wrapper").show();
			var check=$("#page_box").html(); 
			//$("#booking_id").html('');
			var event_status=[];
			$.each($("input[name='event_status']:checked"), function(){            
				event_status.push($(this).val());
			}); 
			var name=$("#name").val();
			$.ajax({
	            type:'POST',
	            url: "my-event-filter",
	            data: {name:name,event_status:event_status},
	            
	            success:function(data)
	            {
	            	ajax_check_session(data,1);
	               if(data.trim().length == 0){//alert();
                      if(check.trim().length == 0){
                        $("#loadMore").text("No Content").addClass("noContent");
                        $('#loader-wrapper').hide();
                        return;
                      }
                    }
                    if(track_page==0){
                        $("#page_box").html('');
                       // alert();
                    }
                    $("#page_box").html('');
                    $('#loader-wrapper').hide(); 
                    $("#page_box").append(data);
	            }
	        });
		}
		$(document).ready(function () {
		    $(".content").slice(0, 4).show();
		    $("#loadMore,#loadMore2").on("click", function (e) {
		      e.preventDefault();
		      $(".content:hidden").slice(0, 1).slideDown();
		      if ($(".content:hidden").length == 0) {
		        $("#loadMore").text("No Content").addClass("noContent");
		      }
		    });
		    $(".content1").slice(0, 4).show();
		    $("#loadMore2").on("click", function (e) {
		      e.preventDefault();
		      $(".content1:hidden").slice(0, 1).slideDown();
		      if ($(".content1:hidden").length == 0) {
		        $("#loadMore2").text("No Content").addClass("noContent");
		      }
		    });
  		});
</script>