<?php if(isset($friends) && !empty($friends)){ 
	$k=0;
	foreach ($friends as $row) { $k=md5($row['sent_by']);?>
	<div class="col-md-6">
		<a href="profile/<?php echo !empty($row['sent_by'])?base64_encode($row['sent_by']):'N/A';?>">
			<div class="single-request-block">
	            <div class="request-profile-image">
	                <img src="<?php echo !empty($booking_details['profile_picture'])?$booking_details['profile_picture']:'assets/front/images/profile0.png'?>" />
	            </div>
	            <div class="request-profile-option">
	                <h4 class="mb-1"><?php echo !empty($row['fullname'])?$row['fullname']:'N/A';?></h4>
	                
					<ul class="add-btnpos">
						<?php if(!empty($row['event_invitation_status'])){
								if($row['event_invitation_status']=='Pending'){
									$invite_btn='warning';
								}if($row['event_invitation_status']=='Accepted'){
									$invite_btn='success';
								}if($row['event_invitation_status']=='Rejected'){
									$invite_btn='danger';
								}?>
							<li><button  class="btn btn-<?php echo $invite_btn;?> join-btn"><?php echo $row['event_invitation_status'];?></button></li>
						<?php } else{ ?>
							<li id="<?php echo $k;?>"><a href="javascript:void(0);"  class="btn btn-primary join-btn" onclick="invite_friend('invite','<?php echo !empty($row['sent_by'])?base64_encode($row['sent_by']):'';?>','<?php echo $k;?>');">Invite</a></li>
						<?php } ?>
					</ul>
	            </div>
	        </div>
	    </a>
	</div>
<?php } } ?>
<script type="text/javascript">

function invite_friend(type,receiver_id,btn_id){
	
	var event_id=$("#event_id").val();
	swal({
        title: "Confirmation",
        text: "Are you sure you want to invite this user ?",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
        	$("#loader-wrapper").show(); 
            $.ajax({
                type: 'POST',
                url: "invite-friend",
                data: {request_type:type,event_id:event_id,receiver_id:receiver_id},
                dataType: 'json',
                success:function(resp){
                	$("#loader-wrapper").hide(); 
                    if(resp.status==0){
                    	$("html, body").animate({ scrollTop: 0 }, "slow");
	                    $(".message_box").html(resp.msg);
	                    $(".message_box").show();
                        $(".message_box").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                            setTimeout(function() {
                             $(".message_box").html('');
                             $(".message_box").hide();
                            }, 5000); 
                    }else{
                        $("#"+btn_id).html('<button  class="btn btn-warning join-btn">Pending</button>');   
                    }
                    
                }
            });
        }
    });
    
}
</script>