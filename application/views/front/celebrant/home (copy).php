<section class="section-div pt-4 home-section">
	<div class="container">
		<div class="row">
			<div class="col-lg-8">
				<div class="post-form profile-post-box">
					<div class="post-control">
						<div class="postForm">
							<div class="profile-container">
								<img src="assets/front/images/profile.png" alt="logo">
							</div>
							<textarea class="form-control post-details"
								placeholder="Hey! what are you upto? Celebrated recently? Let your friend know..."></textarea>
							<div class="image_container">
								<ul>
									<li>
										<a href="#"><i class="fa fa-close"></i></a>
										<img src="assets/front/images/home/gallery-img.jpg" alt="image">
									</li>
									<li>
										<a href="#"><i class="fa fa-close"></i></a>
										<img src="assets/front/images/birthday-1827714_1920.png" alt="image">
									</li>
									<li>
										<a href="#"><i class="fa fa-close"></i></a>
										<img src="assets/front/images/home/gallery-img.jpg" alt="image">
									</li>
								</ul>
							</div>
							<div class="postBtn">
								<ul class="upload-img-video">
									<li class="upload-img-btn">
										<input type="file" class="float-left upload" placeholder="Upload image"
											id="fileUpload" />
									</li>
									<li class="upload-video-btn">
										<input type="file" class="float-left upload" placeholder="Upload video"
											id="fileUpload" />
									</li>
								</ul>
								<button class="btn btn-primary float-right btn-post"><i class="fa fa-paper-plane-o pl-1"
										aria-hidden="true"></i>
								</button>
							</div>
						</div>
					</div>
				</div>
				<div class="flex">
					<div class="content">
						<div class="card content post-form mt-2 card-display-block">
							<div class="image-video-container">
								<div class="card-img-list" data-toggle="modal" data-target="#myModal">
									<div class="cardbox-8">
										<img src="assets/front/images/home/gallery-img.jpg" alt="">
									</div>
									<div class="cardbox-4">
										<ul>
											<li>
												<img src="assets/front/images/birthday-cake-380178_1920.png" alt="">
											</li>
											<li style="position: relative;">
												<img src="assets/front/images/birthday-1827714_1920.png" alt="">
												<div class="more-count">
													+ 1
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="modal" id="myModal">
									<div class="modal-dialog">
										<div class="modal-content">
											<!-- Modal body -->
											<div class="modal-body p-0">
												<button type="button" class="close"
													data-dismiss="modal">&times;</button>
												<div id="demo2" class="carousel slide" data-ride="carousel"
													data-interval="false">

													<!-- Indicators -->
													<ul class="carousel-indicators">
														<li data-target="#demo2" data-slide-to="0" class="active"></li>
														<li data-target="#demo2" data-slide-to="1"></li>
														<li data-target="#demo2" data-slide-to="2"></li>
														<li data-target="#demo2" data-slide-to="3"></li>
													</ul>

													<!-- The slideshow -->
													<div class="carousel-inner" style=" width:100%;">
														<div class="carousel-item">
															<video autoplay controls loop>
																<source src="assets/front/images/video/video1.mp4"
																	type="video/mp4">
																Your browser does not support the video tag.
															</video>
														</div>
														<div class="carousel-item active">
															<img src="assets/front/images/home/gallery-img.jpg"
																alt="Los Angeles">
														</div>
														<div class="carousel-item">
															<img src="assets/front/images/birthday-cake-380178_1920.png"
																alt="Chicago">
														</div>
														<div class="carousel-item">
															<img src="assets/front/images/birthday-1827714_1920.png"
																alt="New York">
														</div>
													</div>

													<!-- Left and right controls -->
													<a class="carousel-control-prev" href="#demo2" data-slide="prev">
														<span class="carousel-control-prev-icon"></span>
													</a>
													<a class="carousel-control-next" href="#demo2" data-slide="next">
														<span class="carousel-control-next-icon"></span>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="main-post-title">
								<div class="profile-user-img">
									<img src="assets/front/images/home/96d99888663a0c871e459e1c687b92de-1@2x.png"
										alt="logo">
								</div>
								<div></div>
							</div>
							<div class="post-control">
								<div class="profile-content-box">
									<span class="sec-1">
										<h6 class="profile-userName">Susan Sharonson <span>- New Jersey, US State</span>
										</h6>
										<small>1 d ago</small>
									</span>

									<div class="latest-sec-2">
										<div class="dropdown">
											<a href="javscript:void(0);" class="zigzag">
												<img src="assets/front/images/option.svg" alt="like">
											</a>
											<div class="dropmenuContainer1">
												<div class="dropdown-menu1">
													<a class="dropdown-item" href="#"><img
															src="assets/front/images/form_icons/ic_remove_connection.png"
															alt="logo" /> Delete Post</a>
													<a class="dropdown-item" href="#">
														<img src="assets/front/images/form_icons/ic_hide_post.png"
															alt="logo" /> Hide Post</a>
													<a class="dropdown-item" href="#">
														<img src="assets/front/images/form_icons/ic_save_post.png"
															alt="logo" /> Save Post</a>
													<a class="dropdown-item" href="#">
														<img src="assets/front/images/form_icons/ic_report.png"
															alt="logo" /> Report Post</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="detailsPostContent">
									<p>New Jersey is a northeastern U.S. state with <a href="#">#Partynight</a> some 130
										miles of Atlantic coast. Jersey City, across the Hudson River from Lower
										Manhattan,</p>
								</div>
								<div class="likeCommentSection">
									<div class="LCSsection">
										<ul class="list-inline">
											<li class="list-inline-item likes-count">
												<a class="heartncount" href="javascript:void(0)">
													<img src="assets/front/images/home/icon/heart.svg" alt="like"> <span
														ml-1>168</span>
												</a>
												<ul class="user-profile-list">
													<li>
														<img src="assets/front/images/home/profile/96d99888663a0c871e459e1c687b92de-1.png"
															alt="">
													</li>
													<li>
														<img src="assets/front/images/home/profile/96d99888663a0c871e459e1c687b92de-2.png"
															alt="">
													</li>
													<li>
														<img src="assets/front/images/home/profile/96d99888663a0c871e459e1c687b92de-3.png"
															alt="">
													</li>
												</ul>
											</li>
											<li class="list-inline-item comment-count-text">
												<a href="javascript:void(0)">
													<img src="assets/front/images/home/icon/big-speech-balloon.svg"
														alt="like"> <span ml-1>59</span>
												</a>
											</li>
											<li class="list-inline-item share-count">
												<a href="javascript:void(0)">
													<img src="assets/front/images/home/icon/share.svg" alt="like"> <span
														ml-1>59</span>
												</a>
											</li>
										</ul>
									</div>
								</div>
								<div class="comment-count">
									<a href="javascript:void(0)"> <span ml-1> 59 Comments</span>
									</a>
								</div>
								<div class="commnetSection">
									<div class="user-profile">
										<img src="assets/front/images/profile.png" alt="logo">
									</div>
									<div class="post-text">
										<div class="recentComment">
											<span class="sec-1">
												<h6 class="userName commentUser">Eden Events</h6>
											</span>
											<div class="latest-sec-2">

												<div class="dropdown">
													<a href="javscript:void(0);" class="zigzag">
														<img src="assets/front/images/option.svg" alt="like">
													</a>
													<div class="dropmenuContainer1">
														<div class="dropdown-menu1">
															<a class="dropdown-item" href="#"><img
																	src="assets/front/images/form_icons/ic_remove_connection.png"
																	alt="logo" /> Remove Comment</a>
															<a class="dropdown-item" href="#"><img
																	src="assets/front/images/form_icons/ic_hide_post.png"
																	alt="logo" /> Report Post</a>
															<a class="dropdown-item" href="#"><img
																	src="assets/front/images/form_icons/ic_report.png"
																	alt="logo" /> Report Post</a>
														</div>
													</div>
												</div>

											</div>
											<p class="singleComment">Lorem ipsum dolor sit amet, consectetur adipiscing
												elit. Sed augue justo, varius vel lobortis ac, volutpat in felis.
												Phasellus euismod dui ipsum, id interdum est vulputate quis.</p>
											<ul class="list-inline">
												<li class="list-inline-item">
													<a href="javascript:void(0)">
														<img src="assets/front/images/favorite.svg" alt="like"> <span
															ml-1>Like</span>
													</a>
												</li>
												<li class="list-inline-item">
													<a href="javascript:void(0)">
														<img src="assets/front/images/reply.svg" alt="like"> <span
															ml-1>Reply</span>
													</a>
												</li>
											</ul>
										</div>
										<ul class="list-inline">
											<div class="user-profile">
												<img src="assets/front/images/profile.png" alt="logo">
											</div>
											<div class="post-text">
												<div class="recentComment">
													<span class="sec-1">
														<h6 class="userName commentUser">Eden Events</h6>
													</span>
													<p class="singleComment">Lorem ipsum dolor sit amet, consectetur
														adipiscing elit. Sed augue justo, varius vel lobortis ac,
														volutpat in felis. Phasellus euismod dui ipsum, id interdum
														est vulputate quis.</p>
													<ul class="list-inline">
														<li class="list-inline-item">
															<a href="javascript:void(0)">
																<img src="assets/front/images/favorite.svg" alt="like"> <span
																	ml-1>Like</span>
															</a>
														</li>
														<li class="list-inline-item">
															<a href="javascript:void(0)">
																<img src="assets/front/images/reply.svg" alt="like"> <span
																	ml-1>Reply</span>
															</a>
														</li>
													</ul>
													<div class="commnetSection2 inner-input">
														<div class="post-text post-text2">
															<div class="recentComment">
																<div class="recomment">
																	<input type="text" class="form-control post-title"
																		placeholder="Write a comment...">
																	<button class="send-icon"></button>
																</div>
															</div>
														</div>
													</div>	
												</div>
											</div>
										</ul>
									</div>
								</div>
								<div class="commnetSection2">
									<div class="user-profile">
										<img src="assets/front/images/profile.png" alt="logo">
									</div>
									<div class="post-text post-text2">
										<div class="recentComment">
											<div class="recomment">
												<input type="text" class="form-control post-title"
													placeholder="Write a comment...">
												<button class="send-icon"></button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card content post-form mt-2 card-display-block">
							<div class="image-video-container">
								<div class="card-img-list" data-toggle="modal" data-target="#myModal">
									<div class="cardbox-8">
										<div class="video-overlay-style">
											<a class="video-icon" href="#"><img
													src="assets/front/images/home/icon/movie-player-play.svg"
													alt=""></a>
										</div>
										<img src="assets/front/images/home/gallery-img1.jpg" alt="">
									</div>
									<div class="cardbox-4">
										<ul>
											<li>
												<img src="assets/front/images/birthday-cake-380178_1920.png" alt="">
											</li>
											<li style="position: relative;">
												<img src="assets/front/images/birthday-1827714_1920.png" alt="">
												<div class="more-count">
													+ 1
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="modal" id="myModal">
									<div class="modal-dialog">
										<div class="modal-content">
											<!-- Modal body -->
											<div class="modal-body p-0">
												<button type="button" class="close"
													data-dismiss="modal">&times;</button>
												<div id="demo2" class="carousel slide" data-ride="carousel"
													data-interval="false">

													<!-- Indicators -->
													<ul class="carousel-indicators">
														<li data-target="#demo2" data-slide-to="0" class="active"></li>
														<li data-target="#demo2" data-slide-to="1"></li>
														<li data-target="#demo2" data-slide-to="2"></li>
														<li data-target="#demo2" data-slide-to="3"></li>
													</ul>

													<!-- The slideshow -->
													<div class="carousel-inner" style=" width:100%;">
														<div class="carousel-item">
															<video autoplay controls loop>
																<source src="assets/front/images/video/video1.mp4"
																	type="video/mp4">
																Your browser does not support the video tag.
															</video>
														</div>
														<div class="carousel-item active">
															<img src="assets/front/images/home/gallery-img.jpg"
																alt="Los Angeles">
														</div>
														<div class="carousel-item">
															<img src="assets/front/images/birthday-cake-380178_1920.png"
																alt="Chicago">
														</div>
														<div class="carousel-item">
															<img src="assets/front/images/birthday-1827714_1920.png"
																alt="New York">
														</div>
													</div>

													<!-- Left and right controls -->
													<a class="carousel-control-prev" href="#demo2" data-slide="prev">
														<span class="carousel-control-prev-icon"></span>
													</a>
													<a class="carousel-control-next" href="#demo2" data-slide="next">
														<span class="carousel-control-next-icon"></span>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="main-post-title">
								<div class="profile-user-img">
									<img src="assets/front/images/home/96d99888663a0c871e459e1c687b92de-1@2x.png"
										alt="logo">
								</div>
								<div></div>
							</div>
							<div class="post-control">
								<div class="profile-content-box">
									<span class="sec-1">
										<h6 class="profile-userName">Susan Sharonson <span>- New Jersey, US State</span>
										</h6>
										<small>1 d ago</small>
									</span>

									<div class="latest-sec-2">
										<div class="dropdown">
											<a href="javscript:void(0);" class="zigzag">
												<img src="assets/front/images/option.svg" alt="like">
											</a>
											<div class="dropmenuContainer1">
												<div class="dropdown-menu1">
													<a class="dropdown-item" href="#"><img
															src="assets/front/images/form_icons/ic_remove_connection.png"
															alt="logo" /> Delete Post</a>
													<a class="dropdown-item" href="#">
														<img src="assets/front/images/form_icons/ic_hide_post.png"
															alt="logo" /> Hide Post</a>
													<a class="dropdown-item" href="#">
														<img src="assets/front/images/form_icons/ic_save_post.png"
															alt="logo" /> Save Post</a>
													<a class="dropdown-item" href="#">
														<img src="assets/front/images/form_icons/ic_report.png"
															alt="logo" /> Report Post</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="detailsPostContent">
									<p>New Jersey is a northeastern U.S. state with <a href="#">#Partynight</a> some 130
										miles of Atlantic coast. Jersey City, across the Hudson River from Lower
										Manhattan,</p>
								</div>
								<div class="likeCommentSection">
									<div class="LCSsection">
										<ul class="list-inline">
											<li class="list-inline-item likes-count">
												<a class="heartncount" href="javascript:void(0)">
													<img src="assets/front/images/home/icon/heart.svg" alt="like"> <span
														ml-1>168</span>
												</a>
												<ul class="user-profile-list">
													<li>
														<img src="assets/front/images/home/profile/96d99888663a0c871e459e1c687b92de-1.png"
															alt="">
													</li>
													<li>
														<img src="assets/front/images/home/profile/96d99888663a0c871e459e1c687b92de-2.png"
															alt="">
													</li>
													<li>
														<img src="assets/front/images/home/profile/96d99888663a0c871e459e1c687b92de-3.png"
															alt="">
													</li>
												</ul>
											</li>
											<li class="list-inline-item comment-count-text">
												<a href="javascript:void(0)">
													<img src="assets/front/images/home/icon/big-speech-balloon.svg"
														alt="like"> <span ml-1>59</span>
												</a>
											</li>
											<li class="list-inline-item share-count">
												<a href="javascript:void(0)">
													<img src="assets/front/images/home/icon/share.svg" alt="like"> <span
														ml-1>59</span>
												</a>
											</li>
										</ul>
									</div>
								</div>
								<div class="comment-count">
									<a href="javascript:void(0)"> <span ml-1> 59 Comments</span>
									</a>
								</div>
								<div class="commnetSection">
									<div class="user-profile">
										<img src="assets/front/images/profile.png" alt="logo">
									</div>
									<div class="post-text">
										<div class="recentComment">
											<span class="sec-1">
												<h6 class="userName commentUser">Eden Events</h6>
											</span>
											<div class="latest-sec-2">

												<div class="dropdown">
													<a href="javscript:void(0);" class="zigzag">
														<img src="assets/front/images/option.svg" alt="like">
													</a>
													<div class="dropmenuContainer1">
														<div class="dropdown-menu1">
															<a class="dropdown-item" href="#"><img
																	src="assets/front/images/form_icons/ic_remove_connection.png"
																	alt="logo" /> Remove Comment</a>
															<a class="dropdown-item" href="#"><img
																	src="assets/front/images/form_icons/ic_hide_post.png"
																	alt="logo" /> Report Post</a>
															<a class="dropdown-item" href="#"><img
																	src="assets/front/images/form_icons/ic_report.png"
																	alt="logo" /> Report Post</a>
														</div>
													</div>
												</div>

											</div>
											<p class="singleComment">Lorem ipsum dolor sit amet, consectetur adipiscing
												elit. Sed augue justo, varius vel lobortis ac, volutpat in felis.
												Phasellus euismod dui ipsum, id interdum est vulputate quis.</p>
											<ul class="list-inline">
												<li class="list-inline-item">
													<a href="javascript:void(0)">
														<img src="assets/front/images/favorite.svg" alt="like"> <span
															ml-1>Like</span>
													</a>
												</li>
												<li class="list-inline-item">
													<a href="javascript:void(0)">
														<img src="assets/front/images/reply.svg" alt="like"> <span
															ml-1>Reply</span>
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="commnetSection2">
									<div class="user-profile">
										<img src="assets/front/images/profile.png" alt="logo">
									</div>
									<div class="post-text post-text2">
										<div class="recentComment">
											<div class="recomment">
												<input type="text" class="form-control post-title"
													placeholder="Write a comment...">
												<button class="send-icon"></button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="text-center pt-4"><a href="javascript:void(0)" id="loadMore">
						<img class="spinner-icon" src="assets/front/images/home/icon/spinner-of-dots.svg" alt="">
						<span>Load More</span></a>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="service-provider half-block pt-0">
					<h4 class="heading">TOP-RATED SERVICE PROVIDERS NEAR YOU</h4>
					<div class="service">
						<a href="javascript:void(0);">
							<div class="service-img">
								<img src="assets/front/images/event.png" alt="cake">
							</div>
							<div class="service-details">
								<h6 class="userName">Eden Events</h6>
								<p>@Petery Cruiser</p>
								<ul class="list-inline review-rating">
									<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
									</li>
									<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
									</li>
									<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
									</li>
									<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
									</li>
									<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
									</li>
								</ul>
							</div>
						</a>
					</div>
					<div class="service">
						<a href="javascript:void(0);">
							<div class="service-img">
								<img src="assets/front/images/event.png" alt="cake">
							</div>
							<div class="service-details">
								<h6 class="userName">Eden Events</h6>
								<p>@Petery Cruiser</p>
								<ul class="list-inline review-rating">
									<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
									</li>
									<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
									</li>
									<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
									</li>
									<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
									</li>
									<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
									</li>
								</ul>
							</div>
						</a>
					</div>
					<div class="service">
						<a href="javascript:void(0);">
							<div class="service-img">
								<img src="assets/front/images/event.png" alt="cake">
							</div>
							<div class="service-details">
								<h6 class="userName">Eden Events</h6>
								<p>@Petery Cruiser</p>
								<ul class="list-inline review-rating">
									<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
									</li>
									<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
									</li>
									<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
									</li>
									<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
									</li>
									<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
									</li>
								</ul>
							</div>
						</a>
					</div>
					<div class="service">
						<a href="javascript:void(0);">
							<div class="service-img">
								<img src="assets/front/images/event.png" alt="cake">
							</div>
							<div class="service-details">
								<h6 class="userName">Eden Events</h6>
								<p>@Petery Cruiser</p>
								<ul class="list-inline review-rating">
									<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
									</li>
									<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
									</li>
									<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
									</li>
									<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
									</li>
									<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
									</li>
								</ul>
							</div>
						</a>
					</div>
					<div class="service">
						<a href="javascript:void(0);">
							<div class="service-img">
								<img src="assets/front/images/event.png" alt="cake">
							</div>
							<div class="service-details">
								<h6 class="userName">Eden Events</h6>
								<p>@Petery Cruiser</p>
								<ul class="list-inline review-rating">
									<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
									</li>
									<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
									</li>
									<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
									</li>
									<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
									</li>
									<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
									</li>
								</ul>
							</div>
					</div>
					</a>
				</div>
				<div class="service-provider not-50">
					<h4 class="heading">UPCOMING EVENT NEAR YOU</h4>
					<div class="service singlePost">
						<div class="service-img full-width">

							<?php include APPPATH.'views/front/include/slider2.php'; ?>
						</div>
						<div class="service-details fWidth">
							<h6 class="userName">Iconic SET 90 Events</h6>
							<p>Kimmi Cruiser</p>
							<p class="address">725 S Mission St, Sapulpa, OK 74066, United States</p>
						</div>
					</div>
				</div>
				<div class="service-provider">
					<h4 class="heading">BE MY DATE</h4>
					<div class="service singlePost">
						<a href="javascript:void(0);">
							<div class="service-img service-img2">
								<img src="assets/front/images/bemy.png" alt="cake">
							</div>
							<div class="service-details service-details2">
								<h6 class="userName">Andrey Kim</h6>
								<p>Sapulpa, OK 74066, UK</p>
							</div>
						</a>
					</div>
					<div class="service singlePost">
						<a href="javascript:void(0);">
							<div class="service-img service-img2">
								<img src="assets/front/images/bemy.png" alt="cake">
							</div>
							<div class="service-details service-details2">
								<h6 class="userName">Andrey Kim</h6>
								<p>Sapulpa, OK 74066, UK</p>
							</div>
						</a>
					</div>
					<div class="service singlePost">
						<a href="javascript:void(0);">
							<div class="service-img service-img2">
								<img src="assets/front/images/bemy.png" alt="cake">
							</div>
							<div class="service-details service-details2">
								<h6 class="userName">Andrey Kim</h6>
								<p>Sapulpa, OK 74066, UK</p>
							</div>
						</a>
					</div>
				</div>
				<div class="service-provider">
					<div class="service service-f">
						<div class="service-img cele-logo">
							<img src="assets/front/images/Group8.png" alt="cake">
						</div>
						<div class="service-details">
							<h6 class="userName">Cele Mobile app</h6>
							<p>Download Cele App.It's free,it's fast and has amazing features.</p>
							<ul class="list-inline app-store mt-2">
								<li class="list-inline-item">
									<a href="https://play.google.com" target="_blank">
										<img src="assets/front/images/google-pay.png" alt="cake">
									</a>
								</li>
								<li class="list-inline-item">
									<a href="https://www.apple.com/itunes/" target="_blank">
										<img src="assets/front/images/app-store.png" alt="cake">
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	/* for load more post */

	$(document).ready(function () {
		$(".content").slice(0, 4).show();
		$("#loadMore").on("click", function (e) {
			e.preventDefault();
			$(".content:hidden").slice(0, 2).slideDown();
			if ($(".content:hidden").length == 0) {
				$("#loadMore").text("No Content").addClass("noContent");
			}
		});
	});

	/**********************/
</script>