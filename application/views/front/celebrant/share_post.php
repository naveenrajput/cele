<div class="modal" id="share_post_modal">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Share Post</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="edit_message_box" style="display:none;"></div>
        <div class="card add-service-card forpop">
          <div class="tab-content" id="singleService">
            <div class="tab-pane show active" id="tab1" role="tabpanel" aria-labelledby="tab1">
              <div class="accordion" id="accordion-tab-1">
                <div class="show" id="accordion-tab-1-content-1">
                  <div class="card-body pd-0">
                   <form class="add-service-form" action="" id="share_post_form" enctype="multipart/form-data" data-parsley-validate>
                        <textarea class="form-control post-details" placeholder="Hey! what are you upto? Celebrated recently? Let your friend know..." name="post_content" maxlength="1500" id="edit_post_content"></textarea>
                        <div>
                            <?php if(!empty($details['post_content'])){ ?>
                                 <p>
                                    <?php $po_content=explode('^', $details['post_content']); echo $po_content[0];?>
                                 </p>
                                 <?php if(isset($po_content[1]) && !empty($po_content[1])) { ?>
                                     <div class="functions-list"><span><?php if(!empty($po_content[1])) echo convertGMTToLocalTimezone($po_content[1]);?>  | <?php if(!empty($po_content[1])) echo convertGMTToLocalTimezone($po_content[1],'',true);?></span>
                                    </div>
                                <?php } 
                            } ?>
                        </div>
                        <div class="image_container">
                            <?php if(isset($media) && !empty($media)){ ?>
                                <ul class="preview_box">
                                   <?php foreach ($media as $key => $value) { 
                                    if($value['media_type']=='image')
                                        {$post_img=$value['media_path'];}
                                    else{ $post_img=$value['video_thumbnail'];}  ?>
                                    <li><img src="<?php echo !empty($post_img)?$post_img:'assets/images/phl_service_banner.png';?>" alt=""></li>
                                    <?php } ?>
                                </ul>
                            <?php }?>
                        </div>
                        <button class="btn btn-primary float-right btn-post" id ="share_post_btn" type="button" onclick="share_post_submit()">Share <i class="fa fa-paper-plane-o pl-1" aria-hidden="true"></i></button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() { 
    $('#share_post_modal').modal('show');
});


function share_post_submit(){
    
    $("#loader-wrapper").show();
    $("#share_post_btn").attr('disabled',true);
    var form = $('#share_post_form')[0];
    var formData = new FormData(form);
    var post_number="<?php echo !empty($details['post_number'])?base64_encode($details['post_number']):'N/A';?>";
    formData.append('post_num', post_number);
    $.ajax({
    url: 'share-post',
    type: 'POST',
    data: formData,
    dataType: 'json',
    // async: false,
    cache: false,
    contentType: false,
    processData: false,

    success:function(resp){
      $("#loader-wrapper").hide();
      $(".edit_message_box").show(); 
      $("#share_post_btn").attr('disabled',false);
        if(resp.status==0){
            $(".edit_message_box").html('<div class="alert alert-danger">'+resp.msg+'</div>');
            setTimeout(function() {
             $(".edit_message_box").html('');
              $(".edit_message_box").hide();
            }, 5000); 
          }else{
            $(".edit_message_box").html( '<div class="alert alert-success">'+resp.msg+'</div>');
            setTimeout(function() { 
             $(".edit_message_box").html('');
             $(".edit_message_box").hide();
             location.reload();
            }, 2000); 
        }
    },
    error:function(err){
      $("#share_post_btn").attr('disabled',false);
      $("#loader-wrapper").hide();
    }
  });
}
</script>