<section class="section-div pt-4 home-section">
	<div class="container">
		<div class="row">
			<div class="col-lg-8">
				<div class="message_box" style="display:none;"></div>
				<div class="category-box">
					<h6>What are you celebrating today?</h6>
					<div class="list">
						
						<?php if(!empty($top_sp)) { ?>
							<ul class="inline-list"> 
								<?php foreach($top_sp as $top_service_providers) { ?>
									<li class="list-inline-item">
										
										<a href="service-provider-detail/<?php echo !empty($top_service_providers['user_id'])?base64_encode($top_service_providers['user_id']):'N/A';?>" class="badge badge-pill badge-primary"><?php if(!empty($top_service_providers['company_name']))echo $top_service_providers['company_name'];?></a>

									</li>
								<?php } ?>
							</ul>
						<?php } ?>
					</div>
				</div>
				<div class="post-form profile-post-box">
					<div class="post-control">
						<div class="postForm">
							<div class="profile-container">
								<img src="<?php echo getProfilePicture(); ?>" alt="logo">
							</div>
							<form class="add-service-form" action="" id="post_form" enctype="multipart/form-data" data-parsley-validate>
								<textarea class="form-control post-details"
									placeholder="Hey! what are you upto? Celebrated recently? Let your friend know..." name="post_content" maxlength="1500" id="post_content" oninput ="post_btn_action()"></textarea>
								<div class="clearfix"></div>
								<div class="image_container">
									<ul class="preview_box">
										<?php for($i=1;$i<=5;$i++){ ?>
										<li class="upload-img-btn">
											<div class="upload-img-wraper">
												
												<img src="assets/front/images/add_image_placeholder.png"  id="img_src_<?php echo $i;?>"><span id="span_<?php echo $i;?>" onclick="remove_image('<?php echo $i;?>');"></span>
												<input type="file" name="file_upload[]" id="fileUpload_<?php echo $i;?>" onchange=" renderImage('<?php echo $i;?>');"/>
												<span class="only_firefox"> UPLOAD</span>
											</div>
										</li>
										<?php } ?>
									</ul>
								</div>
								<div class="postBtn">
									<!-- <ul class="upload-img-video">
										
									</ul> -->
									<button class="btn btn-primary float-right btn-post" id ="post_btn" type="button" onclick="post_submit()"  disabled="true"><i class="fa fa-paper-plane-o pl-1" aria-hidden="true"></i>
									</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="flex">
					<div class="content" id="feed_box">
						
					</div>
				</div>
				<div class="text-center pt-4"><a href="javascript:void(0)" id="loadMore">
						<img class="spinner-icon" src="assets/front/images/home/icon/spinner-of-dots.svg" alt="">
						<span>Load More</span></a>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="service-provider half-block pt-0">
					<h4 class="heading">TOP-RATED SERVICE PROVIDERS NEAR YOU</h4>
					<?php if(isset($top_sp) && !empty($top_sp)){ 
							$tp_sp=0;
							foreach($top_sp as $sp_details){$tp_sp++; ?>
								<div class="service">
									<a href="service-provider-detail/<?php echo !empty($sp_details['user_id'])?base64_encode($sp_details['user_id']):'N/A';?>">
										<div class="service-img">
											<img src="<?php
											 if(!empty($sp_details['img1'])){ 
											 	echo $sp_details['img1'];
												}else{ echo 'assets/images/phl_service_banner.png';
											 	} ?>" alt="Event Image">
										</div>
									</a>
									<div class="service-details">
										<a href="service-provider-detail/<?php echo !empty($sp_details['user_id'])?base64_encode($sp_details['user_id']):'N/A';?>">
											<h6 class="userName"><?php if(!empty($sp_details['company_name'])){ echo ucfirst($sp_details['company_name']);} else {echo 'N/A';}?></h6>
										</a>
											<p>
											<a href="service-provider-detail/<?php echo !empty($sp_details['user_id'])?base64_encode($sp_details['user_id']):'N/A';?>" class="sp_link">
											<?php if(!empty($sp_details['fullname'])){ echo ucfirst($sp_details['fullname']);} else {echo 'N/A';}?>
											</a>
												<?php 
						                            if(checkFollowStatus(!empty($sp_details['user_id'])?base64_encode($sp_details['user_id']):'N/A')){
						                                    $f_cls='btn btn-danger join-btn unfollow';
						                                    $f_text='Un-Join';
						                            }else{
						                                    $f_cls='btn btn-primary join-btn follow';
						                                    $f_text='join';
						                        }?>
						                        <span class="dot"></span>
												<button  onclick="follow_user('<?php echo !empty($sp_details['user_id'])?base64_encode($sp_details['user_id']):'N/A';?>','<?php echo $tp_sp;?>');" class="<?php echo $f_cls;?>" id="follow_btn_<?php echo $tp_sp;?>" type="button"><?php echo $f_text;?></button>
											</p>
										
										
										<ul class="list-inline review-rating">
											<?php if(!empty($sp_details['rating'])){ 
												if($sp_details['rating']=='0.0'){
												echo '<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-starfa fa-star-o" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
													</li>';
												}if($sp_details['rating']=='0.5'){
												echo '<li class="list-inline-item"><i class="fa fa-star-half-o" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-starfa fa-star-o" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
													</li>';
												}if($sp_details['rating']=='1'){
												echo '<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-starfa fa-star-o" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
													</li>';
												}if($sp_details['rating']=='1.5'){
												echo '<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-half-o" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-starfa fa-star-o" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
													</li>';
												}if($sp_details['rating']=='2'){
												echo '<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
													</li>';
												}if($sp_details['rating']=='2.5'){
												echo '<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-half-o" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
													</li>';
												}if($sp_details['rating']=='3'){
												echo '<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
													</li>';
												}if($sp_details['rating']=='3.5'){
												echo '<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-half-o" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
													</li>';
												}if($sp_details['rating']=='4'){
												echo '<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
													</li>';
												}if($sp_details['rating']=='4.5'){
												echo '<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-half-o" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star-o" aria-hidden="true"></i>
													</li>';
												}
												if($sp_details['rating']=='5'){
												echo '<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>
													<li class="list-inline-item"><i class="fa fa-star" aria-hidden="true"></i>
													</li>';
												} }?>
										</ul>
									</div>
									
								</div>
					<?php } }else {echo 'Service providers are not available.';}?>
					</a>
				</div>
				<!-- <div class="service-provider not-50">
					<h4 class="heading">UPCOMING EVENT NEAR YOU</h4>
					<div class="service singlePost">
						<div class="service-img full-width">

							<?php //include APPPATH.'views/front/include/slider2.php'; ?>
						</div>
						<div class="service-details fWidth">
							<h6 class="userName">Iconic SET 90 Events</h6>
							<p>Kimmi Cruiser</p>
							<p class="address">725 S Mission St, Sapulpa, OK 74066, United States</p>
						</div>
					</div>
				</div> -->
				<div class="service-provider">
					<?php include APPPATH.'views/front/include/be_my_date.php';  ?>
				</div>
				<?php include APPPATH.'views/front/include/play_store_link.php';  ?>
			</div>
		</div>
	</div>
</section>
<script>
	/* for load more post */

	$(document).ready(function () {
		$(".content").slice(0, 4).show();
		$("#loadMore").on("click", function (e) {
			e.preventDefault();
			$(".content:hidden").slice(0, 2).slideDown();
			if ($(".content:hidden").length == 0) {
				$("#loadMore").text("No Content").addClass("noContent");
			}
		});
	});
//////////////////////////post data code////////////////////////
var imageCount=0;
function renderImage(seq) {
	  $("#loader-wrapper").show();
	  var file = event.target.files[0];
	  var fileReader = new FileReader();
	  if(!file.type.match('image') && !file.type.match('video')) {
	  		$("#loader-wrapper").hide();
        	$("#fileUpload_"+seq+"").val('');
		  	swal("File must be image or video.\n\n");
		  	return false;
	  }
	  if (file.type.match('image')) {
  		var FileExt = file.name.substr(file.name.lastIndexOf('.') + 1);
	  	if ((FileExt.toUpperCase() != "JPG" && FileExt.toUpperCase() != "JPEG" && FileExt.toUpperCase() != "PNG")) {
		  		$("#loader-wrapper").hide();
	        	$("#fileUpload_"+seq+"").val('');
			  	swal("File must be jpg or jpeg or png .\n\n");
			  	return false;
    	}
	  	var FileSize = file.size;
	  	if(FileSize>20000000)
        {
        	$("#loader-wrapper").hide();
        	$("#fileUpload_"+seq+"").val('');
        	swal("File size should not be more than 20 MB.\n\n");
            return false;
        } 
	    fileReader.onload = function() {
	      $("#img_src_"+seq+"").attr('src', fileReader.result);
	      $("#span_"+seq+"").html('<i class="fa fa-close"></i>');
	      $("#loader-wrapper").hide();
	      imageCount++;
	      $("#post_btn").attr('disabled',false);
	    };
	    fileReader.readAsDataURL(file);
	  } else {
	    fileReader.onload = function() {
	      var blob = new Blob([fileReader.result], {type: file.type});
	      var FileExt = file.name.substr(file.name.lastIndexOf('.') + 1);
		  	if ((FileExt.toLowerCase() != "avi" && FileExt.toLowerCase() != "mov" && FileExt.toLowerCase() != "mp4")) {
			  		$("#loader-wrapper").hide();
		        	$("#fileUpload_"+seq+"").val('');
				  	swal("File must be avi or mov or mp4 .\n\n");
				  	return false;
	    	}
	      var FileSize = file.size;
	      if(FileSize>500000000)
	        {
	        	$("#loader-wrapper").hide();
	        	$("#fileUpload_"+seq+"").val('');
	        	swal("File size should not be more than 50 MB.\n\n");
	            return false;
	        } 
	      var url = URL.createObjectURL(blob);
	      var video = document.createElement('video');
	      var timeupdate = function() {
	        if (snapImage()) {
	          video.removeEventListener('timeupdate', timeupdate);
	          video.pause();
	        }
	      };
	      video.addEventListener('loadeddata', function() {
	        if (snapImage()) {
	          video.removeEventListener('timeupdate', timeupdate);
	        }
	      });
	      var snapImage = function() {
	        var canvas = document.createElement('canvas');
	        canvas.width = video.videoWidth;
	        canvas.height = video.videoHeight;
	        canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
	        var image = canvas.toDataURL();
	        var success = image.length > 100000;
	        if (success) {
	          $("#img_src_"+seq+"").attr('src', image);
	     	  $("#span_"+seq+"").html('<i class="fa fa-close"></i>');
	     	  $("#loader-wrapper").hide();
	          URL.revokeObjectURL(url);
	          imageCount++;
	          $("#post_btn").attr('disabled',false);
	        }
	        return success;
	      };
	      video.addEventListener('timeupdate', timeupdate);
	      video.preload = 'metadata';
	      video.src = url;
	      // Load video in Safari / IE11
	      video.muted = true;
	      video.playsInline = true;
	      video.play();
	    };
	    fileReader.readAsArrayBuffer(file);
	  }
	}
	
	function remove_image(seq){
		$("#img_src_"+seq+"").attr("src","assets/front/images/add_image_placeholder.png");
		$("#span_"+seq+"").html('');
		$("#fileUpload_"+seq+"").val('');
		imageCount--;
		post_btn_action();
		
	}
	function post_btn_action(){
		var text=$("#post_content").val().trim();
		//alert(text.length);
		if(text.length!=0 || imageCount!=0){
			$("#post_btn").attr('disabled',false);
		}else{
			$("#post_btn").attr('disabled',true);
		}
	}
	function post_submit(){
		$("#loader-wrapper").show();
		$("#post_btn").attr('disabled',true);
		var form = $('#post_form')[0];
		var formData = new FormData(form);
      	$.ajax({
        url: 'post',
        type: 'POST',
        data: formData,
        dataType: 'json',
        // async: false,
        cache: false,
        contentType: false,
        processData: false,
    
        success:function(resp){
          $("#loader-wrapper").hide();  
          $("html, body").animate({ scrollTop: 0 }, "slow");
          $(".message_box").show(); 
          $("#post_btn").attr('disabled',false);
          	ajax_check_session(resp);
            if(resp.status==0){
                $(".message_box").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                setTimeout(function() {
                 $(".message_box").html('');
                  $(".message_box").hide();
                }, 5000); 
              }else{
                $(".message_box").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                setTimeout(function() { 
                 $(".message_box").html('');
                 $(".message_box").hide();
                 location.reload();
                }, 2000); 
            }
        },
        error:function(err){
          $("#post_btn").attr('disabled',false);
          $("#loader-wrapper").hide();
        }
      });
	}
/////////////////////////////////////////////////////////////////////////////
	$("#loader-wrapper").show();
    var totalrecords='<?php echo $total_records;?>';
    var track_page = 0;
     //track user scroll as page number, right now page number is 1
    load_contents(track_page);
    $(window).scroll(function() { //detect page scroll
    	
        if($(window).scrollTop() + $(window).height() >= $(document).height()-600) { //if user scrolled to bottom of the page
        	
            track_page++;
            var current_page=<?php echo FRONT_LIMIT; ?>*track_page;
            if(current_page<totalrecords){
            load_contents(track_page);
          }
        }
    }); 

    //Ajax load function
    function load_contents(track_page){
        $('#loader-wrapper').show(); 
        var check=$("#feed_box").html(); 
        $.ajax({
            type:'POST',
            data:{ 
                page:track_page,
            },
            url: "<?php echo base_url();?>front/Home/feeds_data",
           success:function(data) {
           		ajax_check_session(data,1);
                if(data.trim().length == 0){
                  if(check.trim().length == 0){
                    $("#loadMore").text("No Content").addClass("noContent");
                    $('#loader-wrapper').hide();
                    return;
                  }
                }
                if(track_page==0){
                    $("#feed_box").html('');
                }
                $('#loader-wrapper').hide(); 
                $("#feed_box").append(data);
            }/*,error:function(data) {
            	$('#loader-wrapper').hide(); 
                location.reload();
            }*/
        });
    }
    function follow_user(id,tp_sp) {
	    $("#loader-wrapper").show(); 
	    $.ajax({
	        type: 'POST',
	        url: "follow-user",
	        data: {'id':id},
	        dataType: 'json',
	        success:function(resp){
	            $("#loader-wrapper").hide();
	            if(resp.status==0){
	               alert(resp.msg); 
	            }else{
	                $("#follow_btn_"+tp_sp).html(resp.msg);
	                if(resp.msg=='Unfollow'){
	                    $("#follow_btn_"+tp_sp).removeClass('btn-primary');
	                    $("#follow_btn_"+tp_sp).addClass('btn-danger');
	                }else{
	                    $("#follow_btn_"+tp_sp).removeClass('btn-danger');
	                    $("#follow_btn_"+tp_sp).addClass('btn-primary');
	                }
	            }
	            
	        }
	    });																								      
	} 
</script>