<div class="modal" id="edit_post_modal">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Edit Post</h5>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
      <!-- Modal body -->
      <div class="modal-body">
        <div class="edit_message_box" style="display:none;"></div>
        <div class="card">
          <div class="tab-content" id="singleService">
            <div class="tab-pane show active" id="tab1" role="tabpanel" aria-labelledby="tab1">
              <div class="accordion" id="accordion-tab-1">
                <div class="show" id="accordion-tab-1-content-1">
                  <div class="card-body pd-0">
                   <form class="add-service-form" action="" id="edit_post_form" enctype="multipart/form-data" data-parsley-validate>
                        <textarea class="form-control post-details" placeholder="Hey! what are you upto? Celebrated recently? Let your friend know..." name="post_content" maxlength="1500" id="edit_post_content" oninput ="edit_post_btn_action()"><?php if(!empty($details['post_content'])) echo $details['post_content'];?></textarea>
                        <input type="text" value="1" name="check_post" id="check_post" data-parsley-required data-parsley-required-message="You cannot be update blank post." style="display:none;">
                        <button class="btn btn-primary float-right btn-post" id ="edit_post_btn" type="button" onclick="update_post_submit()"><i class="fa fa-paper-plane-o pl-1" aria-hidden="true"></i> Save</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() { 
    $('#edit_post_modal').modal('show');
});

function edit_post_btn_action(){
    var text=$("#edit_post_content").val().trim();
    var media_exist="<?php echo $media_exist;?>";
    if(text.length==0 && media_exist==0){
        $("#edit_post_btn").attr('disabled',true);
        $("#check_post").val('');
    }else{
        $("#edit_post_btn").attr('disabled',false);
        $("#check_post").val('1');
    }
}
function update_post_submit(){
     var text=$("#edit_post_content").val();
    var media_exist="<?php echo $media_exist;?>";
    if(text.length==0 && media_exist==0){
        $("#edit_post_btn").attr('disabled',true);
        $("#check_post").val('');
        return false;
    }
    $("#loader-wrapper").show();
    $("#edit_post_btn").attr('disabled',true);
    var form = $('#edit_post_form')[0];
    var formData = new FormData(form);
    var post_number="<?php echo !empty($details['post_number'])?base64_encode($details['post_number']):'N/A';?>";
    formData.append('post_num', post_number);
    $.ajax({
    url: 'update-post',
    type: 'POST',
    data: formData,
    dataType: 'json',
    // async: false,
    cache: false,
    contentType: false,
    processData: false,

    success:function(resp){
      $("#loader-wrapper").hide();
      $(".edit_message_box").show(); 
      $("#edit_post_btn").attr('disabled',false);
        if(resp.status==0){
            $(".edit_message_box").html('<div class="alert alert-danger">'+resp.msg+'</div>');
            setTimeout(function() {
             $(".edit_message_box").html('');
              $(".edit_message_box").hide();
            }, 5000); 
          }else{
            $(".edit_message_box").html( '<div class="alert alert-success">'+resp.msg+'</div>');
            setTimeout(function() { 
             $(".edit_message_box").html('');
             $(".edit_message_box").hide();
             location.reload();
            }, 2000); 
        }
    },
    error:function(err){
      $("#edit_post_btn").attr('disabled',false);
      $("#loader-wrapper").hide();
    }
  });
}
</script>