<?php if(!empty($bookings)){ 
    //echo "<pre>";print_r($bookings);die;
    foreach($bookings as $row) { ?>
<div class="col-xs-12 col-md-6 col-lg-6">
    <a href="public-event-details/<?php if(!empty($row['event_id'])){ echo base64_encode($row['event_id']);}?>">
        <div class="card">
            <div class="card-banner">
               <img class="card-img-top"
                  src="<?php echo !empty($row['image'])?$row['image']:'assets/images/phl_service_banner.png'?>"
                  alt="Card image cap" />
                  <?php if(!empty($row['request_status'])){
                   if($row['request_status']=='Pending'){
                        $sts_cls='on-going';
                    }if($row['request_status']=='Accepted'){
                        $sts_cls='completed';
                    }if($row['request_status']=='Rejected'){
                        $sts_cls='not-started';
                    }?>
                    <div class="top-ribbon <?php echo $sts_cls;?>"><?php echo $row['request_status'];?></div>
                <?php }?>
               
            </div>
            <div class="card-body">
               <div class="card-title-box">
                    <h5 class="card-title"><?php if(!empty($row['event_title'])){echo $row['event_title'];}else{ echo 'N/A';}?></h5>
                    <div class="latest-sec-2 sec-3">
                        
                    </div>
                </div>
                <div class="ctn inner-profile-title">
                    <div class="user-profile">
                    <a href="profile/<?php echo !empty($row['user_id'])? base64_encode($row['user_id']):'N/A';?>" ><img src="<?php echo !empty($row['profile_picture'])?$row['profile_picture']:'assets/front/images/profile0.png'?>" alt="logo"></a>
                    </div>
                    <div class="post-text">
                        <div class="recentPostDetails">
                            <h6 class="userName"><?php if(!empty($row['fullname'])){echo $row['fullname'];}else{ echo 'N/A';}?>s</h6>
                        </div>
                    </div>
                </div>
                <div class="functions-list"><span><?php if(!empty($row['event_date'])) echo convertGMTToLocalTimezone($row['event_date']);?>  | <?php if(!empty($row['event_date'])) echo convertGMTToLocalTimezone($row['event_date'],'',true);?></span>
                </div>
            </div>
        </div>
    </a>
</div>
<?php }  } ?>

