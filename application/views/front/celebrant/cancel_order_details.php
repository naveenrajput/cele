<div class="modal" id="cancel_order_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<!-- Modal body -->
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Cancel order payment details</h5>
				<div class="card add-service-card forpop">
					<?php if(!empty($details)){ ?>
						<p>Paid Amount: <?php echo ADMIN_CURRENCY;?><?php echo !empty($details['paid_amount'])?$details['paid_amount']:'N/A';?></p>
						<p>Paypal Fees : <?php echo ADMIN_CURRENCY;?><?php echo !empty($details['paypal_fee_amount'])?$details['paypal_fee_amount']:'N/A';?></p>
						<p>Payment Status: <?php echo !empty($details['payment_status'])?$details['payment_status']:'N/A';?></p>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function() { 
		var chk_session='<?php echo $this->session->userdata('user_id');?>';
        if(chk_session==''){
            window.location.href='login';
        }
        $('#cancel_order_modal').modal('show');
    });
</script>