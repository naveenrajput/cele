
 <?php if((isset($row['child_comment']) && !empty($row['child_comment'])) || (isset($child_comment) && !empty($child_comment))){
      if(isset($child_comment) && !empty($child_comment)){
         $row['child_comment']=$child_comment;
         if((CHILD_COMMENT_LIMIT*($child_track_page+1))<$total_child_comment){ ?>
            <a class="comment_more" comment-attr-id="<?php echo !empty($row['child_comment'][0]['parent_id'])?base64_encode($row['child_comment'][0]['parent_id']):'N/A';?>" post-attr-id="<?php echo !empty($row['child_comment'][0]['post_number'])?base64_encode($row['child_comment'][0]['post_number']):'N/A';?>" href="javascript:void(0)" onclick="load_more_child_comment(this,'<?php echo $child_track_page;?>');">View more reply</a>
         <?php }
      }else{
         if($row['reply_count']>INNER_CHILD_COMMENT_LIMIT){ ?>
            <a class="comment_more" comment-attr-id="<?php echo !empty($row['child_comment'][0]['parent_id'])?base64_encode($row['child_comment'][0]['parent_id']):'N/A';?>" post-attr-id="<?php echo !empty($comments[0]['post_number'])?base64_encode($comments[0]['post_number']):'N/A';?>" href="javascript:void(0)" onclick="load_more_child_comment(this,0);">View more reply</a>
      <?php }
      }
      $q=generateRandomString(rand(0,12));
      foreach($row['child_comment'] as $row){$q++;?>
      <ul class="list-inline remove_comment_box" id="remove_comment_<?php echo $q;?>">
         <div class="user-profile">
            <img src="<?php echo !empty($row['profile_picture'])?$row['profile_picture']:'assets/front/images/profile0.png'?>" alt="logo">
         </div>
         <div class="post-text">
            <div class="recentComment">
               <span class="sec-1">
                  <h6 class="userName commentUser"><?php if(!empty($row['fullname'])){echo $row['fullname'];}else{ echo 'N/A';}?></h6>
               </span>
               <?php if($row['user_id']==$this->session->userdata("user_id")){ ?>
               <div class="latest-sec-2">
                  <div class="dropdown">
                     <a href="javascript:void(0);" class="zigzag" data-toggle="dropdown" aria-expanded="false">
                     <img src="assets/front/images/option.svg" alt="like">
                     </a>
                     <div class="dropmenuContainer1">
                        <div class="dropdown-menu1 dropdown-menu">
                           <a class="dropdown-item" href="javascript:void(0)" comment-attr-id="<?php echo !empty($row['post_number'])?base64_encode($row['post_number']):'N/A';?>" remove-id="<?php if(!empty($row['id'])){echo base64_encode($row['id']);}else{ echo 'N/A';} ?>" onclick="remove_comment(this,'<?php echo $q;?>')" ><img
                              src="assets/front/images/form_icons/ic_remove_connection.png"
                              alt="logo" /> Remove Comment</a>
                        </div>
                     </div>
                  </div>
               </div>
               <?php } ?>
               <p class="singleComment"><?php if(!empty($row['comment'])){echo $row['comment'];}else{ echo 'N/A';}?>
               </p>
               <ul class="list-inline">
                  <li class="list-inline-item">
                      <a href="javascript:void(0)" comm-id="<?php if(!empty($row['id'])){echo base64_encode($row['id']);}else{ echo 'N/A';} ?>" onclick="like_comment(this,'<?php echo $q;?>');">
                        <img src="<?php echo !empty($row['is_liked_by_me'])?'assets/front/images/home/icon/heart.svg': 'assets/front/images/favorite.svg'?>" alt="like" id="com_like_img_<?php echo $q;?>"> 
                        <span ml-1 id="com_like_count_<?php echo $q;?>"><?php echo !empty($row['comment_like'])?$row['comment_like']:'';?></span>
                     </a>
                  </li>
                  <li class="list-inline-item">
                     <a href="javascript:void(0)" onclick="reply_comment(this);">
                     <img src="assets/front/images/reply.svg" alt="like"><span ml-1>Reply</span>
                     </a>
                  </li>
               </ul>
               <div class="commnetSection2 inner-input reply_comment" style="display: none;">
                  <div class="post-text post-text2">
                     <div class="recentComment">
                        <div class="recomment">
                           <input type="text" class="form-control post-title"
                              placeholder="Write a comment..." maxlength="500" id="post_comment_text_<?php echo $q;?>" oninput ="comment_post_btn_action(this,'<?php echo $q;?>')" onkeypress="isNumericKeyPress(event,'<?php echo $q;?>')">
                           <button comment-id="<?php echo !empty($row['post_number'])?base64_encode($row['post_number']):'N/A';?>" reply-comment="<?php echo !empty($row['parent_id'])?base64_encode($row['parent_id']):'N/A';?>" class="send-icon post_comment_btn_<?php echo $q;?>" disabled="true" onclick="post_comment(this,'<?php echo $q;?>',1)"></button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </ul>
  <?php } }?>
 