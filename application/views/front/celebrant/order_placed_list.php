<div id="item_box"></div>
<section class="section-div pt-4 order-history">
<div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="tab-container">
          <ul class="nav nav-tabs tab-btn" role="tablist">
            <?php if($this->session->userdata('front_user_type')==3) { ?>
                <li class="nav-item"> <a class="nav-link" href="order-received-history" >Order received</a>
                </li>
             <?php }?>
            <li class="nav-item"> <a class="nav-link active" href="order-placed-history">Orders Placed</a>
            </li>
          </ul>
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="OrderReceived">
              <div class="card">
                <div class="row p-4">
                  <div class="col-sm-6">
                    <h6 class="pt-3" style="color: #3B3F4E;"> Order History </h6>
                  </div>
                  <div class="col-sm-6">
                    <form class="example" action="" method="POST">
                      <input type="text" placeholder="Search Order ID" name="orderid" value='<?php if(isset($_POST["orderid"]) && !empty($_POST["orderid"])){echo $_POST["orderid"];}?>'>
                      <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                  </div>
                </div>
                <hr>
                <div class="order_history_box" id ="order_history_box"></div>
                <hr>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script>
/* for load more post */
  $("#loader-wrapper").show();
        var totalrecords='<?php echo $total_records;?>';
        var track_page = 0;
         //track user scroll as page number, right now page number is 1
        load_contents(track_page);
        $(window).scroll(function() { //detect page scroll
            if($(window).scrollTop() + $(window).height() >= $(document).height()-600) { //if user scrolled to bottom of the page
                track_page++;
                var current_page=<?php echo FRONT_LIMIT; ?>*track_page;
               
                if(current_page<totalrecords){
                load_contents(track_page);
              }
            }
        }); 

    //Ajax load function
    function load_contents(track_page){

        $('#loader-wrapper').show(); 
        var check=$("#order_history_box").html(); 
        var order_id='<?php if(isset($_POST["orderid"]) && !empty($_POST["orderid"])){echo $_POST["orderid"];}?>';
        $.ajax({
            type:'POST',
            data:{ 
                page:track_page,
                orderid:order_id,
            },
            url: "<?php echo base_url();?>front/Booking/order_placed_history_data", 
           success:function(data) {
                ajax_check_session(data,1);
                if(data.trim().length == 0){
                  if(check.trim().length == 0){
                    $("#loadMore").text("No Content").addClass("noContent");
                    $('#loader-wrapper').hide();
                    return;
                  }
                }
                if(track_page==0){
                    $("#order_history_box").html('');
                }
                $('#loader-wrapper').hide(); 
                $("#order_history_box").append(data);

            }
        });
    }
    if ( window.history.replaceState ) {
      window.history.replaceState( null, null, window.location.href );
    }
</script>