<div id="attendee_box"></div>
<section class="section-div pt-5">
	<div class="container">
		<div class="message_box" style="display:none;"></div>
		<div class="row">
			<div class="col-md-8 service-details f-width">
				<div class="card mb-4">
					<div class="card-banner service-slider-container">
						<div id="demo" class="carousel slide" data-ride="carousel">
						    <!-- Indicators -->
						    <ul class="carousel-indicators">
						        <li data-target="#demo" data-slide-to="0" class="active"></li>
						    </ul>

						    <!-- The slideshow -->
						    <div class="carousel-inner">
						        <div class="carousel-item active">
						            <img src="<?php if(!empty($booking_details['image'])){echo $booking_details['image'];} else{echo 'assets/images/phl_service_banner.png';}?>" alt="Event Image">
						        </div>
						    </div>
						</div>
						<div class="d-Service detail-profile-content white-text-style">
							<div class="row">
								<div class="col-sm-8">
									<h5 class="card-title"><?php if(!empty($booking_details['event_title'])){ echo ucfirst($booking_details['event_title']);} else {echo 'N/A';}?></h5>
									<div class="eventVanue">
										<div class="address">	<span><?php if(!empty($booking_details['event_address'])){ echo $booking_details['event_address'];} else {echo 'N/A';}?></span>
											<a target="_blank" href="https://www.google.com/maps/search/?api=1&query=<?php if(!empty($booking_details['event_address'])) echo $booking_details['event_address'];?>"><img class="map-icon-style" src="assets/front/images/icons/GoogleMaps_logo@2x.png"
												alt="" /></a>
										</div>
                                    </div> 
								</div>
								<div class="col-sm-4 text-right img-msg">
                               	<?php if(!empty($booking_details['event_status'])){
					                    if($booking_details['event_status']=='Not started'){
					                        $sts_cls='not-started';
					                    }if($booking_details['event_status']=='On Going'){
					                        $sts_cls='on-going';
					                    }if($booking_details['event_status']=='Completed'){
					                        $sts_cls='completed';
					                    }if($booking_details['event_status']=='Canceled'){
					                        $sts_cls='not-started';
					                    }?>
                                	<a class="btn-sm-style <?php echo $sts_cls;?>" href="javascript:void(0)"><?php  echo $booking_details['event_status'];?></a>
                            	<?php } ?>
								</div>
                            </div>
                            
                            <div class="row ">
								<div class="col-md-7 eventDetails">
									<ul class="list-inline mb-1">
										<li class="list-inline-item  accept-icon-style" onclick="get_event_attendee('Accepted','<?php echo base64_encode($booking_details['event_id']);?>','<?php echo base64_encode($booking_details['user_id']);?>')">
											<img src="assets/front/images/icons/ic_accept.svg" alt="">
											<span><?php if(isset($accepted)){ echo $accepted;} else {echo 'N/A';}?> Accepted</span>
										</li>
										<li class="list-inline-item pending-icon-style" onclick="get_event_attendee('Pending','<?php echo base64_encode($booking_details['event_id']);?>','<?php echo base64_encode($booking_details['user_id']);?>')">
											<img src="assets/front/images/icons/sand-clock.svg" alt="">
											<span><?php if(isset($pending)){ echo $pending;} else {echo 'N/A';}?> pending</span>
										</li>
										<li class="list-inline-item rejected-icon-style" onclick="get_event_attendee('Rejected','<?php echo base64_encode($booking_details['event_id']);?>','<?php echo base64_encode($booking_details['user_id']);?>')">
											<img src="assets/front/images/icons/dislike.svg" alt="">
											<span><?php if(isset($rejected)){ echo $rejected;} else {echo 'N/A';}?> Rejected</span>
										</li>
									</ul>
								</div>
								<div class="col-md-5 text-right">
									<div class="eventDetails dateTMStyle">
										<ul class="list-inline mb-1">
											<li class="list-inline-item dateTextStyle">
												<img src="assets/front/images/icons/event.svg" alt="">
												<span><?php if(!empty($booking_details['event_date'])){ echo convertGMTToLocalTimezone($booking_details['event_date']);} else {echo 'N/A';}?></span>
											</li>
											<li class="list-inline-item timeTextStyle">
												<img src="assets/front/images/icons/clock.svg" alt="">
												<span><?php if(!empty($booking_details['event_date'])){ echo convertGMTToLocalTimezone($booking_details['event_date'],'',true);} else {echo 'N/A';}?>
													<?php
													//$timezone=$this->session->userdata('user_timezone'); 
													/*echo 'new';
													echo $timezone =$this->session->userdata('user_timezone');
													echo convertGMTToLocalTimezone($booking_details['event_date'],'',true,$timezone);*/ ?>
												</span>
											</li>
										</ul>
									</div>
								</div>
							</div>
                        </div>
						<div class="d-Service detail-profile-content ">

						<div class="row">
								<div class="col-sm-8"> 
						  <div class="profileWDtext">
                               <div class="img-circle-style">
								   <img src="<?php echo !empty($booking_details['profile_picture'])?$booking_details['profile_picture']:'assets/front/images/profile0.png'?>" alt="">
							   </div>
							   <span><?php if(!empty($booking_details['fullname'])){ echo $booking_details['fullname'];} else {echo 'N/A';}?></span>
						       </div> 
								</div>
								<?php if($booking_details['event_status']=='On Going' || $booking_details['event_status']=='Not started'){ ?>
									<div class="col-sm-4 text-right img-msg">
									<a class="btn-lg-style yellow-btn mrgT-6" href="invite-friend-list/<?php if(!empty($booking_details['event_id'])){ echo base64_encode($booking_details['event_id']);}?>">Invite Friends</a>
									</div>
								<?php } ?>
							</div>  
						</div>
						<div class="card-body profile-detail-style">
							<p class="pull-left"><?php if(!empty($booking_details['note'])){ echo $booking_details['note'];} else {echo 'N/A';}?></p>
								
							<!-- Material switch -->
							<?php if($booking_details['event_status']=='On Going'){ 
								if($order_record['order_status']!='Pending'){?>
							<div class="material-switch pull-right">
								<h5>Mark as Completed</h5>
								<input id="someSwitchOptionSuccess" name="someSwitchOption001" type="checkbox" onclick="mark_completed()"/>
								<label for="someSwitchOptionSuccess" class="label-success"></label>
							</div>
							<?php } }?>
							<?php if($booking_details['event_status']=='Completed' && $is_provide_rating==0){ ?>
							<div class="clearfix"></div>
							<button class="btn btn-success mark-complted"  data-toggle="modal" data-target="#myModal_review_rating">Add Rating</button> 
							<?php } ?>
						</div>
					</div>
				</div>
				
					<div class="my-booking-request-block">
						<?php if(!empty($total_records)){ ?>
						<h4 class="main-heading mb-4">Requests Received</h4>
						<?php } ?>		
						<!-- <div class="row" id="event_req" style="height:200px;   overflow: scroll; overflow-x: hidden; overflow-y: scroll;"> -->
						<div class="row" id="event_req">
						</div>
					</div>	
				
			</div>
			<div class="col-md-4 no-background">
				<div class="service-provider">
					<?php include APPPATH.'views/front/include/be_my_date.php';  ?>
				</div>
				<?php include APPPATH.'views/front/include/play_store_link.php';  ?>
			</div>
		</div>
	</div>
</section>
<!-- Modal -->
<div class="modal fade" id="myModal_review_rating" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
		<h4 class="modal-title" id="myModalLabel">Add Rating</h4>
		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
      <form class="review_rating" id="review_rating" method="POST" action="" data-parsley-validate >
			<h6>Rating: </h6>
            <div class="rate">
                <input type="radio" id="rating10" name="rating"  value="5" data-parsley-required data-parsley-required-message="Please enter rating"/><label for="rating10"></label>
                <input type="radio" id="rating9" name="rating" data-parsley-required data-parsley-required-message="Please add rating"  value="4.5" /><label class="half" for="rating9"></label>
                <input type="radio" id="rating8" name="rating" data-parsley-required data-parsley-required-message="Please add rating"  value="4" /><label for="rating8"></label>
                <input type="radio" id="rating7" name="rating" data-parsley-required data-parsley-required-message="Please add rating"  value="3.5" /><label class="half" for="rating7"></label>
                <input type="radio" id="rating6" name="rating" data-parsley-required data-parsley-required-message="Please add rating"  value="3" /><label for="rating6"></label>
                <input type="radio" id="rating5" name="rating" data-parsley-required data-parsley-required-message="Please add rating"  value="2.5" /><label class="half" for="rating5"></label>
                <input type="radio" id="rating4" name="rating" data-parsley-required data-parsley-required-message="Please add rating" value="2" /><label for="rating4"></label>
                <input type="radio" id="rating3" name="rating" data-parsley-required data-parsley-required-message="Please add rating"  value="1.5" /><label class="half" for="rating3"></label>
                <input type="radio" id="rating2" name="rating" data-parsley-required data-parsley-required-message="Please add rating"  value="1" /><label for="rating2" ></label>
                <input type="radio" id="rating1" name="rating" data-parsley-required data-parsley-required-message="Please add rating"  value="0.5" /><label class="half" for="rating1"></label>
                <span style="display: none;"><input type="radio" id="rating0" name="rating" value="0" /><label for="rating0" title="No star"></label></span>
            </div>			
			<!-- <textarea class="form-control" name ="review" placeholder="Write Review" maxlength="500" data-parsley-required data-parsley-required-message="Please enter review."></textarea -->
			<button type="button" id="review_btn" class="btn btn-primary pull-right" onclick="rating_form_submit('rating_review')">Submit</button>
		</form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$("#loader-wrapper").show();
    var totalrecords='<?php echo $total_records;?>';
    var track_page = 0;
     //track user scroll as page number, right now page number is 1
    load_contents(track_page);
    $("#event_req").scroll(function() { //detect page scroll
    	 var element =document.getElementById('event_req');
        if($("#event_req").scrollTop() + $("#event_req").height() >= element.offsetHeight+$("#event_req").scrollTop()) { //if user scrolled to bottom of the page
            track_page++;
            var current_page=<?php echo FRONT_LIMIT; ?>*track_page;
           
            if(current_page<totalrecords){
            load_contents(track_page);
          }
        }
    }); 

    //Ajax load function
    function load_contents(track_page){
        $('#loader-wrapper').show(); 
        var check=$("#event_req").html(); 
        var event_id ='<?php if(!empty($booking_details['event_id'])){ echo base64_encode(($booking_details['event_id']));}?>';
        $.ajax({
            type:'POST',
            data:{ 
            	event_id: event_id,
                page:track_page,
            },
            url: "<?php echo base_url();?>front/Booking/event_request_received", 
           success:function(data) {
                if(data.trim().length == 0){
                  if(check.trim().length == 0){
                    $("#loadMore").text("No Content").addClass("noContent");
                    $('#loader-wrapper').hide();
                    return;
                  }
                }
                if(track_page==0){
                    $("#event_req").html('');
                }
                $('#loader-wrapper').hide(); 
                $("#event_req").append(data);

            }
        });
    }
    function mark_completed(){
    	swal({
	        title: "Confirmation",
	        text: "Are you sure you want to complete Event ?",
	        icon: "warning",
	        buttons: true,
	        dangerMode: true,
	    })
	    .then((willDelete) => {
	        if (willDelete) {
	        	$('#loader-wrapper').show(); 
	        	var event_id='<?php if(!empty($booking_details['event_id'])){ echo base64_encode($booking_details['event_id']);} else {echo 'N/A';}?>';
	        	var order_id='<?php if(!empty($order_record['order_id'])){ echo base64_encode($order_record['order_id']);} else {echo 'N/A';}?>';
	            $.ajax({
	                type: 'POST',
	                url: "<?php echo 'complete-event';?>",
	                data: {event_id:event_id,order_id:order_id},
	                dataType: 'json',
	                success:function(resp){
	                	$('#loader-wrapper').hide(); 
	                    $("html, body").animate({ scrollTop: 0 }, "slow");
	                    $(".message_box").html(resp.msg);
	                    $(".message_box").show();
	                    if(resp.status==0){
	                    	$('input[name=someSwitchOption001]').prop('checked', false);
	                        $(".message_box").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
	                            setTimeout(function() {
	                             $(".message_box").html('');
	                             $(".message_box").hide();
	                            }, 5000); 
	                    }else{
	                        $(".message_box").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
	                            setTimeout(function() {
	                                $(".message_box").html('');
	                                $(".message_box").hide();
	                                location.reload();
	                            }, 1000); 
	                    }
	                    
	                }
	            });
	        }else{
	        	$('input[name=someSwitchOption001]').prop('checked', false);
	        	$('#loader-wrapper').hide(); 
	        }
	    });
    }
	function rating_form_submit(){
	$("#review_rating").parsley().validate();
    if($("#review_rating").parsley().isValid()){
    	var form = $('#review_rating')[0];
    	var formData = new FormData(form);
    	$('#myModal_review_rating').modal('hide');
    	$("#review_btn").attr('disabled',true);
    	$("#loader-wrapper").show();
    	var event_id='<?php if(!empty($booking_details['event_id'])){ echo base64_encode($booking_details['event_id']);} else {echo 'N/A';}?>';
    	formData.append('event_id', event_id);
    	$.ajax({
	        url: 'front/Booking/add_rating_review',
	        type: 'POST',
	        data: formData,
	       	dataType: 'json',
	       	cache: false,
	        contentType: false,
	        processData: false,
	        success:function(resp){
			  $("#review_rating").parsley().reset();
	          $("#review_rating")[0].reset();
	          $("#loader-wrapper").hide();  
	          $("html, body").animate({ scrollTop: 0 }, "slow");
	          $(".message_box").show(); 
	          $("#review_btn").attr('disabled',false);
	            if(resp.status==0){
	                $(".message_box").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
	                setTimeout(function() {
	                 $(".message_box").html('');
	                  $(".message_box").hide();
	                }, 5000); 
	              }else{
	                $(".message_box").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
	                setTimeout(function() { 
	                 $(".message_box").html('');
	                 $(".message_box").hide();
	                 location.reload();
	                }, 2000); 
	            }
	        },
	        error:function(err){
	          $("#loader-wrapper").hide();
	        }
      	});
    }
}
</script>