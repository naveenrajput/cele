<section class="section-div pt-5 home-section">
   <div class="container">
      <div class="row">
         <div class="col-md-8 service-details friend-profile">
         	<div class="message_box" style="display:none;"></div>
            <h4 class="main-heading">Saved Post</h4>
	            <div class="flex">
					<div class="content" id="feed_box">
						
					</div>
				</div>
				<div class="text-center pt-4"><a href="javascript:void(0)" id="loadMore">
						<img class="spinner-icon" src="assets/front/images/home/icon/spinner-of-dots.svg" alt="">
						<span>Load More</span></a>
				</div>
         </div>
      </div>
   </div>
</section>
<script>
$(document).ready(function () {
		$(".content").slice(0, 4).show();
		$("#loadMore").on("click", function (e) {
			e.preventDefault();
			$(".content:hidden").slice(0, 2).slideDown();
			if ($(".content:hidden").length == 0) {
				$("#loadMore").text("No Content").addClass("noContent");
			}
		});
	});
$("#loader-wrapper").show();
    var totalrecords='<?php echo $total_records;?>';
    var track_page = 0;
     //track user scroll as page number, right now page number is 1
    load_contents(track_page);
    $(window).scroll(function() { //detect page scroll
    	
        if($(window).scrollTop() + $(window).height() >= $(document).height()-600) { //if user scrolled to bottom of the page
        	
            track_page++;
            var current_page=<?php echo FRONT_LIMIT; ?>*track_page;
            if(current_page<totalrecords){
            load_contents(track_page);
          }
        }
    }); 

    //Ajax load function
    function load_contents(track_page){
        $('#loader-wrapper').show(); 
        var check=$("#feed_box").html(); 
        $.ajax({
            type:'POST',
            data:{ 
                page:track_page,
            },
            url: "<?php echo base_url();?>front/Home/save_post_data", 
           success:function(data) {
                ajax_check_session(data,1);
                if(data.trim().length == 0){
                  if(check.trim().length == 0){
                    $("#loadMore").text("No Content").addClass("noContent");
                    $('#loader-wrapper').hide();
                    return;
                  }
                }
                if(track_page==0){
                    $("#feed_box").html('');
                }
                $('#loader-wrapper').hide(); 
                $("#feed_box").append(data);
            }
        });
    }
</script>
