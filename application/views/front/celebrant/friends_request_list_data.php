<?php 
    if (isset($request_list) && !empty($request_list)) {
        foreach ($request_list as $request) {
?>
<?php
if ($page === 'Friend List') {
?>    
<div class="single-request-block mt-3">
    <div class="request-profile-image">
        <img src="<?php  
            if($request['profile_picture']) { 
                echo base_url().$request['profile_picture'] ; 
            }else
            { 
                if($request['gender'] == 'Male'){ echo base_url().'assets/front/images/profile0.png' ; } 
                if($request['gender'] == 'Female') { echo base_url().'assets/front/images/profile00.jpg' ; } 
                if($request['gender'] == '') { echo base_url().'assets/front/images/profile000.png' ;}
            } ?>" />
    </div>
    <div class="request-profile-option">
        <h4><?php echo $request['fullname']; ?></h4>
        <button class="btn btn-accept" onclick="accept_reject('<?php echo base64_encode($request['sent_by']); ?>', '<?php echo base64_encode($request['request_id']); ?>','Accepted')" >Accept</button>
        <button class="btn btn-reject" onclick="accept_reject('<?php echo base64_encode($request['sent_by']); ?>', '<?php echo base64_encode($request['request_id']); ?>','Rejected')" >Reject</button>
    </div>
</div>
<?php
 }else{
?>
<div class="col-md-6">
    
        <div class="single-request-block">
            <div class="request-profile-image">
                <a href="profile/<?php echo base64_encode($request['sent_by']);?>">
                <img src="<?php  
                            if($request['profile_picture']) { 
                                echo base_url().$request['profile_picture'] ; 
                            }else
                            { 
                                if($request['gender'] == 'Male'){ echo base_url().'assets/front/images/profile0.png' ; } 
                                if($request['gender'] == 'Female') { echo base_url().'assets/front/images/profile00.jpg' ; } 
                                if($request['gender'] == '') { echo base_url().'assets/front/images/profile000.png' ;}
                            } ?>" 
                />
                </a>
            </div>
            <div class="request-profile-option">
                <h4 class="mb-1"><?php echo $request['fullname']; ?></h4>
                
                <ul class="add-btnpos">
                    
                        <li><button class="btn btn-accept" onclick="accept_reject('<?php echo base64_encode($request['sent_by']); ?>', '<?php echo base64_encode($request['request_id']); ?>','Accepted')" >Accept</button></li>
                    
                        <li><button class="btn btn-reject" onclick="accept_reject('<?php echo base64_encode($request['sent_by']); ?>', '<?php echo base64_encode($request['request_id']); ?>','Rejected')" >Reject</button></li>
                    
                </ul>
            </div>
        </div>
</div>
<?php
 } 
?>

<?php            
        }
    }
?>