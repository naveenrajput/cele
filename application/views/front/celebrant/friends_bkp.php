<section class="section-div pt-4 book-service-section">
    <div class="container">
    <ul class="nav nav-tabs tab-btn" role="tablist">
      <li class="nav-item"> <a class="nav-link active" href="javascript:void(0)">Friends</a>
      </li>
      <li class="nav-item"> <a class="nav-link" href="friends-request-list">All Request</a>
      </li>
    </ul>
    <div class="message_box" style="display:none;"></div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 service-details friendpage">

                    <div class="card-banner service-slider-container">
                        <h2 class="py-4 main-heading">friend List</h2>
                    </div>
                        <div class="accordion book-service">
                            <div class="row extra-mar" id="friends_id">
                                
                            </div>
                            
                            <div class="text-center pt-4"><a href="javascript:void(0)" id="loadMore">
                               <img class="spinner-icon" src="assets/front/images/home/icon/spinner-of-dots.svg" alt="">
                               <span>Load More</span></a>
                            </div>
                        </div>
            </div> 
            <div class="col-md-12 col-lg-4 payment-card">
                <h6 class="py-4 main-heading">friend request</h6>
                <div class="accordion book-service" id="request_id">
                        
                    <div class="text-center" id="requestMore"></div>
                </div>
                <?php if(isset($total_request) && !empty($total_request)) { ?>
                <a class="text-center" href="friends-request-list" ><span>View All</span></a>
                <?php }else{ echo '<p>No records found.</p>';} ?>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">

    /* for load more post */
    $("#loader-wrapper").show();

    var totalrecords='<?php echo $total_records;?>';
    var totalrequest='<?php echo $total_request;?>';
    
    var track_page = 0;
    //track user scroll as page number, right now page number is 1
    load_contents(track_page);
    load_request_contents(track_page);
    $(window).scroll(function() { //detect page scroll
        if($(window).scrollTop() + $(window).height() >= $(document).height()-400) { //if user scrolled to bottom of the page 

            track_page++;
            
            var current_page=<?php echo FRONT_LIMIT; ?>*track_page;
            
            if(current_page<totalrecords){
                
                load_contents(track_page);
            }
        }
    }); 

    //Ajax load function
    function load_contents(track_page){

        $('#loader-wrapper').show(); 
        var checkFriend=$("#friends_id").html();
        
        $.ajax({
            type:'POST',
            data:{ 
                page:track_page,
            },
            url: "<?php echo base_url();?>front/Home/my_friends_list_data", 
           success:function(data) {
                 ajax_check_session(data,1);
                if(data.trim().length == 0){
                  if(checkFriend.trim().length == 0){
                    $("#loadMore").text("No Content").addClass("noContent");
                    $('#loader-wrapper').hide();
                    return;
                  }
                 
                }
                if(track_page==0){
                    $("#friends_id").html('');
                    
                }
                $('#loader-wrapper').hide(); 
                $("#friends_id").append(data);

            }
        });
    }

    //Ajax load function request
    function load_request_contents(track_page){

        //$('#loader-wrapper').show(); 
        
        var checkRequest=$("#request_id").html(); 
        
        $.ajax({
            type:'POST',
            data:{ 
                page:track_page,page_name:'Friend List'
            },
            url: "<?php echo base_url();?>front/Home/my_friends_request_list_data", 
           success:function(data) {

                if(data.trim().length == 0){
                  
                  if(checkRequest.trim().length == 0){
                    $("#requestMore").text("No Request").addClass("noRequest");
                    $('#loader-wrapper').hide();
                    return;
                  }
                }
                if(track_page==0){
                    
                    $("#request_id").html('');
                }
                $('#loader-wrapper').hide(); 
                $("#request_id").append(data);

            }
        });
    }

    // accept reject request
    function accept_reject(sent_by,request_id,status){
        if (status == 'Accepted') { st = 'accept'; warn = 'success' ; danger = false;
            var text="Are you sure you want to "+st+" request ?";
         };
        if (status == 'Rejected') { st = 'reject'; warn = 'warning' ; danger = true;
            var text="Are you sure you want to "+st+" request ?";
            };
        if (status == 'unfriend') { st = 'reject'; warn = 'warning' ; danger = true;
            status='Rejected';
        var text="Are you sure you want to unfriend this user ?";
         };

        swal({
            title: "Confirmation",
            text: text,
            icon: warn,
            buttons: true,
            dangerMode: danger,
        })
        .then((willDelete) => {
            if (willDelete) {
                $("#loader-wrapper").show(); 
                $.ajax({
                    type: 'POST',
                    url: "front/Home/accept_reject_request",
                    data: {sent_by:sent_by,request_id:request_id,status:status},
                    dataType: 'json',
                    success:function(resp){
                        $("#loader-wrapper").hide(); 
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                        $(".message_box").html(resp.msg);
                        $(".message_box").show();
                        if(resp.status==0){
                            $(".message_box").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                                setTimeout(function() {
                                 $(".message_box").html('');
                                 $(".message_box").hide();
                                }, 5000); 
                        }else{
                           $(".message_box").html( '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                                setTimeout(function() { 
                                 $(".message_box").html('');
                                 $(".message_box").hide();
                                 location.reload();
                                }, 1000); 
                        }
                        
                    }
                });
            }
        });
    }

    $(document).ready(function () {
        $(".content").slice(0, 4).show();
        $("#loadMore,#loadMore2").on("click", function (e) {
          e.preventDefault();
          $(".content:hidden").slice(0, 1).slideDown();
          if ($(".content:hidden").length == 0) {
            $("#loadMore").text("No Content").addClass("noContent");
          }
        });
        $(".content1").slice(0, 4).show();
        $("#loadMore2").on("click", function (e) {
          e.preventDefault();
          $(".content1:hidden").slice(0, 1).slideDown();
          if ($(".content1:hidden").length == 0) {
            $("#loadMore2").text("No Content").addClass("noContent");
          }
        });
    });
</script>