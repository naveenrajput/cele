<?php //echo '<pre>';print_r($comments);?>
<?php if(!empty($comments)) {
if((COMMENT_LIMIT*($track_page+1))<$comment_total_records){ ?>

<a class="comment_more" comment-attr-id="<?php echo !empty($comments[0]['post_number'])?base64_encode($comments[0]['post_number']):'N/A';?>" href="javascript:void(0)" onclick="load_more_comment(this,'<?php echo $track_page;?>');">View more comment</a>
<?php } $p=generateRandomString(rand(0,12));
foreach($comments as $row){ $p++;
if(empty($row['parent_id'])){ ?>
<div class="parent_comment remove_comment_box" id="remove_comment_<?php echo $p;?>">
    <div class="user-profile">
       <img src="<?php echo !empty($row['profile_picture'])?$row['profile_picture']:'assets/front/images/profile0.png'?>" alt="logo">
   </div>
   <div class="post-text">
        <div class="recentComment">
            <span class="sec-1">
               <h6 class="userName commentUser"><?php if(!empty($row['fullname'])){echo $row['fullname'];}else{ echo 'N/A';}?></h6>
            </span>
            <?php if($row['user_id']==$this->session->userdata("user_id")){ ?>
            <div class="latest-sec-2">
               <div class="dropdown">
                  <a href="javascript:void(0);" class="zigzag" data-toggle="dropdown" aria-expanded="false">
                  <img src="assets/front/images/option.svg" alt="like">
                  </a>
                  <div class="dropmenuContainer1">
                     <div class="dropdown-menu1 dropdown-menu">
                        <a class="dropdown-item" href="javascript:void(0)" comment-attr-id="<?php echo !empty($comments[0]['post_number'])?base64_encode($comments[0]['post_number']):'N/A';?>" remove-id="<?php if(!empty($row['id'])){echo base64_encode($row['id']);}else{ echo 'N/A';} ?>"  onclick="remove_comment(this,'<?php echo $p;?>')"><img
                           src="assets/front/images/form_icons/ic_remove_connection.png"
                           alt="logo"/> Remove Comment</a>
                     </div>
                  </div>
               </div>
            </div>
            <?php } ?>
            <p class="singleComment"><?php if(!empty($row['comment'])){echo $row['comment'];}else{ echo 'N/A';}?>
            </p>
            <ul class="list-inline">
               <li class="list-inline-item">
                  <a href="javascript:void(0)" comm-id="<?php if(!empty($row['id'])){echo base64_encode($row['id']);}else{ echo 'N/A';} ?>" onclick="like_comment(this,'<?php echo $p;?>');">
                  <img src="<?php echo !empty($row['is_liked_by_me'])?'assets/front/images/home/icon/heart.svg': 'assets/front/images/favorite.svg'?>" alt="like" id="com_like_img_<?php echo $p;?>"> 
                     <span ml-1 id="com_like_count_<?php echo $p;?>"><?php echo !empty($row['comment_like'])?$row['comment_like']:'';?></span>
                  </a>
               </li>
               <li class="list-inline-item">
                  <a href="javascript:void(0)" onclick="reply_comment(this);">
                  <img src="assets/front/images/reply.svg" alt="like"> <span
                     ml-1>Reply</span>
                  </a>
               </li>
            </ul>
            <div class="commnetSection2 inner-input reply_comment" style="display: none;">
              <div class="post-text post-text2">
                 <div class="recentComment">
                    <div class="recomment">
                       <input type="text" class="form-control post-title"
                          placeholder="Write a comment..." maxlength="500" id="post_comment_text_<?php echo $p;?>" oninput ="comment_post_btn_action(this,'<?php echo $p;?>')" onkeypress="isNumericKeyPress(event,'<?php echo $p;?>')">
                       <button comment-id="<?php echo !empty($row['post_number'])?base64_encode($row['post_number']):'N/A';?>" reply-comment="<?php echo !empty($row['id'])?base64_encode($row['id']):'N/A';?>" class="send-icon post_comment_btn_<?php echo $p;?>" disabled="true" onclick="post_comment(this,'<?php echo $p;?>',1,'1')"></button>
                    </div>
                 </div>
              </div>
            </div>
        </div>
        <div class="child_comment">
            <?php include APPPATH.'views/front/celebrant/child_comment.php';  ?>
        </div>
   </div>
</div>
<?php } } }?>

<script>
    $(document).ready(function() {
        var chk_session='<?php echo $this->session->userdata('user_id');?>';
        if(chk_session==''){
            window.location.href='login';
        }
    });
    function load_more_child_comment(post_number,track_page){
     var post_num = $(post_number).attr("post-attr-id");
     var parent_id = $(post_number).attr("comment-attr-id");
      $('#loader-wrapper').show();
      $(post_number).html('');
      track_page++;
        $.ajax({
            type:'POST',
            data:{ post_num:post_num,parent_id:parent_id,
                page:track_page,
            },
            url: "<?php echo base_url();?>front/Home/child_comment_list", 
           success:function(data) {
                if(data.trim().length == 0){
                    $('#loader-wrapper').hide();
                    return;
                }
                $('#loader-wrapper').hide();
                $(post_number).parent().prepend(data);
            }
        });
    }
     /*$('.post-title').keydown(function (e){
        if(e.keyCode == 13){
            var str=$(this).attr('id');
            var text=$("#"+str).val().trim();
            var btn_id=str.split("_").pop(-1);
            if(text.length==0){
                $(".post_comment_btn_"+btn_id+"").attr('disabled',true);
                return false;
            }else{
                $(".post_comment_btn_"+btn_id+"").click();
                return false;
            }
        }
    });*/
</script>

