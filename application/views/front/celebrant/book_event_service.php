<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<style>
.navbar {
    position: relative;
    min-height: 50px;
    margin-bottom: 0!important;
    border: 0 none!important;
}
.navbar-brand {
    float: none!important;
    height: auto!important;
    padding: initial;
    line-height: initial;
}
.navbar-collapse.collapse {
    display: flex!important;
}
.user-image {
    margin-top: 8px;
}

</style>
<section class="section-div pt-4 book-service-section book-fixed">
    <div class="container">
         <form action="" id="add_event" enctype="multipart/form-data" data-parsley-validate>
             <div class="row">
                <div class="col-md-8 service-details">
                    <div class="message_box" style="display:none;"></div>
                    <div class="card-banner service-slider-container">
                        <h2 class="py-4 main-heading">Add your event details here</h2>
                        <div class="accordion book-service">
                            <div class="card check-event-details">
                                <div class="row">
                                    <div class="form-group col-md-6">
                                      <img src="assets/front/images/form_icons/ic_event_name.png" class="form-input-icon" alt="mob-img" />
                                      <input type="text" class="form-control" placeholder="Event Title"  name ="event_title" maxlength="200" data-parsley-required data-parsley-required-message="Please enter event title."/>
                                    </div>
                                    <div class="form-group col-md-6">
                                      <img src="assets/front/images/form_icons/ic_full_name.png" class="form-input-icon" alt="mob-img" />
                                      <input type="text" class="form-control" name="members" placeholder="Number of Members"  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" maxlength="5" data-parsley-required data-parsley-required-message="Please enter number of members."/>
                                    </div>
                                    <div class="form-group col-md-6">
                                      <img src="assets/front/images/form_icons/ic_date.png" class="form-input-icon" alt="mob-img" />
                                      <input type="text" class="form-control datepicker" placeholder="Event Date" name="event_date" data-parsley-required data-parsley-required-message="Please select event date." oninput="this.value = this.value.replace(/[^]/g, '').replace(/(\..*)\./g, '$1');"  data-parsley-errors-container="#event_date_err" onchange="$('#event_date_err li').text('');"/>
                                      <div id ="event_date_err"></div>
                                    </div>
                                    <div class="form-group col-md-6 bootstrap-timepicker">
                                        <img src="assets/front/images/form_icons/ic_time.png" class="form-input-icon" alt="mob-img" />
                                        <input type="text" class="form-control timepicker" id="event_time" placeholder="Event Time" name="event_time" data-parsley-required data-parsley-required-message="Please select event time." oninput="this.value = this.value.replace(/[^]/g, '').replace(/(\..*)\./g, '$1');"  data-parsley-errors-container="#event_time_err" onchange="$('#event_time_err li').text('');"/>
                                      <div id ="event_time_err"></div>
                                    </div> 
                                     <div class="form-group col-md-6">
                                        <img src="assets/front/images/form_icons/ic_country.png" class="form-input-icon" alt="mob-img" />
                                        <select class="form-control" name="country" id="sp_country" data-parsley-required data-parsley-required-message="Please select country.">
                                            <option value="">Select Country</option>
                                            <?php if(isset($countries)) {
                                                foreach($countries as $country) { ?>
                                                    <option value=<?php echo $country['id'];?>><?php echo $country['name'];?></option>";
                                                <?php }
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                         <img src="assets/front/images/form_icons/ic_state.png" class="form-input-icon" alt="mob-img" />
                                        <select class="form-control"  name="state" id="sp_state" data-parsley-required data-parsley-required-message="Please select state.">
                                            <option id="sp_first" value="" >Select State</option>
                                         </select>
                                    </div>
                                    <div class="form-group col-md-6">
                                         <img src="assets/front/images/form_icons/ic_state.png" class="form-input-icon" alt="mob-img" />
                                        <select class="form-control" id='sp_city' name="city" data-parsley-required data-parsley-required-message="Please select city.">
                                            <option id='sp_city_first' value="" >Select City</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12">
                                      <img src="assets/front/images/form_icons/ic_event_address.png" class="form-input-icon" alt="mob-img" />
                                      <input type="text" class="form-control google_address" id ="address" name="event_address" placeholder="Address" data-parsley-required data-parsley-required-message="Please enter address." oninput="clear_address()"/>
                                        <p class="error" id="latlong" style="display:none;">Please enter valid address.</p>
                                        <input type="hidden" id="lat" class="lat" name="latitude" value="">
                                        <input type="hidden" id="lng" class="lng" name="longitude" value="">
                                    </div>
                                    <div class="form-group col-md-12">
                                      <img src="assets/front/images/form_icons/ic_note.png" class="form-input-icon" alt="mob-img" />
                                      <textarea class="form-control" name="note" maxlength="200" placeholder="Additional Note (Optional)"></textarea>
                                    </div>
                                    <div class="form-group pl-3 pt-2 mb-5">
                                        <input type="checkbox" id="one" name="event_type">
                                        <label for="one" style="top: -18px;">Only for Friends</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 payment-card">
                    <h2 class="py-4 main-heading">Summary</h2>
                    <?php if(isset($item_details) && !empty($item_details)){ ?>
                        <div class="accordion book-service">
                           <div class="card payment-card-text">
                              <h6><?php echo !empty($sp_info['company_name'])?$sp_info['company_name']:'N/A';?></h6>
                              <p class="pt-2"></p>
                              <table class="table for-dot mb-0">
                                 <tbody>
                                    <tr>
                                       <td class="text-muted">Sub-Total</td>
                                       <td class="text-gray"><?php echo ADMIN_CURRENCY.$total_price;?></td>
                                    </tr>
                                    <tr>
                                       <td class="text-muted">Tax</td>
                                       <td class="text-warning"><?php echo !empty($item_details[0]['tax'])?$item_details[0]['tax'].'%':'';?></td>
                                    </tr>
                                    <?php if(!empty($item_details[0]['discount'])){?>
                                    <tr>
                                       <td class="text-muted">Discount-<?php echo !empty($item_details[0]['discount'])?$item_details[0]['discount'].'% OFF':'';?></td>
                                       <td class="text-gray"><?php echo $deduct_discount=ADMIN_CURRENCY.(($total_price*$item_details[0]['discount'])/100)?></td>
                                    </tr>
                                    <?php } ?>
                                 </tbody>
                                 <tfoot>
                                    <tr>
                                       <th class="text-primary">Total</th>
                                       <th class="text-gray"><?php 
                                       if(!empty($item_details[0]['discount'])){
                                             $discount=$item_details[0]['discount'];
                                             $discounted_sell_price=number_format((($total_price*$discount)/100),2,'.','');
                                             $discount_price=$total_price-$discounted_sell_price;
                                             if($item_details[0]['tax']!='0.00'){
                                                $tax=$item_details[0]['tax'];
                                                $tax_sell_price=number_format((($discount_price*$tax)/100),2,'.','');
                                                $sell_price=$discount_price+$tax_sell_price;
                                             }else{
                                                $sell_price=$discount_price;
                                             }
                                        }else{
                                            $sell_price=$total_price;
                                            if($item_details[0]['tax']!='0.00'){
                                                $tax=$item_details[0]['tax'];
                                                $tax_sell_price=number_format((($total_price*$tax)/100),2,'.','');
                                                $sell_price=$total_price+$tax_sell_price;
                                            }
                                        }

                                       echo ADMIN_CURRENCY.$sell_price;?></th>
                                    </tr>
                                    <tr>
                                       <td colspan="2" class="text-center">
                                          <button class="btn continue-step" id="payment_btn" type="button" onclick="event_submit()">Payment <i class="fa fa-long-arrow-right pl-2" aria-hidden="true"></i></button>
                                       </td>
                                    </tr>
                                 </tfoot>
                              </table>
                           </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </form>
    </div>
</section>
<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo GOOGLE_API_KEY; ?>"></script>
<script>
    $(document).ready(function(){
        $(".datepicker").datepicker({
            todayHighlight: true,
            autoclose: true,
            startDate: '+1d',
            orientation: "bottom auto",
        });
        $('.timepicker').timepicker({
       
            showInputs: false,
            showMeridian:false,
            minuteStep:30,
          
        });

        $("#event_time").val('');
    });
    var options = {
        componentRestrictions: {
            country: 'us'
        }
    };
    var autocompletes = new google.maps.places.Autocomplete(document.getElementsByClassName('google_address')[0],options);

    google.maps.event.addListener(autocompletes, 'place_changed', function () {
        var place = autocompletes.getPlace();
        var lat = place.geometry.location.lat();
        var long = place.geometry.location.lng();
        $("#latlong").hide();
        $('.lat').val(lat);
        $('.lng').val(long);
    });
        $('#sp_country').change(function(e) {
        var id = $("#sp_country").val();
        if(id!='') {
            $('#sp_state option').slice(1).remove();

            $.ajax({
                type:'POST',
                url: "<?php echo base_url(); ?>admin/ajax/get_states/",
                data: {id:id},
                
                success:function(data)
                {
                    if(data) {
                        var states="";
                        data = JSON.parse(data);
                        
                        //sub_outlets += "<option value=''>Select Sub Outlet</option>";
                        for(var i=0;i<data.length;i++) {
                            states += "<option value="+data[i].id+">"+data[i].name+"</option>";
                        }
                        $("#sp_state").find("#sp_first").after(states);
                        $("#sp_city").html('');
                        $("#sp_city").html('<option id="sp_city_first" value="" >Select City</option>');
                    } else {
                        $("#sp_state").find("#sp_first").after('<option value="">No Record</option>');
                    }
                    //console.log(data[0].sub_outlet_id);
                }
            });
        }
    });

    $('#sp_state').change(function(e) {
        var id = $("#sp_state").val();
        if(id!='') {
            $('#sp_city option').slice(1).remove();

            $.ajax({
                type:'POST',
                url: "<?php echo base_url(); ?>admin/ajax/get_cities/",
                data: {id:id},
                
                success:function(data)
                {
                    if(data) {
                        var cities="";
                        data = JSON.parse(data);
                        
                        //sub_outlets += "<option value=''>Select Sub Outlet</option>";
                        for(var i=0;i<data.length;i++) {
                            cities += "<option value="+data[i].id+">"+data[i].name+"</option>";
                        }
                        $("#sp_city").find("#sp_city_first").after(cities);
                    } else {
                        $("#sp_city").find("#sp_city_first").after('<option value="">No Record</option>');
                    }
                }
            });
        }
    }); 
    function clear_address(){ 
        $(".lat").val('');
        $(".lng").val('');
    }

function event_submit(){

    $("#add_event").parsley().validate();
    if($("#add_event").parsley().isValid()){
        $("#loader-wrapper").show();
        var form = $('#add_event')[0];
        var formData = new FormData(form);
        $("#payment_btn").attr('disabled',true);
         $.ajax({
            url: 'create-event',
            type: 'POST',
            data: formData,
            dataType: 'json',
            // async: false,
            cache: false,
            contentType: false,
            processData: false,
    
            success:function(resp){
              $("#loader-wrapper").hide();  
              $("html, body").animate({ scrollTop: 0 }, "slow");
              $(".message_box").show(); 
                if(resp.status==0){
                    $("#payment_btn").attr('disabled',false);
                    $(".message_box").html('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>'+resp.msg+'</div>');
                    setTimeout(function() {
                     $(".message_box").html('');
                      $(".message_box").hide();
                    }, 5000); 
                }else{
                    location.href="make-payment/"+resp.msg;
                }
            },
            error:function(err){
              $("#loader-wrapper").hide();
            }
        });
    }
}


</script>