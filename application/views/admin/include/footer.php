<footer class="main-footer">
 
 
   
<strong>Copyright &copy; <?php echo date('Y');?> All rights reserved - <?php echo SITE_TITLE;?>.</strong>
</footer>

<link href="<?php echo base_url(); ?>assets/admin/css/bootstrap-datepicker.css" rel="stylesheet" />
 </div><!-- ./wrapper -->

    <!-- Bootstrap 3.3.5 -->

    <script src="<?php echo base_url(); ?>assets/admin/bootstrap/js/bootstrap.min.js"></script>

    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>assets/admin/plugins/fastclick/fastclick.min.js"></script>

    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>assets/admin/dist/js/app.min.js"></script>

    <!-- Sparkline -->
    <script src="<?php echo base_url(); ?>assets/admin/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- jvectormap -->

    <script src="<?php echo base_url(); ?>assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

    <!-- SlimScroll 1.3.0 -->

    <script src="<?php echo base_url(); ?>assets/admin/plugins/slimScroll/jquery.slimscroll.min.js"></script>

    <!-- ChartJS 1.0.1 -->
    <script src="<?php echo base_url(); ?>assets/admin/plugins/datatables/jquery.dataTables.min.js"></script>

    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url(); ?>assets/admin/dist/js/demo.js"></script>


  
	 <script src="<?php echo base_url(); ?>assets/js/moment.js"></script>

    <script src="<?php echo base_url(); ?>assets/admin/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- datepicker -->
    <script src="<?php echo base_url(); ?>assets/admin/plugins/datepicker/bootstrap-datepicker.js"></script>

    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/bootstrap-fileupload.js"></script>

	 <script src="<?php echo base_url(); ?>assets/admin/plugins/datatables/jquery.dataTables.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/admin/plugins/datatables/dataTables.bootstrap.min.js"></script> 


<!-- InputMask -->
<script src="<?php echo base_url(); ?>assets/admin/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url(); ?>assets/admin/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script>
  $(function () {
    // $("#datatable1").DataTable();
    $('#datatable1').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": false
    });
  });
  $(function () {
    $('#example1').DataTable();
  });

  $('.datepicker').datepicker({
    format: 'yyyy/mm/dd',
    todayHighlight: true,
    endDate: '+0d',
    orientation: "bottom auto",
  });



function submitDetailsForm(id,seturl='') {
    if(seturl)
    {
        var form_action=seturl;
    }else
    {
        var form_action= "<?php if(!empty($form_action))echo $form_action;?>";
    }
    var last_element = form_action.split("/").pop(-1);
    //alert();
    $("#"+id).parsley().validate();
    var form = $('#'+id)[0];
    if($("#"+id).parsley().isValid()){
      var formData = new FormData(form);
      $("#loader").show(); 
      $("#submit_form").attr('disabled',true);
      $.ajax({
        url: form_action,
        type: 'POST',
        data: formData,
        dataType: 'json',
        // async: false,
        cache: false,
        contentType: false,
        processData: false,
    
        success:function(resp){
          $("#loader").hide();  
          $("html, body").animate({ scrollTop: 0 }, "slow");
          $(".message_box").html(resp.message);
          $(".message_box").show(); 
          $("#submit_form").attr('disabled',false);
            if(resp.status==0){
                setTimeout(function() {
                 $(".message_box").html('');
                  $(".message_box").hide();
                }, 5000); 
              }else{
                // $("#"+id).parsley().reset();
                // $("#"+id)[0].reset();
                setTimeout(function() {
                 $(".message_box").html('');
                  $(".message_box").hide();
                  location.reload();
                }, 2000); 
            }
        },
        error:function(err){
          $("#submit_form").attr('disabled',false);
          $("#loader").hide();

        }
      });
    }
}

function validate_image(input,error_param='') {//alert();
    if(error_param=='')
    {
        var error_container='error1';
    }else{
        var error_container=error_param;
       
    }
    $(input).parent().parent().parent().find('.'+error_container).html('');
    if (!input.files[0].name.toLowerCase().match(/\.(jpg|jpeg|png|JPG|JPEG|PNG|pdf|PDF|DOC|DOCX|doc|docx)$/)) {
        $(input).val("");
        $(input).parent().parent().parent().find('.'+error_container).html("Only JPG|JPEG|PNG|DOC|PDF file types allowed.");
        return false;
    }
    if(input.files[0].size>20000000)
    {
        $(input).parent().parent().parent().find('.'+error_container).html("File size should not be more than 20 MB.");
        return false;
    }  
}


function error_blank_expiry(id) {     
    //alert(id);     
    $("#"+id+" li").text('');
} 



//state city functions
function add_state_city(state_id,boxtype){
  $("#displaystatecityerr").html('');
  $("#loader").show();  
  $.ajax({
    url: "<?php echo site_url('admin/cmscontent/add_state_city_by_ajax');?>",
    type: 'GET',
    cache: false,
    success:function(resp){
      
      $("#loader").hide();  
      $("body").append(resp);
      $("#newstatecitymodal").modal('show');
      $("#add_city_state_form").parsley();
      $("#add_city_state_form").parsley().reset();
      $("#add_city_state_form")[0].reset();
      $("input[name='action_type']").val(boxtype);
      if(boxtype=='state'){
        $("#statecitylabel").html('Add State');
        $("select[name='newstate_id']").removeAttr('data-parsley-required');
        $("select[name='newstate_id']").parent().hide();

        $("input[name='newcity']").removeAttr('data-parsley-required');
        $("input[name='newcity']").parent().hide();

        $("input[name='newstate']").attr('data-parsley-required',true);
        $("input[name='newstate']").parent().show();
      }else{
        get_states('newstate_id',state_id);
        $("#statecitylabel").html('Add City');
        $("select[name='newstate_id']").attr('data-parsley-required',true);
        $("select[name='newstate_id']").parent().show();

        $("input[name='newcity']").attr('data-parsley-required',true);
        $("input[name='newcity']").parent().show();

        $("input[name='newstate']").removeAttr('data-parsley-required');
        $("input[name='newstate']").parent().hide();
      }
    },
    error:function(err){
      $("#loader").hide();
    }
  });
}

function savenewcitystate(){
  $("#displaystatecityerr").html('');
  var form = $('#add_city_state_form')[0];
  if($("#add_city_state_form").parsley().isValid()){
    var formData = new FormData(form);
    $("#loader").show(); 
    $("#save_statecity").attr('disabled',true);
    $.ajax({
      url: "<?php echo site_url('admin/cmscontent/add_state_city_by_ajax');?>",
      type: 'POST',
      data: formData,
      dataType: 'json',
      cache: false,
      contentType: false,
      processData: false,
      success:function(resp){
        $("#loader").hide();  
        $("#displaystatecityerr").html(resp.message);
        $("#displaystatecityerr").show(); 
        $("#save_statecity").attr('disabled',false);
        if(resp.status==1){
          if($("input[name='action_type']").val()=='city'){
            get_cities();
          }else{
            get_states();
          }
          $("#newstatecitymodal").modal('hide');
        }
        setTimeout(function() {
          $("#displaystatecityerr").html('');
          $("#displaystatecityerr").hide();
        }, 2000); 
        
      },
      error:function(err){
        $("#save_statecity").attr('disabled',false);
        $("#loader").hide();
      }
    });
  }
}

function get_cities(){
  var id = $("#state").val();
  if(id!='') {
    $('#city option').slice(1).remove();
    $.ajax({
      type:'POST',
      url: "<?php echo base_url(); ?>admin/ajax/get_cities/",
      data: {id:id},
          
      success:function(data)
      {
        if(data) {
         $('#city option').remove();
          var city_option ="<option value='' id='city_first'>Select City</option>";
          $("#city").append(city_option);

          var cities="";
          data = JSON.parse(data);
          for(var i=0;i<data.length;i++) {
              cities += "<option value="+data[i].id+">"+data[i].title+"</option>";
          }
          //$("#city").find("#city_first").after(cities);
          $("#city").find("#city_first").after(cities);
        } else {
          error_msg = "Some Error occurred. Please try again !!" ;
          error = '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close" type="button">×</button>'+error_msg+'</div>';
          $('#notification_msg').html(error).fadeIn(250).fadeOut(10000);
        }
      }
    });
  }
}

function get_states(container="",secltedid=""){
  var statid="#state";
  if(container!=''){
    statid="#"+container;
  }
  $(statid+' option').remove();
  $.ajax({
    type:'POST',
    url: "<?php echo base_url(); ?>admin/ajax/get_states/",
    data: {},
    success:function(data)
    {
      if(data) {
        var states="";
        data = JSON.parse(data);
        
        $(statid).append("<option value=''>Select State</option>");
        
        for(var i=0;i<data.length;i++) {
          states += "<option value="+data[i].id+">"+data[i].state_name+"</option>";
        }
        
        $(statid).append(states);
        if(container!='' && secltedid!=''){
          $("select[name='"+container+"']").val(secltedid);
        }
        
      } else {
        error_msg = "Some Error occurred. Please try again !!" ;
        error = '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close" type="button">×</button>'+error_msg+'</div>';
        $('#notification_msg').html(error).fadeIn(250).fadeOut(10000);
      }
    }
  });
}




</script>
<script src="<?php echo base_url();?>assets/js/sweetalert.min.js"></script>
</body>

</html> 

