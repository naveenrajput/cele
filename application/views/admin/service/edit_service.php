<div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $page_title;?> </h1>
        <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                <li class="<?php echo $breadcrumb['class'];?>"> 
                    <?php if(!empty($breadcrumb['link'])) { ?>
                        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                    <?php } else {
                        echo $breadcrumb['icon'].$breadcrumb['title'];
                    } ?>
                </li>
            <?php }?>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
     <!--    <div class="box box-primary">  -->
            <!-- /.box-header -->
            <!-- <div class="box-body"> -->
                <div class="row">  
                    <div class="col-md-6">
                        <div class="box box-primary">
                            <!-- /.box-header -->
                             <div class="box-body"> 
                            <!-- flash messages-->
                                <div class="message_box" style="display:none;"></div>
                                <?php if ($this->session->flashdata('error')) { ?>
                                    <div class="alert alert-block alert-danger fade in">
                                        <button data-dismiss="alert" class="close" type="button">×</button>
                                        <?php echo $this->session->flashdata('error') ?>
                                    </div>
                                <?php } ?>
                                <?php if ($this->session->flashdata('success')) { ?>
                                    <div class="alert alert-block alert-success fade in">
                                        <button data-dismiss="alert" class="close" type="button">×</button>
                                        <?php echo $this->session->flashdata('success') ?>
                                    </div>
                                <?php } ?>
                                <div class="">
                                    <div class="panel-body">
                                        <?php if(isset($form_action) && !empty($form_action)){ ?>
                                            <form id="request_type_form" class="" method="POST" action="" enctype="multipart/form-data" role="form"  data-parsley-validate>
                                        <?php } ?>
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label for="title">Title*</label>
                                                <input type="text" class="form-control" name="title" id="title" value="<?php if(!empty($services['title']))echo $services['title']; ?>" placeholder="Title" maxlength="100" data-parsley-required data-parsley-required-message="Please enter title." oninput="title_check();">
                                                 <p class="error" id="un_error_title" style="display:none;">Title already used.</p>
                                                <?php echo form_error('title');?> 
                                            </div>
                                          
                                            <div class="form-group">
                                                <label>Status *</label>
                                                <?php $status=!set_value('status') ? $services['status'] : set_value('status'); 
                                                $options_status = array('Active'  => 'Active','Inactive'  => 'Inactive');
                                                echo form_dropdown('status', $options_status, $status,'class="form-control"');
                                                ?>
                                                <?php echo form_error('status');?>
                                            </div>
                                          
                                        </div>
                                        <div class="box-footer text-center">
                                            <?php if(isset($form_action) && !empty($form_action)){ ?>
                                                <button type="submit" id="submit_form" class="btn btn-primary" onclick="return form_submit('request_type_form');">Update</button>
                                            <?php } ?>
                                            <a href="<?php echo $back_action;?>" class="btn btn-default">Back</a> 
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                      <div class="col-md-6">
                        <div class="box box-primary">
                            <div class="box-body">
                                <form class="form-horizontal">
                                        <input type="hidden" id="record_id" name="record_id" value="<?php echo $services['id']; ?>" />
                                        <input type="hidden" id="table" name="table" value="service_category" />
                                        <input type="hidden" id="upload_path" name="upload_path" value="<?php echo SERVICE_CATEGORY_PATH; ?>" />
                                        <input type="hidden" id="select" name="select" value="image" />
                                        <input type="hidden" id="where" name="where" value="id" />
                                        <input type="hidden" id="height" name="height" value="300" />
                                        <input type="hidden" id="width" name="width" value="700" />
                                        <div class="control-group col-md-6">
                                            <label class="control-label">Image *</label>
                                            <div class="controls">
                                                <div data-provides="fileupload" class="fileupload fileupload-new">
                                                    <div  class="fileupload-new thumbnail">
                                                        <img alt="No Image" src="<?php echo !empty($services['image']) ? base_url().$services['image']:'';?>" >
                                                    </div>
                                                    <div style="min-width: 180px; min-height: 100px; line-height: 5px;" class="fileupload-preview fileupload-exists thumbnail"></div>
                                                    
                                                    <div>
                                                    <?php if(isset($form_action) && !empty($form_action)){ ?>
                                                        <span class="btn btn-file"><span class="fileupload-new btn btn-default">Change image</span>
                                                        <span class="fileupload-exists">Change</span>
                                                        <input type="file" name="image" id="image" class="default" accept="image/*" onchange="validate_images(this);"></span>
                                                        <a data-dismiss="fileupload" class="btn fileupload-exists">Remove</a>
                                                    <?php } ?>
                                                    </div>
                                                    <div id="fimage_error" class="error"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group"></div>
                                            <div class="form-group"></div>
                                            <div class="form-group"></div>
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <button type="button" id="img_upload_button" class="btn btn-info pull-right common_update_image">Save</button>
                                                </div>
                                            </div>
                                            <div class="form-group" >
                                                <div class="col-sm-8">
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar" id="progressBar_image" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12" id="status_image"></div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 text-red text-center" id="error"></div>
                                        <div class="col-sm-12 text-green text-center" id="success"></div>
                                </form>
                            </div><!-- panel body-->
                        </div>
                    </div><!-- col-12-->
                </div><!-- /.box --> 
           <!--  </div> --><!-- col-12--> 
     <!--    </div> --><!-- row--> 
    </section>
</div>
<!-- row--> 
<!-- Include Bootstrap Datepicker -->

<script>
var is_title_name_valid=1;
function title_check(){
    error_msg="";
    $("#un_error_title").hide();
    var matched_value = $("#title").val().trim();
    var matched_column = 'title';
    var matched_id='id';
    var id = "<?php echo $services['id']; ?>";
    var table="service_category";
    if(matched_value!='') {
        $.ajax({
            type:'POST',
            url: "<?php echo base_url(); ?>admin/ajax/check_unique/",
            data: {matched_value:matched_value,matched_column:matched_column,table:table,matched_id:matched_id,id:id},
            
            success:function(data)
            {
                if(data==1) {
                    is_title_name_valid=0;
                    error_msg = "Title already used." ;
                    $("#un_error_title").show();
                } else {
                        $("#un_error_title").hide();
                        is_title_name_valid=1;
                }
                
            }
        });
    }
    else
        {
            is_title_name_valid=1;
            error_msg = "" ;
            $("#un_error_title").hide();
        }
}

function form_submit(id)
{
    
    if(is_title_name_valid==1) {
       submitDetailsForm(id);
        return false;
    } else {
        return false;
    }
    //return false;
}

    

</script>


