<div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $page_title;?> </h1>
        <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                <li class="<?php echo $breadcrumb['class'];?>"> 
                    <?php if(!empty($breadcrumb['link'])) { ?>
                        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                    <?php } else {
                        echo $breadcrumb['icon'].$breadcrumb['title'];
                    } ?>
                </li>
            <?php }?>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-primary"> 
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">  
                    <div class="col-lg-6">
                        <!-- flash messages-->
                        <div class="message_box" style="display:none;"></div>
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('error') ?>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('success')) { ?>
                            <div class="alert alert-block alert-success fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('success') ?>
                            </div>
                        <?php } ?>
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <?php if(isset($form_action) && !empty($form_action)){ ?>
                                    <form id="request_type_form" class="" method="POST" action="" enctype="multipart/form-data" role="form"  data-parsley-validate>
                                <?php } ?>
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="title">Title*</label>
                                        <input type="text" class="form-control" name="title" id="title" value="<?php if(!empty($units['title']))echo $units['title']; ?>" placeholder="Title" maxlength="100" data-parsley-required data-parsley-required-message="Please enter title." oninput="title_check();this.value = this.value.replace(/[^a-zA-Z'_ ]/g,'');">
                                         <p class="error" id="un_error_title" style="display:none;">Measurement unit already used.</p>
                                        <?php echo form_error('title');?> 
                                    </div>
                                  
                                    <div class="form-group">
                                        <label>Status *</label>
                                        <?php $status=!set_value('status') ? $units['status'] : set_value('status'); 
                                        $options_status = array('Active'  => 'Active','Inactive'  => 'Inactive');
                                        echo form_dropdown('status', $options_status, $status,'class="form-control"');
                                        ?>
                                        <?php echo form_error('status');?>
                                    </div>
                                  
                                </div>
                                <div class="box-footer text-center">
                                    <?php if(isset($form_action) && !empty($form_action)){ ?>
                                        <button type="submit" id="submit_form" class="btn btn-primary" onclick="return form_submit('request_type_form');">Update</button>
                                    <?php } ?>
                                    <a href="<?php echo $back_action;?>" class="btn btn-default">Back</a> 
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box --> 
            </div><!-- col-12--> 
        </div><!-- row--> 
    </section>
</div>
<!-- row--> 
<!-- Include Bootstrap Datepicker -->

<script>
var is_title_name_valid=1;
function title_check(){
    error_msg="";
    $("#un_error_title").hide();
    var matched_value = $("#title").val().trim();
    var matched_column = 'title';
    var matched_id='id';
    var id = "<?php echo $units['id']; ?>";
    var table="measurement_unit";
    if(matched_value!='') {
        $.ajax({
            type:'POST',
            url: "<?php echo base_url(); ?>admin/ajax/check_unique/",
            data: {matched_value:matched_value,matched_column:matched_column,table:table,matched_id:matched_id,id:id},
            
            success:function(data)
            {
                if(data==1) {
                    is_title_name_valid=0;
                    error_msg = "Measurement unit already used." ;
                    $("#un_error_title").show();
                } else {
                        $("#un_error_title").hide();
                        is_title_name_valid=1;
                }
                
            }
        });
    }
    else
        {
            is_title_name_valid=1;
            error_msg = "" ;
            $("#un_error_title").hide();
        }
}

function form_submit(id)
{
    
    if(is_title_name_valid==1) {
       submitDetailsForm(id);
        return false;
    } else {
        return false;
    }
    //return false;
}

    

</script>


