
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  	<h1> <?php echo $page_title;?> </h1>
  		<ol class="breadcrumb">
			<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
				<li class="<?php echo $breadcrumb['class'];?>"> 
					<?php if(!empty($breadcrumb['link'])) { ?>
						<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
					<?php } else {
						echo $breadcrumb['icon'].$breadcrumb['title'];
					} ?>
				</li>
			<?php }?>
  		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php if ($this->session->flashdata('error')) { ?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php } ?>
				<?php if ($this->session->flashdata('success')) { ?>
					<div class="alert alert-block alert-success fade in">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
				<?php } ?>
				<div class="box">
					<div class="box-header with-border">  
                  		<!-- <h3 class="box-title">Filter Here</h3>   -->
              			<div class="box-body row"> 
                			<form method="get" action=""> 
                				<div class="form-group col-sm-12"> 
									<div class="form-group"> 
										<div class="col-sm-3">
											<label for="first_name" class="control-label">
												Service Name
											</label><br>
											<?php echo ucfirst($event['service_title'])?> 
										</div> 
										<div class="col-sm-3">
											<label for="first_name" class="control-label">
												Event Creator Name
											</label><br>
											<?php echo ucfirst($event['fullname'])?> 
										</div> 
										<div class="col-sm-3">
											<label for="first_name" class="control-label">
												Service Provider Name  
											</label><br>
											<?php echo ucfirst($event['sp_name'])?> 
										</div> 
										
										<div class="col-sm-3">
											<label for="first_name" class="control-label">
												Created Date
											</label> <br>
											<?php echo convertGMTToLocalTimezone($event['created'],true)?> 
										</div>  
									</div>	
								</div>
								<div class="form-group col-sm-12"> 
									<hr>
								</div>
                				<div class="form-group col-sm-12"> 
									<div class="form-group"> 
										<div class="col-sm-3">
											<label for="first_name" class="control-label">
												Event Number
											</label><br>
											<?php echo ucfirst($event['event_number'])?> 
										</div> 
										<div class="col-sm-3">
											<label for="first_name" class="control-label">
												Event Address
											</label><br>
											<?php echo ucfirst($event['event_address'])?> 
										</div> 
										<div class="col-sm-3">
											<label for="first_name" class="control-label">
												Members
											</label><br>
											<?php echo ucfirst($event['members'])?> 
										</div>  
										<div class="col-sm-3">
											<label for="first_name" class="control-label">
												Event Date & time
											</label> <br>
											<?php echo convertGMTToLocalTimezone($event['event_date'],true)?> 
										</div>  
									</div>	
								</div>	
								<div class="form-group col-sm-12"> 
									<hr>
								</div> 
                				<div class="form-group col-sm-12"> 
									<div class="form-group"> 
										<div class="col-sm-3">
											<label for="first_name" class="control-label">
												Event Country
											</label><br>
											<?php echo ucfirst($event['country_name'])?> 
										</div> 
										<div class="col-sm-3">
											<label for="first_name" class="control-label">
												Event State
											</label><br>
											<?php echo ucfirst($event['state_name'])?> 
										</div> 
										<div class="col-sm-3">
											<label for="first_name" class="control-label">
												Event City
											</label><br>
											<?php echo ucfirst($event['city_name'])?> 
										</div>  
										<div class="col-sm-3">
											<label for="first_name" class="control-label">
												Event Status
											</label><br>
											<b><?php echo ucfirst($event['event_status'])?> </b>
										</div>   
									</div>	
								</div>		
								<div class="form-group col-sm-12"> 
									<hr>
								</div>
						 
                				<div class="form-group col-sm-12"> 
                					<div class="col-sm-12">
										<label for="first_name" class="control-label">
											Event Note
										</label><br> 
										<?php echo $event['note'];?> 	
									</div>	
								</div>	 
								<div class="form-group col-sm-12"> 
									<hr>
								</div> 
								<div class="col-sm-12"> 
											<div class="form-group"> 
												<div class="col-sm-3"> 
												  	<label for="first_name" class="control-label">
												 	Item Name
													</label>  
												</div>  
												<div class="col-sm-3"> 
											 	 	<label for="first_name" class="control-label">
												 	 Price
													</label>  
												</div>
												<div class="col-sm-3"> 
											 	 	<label for="first_name" class="control-label">
												 	 Order Qty
													</label>  
												</div>
												<div class="col-sm-3"> 
											 	 	<label for="first_name" class="control-label">
												 	 Price
													</label>  
												</div>  
													
											</div>	  
										</div>
								
									<?php if(!empty($event_item)){
										foreach ($event_item as $key => $item) { ?>
										<div class="col-sm-12"> 
											<div class="form-group"> 
												<div class="col-sm-3"> 
												 	<?php echo ucfirst($item['item_name'])?>  
												</div>  
												<div class="col-sm-3"> 
												 	<?php echo ADMIN_CURRENCY.' '.$item['price'].'/ '.$item['qty'].' '.$item['unit']?>  
												</div>  
												<div class="col-sm-3"> 
												 	<?php echo $item['customer_qty']?>  
												</div>  
												<div class="col-sm-3"> 
												 	<?php echo ADMIN_CURRENCY.' '.$item['total_price']?>  
												</div>  
												<br>
											</div>	  
										</div>

									<?php } }?> 
			          		</form> 
            			</div>
            			<div class="form-group col-sm-12"> 
									<hr>
								</div> 
            			 	<div class="col-xs-12 text-center">
									   	<?php if(isset($back_action) && !empty($back_action)){ ?>
						                    <a  href="<?php echo $back_action; ?>" class="btn btn-default ">
						                     Back
						                    </a>
					                	<?php } ?> 
									</div>
                	</div> 
 		
				</div>			
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->


