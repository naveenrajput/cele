<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  	<h1> <?php echo $page_title;?> </h1>
  		<ol class="breadcrumb">
			<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
				<li class="<?php echo $breadcrumb['class'];?>"> 
					<?php if(!empty($breadcrumb['link'])) { ?>
						<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
					<?php } else {
						echo $breadcrumb['icon'].$breadcrumb['title'];
					} ?>
				</li>
			<?php }?>
  		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php if ($this->session->flashdata('error')) { ?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php } ?>
				<?php if ($this->session->flashdata('success')) { ?>
					<div class="alert alert-block alert-success fade in">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
				<?php } ?>
				<div class="box">
					 

                	<div class="box-header with-border">  
                  		<!-- <h3 class="box-title">Filter Here</h3>   -->
              			<div class="box-body row"> 
                			<form method="get" action="<?php echo base_url().'admin/subscription/list';?>"> 
	                        	 
							
							 
								<div class="form-group col-md-3">
									<input class="column_filter form-control" oninput="this.value = this.value.replace(/[^A-Za-z0-9-'()&amp; ]/g,'');" name="title" data-column="1" id="col1_filter" type="text" placeholder="Title" value="<?php echo $filter_title?>">
								</div> 
							 
	                        	
				              	<div class="form-group col-md-3">
				              		<select name="status" id="status" class="column_filter form-control">
				              			<option value="">Status</option>
				              			<option value="Active" <?php if(!empty($filter_status)&& $filter_status=='Active'){ echo 'selected'; }?>>Active</option>
				              			<option value="Inactive" <?php if(!empty($filter_status)&& $filter_status=='Inactive'){ echo 'selected'; }?>>Inactive</option>
				              		</select> 
	                        	</div> 
				             	<div class="form-group col-md-3">
				               		<input class="btn btn-primary" type="submit" value="Filter">
				               		<a class="btn btn-default" href="<?php echo base_url().'admin/subscription/list';?>">Reset</a>
				             	</div>
				          
			          	</form> 
		          		<?php if(isset($add_action) && !empty($add_action)){ ?>
		          			<!-- <div class="form-group col-md-offset-0 col-md-3">
                  				<a href="<?php //echo $add_action;?>" title="" data-toggle="tooltip" data-original-title="Add Subscription" class="btn btn-primary pull-right"><i class="fa fa-plus"></i></a>
              				</div> -->
                		<?php } ?>  
            			</div>
                	</div> 

					<div class="box-body">

						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th> 
									<th>Title</th> 
									<th>Duration</th> 
									<th>Amount</th>
									<th>Orders</th>
										<?php if(isset($edit_action) && !empty($edit_action)){ ?>
									<th>Status</th> 
									<th>Actions</th>
									<?php } ?>
								</tr>
							</thead>
							<tbody>
	

								<?php 
								if(!empty($records_results))
								{	
									$i = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
									$table="subscriptions";
									$field = "id";

									foreach ($records_results as $row) { $i++; 
										if(isset($row['status'])) {
	                                        if($row['status']=="Active") {
	                                            $status = "Active";
	                                            $class = "pointer badge bg-green";
	                                        } else {
	                                            $status = "Inactive";
	                                            $class = "pointer badge bg-red";
	                                        }
	                                        
                                    	}?>
										<tr id="tr_<?php echo $row[$field]; ?>">
											<td><?php echo $i; ?></td> 
											
											<td><?php echo $row['title']?></td>
											<td><?php echo $row['time_value'].' '.$row['time_type']?></td> 
											<td><?php echo $row['amount']?></td>
											<td><?php echo $row['orders']?></td>
											<?php if(isset($edit_action) && !empty($edit_action)){ ?>
												<td>
													<p id="status_<?php echo $row[$field]; ?>" onclick="change_status('<?php echo $field; ?>','<?php echo $row[$field]; ?>','<?php echo $table; ?>')" class="<?php echo $class; ?>" title="" data-toggle="tooltip" data-original-title="Change Status"><?php echo $status; ?></p>
												</td>
											<?php } ?>
											<td class="td-actions">
												<?php if(isset($edit_action) && !empty($edit_action)){ ?>
													<a id="" href="<?php echo $edit_action.'/'.$row[$field]; ?>" class="btn btn-xs btn-primary" title="" data-toggle="tooltip" data-original-title="Edit">
														<i class="fa fa-pencil"></i>
													</a>
												<?php }  
												 if(isset($delete_action) && !empty($delete_action)) { 
												 	if(!$this->Common_model->getRecords('user_subscription_history','*',array('plan_id'=>$row[$field]),'',true)){
												 	?> 
													<button class="btn btn-xs bg-red delete" data-toggle="tooltip" data-original-title="Delete" onclick="return soft_delete_record(<?php if(!empty($row[$field])) echo $row[$field]; ?>,'<?php echo $table; ?>','<?php echo $field; ?>');"><i class="fa fa-trash-o"></i></button>
												<?php
												}
												} ?>
											</td>
										</tr>

									<?php }
								} else {
									echo "<tr><td colspan='10' align='center'> No Record Found</td></tr>";
								} ?>
							</tbody>
							<tfoot>						
								<tr>
									<?php if(!empty($pagination)) { ?>
										<td colspan="2" >Total Records - <?php echo $total_records;?></td>
										<td colspan="10" align="center">
											<div><?php echo $pagination; ?></div>
										</td>
									<?php }else{ ?>	
										<td colspan="2">Total Records - <?php if($total_records >0){echo $total_records;} else{echo '0';}?></td>
										<td colspan="10" align="center"></td>
									<?php } ?>			
								</tr>
							</tfoot>
						</table>
					</div>			
				</div>			
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->
