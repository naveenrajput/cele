<!-- Content Wrapper. Contains page content  -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $page_title;?> </h1>
        <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                <li class="<?php echo $breadcrumb['class'];?>"> 
                    <?php if(!empty($breadcrumb['link'])) { ?>
                        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                    <?php } else {
                        echo $breadcrumb['icon'].$breadcrumb['title'];
                    } ?>
                </li>
            <?php }?>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-primary"> 
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- flash messages-->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('error') ?>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('success')) { ?>
                            <div class="alert alert-block alert-success fade in">
                            <button data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo $this->session->flashdata('success') ?>
                        </div>
                        <?php } ?>
                 
                        <div class="panel">
                            <div class="">
                                <?php if(isset($from_action) && !empty($from_action)){ ?>
                                    <form method="POST" id="add_support_form" action="<?php echo $from_action; ?>" role="form"  onsubmit="return form_submit('add_support_form');" data-parsley-validate>
                                <?php } ?>
                                
                                    <div class="">
                                       
                                        <div class="form-group">
                                            <label for="subject">Subject *</label>
                                            <input type="text" class="form-control parsley-error" name="subject" id="subject" placeholder="Subject" data-parsley-required data-parsley-required-message="Please enter subject." data-parsley-errors-container="#subject_error" value="<?php if(set_value('subject')) echo set_value('subject');?>" maxlength="150">
                                            <p class="error" id="subject_error"></p>
                                            <?php echo form_error('subject');?> 
                                        </div>
                                        <div class="form-group">
                                            <label for="subject">Contact No. </label>
                                            <input type="text" class="form-control" id="contact_no"  maxlength="12"  name="contact_no" oninput="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" placeholder="Contact Number" value="" data-parsley-type="number" minlength="10" maxlength="12" data-parsley-minlength-message="Please enter valid phone number" data-parsley-maxlength-message="Please enter valid phone number">

                                            <p class="error" id="subject_error"></p>
                                            <?php echo form_error('subject');?> 
                                        </div>
                                        <div class="form-group">
                                            <label for="message">Message *</label>
                                            <textarea class="form-control parsley-error" name="message" id="ckeditor" placeholder="Message" rows="5" data-parsley-id="5" data-parsley-required data-parsley-required-message="Please enter message." data-parsley-errors-container="#msg_error"><?php if(set_value('message')) echo set_value('message');?> </textarea>
                                            <p class="error" id="msg_error"></p>
                                            <?php echo form_error('message');?> 
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-12 text-right">
                                                    <?php if(isset($from_action) && !empty($from_action)){ ?>
                                                        <button type="submit" id="reply" class="btn btn-primary">Submit</button>
                                                    <?php } ?>
                                                    <a href="<?php echo $back_action;?>" class="btn btn-default">Back</a> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div><!-- panel body--> 
                        </div><!-- end panel --> 
                    </div><!-- col-6--> 
                </div><!-- row--> 
            </div><!-- /.box-body --> 
        </div><!-- /.box --> 
    </section><!-- /.content --> 
</div><!-- /.content-wrapper -->

<script src="<?php echo base_url(); ?>assets/admin/js/ckeditor/ckeditor.js"></script> 
<script>


$(function () {
    // Replace the <textarea id="ckeditor"> with a CKEditor
    CKEDITOR.replace('ckeditor');
        $("#reply").click(function(){
        for (var i in CKEDITOR.instances){
            CKEDITOR.instances[i].updateElement();
      }
    });
});

function form_submit(id)
{
    $("#"+id).parsley().validate();
    if($("#"+id).parsley().isValid()){
       //$("#reply").attr('disabled',true);
       $("#loader").show(); 
       return true;
    }else{
        return false;
    }
}
</script>