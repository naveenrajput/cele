<style type="text/css">
.heading_title{
	position: relative;
    top: -36px;
    left: 12px;
}
.heading_title_child{
	position: relative;
    top: -14px;
    left: 12px;
    border: 1px dotted;
    padding: 8px;
    width: 22%;
    background-color: #226a8b;
    color: #fff;
    border-radius: 11px;
}
.heading_title_child2{
	text-align:center;
	float: right;
    position: relative;
    bottom: 54px;
    width: 15%;
    border: 1px dotted;
    padding: 8px;  
    color: #fff;
    border-radius: 11px;
}
div#history_section hr {
    margin-bottom: -20px !important;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  	<h1> <?php echo $page_title;?> </h1>
  		<ol class="breadcrumb">
			<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
				<li class="<?php echo $breadcrumb['class'];?>"> 
					<?php if(!empty($breadcrumb['link'])) { ?>
						<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
					<?php } else {
						echo $breadcrumb['icon'].$breadcrumb['title'];
					} ?>
				</li>
			<?php }?>
  		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php if ($this->session->flashdata('error')) { ?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php } ?>
				<?php if ($this->session->flashdata('success')) { ?>
					<div class="alert alert-block alert-success fade in">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
				<?php } ?>
				<div class="box">
					<div class="box-header with-border">  
                  		<!-- <h3 class="box-title">Filter Here</h3>   -->
              			<div class="box-body row"> 
                			<form method="get" action=""> 
                				<div class="form-group col-sm-12"> 
									<div class="form-group"> 
										<div class="col-sm-2">
											<label for="first_name" class="control-label">
												Order Number
											</label><br>
											<?php echo ucfirst($order['order_number'])?> 
										</div> 
										<div class="col-sm-2">
											<label for="first_name" class="control-label">
												Event Number
											</label><br>
											<?php echo ucfirst($order['event_number'])?> 
										</div> 
										<div class="col-sm-2">
											<label for="first_name" class="control-label">
												Payment Status
											</label><br>
											<b><?php echo ucfirst($order['payment_status'])?>  </b>
										</div> 
										<div class="col-sm-2">
											<label for="first_name" class="control-label">
												Order Status
											</label><br>
											<b><?php echo ucfirst($order['order_status'])?>  </b>
										</div>

										<div class="col-sm-2">
											<label for="first_name" class="control-label">
												Event Status
											</label><br>
											<b><?php echo ucfirst($order['event_status'])?> </b>
										</div> 
										
										<div class="col-sm-2">
											<label for="first_name" class="control-label">
												Service Name
											</label><br>
											<?php echo ucfirst($order['service_title'])?> 
										</div>   
									</div>	
								</div>
								<div class="form-group col-sm-12"> 
									<hr>
								</div>
								<div class="form-group col-sm-12"> 
									<div class="form-group">  

										<div class="col-sm-3">
											<label for="first_name" class="control-label">
												Event Creator Name
											</label><br>
											<?php echo ucfirst($order['ev_name']).' ('.$order['mobile'].')'?> 
										</div> 
										<div class="col-sm-3">
											<label for="first_name" class="control-label">
												Service Provider Name  
											</label><br>
											<?php echo ucfirst($order['sp_name'])?> 
										</div> 
										<div class="col-sm-3">
											<label for="first_name" class="control-label">
												Service Provider Address  
											</label><br>
											<?php echo ucfirst($order['sp_address'])?> 
										</div> 
										 
										<div class="col-sm-2">
											<label for="first_name" class="control-label">
												Event Date & time
											</label> <br>
											<?php echo convertGMTToLocalTimezone($order['event_date'],true)?> 
										</div> 
										
									</div>	
								</div>		
								<div class="form-group col-sm-12"> 
									<hr>
								</div>
								<div class="form-group col-sm-12"> 
								<div class="form-group"> 

										<div class="col-sm-2">
											<label for="first_name" class="control-label">
												Event Country
											</label><br>
											<?php echo ucfirst($order['country_name'])?> 
										</div> 
										<div class="col-sm-2">
											<label for="first_name" class="control-label">
												Event State
											</label><br>
											<?php echo ucfirst($order['state_name'])?> 
										</div> 
										<div class="col-sm-2">
											<label for="first_name" class="control-label">
												Event City
											</label><br>
											<?php echo ucfirst($order['city_name'])?> 
										</div> 
										<div class="col-sm-2">
											<label for="first_name" class="control-label">
												Event Address<a target="_blank" href="https://www.google.com/maps/dir/?api=1&origin=<?php if(!empty($order['sp_address'])) echo $order['sp_address'];?>&destination=<?php if(!empty($order['event_address'])) echo $order['event_address'];?>"><i class="fa fa-map-marker pull-right" aria-hidden="true" style="font-size:30px;color:red;margin-top: -9px;"></i></a>
											</label><br>
											<?php echo ucfirst($order['event_address'])?> 
										</div>  

										<div class="col-sm-2">
											<label for="first_name" class="control-label">
												Members
											</label><br>
											<?php echo ucfirst($order['members'])?> 
										</div> 
										<div class="col-sm-2">
											<label for="first_name" class="control-label">
												Ip Address
											</label><br>
											<?php echo ucfirst($order['ip_address'])?> 
										</div> 
										 
									</div>	
								</div>
								<div class="form-group col-sm-12"> 
									<hr>
								</div>
								<div class="form-group col-sm-12"> 
									<div class="col-sm-2">
										<label for="first_name" class="control-label">
											Payment Date & time
										</label> <br>
										<?php echo convertGMTToLocalTimezone($order['payment_date'],true)?> 
									</div>

									<div class="col-sm-2">
										<label for="first_name" class="control-label">
											Created Date & time
										</label> <br>
										<?php echo convertGMTToLocalTimezone($order['created'],true)?> 
									</div>  
								</div>
								<div class="form-group col-sm-12"> 
									<hr>
								</div>

						 
                				<div class="form-group col-sm-12"> 
                					<div class="col-sm-12">
										<label for="first_name" class="control-label">
											Event Note
										</label><br> 
										<?php echo $order['note'];?> 	
									</div>	
								</div>	 
						 		<?php if($order['order_status']=='Canceled'){?>

								<div class="form-group col-sm-12"> 
									<hr>
								</div> 
                				<div class="form-group col-sm-12"> 
                					<div class="col-sm-6">
										<label for="first_name" class="control-label">
											Cancel reason
										</label><br> 
										<?php echo $order['cancel_reason'];?> 	
									</div>
                					<div class="col-sm-6">
										<label for="first_name" class="control-label">
											Canceled By
										</label><br> 
										<?php 
										echo $order['cancel_name'];
										?> 	
									</div>	
								</div>	 
							
								<?php }?> 
									<div class="form-group col-sm-12"> 
								<hr>
								</div> 
								<div class="col-sm-12"> 
								<?php 

										$item = $order_total;
										if($item['status']=='Accepted'){
            								$background_color = '#188425';
            							}elseif($item['status']=='Pending'){
            								$background_color = '#226a8b';
            							}else{
            								$background_color = '#c11e1e';
            							}
								?>
										<?php /*<div class="heading_title_child updated_record "><b>Updated :</b> <?php echo convertGMTToLocalTimezone($item['created'],true)?>
										</div> */?>
										<?php if(!empty($item['payment_id'])){ ?>
											<a target="_" href="<?php echo base_url().'admin/payment/createInvoice/'.$item['payment_id']?>"><div class="heading_title_child2 status_record" style="background-color: <?php echo $background_color?>" ><b>Payment Invoice </b></div>
											</a> 
										<?php }?>
										<?php /*<div class="heading_title_child2 status_record" style="background-color: <?php echo $background_color?>" ><b>Status :</b> <?php echo $item['status'];?></div>*/?>
												
										<div class="form-group"> 
											<div class="col-sm-2"> 
											  	<label for="first_name" class="control-label">
											 	Item Name
												</label>  
											</div>  
											<div class="col-sm-2"> 
										 	 	<label for="first_name" class="control-label">
											 	 Price
												</label>  
											</div>
											<div class="col-sm-1"> 
										 	 	<label for="first_name" class="control-label">
											 	  Qty
												</label>  
											</div>
											<div class="col-sm-1"> 
										 	 	<label for="first_name" class="control-label">
											 	 Price
												</label>  
											</div>  
											<div class="col-sm-2"> 
										 	 	<label for="first_name" class="control-label">
											 	 Discount
												</label>  
											</div>  
											<div class="col-sm-2"> 
										 	 	<label for="first_name" class="control-label">
											 	 Tax
												</label>  
											</div>   
											<div class="col-sm-2"> 
										 	 	<label for="first_name" class="control-label">
											 	 Sell Price
												</label>  
											</div>  
												
										</div>	  
									</div>
							
									<?php if(!empty($order_item)){
										foreach ($order_item as $key => $item) { ?>
										<div class="col-sm-12"> 
											<div class="form-group"> 
												<div class="col-sm-2"> 
												 	<?php echo ucfirst($item['item_name'])?>  
												</div>  
												<div class="col-sm-2"> 
												 	<?php echo ADMIN_CURRENCY.' '.number_format((float)$item['price'], 2, '.', '').'/ '.$item['qty'].' '.$item['unit']?>  
												</div>  
												<div class="col-sm-1"> 
												 	<?php echo $item['customer_qty']?>  
												</div>  
												<div class="col-sm-1"> 
												 	<?php echo ADMIN_CURRENCY.' '.number_format((float)$item['total_price'], 2, '.', '')?>  
												</div>  
												<div class="col-sm-2"> 
												 	<?php 
												 	$discount_value  = '0';
												 	if(!empty($item['discount'])){ 

											 			$discount_value = ($item['total_price']*$item['discount'])/100 ;
												 		echo ' ('.$item['discount'].'%) ' .ADMIN_CURRENCY.''.number_format( (float)$discount_value, 2, '.', ''); }?>  
												</div> 
												<div class="col-sm-2"> 
												 		<?php if(!empty($item['tax'])){ 

												 			$tax_value = (($item['total_price']-$discount_value)*$item['tax'])/100;

												 			echo ' ('.$item['tax'].'%) ' .ADMIN_CURRENCY.''.number_format((float)$tax_value, 2, '.', ''); }?>  
												</div> 
												<div class="col-sm-2"> 
												 	<?php echo ADMIN_CURRENCY.' '.number_format((float)$item['sell_price'], 2, '.', '')?>  
												</div>  
												<br>
											</div>	  
										</div>

									<?php } }?>
									<div class="form-group col-sm-12"> 
										<hr>
									</div> 
									<?php 
									// echo "<pre>";print_r($order_total);die;
									if(!empty($order_total)){
										$item = $order_total;
										// foreach ($order_total as $key => $item) { 
											if($item['value']!='0.00'){
											?> 
												<div class="col-sm-12">  
													<div class="form-group"> 
														<div class="col-sm-2">  
														</div> 
														<div class="col-sm-2">  
														</div>  
														<div class="col-sm-1">  
														</div> 
														<div class="col-sm-1">  
														</div> 
														<div class="col-sm-2">  
														</div>  
														<div class="col-sm-2"> 
														 	<b><?php echo $item['title']?>  </b>
														</div>  
														<?php if($item['code']=='discount'){?>
														<div class="col-sm-2"> 
														 	<?php echo '- '.ADMIN_CURRENCY.' '.number_format((float)$item['value'], 2, '.', '')?>  
														</div>  
														<?php }else{?>
														<div class="col-sm-2"> 
														 	<?php echo ADMIN_CURRENCY.' '.number_format((float)$item['value'], 2, '.', '')?>  
														</div>  
														<?php }?>
														<br>
													</div>	  
												</div> 
									<?php }  }?>  
			          		</form>  
            			</div>
            				<div id="history_section" style="display: none;">
            				<div class="form-group col-sm-12"> 
									<hr>
								</div>   
            					<?php 
            					if(!empty($history_array)){
            						foreach ($history_array as $key => $list) { 
            							if($list['status']=='Accepted'){
            								$background_color = '#188425';
            							}elseif($list['status']=='Pending'){
            								$background_color = '#a2a20f';
            							}else{
            								$background_color = '#c11e1e';
            							}
            							?>  
										<div class="col-sm-12"> 
											<div class="heading_title_child updated_record "><b>Updated :</b> <?php echo convertGMTToLocalTimezone($list['date'],true)?></div>
											<?php if(!empty($list['payment_id'])){ ?>
												<a target="_" href="<?php echo base_url().'admin/payment/createInvoice/'.$list['payment_id']?>"><div class="heading_title_child2 status_record" style="background-color: <?php echo $background_color?>" ><b>Payment Invoice </b></div>
												</a> 
											<?php }?>  
											<div class="heading_title_child2 status_record" style="background-color: <?php echo $background_color?>" ><b>Status :</b> <?php echo $list['status'];?></div>
											<div class="form-group"> 
												<div class="col-sm-2"> 
												  	<label for="first_name" class="control-label">
												 		Item Name
													</label>  
												</div>  
												<div class="col-sm-2"> 
											 	 	<label for="first_name" class="control-label">
												 	 Price
													</label>  
												</div>
												<div class="col-sm-1"> 
											 	 	<label for="first_name" class="control-label">
												 	  Qty
													</label>  
												</div>
												<div class="col-sm-1"> 
											 	 	<label for="first_name" class="control-label">
												 	 Price
													</label>  
												</div>  
												<div class="col-sm-2"> 
											 	 	<label for="first_name" class="control-label">
												 	 Discount
													</label>  
												</div>  
												<div class="col-sm-2"> 
											 	 	<label for="first_name" class="control-label">
												 	 Tax
													</label>  
												</div>   
												<div class="col-sm-2"> 
											 	 	<label for="first_name" class="control-label">
												 	 Sell Price
													</label>  
												</div>   
													
											</div>	  
										</div>
										<?php if(!empty($list['sub_record'])){
										foreach ($list['sub_record'] as $key => $item) { 
											if($item['action']=='Remove'){ ?>
											<div class="col-sm-12" style="color:red">  
											<?php }else{ ?>
											<div class="col-sm-12"> 
											<?php }	?> 
											<div class="form-group"> 
												<div class="col-sm-2"> 
												 	<?php echo ucfirst($item['item_name'])?>  
												</div>  
												<div class="col-sm-2"> 
												 	<?php echo ADMIN_CURRENCY.' '.number_format((float)$item['price'], 2, '.', '').'/ '.$item['qty'].' '.$item['unit']?>  
												</div>  
												<div class="col-sm-1"> 
												 	<?php echo $item['customer_qty']?>  
												</div>  
												<div class="col-sm-1"> 
												 	<?php echo ADMIN_CURRENCY.' '.number_format((float)$item['total_price'], 2, '.', '')?>  
												</div>  
												<div class="col-sm-2"> 
												 	<?php 
												 	if(!empty($item['discount'])){ 
											 			$discount_value = ($item['total_price']*$item['discount'])/100 ;
												 		echo ' ('.$item['discount'].'%) ' .ADMIN_CURRENCY.' '.number_format((float)$discount_value, 2, '.', ''); }?>  
												</div> 
												<div class="col-sm-2"> 
												 		<?php if(!empty($item['tax'])){ 
												 			$tax_value = (($item['total_price']-$discount_value)*$item['tax'])/100;

												 			echo ' ('.$item['tax'].'%) ' .ADMIN_CURRENCY.' '.number_format((float)$tax_value, 2, '.', ''); }?>  
												</div> 
												<div class="col-sm-2"> 
												 	<?php echo ADMIN_CURRENCY.' '.number_format((float)$item['sell_price'], 2, '.', '')?>  
												</div>   
											<!-- 	<div class="col-sm-2"> 
												 	<?php echo $item['action']?>  
												</div>  --> 
												<br>
											</div>	  
										</div> 
									<?php } }?>

								
										<?php 
									// echo "<pre>";print_r($order_total);die;
									if(!empty($list['order_total'])){
										
										foreach ($list['order_total'] as $key => $item) { 
											if($item['value']!='0.00'){
										?>

										<div class="col-sm-12"> 
											<div class="form-group"> 
												<div class="col-sm-2">  
												</div> 
												<div class="col-sm-2">  
												</div>  
												<div class="col-sm-1">  
												</div> 
												<div class="col-sm-1">  
												</div> 
												<div class="col-sm-2">  
												</div>  
												<div class="col-sm-2"> 
												 	<b><?php echo $item['title']?>  </b>
												</div>  
												<?php if($item['code']=='discount'){?>
												<div class="col-sm-2"> 
												 	<?php echo '- '.ADMIN_CURRENCY.' '.number_format((float)$item['value'], 2, '.', '')?>  
												</div>  
												<?php }else{?>
												<div class="col-sm-2"> 
												 	<?php echo ADMIN_CURRENCY.' '.number_format((float)$item['value'], 2, '.', '')?>  
												</div>  
												<?php }?>
												<br>
											</div>	  
										</div> 
									<?php } } }?>  
									<?php } } ?>
							</div>  
							<div class="col-xs-12 text-center"> 
									<?php if(!empty($history_array)){?>
									<div class="btn btn-success " style="float: left;" onclick="show_history()">History</div>
									<?php }?>
							   		<?php if(isset($back_action) && !empty($back_action)){ ?>
							        <a style="margin-right: 112px;" href="<?php echo $back_action; ?>" class="btn btn-default ">
							         Back
							        </a>
								<?php } ?> 
							</div>
                	</div> 
 		
				</div>			
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->


<script type="text/javascript">
	function show_history(){
		
		$("#history_section").toggle(500);
		 $("html, body").animate({ scrollTop: 1000 }, 800);



	}




</script>>