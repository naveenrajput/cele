<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  	<h1> <?php echo $page_title;?> </h1>
  		<ol class="breadcrumb">
			<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
				<li class="<?php echo $breadcrumb['class'];?>"> 
					<?php if(!empty($breadcrumb['link'])) { ?>
						<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
					<?php } else {
						echo $breadcrumb['icon'].$breadcrumb['title'];
					} ?>
				</li>
			<?php }?>
  		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12 message_box">
				<?php if ($this->session->flashdata('error')) { ?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php } ?>
				<?php if ($this->session->flashdata('success')) { ?>
					<div class="alert alert-block alert-success fade in">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
				<?php } ?>
				<div class="box">
					<div class="box-header with-border">  
                  		<!--<h3 class="box-title">Filter Here</h3>-->  
                		<form method="get" action="<?php echo $action_url;?>"> 
						<div class="box-body row"> 
							<div class="form-group col-md-3">
								<input class="column_filter form-control" id="userfullname" name='userfullname' type="text" placeholder="User Name" value="<?php echo $filter_user_fullname;?>">
							</div>
			              	<div class="form-group col-md-3">
                                <input class="column_filter form-control" id="post_number" name='post_number' type="text" placeholder="Post Number" value="<?php echo $filter_post_number;?>">
			              	</div>
				             <div class="form-group col-md-3">
				               	<input class="btn btn-primary" type="submit" value="Filter">
				               	<a class="btn btn-default" href="<?php echo $action_url;?>">Reset</a>
				               	<!-- <a class="btn btn-success" href="<?php echo $back_url;?>">Back</a> -->
				             </div> 
			          	</div>
			          </form> 
			            <?php if(isset($add_action) && !empty($add_action)){ ?>
                  			<!-- <a href="<?php echo $add_action;?>" title="" data-toggle="tooltip" data-original-title="Add User" class="btn btn-default pull-right"><i class="fa fa-plus"></i></a> -->
                		<?php } ?>
                	</div> 

					<div class="box-body">

						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Report By</th>
									<th>Post Number</th> 
									<th>Post Content</th>
									<th>Post Owner Name</th> 
									<th>Comment</th> 
									<th>Report Date</th> 
									<th>Post Status</th> 
									<th>Post Owner Status</th> 
									<th>Action</th> 
								
								</tr>
							</thead>
							<tbody> 

								<?php 
								// echo "<pre>";print_r($records_results);die;
								if(!empty($records_results))
								{	
									$i = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
									 
									foreach ($records_results as $row) { $i++; 

										if(isset($row['status'])) {
	                                        if($row['status']=="Active") {
	                                            $status = "Active";
	                                            $class = "pointer badge bg-green";
	                                        }elseif($row['status']=="Unverified") {
	                                            $status = "Unverified";
	                                            $class = "pointer badge bg-yellow";
	                                        } else {
	                                            $status = "Inactive";
	                                            $class = "pointer badge bg-red";
	                                        }
                                    	}

                                    	if(isset($row['post_user_status'])) {
	                                        if($row['post_user_status']=="Active") {
	                                            $status_s = "Active";
	                                            $class_s = "pointer badge bg-green";
	                                        }else {
	                                            $status_s = "Inactive";
	                                            $class_s = "pointer badge bg-red";
	                                        }
                                    	}
								 	?>
										<tr id="tr_<?php echo $row['id']; ?>">
											<td width="5%"><?php echo $i; ?></td>
											<td><?php if(!empty($row['fullname'])) echo ucfirst($row['fullname']);?></td>
											<td><?php if(!empty($row['post_number'])) echo ucfirst($row['post_number']);?></td> 
											<td>
												<?php if(!empty($row['post_content']))  if(strlen($row['post_content']) > 25) {echo substr($row['post_content'],0,25).'...';}else{ echo $row['post_content'];} ?>
											</td> 
											<td><?php if(!empty($row['postusername'])) echo ucfirst($row['postusername']);?></td> 
											
											<td><?php if(!empty($row['comment'])) echo ucfirst($row['comment']);?></td>  
											<td><?php echo convertGMTToLocalTimezone($row['created'],true)?></td> 
											<td>
												<a href="<?php echo base_url().'admin/post/list?post_id='.$row['post_id']?>"><p><?php echo 'Change Status'; ?></p></a>
											</td> 
											<td>
												<a href="<?php echo base_url().'admin/user/list?user_id='.$row['post_user_user_id']?>"><p><?php echo 'Change Status'; ?></p></a>
											</td> 
											<td>
												<a  class="btn btn-xs btn-success" title="Send Warning mail" href="<?php echo base_url().'admin/report/reply/'.$row['id']?>"><i class="fa fa-reply"></i></a>
											</td> 
										</tr>
									<?php }
								} else {
									echo "<tr><td colspan='9' align='center'> No Record Found</td></tr>";
								} ?>
							</tbody>
							<tfoot>						
								<tr>
									<?php if(!empty($pagination)) { ?>
										<td colspan="6">Total Records - <?php echo $total_records;?></td>
										<td colspan="6" align="center">
											<div><?php echo $pagination; ?></div>
										</td>
									<?php }else{ ?>	
										<td  colspan="6" >Total Records - <?php if($total_records >0){echo $total_records;} else{echo '0';}?></td>
										<td colspan="6" align="center"></td>
									<?php } ?>			
								</tr>
							</tfoot>	
						</table>
					</div>			
				</div>	
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->