<div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $page_title;?> </h1>
        <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                <li class="<?php echo $breadcrumb['class'];?>"> 
                    <?php if(!empty($breadcrumb['link'])) { ?>
                        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                    <?php } else {
                        echo $breadcrumb['icon'].$breadcrumb['title'];
                    } ?>
                </li>
            <?php }?>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-primary"> 
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">  
                    <div class="col-lg-12">
                        <!-- flash messages-->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('error') ?>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('success')) { ?>
                            <div class="alert alert-block alert-success fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('success') ?>
                            </div>
                        <?php } ?>
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <?php if(isset($from_action) && !empty($from_action)){ ?>
                                <form id="offers_form"class="" method="POST" action="<?php echo $from_action; ?>" enctype="multipart/form-data" role="form"  data-parsley-validate>
                                <?php } ?>
                                    <div class="box-body">
                                      
                                        <div class="col-sm-12" id="time">
                                            
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="time_type">Duration Type *</label>
                                                    <select disabled name="time_type" id="time_type" class="column_filter form-control ct">
                                                       <option value="Day" <?php if(!empty($subscription['time_type'])&& $subscription['time_type']=='Day'){ echo 'selected'; }?>>Day</option>
                                                        <option value="Month" <?php if(!empty($subscription['time_type'])&& $subscription['time_type']=='Month'){ echo 'selected'; }?>>Month</option>
                                                        <option value="Year" <?php if(!empty($subscription['time_type'])&& $subscription['time_type']=='Year'){ echo 'selected'; }?>>Year</option>
                                                    </select> 
                                                    <?php echo form_error('time_type');?> 
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="time_value" >Time Value *</label>
                                                  
                                                        <input disabled type="text" class="form-control value>" id="time_value" name="time_value" placeholder="Time Value" value="<?php if(isset($subscription['time_value'])) echo $subscription['time_value']; ?>" data-parsley-required data-parsley-required-message="Please enter duration value." data-parsley-min="0.1" data-parsley-min-message="This value should be greater than 0." data-parsley-errors-container="#time_value_error" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                                        
                                                    
                                                    <div id="time_value_error"></div>
                                                    <?php echo form_error('time_value'); ?> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">  
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                     <input type="checkbox" id="is_unlimited" name="is_unlimited" value="1" onclick="get_orders()" <?php if(isset($subscription['orders']) && $subscription['orders']=='Unlimited'){
                                                            echo 'checked';
                                                        }?>>
                                                      <label for="orders"> Is Order Unlimited</label><br>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12"> 
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="orders" >Orders *</label>
                                                  
                                                        <input type="text" class="form-control value>" id="orders" name="orders" placeholder="Orders" value="<?php if(isset($subscription['orders'])){
                                                                if($subscription['orders']!='Unlimited'){
                                                                echo $subscription['orders']; }
                                                            }
                                                          ?>" data-parsley-required-message="Please enter orders." data-parsley-min="0.1" data-parsley-min-message="This value should be greater than 0." data-parsley-errors-container="#value_errorss" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\*)\./g, '$1');" 
                                                          <?php if(isset($subscription['orders'])){
                                                                if($subscription['orders']=='Unlimited'){
                                                                echo 'readonly'; }else{
                                                                    echo 'data-parsley-required'; 
                                                                }
                                                            }
                                                          ?>> 
                                                 
                                                    <div id="value_errorss"></div>
                                                    <?php echo form_error('orders'); ?> 
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="amount" >Amount *</label>
                                                    <div class="input-group value">
                                                        <div class="input-group-addon" > 
                                                        <?php echo ADMIN_CURRENCY?>
                                                       </div>
                                                        <input type="text" class="form-control value>" id="amount" name="amount" placeholder="Amount" value="<?php if(isset($subscription['amount'])) echo $subscription['amount']; ?>" data-parsley-required data-parsley-required-message="Please enter amount." data-parsley-min="0.1" data-parsley-min-message="This value should be greater than 0." data-parsley-errors-container="#value_error" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" readonly>
                                                        
                                                    </div>
                                                    <div id="value_error"></div>
                                                    <?php echo form_error('amount'); ?> 
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-12"> 
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="time_value" >Title *</label> 
                                                        <input type="text" class="form-control value>" id="title" name="title" placeholder="Title" value="<?php if(isset($subscription['title'])) echo $subscription['title']; ?>" data-parsley-required data-parsley-required-message="Please enter title." data-parsley-errors-container="#title_error">
                                                        
                                                    
                                                    <div id="title_error"></div>
                                                    <?php echo form_error('title'); ?> 
                                                </div>
                                            </div>
                                        </div>
                                     
                                        <div class="col-sm-12">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="description">Description</label>
                                                    <textarea name="description" placeholder="Description" class="form-control" rows="3"><?php if(isset($subscription['description'])) echo $subscription['description']; ?></textarea>
                                                  
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="box-footer text-center">
                                        <?php if(isset($from_action) && !empty($from_action)){ ?>
                                            <button type="submit" id="add" class="btn btn-primary" onclick="return form_submit();">Edit</button>
                                        <?php } ?>
                                        <a href="<?php echo $back_action;?>" class="btn btn-default">Back</a> 
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box --> 
            </div><!-- col-12--> 
        </div><!-- row--> 
    </section>
</div>
<!-- row--> 
<!-- Include Bootstrap Datepicker -->

<script>


var is_valid = 1;
$(document).ready(function(){
    $("form").submit(function(){
        $("#loader").show();        
        // Disable #x
        $("#submit").prop( "disabled", true );
        if(is_valid == 0){
             $("#loader").hide();
            // Enable #x
            $("#submit").prop( "disabled", false );
            return false;
        }
        
    });
});

function validate_number() {
    var cash_limit = $('#cash_limit').val();
    if(cash_limit==0) {
        $('#cash_limit').val('');
    }
}

</script>
<script type="text/javascript">
// $('[data-toggle="tooltip"]').tooltip();

  $(function () {  
   
        //Changing Percent input-group addon glyphicon
        $('#dropDownId').change(function(){

            if($("#dropDownId").val()=='Percent') {
                $("#value").attr('Max','100');
                $("#value").removeAttr('Maxlength','5');
                $(".value .input-group-addon").html("%");
            } else {
                $("#value").removeAttr('Max','100');
                $("#value").attr('Maxlength','5');
                 $(".value .input-group-addon").html("$");
            }
        });
    });




    function validate_percent() {
        $("#promotion_error").html('');
        if($("#dropDownId").val()=='Percent') {
            var outputPercentageString = $("#value").val();
            var outputPercentage = parseInt(outputPercentageString);
            if (outputPercentage <= 0 || outputPercentage >= 100) {
                $("#value").focus();
                $("#value").val('');
                $("#promotion_error").html('<span class="parsley-errors-list ">Please enter the correct percentage.</span>');
                return false;
            }
        } else {
            var outputPercentageString = $("#value").val();
            var outputPercentage = parseInt(outputPercentageString);
            if (outputPercentage < 0 || outputPercentage > 99999) {
                $("#value").focus();
                $("#value").val('');
                $("#promotion_error").html('<span class="parsley-errors-list ">Please enter the value upto 5 digits.</span>');
                return false;
            }
        }
    }

get_professional_type();
function get_professional_type(){ 
    var type=$("#penalty_type").val();
  
    if(type=="After_Start_Cancel"){
        $("#time").hide(); 
        $("#time_value").attr('data-parsley-required', false);
     
    }else{
        $("#time").show();
        $("#time_value").attr('data-parsley-required', true); 
      
    }


}
function get_orders(){ 
   
    if ($("#is_unlimited").is(":checked")) { 
        $("#orders").val('');
        $("#orders").attr('readonly',true);
        $("#orders").attr('data-parsley-required',false);
        $("#value_errorss ul li").text('');
    } else { 
       $("#orders").attr('readonly',false);
       $("#orders").attr('data-parsley-required',true);
    } 
}


</script>




