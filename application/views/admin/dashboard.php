<div class="content-wrapper">
    <section class="content-header">
        <h1> Dashboard </h1>
        <ol class="breadcrumb">

            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <?php if(!empty($total_users_url)){ ?>
            <div class="col-md-3 col-sm-4 col-xs-12">
                <a href="<?php echo $total_users_url?>">
                    <div class="info-box">
                        <span class="info-box-icon  bg-yellow"><i class="fa fa-users"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Total Users</span>
                            <span class="info-box-number"><?php if (!empty($total_users)){echo $total_users;}else{ echo 0;} ?>
                            </span>
                        </div>
                    </div>
                </a>
            </div>
            <?php } if(!empty($total_celebrant_url)) {?>
            <div class="col-md-3 col-sm-4 col-xs-12">
                 <a href="<?php echo $total_celebrant_url?>">
                    <div class="info-box">
                        <span class="info-box-icon  bg-green"><i class="fa fa-users"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Total Celebrant</span>
                            <span class="info-box-number"><?php if (!empty($total_celebrant)){echo $total_celebrant;}else{ echo 0;} ?></span>
                        </div>
                    </div>
                </a>
            </div>
            <?php } if(!empty($total_sp_url)) {?>
            <div class="col-md-3 col-sm-4 col-xs-12">
                  <a href="<?php echo $total_sp_url?>">
                    <div class="info-box">
                        <span class="info-box-icon  bg-red"><i class="fa fa-users"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Total Service <br>Provider</span>
                            <span class="info-box-number"><?php if (!empty($total_sp)){echo $total_sp;}else{ echo 0;} ?></span>
                        </div>
                    </div>
                </a>
            </div>
            <?php } if(!empty($total_sp_cp_url)) {?>
            <div class="col-md-3 col-sm-4 col-xs-12">
                 <a href="<?php echo $total_sp_cp_url?>">
                    <div class="info-box">
                        <span class="info-box-icon  bg-blue"><i class="fa fa-users"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Total S P +<br> Celebrant </span>
                            <span class="info-box-number"><?php if (!empty($total_sp_cp)){echo $total_sp_cp;}else{ echo 0;} ?></span>
                        </div>
                    </div>
                </a>
            </div>
            <?php }?>

            <?php if(!empty($total_order_url)){ ?>
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <a href="<?php echo $total_order_url?>">
                        <div class="info-box">
                            <span class="info-box-icon  bg-blue"><i class="fa fa-shopping-cart"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Total Order</span>
                                <span class="info-box-number"><?php if (!empty($total_order)){echo $total_order;}else{ echo 0;} ?></span>
                            </div>
                        </div>
                    </a>
                </div>
            <?php } if(!empty($total_order_url)){ ?>
                <div class="col-md-3 col-sm-4 col-xs-12">
                    <a href="<?php echo $total_order_url?>">
                        <div class="info-box">
                            <span class="info-box-icon  bg-yellow"><i class="fa fa-money"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Total Order<br> Amount</span>
                                <span class="info-box-number"><?php if (!empty($total_order_payment)){echo ADMIN_CURRENCY.' '.$total_order_payment['total_amounts'];}else{ echo 0;} ?></span>
                            </div>
                        </div>
                    </a>
                </div>
            <?php } if(!empty($total_services_url)) {?>
            <div class="col-md-3 col-sm-4 col-xs-12">
                 <a href="<?php echo $total_services_url?>">
                    <div class="info-box">
                        <span class="info-box-icon  bg-green"><i class="fa fa-square-o"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Total Services</span>
                            <span class="info-box-number"><?php if (!empty($total_services)){echo $total_services;}else{ echo 0;} ?></span>
                        </div>
                    </div>
                </a>
            </div>
            <?php } if(!empty($total_post_url)) {?>
            <div class="col-md-3 col-sm-4 col-xs-12">
                  <a href="<?php echo $total_post_url?>">
                    <div class="info-box">
                        <span class="info-box-icon  bg-red"><i class="fa fa-file-text-o"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Total Post</span>
                            <span class="info-box-number"><?php if (!empty($total_post)){echo $total_post;}else{ echo 0;} ?></span>
                        </div>
                    </div>
                </a>
            </div>
            <?php } if(!empty($total_reviews_url)) {?>
            <div class="col-md-3 col-sm-4 col-xs-12">
                 <a href="<?php echo $total_reviews_url?>">
                    <div class="info-box">
                        <span class="info-box-icon  bg-green"><i class="fa fa-star"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Total Review</span>
                            <span class="info-box-number"><?php if (!empty($total_reviews)){echo $total_reviews;}else{ echo 0;} ?></span>
                        </div>
                    </div>
                </a>
            </div>
            <?php }?>
        </div>
       
    </section>
</div>