<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  	<h1> <?php echo $page_title;?> </h1>
  		<ol class="breadcrumb">
			<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
				<li class="<?php echo $breadcrumb['class'];?>"> 
					<?php if(!empty($breadcrumb['link'])) { ?>
						<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
					<?php } else {
						echo $breadcrumb['icon'].$breadcrumb['title'];
					} ?>
				</li>
			<?php }?>
  		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12 message_box">
				<?php if ($this->session->flashdata('error')) { ?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php } ?>
				<?php if ($this->session->flashdata('success')) { ?>
					<div class="alert alert-block alert-success fade in">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
				<?php } ?>
				<div class="box">
					<div class="box-header with-border">  
                  		<!--<h3 class="box-title">Filter Here</h3>-->  
                		<form method="get" action="<?php echo $action;?>"> 
						<div class="box-body row"> 
							<?php if(empty($user_id)){?>
							<div class="form-group col-md-3">
								<input class="column_filter form-control" id="userfullname" name='userfullname' type="text" placeholder="User Name" value="<?php echo $filter_user_fullname;?>">
							</div>
							<?php }?>
			              	<div class="form-group col-md-3">
                                <input class="column_filter form-control" id="post_number" name='post_number' type="text" placeholder="Post Number" value="<?php echo $filter_post_number;?>">
			              	</div>
			             	<input type="hidden" name="user_id" value="<?php echo $user_id?>">
			             	<div class="form-group col-md-3">
				              	<select name="post_type" id="post_type" class="column_filter form-control">
				              		<option value="">Post Content Type</option>
				              		<option value="1" <?php if(!empty($filter_post_type)&& $filter_post_type=='1'){ echo 'selected'; }?>>Media</option>
				              		<option value="2" <?php if(!empty($filter_post_type)&& $filter_post_type=='2'){ echo 'selected'; }?>>No Media</option>
				              	</select> 
	                        </div>
	                        <div class="form-group col-md-3">
				              	<select name="post_kind" id="" class="column_filter form-control">
				              		<option value="">Post Type</option>
				              		<option value="1" <?php if(!empty($filter_post_kind)&& $filter_post_kind=='1'){ echo 'selected'; }?>>Post</option>
				              		<option value="2" <?php if(!empty($filter_post_kind)&& $filter_post_kind=='2'){ echo 'selected'; }?>>Event</option>
				              	</select> 
	                        </div> 
			              	<div class="form-group col-md-3">
				              	<select name="status" id="status" class="column_filter form-control">
				              		<option value="">Status</option>
				              		<option value="Active" <?php if(!empty($filter_status)&& $filter_status=='Active'){ echo 'selected'; }?>>Active</option>
				              		<option value="Inactive" <?php if(!empty($filter_status)&& $filter_status=='Inactive'){ echo 'selected'; }?>>Inactive</option>
				              	</select> 
	                        </div> 
			             <div class="form-group col-md-3">
			               	<input class="btn btn-primary" type="submit" value="Filter">
			               	<a class="btn btn-default" href="<?php echo $action;?>">Reset</a>
			               	<?php if(!empty($back_url)){?>
           	              	<a class="btn btn-success" href="<?php echo $back_url;?>">Back</a>
			             	<?php }?>
			             </div>
			             
			          	</div>
			          </form> 
			            <?php if(isset($add_action) && !empty($add_action)){ ?>
                  			<!-- <a href="<?php echo $add_action;?>" title="" data-toggle="tooltip" data-original-title="Add User" class="btn btn-default pull-right"><i class="fa fa-plus"></i></a> -->
                		<?php } ?>
                	</div> 

					<div class="box-body">

						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th>
									<?php if(empty($user_id)){?>
										<th>User Name</th>
									<?php }?>
									<th>Post Content</th>
									<th>Post Liked</th>
									<th>Post Shared</th>
									<th>Post Abused</th>
									 
									<?php if(isset($edit_action) && !empty($edit_action)){ ?>
									<th>Status</th>
									<th>Posted</th>
									<?php } ?>
									 <?php if(isset($edit_action) || isset($delete_action)){ ?>
                                    	<th>Actions</th>
                                    <?php } ?>
								</tr>
							</thead>
							<tbody>
	

								<?php 
								if(!empty($records_results))
								{	
									$i = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
									$table="post";
									$field = "id";

									foreach ($records_results as $row) { $i++; 
										if(isset($row['status'])) {
	                                        if($row['status']=="Active") {
	                                            $status = "Active";
	                                            $class = "pointer badge bg-green";
	                                        }elseif($row['status']=="Unverified") {
	                                            $status = "Unverified";
	                                            $class = "pointer badge bg-yellow";
	                                        } else {
	                                            $status = "Inactive";
	                                            $class = "pointer badge bg-red";
	                                        }
                                    	}?>
										<tr id="tr_<?php echo $row[$field]; ?>">
											<td width="5%"><?php echo $i; ?></td>
											<?php if(empty($user_id)){?>
											<td><?php if(!empty($row['fullname'])) echo ucfirst($row['fullname']);?></td>
											<?php }?>
											<td style="word-break: break-all;"><?php if(!empty($row['post_content']))
											echo explode('^',$row['post_content'])[0];
											 ?></td>
											<td><?php if(!empty($row['like_count'])) {

												echo '<a href='.base_url().'admin/post/post_like/'.$row['id'].'>'.$row['like_count'].'</a>';
											}else{ echo '0';}?>
												

											</td>
											<td><?php if(!empty($row['sharedcount'])) {

												echo '<a href='.base_url().'admin/post/post_shared/'.$row['id'].'>'.$row['sharedcount'].'</a>';
											}else{ echo '0';}?>
												
												
											</td>
											<td><?php if(!empty($row['report_count'])) {

												echo '<a href='.base_url().'admin/post/post_report/'.$row['id'].'>'.$row['report_count'].'</a>';
											}else{ echo '0';}?>
												
												
											</td>
											
											 
											<?php if(isset($edit_action) && !empty($edit_action)){ ?>
												<td>
													<p id="status_<?php echo $row[$field]; ?>" onclick="change_user_status('<?php echo $field; ?>','<?php echo $row[$field]; ?>','<?php echo $table; ?>')" class="<?php echo $class; ?>" title="" data-toggle="tooltip" data-original-title="Change Status"><?php echo $status; ?></p>
												</td>
											<?php } ?>
											<td><?php echo convertGMTToLocalTimezone($row['created'],true)?></td>
											<td class="td-actions">
												<div class="btn-group">
									                  <button type="button" class="btn btn-sm btn-info">Action</button>
									                  <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									                    <span class="caret"></span>
									                    <span class="sr-only">Toggle Dropdown</span>
									                  </button>
									                  <ul class="dropdown-menu" role="menu"> 
									                    	<li>
										                    <?php if(empty($user_id)){ ?> 
										                    	<a  href="<?php echo base_url()."admin/Post/post_details/".$row['id'] ?>" >Detail</a>
									                    	<?php }else{?>
									                    		<a  href="<?php echo base_url()."admin/Post/post_details/".$row['id'].'?user_id='.$user_id ?>" >Detail</a>
									                    	<?php }?> 
									                    	</li>
										                    <?php /*if(isset($edit_action) && !empty($edit_action)){ ?>
								                    		<li><a   href="<?php echo $edit_action.'/'.$row[$field]; ?>">Edit</a></li>
									                    	<?php } */?>
									                    	<?php if(isset($delete_action) && !empty($delete_action)) { ?>
															<?php if(!empty($undeletable_ids) && !empty($row[$field])) {
															if (!in_array($row[$field], $undeletable_ids)) { ?>
																	<li><a   href="JavaScript:Void();" onclick="return soft_delete_record(<?php if(!empty($row[$field])) echo $row[$field]; ?>,'<?php echo $table; ?>','<?php echo $field; ?>');">Delete</a></li>
																<?php }
															}elseif(empty($undeletable_ids)){ ?>
																<li><a   href="JavaScript:Void();" onclick="return soft_delete_record(<?php if(!empty($row[$field])) echo $row[$field]; ?>,'<?php echo $table; ?>','<?php echo $field; ?>');">Delete</a></li>
															<?php } 
														} ?>
								                  </ul>
								                </div>
											</td>
										</tr>
									<?php }
								} else {
									echo "<tr><td colspan='7' align='center'> No Record Found</td></tr>";
								} ?>
							</tbody>
							<tfoot>						
								<tr>
									<?php if(!empty($pagination)) { ?>
										<td colspan="3">Total Records - <?php echo $total_records;?></td>
										<td colspan="7" align="center">
											<div><?php echo $pagination; ?></div>
										</td>
									<?php }else{ ?>	
										<td  colspan="6" >Total Records - <?php if($total_records >0){echo $total_records;} else{echo '0';}?></td>
										<td colspan="7" align="center"></td>
									<?php } ?>			
								</tr>
							</tfoot>	
						</table>
					</div>			
				</div>	
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->