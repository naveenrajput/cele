<div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $page_title;?> </h1>
        <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                <li class="<?php echo $breadcrumb['class'];?>"> 
                    <?php if(!empty($breadcrumb['link'])) { ?>
                        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                    <?php } else {
                        echo $breadcrumb['icon'].$breadcrumb['title'];
                    } ?>
                </li>
            <?php }?>
        </ol>
    </section>

    <!-- Main content -->
    <!-- <div class="pad margin no-print">
       
        <div class="callout callout-info" style="margin-bottom: 0!important;">
           <div class="row">
             <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green"><i class="ion ion-information-circled"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text text-black">Confirmed Request</span>
                        <span class="info-box-number text-black">Yes</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-olive"><i class="ion ion-information-circled"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text text-black">Completed Request</span>
                        <span class="info-box-number text-black">Yes</span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="ion ion-information-circled"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text text-black">Canceled Request</span>
                        <span class="info-box-number text-black">Yes</span>
                    </div>
                </div>
            </div>
        </div>
       </div>
       
    </div> -->
    <section class="content">
        <div class="invoice">
           <!-- title row -->
           <div class="row">
              <div class="col-xs-12">
                <h2 class="page-header">
                    <i class="fa fa-globe"></i> <?php if(!empty($user_fullname['fullname']))echo Ucfirst($user_fullname['fullname']); ?>
                    <small class="pull-right">
                        Reg Date: <?php if(!empty($post_detail['created']))echo date('m/d/Y',strtotime($post_detail['created'])); ?>
                    </small>
                </h2>
              </div>
              <!-- /.col -->
           </div>
           <!-- info row -->
            <div class="row invoice-info">
                <div class="col-sm-2 invoice-col">
                    <label>Post Number</label>
                    <p><?php echo !empty($post_detail['post_number'])?$post_detail['post_number']:'NA'; ?></p>
                </div>
                <div class="col-sm-2 invoice-col">
                    <label>Status </label>
                    <p><?php if(!empty($post_detail['status']))echo $post_detail['status']; ?></p>
                </div>

                <div class="col-sm-2 invoice-col">
                    <label>Total Comment</label>
                    <p><?php echo !empty($comments_count)?$comments_count:'NA'; ?></p>
                </div>
                  <div class="col-sm-2 invoice-col">
                    <label>Total Share</label>
                    <p><?php echo !empty($sharedcount)?$sharedcount:'NA'; ?></p>
                </div>
                 <div class="col-sm-2 invoice-col">
                    <label>Total Like</label>
                    <p><?php echo !empty($like_count)?$like_count:'NA'; ?></p>
                </div>
                 <div class="col-sm-2 invoice-col">
                    <label>Total Report Abused</label>
                    <p><?php echo !empty($report_count)?$report_count:'NA'; ?></p>
                </div>
                <div class="col-sm-2 invoice-col">
                    <label>Post Type</label>
                    <p><?php if(!empty($post_detail['post_type'])){
                            if($post_detail['post_type']==1){ echo 'Post';}else{ echo 'Event'; } ;
                        }?></p>
                </div>
            </div>
            <hr> 
            <div class="row invoice-info"> 
                 <div class="col-sm-12 invoice-col">
                    <label>Post Content</label>
                    <p><?php echo !empty($post_detail['post_content'])?$post_detail['post_content']:'NA'; ?></p>
                </div>
            </div>
             <hr>
            <div class="row invoice-info">
                <div class="col-sm-12">
                    <h4><b>Images</b></h4>
                </div>
            </div>

            <div class="row invoice-info">
            <?php 
            if(!empty($images)){
                foreach ($images as $key => $list) { ?> 
                        <div class="col-sm-4 invoice-col">
                            <a target="_" href="<?php echo $list['media_path']?>"><img width="300px" height="200px" src="<?php echo $list['media_path']?>"></a>
                            <br>                                            
                        </div>
                  
            <?php     }
            }
            ?>
              </div>
            <hr>
             <div class="row invoice-info">
                <div class="col-sm-12">
                    <h4><b>Videos</b></h4>
                </div>
            </div>
             <div class="row invoice-info">
            <?php 
            if(!empty($videos)){
                foreach ($videos as $key => $list) { ?> 
                        <div class="col-sm-4 invoice-col">
                            <a target="_" href="<?php echo $list['media_path']?>"><img width="300px" height="200px" src="<?php echo $list['video_thumbnail']?>"></a>
                            <br>                                            
                        </div>
                  
            <?php     }
            }
            ?>
              </div>
                <hr>
             <div class="row invoice-info">
                <div class="col-sm-12">
                    <h4><b>Comments (<?php echo $comments_count?>)</b></h4>
                </div>
            </div>
              
                <?php 
                if(!empty($comments)){ ?>
                 <div class="row invoice-info" style="border: 2px solid #d2d6de;margin: 10px; padding: 10px;">
                    <?php // echo "<pre>";print_r($comments);die;
                    foreach ($comments as $key => $list) { 
                        if(empty($list['parent_id'])){
                            $class = 'col-sm-8 col-sm-offset-2';
                        }else{
                            $class = 'col-sm-7 col-sm-offset-3';
                        }
                        ?> 
                            <div class="<?php echo $class?> invoice-col">
                                     <div class="direct-chat-msg" style="width: 100%">
                                            <div class="direct-chat-info clearfix">
                                                <span class="direct-chat-name pull-left"><?php echo ucfirst($list['fullname']);?></span>
                                                <span class="direct-chat-timestamp pull-right"><?php echo get_timeago(strtotime($list['created']));?></span>
                                            </div>
                                            <img class="direct-chat-img" src="<?php if(!empty($list['profile_picture'])){ echo $list['profile_picture'];}else{ echo base_url().'resources/default_image.png';}?>" alt="message user image">
                                                <div class="direct-chat-text"> 
                                                    <?php  
                                                        if(!empty($list['comment'])) echo $list['comment'];   
                                                    ?> 
                                                </div> 
                                    </div>       
                            </div>

                      
                <?php     } ?>
                  </div>
                <?php }
                ?>
            
              <br>
            <div class="row no-print">
              <div class="col-xs-12 text-center">
                <?php if(isset($edit_action) && !empty($edit_action)){ ?>
                    <a  href="<?php echo $edit_action; ?>" class="btn btn-primary">
                     EDIT
                    </a>
                <?php } ?>
                <?php if(isset($back_action) && !empty($back_action)){ ?>
                    <a  href="<?php echo $back_action; ?>" class="btn btn-default ">
                     Back
                    </a>
                <?php } ?>
              </div>
           </div>
        </div>
    </section>
</div>

