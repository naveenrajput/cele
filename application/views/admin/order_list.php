
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  	<h1> <?php echo $page_title;?> </h1>
  		<ol class="breadcrumb">
			<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
				<li class="<?php echo $breadcrumb['class'];?>"> 
					<?php if(!empty($breadcrumb['link'])) { ?>
						<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
					<?php } else {
						echo $breadcrumb['icon'].$breadcrumb['title'];
					} ?>
				</li>
			<?php }?>
  		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php if ($this->session->flashdata('error')) { ?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php } ?>
				<?php if ($this->session->flashdata('success')) { ?>
					<div class="alert alert-block alert-success fade in">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
				<?php }  
						// echo "<pre>";print_r($records_results);die;
						?>
				<div class="box">
					<div class="box-header with-border">  
                  		<!-- <h3 class="box-title">Filter Here</h3>   -->
              			<div class="box-body row"> 
                			<form method="get" action="<?php echo $reset_action; ?>"> 
							

				              	<div class="form-group col-md-3">
	                                <input class="column_filter form-control" id="order_number" name='order_number' type="text" placeholder="Order Number" value="<?php echo $filter_order_number;?>">
				              	</div>  
				              	<div class="form-group col-md-3">
	                                <input class="column_filter form-control" id="event_number" name='event_number' type="text" placeholder="Event Number" value="<?php echo $filter_event_number;?>">
				              	</div>  
				              	<div class="form-group col-md-3">
	                                <input class="column_filter form-control" id="username" name='username' type="text" placeholder="User name" value="<?php echo $filter_username;?>">
				              	</div>   
				              	<div class="form-group col-md-3">
	                                <input class="column_filter form-control" id="event_title" name='event_title' type="text" placeholder="Event Title" value="<?php echo $filter_event_title;?>">
				              	</div>  
				              	<div class="form-group col-md-3">
	                                <input class="column_filter form-control "  oninput="this.value = this.value.replace(/[^]/g, '').replace(/(\..*)\./g, '$1');" id="date_rang" name='date_rang' type="text" placeholder="Date" value="<?php echo $filter_date_rang;?>">
				              	</div>    
					              	<div class="form-group col-md-3">
					              		<select name="event_status" id="event_status" class="column_filter form-control">
					              			<option value="">Event Status</option>
					              			<option value="Not Started" <?php if(!empty($filter_event_status)&& $filter_event_status=='Not Started'){ echo 'selected'; }?>>Not Started</option>
					              			<option value="On Going" <?php if(!empty($filter_event_status)&& $filter_event_status=='On Going'){ echo 'selected'; }?>>On Going</option>
					              			<option value="Completed" <?php if(!empty($filter_event_status)&& $filter_event_status=='Completed'){ echo 'selected'; }?>>Completed</option>
					              		</select> 
		                        	</div>  
                            
					              	<div class="form-group col-md-3">
					              		<select name="order_status" id="status" class="column_filter form-control">
					              			<option value="">Order Status</option>
					              			<option value="Pending" <?php if(!empty($filter_order_status)&& $filter_order_status=='Pending'){ echo 'selected'; }?>>Pending</option>
					              			<option value="Confirmed" <?php if(!empty($filter_order_status)&& $filter_order_status=='Confirmed'){ echo 'selected'; }?>>Confirmed</option>
					              			<option value="Canceled" <?php if(!empty($filter_order_status)&& $filter_order_status=='Canceled'){ echo 'selected'; }?>>Canceled</option>
					              			<option value="Rejected" <?php if(!empty($filter_order_status)&& $filter_order_status=='Rejected'){ echo 'selected'; }?>>Rejected</option>
					              		</select> 
		                        	</div> 
                            	<!-- 
					              	<div class="form-group col-md-3">
					              		<select name="status" id="status" class="column_filter form-control">
					              			<option value="">Status</option>
					              			<option value="Active" <?php if(!empty($filter_status)&& $filter_status=='Active'){ echo 'selected'; }?>>Active</option>
					              			<option value="Inactive" <?php if(!empty($filter_status)&& $filter_status=='Inactive'){ echo 'selected'; }?>>Inactive</option>
					              		</select> 
		                        	</div>   -->
				             	<div class="form-group col-md-3">
				               		<input class="btn btn-primary" type="submit" value="Filter">
				               		<a class="btn btn-default" href="<?php echo $reset_action; ?>">Reset</a>
				             	</div>
			          		</form> 
            			</div>
                	</div> 

					<div class="box-body">

						<table <?php  
					 
						if(!empty($records_results))
								{	?>  <?php } ?> class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Order Number</th>
									<th>Event Number</th>
									<th>User Name</th>
									<th>Event Title</th>
									<th>Event Start Time</th> 
									<th>Event Status</th>
									<th>Order Status</th>
									<th>Created</th> 
									<!-- <th>Status</th>  -->
									<th>Action</th> 
									
								</tr>
							</thead>
							<tbody>
	

								<?php 
								if(!empty($records_results))
								{	
									// echo "<pre>";print_r($records_results);die;
									$i = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
									$table="orders";
									$field = "order_id";

									foreach ($records_results as $row) { $i++; 
										if(isset($row['status'])) {
	                                        if($row['status']=="Active") {
	                                            $status = "Active";
	                                            $class = "pointer badge bg-green";
	                                        } else {
	                                            $status = "Inactive";
	                                            $class = "pointer badge bg-red";
	                                        }
                                    	} ?>                                    	
										<tr id="tr_<?php echo $row[$field]; ?>">
											<td><?php echo $i; ?></td>
											<td><?php if(!empty($row['order_number'])) echo ucfirst($row['order_number']);?></td> 
											<td><?php if(!empty($row['event_number'])) echo ucfirst($row['event_number']);?></td> 
											<td><?php if(!empty($row['fullname'])) echo ucfirst($row['fullname']);?></td> 
											<td><?php if(!empty($row['event_title'])) echo ucfirst($row['event_title']);?></td> 
											<td><?php if(!empty($row['event_date'])) echo convertGMTToLocalTimezone($row['event_date'],true); ?></td>
										 	<td><?php if(!empty($row['event_status'])) echo ucfirst($row['event_status']);?></td>  
										 	<td><?php if(!empty($row['order_status'])) echo ucfirst($row['order_status']);?></td>  
											<td><?php if(!empty($row['created'])) echo convertGMTToLocalTimezone($row['created'],true); ?></td> 
											<!-- <td>
												<p id="status_<?php echo $row[$field]; ?>" onclick="change_status('<?php echo $field; ?>','<?php echo $row[$field]; ?>','<?php echo $table; ?>')" class="<?php echo $class; ?>" title="" data-toggle="tooltip" data-original-title="Change Status"><?php echo $status; ?></p>
											</td>  -->
											<td class="td-actions">
												<div class="btn-group">
									                  <button type="button" class="btn btn-sm btn-info">Action</button>
									                  <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									                    <span class="caret"></span>
									                    <span class="sr-only">Toggle Dropdown</span>
									                  </button>
												 	<ul class="dropdown-menu" role="menu">
													 	<li><a href="<?php echo base_url().'admin/orders/view/'.$row[$field]?>">Details</a></li> 
													 <!-- 	<li><a href="<?php echo base_url().'admin/services/offer/'.$row[$field]?>">Offers</a></li>  -->
												 	</ul>
										 	 	</div>	  
											</td>
										</tr> 
									<?php }
								} else {
									echo "<tr><td colspan='10' align='center'> No Record Found</td></tr>";
								} ?>
							</tbody>
							<tfoot>						
								<tr>
									<?php if(!empty($pagination)) { ?>
										<td colspan="2" >Total Records - <?php echo $total_records;?></td>
										<td colspan="10" align="center">
											<div><?php echo $pagination; ?></div>
										</td>
									<?php }else{ ?>	
										<td colspan="2">Total Records - <?php if($total_records >0){echo $total_records;} else{echo '0';}?></td>
										<td colspan="10" align="center"></td>
									<?php } ?>			
								</tr>
							</tfoot>
						</table>
					</div>			
				</div>			
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->
  <script type="text/javascript">
$(function () {
        $('#date_rang').daterangepicker({
            timePicker:false,
            format: 'MM/DD/YYYY'
        });
		if('<?php echo $filter_date_rang;?>'==''){
			$('#date_rang').val('');
		}

    });         
</script>

