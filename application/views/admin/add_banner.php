<div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $page_title;?> </h1>
        <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                <li class="<?php echo $breadcrumb['class'];?>"> 
                    <?php if(!empty($breadcrumb['link'])) { ?>
                        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                    <?php } else {
                        echo $breadcrumb['icon'].$breadcrumb['title'];
                    } ?>
                </li>
            <?php }?>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-primary"> 
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">  
                    <div class="col-lg-6">
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('error') ?>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('success')) { ?>
                            <div class="alert alert-block alert-success fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('success') ?>
                            </div>
                        <?php } ?>
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <?php if(isset($from_action) && !empty($from_action)){ ?>
                                    <form class="" method="POST" action="<?php echo $from_action; ?>" enctype="multipart/form-data" role="form"  data-parsley-validate onsubmit="return form_submit('add_banner');" id="add_banner">
                                <?php } ?>
                                <div class="box-body">
                                    <div class="form-group">
                                        <label for="title">Title *</label>
                                        <input type="text" class="form-control" name="title" id="title" value="<?php echo set_value('title'); ?>" placeholder="Title" maxlength="150" data-parsley-required data-parsley-required-message="Please enter title.">
                                        <?php echo form_error('title');?> 
                                    </div>
                                    <div class="form-group">
                                        <label for="title">Subtitle </label>
                                        <input type="text" class="form-control" name="subtitle" id="subtitle" value="<?php echo set_value('subtitle'); ?>" placeholder="Subtitle" maxlength="150">
                                        <?php echo form_error('subtitle');?> 
                                    </div>
                                    <div class="form-group">
                                        <label class="">Image *</label>
                                        <div data-provides="fileupload" class="fileupload fileupload-new">
                                            <input type="hidden">
                                            <div style="min-width: 150px;min-height: 120px;" class="fileupload-new thumbnail"> 
                                            </div>
                                            <div style="min-width: 150px;min-height: 120px; line-height: 5px;" class="fileupload-preview fileupload-exists thumbnail"></div>
                                            <div> 
                                                <span class="btn btn-file">
                                                    <span class="fileupload-new btn btn-default">Select image</span> 
                                                    <span class="fileupload-exists">Change</span>
                                                    <input type="file" name="image" class="default" accept="image/*" onchange="validate_banner_image(this);" data-parsley-required data-parsley-required-message="Please upload image." data-parsley-errors-container='#image_error'>
                                                </span> 
                                                <a data-dismiss="fileupload" class="btn fileupload-exists error v-align-middle">Remove</a> 
                                            </div>
                                            <div id="image_error" class="error error1"></div>
                                            <?php echo isset($upload_error)?$upload_error:'';?> 
                                        </div>
                                    </div>
                                </div>
                                <div class="box-footer text-center">
                                    <?php if(isset($from_action) && !empty($from_action)){ ?>
                                        <button type="submit" id="add" class="btn btn-primary">Add</button>
                                    <?php } ?>
                                    <a href="<?php echo $back_action;?>" class="btn btn-default">Back</a> 
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box --> 
            </div><!-- col-12--> 
        </div><!-- row--> 
    </section>
</div>
<!-- row--> 
<!-- Include Bootstrap Datepicker -->

<script type="text/javascript">
function validate_banner_image(input) {
    $(input).parent().parent().parent().find('.error1').html('');
    if (!input.files[0].name.toLowerCase().match(/\.(jpg|jpeg|png)$/)) {
        $(input).val("");
        $(input).parent().parent().parent().find('.error1').html("Only jpg|jpeg|png image types allowed.");
        return false;
    }
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(input.files[0]);

        reader.onload = function (e) {
            var image = new Image();
            image.src = e.target.result; 
            image.onload = function () {
                //Determine the Height and Width.
                var height = this.height;
                var width = this.width;
                // alert(width);
                if(width<1900 && height<500)
                {
                    $(input).val("");
                    $(input).parent().parent().parent().find('.error1').html("Image height and width must be at least 1900*500.");
                    $(input).parent().parent().parent().find('.fileupload-preview img').attr('src','');
                    return false;
                }
            }
        }
    }
}
function form_submit(id)
{
    $("#"+id).parsley().validate();
    if($("#"+id).parsley().isValid()){ 
       //$("#reply").attr('disabled',true);
       $("#loader").show(); 
       return true;
    }else{
        return false;
    }
}

</script>


