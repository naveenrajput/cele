
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  	<h1> <?php echo $page_title;?> </h1>
  		<ol class="breadcrumb">
			<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
				<li class="<?php echo $breadcrumb['class'];?>"> 
					<?php if(!empty($breadcrumb['link'])) { ?>
						<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
					<?php } else {
						echo $breadcrumb['icon'].$breadcrumb['title'];
					} ?>
				</li>
			<?php }?>
  		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php if ($this->session->flashdata('error')) { ?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php } ?>
				<?php if ($this->session->flashdata('success')) { ?>
					<div class="alert alert-block alert-success fade in">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
				<?php } ?>
				<div class="box">
					<div class="box-header with-border">  
                  		<!-- <h3 class="box-title">Filter Here</h3>   -->
              			<div class="box-body row"> 
                			<form method="get" action=""> 
                				<div class="form-group col-sm-12"> 
									<div class="form-group"> 
										<div class="col-sm-3">
											<label for="first_name" class="control-label">
												Service Title
											</label><br>
											<?php echo ucfirst($services['service_title'])?> 
										</div> 
										<div class="col-sm-3">
											<label for="first_name" class="control-label">
												User Name
											</label><br>
											<?php echo ucfirst($services['fullname'])?> 
										</div> 
										<div class="col-sm-3">
											<label for="first_name" class="control-label">
												Category Name
											</label> <br>
											<?php echo $services['title']?> 
										</div>  
										<div class="col-sm-3">
											<label for="first_name" class="control-label">
												Created Date
											</label> <br>
											<?php echo convertGMTToLocalTimezone($services['created'],true)?> 
										</div>  
									</div>	
								</div>	
								<div class="form-group col-sm-12"> 
									<hr>
								</div>
								<div class="form-group col-sm-12"> 
									<div class="form-group"> 
										<div class="col-sm-12">
											<label for="first_name" class="control-label">
												Description
											</label> <br>
											<?php echo $services['description']?> 
										</div>  
									</div>	
								</div>	
								<div class="form-group col-sm-12"> 
									<hr>
								</div> 
								<div class="form-group col-sm-12"> 
									<div class="form-group"> 
										<div class="col-sm-4">
											<label for="first_name" class="control-label">
												Image 1
											</label><br>
											<?php if(!empty($services['img1']) && file_exists($services['img1'])){
												$image = base_url().$services['img1'];
											}else{
												$image = base_url().'resources/default_image.png';
											}
											?> 
											<a target="_" href="<?php echo $image;?>"><img src="<?php echo $image;?>" width="100px" height="100px"></a>
										</div> 
										 <div class="col-sm-4">
											<label for="first_name" class="control-label">
												Image 2
											</label><br>
											<?php if(!empty($services['img2']) && file_exists($services['img2'])){
												$image = base_url().$services['img2'];
											}else{
												$image = base_url().'resources/default_image.png';
											}
											?> 
											<a target="_" href="<?php echo $image;?>"><img src="<?php echo $image;?>" width="100px" height="100px"></a>
										</div> 
										<div class="col-sm-4">
											<label for="first_name" class="control-label">
												Image 3
											</label><br>
											<?php if(!empty($services['img3']) && file_exists($services['img3'])){
												$image = base_url().$services['img3'];
											}else{
												$image = base_url().'resources/default_image.png';
											}
											?> 
											<a target="_" href="<?php echo $image;?>"><img src="<?php echo $image;?>" width="100px" height="100px"></a>
										</div>  
									</div>	
								</div>	
								<div class="form-group col-sm-12"> 
									<hr>
								</div> 
								<div class="col-sm-12"> 
											<div class="form-group"> 
												<div class="col-sm-4"> 
												  	<label for="first_name" class="control-label">
												 	Item Name
													</label>  
												</div>  
												<div class="col-sm-4"> 
											 	 	<label for="first_name" class="control-label">
												 	 Qty
													</label>  
												</div>  
												<div class="col-sm-4"> 
											 	 	<label for="first_name" class="control-label">
												 	 Status
													</label>  
												</div>  <br>
											</div>	  
										</div>
								
									<?php if(!empty($services_item)){
										foreach ($services_item as $key => $item) { ?>
										<div class="col-sm-12"> 
											<div class="form-group"> 
												<div class="col-sm-4"> 
												 	<?php echo ucfirst($item['item_name'])?>  
												</div>  
												<div class="col-sm-4"> 
												 	<?php echo ADMIN_CURRENCY.' '.$item['price'].'/ '.$item['qty'].' '.$item['measurement_title']?>  
												</div>  
												<div class="col-sm-4"> 
												 	<?php if(isset($item['status'])) {
					                                        if($item['status']=="Active") {
					                                            $status = "Active";
					                                            $class = "pointer badge bg-green";
					                                        } else {
					                                            $status = "Inactive";
					                                            $class = "pointer badge bg-red";
					                                        } 
				                                    	} 
			                                    	?>
		                                    		<p id="status_<?php echo $item['id']; ?>" onclick="change_status('<?php echo 'id'; ?>','<?php echo $item['id']; ?>','services_item')" class="<?php echo $class; ?>" title="" data-toggle="tooltip" data-original-title="Change Status"><?php echo $status; ?>
		                                    		</p>
												</div>  <br>
											</div>	  
										</div>

									<?php } }?>   
			          		</form> 
            			</div>
            			   	<div class="col-xs-12 text-center">
	            			 <?php if(isset($back_action) && !empty($back_action)){ ?>
			                    <a  href="<?php echo $back_action; ?>" class="btn btn-default ">
			                     Back
			                    </a>
			                <?php } ?>
                			</div> 
                	</div> 
 		
				</div>			
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->


