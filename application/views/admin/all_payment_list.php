
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  	<h1> <?php echo $page_title;?> </h1>
  		<ol class="breadcrumb">
			<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
				<li class="<?php echo $breadcrumb['class'];?>"> 
					<?php if(!empty($breadcrumb['link'])) { ?>
						<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
					<?php } else {
						echo $breadcrumb['icon'].$breadcrumb['title'];
					} ?>
				</li>
			<?php }?>
  		</ol>
	</section> 
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php if ($this->session->flashdata('error')) { ?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php } ?>
				<?php if ($this->session->flashdata('success')) { ?>
					<div class="alert alert-block alert-success fade in">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
				<?php }   ?>


				<div class="box">

					<div class="box-header with-border">  
                  		<!-- <h3 class="box-title">Filter Here</h3>   -->
              			<div class="box-body row"> 
                			<form method="get" action="<?php echo $reset_action; ?>">  
				              	<div class="form-group col-md-3">
	                                <input class="column_filter form-control" id="order_number" name='order_number' type="text" placeholder="Order Number" value="<?php echo $filter_order_number;?>">
				              	</div>     
				              	<div class="form-group col-md-3">
	                                <input class="column_filter form-control" id="sender_name" name='sender_name' type="text" placeholder="Customer name" value="<?php echo $filter_sender_name;?>">
				              	</div>    
				              	<div class="form-group col-md-3">
	                                <input class="column_filter form-control" id="receiver_name" name='receiver_name' type="text" placeholder="Service Provider name" value="<?php echo $filter_receiver_name;?>">
				              	</div>    
				              	<div class="form-group col-md-3">
	                                <input class="column_filter form-control "  oninput="this.value = this.value.replace(/[^]/g, '').replace(/(\..*)\./g, '$1');" id="date_rang" name='date_rang' type="text" placeholder="Date" value="<?php echo $filter_date_rang;?>">
				              	</div>       
				              	<div class="form-group col-md-3">
				              		<select name="payment_type" id="payment_type" class="column_filter form-control">
				              			<option value="">Payment Type</option>
				              			<option value="Order" 
				              			<?php if(!empty($filter_payment_type)&& $filter_payment_type=='Order'){ echo 'selected'; }?>>Order</option>
				              			<option value="Return" 
				              			<?php if(!empty($filter_payment_type)&& $filter_payment_type=='Return'){ echo 'selected'; }?>>Return</option>
				              		</select> 
	                        	</div>    
				              	<div class="form-group col-md-3">
				              		<select name="transfer_type" id="transfer_type" class="column_filter form-control">
				              			<option value="">Payment To</option>
				              			<option value="Customer_To_Admin" 
				              			<?php if(!empty($filter_transfer_type)&& $filter_transfer_type=='Customer_To_Admin'){ echo 'selected'; }?>>Customer To Admin</option>
				              			<option value="Admin_To_Customer" 
				              			<?php if(!empty($filter_transfer_type)&& $filter_transfer_type=='Admin_To_Customer'){ echo 'selected'; }?>>Admin To Customer</option>
				              			<option value="Admin_To_Service_Provider" 
				              			<?php if(!empty($filter_transfer_type)&& $filter_transfer_type=='Admin_To_Service_Provider'){ echo 'selected'; }?>>Admin To Service Provider</option>
				              		</select> 
	                        	</div>  
				             	<div class="form-group col-md-3">
				               		<input class="btn btn-primary" type="submit" value="Filter">
				               		<a class="btn btn-default" href="<?php echo $reset_action; ?>">Reset</a>
				             	</div>
				             	<?php 	if(!empty($records_results))
								{ ?>
								<div class="form-group col-md-1 "> 	
									<a class="btn btn-danger" href="<?php echo base_url().'admin/Order/export_excel?'.http_build_query($_GET)?>">Export <i class="fa fa-download" aria-hidden="true"></i>  
									</a>
								</div>
								<?php }?>
			          		</form> 
            			</div>
                	</div> 

					<div class="box-body">

						<table  class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Order Number</th> 
									<th>Customer Name</th> 
									<th>Service Provider Name</th> 
									<th>Total  Amount</th> 
									<th>Paid Amount</th> 
									<th>Payment Date</th>  
									<th>Payment Type</th> 
									<th>Payment To</th> 
								</tr>
							</thead>
							<tbody> 
								<?php 
								if(!empty($records_results))
								{ 
									$i = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
									$table="payment_history";
									$field = "id";

									foreach ($records_results as $row) { $i++; 

										if(isset($row['status'])) {
	                                        if($row['status']=="Active") {
	                                            $status = "Active";
	                                            $class = "pointer badge bg-green";
	                                        } else {
	                                            $status = "Inactive";
	                                            $class = "pointer badge bg-red";
	                                        }
                                    	}  
                                	?>                                    	
										<tr id="tr_<?php echo $row[$field]; ?>">
											<td><?php echo $i; ?></td>
											<td><?php if(!empty($row['order_number'])) echo ucfirst($row['order_number']);?></td> 
											<td><?php if(!empty($row['sender_fullname'])) echo ucfirst($row['sender_fullname']);?></td> 
											<td><?php if(!empty($row['receiver_fullname'])) echo ucfirst($row['receiver_fullname']);?></td> 
											<td><?php if(!empty($row['total_order_amount'])) echo ADMIN_CURRENCY.' '.number_format((float)$row['total_order_amount'], 2, '.', '');?></td> 
										
											<td><?php if(!empty($row['paid_amount'])) echo  ADMIN_CURRENCY.' '.number_format((float)$row['paid_amount'], 2, '.', '');?></td> 
										 	<td><?php if(!empty($row['created'])) echo convertGMTToLocalTimezone($row['created'],true); ?></td>
										 	<td><?php if(!empty($row['payment_type'])) echo ucfirst($row['payment_type']);?></td>  
										 	<td><?php if(!empty($row['transfer_type'])) echo str_replace('_',' ',$row['transfer_type']);?></td>  
											</td>  
										</tr> 
									<?php }
								} else {
									echo "<tr><td colspan='10' align='center'> No Record Found</td></tr>";
								} ?>
							</tbody>
							<tfoot>						
								<tr>
									<?php if(!empty($pagination)) { ?>
										<td colspan="2" >Total Records - <?php echo $total_records;?></td>
										<td colspan="10" align="center">
											<div><?php echo $pagination; ?></div>
										</td>
									<?php }else{ ?>	
										<td colspan="2">Total Records - <?php if($total_records >0){echo $total_records;} else{echo '0';}?></td>
										<td colspan="10" align="center"></td>
									<?php } ?>			
								</tr>
							</tfoot>
						</table>
					</div>			
				</div>			
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->
  <script type="text/javascript">
$(function () {
        $('#date_rang').daterangepicker({
            timePicker:false,
            format: 'MM/DD/YYYY'
        });
		if('<?php echo $filter_date_rang;?>'==''){
			$('#date_rang').val('');
		}

    });         
</script>

