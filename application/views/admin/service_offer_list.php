
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  	<h1> <?php echo $page_title;?> </h1>
  		<ol class="breadcrumb">
			<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
				<li class="<?php echo $breadcrumb['class'];?>"> 
					<?php if(!empty($breadcrumb['link'])) { ?>
						<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
					<?php } else {
						echo $breadcrumb['icon'].$breadcrumb['title'];
					} ?>
				</li>
			<?php }?>
  		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php if ($this->session->flashdata('error')) { ?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php } ?>
				<?php if ($this->session->flashdata('success')) { ?>
					<div class="alert alert-block alert-success fade in">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
				<?php }  
						// echo "<pre>";print_r($records_results);die;
						?>
				<div class="box">
					<div class="box-header with-border">  
                  		<!-- <h3 class="box-title">Filter Here</h3>   -->
              			<div class="box-body row"> 
                			<form method="get" action="<?php echo $reset_action; ?>"> 
						
				              	<div class="form-group col-md-3">
	                                <input class="column_filter form-control" id="offer_name" name='offer_name' type="text" placeholder="Offer Name" value="<?php echo $filter_offer_name;?>">
				              	</div>   
				              	<div class="form-group col-md-3">
	                                <input class="column_filter form-control "  oninput="this.value = this.value.replace(/[^]/g, '').replace(/(\..*)\./g, '$1');" id="date_rang" name='date_rang' type="text" placeholder="Date" value="<?php echo $filter_date_rang;?>">
				              	</div>   
                            
					              	<div class="form-group col-md-3">
					              		<select name="status" id="status" class="column_filter form-control">
					              			<option value="">Status</option>
					              			<option value="Active" <?php if(!empty($filter_status)&& $filter_status=='Active'){ echo 'selected'; }?>>Active</option>
					              			<option value="Inactive" <?php if(!empty($filter_status)&& $filter_status=='Inactive'){ echo 'selected'; }?>>Inactive</option>
					              		</select> 
		                        	</div>  
				             	<div class="form-group col-md-3">
				               		<input class="btn btn-primary" type="submit" value="Filter">
				               		<a class="btn btn-default" href="<?php echo $reset_action; ?>">Reset</a>
				               		<a class="btn btn-success" href="<?php echo $back_action; ?>">Back</a>
				             	</div>
			          		</form> 
            			</div>
                	</div> 

					<div class="box-body">

						<table <?php  
					 
						if(!empty($records_results))
								{	?>  <?php } ?> class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Offer Image</th>
									<th>Offer Name</th>
									<th>Start Date</th>
									<th>End Date</th>
									<th>Discount</th>
									<th>Description</th> 
									<th>Created</th> 
									<th>Status</th> 
									
								</tr>
							</thead>
							<tbody>
	

								<?php 
								if(!empty($records_results))
								{	
									$i = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
									$table="offer";
									$field = "id";

									foreach ($records_results as $row) { $i++; 
										if(isset($row['status'])) {
	                                        if($row['status']=="Active") {
	                                            $status = "Active";
	                                            $class = "pointer badge bg-green";
	                                        } else {
	                                            $status = "Inactive";
	                                            $class = "pointer badge bg-red";
	                                        }
                                    	} ?>                                    	
										<tr id="tr_<?php echo $row[$field]; ?>">
											<td><?php echo $i; ?></td>
											<td>
												<?php if(!empty($row['img1']) && file_exists($row['img1'])){
													$image = base_url().$row['img1'];
												}else{
													$image = base_url().'resources/default_image.png';
												}
												?> 
												<a href="<?php echo $image?>"><img src="<?php echo $image?>" width="25px" height="25px"></a> 
											</td> 
											<td><?php if(!empty($row['offer_name'])) echo ucfirst($row['offer_name']);?></td> 
											<td><?php if(!empty($row['start_date'])) echo convertGMTToLocalTimezone($row['start_date']); ?></td>
											<td><?php if(!empty($row['end_date'])) echo convertGMTToLocalTimezone($row['end_date']); ?></td>
												<td><?php if(!empty($row['discount'])) echo ($row['discount']).'%';?></td> 
											<td>
												<?php if(!empty($row['description']))  if(strlen($row['description']) > 25) {echo substr($row['description'],0,25).'...';}else{ echo $row['description'];} ?>
											</td> 
											<td><?php if(!empty($row['created'])) echo convertGMTToLocalTimezone($row['created'],true); ?></td> 
												<td>
													<p id="status_<?php echo $row[$field]; ?>" onclick="change_status('<?php echo $field; ?>','<?php echo $row[$field]; ?>','<?php echo $table; ?>')" class="<?php echo $class; ?>" title="" data-toggle="tooltip" data-original-title="Change Status"><?php echo $status; ?></p>
												</td> 
										</tr>

									<?php }
								} else {
									echo "<tr><td colspan='10' align='center'> No Record Found</td></tr>";
								} ?>
							</tbody>
							<tfoot>						
								<tr>
									<?php if(!empty($pagination)) { ?>
										<td colspan="2" >Total Records - <?php echo $total_records;?></td>
										<td colspan="10" align="center">
											<div><?php echo $pagination; ?></div>
										</td>
									<?php }else{ ?>	
										<td colspan="2">Total Records - <?php if($total_records >0){echo $total_records;} else{echo '0';}?></td>
										<td colspan="10" align="center"></td>
									<?php } ?>			
								</tr>
							</tfoot>
						</table>
					</div>			
				</div>			
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->
  <script type="text/javascript">
$(function () {
        $('#date_rang').daterangepicker({
            timePicker:false,
            format: 'MM/DD/YYYY'
        });
		if('<?php echo $filter_date_rang;?>'==''){
			$('#date_rang').val('');
		}

    });         
</script>

