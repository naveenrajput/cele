<!-- Content Wrapper. Contains page content -->
<style type="text/css">
.stars-container {
  position: relative;
  display: inline-block;
  color: transparent;
}

.stars-container:before {
  position: absolute;
  top: 0;
  left: 0;
  content: '★★★★★';
  color: lightgray;
}

.stars-container:after {
  position: absolute;
  top: 0;
  left: 0;
  content: '★★★★★';
  color: gold;
  overflow: hidden;
}

.stars-0:after { width: 0%; }
.stars-10:after { width: 10%; }
.stars-20:after { width: 20%; }
.stars-30:after { width: 30%; }
.stars-40:after { width: 40%; }
.stars-50:after { width: 50%; }
.stars-60:after { width: 60%; }
.stars-70:after { width: 70%; }
.stars-80:after { width: 80%; }
.stars-90:after { width: 90%; }
.stars-100:after { width: 100; }
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  	<h1> <?php echo $page_title;?> </h1>
  		<ol class="breadcrumb">
			<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
				<li class="<?php echo $breadcrumb['class'];?>"> 
					<?php if(!empty($breadcrumb['link'])) { ?>
						<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
					<?php } else {
						echo $breadcrumb['icon'].$breadcrumb['title'];
					} ?>
				</li>
			<?php }?>
  		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<?php if ($this->session->flashdata('error')) { ?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php } ?>
				<?php if ($this->session->flashdata('success')) { ?>
					<div class="alert alert-block alert-success fade in">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
				<?php } ?>
				<div class="box">
					<div class="box-header with-border">  
                  		<!-- <h3 class="box-title">Filter Here</h3>   -->
              			<div class="box-body row"> 
                			<form method="get" action="<?php echo $reset_action; ?>"> 
						
				              	<div class="form-group col-md-3">
	                                <input class="column_filter form-control" id="event_number" name='event_number' type="text" placeholder="Event Number" value="<?php echo $filter_event_number;?>">
				              	</div> 
					              	<div class="form-group col-md-3">
		                                <input class="column_filter form-control" id="event_title" name='event_title' type="text" placeholder="Event Title" value="<?php echo $filter_event_title;?>">
					              	</div>
					              	<div class="form-group col-md-3">
		                                <input class="column_filter form-control" id="sender_name" name='sender_name' type="text" placeholder="Review From" value="<?php echo $filter_sender_name;?>">
					              	</div>
					              	<div class="form-group col-md-3">
		                                <input class="column_filter form-control" id="receiver_name" name='receiver_name' type="text" placeholder="Review To" value="<?php echo $filter_receiver_name;?>">
					              	</div>
			                
				              	
				              	<div class="form-group col-md-3">
	                               
	                                <select name="rating" id="rating" class="column_filter form-control">
					              		<option value="">Rating</option>
					              		<option value="5.0" <?php if(!empty($filter_rating)&& $filter_rating=="5.0"){ echo 'selected'; }?>>5.0</option>
					              		<option value="4.5" <?php if(!empty($filter_rating)&& $filter_rating=="4.5"){ echo 'selected'; }?>>4.5</option>
					              		<option value="4.0" <?php if(!empty($filter_rating)&& $filter_rating=="4.0"){ echo 'selected'; }?>>4.0</option>
					              		<option value="3.5" <?php if(!empty($filter_rating)&& $filter_rating=="3.5"){ echo 'selected'; }?>>3.5</option>
					              		<option value="3.0" <?php if(!empty($filter_rating)&& $filter_rating=="3.0"){ echo 'selected'; }?>>3.0</option>
					              		<option value="2.5" <?php if(!empty($filter_rating)&& $filter_rating=="2.5"){ echo 'selected'; }?>>2.5</option>
					              		<option value="2.0" <?php if(!empty($filter_rating)&& $filter_rating=="2.0"){ echo 'selected'; }?>>2.0</option>
					              		<option value="1.5" <?php if(!empty($filter_rating)&& $filter_rating=="1.5"){ echo 'selected'; }?>>1.5</option>
					              		<option value="1.0" <?php if(!empty($filter_rating)&& $filter_rating=="1.0"){ echo 'selected'; }?>>1.0</option>
					              		<option value="0.5" <?php if(!empty($filter_rating)&& $filter_rating=="0.5"){ echo 'selected'; }?>>0.5</option>
					              		
					              	</select> 
	                               
	                            </div>
	                            <div class="form-group col-md-3">
	                                <div class="input-group">
	                                  <div class="input-group-addon">
	                                    <i class="fa fa-calendar"></i>
	                                  </div>
	                                  <input type="text" class="column_filter form-control pull-right date_rang" autocomplete="off" id="date_rang" name="date_rang"    Placeholder="Date "  oninput="this.value = this.value.replace(/[^]/g, '').replace(/(\..*)\./g, '$1');" value="<?php if(!empty($filter_date_rang)&& $filter_date_rang){ echo $filter_date_rang; }?>">
	                                </div>
	                            </div>
                        <!--         <input class="column_filter form-control" id="job_id" name='job_id' type="hidden" value="<?php echo $filter_job_id;?>">
                                <input class="column_filter form-control" id="sender_id" name='sender_id' type="hidden" value="<?php echo $filter_sender_id;?>">
                                <input class="column_filter form-control" id="receiver_id" name='receiver_id' type="hidden" value="<?php echo $filter_receiver_id;?>">

                                <input class="column_filter form-control" id="individual" name='individual' type="hidden" value="<?php echo $filter_individual;?>">
                                <input class="column_filter form-control" id="freelancer" name='freelancer' type="hidden" value="<?php echo $filter_freelancer;?>">
				             -->
				              	<?php if ($type==1) { ?>
					              	<div class="form-group col-md-3">
					              		<select name="status" id="status" class="column_filter form-control">
					              			<option value="">Status</option>
					              			<option value="Active" <?php if(!empty($filter_status)&& $filter_status=='Active'){ echo 'selected'; }?>>Active</option>
					              			<option value="Inactive" <?php if(!empty($filter_status)&& $filter_status=='Inactive'){ echo 'selected'; }?>>Inactive</option>
					              		</select> 
		                        	</div> 

			               		<?php } ?>
				             	<div class="form-group col-md-3">
				               		<input class="btn btn-primary" type="submit" value="Filter">
				               		<a class="btn btn-default" href="<?php echo $reset_action; ?>">Reset</a>
				             	</div>
			          		</form> 
            			</div>
                	</div> 

					<div class="box-body">

						<table <?php  if(!empty($records_results))
								{	?>  <?php } ?> class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Event Number</th> 
									<th>Event Title</th> 
									<th>Review From</th>
									<th>Review To</th>  
									<th>Rating</th>
									<th>Rating Date</th>
									<?php if(isset($edit_action) && !empty($edit_action)){ ?>
									<th>Status</th>
									<th>Action</th>
									<?php } ?>
								</tr>
							</thead>
							<tbody>
	

								<?php 
								if(!empty($records_results))
								{	
									$i = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
									$table="reviews";
									$field = "id";

									foreach ($records_results as $row) { $i++; 
										if(isset($row['status'])) {
	                                        if($row['status']=="Active") {
	                                            $status = "Active";
	                                            $class = "pointer badge bg-green";
	                                        } else {
	                                            $status = "Inactive";
	                                            $class = "pointer badge bg-red";
	                                        }
                                    	} ?>
                                    	<?php if ($row['rating']==0.5) {
											$rating = '<div><span class="stars-container stars-10">★★★★★</span></div>';
                                    	} else if($row['rating']==1){
											$rating = '<div><span class="stars-container stars-20">★★★★★</span></div>';
                                    	} else if($row['rating']==1.5){
											$rating = '<div><span class="stars-container stars-30">★★★★★</span></div>';
                                    	} else if($row['rating']==2){
											$rating = '<div><span class="stars-container stars-40">★★★★★</span></div>';
                                    	} else if($row['rating']==2.5){
											$rating = '<div><span class="stars-container stars-50">★★★★★</span></div>';
                                    	} else if($row['rating']==3){
											$rating = '<div><span class="stars-container stars-60">★★★★★</span></div>';
                                    	} else if($row['rating']==3.5){
											$rating = '<div><span class="stars-container stars-70">★★★★★</span></div>';
                                    	} else if($row['rating']==4){
											$rating = '<div><span class="stars-container stars-80">★★★★★</span></div>';
                                    	} else if($row['rating']==4.5){
											$rating = '<div><span class="stars-container stars-90">★★★★★</span></div>';
                                    	} else if($row['rating']==5){
											$rating = '<div><span class="stars-container stars-100">★★★★★</span></div>';
                                    	} else {
											$rating = '<div><span class="stars-container stars-0">★★★★★</span></div>';
                                    	} ?> 
                                    	                                   	
										<tr id="tr_<?php echo $row[$field]; ?>">
											<td><?php echo $i; ?></td>
											<td><?php if(!empty($row['event_number'])) echo ucfirst($row['event_number']);?></td>
											<td><?php if(!empty($row['event_title'])) echo ucfirst($row['event_title']);?></td>

							              	<?php if (empty($is_indi)) { ?>
												<td><?php if(!empty($row['sender_name'])) echo ucfirst($row['sender_name']); ?></td>
												<td><?php if(!empty($row['receiver_name'])) echo ucfirst($row['receiver_name']); ?></td>
							              	<?php } else { 
							              		if ($is_indi=='receiver') { ?>
													<td><?php if(!empty($row['sender_name'])) echo ucfirst($row['sender_name']); ?></td>
							              		<?php }elseif ($is_indi=='sender') { ?>
													<td><?php if(!empty($row['receiver_name'])) echo ucfirst($row['receiver_name']); ?></td>
							              		<?php }
							              	} ?>

											<td>
												<?php echo $rating; ?>
											</td>
											 
											<td><?php if(!empty($row['created'])) echo date('M d,Y h:i A',strtotime($row['created'])); ?></td>
												<?php if(isset($edit_action) && !empty($edit_action)){ ?>
												<td>
													<p id="status_<?php echo $row[$field]; ?>" onclick="change_status('<?php echo $field; ?>','<?php echo $row[$field]; ?>','<?php echo $table; ?>')" class="<?php echo $class; ?>" title="" data-toggle="tooltip" data-original-title="Change Status"><?php echo $status; ?></p>
												</td>
											<?php } ?>
											<td class="td-actions">
											
													<!-- <a id="" href="<?php /*echo base_url().'/admin/reviews/view/'.$row[$field];*/ ?>" class="btn btn-xs btn-success" title="" data-toggle="tooltip" data-original-title="Edit">
														<i class="fa fa-eye"></i>
													</a> -->
											
												<?php if(isset($delete_action) && !empty($delete_action)) { ?>
														<button class="btn btn-xs bg-red delete" data-toggle="tooltip" data-original-title="Delete" onclick="return soft_delete_record(<?php if(!empty($row[$field])) echo $row[$field]; ?>,'<?php echo $table; ?>','<?php echo $field; ?>');"><i class="fa fa-trash-o"></i></button>
												<?php } ?>
											</td>
										</tr>

									<?php }
								} else {
									echo "<tr><td colspan='10' align='center'> No Record Found</td></tr>";
								} ?>
							</tbody>
							<tfoot>						
								<tr>
									<?php if(!empty($pagination)) { ?>
										<td colspan="2" >Total Records - <?php echo $total_records;?></td>
										<td colspan="10" align="center">
											<div><?php echo $pagination; ?></div>
										</td>
									<?php }else{ ?>	
										<td colspan="2">Total Records - <?php if($total_records >0){echo $total_records;} else{echo '0';}?></td>
										<td colspan="10" align="center"></td>
									<?php } ?>			
								</tr>
							</tfoot>
						</table>
					</div>			
				</div>			
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->
  <script type="text/javascript">
$(function () {
        $('#date_rang').daterangepicker({
            timePicker:false,
            format: 'MM/DD/YYYY'
        });
		if('<?php echo $filter_date_rang;?>'==''){
			$('#date_rang').val('');
		}

    });         
</script>

