
<div class="content-wrapper">
    <section class="content-header">
    <h1> <?php echo $page_title;?> </h1>
        <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                <li class="<?php echo $breadcrumb['class'];?>"> 
                    <?php if(!empty($breadcrumb['link'])) { ?>
                        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                    <?php } else {
                        echo $breadcrumb['icon'].$breadcrumb['title'];
                    } ?>
                </li>
            <?php } ?>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <?php if(isset($from_action) && !empty($from_action)){ ?>
                <form id="myform"  method="POST" action="<?php echo $from_action; ?>" role="form" data-parsley-validate>
                <?php } ?>
                    <div class="col-lg-12">
                        <h3><?php if(isset($details)) echo $details; ?></h3>
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="row">
                                    <div class="panel-body">
                                        <?php if ($this->session->flashdata('error')) { ?>
                                            <div class="alert alert-block alert-danger fade in">
                                                <button data-dismiss="alert" class="close" type="button">×</button>
                                                <?php echo $this->session->flashdata('error') ?>
                                                </div>
                                            <?php } ?>
                                            <?php if ($this->session->flashdata('success')) { ?>
                                            <div class="alert alert-block alert-success fade in">
                                                <button data-dismiss="alert" class="close" type="button">×</button>
                                                <?php echo $this->session->flashdata('success') ?>
                                            </div>
                                        <?php } ?>
                                        <div id= 'notification_msg'></div>
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="form-group col-sm-4">
                                                    <label for="title">User *</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <input class="emp" type="radio" name="user" value="1" checked> All &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <input class="emp" type="radio" name="user" value="2"> Select
                                                </div>
                                                <div class="form-group col-sm-8" id="sel" style="display:none;">
                                                    <select class="select2 form-control" name="user_type[]" id="employee" multiple="multiple" placeholder="Select User Type" data-parsley-required-message="Please select user type." style="width:100%;padding:0px;margin: 0px;" data-parsley-errors-container="#user_error">
                                                        <option value="1">Celebrant</option>
                                                        <option value="2">Service Provider</option>
                                                        <option value="3">Celebrant + Service Provider</option>
                                                    </select>
                                                    <div id="user_error"></div>
                                                    <?php echo form_error('user_type');?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="form-group col-sm-6">
                                                    <label for="title">Notification Type *</label><br>
                                                    <select class="form-control" name="type" data-parsley-required data-parsley-required-message="Please select notification type.">
                                                        <option value="">Select Type</option>
                                                        <option value="push">Push</option>
                                                        <option value="email">Email</option>
                                                        <option value="both">Both</option>
                                                    </select>
                                                </div>
                                                <div class="form-group col-sm-6">
                                                    <label for="title">Title *</label>
                                                    <input type="text" class="form-control" name="title" id="title" value="" placeholder="Title" maxlength="25" data-parsley-required data-parsley-required-message="Please enter title.">
                                                    <?php echo form_error('title');?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="content" >Content *</label>
                                                <textarea name="content" placeholder="Content Here" class="form-control" rows="3" data-parsley-required data-parsley-required-message="Please enter content." maxlength="250" data-parsley-errors-container="#content_error"></textarea>
                                                <div id="content_error"></div>
                                                <?php echo form_error('content');?>
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                            <div class="form-group">
                                                <div class="col-sm-12 text-center">
                                                    <?php if(isset($from_action) && !empty($from_action)){ ?>
                                                    <button id="send" type="submit" class="btn btn-primary">Send</button>
                                                    <?php } ?>
                                                    <a href="<?php if(isset($back_action))echo $back_action;?>" class="btn btn-default">Back</a>  </div>
                                            </div>
                                        </div>
                                    </div><!--panel-body-->
                                </div><!--box-row-->
                            </div><!--box-body-->
                        </div><!-- row-->
                    </div><!-- col-6-->
                </form>
            </div>
        </div>
    </section>
</div>



<script type="text/javascript">
     $(".emp").click(function(){
        var val =$(this).val();
        if(val == '1') {
            $('#employee').attr('data-parsley-required', 'false');
            $("#sel").hide();
        } else {
            $('#employee').attr('data-parsley-required', 'true');
            $("#sel").show();
            $('.select2').select2({
                placeholder: "Select User Type",
                allowClear: true
            });
        }        
    });

    $("#myform").submit(function() {
        $("#myform").parsley().validate();
        if($("#myform").parsley().isValid()){
            $("#send").attr("disabled", true);
            $("#loader").show();
        }
    });

    $('.select2').on("select2:selecting", function(e) { 
        $("#user_error").text(''); 
    });
      
</script>














