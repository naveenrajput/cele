
<div class="content-wrapper">
  <section class="content-header">
      <ol class="breadcrumb">
          <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
              <li class="<?php echo $breadcrumb['class'];?>"> 
                  <?php if(!empty($breadcrumb['link'])) { ?>
                      <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                  <?php } else {
                      echo $breadcrumb['icon'].$breadcrumb['title'];
                  } ?>
              </li>
          <?php }?>
      </ol>
  </section>

    <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-lg-12">
        <?php if(isset($from_action) && !empty($from_action)){ ?>
        <form class="" id="change_password" method="POST" enctype="multipart/form-data"  action="<?php echo $from_action; ?>" role="form" data-parsley-validate>
        <?php } ?>
          <div class="col-lg-12">
            <h3><?php if(isset($page_title)) echo $page_title; ?></h3>
            <div class="box box-primary">
              <div class="box-body">
                <div class="row">
                  <div class="panel-body">
                    <?php if ($this->session->flashdata('error')) { ?>
                      <div class="alert alert-block alert-danger fade in">
                          <button data-dismiss="alert" class="close" type="button">×</button>
                          <?php echo $this->session->flashdata('error') ?>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('success')) { ?>
                      <div class="alert alert-block alert-success fade in">
                          <button data-dismiss="alert" class="close" type="button">×</button>
                          <?php echo $this->session->flashdata('success') ?>
                      </div>
                    <?php } ?>
                    <div id= 'notification_msg'></div>
                    <div class="box-body">
                      <div class="form-group">
                        <label for="title">Title *</label>
                        <input type="text" class="form-control" name="title" id="title" value="<?php echo !set_value('title') ? $pages['title'] : set_value('title'); ?>" placeholder="Title" maxlength="150" data-parsley-required data-parsley-required-message="Please enter title.">
                        <?php echo form_error('name');?>
                       </div>
                      <div class="form-group">
                        <label for="content" >Content *</label>
                        <textarea name="content" placeholder="Content Here" class="form-control" id="ckeditor" rows="3" data-parsley-required data-parsley-required-message="Please enter content." data-parsley-errors-container="#content_error"><?php echo !set_value('content') ? $pages['content'] : set_value('content'); ?></textarea>
                        <div id="content_error"></div>
                        <?php echo form_error('content');?>
                      </div>
                    </div>
                    <div class="box-footer">
                      <div class="form-group">
                        <div class="col-sm-12 text-center">
                            <?php if(isset($from_action) && !empty($from_action)){ ?>
                             <button type="submit" class="btn btn-primary">Update</button>
                            <?php } ?>
                            <a href="<?php if(isset($back_action))echo $back_action;?>" class="btn btn-default">Back</a> </div>
                      </div>
                    </div>
                  </div><!--panel-body-->
                </div><!--box-row-->
              </div><!--box-body-->
            </div><!-- row-->
          </div><!-- col-6-->
        </form>
      </div>
    </div>
  </section>
</div>
<!-- /.content-wrapper -->
<script src="<?php echo base_url(); ?>assets/admin/js/ckeditor/ckeditor.js"></script> 
<script>
    $(function () {
        CKEDITOR.replace('ckeditor');
    });
    $('#change_password').submit(function(){
      for (var i in CKEDITOR.instances) {         
        CKEDITOR.instances[i].updateElement();     
      }        
    });

</script> 