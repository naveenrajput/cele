<!-- Content Wrapper. Contains page content  -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $page_title;?> </h1>
        <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                <li class="<?php echo $breadcrumb['class'];?>"> 
                    <?php if(!empty($breadcrumb['link'])) { ?>
                        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                    <?php } else {
                        echo $breadcrumb['icon'].$breadcrumb['title'];
                    } ?>
                </li>
            <?php }?>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-primary"> 
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- flash messages-->
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('error') ?>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('success')) { ?>
                            <div class="alert alert-block alert-success fade in">
                            <button data-dismiss="alert" class="close" type="button">×</button>
                            <?php echo $this->session->flashdata('success') ?>
                        </div>
                        <?php } ?>
                 
                       	<?php if(isset($contact_detail) && !empty($contact_detail)) {
                       		$index=0;
                       		foreach($contact_detail as $list) {
                       			if($index==0) { ?>
                       				<div class="panel panel-primary"> 
							            <div class="box-header with-border">
					                  		<h3 class="box-title"><b>Name:</b> <?php if(!empty($list['name'])) echo $list['name'];?></h3>
                                            &nbsp;<medium><b> Email:</b> &nbsp;<?php if(!empty($list['email'])) echo $list['email'];?></medium>
                                            &nbsp;<medium><b> Contact No.:</b> &nbsp;<?php if(!empty($list['contact_no'])) echo $list['contact_no'];?></medium>
					                  		<span class="pull-right"><?php if(!empty($list['created'])) echo date('m/d/Y H:i',strtotime($list['created']));?></span>
					                	</div>
                                        <div class="box-header with-border">
                                            <p><b> Subject: </b><?php if(!empty($list['subject'])) echo $list['subject'];?></p>
                                        </div>
							            <div class="box-body">
							                <p><b> Message: </b><?php if(!empty($list['message'])) echo $list['message'];?></p>
							            </div>
							        </div>
                       			<?php } else { ?>
                       				<div class="panel panel-primary col-xs-offset-1 col-md-offset-1 col-lg-offset-1"> 
							            <div class="box-header with-border">
					                  		<h3 class="box-title">
                                            <?php 
                                            if($user_type!='Admin' && $list['adminparent']>0){
                                                echo $list['fullname'];
                                            }else{
                                                echo $list['name'];
                                            }
                                            ?>
                                            
                                            </h3>
					                  		<span class="pull-right"><?php if(!empty($list['created'])) echo date('m/d/Y H:i',strtotime($list['created']));?></span>
					                	</div>
							            <div class="box-body">
							                <p><?php if(!empty($list['message'])) echo $list['message'];?></p>
							            </div>
							        </div>
                       			<?php }
                       			$index++;
                       		}

                       	}?>
                        
                        <div class="panel">
                            <div class="">
                                <?php if(isset($from_action) && !empty($from_action)){ ?>
                                    <form method="POST" id="update_support_form" action="<?php echo $from_action; ?>" role="form"  onsubmit="return form_submit('update_support_form');" data-parsley-validate>
                                
                                    <div class="">
                                        <div class="form-group">
                                            <label for="message">Message *</label>
                                            <textarea class="form-control parsley-error ckeditor_required" name="message" id="ckeditor" placeholder="Message" rows="5" data-parsley-id="5" data-parsley-required data-parsley-required-message="Please enter message." data-parsley-errors-container="#msg_error"><?php if(set_value('message')) echo set_value('message');?> </textarea>
                                            <p class="error" id="msg_error"></p>
                                            <?php echo form_error('message');?> 
                                        </div>
                                    </div>
                                    <div class="box-footer">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-12 text-right">

                                                    <?php if(isset($from_action) && !empty($from_action)){ ?>
                                                        <button type="submit" id="reply" class="btn btn-primary" >Reply</button>
                                                    <?php } ?>
                                                    
                                                    <a href="<?php echo $back_action;?>" class="btn btn-default">Back</a> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <?php }else{ ?>
                                <a href="<?php echo $back_action;?>" class="btn btn-default">Back</a> 
                                <?php } ?>
                            </div><!-- panel body--> 
                        </div><!-- end panel --> 
                    </div><!-- col-6--> 
                </div><!-- row--> 
            </div><!-- /.box-body --> 
        </div><!-- /.box --> 
    </section><!-- /.content --> 
</div><!-- /.content-wrapper -->

<script src="<?php echo base_url(); ?>assets/admin/js/ckeditor/ckeditor.js"></script> 
<script>
    $(function () {
        // Replace the <textarea id="ckeditor"> with a CKEditor
        CKEDITOR.replace('ckeditor');
        $("#reply").click(function(){
            $('.ckeditor_required').attr('required', '');
            for (var i in CKEDITOR.instances){
                CKEDITOR.instances[i].updateElement();
              }
        });
    });

    function form_submit(id)
    {
        $("#"+id).parsley().validate();
        if($("#"+id).parsley().isValid()){ 
           //$("#reply").attr('disabled',true);
           $("#loader").show(); 
           return true;
        }else{
            return false;
        }
    }
</script>