<style type="text/css">
    .direct-chat-text {
    border-radius: 5px;
    position: relative;
    padding: 5px 10px;
    background: #337ab7;
    border: 1px solid #337ab7;
    margin: 5px 0 0 50px;
    color: #fff;
}

</style>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?php 
         $role_id  = $this->session->userdata('role_id');  

        if(isset($page_title)) echo $page_title; ?></h1>        
        <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                <li class="<?php echo $breadcrumb['class'];?>"> 
                    <?php if(!empty($breadcrumb['link'])) { ?>
                        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                    <?php } else {
                        echo $breadcrumb['icon'].$breadcrumb['title'];
                    } ?>
                </li>
            <?php }?>
        </ol>
    </section>



    <section class="content">
        <div class="row">
           <div class="col-lg-12">
                <div class="col-lg-12">
                  
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="panel-body">
                              
                                    <!-- ajax error msg -->
                                    <div id= 'notification_msg'> </div> 
                                    <a class="btn btn-success" style="float: right;" href="<?php echo base_url()?>admin/user/friend/<?php echo $my_user_id?>">Back</a>
                                </div><!-- panel body-->
                            </div>
                        </div>
               
                </div><!-- col-6-->

            </div> 


            <div class="col-lg-12">
                <div class="col-lg-12">
                    <div class="box box-primary">
                        <!-- /.box-header -->
                        <div class="box-body" style="height: 504px !important; overflow: scroll;">
                            <div class="row">
                                <div class="panel-body direct-chat direct-chat-warning">
                                    <div class="">
                                
                                <?php  
                                if(isset($chat_history) && !empty($chat_history)) 
                                { 
                                      // echo "<pre>";print_r($chat_history);die;
                                foreach($chat_history as $list) { 
                                 ?>  
                                    <?php if($list['sender_id']!=$my_user_id){ ?> 
                                        <div class="direct-chat-msg" style="width: 60%">
                                            <div class="direct-chat-info clearfix">
                                                <span class="direct-chat-name pull-left"><?php echo ucfirst($list['user_name']);?></span>
                                                <span class="direct-chat-timestamp pull-right"><?php echo get_timeago(strtotime($list['created']));?></span>
                                            </div>
                                            <img class="direct-chat-img" src="<?php if(!empty($list['profile_picture'])){ echo $list['profile_picture'];}else{ echo base_url().'resources/default_image.png';}?>" alt="message user image">
                                                <div class="direct-chat-text"> 
                                                    <?php 
                                                    if($list['type']=='Text'){

                                                        if(!empty($list['message'])) echo $list['message'];  

                                                    }elseif ($list['type']=='Image') { ?>

                                                        <a target="_"  href="<?php if(!empty($list['message'])){ echo base_url().$list['message'];}else{ echo base_url().'resources/default_image.png';} ?>"><img width="100px" height="100px" src="<?php if(!empty($list['message'])){ echo base_url().$list['message'];}else{ echo base_url().'resources/default_image.png';} ?>">
                                                        </a>
                                                        <b><?php echo $list['original_file_name']?></b>

                                                    <?php  }elseif ($list['type']=='Document') { ?>

                                                        <a target="_" href="<?php if(!empty($list['message'])){ echo base_url().$list['message'];}else{ echo base_url().'resources/pdf_demo.png';} ?>"><img width="100px"
                                                         height="100px" src="<?php if(!empty($list['message'])){ echo base_url().'resources/pdf_demo.png';}else{ echo base_url().'resources/pdf_demo.png';} ?>">
                                                        </a> 
                                                        <b><?php echo $list['original_file_name']?></b>

                                                    <?php }elseif ($list['type']=='Video') { ?> 
                                                            <a target="_" href="<?php if(!empty($list['message'])){ echo base_url().$list['message'];}else{ echo base_url().'resources/video_demo.png';} ?>"><img width="100px"
                                                             height="100px" src="<?php if(!empty($list['message'])){ echo base_url().'resources/video_demo.png';}else{ echo base_url().'resources/video_demo.png';} ?>">
                                                            </a> 
                                                            <b><?php echo $list['original_file_name']?></b>
                                                    <?php }   ?>


                                                </div> 
                                        </div> 
                                    <?php }else{?>
                                        <div class="direct-chat-msg right" style="width: 60%;margin-left: 40%;">
                                          <div class="direct-chat-info clearfix">
                                            <span class="direct-chat-name pull-right"><?php echo ucfirst($list['user_name']);?></span>
                                            <span class="direct-chat-timestamp pull-left"><?php echo get_timeago(strtotime($list['created']));?></span>
                                          </div>
                                          <img class="direct-chat-img" src="<?php if(!empty($list['profile_picture'])){ echo $list['profile_picture'];}else{ echo base_url().'resources/default_image.png';}?>" alt="message user image">
                                               <div class="direct-chat-text">
                                                   <?php 
                                                    if($list['type']=='Text'){

                                                        if(!empty($list['message'])) echo $list['message'];  

                                                    }elseif ($list['type']=='Image') { ?>

                                                        <a target="_"  href="<?php if(!empty($list['message'])){ echo base_url().$list['message'];}else{ echo base_url().'resources/default_image.png';} ?>"><img width="100px" height="100px" src="<?php if(!empty($list['message'])){ echo base_url().$list['message'];}else{ echo base_url().'resources/default_image.png';} ?>">
                                                        </a>
                                                        <b><?php echo $list['original_file_name']?></b>

                                                    <?php  }elseif ($list['type']=='Document') { ?>

                                                        <a target="_" href="<?php if(!empty($list['message'])){ echo base_url().$list['message'];}else{ echo base_url().'resources/pdf_demo.png';} ?>"><img width="100px"
                                                         height="100px" src="<?php if(!empty($list['message'])){ echo base_url().'resources/pdf_demo.png';}else{ echo base_url().'resources/pdf_demo.png';} ?>">
                                                        </a> 
                                                        <b><?php echo $list['original_file_name']?></b>

                                                    <?php }elseif ($list['type']=='Video') { ?> 
                                                            <a target="_" href="<?php if(!empty($list['message'])){ echo base_url().$list['message'];}else{ echo base_url().'resources/video_demo.png';} ?>"><img width="100px"
                                                             height="100px" src="<?php if(!empty($list['message'])){ echo base_url().'resources/video_demo.png';}else{ echo base_url().'resources/video_demo.png';} ?>">
                                                            </a> 
                                                            <b><?php echo $list['original_file_name']?></b>
                                                    <?php }   ?>

                                                </div>  

                                        </div> 

                                    <?php }?> 
                                <?php } } ?> 
                               
                                </div><!-- panel body-->
                            </div>
                        </div>
                    </div><!-- row-->
                </div><!-- col-6-->

            </div>            
        </div>
        <!-- /.box -->
    </section>

</div>

 