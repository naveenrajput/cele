<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  	<h1> <?php echo $page_title;?> </h1>
  		<ol class="breadcrumb">
			<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
				<li class="<?php echo $breadcrumb['class'];?>"> 
					<?php if(!empty($breadcrumb['link'])) { ?>
						<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
					<?php } else {
						echo $breadcrumb['icon'].$breadcrumb['title'];
					} ?>
				</li>
			<?php }?>
  		</ol>
	</section>
	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12 message_box">
				<?php if ($this->session->flashdata('error')) { ?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php } ?>
				<?php if ($this->session->flashdata('success')) { ?>
					<div class="alert alert-block alert-success fade in">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
				<?php } ?>
				<div class="box">
					<div class="box-header with-border">  
                  		<!--<h3 class="box-title">Filter Here</h3>-->  
                		<form method="get" action="<?php echo $action_url;?>"> 
						<div class="box-body row"> 
							<div class="form-group col-md-3">
								<input class="column_filter form-control" id="userfullname" name='userfullname' type="text" placeholder="User Name" value="<?php echo $filter_user_fullname;?>">
							</div>
				             <div class="form-group col-md-3">
				               	<input class="btn btn-primary" type="submit" value="Filter">
				               	<a class="btn btn-default" href="<?php echo $action_url;?>">Reset</a>
				               	<a class="btn btn-success" href="<?php echo $back_url;?>">Back</a>
				             </div> 
			          	</div>
			          </form> 
			            <?php if(isset($add_action) && !empty($add_action)){ ?>
                  			<!-- <a href="<?php echo $add_action;?>" title="" data-toggle="tooltip" data-original-title="Add User" class="btn btn-default pull-right"><i class="fa fa-plus"></i></a> -->
                		<?php }

                		// echo "<pre>";print_r($records_results);die;
                		 ?>
                	</div> 

					<div class="box-body">

						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>User Name</th>
									<th>Login ID</th>
									<th>Join Date</th> 
									<th>Friends since</th> 
									<th>User Status</th> 
									<th>Action</th> 
								
								</tr>
							</thead>
							<tbody>  
								<?php 
								if(!empty($records_results))
								{	
									// echo "<pre>";print_r($records_results);
									$i = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
									foreach ($records_results as $row) { $i++; 
										if(isset($row['status'])) {
	                                        if($row['status']=="Active") {
	                                            $status = "Active";
	                                            $class = "pointer badge bg-green";
	                                        }elseif($row['status']=="Unverified") {
	                                            $status = "Unverified";
	                                            $class = "pointer badge bg-yellow";
	                                        } else {
	                                            $status = "Inactive";
	                                            $class = "pointer badge bg-red";
	                                        }
                                    	}
								 	?>
										<tr id="tr_<?php echo $row['id']; ?>">
											<td width="5%"><?php echo $i; ?></td>
											<td><?php if(!empty($row['fullname'])) echo ucfirst($row['fullname']);?></td>
											<td><?php if(!empty($row['staging_id'])) echo ucfirst($row['staging_id']);?></td>  
											<td><?php echo convertGMTToLocalTimezone($row['join_date'],true)?></td> 
											<td><?php echo convertGMTToLocalTimezone($row['modified'],true)?></td> 
											<td><p id="" class="<?php echo $class; ?>" title=""><?php echo $status; ?></p></td>
											<td>
											<?php if($this->Chat_model->findRoomId($user_id,$row['user_id'])){?>
											<a id="" href="<?php echo base_url().'admin/user/chat_history/'.$user_id.'/'.$row['user_id']?>" class="btn btn-xs btn-success" title="" data-toggle="tooltip" data-original-title="View Chat">
														<i class="fa fa-eye"></i>
													</a>
											<?php }?>
													</td>
										</tr>
									<?php }
								} else {
									echo "<tr><td colspan='7' align='center'> No Record Found</td></tr>";
								} ?>
							</tbody>
							<tfoot>						
								<tr>
									<?php if(!empty($pagination)) { ?>
										<td colspan="3">Total Records - <?php echo $total_records;?></td>
										<td colspan="4" align="center">
											<div><?php echo $pagination; ?></div>
										</td>
									<?php }else{ ?>	
										<td  colspan="3" >Total Records - <?php if($total_records >0){echo $total_records;} else{echo '0';}?></td>
										<td colspan="4" align="center"></td>
									<?php } ?>			
								</tr>
							</tfoot>	
						</table>
					</div>			
				</div>	
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->