<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
	  	<h1> <?php echo $page_title;?> </h1>
  		<ol class="breadcrumb">
			<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
				<li class="<?php echo $breadcrumb['class'];?>"> 
					<?php if(!empty($breadcrumb['link'])) { ?>
						<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
					<?php } else {
						echo $breadcrumb['icon'].$breadcrumb['title'];
					} ?>
				</li>
			<?php }?>
  		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-xs-12 message_box">
				<?php if ($this->session->flashdata('error')) { ?>
					<div class="alert alert-block alert-danger fade in">
						<button data-dismiss="alert" class="close" type="button">×</button>
						<?php echo $this->session->flashdata('error') ?>
					</div>
				<?php } ?>
				<?php if ($this->session->flashdata('success')) { ?>
					<div class="alert alert-block alert-success fade in">
					<button data-dismiss="alert" class="close" type="button">×</button>
					<?php echo $this->session->flashdata('success') ?>
				</div>
				<?php } ?>
				<div class="box">
					<div class="box-header with-border">  
                  		<!--<h3 class="box-title">Filter Here</h3>-->  
                		<form method="get" action="<?php echo base_url().'admin/user/list';?>"> 
						<div class="box-body row"> 
							<div class="form-group col-md-3">
								<input class="column_filter form-control" id="fullname" name='fullname' type="text" placeholder="User Name" value="<?php echo $filter_fullname;?>">
							</div>
			              	<div class="form-group col-md-3">
                                <input class="column_filter form-control" id="staging_id" name='staging_id' type="text" placeholder="Login ID" value="<?php echo $filter_staging_id;?>">
			              	</div>
			              	<div class="form-group col-md-3">
                                <input class="column_filter form-control" id="mobile" name='mobile' type="text" placeholder="Mobile" value="<?php echo $filter_mobile;?>">
			              	</div>
			              	<div class="form-group col-md-3">
				              	<select name="user_type" id="user_type" class="column_filter form-control">
				              		<option value="">User Type</option>
				              		<option value="1" <?php if(!empty($filter_user_type)&& $filter_user_type=='1'){ echo 'selected'; }?>>Celebrant</option>
				              		<option value="2" <?php if(!empty($filter_user_type)&& $filter_user_type=='2'){ echo 'selected'; }?>>Service Provider</option>
				              		<option value="3" <?php if(!empty($filter_user_type)&& $filter_user_type=='3'){ echo 'selected'; }?>>Celebrant + Service Provider</option>
				              		
				              	</select> 
                        	</div> 
			              	<div class="form-group col-md-3">
				              	<select name="status" id="status" class="column_filter form-control">
				              		<option value="">Status</option>
				              		<option value="Unverified" <?php if(!empty($filter_status)&& $filter_status=='Unverified'){ echo 'selected'; }?>>Unverified</option>
				              		<option value="Active" <?php if(!empty($filter_status)&& $filter_status=='Active'){ echo 'selected'; }?>>Active</option>
				              		<option value="Inactive" <?php if(!empty($filter_status)&& $filter_status=='Inactive'){ echo 'selected'; }?>>Inactive</option>
				              	</select> 
	                        </div> 
			             <div class="form-group col-md-3">
			               	<input class="btn btn-primary" type="submit" value="Filter">
			               	<a class="btn btn-default" href="<?php echo base_url().'admin/user/list';?>">Reset</a>
			 
			             </div>
			             
			          	</div>
			          </form> 
			            <?php if(isset($add_action) && !empty($add_action)){ ?>
                  			<!-- <a href="<?php echo $add_action;?>" title="" data-toggle="tooltip" data-original-title="Add User" class="btn btn-default pull-right"><i class="fa fa-plus"></i></a> -->
                		<?php } ?>
                	</div> 

					<div class="box-body">

						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Profile Pic</th>
									<th>User Name</th>
									<th>Login ID</th>
									<th>Mobile</th>
									<th>User Type</th>
									<th>Registered Since </th>
									<?php if(isset($edit_action) && !empty($edit_action)){ ?>
									<th>Status</th>
									<?php } ?>
									 <?php if(isset($edit_action) || isset($delete_action)){ ?>
                                    	<th>Actions</th>
                                    <?php } ?>
								</tr>
							</thead>
							<tbody>
	

								<?php 
								if(!empty($records_results))
								{	
									// echo "<pre>";print_r($records_results);die;
									$i = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
									$table="users";
									$field = "user_id";

									foreach ($records_results as $row) { $i++; 
										if(isset($row['status'])) {
	                                        if($row['status']=="Active") {
	                                            $status = "Active";
	                                            $class = "pointer badge bg-green";
	                                        }elseif($row['status']=="Unverified") {
	                                            $status = "Unverified";
	                                            $class = "pointer badge bg-yellow";
	                                        } else {
	                                            $status = "Inactive";
	                                            $class = "pointer badge bg-red";
	                                        }
                                    	}?>
										<tr id="tr_<?php echo $row[$field]; ?>">
											<td><?php echo $i; ?></td> 
											<td>
												<?php if(!empty($row['profile_picture']) && file_exists($row['profile_picture'])){
													$image = base_url().$row['profile_picture'];
												}else{
													$image = base_url().'resources/default_image.png';
												}
												?> 
												<a href="<?php echo $image?>"><img src="<?php echo $image?>" width="25px" height="25px"></a> 
											</td> 
											<td><?php if(!empty($row['fullname'])) echo ucfirst($row['fullname']);?></td>
											<td><?php if(!empty($row['staging_id'])) echo ucfirst($row['staging_id']);?></td>
											<td><?php if(!empty($row['mobile'])) echo ucfirst($row['mobile']);?></td>
											<td><?php if(!empty($row['user_type'])) {
													if($row['user_type']==1){
														echo 'Celebrant';
													}if($row['user_type']==2){
														echo 'Service Provider';
													}if($row['user_type']==3){
														echo 'Celebrant+Service Provider';
													}
											}?></td>
											<td><?php if(!empty($row['created'])) echo convertGMTToLocalTimezone($row['created']);?></td>
											<?php if(isset($edit_action) && !empty($edit_action)){ ?>
												<td>
													<p id="status_<?php echo $row[$field]; ?>" onclick="change_user_status('<?php echo $field; ?>','<?php echo $row[$field]; ?>','<?php echo $table; ?>')" class="<?php echo $class; ?>" title="" data-toggle="tooltip" data-original-title="Change Status"><?php echo $status; ?></p>
												</td>
											<?php } ?>
											
											<td class="td-actions">
												<div class="btn-group">
								                  <button type="button" class="btn btn-sm btn-info">Action</button>
								                  <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								                    <span class="caret"></span>
								                    <span class="sr-only">Toggle Dropdown</span>
								                  </button>
								                  <ul class="dropdown-menu" role="menu">
								                    <li><a  href="<?php echo base_url()."admin/user/user_profile/".$row['user_id'] ?>" >Detail</a>
								                    </li>
							                    	<li><a  href="<?php echo base_url()."admin/user/post?user_id=".$row['user_id'] ?>" >Post List</a>
								                    </li>
							                    	<li><a  href="<?php echo base_url()."admin/user/subscription?user_id=".$row['user_id'] ?>" >Subscription List</a>
								                    </li>
								                    <?php if($row['user_type']!='1'){?>
							                    	<li><a  href="<?php echo base_url()."admin/user/services_list?user_id=".$row['user_id'] ?>" >Services List</a>
								                    </li>
								                    <?php }?>
								                    <?php if(isset($edit_action) && !empty($edit_action)){ ?>
							                    		<li><a   href="<?php echo $edit_action.'/'.$row[$field]; ?>">Edit</a></li>
							                    	<?php } ?>
							                    	<?php if(isset($delete_action) && !empty($delete_action)) { ?>
													<?php if(!empty($undeletable_ids) && !empty($row[$field])) {
														if (!in_array($row[$field], $undeletable_ids)) { ?>
															<li><a   href="JavaScript:Void();" onclick="return soft_delete_record(<?php if(!empty($row[$field])) echo $row[$field]; ?>,'<?php echo $table; ?>','<?php echo $field; ?>');">Delete</a></li>
														<?php }
													}elseif(empty($undeletable_ids)){ ?>
														<li><a   href="JavaScript:Void();" onclick="return soft_delete_record(<?php if(!empty($row[$field])) echo $row[$field]; ?>,'<?php echo $table; ?>','<?php echo $field; ?>');">Delete</a></li>
													<?php } 
												} ?>
								                  </ul>
								                </div>
											</td>
										</tr>

									<?php }
								} else {
									echo "<tr><td colspan='7' align='center'> No Record Found</td></tr>";
								} ?>
							</tbody>
						</table>
							<tfoot>						
								<tr>
									<?php if(!empty($pagination)) { ?>
										<td >Total Records - <?php echo $total_records;?></td>
										<td colspan="7" align="center">
											<div><?php echo $pagination; ?></div>
										</td>
									<?php }else{ ?>	
										<td align="center">Total Records - <?php if($total_records >0){echo $total_records;} else{echo '0';}?></td>
										<td colspan="7" align="center"></td>
									<?php } ?>			
								</tr>
							</tfoot>	
					</div>			
				</div>			
			</div>
		</div>
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

