<div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $page_title;?> </h1>
        <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                <li class="<?php echo $breadcrumb['class'];?>"> 
                    <?php if(!empty($breadcrumb['link'])) { ?>
                        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                    <?php } else {
                        echo $breadcrumb['icon'].$breadcrumb['title'];
                    } ?>
                </li>
            <?php } ?>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box box-primary"> 
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">  
                    <div class="col-lg-8">
                        <!-- flash messages-->
                        <div class="message_box" style="display:none;"></div>
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="alert alert-block alert-danger fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('error') ?>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('success')) { ?>
                            <div class="alert alert-block alert-success fade in">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $this->session->flashdata('success') ?>
                            </div>
                        <?php } ?>
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <?php if(isset($form_action) && !empty($form_action)){ ?>
                                    <form id="request_type_form" class="" method="POST" action="" enctype="multipart/form-data" role="form"  data-parsley-validate>
                                <?php } ?>
                                <div class="box-body">
                                    <div class="row"> 
                                        <div class="form-group col-md-6">
                                            <label for="email">User Type *</label>
                                            <select disabled name="user_type" id="user_type" class="column_filter form-control" data-parsley-required data-parsley-required-message="Please select user type.">
                                                <option value="">User Type</option>
                                                <option value="1" <?php echo ($details['user_type']==1)?'selected':''; ?>>Celebrant</option>
                                                <option value="2" <?php echo ($details['user_type']==2)?'selected':''; ?>>Service Provider</option>
                                                <option value="3" <?php echo ($details['user_type']==3)?'selected':''; ?>>Celebrant + Service Provider</option>
                                            </select> 
                                        </div> 

                                        <div class="form-group col-md-6">
                                            <label for="email">Login ID *</label>
                                                <?php //$stag_id = explode('@', $details['staging_id']); ?>
                                                <?php $stag_id = $details['staging_id']; ?>
                                                <input disabled type="text"  class="form-control" id="staging_id" name="staging_id" placeholder="Login ID"   oninput="this.value = this.value.replace(/[^A-Za-z0-9.']/g,'');" value="<?php echo !empty($stag_id)?$stag_id:''; ?>" maxlength="200" data-parsley-required data-parsley-required-message="Please enter login ID.">
                                                <!-- <span class="input-group-addon">@cele.com</span> -->
                                                                                   
                                            <p class="error" id="un_error_title" style="display:none;">Login ID already used.</p>
                                            <?php echo form_error('staging_id'); ?>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="form-group col-md-6">
                                            <label for="title">Email *</label>
                                            <input type="text" disabled class="form-control" name="email" id="email" value="<?php echo !empty($details['email'])?$details['email']:''; ?>" placeholder="Email" maxlength="100" data-parsley-required data-parsley-required-message="Please enter email."> 
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="mobile">Mobile Number *</label>
                                            <input disabled type="text" class="form-control" id="mobile"  name="mobile" oninput="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'');mobile_check()" placeholder="Mobile Number" value="<?php echo !empty($details['mobile'])?$details['mobile']:''; ?>" 
                                            data-parsley-required data-parsley-required-message="Please enter telephone number." 
                                            data-parsley-type="number" 
                                            maxlength="25" 
                                            data-parsley-type-message="Please enter valid telephone number" data-parsley-minlength-message="Please enter valid number">
                                            <p class="error" id="access_mobile_error" style="display:none;">Mobile number already used.</p>
                                            <?php echo form_error('mobile'); ?>
                                        </div>

                                    </div>
                                    <div class="row">

                                        <div class="form-group col-md-6">
                                            <label for="title">Full Name *</label>
                                            <input type="text" class="form-control" name="fullname" id="fullname" value="<?php echo !empty($details['fullname'])?$details['fullname']:''; ?>" placeholder="Full Name" maxlength="100" data-parsley-required data-parsley-required-message="Please enter full name.">
                                            <?php echo form_error('fullname'); ?>
                                        </div>
                                        <?php if($details['user_type']!=1){ ?>
                                            <div class="form-group col-md-6">
                                                <label for="title">Company Name *</label>
                                                <input type="text" class="form-control" name="company_name" id="company_name" value="<?php echo !empty($details['company_name'])?$details['company_name']:''; ?>" placeholder="Company Name" maxlength="150" data-parsley-required data-parsley-required-message="Please enter company name.">
                                                <?php echo form_error('company_name'); ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <?php if($details['user_type']!=1){ ?>
                                        <div class="row">  
                                            <div class="form-group col-md-6">
                                                <label for="description">Year of Establishment *</label>
                                                <input type="text" class="form-control" name="establishment_year" id="establishment_year" value="<?php echo !empty($details['establishment_year'])?$details['establishment_year']:''; ?>" placeholder="Year of Establishment" maxlength="4" oninput="this.value = this.value.replace(/[^0-9]/g,'');" data-parsley-required data-parsley-required-message="Please enter year of establishment." >
                                                <?php echo form_error('establishment_year'); ?>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="description">Business Number </label>
                                                <input type="text" class="form-control" name="business_no" id="business_no" value="<?php echo !empty($details['business_no'])?$details['business_no']:''; ?>" placeholder="Business Number" maxlength="50">
                                                <?php echo form_error('business_no'); ?>
                                            </div>
                                        </div>
                                    <?php }else{ ?>
                                        <div class="row">
                                            <div class="form-group col-md-6">
                                                <label for="Gender">Date of Birth *</label>
                                                <div class="input-group">
                                                  <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                  </div>
                                                  <input type="text" class="form-control datepicker" name ="dob" id="dob" placeholder="Date of Birth" value="<?php if($details['dob']!='0000-00-00'){echo date('m/d/Y',strtotime($details['dob']));}?>" oninput="this.value = this.value.replace(/[^]/g, '').replace(/(\..*)\./g, '$1');"/>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="relationship_status">Relationship Status</label>
                                                <select  name="relationship_status" id="relationship_status" class="column_filter form-control">
                                                <option value="">Select Relationship Status</option>
                                                    <option value="Single" <?php echo ($details['relationship_status']=='Single')?'selected':''; ?>>Single</option>
                                                    <option value="Married" <?php echo ($details['relationship_status']=='Married')?'selected':''; ?>>Married</option> 
                                                </select> 
                                            </div>
                                        </div>
                                        <div class="row">  
                                            <div class="form-group col-md-6">
                                                <label for="Gender">Gender</label>
                                                <select  name="gender" id="gender" class="column_filter form-control">
                                                    <option value="">Select Gender</option>
                                                    <option value="Male" <?php echo ($details['gender']=='Male')?'selected':''; ?>>Male</option>
                                                    <option value="Female" <?php echo ($details['gender']=='Female')?'selected':''; ?>>Female</option> 
                                                    <option value="Other" <?php echo ($details['gender']=='Other')?'selected':''; ?>>Other</option> 
                                                </select> 
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="description">Education </label>
                                                <input type="text" class="form-control" name="education" id="education" value="<?php echo !empty($details['education'])?$details['education']:''; ?>" placeholder="Education" maxlength="150">
                                                <?php echo form_error('education'); ?>
                                            </div>
                                        </div>
                                        <div class="row">   
                                            <div class="form-group col-md-6">
                                                <label for="description">Hobbies </label>
                                                <input type="text" class="form-control" name="hobbies" id="hobbies" value="<?php echo !empty($details['hobbies'])?$details['hobbies']:''; ?>" placeholder="hobbies" maxlength="200">
                                                <?php echo form_error('hobbies'); ?>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="description">Interests </label>
                                                <input type="text" class="form-control" name="interests" id="interests" value="<?php echo !empty($details['interests'])?$details['interests']:''; ?>" placeholder="Interests" maxlength="200">
                                                <?php echo form_error('interests'); ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-12">
                                                <label for="description">About Description </label>
                                               <textarea class="form-control" maxlength="250" name="about_description" id="about_description" placeholder="About Description" style="height: 100px;"><?php if(!empty($details['about_description'])){echo $details['about_description'];}?></textarea>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <!-- <div class="row">   
                                        <div class="form-group col-md-6">
                                            <label for="description">Hometown </label>
                                            <input type="text" class="form-control" name="hometown" id="hometown" value="<?php /*echo !empty($details['hometown'])?$details['hometown']:'';*/ ?>" placeholder="Hometown" maxlength="100">
                                            <?php /*echo form_error('hometown');*/ ?>
                                        </div>
                                    </div>  -->


                                 <?php if(!empty($address)){
                                    foreach ($address as $key => $add_list) { 
                                        $states = '';
                                        $city = '';

                                        $states = $this->Common_model->getRecords('states','*',array('country_id'=>$add_list['country']));
                                        $cities = $this->Common_model->getRecords('cities','*',array('state_id'=>$add_list['state']));
                                        ?>

                                     <div class="row">   
                                        <div class="form-group col-md-12">
                                            <label for="description"><b>Address <?php echo ++$key?></b> </label> 
                                         </div> 
                                    </div>
                                    <input type="hidden" name="id_array[]" value="<?php echo $add_list['id']?>">
                                    <div class="row"> 
                                        <div class="form-group col-md-6">
                                            <label for="country">Country *</label>
                                            <select class="form-control" id="country<?php echo $add_list['id']?>" name="country[]" onchange="country_change(<?php echo $add_list['id']?>)" data-parsley-required data-parsley-required-message="Please select country.">
                                                <option value=''>Select Country</option>
                                                <?php if(isset($countries)) {
                                                    foreach($countries as $country) { ?>
                                                        <option <?php echo ($add_list['country']==$country['id'])?'selected':''; ?> value=<?php echo $country['id'];?> ><?php echo $country['name'];?></option>";
                                                    <?php }
                                                } ?>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="state">State</label>
                                            <select class="form-control" id="state<?php echo $add_list['id']?>"  onchange="state_change(<?php echo $add_list['id']?>)"  name="state[]" data-parsley-required data-parsley-required-message="Please select state.">
                                                <option value='' id='first<?php echo $add_list['id']?>'>Select State</option>
                                                <?php if(isset($states)) {
                                                    foreach($states as $state) { ?>
                                                        <option <?php echo ($add_list['state']==$state['id'])?'selected':''; ?> value=<?php echo $state['id'];?> ><?php echo $state['name'];?></option>";
                                                    <?php }
                                                } ?>                                                
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="state">City</label>
                                            <select class="form-control" id="city<?php echo $add_list['id']?>" name="city[]" data-parsley-required data-parsley-required-message="Please select city.">
                                                <option value='' id='first<?php echo $add_list['id']?>'>Select City</option>
                                                <?php if(isset($cities)) {
                                                    foreach($cities as $city) { ?>
                                                        <option <?php echo ($add_list['city']==$city['id'])?'selected':''; ?> value=<?php echo $city['id'];?> ><?php echo $city['name'];?></option>";
                                                    <?php }
                                                } ?>                                                
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6"> 
                                            <label for="description">Address *</label>
                                             <input type="text" name="address[]" placeholder="Address" class="form-control" rows="3" autocomplete="off" data-parsley-required data-parsley-required-message="Please enter address." maxlength="200" value="<?php echo !empty($add_list['address'])?$add_list['address']:''; ?>">
                                         
                                        </div>
                                    </div>
                                    <?php }  
                                    }?>
                                    <?php if($details['user_type']!=1){ ?>
                                        <div class="row">  
                                            <div class="form-group col-md-6">
                                                <label for="description">Document Type *</label>
                                                <input disabled type="text" class="form-control" name="document_type" id="document_type" value="<?php echo !empty($details['document_type'])?str_replace('_',' ',$details['document_type']):''; ?>" placeholder="Document Type"  >
                                                <?php echo form_error('document_type'); ?>
                                            </div>
                                             <div class="form-group col-md-6">
                                              <label for="email">Document Status *</label>
                                                    <select  name="document_status" id="document_status" class="column_filter form-control" data-parsley-required data-parsley-required-message="Please select document status.">
                                                    <?php if($details['document_status']=='Unverified'){?>
                                                    <option value="Unverified" <?php echo ($details['document_status']=='Unverified')?'selected':''; ?>>Unverified</option>
                                                    <?php }?>
                                                    <option value="Accepted" <?php echo ($details['document_status']=='Accepted')?'selected':''; ?>>Accepted</option>
                                                    <option value="Rejected" <?php echo ($details['document_status']=='Rejected')?'selected':''; ?>>Rejected</option>
                                                </select> 
                                            </div> 
                                        </div>

                                        <div class="row">  
                                            <div class="form-group col-md-6">
                                                <label for="description">Document  *</label><br>
                                                <?php if(!empty($details['document_proof'])){ 
                                                    echo '<a target="_" href='.base_url().$details['document_proof'].'>View Document</a>';
                                                }?>
                                            </div> 
                                        </div> 
                                    <?php } ?>
                                </div>
                                <div class="box-footer text-center">
                                    <?php if(isset($form_action) && !empty($form_action)){ ?>
                                        <!-- <button type="submit" id="submit_form" class="btn btn-primary" onclick=" form_submit('request_type_form');">Update</button> -->
                                        <a href="javascript:void(0)" class="btn btn-primary" onclick=" form_submit('request_type_form');">Update</a>
                                    <?php } ?>
                                    <a href="<?php echo $back_action;?>" class="btn btn-default">Back</a> 
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!-- /.box --> 
            </div><!-- col-12--> 
        </div><!-- row--> 
    </section>
</div>
<!-- row--> 
<!-- Include Bootstrap Datepicker -->

<script>
 $('.datepicker').datepicker({
        format: 'mm/dd/yyyy',
        todayHighlight: true,
        endDate: '+0d',
        autoclose: true,
        orientation: "bottom auto",
    });
 
function country_change(loopid){
        var id = $("#country"+loopid).val();
            if(id!='') {
                $('#state'+loopid+' option').slice(1).remove();
                $('#city'+loopid+' option').slice(1).remove();
                $.ajax({
                    type:'POST',
                    url: "<?php echo base_url(); ?>admin/ajax/get_states/",
                    data: {id:id},
                    
                    success:function(data)
                    {
                        if(data) {
                            var states="";
                            data = JSON.parse(data);
                            for(var i=0;i<data.length;i++) {
                                states += "<option value="+data[i].id+">"+data[i].name+"</option>";
                            }
                            $("#state"+loopid).find("#first"+loopid).after(states);
                        } else {
                            error_msg = "Some Error occured. Please try again !!" ;
                            error = '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close" type="button">×</button>'+error_msg+'</div>';
                            $('#notification_msg').html(error).fadeIn(250).fadeOut(10000);
                        }
                    }
                });
            }
}

function state_change(loopid){
  var id = $("#state"+loopid).val();
    
  if(id!='') {
    $('#city'+loopid+' option').slice(1).remove();
    $.ajax({
      type:'POST',
      url: "<?php echo base_url(); ?>admin/ajax/get_cities/",
      data: {id:id},
          
      success:function(data)
      {
        if(data) {
         $('#city'+loopid+' option').remove();
      
          var city_option ="<option value='' id='city_first"+loopid+"'>Select City</option>";
          $("#city"+loopid).append(city_option);

          var cities="";
          data = JSON.parse(data);
          for(var i=0;i<data.length;i++) {
              cities += "<option value="+data[i].id+">"+data[i].name+"</option>";
          }
          //$("#city").find("#city_first").after(cities);
          $("#city"+loopid).find("#city_first"+loopid).after(cities);
        } else {
          error_msg = "Some Error occurred. Please try again !!" ;
          error = '<div class="alert alert-block alert-danger fade in"><button data-dismiss="alert" class="close" type="button">×</button>'+error_msg+'</div>';
          $('#notification_msg').html(error).fadeIn(250).fadeOut(10000);
        }
      }
    });
  }
}



var is_mobile_valid=1;
function mobile_check(){
    error_msg_mobile="";
    $("#access_mobile_error").hide();
    var matched_value = $("#mobile").val().trim();
    var matched_column = 'mobile';
    var table="users";
    var id = <?php echo $details['user_id'] ?>;
    var matched_id="user_id";
    if(matched_value!='') {
        $.ajax({
            type:'POST',
            url: "<?php echo base_url(); ?>admin/ajax/check_unique/",
            data: {matched_value:matched_value,matched_column:matched_column,table:table,id:id,matched_id:matched_id},
            
            success:function(data)
            {
                if(data==1) {
                    is_mobile_valid=0;
                    error_msg_mobile = "Mobile number already used." ;
                    $("#access_mobile_error").show();
                } else {
                    $("#access_mobile_error").hide();
                    is_mobile_valid=1;
                }
                
            }
        });
    } else {
        is_mobile_valid=1;
        error_msg_mobile = "" ;
        $("#access_mobile_error").hide();
    }
}




function form_submit(id)
{
 
       submitDetailsForm(id);

    //return false;
}
</script>


