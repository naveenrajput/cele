<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $page_title;?> </h1>
        <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                <li class="<?php echo $breadcrumb['class'];?>"> 
                    <?php if(!empty($breadcrumb['link'])) { ?>
                        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                    <?php } else {
                        echo $breadcrumb['icon'].$breadcrumb['title'];
                    } ?>
                </li>
            <?php }?>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <?php if ($this->session->flashdata('error')) { ?>
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <?php echo $this->session->flashdata('error') ?>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('success')) { ?>
                    <div class="alert alert-block alert-success fade in">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <?php echo $this->session->flashdata('success') ?>
                </div>
                <?php } ?>
                <div class="box">
                    <div class="box-header with-border">  
                        <?php if(isset($add_action) && !empty($add_action)){ ?>
                            <a href="<?php echo $add_action;?>" title="" data-toggle="tooltip" data-original-title="Add Advertisement" class="btn btn-default pull-right"><i class="fa fa-plus"></i></a>
                        <?php } ?>  
                    </div> 

                    <div class="box-body">
                        <table <?php  if(!empty($records_results))
                                {   ?>  <?php } ?> class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Image</th>
                                    <th>Expiry Date</th>
                                    <?php if(isset($edit_action) && !empty($edit_action)){ ?>
                                        <th>Status</th>
                                    <?php } ?>
                                    <?php if(isset($edit_action) || isset($delete_action)){ ?>
                                      <th>Actions</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
    

                             <?php 
                            if(!empty($records_results))
                            {   
                                $i = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                                $table="advertisement";
                                $field = "advertisement_id";
                                foreach ($records_results as $row) { $i++; 
                                    if(isset($row['status'])) {
                                        if($row['status']=="Active") {
                                            $status = "Active";
                                            $class = "pointer badge bg-green";
                                        } else {
                                            $status = "Inactive";
                                            $class = "pointer badge bg-red";
                                        }
                                    }?>
                                    <tr id="tr_<?php echo $row['advertisement_id']; ?>">
                                        <td><?php echo $i; ?></td>
                                        <td><img  src="<?php echo base_url()."/".$row['image']; ?>" width="170px" ></td>
                                        <td><?php if(!empty($row['expiry_date'])) echo date('m/d/Y',strtotime($row['expiry_date'])); ?></td>
                                        <?php if(isset($edit_action) && !empty($edit_action)){ ?>
                                        <td>
                                            <p id="status_<?php echo $row['advertisement_id']; ?>" onclick="change_status('<?php echo $field; ?>','<?php echo $row['advertisement_id']; ?>','<?php echo $table; ?>')" class="<?php echo $class; ?>" title="" data-toggle="tooltip" data-original-title="Change Status"><?php echo $status; ?></p>
                                        </td>
                                        <?php } ?>
                                       <?php if(isset($edit_action) || isset($delete_action)){ ?>
                                        <td class="td-actions">
                                            <?php if(isset($edit_action) && !empty($edit_action)){ ?>
                                                <a id="edit_product" href="<?php echo $edit_action.'/'.$row['advertisement_id']; ?>" class="btn btn-xs btn-primary edit_product" title="" data-toggle="tooltip" data-original-title="Edit">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                             <?php } ?>
                                             <?php if(isset($delete_action) && !empty($delete_action)){ 
                                                $able_to_delete = $this->Common_model->getNumRecords('advertisement','advertisement_id');
                                                if($able_to_delete > 1){
                                                ?>
                                                <button class="btn btn-xs bg-red delete" data-toggle="tooltip" data-original-title="Delete" onclick="delete_record(<?php if(!empty($row[$field])) echo $row[$field]; ?>,'<?php echo $table; ?>','<?php echo $field; ?>');"><i class="fa fa-trash-o"></i></button>
                                            <?php } } ?>
                                        </td>
                                        <?php } ?>
                                </tr>

                                    <?php }
                                } else {
                                    echo "<tr><td colspan='7' align='center'> No Record Found</td></tr>";
                                } ?>
                            </tbody>
                            <tfoot>                     
                                <tr>
                                    <?php if(!empty($pagination)) { ?>
                                        <td colspan="2">Total Records - <?php echo $total_records;?></td>
                                        <td colspan="7" align="center">
                                            <div><?php echo $pagination; ?></div>
                                        </td>
                                    <?php }else{ ?> 
                                        <td colspan="2">Total Records - <?php if($total_records >0){echo $total_records;} else{echo '0';}?></td>
                                        <td colspan="7"></td>
                                    <?php } ?>          
                                </tr>
                            </tfoot>    
                        </table>
                    </div>          
                </div>          
            </div>
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

