<!-- Content Wrapper. Contains page content  -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $page_title;?> </h1>
        <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                <li class="<?php echo $breadcrumb['class'];?>"> 
                    <?php if(!empty($breadcrumb['link'])) { ?>
                        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                    <?php } else {
                        echo $breadcrumb['icon'].$breadcrumb['title'];
                    } ?>
                </li>
            <?php }?>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12"> 
                 <!-- flash messages-->
                <?php if ($this->session->flashdata('error')) { ?>
                    <div class="alert alert-block alert-danger fade in">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <?php echo $this->session->flashdata('error') ?>
                    </div>
                <?php } ?>
                <?php if ($this->session->flashdata('success')) { ?>
                    <div class="alert alert-block alert-success fade in">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <?php echo $this->session->flashdata('success') ?>
                </div>
                <?php } ?>
                <div class="box">
                    <div class="box-header with-border">
                        <form method="get" action="<?php echo $filter_action;?>" data-parsley-validate>
                            <div class="row box-body row">
                                <div class="form-group col-md-3">
                                    <input class="column_filter form-control" name="ticket_id" id="col11_filter" type="text" placeholder="Ticket Id" value="<?php echo $filter_ticket_id;?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <input class="column_filter form-control" name="name" id="col1_filter" type="text" placeholder="Name" value="<?php echo $filter_name;?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <input class="column_filter form-control" name="email" id="col2_filter" type="text" placeholder="Email" value="<?php echo $filter_email;?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <input class="column_filter form-control" name="subject" id="col4_filter" type="text" placeholder="Subject" value="<?php echo $filter_subject;?>">
                                </div>
                                
                                <div class="form-group col-md-3">
                                    <input class="btn btn-primary" type="submit" value="Filter">
                                    <a class="btn btn-default" href="<?php echo $filter_action;?>">Reset</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="3%">#</th>
                                    <th width="8%">Ticket Id</th>
                                    <th width="10%">Name</th>
                                    <th width="25%">Email</th>
                                    <th width="35%">Subject</th>
                                    <th width="8%">Date</th>
                                    <th width="5%">Action</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(!empty($records_results)) {   
                                    $i = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                                    foreach ($records_results as $row) { $i++; 
                                        ?>
                                        <tr id="tr_<?php echo $row['contact_id']; ?>">
                                            <td><?php echo $i; ?></td>
                                            <td><?php if(!empty($row['ticket_id'])) echo $row['ticket_id']; ?></td>
                                            <td><?php if(!empty($row['name'])) echo $row['name']; ?></td>
                                            <td><?php if(!empty($row['email'])) echo $row['email']; ?></td>
                                            <td><?php if(!empty($row['subject'])) echo $row['subject']; ?></td>
                                            <td><?php echo convertGMTToLocalTimezone($row['created']); ?></td>
                                            <td class="td-actions">
                                                <a id="reply" href="<?php echo $detail_action.'/'.$row['contact_id']; ?>" class="btn btn-xs btn-primary" title="" data-toggle="tooltip" data-original-title="View"><i class="fa fa-eye"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php }
                                } else {
                                    echo "<tr><td colspan='12' align='center'> No Record Found</td></tr>";
                                } ?>
                            </tbody>
                        
                            <tfoot>                     
                                <tr>
                                    <?php if(!empty($pagination)) { ?>
                                        <td colspan="3"  >Total Records - <?php echo $total_records;?></td>
                                        <td colspan="10">
                                            <div><?php echo $pagination; ?></div>
                                        </td>
                                    <?php }else{ ?> 
                                       <td colspan="3">Total Records - <?php if($total_records >0){echo $total_records;} else{echo '0';}?></td>
                                        <td colspan="2"></td>
                                    <?php } ?>          
                                </tr>
                            </tfoot>
                        </table>    
                    </div>          
                </div>          
            </div>
        </div>
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
function delete_report_page_list(field,id,table)
{ 
    if(id) {
         var a = confirm("Are you sure to delete this record?");
        if(a) {
             $.ajax({
            type:'POST',
            data:{ 
                id:id,
                table_name:table,
                field:field 
            },
            url: base_url+"admin/ajax/delete_record/",
            success:function(data)
            {
             location.reload(); 
            }
        });
        }else{
          return false;
        }

       
    }
}
</script>