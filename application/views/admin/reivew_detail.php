<div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> <?php echo $page_title;?> </h1>
        <ol class="breadcrumb">
            <?php foreach ($breadcrumbs as  $breadcrumb) { ?>
                <li class="<?php echo $breadcrumb['class'];?>"> 
                    <?php if(!empty($breadcrumb['link'])) { ?>
                        <a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
                    <?php } else {
                        echo $breadcrumb['icon'].$breadcrumb['title'];
                    } ?>
                </li>
            <?php }?>
        </ol>
    </section>
 
    <section class="content">
        <div class="invoice">
           <!-- info row --> 
            <div class="row invoice-info">
                <div class="col-sm-3 invoice-col">
                    <label>Event Title </label>
                    <p><?php echo !empty($details['event_title'])?ucfirst($details['event_title']):'NA'; ?></p>
                </div>
                <div class="col-sm-3 invoice-col">
                    <label>Review From  </label>
                    <p><?php echo !empty($details['sender_name'])?ucfirst($details['sender_name']):'NA'; ?></p>
                </div> 
                <div class="col-sm-3 invoice-col">
                    <label>Review To </label>
                    <p><?php echo !empty($details['receiver_name'])?ucfirst($details['receiver_name']):'NA'; ?></p>
                </div> 
                <div class="col-sm-1 invoice-col">
                    <label>Rating </label>
                    <p><?php if(!empty($details['rating']))echo $details['rating']; ?></p>
                </div> 
                <div class="col-sm-2 invoice-col">
                    <label>Review Date & Time </label>
                    <p><?php if(!empty($details['created']))echo  convertGMTToLocalTimezone($details['created'],true); ?></p>
                </div> 
            </div>  
            <BR>  
            <div class="invoice-col">
                <label>Review </label>
                <p><?php echo !empty($details['review'])?$details['review']:'NA'; ?></p>  
            </div>     
            <div class="row no-print">
              <div class="col-xs-12 text-center">    
                <?php if(isset($back_action) && !empty($back_action)){ ?>
                    <a  href="<?php echo $back_action; ?>" class="btn btn-default ">
                     Back
                    </a>
                <?php } ?>
              </div>
           </div>
        </div>
    </section>
</div>

