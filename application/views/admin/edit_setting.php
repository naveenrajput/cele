
<div class="content-wrapper">
	<section class="content-header">
			<ol class="breadcrumb">
					<?php foreach ($breadcrumbs as  $breadcrumb) { ?>
							<li class="<?php echo $breadcrumb['class'];?>"> 
									<?php if(!empty($breadcrumb['link'])) { ?>
											<a href="<?php echo $breadcrumb['link'];?>"><?php echo $breadcrumb['icon'].$breadcrumb['title'];?></a>
									<?php } else {
											echo $breadcrumb['icon'].$breadcrumb['title'];
									} ?>
							</li>
					<?php }?>
			</ol>
	</section>

		<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-lg-12">
				<?php if(isset($form_action) && !empty($form_action)){ ?>
				<form class="" id="change_password" method="POST" enctype="multipart/form-data"  action="<?php echo $form_action; ?>" role="form" data-parsley-validate>
				<?php } ?>
					<div class="col-lg-6">
						<h3><?php if(isset($page_title)) echo $page_title; ?></h3>
						<div class="box box-primary">
							<div class="box-body">
								<div class="row">
									<div class="panel-body">
										<?php if ($this->session->flashdata('error')) { ?>
											<div class="alert alert-block alert-danger fade in">
													<button data-dismiss="alert" class="close" type="button">×</button>
													<?php echo $this->session->flashdata('error') ?>
												</div>
										<?php } ?>
										<?php if ($this->session->flashdata('success')) { ?>
											<div class="alert alert-block alert-success fade in">
													<button data-dismiss="alert" class="close" type="button">×</button>
													<?php echo $this->session->flashdata('success') ?>
											</div>
										<?php } ?>
										<div id= 'notification_msg'></div>
										<div class="box-body">
											<div class="form-group">
													<label for="name" >Admin Commission (%) *</label>
													<input type="text"  maxlength="5" max="100"  class="form-control" name="commission" id="commission" value="<?php if(isset($details['commission'])) echo $details['commission']; ?>" placeholder="Commission" data-parsley-required data-parsley-required-message="Please enter commission." maxlength="4"  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
													<?php echo form_error('commission'); ?> 
											</div>
											<div class="form-group">
													<label for="name" >Cancellation Charges Before or Till Given Date (%) *</label>
													<input type="text" class="form-control" name="before_cancellation" id="before_cancellation" value="<?php if(isset($details['before_cancellation'])) echo $details['before_cancellation']; ?>" placeholder="Cancellation Charges Before or Till Given Date (%)" data-parsley-required data-parsley-required-message="Please enter before cancellation charges."  maxlength="5" max="100"  maxlength="4"  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
													<?php echo form_error('before_cancellation'); ?> 
											</div>
											<div class="form-group">
													<label for="name" >Cancellation Charges After Given Date (%) *</label>
													<input type="text" class="form-control" name="after_cancellation" id="after_cancellation" value="<?php if(isset($details['after_cancellation'])) echo $details['after_cancellation']; ?>" placeholder="Cancellation Charges After Given Date (%)" data-parsley-required data-parsley-required-message="Please enter after cancellation charges." maxlength="5" max="100"  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
													<?php echo form_error('after_cancellation'); ?> 
											</div>
										</div>
										<div class="box-footer">
											<div class="form-group">
												<div class="col-md-12 text-center">
													<?php if(isset($form_action) && !empty($form_action)){ ?>
													 <button type="submit" class="btn btn-primary">Update</button>
													<?php } ?>
													<a href="<?php if(isset($back_action))echo $back_action;?>" class="btn btn-default">Back</a> 
												</div>
											</div>
										</div>
									</div><!--panel-body-->
								</div><!--box-row-->
							</div><!--box-body-->
						</div><!-- row-->
					</div><!-- col-6-->
				</form>
			</div>
		</div>
	</section>
</div>



















