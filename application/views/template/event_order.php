<!doctype>
<html>
<head>
    <title><?php echo WEBSITE_NAME; ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,700i" rel="stylesheet">
	<style>
  body, html {font-family: 'Roboto', sans-serif;}
		@CHARSET "UTF-8";
		.page-break {
			page-break-after: always;
			page-break-inside: avoid;
			clear:both;
		}
		.page-break-before {
			page-break-before: always;
			page-break-inside: avoid;
			clear:both;
    }
    td {
    padding: 4px 4px;
}
	</style>
 </head>
<body>
<div style='overflow: auto; width: 100%; max-width: 800px;text-align: center;margin: auto;background:#f7f7f7;'>
<table width="100%" cellpadding="5" align="center">		
  <tr>
<td style="background: #f0f0f0;text-align: center;margin-bottom:12px;" > 
   <table width="100%" cellpadding="10">	
    <tr>
    <td style="text-align: left;">	
      <img src="<?php echo base_url()?>/resources/logo.png" style="margin-top:12px;width:12%;">
    </td>
    <td align="right">
      <p style="color:#8b8686;font-size: 15px;"><strong style="color:#000;margin-top: 10px;display: inline-block;">Payment Date:</strong> <?php echo convertLocalTimezoneToGMT($order['created'],$timezone)?> </p>
      <!-- <p style="color:#8b8686;font-size: 15px;"><strong style="color:#000;">Time:</strong>  <?php //echo convertLocalTimezoneToGMT($order['created'],$timezone,'',true)?></p> -->
    </td>
  </tr>
   </table>

</td>		
   </tr>
   <tr> 
    <td >
        <table width="100%" cellpadding="10">
            <tr>
               <td align="center" style="width:100%;">
                <!-- <h2 style="margin: 6px 0;"> It is a long established fact that a reader will be </h2> -->
                  </td>
               </tr>
             
         </table>
     <table width="100%" cellpadding="10" style="padding:15px 0; ">
        <tr>
            <td style="width:50%;"> 
             <label><b style="width: 112px;display: inline-block;"> Order Status:</b> <?php if(!empty($order['order_status'])){echo $order['order_status'];}else{ echo 'N/A';}?> </label>
               </td>
                <td style="width:50%;" align="right"><label><b > Event Number :</b> 
                  <span style="width: 112px;display: inline-block;"><?php if(!empty($order['event_number'])){echo $order['event_number'];}else{ echo 'N/A';}?> </span> </label>
               </td>
        </tr>
        <tr>
            <td style="width:50%;"><label><b style="width: 112px;display: inline-block;">Event Status :</b> <?php if(!empty($order['event_status'])){echo $order['event_status'];}else{ echo 'N/A';}?></label>
            </td>
            <td style="width:50%;" align="right"><label><b >Event Type: </b><span style="width: 112px;display: inline-block;"> <?php if(!empty($order['event_type'])){echo $order['event_type'];}else{ echo 'N/A';}?> </span></label>
           </td>
        </tr>
            <tr>
                <td style="width:50%;">
                    <label><b style="width: 112px;display: inline-block;">Service Name: </b> <?php if(!empty($order['service_title'])){echo $order['service_title'];}else{ echo 'N/A';}?></label>
                </td>
                <td style="width:50%;" align="right"><label><b>Order Number :</b> 
                    <span style="width: 112px;display: inline-block;"> <?php if(!empty($order['order_number'])){echo $order['order_number'];}else{ echo 'N/A';}?></span></label>
                </td>
            </tr>
            <tr>
              <td style="width:50%;">
                  <label><b style="width: 112px;display: inline-block;">Customer Name :</b><?php if(!empty($order['sender_fullname'])){echo $order['sender_fullname'];}else{ echo 'N/A';}?></label>
                </td>
               <td style="width:50%;" align="right"><label><b> Payment Status : </b>
                     <span style="width: 112px;display: inline-block;"><?php if(!empty($order['payment_status'])){echo $order['payment_status'];}else{ echo 'N/A';}?></label>
                </td>
            </tr>
                <td style="width:50%;">
                    <label><b style="width: 112px;display: inline-block;">Service Provider:</b> <?php if(!empty($order['sp_name'])){echo $order['sp_name'];}else{ echo 'N/A';}?></label>
                </td>
                <td style="width:50%;" align="right"><label><b> Event Address : </b>
                    <span style="width: 112px;display: inline-block;"> <?php if(!empty($order['event_address'])){echo $order['event_address'];}else{ echo 'N/A';}?> </span></label>
                </td>
            </tr>
            <tr>
                <td style="width:50%;"><label><b> Event Date : </b>
                   <span style="width: 112px;display: inline-block;"> 
                   
                   <?php 
                  
                   if(!empty($order['event_date'])){ echo convertGMTToLocalTimezone($order['event_date'],'','',$timezone);} else {echo 'N/A';}?>
                   <?php if(!empty($order['event_date'])){ echo convertGMTToLocalTimezone($order['event_date'],'',true,$timezone);} else {echo 'N/A';}?>
                   </span></label>
                </td>
                <td style="width:50%;" align="right"></td>
            </tr>
      </table>
     <table width="100%" cellpadding="20" style="border:1px solid #000;margin:15px 0; ">
        <tr>
             <th style="border:1px solid #000;"> Item Name </th>
             <th style="border:1px solid #000;"> Price </th>
             <th style="border:1px solid #000;"> Quantity </th>
             <th style="border:1px solid #000;"> Total Price </th>
             <th style="border:1px solid #000;"> Discount </th>
             <th style="border:1px solid #000;"> Tax </th>
             <th style="border:1px solid #000;"> Sell Price </th>
        </tr>
        <?php if(!empty($order_details)){
            $total_tax=0;
            foreach ($order_details as $key => $item) { $discount_value=0;?> 
             <tr>
                <td align="center"  style="border:1px solid #000;"> 
                    <label><?php echo ucfirst($item['item_name'])?></label>
                </td>  
                <td align="center"  style="border:1px solid #000;"> 
                    <label><?php echo ADMIN_CURRENCY.' '.number_format((float)$item['price'], 2, '.', '').'/ '.$item['qty'].' '.$item['unit']?></label> 
                </td>  
                <td align="center"  style="border:1px solid #000;"> 
                    <label><?php echo $item['customer_qty']?></label> 
                </td>  
                <td align="center"  style="border:1px solid #000;"> 
                    <label><?php echo ADMIN_CURRENCY.' '.number_format((float)$item['total_price'], 2, '.', '')?></label>  
                </td>  
                <td align="center"  style="border:1px solid #000;"> 
                    <label><?php 
                    if(!empty($item['discount'])){ 
                    $discount_value = ($item['total_price']*$item['discount'])/100 ;
                    echo ' ('.$item['discount'].'%) ' .ADMIN_CURRENCY.''.number_format((float)$discount_value, 2, '.', ''); }else{echo '0.00';}?> </label> 
                </td> 
                <td align="center"  style="border:1px solid #000;"> 
                <label><?php if(!empty($item['tax'])){ 
                    $tax_value = (($item['total_price']-$discount_value)*$item['tax'])/100;
                    $total_tax+=$tax_value;
                    echo ' ('.$item['tax'].'%) ' .ADMIN_CURRENCY.''. number_format((float)$tax_value, 2, '.', ''); }else{echo '0.00';}?> </label> 
                </td> 
                <td align="center"  width="20%" style="border:1px solid #000;"> 
                    <label><?php echo ADMIN_CURRENCY.' '.number_format((float)$item['sell_price'], 2, '.', '')?> </label> 
                </td>  
             </tr> 
        <?php } }?> 
     </table>
     <table width="100%" cellpadding="10"> 
        <?php if(!empty($order_total)){
            foreach ($order_total as $key => $value) { ?>
                
                    <tr>  
                        <td style="width:50%;"></td>
                        <td  style="width:50%;" align="right"> 
                            <label><b><span style="width: 112px;display: inline-block;"> <?php echo $value['title'];?></span></b></label>
                        </td> 
                        <?php if($value['code']=='total'){?>  
                            <td style="text-align: center;" > 
                               <label><span style="width: 112px;display: inline-block;"><b>  <?php echo ADMIN_CURRENCY.' '.number_format((float)$value['value'], 2, '.', '');?> </b> </span></label>
                            </td> 
                        <?php } else if($value['code']=='tax'){ ?>
                             <td style="text-align: center;" > 
                               <label><span style="width: 112px;display: inline-block;">  <?php echo ADMIN_CURRENCY.' '.number_format((float)$total_tax, 2, '.', '');?>  </span></label>
                            </td> 
                            <?php }else{ ?> 
                            <td style="text-align: center;" > 
                               <label><span style="width: 112px;display: inline-block;">  <?php echo ADMIN_CURRENCY.' '.number_format((float)$value['value'], 2, '.', '');?>  </span></label>
                            </td> 
                        <?php } ?>
                    </tr> 
                
        <?php } }  ?> 
    </table>
      </td>
   </tr>
</table>		
</div>
</body>
</html>
