<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Forgot Password</title>
</head>
<body>
    <table width="85%" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody>
            <tr>
                <td style="background:#DD5F27;height:40px; padding-left:15px; color:#ffffff;">
                    <strong>
                        <span style="color:#ffffff;font-family:Verdana;"><font size="4"><?php echo WEBSITE_NAME; ?></font></span>
                    </strong>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="background:#f6f6f6;padding-left:20px;padding-top:20px;padding-bottom:10px;line-height:20px;">
                    <p style="font-family:Verdana">
                        <strong><font size="2">Hello,</strong></font>
                    </p>
                    
                    <p style="font-family:Verdana">
                        <font size="2"><?php if(!empty($details)) echo $details;?></font>
                  
                    </p>
                </td>
            </tr>
            <tr>
                <td style="background:#f6f6f6;padding:20px;padding-top:0px;line-height:20px;" colspan="2" height="10" >
                    <p style="font-family:Verdana">
                        <font  size="2">Please feel free to get in touch for any other assistance. </font>
                    </p>
                    <p style="font-family:Verdana">
                        <font size="2">
                            Best Regards,<br>
                            <?php echo WEBSITE_NAME.' Team.';?> 
                        </font>
                    </p>
                </td>
            </tr>
             <tr>
                <td style="background:#c4c4c4;height:30px; padding-left:15px;">
                    <!-- <b>Note :</b> 
                    <font size="2"><?php //echo $last_line;?></font> -->
                </td>
            </tr> 
        </tbody>
    </table>
    </body>
</html>