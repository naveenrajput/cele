<!doctype>
<html>
<head>
    <title><?php echo $title;?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,700i" rel="stylesheet">
	<style>
  body, html {font-family: 'Roboto', sans-serif;}
		@CHARSET "UTF-8";
		.page-break {
			page-break-after: always;
			page-break-inside: avoid;
			clear:both;
		}
		.page-break-before {
			page-break-before: always;
			page-break-inside: avoid;
			clear:both;
		}
	</style>
 </head>
<body>
<div style='overflow: auto; width: 100%; max-width: 1920px;text-align: center; '>
<table width="100%" cellpadding="5" align="center">		
  <tr>
<td style="background: #fcfbfb;text-align: center;margin-bottom:12px;" > 
   <table  width="100%" cellpadding="10">	
    <tr>
    <td style="text-align: left;">	
      <img src="http://creative.progressivecoders.com/Celebrant/images/Group8.png" style="margin-top:12px;width:5%;">
    </td>
    <td align="right">
      <p style="color:#8b8686;font-size: 15px;"><strong style="color:#000;margin-top: 10px;display: inline-block;">Payment Date:</strong> <?php echo convertGMTToLocalTimezone($order['created'])?> </p>
      <p style="color:#8b8686;font-size: 15px;"><strong style="color:#000;">Time:</strong>  <?php echo convertGMTToLocalTimezone($order['created'],'',true)?></p>
    </td>
  </tr>
   </table>

</td>		
   </tr>
   <tr> 
    <td >
     <table width="100%"  cellpadding="10">
       <tr>
         <th width="100%" align="left">  <h3 style="color: #3354a5;border-bottom: 1px solid #ffeae3;margin-top: 12px;text-align: left;">Contact Information</h3> </th>
         </tr>
         <tr>
            <td style="width:50%;"> 
             <label><b style="width: 146px;display: inline-block;">Customer Name: </b> <?php echo ucfirst($order['sender_fullname']); ?> </label>
               </td>
                <td style="width:50%;" align="right"><label><b > Service Provider Name: </b> <span style="width: 112px;display: inline-block;"><?php echo ucfirst($order['sp_name']); ?> </span> </label>
               </td>
        </tr>
        <tr>
            <td style="width:50%;"><label><b style="width: 145px;display: inline-block;">Customer Number: </b> <?php echo $order['sender_mobile'];?></label>
                 </td>
            <td style="width:50%;" align="right"><label><b > Service Provider Number: </b><span style="width: 112px;display: inline-block;"> <?php echo $order['receiver_mobile'];?> </span></label>
            </td>
            </tr>
           <tr>
            <td style="width:50%;"> 
                <label><b style="width: 146px;display: inline-block;">Event Title: </b> <?php echo ucfirst($order['event_title']); ?> </label>
            </td>
            <td style="width:50%;" align="right"><label><b >Event Number: </b> <span style="width: 112px;display: inline-block;"><?php echo ucfirst($order['event_number']); ?> </span> </label>
            </td>
           </tr>
            <tr>

             <td style="width:50%;">

                <label><b> Event Date:  </b> <span style="width: 112px;display: inline-block;"> <?php echo convertGMTToLocalTimezone($order['event_date'],true)?> </span></label>
             </td>
              
                <td style="width:50%;" align="right"><label><b>Order Number: </b> <span style="width: 112px;display: inline-block;"> <?php echo $order['order_number'];?> </span></label>
                </td>
            </tr>
            <tr>

              <td style="width:50%;">
                <label><b>Event Address</b></label>
                <p style="margin: 6px 0;"><?php echo $order['event_address'];?></p>
                </td> 
             <td style="width:50%;" align="right"><label><b> Transaction Id:  </b> <span style="width: 112px;display: inline-block;"> <?php echo $order['transaction_id']?> </span></label>
             </td>

            </tr>
      </table>
      <table width="100%" cellpadding="10">
        <tr>
           <td style="width:100%;">
            <label><b>Note :</b></label>
            <p style="margin: 6px 0;"> <?php $order['note']?></p>
              </td>
           </tr> 
     </table>
     <table width="100%" border="1" cellpadding="20" >
            <tr>
             <th> Item Name </th>
             <th width="200px"> Price </th>
             <th > Quantity </th>
             <th> Total Price </th>
             <th> Discount </th>
             <th> Tax </th>
             <th> Sell Price </th>
             </tr> 
          <?php if(!empty($order_item)){
            foreach ($order_item as $key => $item) { ?> 
             <tr>
                  <td style="text-align: center;"> 
                  <?php echo ucfirst($item['item_name'])?>  
                  </td>  
                  <td style="text-align: center;"> 
                  <?php echo ADMIN_CURRENCY.' '.number_format((float)$item['price'], 2, '.', '').'/ '.$item['qty'].' '.$item['unit']?>  
                  </td>  
                  <td style="text-align: center;"> 
                  <?php echo $item['customer_qty']?>  
                  </td>  
                  <td style="text-align: center;"> 
                  <?php echo ADMIN_CURRENCY.' '.number_format((float)$item['total_price'], 2, '.', '')?>  
                  </td>  
                  <td style="text-align: center;"> 
                  <?php 
                  $discount_value= 0;
                  if(!empty($item['discount'])){ 
                  $discount_value = ($item['total_price']*$item['discount'])/100 ;
                  echo ' ('.$item['discount'].'%) ' .ADMIN_CURRENCY.''.number_format((float)$discount_value, 2, '.', ''); }?>  
                  </td> 
                  <td style="text-align: center;"> 
                  <?php if(!empty($item['tax'])){ 
                  $tax_value = (($item['total_price']-$discount_value)*$item['tax'])/100;

                  echo ' ('.$item['tax'].'%) ' .ADMIN_CURRENCY.''. number_format((float)$tax_value, 2, '.', ''); }?>  
                  </td> 
                  <td style="text-align: center;" width="20%"> 
                  <?php echo ADMIN_CURRENCY.' '.number_format((float)$item['sell_price'], 2, '.', '')?>  
                  </td>  
             </tr> 
           
          <?php } }?> 
     </table>

     <table width="100%" border="1" cellpadding="20"  >  
        
            <?php if($order['payment_type']=='Order'){  ?>
                <tr>  
                    <td  colspan="6"  style="text-align: right;"> 
                        <b>Total Order Amount</b>
                    </td>   
                    <td style="text-align: center;" > 
                        <?php echo ADMIN_CURRENCY.' '.number_format((float)$order_total['value'], 2, '.', '');?>
                    </td>  
                </tr>  
                <tr> 
                    <td  colspan="6" style="text-align: right;" > 
                      <b>Paid Amount</b>
                    </td>   
                    <td   width="20%" style="text-align: center;"> 
                        <?php echo ADMIN_CURRENCY.' '.number_format((float)$order['paid_amount'], 2, '.', '');?>
                    </td>  
                </tr>   
             <?php  
             }else{ ?> 
                <tr>  
                    <td  colspan="6"  style="text-align: right;"> 
                        <b>Total Order Amount</b>
                    </td>   
                    <td style="text-align: center;" > 
                        <?php echo  ADMIN_CURRENCY.' '.number_format((float)$order_total['value'], 2, '.', '');?>
                    </td>  
                </tr>  
                <?php 

                $minus = $order_total['value']-$order['paid_amount'];

                if( $minus>0 ){ ?>
                <tr>  
                    <td  colspan="6"  style="text-align: right;"> 
                        <b>Order Cancel Charge Amount</b>
                    </td>   
                    <td style="text-align: center;" > 
                        <?php echo '-'.ADMIN_CURRENCY.' '.number_format((float)($minus), 2, '.', '');?>
                    </td>  
                </tr> 
                <?php }?>

                <tr> 
                    <td  colspan="6" style="text-align: right;" > 
                      <b>Return Amount</b>
                    </td>   
                    <td   width="20%" style="text-align: center;"> 
                        <?php echo ADMIN_CURRENCY.' '.number_format((float)$order['paid_amount'], 2, '.', '');?>
                    </td>  
                </tr>  
 
            <?php  } ?>  
  

     </table>


   </td>
   </tr>
</table>		
</div>
</body>
</html>
