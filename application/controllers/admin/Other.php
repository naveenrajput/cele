<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Other extends CI_Controller {

	public function __construct() {
		parent::__construct(); 
		$this->load->model('admin/Common_model');
		$this->load->model('admin/Other_model');
		
		$this->load->helper('common_helper');
		$this->load->library('PHPExcel');
		$this->load->library("excel");
		ini_set('max_execution_time', 0);
	}

	public function index()	{
		$this->Common_model->check_login(); 
		
	}

	/* Action Log list */	
	public function action_log_list() {
		$this->Common_model->check_login();
		check_permission('69','view','yes');
		$data['title']="Action Log List | ".SITE_TITLE;
		$data['page_title']="Action Log List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Action Log List',
			'link' => ""
		);
		$page = $this->uri->segment(4) ? $this->uri->segment(4) : 0;
		
		$like=array();
		$data['filter_admin_name']='';
		if($this->input->get('admin_name')){
			$data['filter_admin_name']=$this->input->get('admin_name');
		}
		$data['filter_username']='';
		if($this->input->get('username')){
			$data['filter_username']=$this->input->get('username');
		}
		$data['filter_action_date']='';
		if($this->input->get('action_date')){
			$data['filter_action_date']=$this->input->get('action_date');
		}
		$data['filter_keyword']='';
		if($this->input->get('keyword')){
			$data['filter_keyword']=$this->input->get('keyword');
		}
		
		$data['total_records']=$this->Other_model->get_log_list(0,0);
	 	$data['records_results']=$this->Other_model->get_log_list(ADMIN_LIMIT,$page);
		$data['pagination']=$this->Common_model->paginate(site_url('admin/action_log/list'),$data['total_records']);

		
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/action_log_list');	
		$this->load->view('admin/include/footer');
	} 	
	/* Action Log list */	



	/* Offers list */
	public function offers_list() {
		$this->Common_model->check_login();
		check_permission('17','view','yes');
		$data['title']="Offers List | ".SITE_TITLE;
		$data['page_title']="Offers List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Offers  List',
			'link' => ""
		);
		$page = $this->uri->segment(4) ? $this->uri->segment(4) : 0;
		
		$like=array();
		$data['filter_title']='';
		if($this->input->get('title')){
			$data['filter_title']=$this->input->get('title');
		}
		$data['filter_code']='';
		if($this->input->get('code')){
			$data['filter_code']=$this->input->get('code');
		}
		$data['filter_date_rang']='';
		if($this->input->get('date_rang')){
			$data['filter_date_rang']=$this->input->get('date_rang');
		}
		$data['filter_type']='';
		if($this->input->get('type')){
			$data['filter_type']=$this->input->get('type');
		}

		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}
		
		$data['total_records']=$this->Other_model->getOffers(0,0);
	 	$data['records_results']=$this->Other_model->getOffers($page,ADMIN_LIMIT);


		$data['pagination']=$this->Common_model->paginate(site_url('admin/offers/list'),$data['total_records']);

		$data['undeletable_ids'] = array(); 
		if(check_permission('17','add')){$data['add_action']=site_url('admin/offers/add');}
		if(check_permission('17','edit')){$data['edit_action']=site_url('admin/offers/edit');}
		if(check_permission('17','view')){$data['view_action']=site_url('admin/offers/used_by');}
		if(check_permission('17','delete')){$data['delete_action']=site_url('admin/offers/delete');}
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/offers_list');	
		$this->load->view('admin/include/footer');
	}


	//add offers
	public function add_offers() 
	{
		check_permission('17','add','yes');
		$this->Common_model->check_login();			
		$data['title']="Add Offers | ".SITE_TITLE;
		$data['page_title']="Add Offers";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Offers List',
			'link' => site_url('admin/offers/list')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Add Offers',
			'link' => ""
		);	

		if($this->input->post()) {
			$this->form_validation->set_rules('title', 'title', 'trim|required|max_length[50]',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('code', 'code', 'trim|required|is_unique[offers.code]|max_length[20]',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('value', 'value', 'trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('timerange', 'date and time range', 'trim|required',array('required'=>'Please select %s'));
			$this->form_validation->set_rules('redemption_per_user', 'redemption_per_user', 'trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('total_redemption', 'total_redemption', 'trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');

			if($this->form_validation->run()==TRUE) 
			{
				$reserver_timerange = explode('-',$this->input->post('timerange'));
				$description=$this->input->post('description');
				$description=trim(strip_tags($description));
				$insert_data = array(
					'title'=> $this->input->post('title'),
					'code'=> $this->input->post('code'),
					'type'=> "Flat",
					'value'=> $this->input->post('value'),
					'redemption_per_user' =>  $this->input->post('redemption_per_user'),
					'total_redemption' =>  $this->input->post('total_redemption'),
				
					'description'=> $description,
					'start_date'=> date("Y-m-d H:i:s",strtotime(trim($reserver_timerange[0]))),
					'end_date'=> date("Y-m-d H:i:s",strtotime(trim($reserver_timerange[1]))),
					'created'=> date("Y-m-d H:i:s"),
					'modified'=> date("Y-m-d H:i:s")
				);
				
		 		if($last_id = $this->Common_model->addEditRecords('offers', $insert_data)) {
		 			$admin_id = $this->session->userdata('admin_id');
					$admin_username = getAdminUsername($admin_id);
					$log_msg = $admin_username ." added new offer '".$this->input->post('title')."'";
					actionLog('offers',$last_id,'Add',$log_msg,'Admin',$admin_id);
					$this->session->set_flashdata('success', 'Offers Added Successfully.');
					redirect('admin/offers/list');
				} else {
					$this->session->set_flashdata('error', 'Some error occured! Please try again.');
				}
			}
		}
		
		$data['from_action']=site_url('admin/offers/add');
		$data['back_action']=site_url('admin/offers/list');
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/add_offers');
		$this->load->view('admin/include/footer');
	}


	public function edit_offers($id) {
		check_permission('17','edit','yes');
		$this->Common_model->check_login();

		if(!$id) {
			redirect('pages/page_not_found');
		}
		$data['title']="Edit Offers | ".SITE_TITLE;
		$data['page_title']="Edit Offers ";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Offers List',
			'link' => site_url('admin/offers/list')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Edit Offers',
			'link' => ""
		);	
		
		if(!$data['offers']=$this->Common_model->getRecords('offers','*',array('offer_id'=>$id),'',true)) {
			redirect('pages/page_not_found');
		}

		if($this->input->post()) {
			$this->form_validation->set_rules('title', 'title', 'trim|required|max_length[50]',array('required'=>'Please enter %s'));
			if($data['offers']['code']==trim($this->input->post('code'))) {
				$this->form_validation->set_rules('code', 'code', 'trim|required|max_length[12]',array('required'=>'Please enter %s'));
			} else {
				$this->form_validation->set_rules('code', 'code', 'trim|required|is_unique[offers.code]|max_length[12]',array('required'=>'Please enter %s'));
			}
			$this->form_validation->set_rules('redemption_per_user', 'redemption_per_user', 'trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('total_redemption', 'total_redemption', 'trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');

			if($this->form_validation->run()==TRUE) 
			{
				$description=$this->input->post('description');
				$description=trim(strip_tags($description));
				$reserver_timerange = explode('-',$this->input->post('timerange'));
				$update_data = array(
					'title'=> $this->input->post('title'),
					'code'=> $this->input->post('code'),
					'type'=> "Flat",
					'value'=> $this->input->post('value'),
					'redemption_per_user' =>  $this->input->post('redemption_per_user'),
					'total_redemption' =>  $this->input->post('total_redemption'),
					'description'=> $description,
					'start_date'=> date("Y-m-d H:i:s",strtotime(trim($reserver_timerange[0]))),
					'end_date'=> date("Y-m-d H:i:s",strtotime(trim($reserver_timerange[1]))),
					'modified'=> date("Y-m-d H:i:s")
				);
	 		 	$this->Common_model->addEditRecords('offers', $update_data,array('offer_id'=>$id));
				$admin_id = $this->session->userdata('admin_id');
				$admin_username = getAdminUsername($admin_id);
				$log_msg = $admin_username ." updated offers '". $this->input->post('title')."'";
				actionLog('offers',$id,'update',$log_msg,'Admin',$admin_id);
				$this->session->set_flashdata('success', 'Offers Updated Successfully.');
			}
		}

		$data['from_action']=site_url('admin/offers/edit/'.$id);
		$data['back_action']=site_url('admin/offers/list');

		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/edit_offers');
		$this->load->view('admin/include/footer');
	}

	public function offer_used_by($offer_id){
		$this->Common_model->check_login();
		check_permission('3','view','yes');
		$data['title']="Offer Used By | ".SITE_TITLE;
		$data['page_title']="Offer Used By";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Offer List',
			'link' => site_url('admin/offers/list')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Offer Used By',
			'link' => ""
		);

		$page = $this->uri->segment(5) ? $this->uri->segment(5) : 0;
		
		$like=array();
		
		
		$data['filter_name']='';
		if($this->input->get('name')){
			$data['filter_name']=$this->input->get('name');
		}
		$data['filter_email']='';
		if($this->input->get('email')){
			$data['filter_email']=$this->input->get('email');
		}
		
		$data['filter_date']='';
		if($this->input->get('date')){
			$data['filter_date']=$this->input->get('date');
		}
	
  
  
		/************************/

		$data['total_records']=$this->Other_model->get_offer_used(0,0,$offer_id);
	 	$data['records_results']=$this->Other_model->get_offer_used(ADMIN_LIMIT,$page,$offer_id);
	 	//print_r($this->db->last_query());die;

		$data['pagination']=$this->Common_model->paginate(site_url('admin/offers/used_by/'.$offer_id),$data['total_records'],'seg5');
        $data['reset_link']='admin/offers/used_by/'.$offer_id;
		
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/offer_used_by_list');	
		$this->load->view('admin/include/footer');
	}




	public function rolekey_exists($key,$field) {
		$param=explode('-', $field);
		$table=$param[0];
		$col_name=$param[1];
		$type=$param[2];
		if($type=='add') {
			if ($table=='sections') {
				$where_cond = array($col_name=>$key);
			}else{
				$where_cond = array($col_name=>$key,'is_deleted'=>0);
			}
       		if($this->Common_model->getRecords($table, $col_name, $where_cond, '', true)) {
   			 	$this->form_validation->set_message(__FUNCTION__, 'This %s is already exist.');
       			return false;
       		} else {
       			return true;
       		}
		} else {
			$id_col=$param[3];
			$id_col_val=$param[4];
			if ($table=='sections') {
				$where_cond = array("$id_col!=" =>$id_col_val,$col_name=>$key);
			}else{
				$where_cond = array("$id_col!=" =>$id_col_val,$col_name=>$key,'is_deleted'=>0);
			}
			if($this->Common_model->getRecords($table, $col_name, $where_cond, '', true)) {
				$this->form_validation->set_message(__FUNCTION__, 'This %s is already exist.');
				return false;
			} else {
       			return true;
       		}
		}
	}

	public function section_list() {
		$this->Common_model->check_login();
		$data['title']="Sections List | ".SITE_TITLE;
		$data['page_title']="Sections List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Sections List',
			'link' => ""
		);
		$page = $this->uri->segment(4) ? $this->uri->segment(4) : 0;
		

		$data['total_records']=$this->Other_model->getSections(0,0);
	 	$data['records_results']=$this->Other_model->getSections($page,ADMIN_LIMIT);

		$data['pagination']=$this->Common_model->paginate(site_url('admin/section/list'),$data['total_records']);

		$data['add_action']		=	site_url('admin/section/add');
		$data['edit_action']	=	'';
		$data['delete_action']	=	'';
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/section_list');	
		$this->load->view('admin/include/footer');
	} 
	
	public function add_section() {
		$this->Common_model->check_login();
		$data['title']="Add Section | ".SITE_TITLE;
		$data['page_title']="Add Section";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Section List',
			'link' => site_url('admin/section/list')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Add Section',
			'link' => ""
		);	
		if($this->input->post()) {
			$this->form_validation->set_rules('section_name', 'Section Name','callback_rolekey_exists[sections-name-add]', 'trim|required|max_length[150]',array('required'=>'Please enter %s'));
			$this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');

			if($this->form_validation->run()==TRUE) 
			{
				$insert_data = array(
					'name'=> $this->input->post('section_name')
				);
						
		 		if($last_id = $this->Common_model->addEditRecords('sections', $insert_data)) {
		 			$admin_id = $this->session->userdata('admin_id');
					$admin_username = getAdminUsername($admin_id);
					$log_msg = $admin_username ." added new section '".$this->input->post('section_name')."'";
					actionLog('sections',$last_id,'Add',$log_msg,'Admin',$admin_id);

					if($roles=$this->Common_model->getRecords('roles','*')) {
						foreach ($roles as $role) {

							$insert_role = array(
								'role_id'=> $role['role_id'],
								'section_id'=> $last_id,
								'add'=> 0,
								'edit'=> 0,
								'delete'=> 0,
								'view'=> 0,
								'created'=> date("Y-m-d H:i:s")
							);
					 		$this->Common_model->addEditRecords('role_permissions', $insert_role);
						}
					}

					$this->session->set_flashdata('success', 'Section Added Successfully.');
					redirect('admin/section/list');
				} else {
					$this->session->set_flashdata('error', 'Some error occured! Please try again.');
				}
			}
		}
		
		$data['from_action']=site_url('admin/section/add');
		$data['back_action']=site_url('admin/section/list');
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/add_section');
		$this->load->view('admin/include/footer');
	}	


    public function check_sectionname()
    {
       	if(isset($_POST['section_name'])) {
       		if($this->Common_model->getRecords('sections', 'id', array('name'=>$_POST['section_name']), '', true)) {
       			echo 1;die;
       		} else {
       			echo 0;die;
       		}
       	}
    }


	/* Reviews list */
	public function reviews_list() {
		$this->Common_model->check_login();
		check_permission('51','view','yes');
		$data['title']="Rating List | ".SITE_TITLE;
		$data['page_title']="Rating List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Reviews  List',
			'link' => ""
		);
		$page = $this->uri->segment(4) ? $this->uri->segment(4) : 0;
		
		$individual 	= $this->input->get('individual');
		$freelancer = $this->input->get('freelancer');
		
	 

		$data['type'] = 1;
		$data['reset_action']=site_url('admin/rating/list');
		if(check_permission('51','edit')){$data['edit_action']=site_url('admin/rating/edit');}
		if(check_permission('51','delete')){$data['delete_action']=site_url('admin/rating/delete');}

		/*if (isset($individual) && !empty($individual)) {
			if($this->input->get('sender_id')){
				$data['reset_action']=site_url('admin/individual/review_list?individual=yes&sender_id='.$this->input->get('sender_id'));
			}
			if($this->input->get('receiver_id')){
				$data['reset_action']=site_url('admin/individual/review_list?individual=yes&receiver_id='.$this->input->get('receiver_id'));
			}
			$data['type'] = 2;
			$data['filter_individual']=$individual;
			$data['edit_action']='';
			$data['delete_action']='';
		}
		if (isset($freelancer) && !empty($freelancer)) {
			if($this->input->get('sender_id')){
				$data['reset_action']=site_url('admin/freelancer/review_list?freelancer=yes&sender_id='.$this->input->get('sender_id'));
			}
			if($this->input->get('receiver_id')){
				$data['reset_action']=site_url('admin/freelancer/review_list?freelancer=yes&receiver_id='.$this->input->get('receiver_id'));
			}			
			$data['type'] = 3;
			$data['filter_freelancer']=$freelancer;
			$data['edit_action']='';
			$data['delete_action']='';
		}*/


		$data['is_indi'] = '';

		$data['filter_event_number']='';
		if($this->input->get('event_number')){
			$data['filter_event_number']=$this->input->get('event_number');
		}
		$data['filter_event_title']='';
		if($this->input->get('event_title')){
			$data['filter_event_title']=$this->input->get('event_title');
		}

		$data['filter_sender_name']='';
		if($this->input->get('sender_name')){
			$data['filter_sender_name']=$this->input->get('sender_name');
		}
		$data['filter_receiver_name']='';
		if($this->input->get('receiver_name')){
			$data['filter_receiver_name']=$this->input->get('receiver_name');
		}
		$data['filter_job_id']='';
		if($this->input->get('job_id')){
			$data['filter_job_id']=$this->input->get('job_id');
		}
		$data['filter_sender_id']='';
		if($this->input->get('sender_id')){
			$data['filter_sender_id']=$this->input->get('sender_id');
			$data['is_indi'] = 'sender';
		}
		$data['filter_receiver_id']='';
		if($this->input->get('receiver_id')){
			$data['filter_receiver_id']=$this->input->get('receiver_id');
			$data['is_indi'] = 'receiver';
		}
		$data['filter_date_rang']='';
		if($this->input->get('date_rang')){
			$data['filter_date_rang']=$this->input->get('date_rang');
		}
		$data['filter_rating']='';
		if($this->input->get('rating')){
			$data['filter_rating']=$this->input->get('rating');
		}
		$data['filter_type']='';
		if($this->input->get('type')){
			$data['filter_type']=$this->input->get('type');
		}

		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}
		
		$data['total_records']=$this->Other_model->getReviews(0,0);
	 	$data['records_results']=$this->Other_model->getReviews($page,ADMIN_LIMIT);


		$data['pagination']=$this->Common_model->paginate(site_url('admin/rating/list'),$data['total_records']);

		$data['undeletable_ids'] = array(); 
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/reviews_list');	
		$this->load->view('admin/include/footer');
	}


	public function view_reviews($id) 
	{
		$this->Common_model->check_login();
	    check_permission('51','view','yes');
		$data['title']="Review Detail | ".SITE_TITLE;
		$data['page_title']="Review Detail";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Review List',
			'link' => site_url('admin/reviews/list')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Detail',
			'link' => ""
		);	
		if(!$data['details'] = $this->Other_model->getReviewsdetail($id) ){
			redirect('pages/page_not_found');
		}
		 
		 
		$data['back_action']=site_url('admin/reviews/list');

		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/reivew_detail');
		$this->load->view('admin/include/footer');
	}



	//add reviews
	/*public function add_reviews() 
	{
		check_permission('18','add','yes');
		$this->Common_model->check_login();			
		$data['title']="Add Reviews | ".SITE_TITLE;
		$data['page_title']="Add Reviews";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Review List',
			'link' => site_url('admin/reviews/list')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Add Reviews',
			'link' => ""
		);	

		if($this->input->post()) {
			$this->form_validation->set_rules('title', 'title', 'trim|required|max_length[50]',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('code', 'code', 'trim|required|is_unique[reviews.code]|max_length[20]',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('value', 'value', 'trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('timerange', 'date and time range', 'trim|required',array('required'=>'Please select %s'));
			$this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');

			if($this->form_validation->run()==TRUE) 
			{
				$reserver_timerange = explode('-',$this->input->post('timerange'));
				$description=$this->input->post('description');
				$description=trim(strip_tags($description));
				$insert_data = array(
					'title'=> $this->input->post('title'),
					'code'=> $this->input->post('code'),
					'type'=> $this->input->post('type'),
					'value'=> $this->input->post('value'),
					'min_amount'=> $this->input->post('min_amount'),
					'description'=> $description,
					'start_date'=> date("Y-m-d H:i:s",strtotime(trim($reserver_timerange[0]))),
					'end_date'=> date("Y-m-d H:i:s",strtotime(trim($reserver_timerange[1]))),
					'created'=> date("Y-m-d H:i:s"),
					'modified'=> date("Y-m-d H:i:s")
				);
				
		 		if($last_id = $this->Common_model->addEditRecords('reviews', $insert_data)) {
		 			$admin_id = $this->session->userdata('admin_id');
					$admin_username = getAdminUsername($admin_id);
					$log_msg = $admin_username ." added new offer '".$this->input->post('title')."'";
					actionLog('reviews',$last_id,'Add',$log_msg,'Admin',$admin_id);
					$this->session->set_flashdata('success', 'Reviews Added Successfully.');
					redirect('admin/reviews/list');
				} else {
					$this->session->set_flashdata('error', 'Some error occured! Please try again.');
				}
			}
		}
		
		$data['from_action']=site_url('admin/reviews/add');
		$data['back_action']=site_url('admin/reviews/list');
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/add_reviews');
		$this->load->view('admin/include/footer');
	}


	public function edit_reviews($id) {
		check_permission('18','edit','yes');
		$this->Common_model->check_login();

		if(!$id) {
			redirect('pages/page_not_found');
		}
		$data['title']="Edit Reviews | ".SITE_TITLE;
		$data['page_title']="Edit Reviews ";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Review List',
			'link' => site_url('admin/reviews/list')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Edit Reviews',
			'link' => ""
		);	
		
		if(!$data['reviews']=$this->Other_model->getReviewsDetail($id)) {
			redirect('pages/page_not_found');
		}

		if($this->input->post()) {
			$this->form_validation->set_rules('review', 'review', 'trim|required|max_length[255]',array('required'=>'Please enter %s'));
			$this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');

			if($this->form_validation->run()==TRUE) 
			{
				$is_private=$this->input->post('is_private');
				if ($is_private==1) {
					$update_data['private_review'] = $this->input->post('private_review');
					$update_data['is_private'] = 1;
				} else {
					$update_data['private_review'] = '';
					$update_data['is_private'] = 0;
				}

				$update_data['review'] = $this->input->post('review');
				$update_data['rating'] = $this->input->post('rating');
				$update_data['modified'] = date("Y-m-d H:i:s");

	 		 	$this->Common_model->addEditRecords('reviews', $update_data,array('id'=>$id));
				$admin_id = $this->session->userdata('admin_id');
				$admin_username = getAdminUsername($admin_id);
				$log_msg = $admin_username ." updated reviews for job '". $data['reviews']['job_title']."'";
				actionLog('reviews',$id,'Edit',$log_msg,'Admin',$admin_id);
				$this->session->set_flashdata('success', 'Reviews  Updated Successfully.');
				redirect('admin/reviews/edit/'.$id);
			}
		}

		$data['from_action']=site_url('admin/reviews/edit/'.$id);
		$data['back_action']=site_url('admin/reviews/list');

		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/edit_reviews');
		$this->load->view('admin/include/footer');
	}*/



 
	public function settings() 
	{
		$this->Common_model->check_login();
		// check_permission('14','edit','yes');
		$data['title']="Settings | ".SITE_TITLE;
		$data['page_title']="Settings";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Settings',
			'link' => ""
		);	
		
		if(!$data['settings']=$this->Common_model->getRecords('settings','*','','',true)) {
			redirect('pages/page_not_found');
		}
		// echo "<pre>"; print_r($data); exit;
	
		if($this->input->post()) {
			$admin_commission = $this->input->post('admin_commission');  
			$distance = $this->input->post('distance');  
			$elite_employee_rating = $this->input->post('elite_employee_rating');  
			$bad_rating = $this->input->post('bad_rating');  

			$referal_amount = $this->input->post('referal_amount');  
			$code = $this->input->post('code');

				$update_data = array(
					'admin_commission'=> $admin_commission,
					'distance'=> $distance,
					'elite_employee_rating'=> $elite_employee_rating,
					'bad_rating'=> $bad_rating,
					'referal_amount' => $referal_amount,
					'referal_amount_pay' => $code,
					'modified'=> date("Y-m-d H:i:s")
				);
				//echo "<pre>"; print_r($id); exit;	
		 		if(!$this->Common_model->addEditRecords('settings', $update_data,array('id'=>1))) {

					$this->session->set_flashdata('error', 'Some error occured! Please try again.');
				} else { 
						// echo "<pre>";print_r($this->db->last_query());die;
					$this->session->set_flashdata('success', 'Settings Updated Successfully.');
					redirect('admin/settings');
				}
		 
		}
	 	$data['form_action']=site_url('admin/settings');
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/setting');
		$this->load->view('admin/include/footer');

	}

	public function pay_now(){
     
       
        $this->load->view('frontend/payment_page');
       
    }



	public function make_payment(){
       
        echo $token = $_POST['stripeToken'];
        $job_id = 20;
        $user_id = 1;
        $amount = $_POST['amount'];

         $userdata = $this->Common_model->getRecords('users','email,first_name,last_name',array('user_id'=>$user_id),'',true);
        $name = $userdata['first_name']." ".$userdata['last_name'] ;
        $email = $userdata['email'];
        //check whether stripe token is not empty
        if(!empty($_POST['stripeToken']))
        {
           
            require_once APPPATH."third_party/stripe/init.php";
            
            //set api key
             $stripe = array(
              "secret_key"      => "sk_test_Tr5hmR3bgqUQFwYHF9aQU9wr",
              "publishable_key" => "pk_test_hyaxWO6SNhf4WTL6ZIguvrGD"
            );
            //set api key
          /*  $stripe = array(
              "secret_key"      => "sk_test_Diev1y9VHQBtMQVX2CKMAbAD00yQBFcARB",
              "publishable_key" => "pk_test_uy9gGoLmm5AYrT094QjHB9vb00xGoZvoKa"
            );*/
            
            \Stripe\Stripe::setApiKey($stripe['secret_key']);

                 //add customer to stripe
                $token  = $_POST['stripeToken'];
                $customer = \Stripe\Customer::create(array(
                'name' => $name,
                'email' => $email,
                'source'  => $token
                ));
                $customer_id = $customer->id;
			  //item information

	            $itemName = "Stripe Donation";
	            $itemNumber = "PS123456";
	            $itemPrice = '1000000';
	            $currency = "usd";
	            $orderID = "SKA92712382139";
	            
	            //charge a credit or a debit card
	            $charge = \Stripe\Charge::create(array(
	                'customer' => $customer_id,
	                'amount'   => $itemPrice,
	                'currency' => $currency,
	                'description' => $itemNumber,
	                'metadata' => array(
	                'item_id' => $itemNumber
	                )
	            ));
	            
	            //retrieve charge details
	            $chargeJson = $charge->jsonSerialize();
	           
	            if($chargeJson['amount_refunded'] == 0 && empty($chargeJson['failure_code']) && $chargeJson['paid'] == 1 && $chargeJson['captured'] == 1)
	            {

	            $admin_commission_percentage = 4;
	             
	            $payment_gateway_fee = $itemPrice*2/100; 
	            $admin_commission_amount = $itemPrice*$admin_commission_percentage/100; 

	            $total_payable_amount = $itemPrice - ($payment_gateway_fee + $admin_commission_amount);

                 
            $transaction_data = array(
                'job_id' => $job_id,
                'user_id' => $user_id,
                'transaction_id' => $chargeJson['balance_transaction'],
                'total_amount' => $itemPrice,
                'admin_commission_amount' => $admin_commission_amount,
                'admin_commission_percentage' => $admin_commission_percentage,
                'total_payable_amount' => $total_payable_amount,
                'status' => "Completed",
                'payment_gateway_fee' => $payment_gateway_fee,
                'payment_type' => "Online",
                'created' => date('Y-m-d H:i:s'),
            );
            if($this->Common_model->addEditRecords('transaction_history', $transaction_data)){
               
                echo "Payment Successfully Done";

            }else{
                echo "Payment Process Failed";
              
            }
        }else{
        	 echo "Payment Process Failed";
        }
    }else{
    	echo "Invelid Token";
    }
 	
}




function teststripe()
{

			require_once APPPATH."third_party/stripe/init.php";
            
            //set api key
            $stripe = array(
              "secret_key"      => "sk_test_Diev1y9VHQBtMQVX2CKMAbAD00yQBFcARB",
              "publishable_key" => "pk_test_uy9gGoLmm5AYrT094QjHB9vb00xGoZvoKa"
            );


	\Stripe\Stripe::setApiKey('sk_test_4eC39HqLyjWDarjtT1zdp7dc');
	$customer = \Stripe\Customer::create(array(
				  "description" => "Customer for payout"
				));

				$customer_id =  $customer->id;

				$customer = \Stripe\Customer::retrieve($customer_id);


				$bank_data = \Stripe\Token::create(array(
				  "bank_account" => array(
				    "country" => "US",
				    "currency" => "usd",
				    "account_holder_name" => "Charlotte Thomas",
				    "account_holder_type" => "individual",
				    "routing_number" => "110000000",
				    "account_number" => "000123456789"
				  )
				));

				$bank_token = $bank_data->id;

				$bank_account = $customer->sources->create(array("source" => $bank_token));


				$payout_data = \Stripe\Payout::create(array(
				  "amount" => 100,
				  "currency" => "usd",
				));

				echo "<pre>";
				print_r($payout_data);
				die;

}



	/* Offers list */
	public function subscription_list() {
		$this->Common_model->check_login();
		check_permission('53','view','yes');
		$data['title']="Subscription List | ".SITE_TITLE;
		$data['page_title']="Subscription List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Subscription List',
			'link' => ""
		);
		$page = $this->uri->segment(4) ? $this->uri->segment(4) : 0;
		
		$like=array();
		
		/*$data['filter_penalty_type']='';
		if($this->input->get('penalty_type')){
			$data['filter_penalty_type']=$this->input->get('penalty_type');
		}*/
		$data['filter_title']='';
		if($this->input->get('title')){
			$data['filter_title']=$this->input->get('title');
		}
	/*	$data['filter_amount_type']='';
		if($this->input->get('amount_type')){
			$data['filter_amount_type']=$this->input->get('amount_type');
		}*/
		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}

		
		
		$data['total_records']=$this->Other_model->getSubscription(0,0);
	 	$data['records_results']=$this->Other_model->getSubscription($page,ADMIN_LIMIT);
	 	

		$data['pagination']=$this->Common_model->paginate(site_url('admin/subscription/list'),$data['total_records']);

		$data['undeletable_ids'] = array(); 
		if(check_permission('53','add')){$data['add_action']=site_url('admin/subscription/add');}
		if(check_permission('53','edit')){$data['edit_action']=site_url('admin/subscription/edit');}
		if(check_permission('53','delete')){$data['delete_action']=site_url('admin/subscription/delete');}
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/subscription_list');	
		$this->load->view('admin/include/footer');
	}

	//add offers
	public function add_subscription() 
	{
		check_permission('53','add','yes');
		$this->Common_model->check_login();			
		$data['title']="Add Subscription | ".SITE_TITLE;
		$data['page_title']="Add Subscription";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Subscription List',
			'link' => site_url('admin/subscription/list')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Add Subscription',
			'link' => ""
		);	
		
		if($this->input->post()) {
			// echo "<pre>";print_r($_POST);die;
			$this->form_validation->set_rules('amount', 'amount', 'trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('title', 'title', 'trim|required',array('required'=>'Please enter %s'));
		
			$this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');

			if($this->form_validation->run()==TRUE) 
			{	

				$subscriptioninfo=$this->Common_model->getRecords('subscriptions','max(sort_order) as sort_order','','id desc',true);
				
				$is_unlimted = $this->input->post('is_unlimited');
				if($is_unlimted==1){
					$orders='Unlimited';
				}else{
					$orders = $this->input->post('orders') ;
				} 
				$description=$this->input->post('description');
				$description=trim(strip_tags($description));
			  
				$title=$this->input->post('title');
				$title=trim(strip_tags($title));
				$time_type = $this->input->post('time_type');
				$time_value = $this->input->post('time_value');

				if($time_type == "Day"){  
					$total_days	 = $time_value;

				}elseif($time_type == "Month"){

					$total_days	 = $time_value*30;

				}elseif($time_type == "Year"){

					$total_days	 = $time_value*365;
				}


				$insert_data = array( 
					'time_type'=> $this->input->post('time_type'),
					'time_value'=> $this->input->post('time_value'), 
					'total_days' => $total_days,
					'amount'=> $this->input->post('amount'),
					'title'=> $title,
					'description'=> $description,
					'orders'=> $orders,
					'status'=> 'Active',
					'sort_order'=> $subscriptioninfo['sort_order']+1,
				    'created'=> date("Y-m-d H:i:s"),
				);
				
		 		if($last_id = $this->Common_model->addEditRecords('subscriptions', $insert_data)) {
		 			$admin_id = $this->session->userdata('admin_id');
					$admin_username = getAdminUsername($admin_id);
					$log_msg = $admin_username ." has created a new subscription '".$title."'";
					actionLog('subscription',$last_id,'Add',$log_msg,'Admin',$admin_id);
					$this->session->set_flashdata('success', 'Subscription Added Successfully.');
					redirect('admin/subscription/list');
				} else {
					$this->session->set_flashdata('error', 'Some error occured! Please try again.');
					redirect('admin/subscription/list');
				}
			}
		}
		
		$data['from_action']=site_url('admin/subscription/add');
		$data['back_action']=site_url('admin/subscription/list');
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/add_subscription');
		$this->load->view('admin/include/footer');
	}

	public function edit_subscription($id) {
		check_permission('53','edit','yes');
		$this->Common_model->check_login();

		if(!$id) {
			redirect('pages/page_not_found');
		}
		$data['title']="Edit Subscription | ".SITE_TITLE;
		$data['page_title']="Edit Subscription ";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Subscription List',
			'link' => site_url('admin/subscription/list')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Edit Subscription',
			'link' => ""
		);	
		
		if(!$data['subscription']=$this->Common_model->getRecords('subscriptions','*',array('id'=>$id),'',true)) {
			redirect('pages/page_not_found');
		}

		if($this->input->post()) {
			$this->form_validation->set_rules('amount', 'amount', 'trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('title', 'title', 'trim|required',array('required'=>'Please enter %s'));
			
			
			$this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');

			if($this->form_validation->run()==TRUE) 
			{
				
				$is_unlimted = $this->input->post('is_unlimited');
				if($is_unlimted==1){
					$orders='Unlimited';
				}else{
					$orders = $this->input->post('orders') ;
				} 
				$description=$this->input->post('description');
				$description=trim(strip_tags($description));
			  
				$title=$this->input->post('title');
				$title=trim(strip_tags($title));
				$time_type = $this->input->post('time_type');
				$time_value = $this->input->post('time_value');

				if($time_type == "Day"){  
					$total_days	 = $time_value;

				}elseif($time_type == "Month"){

					$total_days	 = $time_value*30;

				}elseif($time_type == "Year"){

					$total_days	 = $time_value*365;
				}
 

				$update_data = array(
					// 'time_type'=> $this->input->post('time_type'),
					// 'time_value'=> $this->input->post('time_value'), 
					'total_days' => $total_days,
					'amount'=> $this->input->post('amount'),
					'title'=> $title,
					'description'=> $description,
					'orders'=> $orders, 
				    'modified'=> date("Y-m-d H:i:s"),
					
				);
				
		 		if($this->Common_model->addEditRecords('subscriptions', $update_data,array('id'=>$id))) {
		 			$admin_id = $this->session->userdata('admin_id');
					$admin_username = getAdminUsername($admin_id);
					$log_msg = $admin_username ." updated subscription plan '".$title."'";
					actionLog('Subscription',$id,'Update',$log_msg,'Admin',$admin_id);
					$this->session->set_flashdata('success', 'Subscription Update Successfully.');
					redirect('admin/subscription/list');
				} else {
					$this->session->set_flashdata('error', 'Some error occured! Please try again.');
				}
			}
		}

		$data['from_action']=site_url('admin/subscription/edit/'.$id);
		$data['back_action']=site_url('admin/subscription/list');

		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/edit_subscription');
		$this->load->view('admin/include/footer');
	}

	public function wallet_history(){
		$this->Common_model->check_login();
		check_permission('22','view','yes');
		$data['title']="Admin Wallet History | ".SITE_TITLE;
		$data['page_title']="Admin Wallet History";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Admin Wallet History',
			'link' => ""
		);
		$page = $this->uri->segment(4) ? $this->uri->segment(4) : 0;
		
		$like=array();
		
		
		$data['filter_pay_from']='';
		if($this->input->get('pay_from')){
			$data['filter_pay_from']=$this->input->get('pay_from');
		}
		$data['filter_pay_to']='';
		if($this->input->get('pay_to')){
			$data['filter_pay_to']=$this->input->get('pay_to');
		}

		$data['filter_amount']='';
		if($this->input->get('amount')){
			$data['filter_amount']=$this->input->get('amount');
		}

		$data['filter_date']='';
		if($this->input->get('date')){
			$data['filter_date']=$this->input->get('date');
		}

		$data['filter_transaction_type']='';
		if($this->input->get('transaction_type')){
			$data['filter_transaction_type']=$this->input->get('transaction_type');
		}

		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}


		$data['total_records']=$this->Other_model->getWalletHistory(0,0);
	 	$data['records_results']=$this->Other_model->getWalletHistory($page,ADMIN_LIMIT);
	 	

		$data['pagination']=$this->Common_model->paginate(site_url('admin/wallet/history'),$data['total_records']);
		//print_r($this->db->last_query());die;

		if(check_permission('22','edit')){$data['edit_action']=1;}

		$data['from_action']=site_url('admin/wallet/history');
		$data['back_action']=site_url('admin/wallet/history');

	
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/wallet_history');	
		$this->load->view('admin/include/footer');
	}

	public function withdraw_request() {
		$this->Common_model->check_login();
		check_permission('22','view','yes');
		$data['title']="Withdraw Request| ".SITE_TITLE;
		$data['page_title']="Withdraw Request";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Withdraw Request',
			'link' => ""
		);
		$page = $this->uri->segment(4) ? $this->uri->segment(4) : 0;
		
		$like=array();

		$data['filter_name']='';
		if($this->input->get('name')){
			$data['filter_name']=$this->input->get('name');
		}
		$data['filter_user_type']='';
		if($this->input->get('user_type')){
			$data['filter_user_type']=$this->input->get('user_type');
		}
		
		$data['filter_amount']='';
		if($this->input->get('amount')){
			$data['filter_amount']=$this->input->get('amount');
		}
		$data['filter_transaction_id']='';
		if($this->input->get('transaction_id')){
			$data['filter_transaction_id']=$this->input->get('transaction_id');
		}


		$data['filter_date']='';
		if($this->input->get('date')){
			$data['filter_date']=$this->input->get('date');
		}
		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}
		$data['filter_user_id']='';
		if($this->input->get('user_id')){
			$data['filter_user_id']=$this->input->get('user_id');
		}

		$data['total_records']=$this->Other_model->getWithdrawRequest(0,0);
	 	$data['records_results']=$this->Other_model->getWithdrawRequest($page,ADMIN_LIMIT);
	 	
		$data['pagination']=$this->Common_model->paginate(site_url('admin/withdraw/request'),$data['total_records']);

		$data['undeletable_ids'] = array(); 
		
		if(check_permission('23','edit')){$data['edit_action']=1;}

		$individual = $this->input->get('individual');
		$freelancer = $this->input->get('freelancer');
		
		$data['filter_individual']='';
		$data['filter_freelancer']='';

		$data['type'] = 1;
		$data['reset_action']=site_url('admin/withdraw/request');
		
		if (isset($individual) && !empty($individual)) {
			
			if($this->input->get('user_id')){
				$data['reset_action']=site_url('admin/individual/withdraw/list?individual=yes&user_id='.$this->input->get('user_id'));
			}
			$data['type'] = 2;
			$data['filter_individual']=$individual;
			$data['edit_action']='1';
			$data['delete_action']='';
		}
		if (isset($freelancer) && !empty($freelancer)) {
		
			if($this->input->get('user_id')){
				$data['reset_action']=site_url('admin/freelancer/withdraw/list?freelancer=yes&user_id='.$this->input->get('user_id'));
			}			
			$data['type'] = 3;
			$data['filter_freelancer']=$freelancer;
			$data['edit_action']='1';
			$data['delete_action']='';
		}
 
		
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/withdraw_request');	
		$this->load->view('admin/include/footer');
	}

	public function reports(){
		$this->Common_model->check_login();
		check_permission('22','view','yes');
		$data['title']=" Reports| ".SITE_TITLE;
		$data['page_title']="Reports ";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Reports ',
			'link' => ""
		);
	
		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
			$data['status']=$this->input->get('status');
		}

		if(empty($data['status'])){
			$data['filter_date']='';
			if($this->input->get('date')){
				$data['filter_date']=$this->input->get('date');
				$date_range=$this->input->get('date');
				$dates_range=explode(' - ',$date_range);
				$daysLeft = abs(strtotime($dates_range[0]) - strtotime($dates_range[1]));
				$days = $daysLeft/(60 * 60 * 24)+1;
				if($days > 30){
					$this->session->set_flashdata('error', 'Date range is maximum 30 days');
					redirect('admin/reports');
				}
	      	}
		}
		

		if(!empty($data['status'])){
			$date = getRangeDate($data['status']);
			$start_date = $date['start_date'];
			$end_date = $date['end_date'];
		}else{
			$start_date = $dates_range[0];
			$end_date = $dates_range[1];
		}
		
		$data['reports']=$this->Other_model->getReports($start_date,$end_date,$data['status']);
		$data['total_amount']=$this->Other_model->getTotalAmount($start_date,$end_date);

		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/reports');	
		$this->load->view('admin/include/footer');
	
	}

	public function export_reports(){

		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
			$data['status']=$this->input->get('status');
		}

		if(empty($data['status'])){
			$data['filter_date']='';
			if($this->input->get('date')){
				$data['filter_date']=$this->input->get('date');
				$date_range=$this->input->get('date');
				$dates_range=explode(' - ',$date_range);
				$daysLeft = abs(strtotime($dates_range[0]) - strtotime($dates_range[1]));
				$days = $daysLeft/(60 * 60 * 24)+1;
				if($days > 30){
					$this->session->set_flashdata('error', 'Date range is maximum 30 days');
					redirect('admin/reports');
				}
	      	}
		}
		

		if(!empty($data['status'])){
			$date = getRangeDate($data['status']);
			$start_date = $date['start_date'];
			$end_date = $date['end_date'];
		}else{
			$start_date = $dates_range[0];
			$end_date = $dates_range[1];
		}
		
		$reports=$this->Other_model->getReports($start_date,$end_date,$data['status']);
		$total_amount=$this->Other_model->getTotalAmount($start_date,$end_date);
		$size = sizeof($reports);
		//echo "<pre>";print_r($reports);die;
		
		$fileName = 'resources/excel/reports-'.time().'.xlsx';  

	    $objPHPExcel = new PHPExcel();
	    $objPHPExcel->setActiveSheetIndex(0);
		// set Header
		
		if($data['status'] == "this_week" || $data['status'] == "last_week"){
			$h = 'A';
        	$n = 1;
           for($i=0;$i<$size;$i++){
           		 
           		$objPHPExcel->getActiveSheet()->SetCellValue($h.$n,  date('D', strtotime($reports[$i]['date'])));
           		$h++;
           }

        }
        if($data['status'] == "this_month" || $data['status'] == "last_month"){
         	$h = 'A';
        	$n = 1;
           	for($i=0;$i<$size;$i++){
           		
           		$objPHPExcel->getActiveSheet()->SetCellValue($h.$n, $reports[$i]['date']);
           		$h++;
           	}
        }

        if($data['status'] == "last_year" || $data['status'] == "this_year"){
         	$h = 'A';
        	$n = 1;
           	for($i=0;$i<$size;$i++){
           		
           		$objPHPExcel->getActiveSheet()->SetCellValue($h.$n, date('M-Y', strtotime($reports[$i]['date'])));
           		$h++;
           	}
        }

        if($data['status'] == "today"){
           	$h = 'A';
        	$n = 1;
           	for($i=0;$i<$size;$i++){
           		
           		$objPHPExcel->getActiveSheet()->SetCellValue($h.$n, $reports[$i]['date']);
           		$h++;
           	}
        
        }

		if($data['status'] == ""){
            $h = 'A';
        	$n = 1;
           	for($i=0;$i<$size;$i++){
           		
           		$objPHPExcel->getActiveSheet()->SetCellValue($h.$n, $reports[$i]['date']);
           		$h++;
           	}
        
        }
	  
	    $rowCount = 2;
	   
	    if (!empty($reports)) {
	    	$f = 'A';
        	
		    foreach ($reports as $key=> $row) {  

		        $objPHPExcel->getActiveSheet()->SetCellValue($f.$rowCount, $row['admin_commission']);
		        $f++;
		       
	   		}
	    }
	

	    $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
	    $objWriter->save($fileName);
		//download file
	    header("Content-Type: application/vnd.ms-excel");
	    redirect($fileName);  
	}

	public function job_payment_history_invoice($job_id){
		$this->Other_model->paymentHistoryInvoice($job_id);

 	}



 	public function make_account(){
 		   require_once APPPATH."third_party/stripe/init.php";
         
            //set api key
             $stripe = array(
              "secret_key"      => "sk_test_Tr5hmR3bgqUQFwYHF9aQU9wr",
              "publishable_key" => "pk_test_hyaxWO6SNhf4WTL6ZIguvrGD"
            );

			\Stripe\Stripe::setApiKey('sk_test_Tr5hmR3bgqUQFwYHF9aQU9wr');

		



			$fp = fopen('resources/images/govermentid/1576566662128.jpg', 'r');
			$fp1 = fopen('resources/images/govermentid/1576566662128.jpg', 'r');
			$file1 = \Stripe\File::create([
			  'purpose' => 'identity_document',
			  'file' => $fp
			]);
			$file2 = \Stripe\File::create([
			  'purpose' => 'identity_document',
			  'file' => $fp1
			]);
			



			$data = \Stripe\Account::create([
			  'type' => 'custom',
			  'country' => 'US',
			  'email' => 'vsolanki@example.com',
			  'business_type' => 'individual',
			
			  'individual' => [

					  'first_name' => 'vsolanki',
					  'last_name' => 'vsolanki',
					  'phone'=>'8754124587',
					  'email' => 'vsolanki@example.com',
					  'ssn_last_4'=>'4512',
					  'id_number'=>"123454512",
					  'dob' => [
							  'day'=>'25',
							  'month'=>'02',
							  'year'=>'1994',
							  ],
					  'address'=>[
							  'city'=>'newcastle',
							  'country'=>'US',
							  'line1'=>'12716 SE 74th',
							  'postal_code'=>'98056',
							  'state'=>'WA',
							  ],
					   'verification' => [
										'document'=> [
													'back' => $file2->id,
													'front' => $file1->id,
													],
										],
      					
					 

				  ],
				'business_profile' => [
					  'name'=>'Software',
					  'mcc'=>'5734',
					  'product_description'=>'This is a test payout ',
					  
					  ],
				'external_account' => [
					  'object'=>'bank_account',
					  'country'=>'US',
					  'currency'=>'USD',
					  'account_holder_name'=>'vsolanki solanki',
					  'account_holder_type'=>'individual',
					  'routing_number'=>'110000000',
					  'account_number' => "000123456789",
					],
				'tos_acceptance'=>[
					  'date' => time(),
      					'ip' => $_SERVER['REMOTE_ADDR'],
				],
				

			  'requested_capabilities' => [
			    'card_payments',
			    'transfers',
			  ],
			]);

			$data->id;



 	}

 		public function send_amount(){
 		   require_once APPPATH."third_party/stripe/init.php";
            
            //set api key
             $stripe = array(
              "secret_key"      => "sk_test_Tr5hmR3bgqUQFwYHF9aQU9wr",
              "publishable_key" => "pk_test_hyaxWO6SNhf4WTL6ZIguvrGD"
            );
			\Stripe\Stripe::setApiKey("sk_test_Tr5hmR3bgqUQFwYHF9aQU9wr");

			\Stripe\Transfer::create([
			  "amount" => 40,
			  "currency" => "usd",
			  "destination" => "acct_1CFGM1Di1mhsSOMo",
			  "transfer_group" => "ORDER_95"
			]);
					

 	}

 	


} 