<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notification extends CI_Controller {

	public function __construct() {
		parent::__construct(); 
		$this->load->model('admin/Common_model');
		
		$this->load->helper('common_helper');
		
		ini_set('max_execution_time', 0);
	}

	public function index()	{

		$this->Common_model->check_login();
		check_permission('46','view','yes');

		$data['title']="Broadcast Notification | ".SITE_TITLE;
		$data['page_title']="Broadcast Notification";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Broadcast Notification List',
			'link' => ""
		);		
		 
		$page = $this->uri->segment(4) ? $this->uri->segment(4) : 0;
		$data['role_list'] = $this->Common_model->getRecords('roles','*',array('status'=>'active'),'',false);
		$like=array();
		$data['filter_title']='';
		
		if($this->input->get('title')){
			$data['filter_title']=$this->input->get('title');
		}
		$data['filter_content']='';
		if($this->input->get('content')){
			$data['filter_content']=$this->input->get('content');
		}
	 	  
		$data['total_records']=$this->Common_model->get_notification_total();
	 	$data['records_results']=$this->Common_model->get_notification_list($page);
	 	
	 	$data['pagination']=$this->Common_model->paginate(site_url('admin/broadcast_notification/list'),$data['total_records']);
		if(check_permission('46','add')){$data['add_action']=site_url('admin/broadcast_notification/send');}
		
		if(check_permission('46','delete')){$data['delete_action']=site_url('admin');}
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/broadcast_notification');
		$this->load->view('admin/include/footer');
		
	}

	public function broadcast_notification_send()	{
		$this->Common_model->check_login();
		check_permission('46','add','yes');
		$data['title']="Add Broadcast Notification | ".SITE_TITLE;
		$data['page_title']="Add Broadcast Notification";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Broadcast Notification Send',
			'link' => ""
		);
		
		if($this->input->post()) {
			$this->form_validation->set_rules('user', 'user', 'trim|required',array('required'=>'Please select %s'));
			$this->form_validation->set_rules('title', 'title', 'trim|required|max_length[25]',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('content', 'content', 'trim|required|max_length[250]',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('type', 'type', 'trim|required',array('required'=>'Please select %s'));
			if ($this->form_validation->run() == FALSE) 
			{	
				$this->form_validation->set_error_delimiters('', '');
			} else {
				$notify['type']='Broadcast';
				$from_email =  getAdminEmail(); 
				$admin_id = $this->session->userdata('admin_id');
				$type=$this->input->post('type');
				$noti_send_type=$this->input->post('user');
				$title=$this->input->post('title');
				$content=$this->input->post('content');
				$add_data = array(
					'notification_type' =>	$type, 
                  	'title' 			=> $this->input->post('title'),                  	
					'content' 			=> $this->input->post('content'),					
					'created' 			=> date("Y-m-d H:i:s")
				);
				$notification_id=$this->Common_model->addEditRecords('broadcast_notification', $add_data);
				if(!$notification_id) {
					$this->session->set_flashdata('error', 'Some error occured! Please try again.');
				} else {
					
		            $subject = $title; 
		            $data['details']	=	$this->input->post('content');
		            if($noti_send_type =='2') {
						$this->form_validation->set_rules('user_type', 'user type', 'trim|required',array('required'=>'Please select %s'));
						$user_types= $this->input->post('user_type');
						if(!empty($user_types))
	                    {	
							$all_types=implode(',', $user_types);
	                    	$where_type="user_type IN($all_types) and status='Active' and is_deleted=0";		
							$user_details=$this->Common_model->getRecords('users','email,user_id',$where_type,'',false);
						}
					}else{
						$user_details=$this->Common_model->getRecords('users','email,user_id',array('is_deleted'=>0,'status'=>'Active'),'',false);
					}//echo '<pre>';print_r($user_details);exit;
					$user_data=array();
					if(!empty($user_details)){
						for ($i=0; $i < count($user_details); $i++) { 

							$user_data[] = array(
								'user_id'=>$user_details[$i]['user_id'],
								'email'=>$user_details[$i]['email'],
								'type'=>$type,
								'title' => $this->input->post('title'),               	
								'content'=> $this->input->post('content'),
								'notification_id'=>$notification_id,		
								'created'=>date('Y-m-d H:i:s'),
							);
						}
						if(!empty($user_data)){
							$this->db->insert_batch('email_notification_cron', $user_data); 
						}
					}
					$performer_id = $this->session->userdata('admin_id');
					$admin_username = getAdminUsername($performer_id);
					$log_msg = $admin_username ." broadcast with title '". $this->input->post('title')."' and notification type '". $type."'";
					actionLog('broadcast_notification',$notification_id,'Add',$log_msg,'Admin',$performer_id);
					$this->session->set_flashdata('success', 'Broadcast notification sent by cron.');
					//redirect('admin/broadcast_notification/send');
					redirect('admin/broadcast_notification/list');
				}
			}
		}
		$data['from_action']=site_url('admin/broadcast_notification/send');
		$data['back_action']=site_url('admin/broadcast_notification/list');
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/broadcast_notification_send');
		$this->load->view('admin/include/footer');
		
	}


 


} // class end