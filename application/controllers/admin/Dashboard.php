<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Dashboard extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	
        $this->load->model(array('admin/Common_model','admin/Dashboard_model'));
       // $this->load->model(array('admin/Common_model','admin/F_model'));
		$this->load->helper('Common_helper'); 
	}
	public function index()
	{
		
		$this->Common_model->check_login();
		$type = $this->session->userdata('user_type');
		if($type=='Super Admin'){
			$type='Admin';
		}
		$role_id = $this->session->userdata('role_id');
		$data['title']="Dashboard| ".SITE_TITLE;
		$data['page_title']="Dashboard";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Home',
			'link' => '#'
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

	 	if(check_permission('13','view')){ 
	 		$data['total_users_url'] =base_url().'admin/user/list'; 
			//$data['total_users'] = $this->Common_model->getNumRecords('users','user_id',array('is_deleted'=>0)); 
			$data['total_users'] = $this->Common_model->getNumRecords('users','user_id',array('is_deleted'=>0));

	 	}else{
	 		$data['total_users_url'] ='';
	 	}

 		if(check_permission('13','view')){ 
	 		$data['total_celebrant_url'] =base_url().'admin/user/list?user_type=1'; 
			$data['total_celebrant'] = $this->Common_model->getNumRecords('users','user_id',array('is_deleted'=>0,'user_type'=>1)); 
	 	}else{
	 		$data['total_celebrant_url'] ='';
	 	}


 		if(check_permission('13','view')){ 
	 		$data['total_sp_url'] =base_url().'admin/user/list?user_type=2'; 
			$data['total_sp'] = $this->Common_model->getNumRecords('users','user_id',array('is_deleted'=>0,'user_type'=>2)); 
	 	}else{
	 		$data['total_sp_url'] ='';
	 	}
		
 		if(check_permission('13','view')){ 
	 		$data['total_sp_cp_url'] =base_url().'admin/user/list?user_type=3'; 
			$data['total_sp_cp'] = $this->Common_model->getNumRecords('users','user_id',array('is_deleted'=>0,'user_type'=>3)); 
	 	}else{
	 		$data['total_sp_cp_url'] ='';
	 	}
		
 		if(check_permission('56','view')){ 
	 		$data['total_order_url'] =base_url().'admin/orders/list'; 
			$data['total_order'] = $this->Common_model->getNumRecords('orders','id',array('type!='=>'Draft')); 
	 	}else{
	 		$data['total_order_url'] ='';
	 	}
		
 		if(check_permission('54','view')){ 
	 		$data['total_services_url'] =base_url().'admin/services/list'; 
			$data['total_services'] = $this->Common_model->getNumRecords('services','id',array('is_deleted'=>0)); 
	 	}else{
	 		$data['total_services_url'] ='';
	 	}
		
 		if(check_permission('51','view')){ 
	 		$data['total_reviews_url'] =base_url().'admin/rating/list'; 
			$data['total_reviews'] = $this->Common_model->getNumRecords('reviews','id',array('is_deleted'=>0)); 
	 	}else{
	 		$data['total_reviews_url'] ='';
	 	}
 		if(check_permission('52','view')){ 
	 		$data['total_post_url'] =base_url().'admin/post/list'; 
			$data['total_post'] = $this->Common_model->getNumRecords('post','id',array('is_deleted'=>0)); 
	 	}else{
	 		$data['total_post_url'] ='';
	 	}


 		$data['total_order_payment_url'] =''; 
		$data['total_order_payment'] = $this->Common_model->getRecords('orders','SUM(total_amount) as total_amounts','','',true); 
	 	
		

		
		 
		
		// echo "<pre>";print_r($data['total_order_payment']);die;
		switch ($type){
			
			
			case 'Admin':

			
			$data['total_users'] = $this->Common_model->getNumRecords('users','user_id',array('is_deleted'=>0));
           

			$this->load->view('admin/include/header',$data);
		
			$this->load->view('admin/include/sidebar');
			if($this->session->userdata('user_type')!='Super Admin'){
	            if($this->Common_model->getRecords('role_permissions','view',array('role_id'=>$role_id,'section_id'=>1,'view'=>1),'',true)){
					$this->load->view('admin/dashboard');
				}else{
					$this->load->view('admin/prohibit_dashboard');
				}
			}else{
				$this->load->view('admin/dashboard');
			}
			
			$this->load->view('admin/include/footer');
                        # code...
            break;
    	}

	}
	
}

