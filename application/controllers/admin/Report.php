<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller {
	public function __construct()
	{
		parent:: __construct();
		$this->load->model(array('admin/Common_model','admin/User_model','admin/Chat_model'));
		$this->load->helper('common_helper');
		$this->load->library('Ajax_pagination');
	}

	public function index()
	{
		$this->Common_model->check_login();
	}
 

	public function report_list()
	{
		// echo $id;die;
		$this->Common_model->check_login();
		
	    check_permission('66','view','yes');
     
		
		$data['title']="Report List | ".SITE_TITLE;
		$data['page_title']="Report List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Report List',
			'link' => ""
		);

		$data['action_url'] = base_url().'admin/report/list';
		$data['back_url'] = base_url().'admin/report/list';
		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$like=array();

		$data['filter_user_fullname']='';
		if($this->input->get('userfullname')){
			$data['filter_user_fullname']=$this->input->get('userfullname');
		}
		$data['filter_post_number']='';
		if($this->input->get('post_number')){
			$data['filter_post_number']=$this->input->get('post_number');
		}
		/*
		$data['filter_media_type']='';
		if($this->input->get('media_type')){
			$data['filter_media_type']=$this->input->get('media_type');
		}*/
		
		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}
 
		// if(!$data['details'] = $this->Common_model->getRecords('users','*',array('user_id'=>$id),'',true)){
		// 	redirect('pages/page_not_found');
		// }
 
		$data['total_records']=$this->User_model->getReportPostAll(true);
	 	
	 	$data['records_results']=$this->User_model->getReportPostAll('',ADMIN_LIMIT,$page);
	 	// echo "<pre>";print_r($data['records_results']);die;
		$data['pagination']=$this->Common_model->paginate(site_url('admin/report/list'),$data['total_records']);
  		
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/all_report_list');
		$this->load->view('admin/include/footer');
	} 

 
	public function report_reply($report_id) 
	{ 	
		$this->Common_model->check_login();
		
		if(!$report_id) {
			redirect('pages/page_not_found');
		}
		$data['title']="Report Details | ".SITE_TITLE;
		$data['page_title']="Report Details";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Report Center',
			'link' => site_url('admin/report/list')
		);	
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Report Details',
			'link' => ""
		);
		$user_type = $this->session->userdata('user_type');
		if($user_type=='Super Admin'){
            $user_type='Admin';
        }

        $admin_id=$this->session->userdata('admin_id');

		if(!$data['report_detail'] = $this->User_model->getreportDetail($report_id)) {

			redirect('pages/page_not_found');
		} else {	

			$data['report_reply']  =$this->Common_model->getRecords('report_reply','*',array('report_id'=>$data['report_detail']['id']),'id DESC',false); 
					
			// echo "<pre>";print_r($data['report_reply']);die;
			
		 
			if($this->input->post()) {
				$this->form_validation->set_rules('message', 'message', 'trim|required');
				if ($this->form_validation->run() == FALSE) {	
					$this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');
				} else {
					

				$post_user_user_id  =$this->Common_model->getRecords('users','*',array('user_id'=>$data['report_detail']['post_user_user_id']),'',true); 
					
					$emaildetail=getNameEmailAddress($admin_id);

		 			$from_name = $emaildetail['fullname']; 
		 			//$from_email = $emaildetail['email']; 
		 			$from_email = getNotificationEmail(); 
		 			
		 			$to_name = $post_user_user_id['fullname'];
					$to_email = $post_user_user_id['email']; 

					$message = $this->input->post('message');
					$subject = WEBSITE_NAME . ' - Warning Mail ';


					$data['message'] = $message; 
					$data['name'] = $to_name; 
				
					$body = $this->load->view('template/common', $data,TRUE);
				
					
					$insert_data = array(
						'message' 	=> $message, 
						'reply_by'   => $admin_id,
						'report_id'   => $report_id,
						'subject' 	=> $subject,
						'created' 	=> date("Y-m-d H:i:s")
					);
					//Send mail 
					if($this->Common_model->sendEmail($to_email,$subject,$body,$from_email)) {

						$last_id=$this->Common_model->addEditRecords('report_reply', $insert_data); 
						$admin_id = $this->session->userdata('admin_id');
						$admin_username = getAdminUsername($admin_id);
						$log_msg = $admin_username ." sent email for reported post '".$data['report_detail']['post_number']."'";
						actionLog('report_reply',$last_id,'Update',$log_msg,'Admin',$admin_id);
							$this->session->set_flashdata('success', 'Mail Sent Successfully.');
						redirect($_SERVER['HTTP_REFERER']);
					} else {
						$this->session->set_flashdata('error', 'Message not sent. Please try again !!');
						redirect($_SERVER['HTTP_REFERER']);
					}
				}
			}

			$data['user_type']=$user_type;
			$data['from_action']=base_url().'admin/report/reply/'.$report_id;
			
			// echo "<pre>";print_r($data);die;
			$data['back_action']=site_url('admin/contact/list');
			
			$this->load->view('admin/include/header',$data);
			$this->load->view('admin/include/sidebar');
			$this->load->view('admin/report_reply');
			$this->load->view('admin/include/footer');
		}
	}
	
	/***************** Email Templates *******************/
}
