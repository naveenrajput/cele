<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Support extends CI_Controller {
	public function __construct() {
		parent:: __construct();
		$this->load->model(array('admin/Common_model','admin/Cmspage_model'));
		$this->load->helper('common_helper');
		$this->load->library('Ajax_pagination');
	}

	public function index() {
		$this->Common_model->check_login();
		$this->banners_list();
	}

	public function contact_list() 
	{
		$this->Common_model->check_login();
		if($this->session->userdata('user_type')=='Super Admin' || $this->session->userdata('user_type')=='Admin'){
			check_permission('4','view','yes');
			if(check_permission('4','view')){$data['edit_action']=site_url('admin/contact/reply');}
		}
		
		$data['title']="Support Center | ".SITE_TITLE;
		$data['page_title']="Support Center";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Support Center',
			'link' => ""
		);
		$user_type = $this->session->userdata('user_type');
		if($user_type=='Super Admin'){
            $user_type='Admin';
        }

        

		$per_page = ADMIN_LIMIT;
		$offset = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$like=array();
		$data['filter_type']='';
		if($this->input->get('filter_type')){
			$data['filter_type']=$this->input->get('filter_type');
		}
		$data['filter_ticket_id']='';
		if($this->input->get('ticket_id')){
			$data['filter_ticket_id']=$this->input->get('ticket_id');
		}
		$data['filter_name']='';
		if($this->input->get('name')){
			$data['filter_name']=$this->input->get('name');
		}

		$data['filter_email']='';
		if($this->input->get('email')){
			$data['filter_email'] = $this->input->get('email');
		}
		
		$data['filter_mobile']='';
		if($this->input->get('contact_no')){
			$data['filter_mobile'] = $this->input->get('contact_no');
		}

		$data['filter_subject']='';
		if($this->input->get('subject')){
			$data['filter_subject']=$this->input->get('subject');
		}

		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}
		
		$data['total_records'] = $this->Cmspage_model->get_all_admin_contacts(0,0);
	 	$data['records_results']=$this->Cmspage_model->get_all_admin_contacts($per_page,$offset);

		$data['pagination']=$this->Common_model->paginate(site_url('admin/contact/list'),$data['total_records']);
  
 		if($user_type!='Admin'){
 			$data['add_action']=site_url('admin/contact/add_contact');
 		}
 		$data['user_type']=$user_type;
 		
		
		$data['filter_action']=site_url('admin/contact/list');
		// echo "<pre>";print_r($data);exit;
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/contact_list');
		$this->load->view('admin/include/footer');
	}


	public function add_contact() {
		
		$this->Common_model->check_login();
		if($this->session->userdata('user_type')=='Super Admin' || $this->session->userdata('user_type')=='Admin'){
			check_permission('4','add','yes');
		}
		
		$data['title']="Add Support Ticket | ".SITE_TITLE;
		$data['page_title']="Support Ticket";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Support Center',
			'link' => site_url('admin/contact/list')
		);	
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Reply',
			'link' => ""
		);
		$user_type = $this->session->userdata('user_type');
		if($user_type=='Super Admin'){
            $user_type='Admin';
        }
        $admin_id=$this->session->userdata('admin_id');

		if($this->input->post()) {
			$this->form_validation->set_rules('subject', 'subject', 'trim|required');
			$this->form_validation->set_rules('message', 'message', 'trim|required');
			if ($this->form_validation->run() == FALSE) {	
				$this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');
			} else {

				$emaildetail=getNameEmailAddress($admin_id);
	 			$from_name = $emaildetail['fullname']; 
	 			$from_email = $emaildetail['email']; 

				$todetail=getSuperAdminDetail();
				$to_name = $todetail['fullname']; 
	 			$to_email = $todetail['email']; 

				$message = $this->input->post('message');
				$subject = $this->input->post('subject');
				$subject = WEBSITE_NAME . ' - ' . $subject;


				$data['message'] = $message; 
				$data['fromname'] = $from_name; 
				$data['toname'] = $to_name;

				$senttype="";
				if($user_type=='User'){
					$senttype="user_admin";
				}

				
				$body = $this->load->view('template/contact_us_template', $data,TRUE);
				$admin_id=getParentAdminId($admin_id);
				$insert_data = array(
					'message' 	=> $message,
					'subject' 	=> $this->input->post('subject'),
					'contact_no'=> $this->input->post('contact_no'),
					'name' 		=> $from_name,
					'email' 	=> $from_email,
					'type'  	=> $senttype,
					'from_id'  	=> $admin_id,
					'to_id'  	=> $todetail['admin_id'],
					'parent_id' => 0,
					'status' 	=> 'Pending',
					'created' 	=> date("Y-m-d H:i:s")
				);
				//Send mail 
				if($this->Common_model->sendEmail($to_email,$subject,$body,$from_email)) {

					$contact_id=$this->Common_model->addEditRecords('contact', $insert_data);
					$ticket_id=str_pad($contact_id,6,0,STR_PAD_LEFT);
					$this->Common_model->addEditRecords('contact', array('ticket_id'=>$ticket_id), array('contact_id'=>$contact_id));

					$this->session->set_flashdata('success', 'Message Sent Successfully.');
					redirect($_SERVER['HTTP_REFERER']);
				} else {
					$this->session->set_flashdata('error', 'Message not sent. Please try again !!');
					redirect($_SERVER['HTTP_REFERER']);
				}
			}
		}
		$data['from_action']=site_url('admin/contact/add_contact');
		$data['back_action']=site_url('admin/contact/list');
		$data['user_type']=$user_type;
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/add_contact');
		$this->load->view('admin/include/footer');
		
	}


	public function contact_reply($contact_id) 
	{
		
		$this->Common_model->check_login();
		
		if(!$contact_id) {
			redirect('pages/page_not_found');
		}
		$data['title']="Support Details | ".SITE_TITLE;
		$data['page_title']="Support Details";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Support Center',
			'link' => site_url('admin/contact/list')
		);	
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Support Details',
			'link' => ""
		);
		$user_type = $this->session->userdata('user_type');
		if($user_type=='Super Admin'){
            $user_type='Admin';
        }

        $admin_id=$this->session->userdata('admin_id');

		if(!$data['contact_detail'] = $this->Cmspage_model->getContactDetail($contact_id)) {
			redirect('pages/page_not_found');
		} else {	
			//echo '<pre>';print_r($data['contact_detail']);exit;
			if($data['contact_detail'][0]['type']=='user_admin')
			{
				if($user_type!='Admin' && $user_type!='User'){
					redirect('pages/page_not_found');
				}
			}
			
			
			if($this->input->post()) {
				$this->form_validation->set_rules('message', 'message', 'trim|required');
				if ($this->form_validation->run() == FALSE) {	
					$this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');
				} else {
					

					if($user_type=='Admin'){

			 			$emaildetail=getNameEmailAddress($admin_id);

			 			$from_name = $emaildetail['fullname']; 
			 			$from_email = $emaildetail['email']; 
			 			
			 			$to_name = $data['contact_detail'][0]['name'];
						$to_email = $data['contact_detail'][0]['email']; 
					
					}else{

						$fromdetail=getNameEmailAddress($data['contact_detail'][0]['from_id']);
						$from_name = $fromdetail['fullname']; 
			 			$from_email = $fromdetail['email']; 

						$todetail=getNameEmailAddress($data['contact_detail'][0]['to_id']);

						$to_name = $todetail['fullname']; 
			 			$to_email = $todetail['email']; 
					}

					$message = $this->input->post('message');
					$subject = WEBSITE_NAME . ' - ' . $data['contact_detail'][0]['subject'].' #' .$data['contact_detail'][0]['ticket_id'];


					$data['message'] = $message; 
					$data['name'] = $to_name; 
					if($user_type=='Admin'){
						$body = $this->load->view('template/common', $data,TRUE);
					}else{
						$data['fromname'] = $from_name; 
						$data['toname'] = $to_name; 
						$body = $this->load->view('template/contact_us_template', $data,TRUE);
					}
					
					$insert_data = array(
						'message' 	=> $message,
						'name' 		=> $from_name,
						'parent_id' => $contact_id,
						'type'   => $data['contact_detail'][0]['type'],
						'reply_by'   => $admin_id,
						'replied_by_type' => $user_type,
						'subject' 	=> $subject,
						'status' 	=> 'Replied',
						'created' 	=> date("Y-m-d H:i:s")
					);
					//Send mail 
					if($this->Common_model->sendEmail($to_email,$subject,$body,$from_email)) {

						$this->Common_model->addEditRecords('contact', $insert_data);
						$this->Common_model->addEditRecords('contact', array('status'=>'Replied','last_updated'=>date('Y-m-d H:i:s')), array('contact_id'=>$contact_id));

						$this->session->set_flashdata('success', 'Message Sent Successfully.');
						redirect($_SERVER['HTTP_REFERER']);
					} else {
						$this->session->set_flashdata('error', 'Message not sent. Please try again !!');
						redirect($_SERVER['HTTP_REFERER']);
					}
				}
			}

			$data['user_type']=$user_type;
			$data['from_action']="";
			
			if(count($data['contact_detail'])<=1 && $user_type=='Admin'){
				$data['from_action']=site_url('admin/contact/reply/'.$contact_id);	
				if(check_permission('4','edit')){$data['from_action']=site_url('admin/contact/reply/'.$contact_id);}

			}else if(count($data['contact_detail'])>1 && $user_type=='Admin'){
				$data['from_action']=site_url('admin/contact/reply/'.$contact_id);	
			}
			
			$data['back_action']=site_url('admin/contact/list');
			
			$this->load->view('admin/include/header',$data);
			$this->load->view('admin/include/sidebar');
			$this->load->view('admin/contact_reply');
			$this->load->view('admin/include/footer');
		}
	}

	
	


	

	



/* End Class */
}