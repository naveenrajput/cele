<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cmscontent extends CI_Controller {
	public function __construct() {
		parent:: __construct();
		$this->load->model(array('admin/Common_model','admin/Cmspage_model'));
		$this->load->helper('common_helper');
		$this->load->library('Ajax_pagination');
	}

	public function index() {
		$this->Common_model->check_login();
		$this->banners_list();
	}

	public function states_list()
	{
		$this->Common_model->check_login();
		$data['title']="States List | ".SITE_TITLE;
		$data['page_title']="States List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'States List',
			'link' => ""
		);

		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$like=array();
		$data['filter_title']='';
		if($this->input->get('title')){
			$data['filter_title']=$this->input->get('title');
		}
		
		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}
		
		$data['total_records']=$this->Cmspage_model->get_states_list(0,0);
	 	
	 	$data['records_results']=$this->Cmspage_model->get_states_list(ADMIN_LIMIT,$page);
		if($data['undeletable'] = $this->Common_model->group_By_Records('facility_requests','request_type')) {	
			$data['undeletable_ids'] = array();
			foreach($data['undeletable'] as $list) {
				$data['undeletable_ids'][] = $list['request_type'];
			}
		}
	 	//echo '<pre>';print_r()
		//echo $this->db->last_query();exit;
		$data['pagination']=$this->Common_model->paginate(site_url('admin/request/request_type_list'),$data['total_records']);
  		$data['add_action']='admin/request/add_request_type';
  		$data['edit_action']='admin/request/edit_request_type';
  		$data['delete_action']='1';

		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/request/request_type_list');
		$this->load->view('admin/include/footer');
	} 

	
	public function add_states() 
	{
		//check_permission('4','add','yes');
		$this->Common_model->check_login();
		$data['title']="Add Request Type | ".SITE_TITLE;
		$data['page_title']="Add Request Type";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Request Type List',
			'link' => site_url('admin/request/request_type_list')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Add Request Type',
			'link' => ""
		);	
		
		if($this->input->post()) {
			$this->form_validation->set_rules('title', 'title', 'trim|required|callback_rolekey_exists[request_types-title-add]',array('required'=>'Please enter %s'));
			
			$this->form_validation->set_rules('status', 'status', 'trim|required',array('required'=>'Please select %s'));
		
			$this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');

			if($this->form_validation->run()==TRUE) 
			{
				if($last_sort_order=$this->Common_model->getRecords('request_types','sort_order',array('is_deleted'=>0),'id DESC',true))
				{
					$sort_order=$last_sort_order['sort_order']+1;
				}else
				{
					$sort_order=1;
				}

				$insert_data = array(
					'title'=> $this->input->post('title'),
					'status'=> $this->input->post('status'),
					'sort_order'=> $sort_order,
					'created'=> date("Y-m-d H:i:s"),
					'created_by'=> $this->session->userdata('admin_id')
				);
		 		
				if(!$id = $this->Common_model->addEditRecords('request_types', $insert_data)) { 
                	echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Some error occurred, Please try later .</div>')); exit;               
	            } else {     
	                echo json_encode(array('status'=>1,'message'=>'<div class="alert alert-success">Request Type Added Successfully.</div>')); exit; 
	            } 
						
			}
		}
		
		$data['form_action']=site_url('admin/request/add_request_type');
		$data['back_action']=site_url('admin/request/request_type_list');

		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/request/add_request_type');
		$this->load->view('admin/include/footer');
	}

	
	//Cms page start
	public function pages_list() {
		$this->Common_model->check_login();
		// if($this->session->userdata('user_type')=='Facility')
		// {
		// 	check_permission('83','view','yes');
		// 	if(check_permission('83','edit')){$data['edit_action']=site_url('admin/pages/edit');}
		// 	if(check_permission('83','view')){$data['view_action']=site_url('admin/user/view');}
		// }
		// else{
		check_permission('3','view','yes');
		if(check_permission('3','edit')){$data['edit_action']=site_url('admin/pages/edit');}
		if(check_permission('3','view')){$data['view_action']=site_url('admin/user/view');}			
		// }
		$data['title']="Pages | ".SITE_TITLE;
		$data['page_title']="Pages";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Pages',
			'link' => ""
		);
		$page = $this->uri->segment(4) ? $this->uri->segment(4) : 0;
		
		$like=array();
		$data['filter_title']='';
		if($this->input->get('title')){
			$data['filter_title']=$this->input->get('title');
		}
		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}

		$data['total_records']=$this->Cmspage_model->get_pages_total();
		//	print_r($data['total_records']);
	 	$data['records_results']=$this->Cmspage_model->get_pages_list($page);
		//$data['total_records']=count($this->Common_model->getRecords('response', '*', "","id Desc", false));
		$data['pagination']=$this->Common_model->paginate(site_url('admin/pages/list'),$data['total_records']);
		//$data['records_results']=$this->Common_model->getRecords('users', '*',"","user_id Desc", false);
		$index=0;
		$admin_id = $this->session->userdata('admin_id');
		
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/pages_list');	
		$this->load->view('admin/include/footer');
	} 
	
		

	public function edit_page($id) {
		$this->Common_model->check_login();

		if(!$id) {
			redirect('pages/page_not_found');
		}
		check_permission('3','edit','yes');			
		
		$data['title']="Edit Page | ".SITE_TITLE;
		$data['page_title']="Edit Page";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Pages List',
			'link' => site_url('admin/pages/list')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Edit Page',
			'link' => ""
		);	
		
		if(!$data['pages']=$this->Common_model->getRecords('pages','*',array('page_id'=>$id),'',true)) {
			redirect('pages/page_not_found');
		}

		if($this->input->post()) {
			$this->form_validation->set_rules('title', 'name', 'trim|required|max_length[150]',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('content', 'content', 'trim|required',array('required'=>'Please enter %s.'));
			$this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');
			//	echo "<pre>"; print_r($_POST); exit;	
			if($this->form_validation->run()==TRUE) 
			{
				$update_data = array(
					'title'=> $this->input->post('title'),
					'content'=> $this->input->post('content'),
					'created'=> date("Y-m-d H:i:s")
				);
				//echo "<pre>"; print_r($id); exit;	
		 		if(!$this->Common_model->addEditRecords('pages', $update_data,array('page_id'=>$id))) {
					$this->session->set_flashdata('error', 'Some error occurred! Please try again.');
				} else {
					$admin_id = $this->session->userdata('admin_id');
					$admin_username = getAdminUsername($admin_id);
	            	$log_msg = $admin_username." updated '".$this->input->post('title')."' page.";
	            	actionLog('pages',$id,'update',$log_msg,'Admin',$admin_id); 
					$this->session->set_flashdata('success', 'Page updated successfully.');
					redirect('admin/pages/list');
				}
			}
		}

		$data['from_action']=site_url('admin/pages/edit/'.$id);
		$data['back_action']=site_url('admin/pages/list');

		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/edit_page');
		$this->load->view('admin/include/footer');

	}
	//end of cms pages

	public function add_state_city_by_ajax()
    {
       	if($_POST) {
       		
       		if($_POST['action_type']=='state'){
       			$state_name=$_POST['newstate'];
       			if($exist_data=$this->Common_model->getRecords('us_states', 'id', array('state_name'=>$state_name), '', true)) {
       				$msg='<div class="alert alert-danger fade in"><button data-dismiss="alert" class="close" type="button">×</button>State name already exist</div>';
       				echo json_encode(array('status'=>0,'message'=>$msg));
       				exit;
       			}else{
       				$this->Common_model->addEditRecords('us_states',array('state_name'=>$state_name));

       				$msg='<div class="alert alert-success fade in"><button data-dismiss="alert" class="close" type="button">×</button>State Added Successfully</div>';

       				echo json_encode(array('status'=>1,'message'=>$msg));
       				exit;
       			}

       		}else{
       			$state_id=$_POST['newstate_id'];
       			$city=$_POST['newcity'];

       			if($exist_data=$this->Common_model->getRecords('us_cities', 'id', array('state_id'=>$state_id,'title'=>$city), '', true)) {
       				$msg='<div class="alert alert-danger fade in"><button data-dismiss="alert" class="close" type="button">×</button>City name already exist</div>';

       				echo json_encode(array('status'=>0,'message'=>$msg));
       				exit;
       			}else{
       				$this->Common_model->addEditRecords('us_cities',array('state_id'=>$state_id,'title'=>$city));

       				$msg='<div class="alert alert-success fade in"><button data-dismiss="alert" class="close" type="button">×</button>City Added Successfully</div>';
       				echo json_encode(array('status'=>1,'message'=>$msg));
       				exit;
       			}
       		}
	       	
       	}else{
       		$data['states'] = $this->Common_model->getRecords('us_states','*','','state_name');
       		echo $this->load->view('admin/user/state_city_popup',$data,true);
       		die;
       	}
    }
    public function contact_us_form() {
		$this->Common_model->check_login();
		check_permission('68','view','yes');
		$data['title']="Contact-Us | ".SITE_TITLE;
		$data['page_title']="Contact-Us";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Contact-Us',
			'link' => ""
		);
		
		$per_page = ADMIN_LIMIT;
		$offset = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$like=array();
		$data['filter_type']='';
		if($this->input->get('filter_type')){
			$data['filter_type']=$this->input->get('filter_type');
		}
		$data['filter_ticket_id']='';
		if($this->input->get('ticket_id')){
			$data['filter_ticket_id']=$this->input->get('ticket_id');
		}
		$data['filter_name']='';
		if($this->input->get('name')){
			$data['filter_name']=$this->input->get('name');
		}

		$data['filter_email']='';
		if($this->input->get('email')){
			$data['filter_email'] = $this->input->get('email');
		}
		
		$data['filter_mobile']='';
		if($this->input->get('contact_no')){
			$data['filter_mobile'] = $this->input->get('contact_no');
		}

		$data['filter_subject']='';
		if($this->input->get('subject')){
			$data['filter_subject']=$this->input->get('subject');
		}

		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}
		
		$data['total_records'] = $this->Cmspage_model->get_all_admin_contacts(0,0);
	 	$data['records_results']=$this->Cmspage_model->get_all_admin_contacts($per_page,$offset);

		$data['pagination']=$this->Common_model->paginate(site_url('admin/contact_us/list'),$data['total_records']);
  
 		
		$data['filter_action']=site_url('admin/contact_us/list');
		$data['detail_action']=site_url('admin/contact_us/detail');
		
		
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/contact_form_list');
		$this->load->view('admin/include/footer');
	} 
	public function contact_details($contact_id) 
	{
		
		$this->Common_model->check_login();
		check_permission('68','view','yes');
		if(!$contact_id) {
			redirect('pages/page_not_found');
		}
		$data['title']="Contact-Us Detail | ".SITE_TITLE;
		$data['page_title']="Contact-Us Detail";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Contact-Us',
			'link' => site_url('admin/contact_us/list')
		);	
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Contact-Us Detail',
			'link' => ""
		);
		
        $admin_id=$this->session->userdata('admin_id');
        if(!$data['contact_detail']=$this->Common_model->getRecords('contact','*',array('contact_id'=>$contact_id),'',true)) {
			redirect('pages/page_not_found');
		}
			$data['back_action']=site_url('admin/contact_us/list');
			
			$this->load->view('admin/include/header',$data);
			$this->load->view('admin/include/sidebar');
			$this->load->view('admin/contact_form_detail');
			$this->load->view('admin/include/footer');
		
	}
	public function banner_list()
	{
		$this->Common_model->check_login();
		
	    check_permission('49','view','yes');
    	if(check_permission('49','add')){$data['add_action']='admin/banner/add';}
  		if(check_permission('49','edit')){$data['edit_action']='admin/banner/edit';}
  		if(check_permission('49','delete')){$data['delete_action']='1';}
	  	
		
		$data['title']="Banner | ".SITE_TITLE;
		$data['page_title']="Banner";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Banner',
			'link' => ""
		);

		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$like=array();
		$data['filter_title']='';
		if($this->input->get('title')){
			$data['filter_title']=$this->input->get('title');
		}
				
		$data['total_records']=$this->Cmspage_model->get_banner_list(0,0);
	 	$data['records_results']=$this->Cmspage_model->get_banner_list(ADMIN_LIMIT,$page);
		$data['pagination']=$this->Common_model->paginate(site_url('admin/banner/list'),$data['total_records']);
  		
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/banner_list');
		$this->load->view('admin/include/footer');
	} 

	public function add_banner() 
	{
		$this->Common_model->check_login();
		check_permission('49','view','yes');
		$data['title']="Add Banner | ".SITE_TITLE;
		$data['page_title']="Add Banner";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Banners List',
			'link' => site_url('admin/banner/list')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Add Banner',
			'link' => ""
		);	
		if($this->input->post()) {
			$this->form_validation->set_rules('title', 'title', 'trim|required|max_length[50]',array('required'=>'Please enter %s'));
			$this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');

			if($this->form_validation->run()==TRUE) 
			{
				if($_FILES['image']['name'] =='') { 
					$this->session->set_flashdata('error', "Please upload image.");
				} else {
					if($_FILES['image']['error']==0) {
						$image_path = BANNER_PATH;
						$allowed_types = 'jpg|jpeg|png|JPG|PNG|JPEG';
						$file='image';
						$responce = BannerUpload($image_path,$allowed_types,$file);
				
						if($responce['status']==0){
							$data['upload_error'] = $responce['msg'];	
						} else {
							$insert_data = array(
								'title'=> $this->input->post('title'),
								'subtitle'=> $this->input->post('subtitle'),
								'image'=> $responce['image_path'],
								'created'=> date("Y-m-d H:i:s")
							);
							
					 		if(!$this->Common_model->addEditRecords('banner', $insert_data)) {
								$this->session->set_flashdata('error', 'Some error occured! Please try again.');
							} else {
								$this->session->set_flashdata('success', 'Banner added successfully.');
								redirect('admin/banner/list');
							}
						}
					} else {
						$this->session->set_flashdata('error', "Invalid image, Please try again.");
					}
				}
			}
		}
		
		$data['from_action']=site_url('admin/banner/add');
		$data['back_action']=site_url('admin/banner/list');

		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/add_banner');
		$this->load->view('admin/include/footer');
	}

	

	public function edit_banner($id) 
	{
		$this->Common_model->check_login();
		if(!$id) {
			redirect('pages/page_not_found');
		}
		$data['title']="Edit Banner | ".SITE_TITLE;
		$data['page_title']="Edit Banner";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Banner List',
			'link' => site_url('admin/banner/list')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Edit Banner',
			'link' => ""
		);	
		
		if(!$data['banners']=$this->Common_model->getRecords('banner','*',array('id'=>$id),'',true)) {
			redirect('pages/page_not_found');
		}
		if($id==1 || $id==2 || $id==3){
			$data['title_edit']='no';
		}

		if($this->input->post()) {
			$this->form_validation->set_rules('title', 'title', 'trim|required|max_length[150]',array('required'=>'Please enter %s'));
			$this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');

			if($this->form_validation->run()==TRUE) 
			{
				$update_data = array(
					'title'=> $this->input->post('title'),
					'subtitle'=> $this->input->post('subtitle'),
					'modified'=> date("Y-m-d H:i:s")
				);
		 		if(!$this->Common_model->addEditRecords('banner', $update_data,array('id'=>$id))) {
					$this->session->set_flashdata('error', 'Some error occured! Please try again.');
				} else {
					$this->session->set_flashdata('success', 'Banner updated successfully.');
					//redirect('admin/edit_banner/'.$id);
				}
			}
		}

		$data['banners']=$this->Common_model->getRecords('banner','*',array('id'=>$id),'',true);
		$data['from_action']=site_url('admin/banner/edit/'.$id);
		$data['back_action']=site_url('admin/banner/list');

		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/edit_banner');
		$this->load->view('admin/include/footer');

	}
/* End Class */
}