<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends CI_Controller {
	public function __construct()
	{
		parent:: __construct();
		$this->load->model(array('admin/Common_model','admin/Event_model'));
		$this->load->helper('common_helper');
		$this->load->library('Ajax_pagination');
	}

	public function index()
	{
		$this->Common_model->check_login();
	}
 

	public function events_list() {
		$this->Common_model->check_login();
		check_permission('55','view','yes');
		$data['title']="Event List | ".SITE_TITLE;
		$data['page_title']="Event List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Event List',
			'link' => ""
		);
		$page = $this->uri->segment(5) ? $this->uri->segment(5) : 0;
		 
		$data['type'] = 1;
		$data['reset_action']=site_url('admin/events/list');
 
 
		$data['filter_event_title']='';
		if($this->input->get('event_title')){
			$data['filter_event_title'] = trim($this->input->get('event_title')); 
		}
		$data['filter_event_number']='';
		if($this->input->get('event_number')){
			$data['filter_event_number'] = trim($this->input->get('event_number')); 
		}
		$data['filter_username']='';
		if($this->input->get('username')){
			$data['filter_username'] = trim($this->input->get('username')); 
		}
		$data['filter_date_rang']='';
		if($this->input->get('date_rang')){
		 	$date_rang=trim($this->input->get('date_rang'));
            $split_date=explode(" - ",$date_rang);
            $start_date= date('Y-m-d',strtotime($split_date[0]));
            $end_date=date('Y-m-d',strtotime($split_date[1]));

            $data['filter_date_rang'] = $this->input->get('date_rang');
		 
		} 

		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status'); 
		}
 	  
		$data['filter_event_status']='';
		if($this->input->get('event_status')){
			$data['filter_event_status']=$this->input->get('event_status'); 
		}
 	 
		 
	 	$data['total_records']=$this->Event_model->getEvent();
	
	 	$data['records_results']=$this->Event_model->getEvent($page,ADMIN_LIMIT);
 		// echo "<pre>";print_r($this->db->last_query());die;
 		// echo "<pre>";print_r($data['records_results']);die;
	 	
		$data['pagination']=$this->Common_model->paginate(site_url('admin/events/list'),$data['total_records']);

		$data['undeletable_ids'] = array(); 
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/event_list');	
		$this->load->view('admin/include/footer');
	}
 

	public function view_event($id) 
	{
		
		$this->Common_model->check_login();
		check_permission('55','view','yes');
		if(!$id) {
			redirect('pages/page_not_found');
		}
		$data['title']="View Event| ".SITE_TITLE;
		$data['page_title']="View Event";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'Event List',
			'link' => site_url('admin/events/list')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'View Event',
			'link' => ""
		);	
		
		if(!$data['event']=$this->Event_model->geteventedetails($id)) {
			redirect('pages/page_not_found');
		}
		// echo "<pre>";print_r($this->db->last_query());die;

		$data['event_item']=$this->Event_model->getOrderItem($id);

		 // echo "<pre>";print_r($data['services']);
		 // echo "<pre>";print_r($data['services_item']);die;
		
		$data['form_action']=site_url('admin/service_category/edit/'.$id);
		$data['back_action']=site_url('admin/events/list');

		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/view_event');
		$this->load->view('admin/include/footer'); 
	}

	/***************** Email Templates *******************/
}
