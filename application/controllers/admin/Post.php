<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Post extends CI_Controller {
	public function __construct()
	{
		parent:: __construct();
		$this->load->model(array('admin/Common_model', 'admin/User_model', 'admin/Post_model'));
		$this->load->helper('common_helper');
		$this->load->library('Ajax_pagination');
	}

	public function index()
	{
		$this->Common_model->check_login();
	}

	public function post_list()
	{
		$this->Common_model->check_login();
		
	    check_permission('52','view','yes');
		if(check_permission('52','add')){$data['add_action']='admin/post/add';}
  		if(check_permission('52','edit')){$data['edit_action']='admin/post/edit';}
  		if(check_permission('52','delete')){$data['delete_action']='1';}

		$data['title']="Post List | ".SITE_TITLE;
		$data['page_title']="Post List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Post List',
			'link' => ""
		);

		$data['filter_user_fullname']='';
		if($this->input->get('userfullname')){
			$data['filter_user_fullname']=$this->input->get('userfullname');
		}
		$data['filter_post_number']='';
		if($this->input->get('post_number')){
			$data['filter_post_number']=$this->input->get('post_number');
		}
		$data['filter_post_type']='';
		if($this->input->get('post_type')){
			$data['filter_post_type']=$this->input->get('post_type');
		}
		$data['filter_post_kind']='';
		if($this->input->get('post_kind')){
			$data['filter_post_kind']=$this->input->get('post_kind');
		}

		$data['user_id'] = $this->input->get('user_id');
		 
		if(empty($data['user_id'])){
			$data['action'] = base_url().'admin/post/list';
			$data['back_url'] = '';
		}else{
			$data['action'] = base_url().'admin/user/post?user_id='.$data['user_id'];
			$data['back_url'] = base_url().'admin/user/list';
		}


 

		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}

		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		
		$data['total_records']=$this->Post_model->get_post_list(0,0);
	 	
	 	$records_results=$this->Post_model->get_post_list(ADMIN_LIMIT,$page);
	 	if(!empty($records_results)){
	 		foreach ($records_results as $key => $list) {
	 		 	$data['records_results'][$key] = $list;

	 		 	$report_count = $this->Post_model->getReportusers($list['id'],true);
	 		 	$data['records_results'][$key]['report_count'] = $report_count;

 		 	 	$like_count = $this->Post_model->getLikedusers($list['id'],true,'','',1);
	 		 	$data['records_results'][$key]['like_count'] = $like_count;


 		 	 	$shared_count = $this->Post_model->getPostshared($list['id'],true);
	 		 	$data['records_results'][$key]['sharedcount'] = $shared_count;
	 		}
	 	}
  	// echo "<pre>";print_r($data['records_results']);die;
  		if(empty($data['user_id'])){
			$data['pagination']=$this->Common_model->paginate(site_url('admin/post/list'),$data['total_records']);
		}else{
			$data['pagination']=$this->Common_model->paginate(site_url('admin/user/post'),$data['total_records']);
		}
  		
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/post/post_list');
		$this->load->view('admin/include/footer');
	}

	public function post_details($id) 
    {
        $this->Common_model->check_login();
        check_permission('52','view','yes');
        $data['title']="Post Detail | ".SITE_TITLE;
        $data['page_title']="Post Detail";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'icon'=>'<i class="fa fa-dashboard"></i>',
            'class'=>'',
            'title' => 'Dashboard',
            'link' => site_url('admin/dashboard')
        );
        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'',
            'title' => 'User List',
            'link' => site_url('admin/post/list')
        );
        $data['breadcrumbs'][] = array(
            'icon'=>'',
            'class'=>'active',
            'title' => 'Detail',
            'link' => ""
        );  
        if(!$data['post_detail'] = $this->Common_model->getRecords('post','*',array('id'=>$id),'',true)){
            redirect('pages/page_not_found');
        }


		$report_count = $this->Post_model->getReportusers($id,true);
		$data['report_count'] = $report_count;

		$like_count = $this->Post_model->getLikedusers($id,true);
		$data['like_count'] = $like_count;


		$shared_count = $this->Post_model->getPostshared($id,true);
		$data['sharedcount'] = $shared_count;
 
       
        $data['user_fullname'] = $this->Common_model->getRecords('users','*',array('user_id'=>$data['post_detail']['user_id']),'',true);

        $array=array();

        $comments = $this->n_level_comment($id,0,$array);

        $data['comments_count'] = count($comments); 
        $data['comments'] = $comments; 

        // echo "<pre>";print_r($data['comments']);die;

       $data['images'] = $this->Common_model->getRecords('post_media','*',array('post_id'=>$id,'media_type'=>'Image'),'',false);
       $data['videos'] = $this->Common_model->getRecords('post_media','*',array('post_id'=>$id,'media_type'=>'Video'),'',false);


       $user_id = $this->input->get('user_id');

        // echo $this->db->last_query(); exit;

        // $data['edit_action']=site_url('admin/post/edit/'.$id);
      	if(empty($user_id)){
        	$data['back_action']=site_url('admin/post/list');
      	}else{
      		$data['back_action']=site_url('admin/user/list?user_id='.$user_id);
      	} 

        $this->load->view('admin/include/header',$data);
        $this->load->view('admin/include/sidebar');
        $this->load->view('admin/post/post_detail');
        $this->load->view('admin/include/footer');
    }




    function n_level_comment($post_id,$parent_id=0,&$array){

    		$record = $this->Post_model->getpostcomment($post_id,$parent_id);
    		// echo $this->db->last_query();die;
    		if(!empty($record)){ 
    			foreach ($record as $key => $list) {  
    			 	$array[] = $list;
    			 	
    				 $this->n_level_comment($post_id,$list['id'],$array); 

    			}
    		} 
    		return $array;
    }




    public function post_like_list($id)
	{
		// echo $id;die;
		$this->Common_model->check_login();
		
	    check_permission('52','view','yes');
     
		
		$data['title']="User List | ".SITE_TITLE;
		$data['page_title']="User List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'User List',
			'link' => ""
		);

		$data['action_url'] = base_url().'admin/post/post_like/'.$id;
		$data['back_url'] = base_url().'admin/post/list/';
		$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
		$like=array();

		$data['filter_user_fullname']='';
		if($this->input->get('userfullname')){
			$data['filter_user_fullname']=$this->input->get('userfullname');
		}
		$data['filter_staging_id']='';
		if($this->input->get('staging_id')){
			$data['filter_staging_id']=$this->input->get('staging_id');
		}
		/*
		$data['filter_media_type']='';
		if($this->input->get('media_type')){
			$data['filter_media_type']=$this->input->get('media_type');
		}*/
		
		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}
 
		if(!$data['details'] = $this->Common_model->getRecords('post','*',array('id'=>$id),'',true)){
			redirect('pages/page_not_found');
		}
 
		
		$data['total_records']=$this->Post_model->getLikedusers($id,true);
	 	
	 	$data['records_results']=$this->Post_model->getLikedusers($id,'',ADMIN_LIMIT,$page);
	 	// echo "<pre>";print_r($data['records_results']);die;
		$data['pagination']=$this->Common_model->paginate(site_url('admin/post/post_like/'.$id),$data['total_records'],'seg5');
  		
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/post/post_like_list');
		$this->load->view('admin/include/footer');
	} 


	public function post_shared_list($id)
	{
		// echo $id;die;
		$this->Common_model->check_login();
		
	    check_permission('52','view','yes');
     
		
		$data['title']="User List | ".SITE_TITLE;
		$data['page_title']="User List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'User List',
			'link' => ""
		);

		$data['action_url'] = base_url().'admin/post/post_shared/'.$id;
		$data['back_url'] = base_url().'admin/post/list';
		$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
		$like=array();

		$data['filter_user_fullname']='';
		if($this->input->get('userfullname')){
			$data['filter_user_fullname']=$this->input->get('userfullname');
		}
		$data['filter_staging_id']='';
		if($this->input->get('staging_id')){
			$data['filter_staging_id']=$this->input->get('staging_id');
		}
		/*
		$data['filter_media_type']='';
		if($this->input->get('media_type')){
			$data['filter_media_type']=$this->input->get('media_type');
		}*/
		
		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}
 
		if(!$data['details'] = $this->Common_model->getRecords('post','*',array('id'=>$id),'',true)){
			redirect('pages/page_not_found');
		}

		
		$data['total_records']=$this->Post_model->getPostshared($id,true);
	 	
	 	$data['records_results']=$this->Post_model->getPostshared($id,'',ADMIN_LIMIT,$page);
	 	// echo "<pre>";print_r($data['records_results']);die;
		$data['pagination']=$this->Common_model->paginate(site_url('admin/post/post_shared/'.$id),$data['total_records'],'seg5');
  		
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/post/post_shared_list');
		$this->load->view('admin/include/footer');
	} 


	public function report_port_list($id)
	{
		// echo $id;die;
		$this->Common_model->check_login();
		
	    check_permission('52','view','yes');
     
		
		$data['title']="User List | ".SITE_TITLE;
		$data['page_title']="User List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'User List',
			'link' => ""
		);

		$data['action_url'] = base_url().'admin/post/post_report/'.$id;
		$data['back_url'] = base_url().'admin/post/list';
		$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
		$like=array();


		$data['filter_user_fullname']='';
		if($this->input->get('userfullname')){
			$data['filter_user_fullname']=$this->input->get('userfullname');
		}
		$data['filter_staging_id']='';
		if($this->input->get('staging_id')){
			$data['filter_staging_id']=$this->input->get('staging_id');
		}
		/*
		$data['filter_media_type']='';
		if($this->input->get('media_type')){
			$data['filter_media_type']=$this->input->get('media_type');
		}*/
		
		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}


		if(!$data['details'] = $this->Common_model->getRecords('post','*',array('id'=>$id),'',true)){
			redirect('pages/page_not_found');
		}

		
		$data['total_records']=$this->Post_model->getReportPost($id,true);
	 	
	 	$data['records_results']=$this->Post_model->getReportPost($id,'',ADMIN_LIMIT,$page);
	 	// echo "<pre>";print_r($data['records_results']);die;
		$data['pagination']=$this->Common_model->paginate(site_url('admin/post/post_report/'.$id),$data['total_records'],'seg5');
  		
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/post/post_report_list');
		$this->load->view('admin/include/footer');
	} 


}