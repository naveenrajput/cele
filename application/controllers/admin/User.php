<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	public function __construct()
	{
		parent:: __construct();
		$this->load->model(array('admin/Common_model','admin/User_model','admin/Chat_model'));
		$this->load->helper('common_helper');
		$this->load->library('Ajax_pagination');
	}

	public function index()
	{
		$this->Common_model->check_login();
	}


	public function rolekey_exists($key,$field) {
		$param=explode('-', $field);
		$table=$param[0];
		$col_name=$param[1];
		$type=$param[2];
		if($type=='add') {
			$where_cond = array($col_name=>$key,'is_deleted'=>0);
       		if($this->Common_model->getRecords($table, $col_name, $where_cond, '', true)) {
   			 	$this->form_validation->set_message(__FUNCTION__, 'This %s is already exist.');
       			return false;
       		} else {
       			return true;
       		}
		} else {
			$id_col=$param[3];
			$id_col_val=$param[4];
			$where_cond = array("$id_col!=" =>$id_col_val,$col_name=>$key,'is_deleted'=>0);
			if($this->Common_model->getRecords($table, $col_name, $where_cond, '', true)) {
				$this->form_validation->set_message(__FUNCTION__, 'This %s is already exist.');
				return false;
			} else {
       			return true;
       		}
		}
	}



	public function users_list()
	{
		$this->Common_model->check_login();
		
    	check_permission('13','view','yes');
    	if(check_permission('13','add')){$data['add_action']='admin/user/add';}
  		if(check_permission('13','edit')){$data['edit_action']='admin/user/edit';}
  		if(check_permission('13','delete')){$data['delete_action']='1';}
	  	
		
		$data['title']="User List | ".SITE_TITLE;
		$data['page_title']="User List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'User List',
			'link' => ""
		);

		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
		$like=array();
		$data['filter_fullname']='';
		if($this->input->get('fullname')){
			$data['filter_fullname']=$this->input->get('fullname');
		}
		$data['filter_staging_id']='';
		if($this->input->get('staging_id')){
			$data['filter_staging_id']=$this->input->get('staging_id');
		}
		$data['filter_mobile']='';
		if($this->input->get('mobile')){
			$data['filter_mobile']=$this->input->get('mobile');
		}

		$data['filter_user_type']='';
		if($this->input->get('user_type')){
			$data['filter_user_type']=$this->input->get('user_type');
		}
		
		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}
		
		$data['total_records']=$this->User_model->get_user_list(0,0);
	 	
	 	$data['records_results']=$this->User_model->get_user_list(ADMIN_LIMIT,$page);
	 	
		$data['pagination']=$this->Common_model->paginate(site_url('admin/user/list'),$data['total_records']);
  		
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/user/user_list');
		$this->load->view('admin/include/footer');
	} 

	public function add_user() 
	{
		$this->Common_model->check_login();
	    check_permission('13','add','yes');
		$data['title']="Add User | ".SITE_TITLE;
		$data['page_title']="Add User";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'User List',
			'link' => site_url('admin/user/list')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Add User',
			'link' => ""
		);	
		$data['countries'] = $this->Common_model->getRecords('countries','*','','name asc',false);
		
		if($this->input->post()) {

			$this->form_validation->set_rules('fullname', 'full name', 'trim|required|max_length[100]',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('staging_id', 'login id', 'callback_rolekey_exists[users-staging_id-add]', 'trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('mobile', 'mobile', 'callback_rolekey_exists[users-mobile-add]', 'trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('password', 'password', 'trim|required|min_length[6]|max_length[15]',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('user_type', 'user type', 'trim|required',array('required'=>'Please enter %s'));

			// $this->form_validation->set_rules('status', 'status', 'trim|required',array('required'=>'Please select %s'));
		
			$this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');

			if($this->form_validation->run()==TRUE) 
			{
				$password=base64_encode($this->input->post('password'));
				$insert_data = array(
					'fullname'=> $this->input->post('fullname'),
					'staging_id'=> $this->input->post('staging_id').'@cele.com',
					'mobile'=> $this->input->post('mobile'),
					'password'=> $password,
					'user_type'=> $this->input->post('user_type'),
					'company_name'=> $this->input->post('company_name'),
					'country'=> $this->input->post('country'),
					'state'=> $this->input->post('state'),
					'address'=> $this->input->post('address'),
					'business_no'=> $this->input->post('business_no'),
					'establishment_year'=> $this->input->post('establishment_year'),
					'identification_number'=> $this->input->post('identification_number'),
					'status'=> 'Active',
					'created'=> date("Y-m-d H:i:s"),
					'created_by'=> $this->session->userdata('admin_id')
				);
		 		
				if(!$last_id = $this->Common_model->addEditRecords('users', $insert_data)) { 
                	echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Some error occurred, Please try later .</div>')); exit;               
	            } else { 
	            	$admin_id = $this->session->userdata('admin_id');
					$admin_username = getAdminUsername($admin_id);
	            	$log_msg = $admin_username." has created a user with login ID - '".$this->input->post('staging_id')."'";
		            actionLog('users',$last_id,'add',$log_msg,'Admin',$admin_id);    
	                echo json_encode(array('status'=>1,'message'=>'<div class="alert alert-success">User added successfully.</div>')); exit; 
	            } 
			} else {
                echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Please fill all required fields.</div>')); exit; 
			}
		}
		
		$data['form_action']=site_url('admin/user/add');
		$data['back_action']=site_url('admin/user/list');

		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/user/add_user');
		$this->load->view('admin/include/footer');
	}

	public function edit_user($id) 
	{
		$this->Common_model->check_login();
	    check_permission('13','edit','yes');
		$data['title']="Edit User | ".SITE_TITLE;
		$data['page_title']="Edit User";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'User List',
			'link' => site_url('admin/user/list')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Edit User',
			'link' => ""
		);	
		$data['countries'] = $this->Common_model->getRecords('countries','*','','name asc',false);
		if(!$data['details'] = $this->Common_model->getRecords('users','*',array('user_id'=>$id),'',true)){
			redirect('pages/page_not_found');
		}


		$user_address = $this->Common_model->getRecords('user_address','*',array('user_id'=>$id,'is_deleted'=>0),'status DESC',false); 
		$address  = array();
		if(!empty($user_address)){
			foreach ($user_address as $key => $list) {
					$address[$key]= $list;
					$country = $this->Common_model->getRecords('countries','name',array('id'=>$list['country']),'name asc',true);

					$address[$key]['country_name']= $country['name'];
					$state = $this->Common_model->getRecords('states','name',array('country_id'=>$list['country'], 'id'=>$list['state']), '',true);
					$address[$key]['state_name']= $state['name'];
					$city = $this->Common_model->getRecords('cities','name',array('state_id'=>$list['state'], 'id'=>$list['city']), '',true); 
					$address[$key]['city_name']= $city['name'];  
			}
		}
		$data['address'] = $address;
		// echo "<pre>";print_r($data['details']);die;
		if($this->input->post()) {

			$this->form_validation->set_rules('fullname', 'full name', 'trim|required|max_length[100]',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('staging_id', 'login ID', 'callback_rolekey_exists[users-staging_id-edit-user_id-'.$id.']', 'trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('mobile', 'mobile', 'callback_rolekey_exists[users-mobile-edit-user_id-'.$id.']', 'trim|required',array('required'=>'Please enter %s'));
			// $this->form_validation->set_rules('user_type', 'user type', 'trim|required',array('required'=>'Please enter %s'));

			$this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');

			if($this->form_validation->run()==TRUE) 
			{

				$dob=$this->input->post('dob');
				$insert_data = array(
					// 'staging_id'=> $this->input->post('staging_id').'@cele.com',
					// 'mobile'=> $this->input->post('mobile'),
					// 'user_type'=> $this->input->post('user_type'),
					'fullname'=> $this->input->post('fullname'),
					'company_name'=> $this->input->post('company_name'),
					'dob'=>$dob?date('Y-m-d',strtotime($dob)):'',
		            'about_description'=>$this->input->post('about_description'),
		            'relationship_status'=>$this->input->post('relationship_status'),
					// 'country'=> $this->input->post('country'),
					// 'state'=> $this->input->post('state'),
					'establishment_year'=> $this->input->post('establishment_year'),
					'business_no'=> $this->input->post('business_no'),
					'gender'=> $this->input->post('gender'), 
					'interests'=> $this->input->post('interests'),
					'education'=> $this->input->post('education'),
					'hobbies'=> $this->input->post('hobbies'),
					'hometown'=> $this->input->post('hometown'),  
					'address'=> $this->input->post('address'),
					'document_status'=> $this->input->post('document_status'),
					'status'=> 'Active',
					'modified'=> date("Y-m-d H:i:s"),
					'created_by'=> $this->session->userdata('admin_id')
				);
		 		
				if(!$last_id = $this->Common_model->addEditRecords('users', $insert_data,array('user_id'=>$id))) { 
                	echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Some error occurred, Please try later .</div>')); exit;     

	            } else { 

	            	$country_array = $this->input->post('country');
	            	$state_array = $this->input->post('state');
	            	$city_array = $this->input->post('city');
	            	$address_array = $this->input->post('address');
	            	$id_array = $this->input->post('id_array');



	            	if(!empty($id_array)){
	            		for ($i=0; $i < count($id_array) ; $i++) { 
	            		 	$array = array(
	            		 		 'country'=>$country_array[$i],
	            		 		 'state'=>$state_array[$i],
	            		 		 'city'=>$city_array[$i],
	            		 		 'address'=>$address_array[$i], 
	            		 		 'user_id'=>$id, 
	            		 		);
            		 	 $this->Common_model->addEditRecords('user_address', $array,array('id'=>$id_array[$i]));
 	            		}
	            	}



            		$status_d = $this->input->post('document_status');
					if($data['details']['document_status']!=$status_d && !empty($status_d)){
						// echo'123';die;
					  	$from_email =getNotificationEmail();
			            $to_email =$data['details']['email'];
			            $subject =  WEBSITE_NAME.' :  Document Status Update';
			            $data['name']= $data['details']['fullname'];
			            $data['message']= 'Your <b>'.str_replace('_',' ',$data['details']['document_type']).'</b> document status is '.$status_d.' from admin.';
			            $body = $this->load->view('template/common', $data,TRUE);
			            $this->Common_model->sendEmail($to_email,$subject,$body,$from_email);
			            $notdata['type']='document_status_update';
		              	$this->Common_model->push_notification_send($data['details']['user_id'],$notdata,$data['details']['user_id'],'Document Status Update', $data['message'],'',$this->session->userdata('admin_id'));

					}


	            	$admin_id = $this->session->userdata('admin_id');
					$admin_username = getAdminUsername($admin_id);
	            	$log_msg = $admin_username." has update a user with login ID '".$data['details']['staging_id']."'";
		            actionLog('users',$last_id,'add',$log_msg,'Admin',$admin_id);    
	                echo json_encode(array('status'=>1,'message'=>'<div class="alert alert-success">User updated successfully.</div>')); exit; 
	    //             $this->session->set_flashdata('success', 'User updated successfully.');
					// redirect('admin/user/list');
	            } 
			} else {
                echo json_encode(array('status'=>0,'message'=>'<div class="alert alert-danger">Please fill all required fields.</div>')); exit; 
			}
		}

	

		$data['form_action']=site_url('admin/user/edit/'.$id);
		$data['back_action']=site_url('admin/user/list');

		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/user/edit_user');
		$this->load->view('admin/include/footer');
	}

	public function user_profile($id) 
	{
		$this->Common_model->check_login();
	    check_permission('13','view','yes');
		$data['title']="User Detail | ".SITE_TITLE;
		$data['page_title']="User Detail";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'',
			'title' => 'User List',
			'link' => site_url('admin/user/list')
		);
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Detail',
			'link' => ""
		);	
		if(!$data['details'] = $this->Common_model->getRecords('users','*',array('user_id'=>$id),'',true)){
			redirect('pages/page_not_found');
		}

		$user_address = $this->Common_model->getRecords('user_address','*',array('user_id'=>$id,'is_deleted'=>0),'status DESC',false); 
		$address  = array();
		if(!empty($user_address)){
			foreach ($user_address as $key => $list) {
					$address[$key]= $list;
					$country = $this->Common_model->getRecords('countries','name',array('id'=>$list['country']),'name asc',true);

					$address[$key]['country_name']= $country['name'];
					$state = $this->Common_model->getRecords('states','name',array('country_id'=>$list['country'], 'id'=>$list['state']), '',true);
					$address[$key]['state_name']= $state['name'];
					$city = $this->Common_model->getRecords('cities','name',array('state_id'=>$list['state'], 'id'=>$list['city']), '',true); 
					$address[$key]['city_name']= $city['name']; 

			}
		}
		
		$data['address'] = $address;
		// echo "<pre>";print_r($address);die;

		$data['get_report_post'] = $this->User_model->getReportPost($data['details']['user_id'],true);
		$data['get_like_post'] = $this->User_model->getPostLike($data['details']['user_id'],true);
		$data['get_shared_post'] = $this->User_model->getPostshared($data['details']['user_id'],true);
		$data['get_friends_count'] = $this->Chat_model->getFriendList($data['details']['user_id'],true);
	 	// echo $this->db->last_query();die;

		if(check_permission('13','edit')){$data['edit_action']=site_url('admin/user/edit/'.$id);}
		
		$data['back_action']=site_url('admin/user/list');

		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/user/user_detail');
		$this->load->view('admin/include/footer');
	}



	public function report_port_list($id)
	{
		// echo $id;die;
		$this->Common_model->check_login();
		
	    check_permission('13','view','yes');
     
		
		$data['title']="Post List | ".SITE_TITLE;
		$data['page_title']="Post List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Post List',
			'link' => ""
		);

		$data['action_url'] = base_url().'admin/user/post_report/'.$id;
		$data['back_url'] = base_url().'admin/User/user_profile/'.$id;
		$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
		$like=array();

			$data['filter_user_fullname']='';
		if($this->input->get('userfullname')){
			$data['filter_user_fullname']=$this->input->get('userfullname');
		}
		$data['filter_post_number']='';
		if($this->input->get('post_number')){
			$data['filter_post_number']=$this->input->get('post_number');
		}
		/*
		$data['filter_media_type']='';
		if($this->input->get('media_type')){
			$data['filter_media_type']=$this->input->get('media_type');
		}*/
		
		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}


		if(!$data['details'] = $this->Common_model->getRecords('users','*',array('user_id'=>$id),'',true)){
			redirect('pages/page_not_found');
		}

		
		$data['total_records']=$this->User_model->getReportPost($data['details']['user_id'],true);
	 	
	 	$data['records_results']=$this->User_model->getReportPost($data['details']['user_id'],'',ADMIN_LIMIT,$page);
	 	// echo "<pre>";print_r($data['records_results']);die;
		$data['pagination']=$this->Common_model->paginate(site_url('admin/user/post_report/'.$data['details']['user_id']),$data['total_records'],'seg5');
  		
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/user/post_report_list');
		$this->load->view('admin/include/footer');
	} 


	public function post_like_list($id)
	{
		// echo $id;die;
		$this->Common_model->check_login();
		
	    check_permission('13','view','yes');
     
		
		$data['title']="Post List | ".SITE_TITLE;
		$data['page_title']="Post List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Post List',
			'link' => ""
		);

		$data['action_url'] = base_url().'admin/user/post_like/'.$id;
		$data['back_url'] = base_url().'admin/User/user_profile/'.$id;
		$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
		$like=array();

			$data['filter_user_fullname']='';
		if($this->input->get('userfullname')){
			$data['filter_user_fullname']=$this->input->get('userfullname');
		}
		$data['filter_post_number']='';
		if($this->input->get('post_number')){
			$data['filter_post_number']=$this->input->get('post_number');
		}
		/*
		$data['filter_media_type']='';
		if($this->input->get('media_type')){
			$data['filter_media_type']=$this->input->get('media_type');
		}*/
		
		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}
 
		if(!$data['details'] = $this->Common_model->getRecords('users','*',array('user_id'=>$id),'',true)){
			redirect('pages/page_not_found');
		}

		
		$data['total_records']=$this->User_model->getPostLike($data['details']['user_id'],true);
	 	
	 	$data['records_results']=$this->User_model->getPostLike($data['details']['user_id'],'',ADMIN_LIMIT,$page);
	 	// echo "<pre>";print_r($data['records_results']);die;
		$data['pagination']=$this->Common_model->paginate(site_url('admin/user/post_like/'.$data['details']['user_id']),$data['total_records'],'seg5');
  		
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/user/post_like_list');
		$this->load->view('admin/include/footer');
	} 

	public function post_shared_list($id)
	{
		// echo $id;die;
		$this->Common_model->check_login();
		
	    check_permission('13','view','yes');
     
		
		$data['title']="Post List | ".SITE_TITLE;
		$data['page_title']="Post List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Post List',
			'link' => ""
		);

		$data['action_url'] = base_url().'admin/user/post_shared/'.$id;
		$data['back_url'] = base_url().'admin/User/user_profile/'.$id;
		$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
		$like=array();

			$data['filter_user_fullname']='';
		if($this->input->get('userfullname')){
			$data['filter_user_fullname']=$this->input->get('userfullname');
		}
		$data['filter_post_number']='';
		if($this->input->get('post_number')){
			$data['filter_post_number']=$this->input->get('post_number');
		}
		/*
		$data['filter_media_type']='';
		if($this->input->get('media_type')){
			$data['filter_media_type']=$this->input->get('media_type');
		}*/
		
		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}
 
		if(!$data['details'] = $this->Common_model->getRecords('users','*',array('user_id'=>$id),'',true)){
			redirect('pages/page_not_found');
		}

		
		$data['total_records']=$this->User_model->getPostshared($data['details']['user_id'],true);
	 	
	 	$data['records_results']=$this->User_model->getPostshared($data['details']['user_id'],'',ADMIN_LIMIT,$page);
	 	// echo "<pre>";print_r($data['records_results']);die;
		$data['pagination']=$this->Common_model->paginate(site_url('admin/user/post_shared/'.$data['details']['user_id']),$data['total_records'],'seg5');
  		
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/user/post_shared_list');
		$this->load->view('admin/include/footer');
	} 



	public function friend_list($id)
	{
		// echo $id;die;
		$this->Common_model->check_login();
		
	    check_permission('13','view','yes');
     
		
		$data['title']="Friend List | ".SITE_TITLE;
		$data['page_title']="Friend List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Friend List',
			'link' => ""
		);

		$data['filter_user_fullname']='';
		if($this->input->get('userfullname')){
			$data['filter_user_fullname']=$this->input->get('userfullname');
		}
		$data['user_id'] = $id;
		$data['action_url'] = base_url().'admin/user/friend/'.$id;
		$data['back_url'] = base_url().'admin/User/user_profile/'.$id;
		$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 0;
		$like=array();

	  
		
		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}
 		if(!$data['details'] = $this->Common_model->getRecords('users','*',array('user_id'=>$id),'',true)){
			redirect('pages/page_not_found');
		}
	
		$data['total_records']=$this->Chat_model->getFriendList($data['details']['user_id'],true);
	 	
	 	$data['records_results']=$this->Chat_model->getFriendList($data['details']['user_id'],'',ADMIN_LIMIT,$page);
	 	// echo "<pre>";print_r($this->db->last_query());die;
	 	// echo "<pre>";print_r($data['records_results']);die;
		$data['pagination']=$this->Common_model->paginate(site_url('admin/user/friend/'.$data['details']['user_id']),$data['total_records'],'seg5');
  		
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/user/friend_list');
		$this->load->view('admin/include/footer');
	} 



	public function chat_history($first_user,$second_user)
	{
		// echo $id;die;
		$this->Common_model->check_login();
		
	    check_permission('13','view','yes');
     
		
		$data['title']="Chat History | ".SITE_TITLE;
		$data['page_title']="Chat History";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Chat History',
			'link' => ""
		);
  
		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status');
		}
  
		$room_id=$this->Chat_model->findRoomId($first_user,$second_user);
		$data['my_user_id'] = $first_user;
	 	if(!empty($room_id['id'])){
			$data['chat_history']=$this->Chat_model->chatHistory($first_user,$room_id['id'],10000000000,0);
	 	}else{
	 		redirect('pages/page_not_found');
	 	}
	 	
	  
	 	// echo "<pre>";print_r($data['chat_history']);die;
	 
  		
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/user/chat_history');
		$this->load->view('admin/include/footer');
	} 


	
	/***************** Email Templates *******************/
}
