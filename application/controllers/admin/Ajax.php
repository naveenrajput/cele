<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {
 

public function __construct()
{
	parent:: __construct();
	$this->load->database();
	$this->load->model(array('admin/Common_model','admin/User_model'));
	$this->load->helper('Common_helper');
}
	public function getTimeZone() {
	   	$timezone=$this->input->post("timezone");
	   	$this->session->set_userdata('admin_timezone',$timezone);
	}
	public function getFrontTimeZone() {
	   	$timezone=$this->input->post("timezone");
	   	$this->session->set_userdata('user_timezone',$timezone);
	}
	//upload_image
	public function image_upload() {
		$config['upload_path'] = 'resources/images/product/';
		$config['allowed_types'] = 'gif|jpg|png';
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		$this->upload->set_allowed_types('*');
		
		$pid=$this->input->post("pid");
		if (!$this->upload->do_upload('img_upload')) 
		{
			echo $this->upload->display_errors();		
		}
		else
		{
			$upload_data=$this->upload->data();
			$product_data=array('img_name' =>$upload_data['file_name'] 
			);
			$this->db->where('product_id',$pid);
			$this->db->update('products',$product_data);
			echo "<img src='".base_url('resources/images/product')."/".$upload_data['file_name']."'  width='200px' height='200px' classs='img-responsive'/>";
		}
	}/* Product Image Upload*/


	/* profile pic upload */
	public function profile_pic_upload() {
		$newFileName = $_FILES['image']['name'];
		$fileExt = pathinfo($newFileName, PATHINFO_EXTENSION);
		// $fileExt = array_pop(explode(".", $newFileName));
		$filename = uniqid(time()).".".$fileExt; 
		//set filename in config for upload

		$config['upload_path'] = 'resources/images/profile/';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$config['file_name'] = $filename;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		// $this->upload->set_allowed_types('*');
		
		$aid=$this->input->post("aid");
		if (!$this->upload->do_upload('image')) 
		{
			echo $this->upload->display_errors();		
		}
		else
		{
			$upload_data=$this->upload->data();
			if($upload_data['file_type']!='image/svg+xml'){
				$config['image_library'] = 'gd2';
				$config['source_image'] = $upload_data['full_path'];
				$config['new_image'] = $filename;
				$config['quality'] = 100;
				$config['maintain_ratio'] = FALSE;
				$config['width']         = 150;
				$config['height']       = 150;

				$this->load->library('image_lib', $config);

				$this->image_lib->resize();
				$this->image_lib->clear();
				//unlink($upload_data['full_path']);
			}
			$this->db->select('profile_pic');
			$this->db->where('admin_id',$aid);
			$q=$this->db->get('admin');
			$r=$q->row();
			if(!empty($r->image)){
				unlink($r->image);
			}

			$update_data=array('profile_pic' =>'resources/images/profile/'.$upload_data['file_name']);
			$this->db->where('admin_id',$aid);
			$this->db->update('admin',$update_data);
			$admin_id = $this->session->userdata('admin_id');
			$admin_username = getAdminUsername($admin_id);
			if($aid == $admin_id){
				$this->session->set_userdata($update_data);
            	$log_msg = $admin_username." updated his/her own profile picture";
			}else{
				$subadminname = getAdminUsername($aid);
				$log_msg = $admin_username." updated profile picture of admin user '".$subadminname."'.";
			}
			actionLog('admin',$aid,'update',$log_msg,'Admin',$admin_id); 
			$res = array('status'=>'1','msg' =>"Image updated successfully.");
			echo json_encode($res); exit;	
		}
	}/* Profile Pic Upload*/

	/* profile pic upload */
	public function profile_pic_user() {
		$newFileName = $_FILES['image']['name'];
		$fileExt = pathinfo($newFileName, PATHINFO_EXTENSION);
		// $fileExt = array_pop(explode(".", $newFileName));
		$filename = uniqid(time()).".".$fileExt;
		//set filename in config for upload

		$config['upload_path'] = 'resources/images/profile/';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$config['file_name'] = $filename;

		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		// $this->upload->set_allowed_types('*');
		
		$aid=$this->input->post("aid");
		if (!$this->upload->do_upload('image')) 
		{
			echo $this->upload->display_errors();		
		}
		else
		{
			$upload_data=$this->upload->data();
			if($upload_data['file_type']!='image/svg+xml'){
				$config['image_library'] = 'gd2';
				$config['source_image'] = $upload_data['full_path'];
				$config['new_image'] = $filename;
				$config['quality'] = 100;
				$config['maintain_ratio'] = FALSE;
				$config['width']         = 150;
				$config['height']       = 150;

				$this->load->library('image_lib', $config);

				$this->image_lib->resize();
				$this->image_lib->clear();
				//unlink($upload_data['full_path']);
			}
			$this->db->select('profile_pic');
			$this->db->where('user_id',$aid);
			$q=$this->db->get('users');
			$r=$q->row();
			if(!empty($r->image)){
				unlink($r->image);
			}

			$update_data=array('profile_pic' =>'resources/images/profile/'.$upload_data['file_name']);
			$this->db->where('user_id',$aid);
			$this->db->update('users',$update_data);
			$this->session->set_userdata($update_data);
			echo "1";
		}
	}
	public function get_states()
    {
    	$states = array();
       	if($_POST['id']) {
       		$states = $this->Common_model->getRecords('states', 'id,name', array('country_id'=>$_POST['id']), '', false);
       	} 
        echo json_encode($states);  exit;
    }
	public function get_cities()
    {
    	$states = array();
       	if($_POST['id']) {
       		$states = $this->Common_model->getRecords('cities', 'id,name', array('state_id'=>$_POST['id']), '', false);
       	} 
        echo json_encode($states);  exit;
    }
	public function check_unique()
    {
       	if(isset($_POST['matched_column']) && isset($_POST['table']) && isset($_POST['matched_value']) ) {
       		if((isset($_POST['id'])) && isset($_POST['matched_id'])) {
       			$field = $_POST['matched_id'];
	       		if($this->Common_model->getRecords($_POST['table'], $_POST['matched_column'], array("$field!=" =>$_POST['id'],$_POST['matched_column']=>$_POST['matched_value'],'is_deleted'=>0), '', true)) {
	       			echo 1;die;
	       		} else {
	       			echo 0;die;
	       		}
	       	} else {
	       		if($this->Common_model->getRecords($_POST['table'], $_POST['matched_column'], array($_POST['matched_column']=>$_POST['matched_value'],'is_deleted'=>0), '', true)) {
	       			echo 1;die;
	       		} else {
	       			echo 0;die;
	       		}
	       	}
       	}
    }
    
    //check username already exists or not
    public function check_username()
    {
       	if($_POST['id'] && $_POST['username']) {
       		if($this->Common_model->getRecords('admin', 'admin_id', array('admin_id!='=>$_POST['id'],'username'=>$_POST['username'],'is_deleted'=>0), '', true)) {
       			echo 1;
       		} else {
       			echo 0;
       		}
       	} 
    }

      public function check_username_user()
    {
       	if($_POST['id'] && $_POST['username']) {
       		if($this->Common_model->getRecords('users', 'user_id', array('user_id!='=>$_POST['id'],'username'=>$_POST['username']), '', true)) {
       			echo 1;
       		} else {
       			echo 0;
       		}
       	} 
    }

  	
    
    //check email already exists or not
    public function check_admin_email()
    {
       	if($_POST['id'] && $_POST['email']) {
       		if($this->Common_model->getRecords('admin', 'admin_id', array('admin_id!='=>$_POST['id'],'email'=>$_POST['email'],'is_deleted'=>0), '', true)) {
       			echo 1;
       		} else {
       			echo 0;
       		}
       	} 
    }
   	
   	public function check_role_name()
    {
       	if($_POST['id'] && $_POST['role_name'] && $_POST['admin_id']) {
       		if($this->Common_model->getRecords('roles', 'role_id', array('role_id!='=>$_POST['id'],'name'=>$_POST['role_name'],'parent_id'=>$_POST['parent_id'],'created_by'=>$_POST['admin_id']), '', true)) {
       			echo 1;
       		} else {
       			echo 0;
       		}
       	} 
    }

    
  
       	
    

    public function check_ssn()
    {
       	if(isset($_POST['matched_column']) && isset($_POST['table']) && isset($_POST['matched_value']) ) {
       		
       		
       		$ssn=get_number_to_albhabet(trim($_POST['matched_value']));
       		if($exist_data=$this->Common_model->getRecords($_POST['table'], 'id', array($_POST['matched_column']=>$ssn), '', true)) {
       			return '1';exit;    
       			
       		} else {
       			
       			$data['ssn']=$_POST['matched_value'];
       			$data['certification_level'] = $this->Common_model->getRecords('user_certification_level','*',array('status'=>'active','is_deleted'=>0),'title asc',false);
       			$data['states'] = $this->Common_model->getRecords('us_states','*','','state_name');
       			$data['form_action']=site_url('admin/user/add_user');
				$data['back_action']=site_url('admin/user/user_list');
       			//return $this->load->view('admin/user/calender',$data);die;
       			return $this->load->view('admin/user/new_user',$data);die;

       		}
	       	
       	}
    }
    public function edit_check_ssn()
    {
   		if(isset($_POST['matched_column']) && isset($_POST['table']) && isset($_POST['matched_value']) ) {
       		if((isset($_POST['id'])) && isset($_POST['matched_id'])) {
       			$ssn=get_number_to_albhabet(trim($_POST['matched_value']));
       			$field = $_POST['matched_id'];
	       		if($this->Common_model->getRecords($_POST['table'], $_POST['matched_column'], array("$field!=" =>$_POST['id'],$_POST['matched_column']=>$ssn), '', true)) {
	       			echo 1;die;
	       		} else {
	       			echo 0;die;
	       		}
	       	} else {
	       		if($this->Common_model->getRecords($_POST['table'], $_POST['matched_column'], array($_POST['matched_column']=>$_POST['matched_value']), '', true)) {
	       			echo 1;die;
	       		} else {
	       			echo 0;die;
	       		}
	       	}
       	}
    }

    //check email already exists or not
    public function check_user_email()
    {   
    	if($_POST['id'] && $_POST['email']) {
            echo  user_email($_POST['id'],$_POST['email']);
       }	 
    }


   

    public function change_status(){
		if($this->input->post()) 
		{
			$field = $this->input->post('field'); 
			$id = $this->input->post('id'); 
			$table_name = $this->input->post('table_name');
			$where = array($field=> $id);
			$date = date("Y-m-d H:i:s");
			// for alleast one ad check 
			
			$col='status';
			
			//$status = $this->Common_model->getFieldValue($table_name,$col,$where);
		
			if($status = $this->Common_model->getFieldValue($table_name,$col,$where)) {
				$new_status = "Active";
				if($status=="Active") {
					$new_status = "Inactive";
				}
				$admin_id = $this->session->userdata('admin_id');
				$admin_username = getAdminUsername($admin_id);

				if($this->Common_model->addEditRecords($table_name,array($col=>$new_status,'modified'=>$date,'modified_by'=>$this->session->userdata('admin_id')),$where))
				{
					$data = array('msg'=>'success','status' =>$new_status);
					if($table_name=='roles'){
						$name = $this->Common_model->getFieldValue($table_name,'name',array($field =>$id));
						$log_msg = $admin_username ." updated status from ".$status." to ".$new_status." of role  '" .$name."'";
						actionLog($table_name,$id,'update',$log_msg,'Admin',$admin_id); 
					}
					if($table_name=='admin'){
						$name = $this->Common_model->getFieldValue($table_name,'username',array($field =>$id));
						$log_msg = $admin_username ." updated status from ".$status." to ".$new_status." of admin  '" .$name."'";
						actionLog($table_name,$id,'update',$log_msg,'Admin',$admin_id); 
					}
					if($table_name=='services'){
						$name = $this->Common_model->getFieldValue($table_name,'service_title',array($field =>$id));
						$log_msg = $admin_username ." updated status from ".$status." to ".$new_status." of service  '" .$name."'";
						actionLog($table_name,$id,'update',$log_msg,'Admin',$admin_id); 
					}
					if($table_name=='offer'){
						$name = $this->Common_model->getFieldValue($table_name,'offer_name',array($field =>$id));
						$log_msg = $admin_username ." updated status from ".$status." to ".$new_status." of offer  '" .$name."'";
						actionLog($table_name,$id,'update',$log_msg,'Admin',$admin_id); 
					}
					if($table_name=='services_item'){
						$name = $this->Common_model->getFieldValue($table_name,'item_name',array($field =>$id));
						$log_msg = $admin_username ." updated status from ".$status." to ".$new_status." of service item  '" .$name."'";
						actionLog($table_name,$id,'update',$log_msg,'Admin',$admin_id); 
					}
					if($table_name=='measurement_unit'){
						$name = $this->Common_model->getFieldValue($table_name,'title',array($field =>$id));
						$log_msg = $admin_username ." updated status from ".$status." to ".$new_status." of  measurement unit '" .$name."'";
						actionLog($table_name,$id,'update',$log_msg,'Admin',$admin_id); 
					}
					if($table_name=='service_category'){
						$name = $this->Common_model->getFieldValue($table_name,'title',array($field =>$id));
						$log_msg = $admin_username ." updated status from ".$status." to ".$new_status." of  service category '" .$name."'";
						actionLog($table_name,$id,'update',$log_msg,'Admin',$admin_id); 
					}
					if($table_name=='reviews'){
						$name = $this->Common_model->getFieldValue($table_name,'event_id',array($field =>$id));
						$event_num='E'.str_pad($name,8,0,STR_PAD_LEFT);
						$log_msg = $admin_username ." updated status from ".$status." to ".$new_status." of  rating for event '" .$event_num."'";
						actionLog($table_name,$id,'update',$log_msg,'Admin',$admin_id); 
					}
					if($table_name=='subscriptions'){
						$name = $this->Common_model->getFieldValue($table_name,'title',array($field =>$id));
						$log_msg = $admin_username ." updated status from ".$status." to ".$new_status." of  subscription '" .$name."'";
						actionLog($table_name,$id,'update',$log_msg,'Admin',$admin_id); 
					}
									
				} else {
					$data = array('msg'=>'fail','admin_approve' =>$admin_approve);
				}
				echo json_encode($data);
			}
		}
	}

	public function change_user_status(){
		if($this->input->post()) 
		{
			$field = $this->input->post('field'); 
			$id = $this->input->post('id'); 
			$table_name = $this->input->post('table_name');
			$where = array($field=> $id);
			$date = date("Y-m-d H:i:s");
			// for alleast one ad check 
			
			$col='status';
			
			//$status = $this->Common_model->getFieldValue($table_name,$col,$where);
			$admin_id = $this->session->userdata('admin_id');
			$admin_username = getAdminUsername($admin_id);
			if($status = $this->Common_model->getFieldValue($table_name,$col,$where)) {
				if($status=='Unverified' || $status=="Inactive"){
					$new_status = "Active";
				}else if($status=='Active'){
					$new_status = "Inactive";
				}
				if($table_name=='users'){
					$name = $this->Common_model->getFieldValue($table_name,'staging_id',array($field =>$id));
					$log_msg = $admin_username ." updated status from ".$status." to ".$new_status." of user  '" .$name."'";
					actionLog($table_name,$id,'update',$log_msg,'Admin',$admin_id);
				}
				if($table_name=='post'){
					$name = $this->Common_model->getFieldValue($table_name,'post_number',array($field =>$id));
					$log_msg = $admin_username ." updated status from ".$status." to ".$new_status." of post  '" .$name."'";
					actionLog($table_name,$id,'update',$log_msg,'Admin',$admin_id); 
				}
				if($this->Common_model->addEditRecords($table_name,array($col=>$new_status,'modified'=>$date,'modified_by'=>$this->session->userdata('admin_id')),$where))
				{
					$data = array('msg'=>'success','status' =>$new_status);
				
				} else {
					$data = array('msg'=>'fail','admin_approve' =>$admin_approve);
				}
				echo json_encode($data);
			}
		}
	}

	

   

	public function delete_record()
	{ 
		if($this->input->post()) 
		{

	        $field = $this->input->post('field'); 
		    $id = $this->input->post('id'); 
			$table = $this->input->post('table_name');
			$where = array($field=> $id);
			if($table=='roles'){
		    	$admin_id = $this->session->userdata('admin_id');
				$admin_username = getAdminUsername($admin_id);
				$name = $this->Common_model->getFieldValue($table,'name',array($field =>$id));
				$log_msg = $admin_username ." deleted role name '" .$name."'";
				
			}
		    if($delete_gallery=$this->Common_model->deleteRecords($table, $where)){
		    	actionLog('roles',$id,'delete',$log_msg,'Admin',$admin_id); 
		    }
	    }
	}

	public function delete_records()
	{
		if($_POST) {
			$id=$this->input->post('id');
			$table=$this->input->post('table');
			$field=$this->input->post('field');
			$where=array($field=>$id);
				if($table=='banners') 
				{
					$count = $this->Common_model->getNumRecords($table,'banner_id',array('status'=>'Active'));
					if($count<=1)
					{
						$this->session->set_flashdata('error', 'Unable to delete last record.');
						$data = array('msg'=>'Unable to delete last record.','status'=>false);
						echo json_encode($data);  exit;
					}
					
				}

				$data=array();
				$record = $this->Common_model->getRecords($table, '*', array($field =>$id), '', true);
				if($this->Common_model->deleteRecords($table,$where))
				{	
				//To Remove Image from folder after deleting the respective record
					if(isset($record['image']) && !empty($record['image'])) {
						if(file_exists($record['image'])) {
							unlink($record['image']);
						}
				}
				//To Delete Comments associated with Blogs and Style Guide 
				
				$this->session->set_flashdata('success', 'Record deleted successfully.');
				$data = array('msg'=>'Record deleted successfully.','status'=>true);
		} 
		echo json_encode($data);  exit;
		} 

	}
	public function delete_front_records()
	{
		if($_POST) {
			$id=$this->input->post('id');
			$table=$this->input->post('table');
			$field=$this->input->post('field');
			$where=array($field=>$id);
				if($table=='banners') 
				{
					$count = $this->Common_model->getNumRecords($table,'banner_id',array('status'=>'Active'));
					if($count<=1)
					{
						$this->session->set_flashdata('error', 'Unable to delete last record.');
						$data = array('msg'=>'Unable to delete last record.','status'=>false);
						echo json_encode($data);  exit;
					}
					
				}

				$data=array();
				$record = $this->Common_model->getRecords($table, '*', array($field =>$id), '', true);
				if($this->Common_model->deleteRecords($table,$where))
				{	
				//To Remove Image from folder after deleting the respective record
					if(isset($record['image']) && !empty($record['image'])) {
						if(file_exists($record['image'])) {
							unlink($record['image']);
						}
				}
				//To Delete Comments associated with Blogs and Style Guide 
				
				$this->session->set_flashdata('success', 'Record deleted successfully.');
				$data = array('msg'=>'Record deleted successfully.','status'=>true);
		} 
		echo json_encode($data);  exit;
		} 

	}


	

	/* End of file ajax_controller.php */
	/* Location: ./application/controllers/ajax_controller.php */


  
    /////////////////////////////////////////////////////
    public function soft_delete_record()
	{
		if($_POST) {
			
			$id=$this->input->post('id');
			$table=$this->input->post('table');
			$field=$this->input->post('field');
			$where=array($field=>$id);

			$data=array();
			$record = $this->Common_model->getRecords($table, '*', array($field =>$id), '', true);
			//echo '<pre>';print_r($record);exit;
			$update_data=array('is_deleted'=>1);
			$this->db->where($where,$field);
			
			if($this->db->update($table,$update_data)) {
				
				//To Remove Image from folder after deleting the respective record
				if(isset($record['image']) && !empty($record['image'])) {
					if(file_exists($record['image'])) {
						unlink($record['image']);
					}
				}
				//To Delete Comments associated with Blogs and Style Guide 
				$admin_id = $this->session->userdata('admin_id');
				$admin_username = getAdminUsername($admin_id);
				if($table=='admin'){
					$name = $this->Common_model->getFieldValue($table,'username',array($field =>$id));
					$log_msg = $admin_username ." deleted account of admin user '" .$name."'";
					actionLog($table,$id,'delete',$log_msg,'Admin',$admin_id); 
				}
				if($table=='role'){
					$name = $this->Common_model->getFieldValue($table,'username',array($field =>$id));
					$log_msg = $admin_username ." deleted role '" .$name."'";
					actionLog($table,$id,'delete',$log_msg,'Admin',$admin_id); 
				}
				if($table=='post'){
					$name = $this->Common_model->getFieldValue($table,'post_number',array($field =>$id));
					$log_msg = $admin_username ." deleted post '" .$name."'";
					actionLog($table,$id,'delete',$log_msg,'Admin',$admin_id); 
				}
				if($table=='services'){
					$name = $this->Common_model->getFieldValue($table,'service_title',array($field =>$id));
					$log_msg = $admin_username ." deleted service '" .$name."'";
					actionLog($table,$id,'delete',$log_msg,'Admin',$admin_id); 
				}
				if($table=='measurement_unit'){
					$name = $this->Common_model->getFieldValue($table,'title',array($field =>$id));
					$log_msg = $admin_username ." deleted measurement unit '" .$name."'";
					actionLog($table,$id,'delete',$log_msg,'Admin',$admin_id); 
				}
				if($table=='service_category'){
					$name = $this->Common_model->getFieldValue($table,'title',array($field =>$id));
					$log_msg = $admin_username ." deleted service category '" .$name."'";
					actionLog($table,$id,'delete',$log_msg,'Admin',$admin_id); 
				}
				if($table=='reviews'){
					$name = $this->Common_model->getFieldValue($table,'event_id',array($field =>$id));
					$event_num='E'.str_pad($name,8,0,STR_PAD_LEFT);
					$log_msg = $admin_username ." deleted rating for event '" .$event_num."'";
					actionLog($table,$id,'delete',$log_msg,'Admin',$admin_id); 
				}
				if($table=='subscriptions'){
					$name = $this->Common_model->getFieldValue($table,'title',array($field =>$id));
					$log_msg = $admin_username ." deleted subscription '" .$name."'";
					actionLog($table,$id,'delete',$log_msg,'Admin',$admin_id); 
				}
				$this->session->set_flashdata('success', 'Record deleted successfully.');
				$data = array('msg'=>'Record deleted successfully.','status'=>true);
			} 
			echo json_encode($data);  exit;
		} 
	} 
	public function front_soft_delete_record()
	{
		if($_POST) {
			$id=$this->input->post('id');
			$table=$this->input->post('table');
			$field=$this->input->post('field');
			$where=array($field=>base64_decode($id));

			$data=array();
			$record = $this->Common_model->getRecords($table, '*', array($field =>base64_decode($id)), '', true);
			//echo $this->db->last_query();exit;
			//echo '<pre>';print_r($record);
			$update_data=array('is_deleted'=>1,'modified'=>date('Y-m-d H:i:s'));
			$user_id = $this->session->userdata('user_id');
            $frontend_username = getFrontUsername($user_id);
			if($this->Common_model->addEditRecords($table,$update_data,$where)) {
				if($table=='services' || $table=='offer'){
					if(isset($record['img1']) && !empty($record['img1'])) {
						if(file_exists($record['img1'])) {
							unlink($record['img1']);
						}
						if(isset($record['img2']) && !empty($record['img2'])) {
							if(file_exists($record['img2'])) {
								unlink($record['img2']);
							}
						}
						if(isset($record['img3']) && !empty($record['img3'])) {
							if(file_exists($record['img3'])) {
								unlink($record['img3']);
							}
						}
					}
					if($table=='services'){
						$name = $this->Common_model->getFieldValue($table,'service_title',array($field =>base64_decode($id)));
						$log_msg = getUserType()." ".$frontend_username." deleted service '".$name."'.";
                    	actionLog($table,base64_decode($id),'delete',$log_msg,'User',$user_id);
					}
					if($table=='offer'){
						$name = $this->Common_model->getFieldValue($table,'offer_name',array($field =>base64_decode($id)));
						$log_msg = getUserType()." ".$frontend_username." deleted offer '".$name."'.";
                    	actionLog($table,base64_decode($id),'delete',$log_msg,'User',$user_id);
					}

				}else{
					if(isset($record['image']) && !empty($record['image'])) {
						if(file_exists($record['image'])) {
							unlink($record['image']);
						}
					}
				}
				display_output('1','Record deleted successfully.');
			}else{
				display_output('0','Some error occured.');
			} 
			
		} 
	} 
	public function front_hard_delete_record()
	{
		if($_POST) {
			$id=$this->input->post('id');
			$table=$this->input->post('table');
			$field=$this->input->post('field');
			$where=array($field=>base64_decode($id));

			$data=array();
			$record = $this->Common_model->getRecords($table, '*', array($field =>base64_decode($id)), '', true);
			//echo $this->db->last_query();exit;
			//echo '<pre>';print_r($record);
			
			
			if($this->Common_model->deleteRecords($table, $where)) {
				if($table=='services' || $table=='offer'){
					if(isset($record['img1']) && !empty($record['img1'])) {
						if(file_exists($record['img1'])) {
							unlink($record['img1']);
						}
						if(isset($record['img2']) && !empty($record['img2'])) {
							if(file_exists($record['img2'])) {
								unlink($record['img2']);
							}
						}
						if(isset($record['img3']) && !empty($record['img3'])) {
							if(file_exists($record['img3'])) {
								unlink($record['img3']);
							}
						}
					}
				}else{
					if(isset($record['image']) && !empty($record['image'])) {
						if(file_exists($record['image'])) {
							unlink($record['image']);
						}
					}
				}
				display_output('1','Record deleted successfully.');
			}else{
				display_output('0','Some error occured.');
			} 
			
		} 
	} 
	
	public function sort_delete_record()
	{
		if($_POST) {
			
			$id=$this->input->post('id');
			$table=$this->input->post('table');
			$field=$this->input->post('field');
			$where=array($field=>$id);
			
			
			$update_data=array('is_deleted'=>1,'deleted_by'=>$this->session->userdata('admin_id'));
			$this->db->where($where,$field);
			
			if($this->db->update($table,$update_data)) {
				if($record = $this->Common_model->getRecords($table, 'sort_order,id', 
					array("$field >"  =>$id), '', false))
				{
					
					foreach($record as $list)
					{
						$update_data=array(
							'sort_order' => $list['sort_order']-1,
							'modified' => date("Y-m-d H:i:s"),
							'modified_by' => $this->session->userdata('admin_id')
							);
						$this->db->where($field,$list['id']);
						$this->db->update($table,$update_data);
					
					}
				}
				
				$this->session->set_flashdata('success', 'Record deleted successfully.');
				$data = array('msg'=>'Record deleted successfully.','status'=>true);
			} 
			echo json_encode($data);  exit;
		} 
	} 

	public function common_update_image() {
		$newFileName = $_FILES['image']['name'];
		$fileExt = pathinfo($newFileName, PATHINFO_EXTENSION);
		// $fileExt = array_pop(explode(".", $newFileName));
		$filename = uniqid(time()).".".$fileExt;
		//post data
		$record_id=$this->input->post("record_id");
		$table=$this->input->post("table");
		$where=$this->input->post("where");
		$select=$this->input->post("select");
		$upload_path=$this->input->post("upload_path");
		$height=$this->input->post("height");
		$width=$this->input->post("width");
		//die($record_id);
		//set filename in config for upload
		//$height=300;
		//$width=300;
		if($this->input->post("height")){
			$height=$this->input->post("height");
			//echo $height;exit;
		}
		if($this->input->post("width")){
			$width=$this->input->post("width");
		}
		// echo $width;
		//	echo $height;
		// echo "<pre>"; print_r($_POST); exit;
		$config['upload_path'] = $upload_path;
		$config['allowed_types'] = '*';
		// $config['file_name'] = 'orignal_'.$filename;
		$config['file_name'] = $filename;


		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		// $this->upload->set_allowed_types('*');
		
		
		if (!$this->upload->do_upload('image')) 
		{
			$msg = $this->upload->display_errors();	
			$res = array('status'=>'0','msg' =>$msg);
			echo json_encode($res); exit;	
		}
		else
		{
			$upload_data=$this->upload->data();
			if($upload_data['file_type']!='image/svg+xml'){
				$config['image_library'] = 'gd2';
				$config['source_image'] = $upload_data['full_path'];
				$config['new_image'] = $filename;
				$config['quality'] = 100;
				$config['maintain_ratio'] = true;
				$config['width']         = $width;
				$config['height']       = $height;
				// echo "<pre>"; print_r($config); exit;
				$this->load->library('image_lib',$config);
				// $this->upload->initialize($config);
				$this->image_lib->resize();
				$this->image_lib->clear();
				//unlink($upload_data['full_path']);
			}

			$this->db->select($select);
			$this->db->where($where,$record_id);
			$q=$this->db->get($table);
			$r=$q->row();
			if(!empty($r->$select)){
				if(file_exists($r->$select))
					unlink($r->$select);
			}

			$update_data=array($select =>$upload_path.$filename);
			$this->db->where($where,$record_id);
			$this->db->update($table,$update_data);
			//echo $this->db->last_query();exit;
			//$this->session->set_userdata($update_data);
			
			$res = array('status'=>'1','msg' =>"Image updated successfully.");
			echo json_encode($res); exit;	
		}
	}/* Profile Pic Upload*/

	

    public function get_lat_long()
    {
       	if($_POST['address']) {
		    $address = str_replace(" ", "+", $_POST['address']);

		    //$json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$address&key=".GOOGLE_API_KEY_LATLONG."&components=country:US&sensor=false");
		    $Url ="https://maps.google.com/maps/api/geocode/json?address=$address&key=".GOOGLE_API_KEY_LATLONG."&components=country:US&sensor=false";
		    $ch = curl_init();
		    curl_setopt($ch, CURLOPT_URL, $Url);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    $json = curl_exec($ch);
		    curl_close($ch);
		    $json = json_decode($json);
		    if ($json->{'status'}=='OK') {
			    $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
			    $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
		    } else {
			    $lat = '';
			    $long = '';
		    }

		    if (!empty($lat) && !empty($long)) {
				$res = array('status'=>'1','lat' =>$lat,'long' =>$long);
		    }else{
				$res = array('status'=>'0');
		    }
		    // echo "<pre>";
		    // print_r ($res);
		    // echo "</pre>";exit();
			echo json_encode($res); exit;
       	} 
    }

   
 
 }
?>