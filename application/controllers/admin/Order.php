<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order extends CI_Controller {
	public function __construct()
	{
		parent:: __construct();
		$this->load->model(array('admin/Common_model','admin/Order_model'));
		$this->load->helper('common_helper');
		$this->load->library('Ajax_pagination');
		 $this->load->library('PHPExcel');
	}

	public function index()
	{
		$this->Common_model->check_login();
	}
 

	public function user_subscription_list() {
		$this->Common_model->check_login();
		check_permission('59','view','yes');
		$data['title']="User Subscription List | ".SITE_TITLE;
		$data['page_title']="User Subscription ";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'User Subscription List',
			'link' => ""
		);
		$page = $this->uri->segment(4) ? $this->uri->segment(4) : 0;
		 
		$data['type'] = 1;

		if($this->input->get('user_id')){
			$data['reset_action']=site_url('admin/user/subscription?user_id='.$this->input->get('user_id'));
		}else{
			$data['reset_action']=site_url('admin/payment/subscription');
		}	
 
 
	
		$data['filter_title']='';
		if($this->input->get('title')){
			$data['filter_title'] = trim($this->input->get('title')); 
		}
		$data['filter_username']='';
		if($this->input->get('username')){
			$data['filter_username'] = trim($this->input->get('username')); 
		}
		$data['filter_user_name']='';
		if($this->input->get('user_name')){
			$data['filter_user_name'] = trim($this->input->get('user_name')); 
		}
		 
		$data['filter_date_rang']='';
		if($this->input->get('date_rang')){
		 	$date_rang=trim($this->input->get('date_rang'));
            $split_date=explode(" - ",$date_rang);
            $start_date= date('Y-m-d',strtotime($split_date[0]));
            $end_date=date('Y-m-d',strtotime($split_date[1])); 
            $data['filter_date_rang'] = $this->input->get('date_rang');  
		} 

	
		 
	 	$data['total_records']=$this->Order_model->getUsreSubscription();
	
	 	$data['records_results']=$this->Order_model->getUsreSubscription($page,ADMIN_LIMIT);
 		// echo "<pre>";print_r($data['records_results']);die;
 		// echo "<pre>";print_r($data['records_results']);die;
	 	if($this->input->get('user_id')){
			$data['pagination']=$this->Common_model->paginate(site_url('admin/user/subscription?user_id='.$this->input->get('user_id')),$data['total_records']);
		}else{
			$data['pagination']=$this->Common_model->paginate(site_url('admin/payment/subscription'),$data['total_records']);
		}

		$data['undeletable_ids'] = array(); 
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/user_subscription_list');	
		$this->load->view('admin/include/footer');
	}

 	public function payment_list() {
		$this->Common_model->check_login();
		check_permission('58','view','yes');
		$data['title']="Payment List | ".SITE_TITLE;
		$data['page_title']="Payment List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Payment List',
			'link' => ""
		);
		$page = $this->uri->segment(4) ? $this->uri->segment(4) : 0;
		 
		$data['type'] = 1;
		$data['reset_action']=site_url('admin/payment/list');
 
 
		$data['filter_order_number']='';
		if($this->input->get('order_number')){
			$data['filter_order_number'] = trim($this->input->get('order_number')); 
		}
		$data['filter_sender_name']='';
		if($this->input->get('sender_name')){
			$data['filter_sender_name'] = trim($this->input->get('sender_name')); 
		}
		$data['filter_receiver_name']='';
		if($this->input->get('receiver_name')){
			$data['filter_receiver_name'] = trim($this->input->get('receiver_name')); 
		}
		$data['filter_payment_type']='';
		if($this->input->get('order_total_amount')){
			$data['filter_order_total_amount'] = trim($this->input->get('payment_type')); 
		}
		$data['filter_payment_type']='';
		if($this->input->get('payment_type')){
			$data['filter_payment_type'] = trim($this->input->get('payment_type')); 
		}

		$data['filter_date_rang']='';
		if($this->input->get('date_rang')){
		 	$date_rang=trim($this->input->get('date_rang'));
            $split_date=explode(" - ",$date_rang);
            $start_date= date('Y-m-d',strtotime($split_date[0]));
            $end_date=date('Y-m-d',strtotime($split_date[1])); 
            $data['filter_date_rang'] = $this->input->get('date_rang');  
		} 

		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status'); 
		}
 	 
		 
	 	$data['total_records']=$this->Order_model->getPaymentHistory();
	
	 	$data['records_results']=$this->Order_model->getPaymentHistory($page,ADMIN_LIMIT);
 		// echo "<pre>";print_r($data['records_results']);die;
 		// echo "<pre>";print_r($data['records_results']);die;
	 	
		//$data['pagination']=$this->Common_model->paginate(site_url('admin/orders/list'),$data['total_records']);
		$data['pagination']=$this->Common_model->paginate(site_url('admin/payment/list'),$data['total_records']);

		$data['undeletable_ids'] = array(); 
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/payment_list');	
		$this->load->view('admin/include/footer');
	}

	public function refund_list() {
		$this->Common_model->check_login();
		check_permission('61','view','yes');
		$data['title']="Payment Refund List | ".SITE_TITLE;
		$data['page_title']="Payment Refund List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Payment Refund List',
			'link' => ""
		);
		$page = $this->uri->segment(4) ? $this->uri->segment(4) : 0;
		 
		$data['type'] = 1;
		$data['reset_action']=site_url('admin/payment/refund');
 
 
		$data['filter_order_number']='';
		if($this->input->get('order_number')){
			$data['filter_order_number'] = trim($this->input->get('order_number')); 
		}
		$data['filter_sender_name']='';
		if($this->input->get('sender_name')){
			$data['filter_sender_name'] = trim($this->input->get('sender_name')); 
		}
		$data['filter_receiver_name']='';
		if($this->input->get('receiver_name')){
			$data['filter_receiver_name'] = trim($this->input->get('receiver_name')); 
		}
		$data['filter_payment_type']='';
		if($this->input->get('order_total_amount')){
			$data['filter_order_total_amount'] = trim($this->input->get('payment_type')); 
		}
		$data['filter_payment_type']='';
		if($this->input->get('payment_type')){
			$data['filter_payment_type'] = trim($this->input->get('payment_type')); 
		}

		$data['filter_date_rang']='';
		if($this->input->get('date_rang')){
		 	$date_rang=trim($this->input->get('date_rang'));
            $split_date=explode(" - ",$date_rang);
            $start_date= date('Y-m-d',strtotime($split_date[0]));
            $end_date=date('Y-m-d',strtotime($split_date[1])); 
            $data['filter_date_rang'] = $this->input->get('date_rang');  
		} 

		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status'); 
		}
 	 
		 
	 	$data['total_records']=$this->Order_model->getPaymentRefundHistory();
	
	 	$data['records_results']=$this->Order_model->getPaymentRefundHistory($page,ADMIN_LIMIT);
 		//echo "<pre>";print_r($data['records_results']);die;
 		// echo "<pre>";print_r($data['records_results']);die;
	 	
		$data['pagination']=$this->Common_model->paginate(site_url('admin/payment/refund'),$data['total_records']);

		$data['undeletable_ids'] = array(); 
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/payment_refund_list');	
		$this->load->view('admin/include/footer');
	}
 

	public function report_excel() {

		$this->Common_model->check_login();
		check_permission('67','view','yes');
		$data['title']="Payment Transaction Details | ".SITE_TITLE;
		$data['page_title']="Payment Transaction Details";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Payment Transaction Details',
			'link' => ""
		);
		$page = $this->uri->segment(4) ? $this->uri->segment(4) : 0;
		 
		$data['type'] = 1;
		$data['reset_action']=site_url('admin/report_export/list');
 
 
		$data['filter_order_number']='';
		if($this->input->get('order_number')){
			$data['filter_order_number'] = trim($this->input->get('order_number')); 
		}
		$data['filter_sender_name']='';
		if($this->input->get('sender_name')){
			$data['filter_sender_name'] = trim($this->input->get('sender_name')); 
		}
		$data['filter_receiver_name']='';
		if($this->input->get('receiver_name')){
			$data['filter_receiver_name'] = trim($this->input->get('receiver_name')); 
		}
		$data['filter_payment_type']='';
		if($this->input->get('order_total_amount')){
			$data['filter_order_total_amount'] = trim($this->input->get('payment_type')); 
		}
		$data['filter_payment_type']='';
		if($this->input->get('payment_type')){
			$data['filter_payment_type'] = trim($this->input->get('payment_type')); 
		}
		$data['filter_transfer_type']='';
		if($this->input->get('transfer_type')){
			$data['filter_transfer_type'] = trim($this->input->get('transfer_type')); 
		}

		$data['filter_date_rang']='';
		if($this->input->get('date_rang')){
		 	$date_rang=trim($this->input->get('date_rang'));
            $split_date=explode(" - ",$date_rang);
            $start_date= date('Y-m-d',strtotime($split_date[0]));
            $end_date=date('Y-m-d',strtotime($split_date[1])); 
            $data['filter_date_rang'] = $this->input->get('date_rang');  
		} 

		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status'); 
		}
 	  
	 	$data['total_records']=$this->Order_model->getAllPayment();
	
	 	$data['records_results']=$this->Order_model->getAllPayment($page,ADMIN_LIMIT);
	 	// echo "<pre>";print_r($this->db->last_query());die;
		$data['pagination']=$this->Common_model->paginate(site_url('admin/report_export/list'),$data['total_records']);

		
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/all_payment_list');	
		$this->load->view('admin/include/footer');
	}

 	
	public function export_excel() {
		
		$admin_id = $this->session->userdata('admin_id');
		$admin_username = getAdminUsername($admin_id);
		$log_msg = $admin_username ." exported excel of payment transaction details.";
		actionLog('payment_history',$admin_id,'Update',$log_msg,'Admin',$admin_id);
 
	 	$records_results=$this->Order_model->getAllPayment(0,1000000000000);
	 	// echo "<pre>";print_r($records_results);die;
	 	
	 	$a = 'A';
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        // set Header
        $objPHPExcel->getActiveSheet()->SetCellValue($a++.'1', 'Payment To');
        $objPHPExcel->getActiveSheet()->SetCellValue($a++.'1', 'Payment Status');
        $objPHPExcel->getActiveSheet()->SetCellValue($a++.'1', 'Event Status');
        $objPHPExcel->getActiveSheet()->SetCellValue($a++.'1', 'Transaction Id');
        $objPHPExcel->getActiveSheet()->SetCellValue($a++.'1', 'Order Number');
        $objPHPExcel->getActiveSheet()->SetCellValue($a++.'1', 'Event Number');
        $objPHPExcel->getActiveSheet()->SetCellValue($a++.'1', 'Customer Name');
        $objPHPExcel->getActiveSheet()->SetCellValue($a++.'1', 'Service Provider Name');
        $objPHPExcel->getActiveSheet()->SetCellValue($a++.'1', 'Event Title');
        $objPHPExcel->getActiveSheet()->SetCellValue($a++.'1', 'Event Date');
        $objPHPExcel->getActiveSheet()->SetCellValue($a++.'1', 'Order Total ');
        $objPHPExcel->getActiveSheet()->SetCellValue($a++.'1', 'Paid Amount');
        $objPHPExcel->getActiveSheet()->SetCellValue($a++.'1', 'Admin Commission Amount'); 
        $objPHPExcel->getActiveSheet()->SetCellValue($a++.'1', 'Payment Date'); 
  
        // set Row
        $rowCount = 2;
        if(!empty($records_results)){
	        foreach ($records_results as $key=> $element) {  

		 	 	$stating = 'A'; 
	            $objPHPExcel->getActiveSheet()->SetCellValue($stating++ . $rowCount,str_replace('_',' ',$element['transfer_type']));
	            $objPHPExcel->getActiveSheet()->SetCellValue($stating++ . $rowCount,$element['payment_status']);
	            $objPHPExcel->getActiveSheet()->SetCellValue($stating++ . $rowCount,$element['event_status']);
	            $objPHPExcel->getActiveSheet()->SetCellValue($stating++ . $rowCount,$element['transaction_id']);
	            $objPHPExcel->getActiveSheet()->SetCellValue($stating++ . $rowCount,$element['order_number']);
	            $objPHPExcel->getActiveSheet()->SetCellValue($stating++ . $rowCount,$element['event_number']);
	            $objPHPExcel->getActiveSheet()->SetCellValue($stating++ . $rowCount,$element['sender_fullname']);
	            $objPHPExcel->getActiveSheet()->SetCellValue($stating++ . $rowCount,$element['receiver_fullname']);
	            $objPHPExcel->getActiveSheet()->SetCellValue($stating++ . $rowCount,$element['event_title']); 
	            $objPHPExcel->getActiveSheet()->SetCellValue($stating++ . $rowCount, convertGMTToLocalTimezone($element['event_date'],true)); 
 	//ADMIN_CURRENCY.' '.
	            $objPHPExcel->getActiveSheet()->SetCellValue($stating++ . $rowCount,$element['total_order_amount']); 
	            $objPHPExcel->getActiveSheet()->SetCellValue($stating++ . $rowCount,$element['paid_amount']); 
	            $objPHPExcel->getActiveSheet()->SetCellValue($stating++ . $rowCount,$element['admin_commission_amount']);  
	            $objPHPExcel->getActiveSheet()->SetCellValue($stating++ . $rowCount,convertGMTToLocalTimezone($element['created'],true) ); 
	      
	            $rowCount++;
 
			 
	        }
        }

		$object_writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename='.date('d-m-Y').'_report.xls');
		$object_writer->save('php://output'); 
	}

 
 
	public function orders_list() {
		$this->Common_model->check_login();
		check_permission('56','view','yes');
		$data['title']="Order List | ".SITE_TITLE;
		$data['page_title']="Order List";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);

		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'Order List',
			'link' => ""
		);
		$page = $this->uri->segment(4) ? $this->uri->segment(4) : 0;
		 
		$data['type'] = 1;
		$data['reset_action']=site_url('admin/orders/list');
 
 
		$data['filter_event_title']='';
		if($this->input->get('event_title')){
			$data['filter_event_title'] = trim($this->input->get('event_title')); 
		}
		$data['filter_event_number']='';
		if($this->input->get('event_number')){
			$data['filter_event_number'] = trim($this->input->get('event_number')); 
		}
		$data['filter_order_number']='';
		if($this->input->get('order_number')){
			$data['filter_order_number'] = trim($this->input->get('order_number')); 
		}
		$data['filter_username']='';
		if($this->input->get('username')){
			$data['filter_username'] = trim($this->input->get('username')); 
		}
		$data['filter_order_status']='';
		if($this->input->get('order_status')){
			$data['filter_order_status'] = trim($this->input->get('order_status')); 
		}
		$data['filter_date_rang']='';
		if($this->input->get('date_rang')){
		 	$date_rang=trim($this->input->get('date_rang'));
            $split_date=explode(" - ",$date_rang);
            $start_date= date('Y-m-d',strtotime($split_date[0]));
            $end_date=date('Y-m-d',strtotime($split_date[1]));

            $data['filter_date_rang'] = $this->input->get('date_rang');
		 
		} 

		$data['filter_status']='';
		if($this->input->get('status')){
			$data['filter_status']=$this->input->get('status'); 
		}
 	 

		$data['filter_event_status']='';
		if($this->input->get('event_status')){
			$data['filter_event_status']=$this->input->get('event_status'); 
		}
 	 
		 
	 	$data['total_records']=$this->Order_model->getOrder();
	
	 	$data['records_results']=$this->Order_model->getOrder($page,ADMIN_LIMIT);
 		// echo "<pre>";print_r($this->db->last_query());die;
 		// echo "<pre>";print_r($data['records_results']);die;
	 	
		$data['pagination']=$this->Common_model->paginate(site_url('admin/orders/list'),$data['total_records']);

		$data['undeletable_ids'] = array(); 
		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/order_list');	
		$this->load->view('admin/include/footer');
	}
 


	public function view_order($id) 
	{ 
		$this->Common_model->check_login();
		check_permission('56','view','yes');
		if(!$id) {
			redirect('pages/page_not_found');
		}
		$seg=$this->uri->segment(2);
		$data['title']="View Order| ".SITE_TITLE;
		$data['page_title']="View Order";
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'icon'=>'<i class="fa fa-dashboard"></i>',
			'class'=>'',
			'title' => 'Dashboard',
			'link' => site_url('admin/dashboard')
		);
		if($seg=='payment'){
			$data['breadcrumbs'][] = array(
				'icon'=>'',
				'class'=>'',
				'title' => 'Payment List',
				'link' => site_url('admin/payment/list')
			);
			$data['back_action']=site_url('admin/payment/list');
		}elseif($seg=='payment_refund'){
			$data['breadcrumbs'][] = array(
				'icon'=>'',
				'class'=>'',
				'title' => 'Payment Refund List',
				'link' => site_url('admin/payment/refund')
			);
			$data['back_action']=site_url('admin/payment/refund');
		}else{
			$data['breadcrumbs'][] = array(
				'icon'=>'',
				'class'=>'',
				'title' => 'Order List',
				'link' => site_url('admin/orders/list')
			);
			$data['back_action']=site_url('admin/orders/list');
		}
		
		$data['breadcrumbs'][] = array(
			'icon'=>'',
			'class'=>'active',
			'title' => 'View Order',
			'link' => ""
		);	
		
		if(!$data['order']=$this->Order_model->getOrderdetails($id)) {
			redirect('pages/page_not_found');
		}

		//echo "<pre>";print_r($this->db->last_query());
		//echo '<pre>';print_r($data['order']);die;

		$data['order_item']=$this->Order_model->getOrderItem($id);
 	
 		$data['order_total']=$this->Common_model->getRecords('order_total','*',array('order_id'=>$id,'status'=>'Active'),'id DESC',true);

		$status = $this->Common_model->getRecords('order_status_history','status,payment_id',array('order_id'=>$id,'order_batch_id'=>$data['order_total']['order_batch_id']),'id DESC',true);

		$data['order_total']['status']=$status['status'];
		$data['order_total']['payment_id']=$status['payment_id'];

 		$history_array = array();
 		
 		$data['history_array'] = $history_array;
		$data['form_action']=site_url('admin/service_category/edit/'.$id);
		

		$this->load->view('admin/include/header',$data);
		$this->load->view('admin/include/sidebar');
		$this->load->view('admin/view_order');
		$this->load->view('admin/include/footer'); 
	}





	public function createInvoice($id,$type='print'){
		$this->Common_model->check_login();
		include(APPPATH.'/third_party/mpdf/mpdf.php');
		define('_MPDF_PATH',APPPATH.'/third_party/mpdf/');

		$data['title']="View Order| ".SITE_TITLE;
		if(!$data['order']=$this->Order_model->getPaymentdetails($id)) {
			redirect('pages/page_not_found');
		}
		// echo $this->db->last_query();
		// echo "<pre>";print_r($data['order']);die;
		$data['order_item']=$this->Order_model->getOrderItem($data['order']['order_id']);

		// $data['payment_details']=$this->Common_model->getRecords('order_history_details','*',array('order_id'=>$id,'order_group_id'=>$list['order_group_id'],'status'=>'Inactive'),'id DESC',false);


 		$data['order_total']=$this->Common_model->getRecords('order_total','*',array('order_id'=>$data['order']['order_id'],'status'=>'Active'),'id DESC',true);
 		// echo "<pre>";print_r($data['order']);die;

 	 	$html=$this->load->view('template/order_pdf.php',$data,true);

	 
		$html = mb_convert_encoding($html, 'UTF-8', 'UTF-8');

	 
		$mpdf=new mPDF('','A4','','',20,15,25,25,10,10); 
	 
		$mpdf->SetProtection(array('print'));

		$mpdf->autoScriptToLang = true;
		$mpdf->autoLangToFont = true;
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->WriteHTML($html);
	 
	

        //F for file and D for download
	 //  	$mpdf->Output($pdfFilePath, "F");
		//  echo 	$mpdf->Output(); 
		// exit;
		// echo ""
	   if($type=='print'){
    	 	echo 	$mpdf->Output(); 
			exit;
        }elseif($type=='download'){
        	//F for file and D for download
        	$pdf_name = 'Payment_Invoice_'.$data['order']['payment_id'].".pdf"; 
        	$pdfFilePath = 'resources/invoice/'.$pdf_name;
	  		$mpdf->Output($pdfFilePath, "F");

        }else{
    		$pdf_name = 'Payment_Invoice_'.$data['order']['payment_id'].".pdf"; 
        	$pdfFilePath = 'resources/invoice/'.$pdf_name;
	       return $pdfFilePath = 'resources/pdfs/'.$pdf_name; 
        }

	}






















	/***************** Email Templates *******************/
}
