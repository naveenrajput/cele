<?php defined('BASEPATH') OR exit('No direct script access allowed');
class App extends CI_Controller
{
    function __construct() 
    {
      
        parent::__construct();
        $this->load->model(array('admin/Common_model','App_model'));
        $this->load->helper('common_helper'); 
        $this->load->helper('string');
       // $this->load->library(array('encrypt','PHPExcel'));
        $this->load->library(array('upload','S3'));
        ini_set('display_errors', 0);
        error_reporting(0);

        $h_key= getallheaders();
        if(isset($h_key['Apikey']))
        {
            $h_key['Apikey']=$h_key['Apikey'];
        }
        if(isset($h_key['apikey']))
        {
            $h_key['Apikey']=$h_key['apikey'];
        }
        if(APP_KEY !== $h_key['Apikey'])  //check header key for authorizetion 
        {
            echo json_encode(array('data'=> array('status' =>'0' ,'msg'=>"Error Invalid Api Key")));
            header('HTTP/1.0 401 Unauthorized');
            die;
        }
    }
    public function otp_send($mobile,$otp)
    {

        $message = SITE_TITLE.'- Verification OTP : '.$otp;

        $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.msg91.com/api/sendhttp.php?campaign=attendance&response=json&mobiles=$mobile&authkey=284459AmmGJoNl5d246f3e&route=4&sender=TESTIN&message=$message&country=91",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
           
            return $response;
           
        }
    }

    public function send_sms($mobile,$msg='')
    {

        $message = SITE_TITLE.' - '.$msg. 'Regards,'.SITE_TITLE. 'Team';
        $curl = curl_init();
        curl_setopt_array($curl, array(
           CURLOPT_URL => "https://api.msg91.com/api/sendhttp.php?campaign=attendance&response=json&mobiles=$mobile&authkey=284459AmmGJoNl5d246f3e&route=4&sender=TESTIN&message=$message&country=91",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
           
            return $response;
        }
    }
    public function signup() 
    {
        $fullname               =   $this->clear_input($this->input->post('fullname'));
        $staging_id             =   $this->clear_input($this->input->post('staging_id'));
        $email                  =   $this->clear_input($this->input->post('email'));
        $mobile                 =   $this->clear_input($this->input->post('mobile'));
        $user_type              =   $this->clear_input($this->input->post('user_type'));
        $password               =   $this->clear_input($this->input->post('password'));
        $device_id              =   $this->clear_input($this->input->post('device_id'));
        $device_type            =   $this->clear_input($this->input->post('device_type'));
        $company_name           =   $this->clear_input($this->input->post('company_name'));
        $country                =   $this->clear_input($this->input->post('country'));
        $state                  =   $this->clear_input($this->input->post('state'));
        $address                =   $this->clear_input($this->input->post('address'));
        $business_no       =   $this->clear_input($this->input->post('business_no'));
        $establishment_year     =   $this->clear_input($this->input->post('establishment_year'));
        $document_type          =   $this->clear_input($this->input->post('document_type'));

        $mobile_auth_token      =   $this->App_model->generateRandomString();
        $tableName ="users";
        if(empty($fullname)){
            $this->display_output('0','Please enter fullname.');
        }
        if(empty($user_type)){
            $this->display_output('0','Please enter user type.');
        } 
        if(empty($password)) {
            $this->display_output('0','Please enter password.');
        }
        if(strlen($password) < 6) {
            $this->display_output('0','Password length must be minimum of 6 characters.');
        }
        if(empty($staging_id)){
            $this->display_output('0','Please enter staging id.');
        }
        if(strlen($staging_id) < 6 && strlen($staging_id) > 15) {
            $this->display_output('0','Staging ID length must be minimum of 6 characters and maximum of 15 characters long.');
        } 
        if(!empty($staging_id)){
            $where1 = array('staging_id' => $staging_id,'is_deleted'=>'0');
            if($this->Common_model->getRecords($tableName,'user_id',$where1,'',true)) {
                $this->display_output('0','Staging ID already used.');
            }
        }
        if(empty($email)){
            $this->display_output('0','Please enter email.');
        }
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            $this->display_output('0','Please enter a valid email.');
        }
        if(!empty($email)){
            $where2 = array('email' => $email,'is_deleted'=>'0');
            if($this->Common_model->getRecords($tableName,'user_id',$where2,'',true)) {
                $this->display_output('0','Email already used.');
            }
        }
        if(empty($mobile)) {
            $this->display_output('0','Please enter mobile number.');
        }
        if(!empty($mobile)){
            $where = array('mobile' => $mobile,'is_deleted'=>'0');
            if($this->Common_model->getRecords($tableName,'user_id',$where,'',true)) {
                $this->display_output('0','Mobile number already used.');
            }
        }
        if(empty($device_id)) {
            $this->display_output('0','Please enter device id.');
        }
        if(empty($device_type)) {
            $this->display_output('0','Please enter device type.');
        } else if($device_type !='android' && $device_type !='ios' ){
            $this->display_output('0','Device type must be either android or ios.');
        }
        $password= base64_encode($password);
        $otp = mt_rand(1000,9999);
        $otp ='1234';
        $signup_data = array( 
            'fullname'=>$fullname,
            'staging_id'=>$staging_id,
            'email'=>$email,
            'user_type'=>$user_type,
            'mobile'=>$mobile,
            'status'=>'Unverified',
            'password'=> $password,
            'otp' =>$otp,
            'otp_expired'=>date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))+900),
            'device_type'=>$device_type,
            'device_id'=>$device_id,
            'mobile_auth_token'=> $mobile_auth_token,
            'created'=> date("Y-m-d H:i:s"),
        );
        if($user_type==2 || $user_type==3){
            if(empty($company_name)){
                $this->display_output('0','Please enter company name.');
            }if(empty($country)){
                $this->display_output('0','Please enter country.');
            }if(empty($state)){
                $this->display_output('0','Please enter state.');
            }if(empty($address)){
                $this->display_output('0','Please enter address.');
            }if(empty($latitude)){
                $this->display_output('0','Please enter latitude.');
            }if(empty($longitude)){
                $this->display_output('0','Please enter longitude.');
            }if(empty($establishment_year)){
                $this->display_output('0','Please enter establishment year.');
            }if(empty($document_type)){
                $this->display_output('0','Please enter document type.');
            }
            $filepath="";
            $filerror="";
            if(isset($_FILES['document_proof']) && !empty($_FILES['document_proof']['name'])){
                if($_FILES['document_proof']['error']==0) {
                    $image_path = USER_DOCUMNET_PATH;
                    $allowed_types = '*';
                    $file='document_proof';
                    $height = 150;
                    $width = 150;
                    $responce = commonImageUpload($image_path,$allowed_types,$file,$width,$height);

                    if($responce['status']==0){
                        $upload_error = $responce['msg'];   
                        $filerror="1";
                    } else {
                        $filepath=$responce['image_path'];
                    }
                }
            }
            if($filerror!=''){
                $this->display_output('0',strip_tags($upload_error));
            }
            if(!empty($filepath)){
                $signup_data['document_proof']=$filepath;
            }
            $signup_data['company_name']=$company_name;
            $signup_data['country']=$country;
            $signup_data['state']=$state;
            $signup_data['address']=$address;
            $signup_data['establishment_year']=$establishment_year;
            $signup_data['business_no']=$business_no;
            $signup_data['document_type']=$document_type;
            $signup_data['document_status']='Unverified';
        }
        if($last_id = $this->Common_model->addEditRecords('users',$signup_data)){
            //$this->otp_send($mobile,$otp); 
            $where  = array('user_id' => $last_id);
            $user_number=str_pad($last_id,8,0,STR_PAD_LEFT);
            $this->Common_model->addEditRecords('users',array('user_number'=>$user_number),$where);
            $user_record=$this->Common_model->getRecords('users','user_id,fullname,staging_id,mobile,user_type,status,email,company_name,country,state,address,establishment_year,business_no,document_type,document_proof,document_status',$where,'',true);
            if($user_type==2 || $user_type==3){
                $message='Registration completed & verification OTP sent on your registered mobile number, Please check it. We\'re reviewing your information. Your account should be verified shortly.' ;
            }else{
                 $message='Registration completed & verification OTP sent on your registered mobile number, Please check it.';
            }
            $this->display_output('1',$message,array('details'=>$user_record));
        }else{
            $this->display_output('0','Some error occured! Please try again.');  
        }
    }
    public function verify_otp(){
        $mobile  = $this->clear_input($this->input->post('mobile'));
        $otp  = $this->clear_input($this->input->post('otp'));
        if(empty($mobile)) {
            $this->display_output('0','Please enter mobile number.');
        }
        if(empty($otp)) {
            $this->display_output('0','Please enter otp.');
        }
       
        if($user_data = $this->Common_model->getRecords('users','user_id,fullname,staging_id,mobile,user_type,status,email,company_name,country,state,address,establishment_year,business_no,document_type,document_proof,document_status,otp,otp_expired',array('mobile'=>$mobile,'otp'=>$otp,'is_deleted'=>0),'user_id DESC',true)){
            $current_date = new DateTime("now", new DateTimeZone('UTC'));
            if(strtotime($user_data['otp_expired']) < strtotime($current_date->format('Y-m-d H:i:s'))){
                $this->display_output('0','Your OTP has been expired.'); 
            }
            if($user_data['otp']==$otp){
                $mobile_auth_token= $this->App_model->generateRandomString();
                $otp_data = array( 
                    'otp'=>'',
                    'otp_expired'=>'',
                    'status'=>'Active',
                    'modified'=>date('Y-m-d H:i:s')
                );
                if($this->Common_model->addEditRecords('users',$otp_data,array('mobile'=>$mobile))){
                    $login=array(   
                        'user_id'=>$user_data['user_id'],
                        'fullname'=>$user_data['fullname'],
                        'staging_id'=>$user_data['staging_id'],
                        'mobile'=>$user_data['mobile'],
                        'email'=>$user_data['email'],
                        'user_type'=>$user_data['user_type'],
                        'status'=>'Active',
                        'company_name'=>$user_data['company_name'],
                        'country'=>$user_data['country'],
                        'state'=>$user_data['state'],
                        'address'=>$user_data['address'],
                        'establishment_year'=>$user_data['establishment_year'],
                        'business_no'=>$user_data['business_no'],
                        'document_type'=>$user_data['document_type'],
                        'document_proof'=>$user_data['document_proof'],
                        'document_status'=>$user_data['document_status'],
                        'mobile_auth_token'=>$mobile_auth_token,
                        );
                    $this->display_output('1','OTP verified successfully.',array('details'=>$login)); 
                }else{
                    $this->display_output('0','Some error occured! Please try again.'); 
                }
            }else{
                $this->display_output('0','Wrong OTP ! Please enter correct OTP.');   
            }

        }else{
            $this->display_output('0','Invalid OTP.'); 
        }
    }
    public function login() 
    {
        $mobile       =  $this->clear_input($this->input->post('mobile'));
        $staging_id   =  $this->clear_input($this->input->post('staging_id'));
        $password     =  $this->clear_input($this->input->post('password'));
        $device_id    =  $this->clear_input($this->input->post('device_id'));
        $device_type  =  $this->clear_input($this->input->post('device_type')); 

        if(empty($device_type)) {
            $this->display_output('0','Please enter device type');
        } else if($device_type !='android' && $device_type !='ios') {
            $this->display_output('0','Device type must be either andriod or ios.');
        }
        
        if(empty($device_id)) {
            $this->display_output('0','Device required.');
        }

        if(empty($mobile) && empty($staging_id)) {
            $this->display_output('0','Please enter mobile or staging ID.');
        }
        if(!empty($staging_id)){
            if(empty($password)) {
                $this->display_output('0','Please enter your password.');
            }
        }


        $device_type = strtolower($device_type);
        $password=base64_encode($password);
        if(!empty($staging_id)){
            $where="staging_id='".$staging_id."'";
        }else{
            $where="mobile='".$mobile."'";
        }
        
        if($user_data = $this->Common_model->getRecords('users','user_id,fullname,staging_id,mobile,user_type,status,email,password,company_name,country,state,address,establishment_year,business_no,document_type,document_proof,document_status,is_deleted,mobile_auth_token',$where,'user_id DESC',true)) {
           
            if($user_data['status']=='Inactive')
            { 
                $this->display_output('0','Your account has been deactivated! Please contact administrator for more details.');
            }
            
            if($user_data['is_deleted']==1)
            { 
                $this->display_output('0','Your account has been deleted ! Please contact administrator for more details.');
            }
            if(!empty($staging_id)){
                if($user_data['password']==$password){
                    if($user_data['status']=='Unverified')
                    { 
                        $this->display_output('2','Your account is not verified.');
                    }
                    if($user_data['is_deleted']==0 && $user_data['status']=='Active'){
                        
                        $mobile_auth_token= $this->App_model->generateRandomString();
                        $formData = array('device_id' => $device_id,'device_type' => $device_type,'mobile_auth_token' => $mobile_auth_token); 
                        $this->Common_model->addEditRecords('users',$formData,array('user_id'=>$user_data['user_id']));  

                        $login=array(   
                            'user_id'=>$user_data['user_id'],
                            'fullname'=>$user_data['fullname'],
                            'staging_id'=>$user_data['staging_id'],
                            'mobile'=>$user_data['mobile'],
                            'email'=>$user_data['email'],
                            'user_type'=>$user_data['user_type'],
                            'status'=>$user_data['status'],
                            'company_name'=>$user_data['company_name'],
                            'country'=>$user_data['country'],
                            'state'=>$user_data['state'],
                            'address'=>$user_data['address'],
                            'establishment_year'=>$user_data['establishment_year'],
                            'business_no'=>$user_data['business_no'],
                            'document_type'=>$user_data['document_type'],
                            'document_proof'=>$user_data['document_proof'],
                            'document_status'=>$user_data['document_status'],
                            'mobile_auth_token'=>$mobile_auth_token,
                            );
                       
                        $this->display_output('1','Logged in successful.',array('details'=>$login));
                    }
                    else
                    {
                        $this->display_output('0','Your account has been deactivated!  Please contact administrator for details.');
                    }
                }else{
                    $this->display_output('0','Incorrect Password.');
                }
            }else{
                if($user_data['is_deleted']==0){
                    $otp = mt_rand(1000,9999);
                    $otp ='1234';
                    $mobile_auth_token= $this->App_model->generateRandomString();
                    //$this->otp_send($mobile,$otp); 
                    $formData = array('device_id' => $device_id,'device_type' => $device_type,'mobile_auth_token' => $mobile_auth_token,'otp' =>$otp,'otp_expired'=>date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))+900)); 
                    $this->Common_model->addEditRecords('users',$formData,array('user_id'=>$user_data['user_id']));  
                    $login=array(   
                        'user_id'=>$user_data['user_id'],
                        'fullname'=>$user_data['fullname'],
                        'staging_id'=>$user_data['staging_id'],
                        'mobile'=>$user_data['mobile'],
                        'email'=>$user_data['email'],
                        'user_type'=>$user_data['user_type'],
                        'status'=>$user_data['status'],
                        'company_name'=>$user_data['company_name'],
                        'country'=>$user_data['country'],
                        'state'=>$user_data['state'],
                        'address'=>$user_data['address'],
                        'establishment_year'=>$user_data['establishment_year'],
                        'business_no'=>$user_data['business_no'],
                        'document_type'=>$user_data['document_type'],
                        'document_proof'=>$user_data['document_proof'],
                        'document_status'=>$user_data['document_status'],
                        'mobile_auth_token'=>$mobile_auth_token,
                        );
                   
                    $this->display_output('1','Verification OTP sent on your registered mobile number, Please check it.',array('details'=>$login));
                }
                else
                {
                    $this->display_output('0','Your account has been deactivated!  Please contact administrator for details.');
                }
               
            }
            
        }
        else
        {
            $this->display_output('0','Staging ID or mobile number does not match.');
        }
    }
    public function resend_otp(){
        $mobile= $this->clear_input($this->input->post('mobile'));
      
        if(empty($mobile)) {
            $this->display_output('0','Please enter mobile no.');
        }
        if(!$user_data= $this->Common_model->getRecords('users','status,is_deleted,mobile,mobile_auth_token',array('mobile'=>$mobile),'user_id DESC',true)) { 
            $this->display_output('0','Please enter correct mobile no.'); 
        }
        if($user_data['is_deleted']==1) {
            $this->display_output('0','Your account has been deleted, Please contact us for more details.'); 
        } 
        if($user_data['status']=='Inactive') {
            $this->display_output('0','Your account has been inactive, Please contact us for more details.');
        } 
        $otp = mt_rand(1000,9999);
        $otp ='1234';
        //$this->otp_send($mobile,$otp);
        $otp_data = array( 
            'otp'=>$otp,
            'otp_expired'=>date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))+900),
            'modified'=>date('Y-m-d H:i:s')
        );
        if($this->Common_model->addEditRecords('users',$otp_data,array('mobile'=>$mobile))){
            $this->display_output('1','OTP sent to your registered mobile no.'); 
        }else{
            $this->display_output('0','Some error occured! Please try again.'); 
        }

    } 
    public function logout() 
    {
        $user_id            =   $this->clear_input($this->input->post('user_id'));
        $mobile_auth_token  =   $this->clear_input($this->input->post('mobile_auth_token'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        
        $update = array(
            'mobile_auth_token'=>'',
            'device_id'=>'',
            'device_type'=>''
        );
        $this->Common_model->addEditRecords('users',$update,array('user_id' => $user_id));
        $this->display_output('1','Logout successfully.');
    }

    public function change_password() 
    {
        $user_id            = $this->clear_input($this->input->post('user_id')); 
        $current_password   = $this->clear_input($this->input->post('current_password'));
        $new_password       = $this->clear_input($this->input->post('new_password'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }  
        if(empty($current_password)){
            $this->display_output('0','Please enter current password.');
        }
        if(empty($new_password)){
            $this->display_output('0','Please enter new password.');
        }  
        $this->App_model->check_user_status();

        $old_password = $this->Common_model->getFieldValue('users', 'password', array('user_id'=>$user_id));

        if($old_password == base64_encode($current_password)) {
            $new_password = base64_encode($new_password);
            $where = array('user_id'=>$user_id);
            $update_data = array(
                'password' => $new_password,
                'modified'=>date("Y-m-d H:i:s")
                );

            if(!$this->Common_model->addEditRecords('users', $update_data, $where)) {
                $this->display_output('0','Some error occurred! Please try again.');
            } else {
                $this->display_output('1','Password changed successfully.');
            }
        } else {
            $this->display_output('0','Current password is incorrect.');
        }
    }
 
    public function forgot_password()
    {   
        $email    =   $this->clear_input($this->input->post('email'));  
        if(empty($email)){
            $this->display_output('0','Please enter email/staging ID.');
        } 
       
        if(!$user_data= $this->Common_model->getRecords('users','user_id,staging_id,status,is_deleted,email,mobile_auth_token',array('email'=>$email),'user_id DESC',true)) {  
            if(!$user_data= $this->Common_model->getRecords('users','user_id,staging_id,status,is_deleted,email, mobile_auth_token',array('staging_id'=>$email),'user_id DESC',true)) {  
                $this->display_output('0','Please enter registered email/staging id.');
            }
        }
        if($user_data) {
            if($user_data['is_deleted']==1) {
                $this->display_output('0','Your account has been deleted, Please contact us for more details.');
            } 
            if($user_data['status']=='Inactive') {
                $this->display_output('0','Your account has been inactive, Please contact us for more details.');
            } 
            if($user_data['status']=='Unverified') {
                $this->display_output('2','Your account is not verified.');
            } 
            $token = mt_rand(1000,9999);
            $reset_token_date = date("Y-m-d H:i:s");
            $to_email = $user_data['email']; 
            $from_email =  getAdminEmail();  
            $subject = WEBSITE_NAME." : Reset Password"; 
            
            $data['name'] = ucwords($user_data['staging_id']); 
            
            $data['reset_token'] = $token;
            $body = $this->load->view('template/forgot_password', $data,TRUE);


            if($this->Common_model->sendEmail($to_email,$subject,$body,$from_email)) 
            {
                $reset_token_date = date("Y-m-d H:i:s");
                $where = array('user_id'=> $user_data['user_id']); 
                $update_data = array(
                    'reset_token'=>$token, 
                    'reset_token_date'=>$reset_token_date
                    );
                $this->Common_model->addEditRecords('users', $update_data, $where);
                $this->display_output('1','Reset password OTP sent on your email address, Please check your inbox and spam.');
            } else {
                $this->display_output('0','Some error occurred! Please try again.');
            } 
        }
            
    }

    public function reset_password() 
    {
        $email  =   $this->clear_input($this->input->post('email'));  
        $otp    =   $this->clear_input($this->input->post('reset_token'));  
        $new_password    =   $this->clear_input($this->input->post('new_password'));  

        if(empty($email)){
            $this->display_output('0','Please enter email/staging ID.');
        }
        if(empty($otp)){
            $this->display_output('0','Please enter reset password token.');
        }  
        if(empty($new_password)){
            $this->display_output('0','Please enter new password.');
        }  
        if(!$user_data= $this->Common_model->getRecords('users','user_id,status,is_deleted,mobile_auth_token,reset_token,reset_token_date',array('email'=>$email),'user_id DESC',true)) {  
            if(!$user_data= $this->Common_model->getRecords('users','user_id,status,is_deleted, mobile_auth_token,reset_token,reset_token_date',array('staging_id'=>$email),'user_id DESC',true)) {  
                $this->display_output('0','Please enter registered email/staging id.');
            }
        }

        if($user_data){
            if($user_data['reset_token'] != trim($otp)) {
                $this->display_output('0','Invalid reset password token.');
            }
            if($user_data['is_deleted']==0 && $user_data['status']=='Active'){

                $token_date = strtotime($user_data['reset_token_date']);
                $current_date = new DateTime("now", new DateTimeZone('UTC'));
                $current_date = $current_date->format('Y-m-d H:i:s');
                $diff=strtotime($current_date)-$token_date;
                if($diff > 3600) {
                    $this->display_output('0','Reset password token has been expired !!');
                } else {
                    if($user_data['password'] != base64_encode($new_password) )
                    {
                        $where = array('user_id'=> $user_data['user_id']); 
                        $update_data = array(
                                'password'=>base64_encode($new_password),
                                'reset_token'=>'', 
                                'reset_token_date'=>'',
                                'modified'=>date('Y-m-d H:i:s')
                            );

                        $this->Common_model->addEditRecords('users', $update_data, $where);
                        $this->display_output('1','Password reset successfully.');
                        
                    }else{
                        $this->display_output('0',"Password Can't be same as old password !!");
                    } 
                } 
            }else{
                $this->display_output('0','Your account has been deactivated!  Please contact administrator for details.');
            }
        }

    }

    public function get_user_detail() 
    {
        $user_id            = $this->clear_input($this->input->post('user_id')); 
        
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }  
        if(empty($role_id)){
            $this->display_output('0','Please enter role id.');
        } 
        
        $this->App_model->check_user_status();
        
        $detail=  $this->Common_model->getRecords('users','fullname,mobile,staging_id,user_type',array('user_id'=>$mobile,'otp'=>$otp),'',true);
        if($detail){
            $this->display_output('1','User Detail',array('details'=>$detail));
        }else{
            $this->display_output('0','No detail found.');  
        }
    }


    public function update_user_profile() 
    {
        $user_id            = $this->clear_input($this->input->post('user_id')); 
        $role_id            = $this->clear_input($this->input->post('role_id')); 

        $first_name         = $this->clear_input($this->input->post('first_name')); 
        $middle_name        = $this->clear_input($this->input->post('middle_name')); 
        $last_name          = $this->clear_input($this->input->post('last_name')); 
        $email              = $this->clear_input($this->input->post('email')); 
        $state_id           = $this->clear_input($this->input->post('state_id')); 
        $city_id            = $this->clear_input($this->input->post('city_id')); 
        $zipcode            = $this->clear_input($this->input->post('zipcode')); 
        $phone_no           = $this->clear_input($this->input->post('phone_no')); 
        $address1           = $this->clear_input($this->input->post('address1')); 
        $address2           = $this->clear_input($this->input->post('address2')); 

        $latitude           = $this->clear_input($this->input->post('latitude')); 
        $longitude           = $this->clear_input($this->input->post('longitude')); 

        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }  
        if(empty($role_id)){
            $this->display_output('0','Please enter role id.');
        } 

        if(empty($first_name)) {
            $this->display_output('0','Please enter first name.');
        }
        if(empty($last_name)){
            $this->display_output('0','Please enter last name.');
        } 
        if(empty($email)) {
            $this->display_output('0','Please enter email.');
        }
        
        if(empty($state_id)){
            $this->display_output('0','Please select state.');
        }
        if(empty($city_id)){
            $this->display_output('0','Please select city.');
        }
        if(empty($zipcode)){
            $this->display_output('0','Please enter zipcode.');
        }

        if(empty($phone_no)){
            $this->display_output('0','Please enter phone number.');
        }
        if(empty($address1)){
            $this->display_output('0','Please enter full address.');
        }

        $this->App_model->check_user_status();

        //if user is subuser of agency then get parent id
        $parent_id=getParentAdminId($user_id);
        //check user status
        $user = $this->Common_model->getRecords('users','id,status',array('admin_id'=>$parent_id),'',true);
        if(!$user){
            $this->display_output('0','You don\'t have permission to see details.');
        }
        if($user['status']=='Inactive'){

            $this->display_output('0','User has been deactivated!  Please contact administrator for details.');
        }

        $userId=$user['id'];

        if($this->Common_model->getRecords('admin', 'email', array('admin_id !='=>$user_id,'email'=>$email,'is_deleted'=>0), '', true)) {
            $this->display_output('0','Email already exist.');
        }

        $filepath="";
        $filerror="";
        if(isset($_FILES['profile_pic']) && !empty($_FILES['profile_pic']['name'])){
            if($_FILES['profile_pic']['error']==0) {
                $image_path = 'resources/images/profile/';
                // $allowed_types = 'jpg|JPG|jpeg|JPEG|png|PNG|svg';
                $allowed_types = '*';
                $file='profile_pic';
                $height = 150;
                $width = 150;
                $responce = commonImageUpload($image_path,$allowed_types,$file,$width,$height);

                if($responce['status']==0){
                    $upload_error = $responce['msg'];   
                    $filerror="1";
                } else {
                    $filepath=$responce['image_path'];
                }
            }
        }
        if($filerror!=''){
            $this->display_output('0',strip_tags($upload_error));
        }

        $personal_data = array(
            'email'=>$email,
            'modified'=> date("Y-m-d H:i:s"),
            'modified_by'=> $user_id
        );
        
        if(!$this->Common_model->addEditRecords('admin',$personal_data,array('admin_id'=>$user_id))) {
            $this->display_output('0','Something went wrong. Please try again.');  
        }

        $set = array(
            'first_name'=>$first_name,
            'middle_name'=> $middle_name,
            'last_name'=> $last_name,
            'state_id'=> $state_id,
            'city_id'=> $city_id,
            'zipcode'=> $zipcode,
            'phone_no'=> $phone_no,
            'address1'=> $address1,
            'address2'=> $address2,
            'latitude'=> $latitude,
            'longitude'=> $longitude,
            'modified'=> date("Y-m-d H:i:s"),
            'modified_by'=> $user_id
        );

        $detail=$this->App_model->get_user_edit_details($userId);
        if(!empty($filepath)){
            $set['profile_pic']=$filepath;
        }


        $where['id']=$userId;
        $where['admin_id']=$user_id;
        if($this->Common_model->addEditRecords('users',$set,$where))
        {
            if(!empty($filepath)){
                if(!empty($detail['profile_pic'])){
                    unlink($detail['profile_pic']);
                }
            }
            $this->display_output('1','Profile Updated successfully.');
        }else{
            $this->display_output('0','Something went wrong. Please try again.'); 
        }

    }

    public function get_countries() 
    {
        $list=$this->Common_model->getRecords('countries', 'id,name', array(), 'name', false);
        if($list){
            $this->display_output('1','Country list',array('list'=>$list));
        }else{
            $this->display_output('0','No data available at this time.');  
        }

    }
    public function get_states() 
    {
        $country_id   = $this->clear_input($this->input->post('country_id'));
        if(empty($country_id)){
            $this->display_output('0','Please enter country id.');  
        }
        $list=$this->Common_model->getRecords('states', 'id,name', array('country_id'=>$country_id), 'name', false);
        if($list){
            $this->display_output('1','State list',array('list'=>$list));
        }else{
            $this->display_output('0','No data available at this time.');  
        }

    }
    public function get_service_category_list() 
    {
        $list=$this->Common_model->getRecords('service_category', 'id,title', array('status'=>'Active','is_deleted'=>0), 'title', false);
        if($list){
            $this->display_output('1','Service category list',array('list'=>$list));
        }else{
            $this->display_output('0','No data available at this time.');  
        }

    }
    public function get_measurement_unit_list() 
    {
        $list=$this->Common_model->getRecords('measurement_unit', 'id,title', array('status'=>'Active','is_deleted'=>0), 'title', false);
        if($list){
            $this->display_output('1','Measurement unit list',array('list'=>$list));
        }else{
            $this->display_output('0','No data available at this time.');  
        }

    }
    ////////////////////////////////////service provider section/////////////////////////////////
    public function create_services() 
    {
        $user_id            = $this->clear_input($this->input->post('user_id')); 
        $service_title            = $this->clear_input($this->input->post('service_title')); 
        $category_id        = $this->clear_input($this->input->post('category_id')); 
        $description        = $this->clear_input($this->input->post('description'));
       /* $address            = $this->clear_input($this->input->post('address'));//after discussion remove it & all the location added in profile
        $latitude         = $this->clear_input($this->input->post('latitude'));
        $longitude        = $this->clear_input($this->input->post('longitude'));*/
        $item        = $this->clear_input($this->input->post('item'));
        $item_arr=json_decode($item);
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        } 
        if(empty($service_title)) {
            $this->display_output('0','Please enter service title.');
        } 
        if(empty($category_id)){
            $this->display_output('0','Please enter category id.');
        } 
        if(empty($description)){
            $this->display_output('0','Please enter description.');
        }
        if(empty($item)){
            $this->display_output('0','Please enter item.');
        }
       /* if(empty($address)){
            $this->display_output('0','Please enter address.');
        }
        if(empty($latitude)){
            $this->display_output('0','Please enter correct latitude.');
        }
        if(empty($longitude)){
            $this->display_output('0','Please enter correct longitude.');
        }*/
        /*$where2 = array('user_id' => $user_id,'category_id'=>$category_id,'is_deleted'=>'0');
        if($this->Common_model->getRecords('services','id',$where2,'',true)) {
            $this->display_output('0','You already created a service for this category.');
        }*/
       
        $this->App_model->check_user_status();
       
        $service_data=array(   
            'user_id'=>$user_id,
            'service_title'=>$service_title,
            'category_id'=>$category_id,
            'description'=>$description,
            'created'=>date('Y-m-d H:i:s')
        );
        if($_FILES['img_1']['name']!="" && !empty($_FILES['img_1']['name']))
        {
            $file_ext_ary= explode(".",$_FILES['img_1']['name']);
            $file_ext= end($file_ext_ary);
            $filename = rand().time().'_'.'01'.'.'.$file_ext;
            $file='img_1';
            $image_path = SERVICE_BANNER_PATH;
            $allowed_types = '*';
            $file='img_1';
            if($_FILES['img_1']['error']==0) {
                $responce_img_1 = multiImageUpload($image_path,$allowed_types,$file,$width,$height,$filename);
                if($responce_img_1['status']==0){
                    $this->display_output('0','Some error occured in upload of image 1.');
                }else{
                    $service_data['img1']=$responce_img_1['image_path'];
                }
            }
        }else{
            $this->display_output('0','Please insert an image.');
        }
        if($_FILES['img_2']['name']!="" && !empty($_FILES['img_2']['name']))
        {
            $file_ext_ary= explode(".",$_FILES['img_2']['name']);
            $file_ext= end($file_ext_ary);
            $filename = rand().time().'_'.'02'.'.'.$file_ext;
            $file='img_2';
            $image_path = SERVICE_BANNER_PATH;
            $allowed_types = '*';
            $file='img_2';
          
            if($_FILES['img_2']['error']==0) {
                $responce_img_2 = multiImageUpload($image_path,$allowed_types,$file,$width,$height,$filename);
                if($responce_img_2['status']==0){
                     $this->display_output('0','Some error occured in upload of image 2.');  
                }
                else{
                    $service_data['img2']=$responce_img_2['image_path'];
                }
            }
        }
        if($_FILES['img_3']['name']!="" && !empty($_FILES['img_3']['name']))
        {
            $file_ext_ary= explode(".",$_FILES['img_3']['name']);
            $file_ext= end($file_ext_ary);
            $filename = rand().time().'_'.'03'.'.'.$file_ext;
            $file='img_3';
            $image_path = SERVICE_BANNER_PATH;
            $allowed_types = '*';
            $file='img_3';
          
            if($_FILES['img_3']['error']==0) {
                $responce_img_3 = multiImageUpload($image_path,$allowed_types,$file,$width,$height,$filename);
                if($responce_img_3['status']==0){
                    $this->display_output('0','Some error occured in upload of image 3.');
                }else{
                    $service_data['img3']=$responce_img_3['image_path'];
                }
            }
        }
        $this->db->trans_begin();
        if($last_id = $this->Common_model->addEditRecords('services',$service_data)){
            for($i=0;$i<count($item_arr);$i++){
                $insert_array=array('item_name' => $item_arr[$i]->item,
                                    'price' =>$item_arr[$i]->price,
                                    'unit' =>$item_arr[$i]->unit,
                                    'qty' =>$item_arr[$i]->qty,
                                    'service_id'=>$last_id,
                                    'created'=>date('Y-m-d H:i:s'));
                 $this->Common_model->addEditRecords('services_item',$insert_array);
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $this->display_output('0','Something went wrong. Please try again.'); 
        } else {
            $this->db->trans_commit();
            $this->display_output('1','Service created successfully.');
        }
    }
    
    public function sp_service_list(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $list=$this->App_model->get_sp_service_list($user_id);
        if(!empty($list)) {
            $this->display_output('1','service list', array('details'=>$list));
        } else {
            $this->display_output('0','Service not found');
        }
    }
    public function sp_service_details(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $service_id  = $this->clear_input($this->input->post('service_id'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($service_id)) {
            $this->display_output('0','Please enter service id.');
        }
        $detail=$this->App_model->sp_service_details($service_id);
        if(!empty($detail)) {
            $items=$this->App_model->service_item_details($service_id);
            $response = array('service_details'=>$detail,'service_item'=>$items);
            $this->display_output('1','service detail', array('details'=>$response));
        } else {
            $this->display_output('0','Service detail not found');
        }
    }
    public function get_offer_service_list() 
    {
        $user_id            = $this->clear_input($this->input->post('user_id'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $list=$this->Common_model->getRecords('services', 'id,service_title', array('status'=>'Active','is_deleted'=>0,'user_id'=>$user_id), 'service_title', false);
        if($list){
            $this->display_output('1','Service category list',array('list'=>$list));
        }else{
            $this->display_output('0','No data available at this time.');  
        }

    }
    public function create_offers() 
    {
        $user_id            = $this->clear_input($this->input->post('user_id')); 
        $offer_name         = $this->clear_input($this->input->post('offer_name')); 
        $service_id         = $this->clear_input($this->input->post('service_id')); 
        $description        = $this->clear_input($this->input->post('description'));
        $start_date         = $this->clear_input($this->input->post('start_date'));
        $end_date           = $this->clear_input($this->input->post('end_date'));
        $discount           = $this->clear_input($this->input->post('discount'));
        $timezone           = $this->clear_input($this->input->post('timezone'));
        $st_date=convertLocalTimezoneToGMT($start_date,$timezone,$showTime=true);
        $end_date=convertLocalTimezoneToGMT($end_date,$timezone,$showTime=true);
        $offer_st_date=date('Y-m-d H:i:s',strtotime($st_date));
        $offer_end_date= date('Y-m-d H:i:s',strtotime($end_date));
       

        if($this->App_model->check_offer($service_id,$offer_st_date,$offer_end_date)){
            $this->display_output('0','You already added offer for this time duration.');
        }
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($offer_name)){
            $this->display_output('0','Please enter offer name.');
        }  
        if(empty($service_id)){
            $this->display_output('0','Please enter service id.');
        } 
        /*if(empty($description)){
            $this->display_output('0','Please enter description.');
        }*/
        if(empty($start_date)){
            $this->display_output('0','Please enter start date.');
        }
        if(empty($end_date)){
            $this->display_output('0','Please enter end date.');
        }if(empty($discount)){
            $this->display_output('0','Please enter discount.');
        }if(empty($timezone)){
            $this->display_output('0','Please enter time zone.');
        }
        $this->App_model->check_user_status();
        if($_FILES['img_1']['name']!="" && !empty($_FILES['img_1']['name']))
        {
            $file_ext_ary= explode(".",$_FILES['img_1']['name']);
            $file_ext= end($file_ext_ary);
            $filename = rand().time().'_'.'01'.'.'.$file_ext;
            $file='img_1';
            $image_path = OFFER_BANNER_PATH;
            $allowed_types = '*';
            $file='img_1';
            if($_FILES['img_1']['error']==0) {
                $responce_img_1 = multiImageUpload($image_path,$allowed_types,$file,$width,$height,$filename);
                
                //unlink($image_path);
                if($responce_img_1['status']==0){
                    $this->display_output('0','Some error occured in upload of image.');
                }
            }
        }else{
            $this->display_output('0','Please insert an image.');
        }
        
        $offer_data=array(   
            'user_id'=>$user_id,
            'offer_name'=>$offer_name,
            'service_id'=>$service_id,
            'description'=>$description,
            'start_date'=>$offer_st_date,
            'end_date'=>$offer_end_date,
            'discount'=>$discount,
            'img1'=>$responce_img_1['image_path'],
            'created'=>date('Y-m-d H:i:s')
        );
        if($last_id = $this->Common_model->addEditRecords('offer',$offer_data)){
            $this->display_output('1','Offer created successfully.');
        }else{
            $this->display_output('0','Something went wrong. Please try again.'); 
        }
        
    }
    public function edit_offers() 
    {
        $offer_id           = $this->clear_input($this->input->post('offer_id')); 
        $user_id            = $this->clear_input($this->input->post('user_id')); 
        $offer_name         = $this->clear_input($this->input->post('offer_name')); 
        $service_id         = $this->clear_input($this->input->post('service_id')); 
        $description        = $this->clear_input($this->input->post('description'));
        $start_date         = $this->clear_input($this->input->post('start_date'));
        $end_date           = $this->clear_input($this->input->post('end_date'));
        $discount           = $this->clear_input($this->input->post('discount'));
        $timezone           = $this->clear_input($this->input->post('timezone'));
        $st_date=convertLocalTimezoneToGMT($start_date,$timezone,$showTime=true);
        $end_date=convertLocalTimezoneToGMT($end_date,$timezone,$showTime=true);
        $offer_st_date=date('Y-m-d H:i:s',strtotime($st_date));
        $offer_end_date= date('Y-m-d H:i:s',strtotime($end_date));

        if($this->App_model->check_offer($service_id,$offer_st_date,$offer_end_date,$offer_id)){
            $this->display_output('0','You already added offer for this time duration.');
        }

        if(empty($offer_id)) {
            $this->display_output('0','Please enter offer id.');
        }
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($offer_name)){
            $this->display_output('0','Please enter offer name.');
        }  
        if(empty($service_id)){
            $this->display_output('0','Please enter service id.');
        } 
        /*if(empty($description)){
            $this->display_output('0','Please enter description.');
        }*/
        if(empty($start_date)){
            $this->display_output('0','Please enter start date.');
        }
        if(empty($end_date)){
            $this->display_output('0','Please enter end date.');
        }if(empty($discount)){
            $this->display_output('0','Please enter discount.');
        }if(empty($timezone)){
            $this->display_output('0','Please enter time zone.');
        }
        $this->App_model->check_user_status();
        
        $offer_data=array(   
            'user_id'=>$user_id,
            'offer_name'=>$offer_name,
            'service_id'=>$service_id,
            'description'=>$description,
            'start_date'=>$offer_st_date,
            'end_date'=>$offer_end_date,
            'discount'=>$discount,
            'modified'=>date('Y-m-d H:i:s')
        );
        if($_FILES['img_1']['name']!="" && !empty($_FILES['img_1']['name']))
        {
            $file_ext_ary= explode(".",$_FILES['img_1']['name']);
            $file_ext= end($file_ext_ary);
            $filename = rand().time().'_'.'01'.'.'.$file_ext;
            $file='img_1';
            $image_path = OFFER_BANNER_PATH;
            $allowed_types = '*';
            $file='img_1';
            if($_FILES['img_1']['error']==0) {
                $responce_img_1 = multiImageUpload($image_path,$allowed_types,$file,$width,$height,$filename);
                if($responce_img_1['status']==0){
                    $this->display_output('0','Some error occured in upload of image.');
                }else{
                    $offer_data['img1']=$responce_img_1['image_path'];
                    $record=$this->Common_model->getRecords('offer', 'img1', array('id'=>$offer_id), '', true);
                }
            }
        }
        
        if($last_id = $this->Common_model->addEditRecords('offer',$offer_data,array('id'=>$offer_id))){
            if(isset($record['img1']) && !empty($record['img1'])){
                if(file_exists($record['img1'])){
                   unlink($record['img1']);
                }
            }
            $this->display_output('1','Offer updated successfully.');
        }else{
            $this->display_output('0','Something went wrong. Please try again.'); 
        }
        
    }
    public function sp_offers_list(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $list=$this->App_model->get_sp_offer_list($user_id);
        if(!empty($list)) {
            $this->display_output('1','Offer list', array('details'=>$list));
        } else {
            $this->display_output('0','Offers not found');
        }
    }
    public function sp_offer_details(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $offer_id  = $this->clear_input($this->input->post('offer_id'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($offer_id)) {
            $this->display_output('0','Please enter offer id.');
        }
        $detail=$this->App_model->sp_offer_details($offer_id);
        if(!empty($detail)) {
            $this->display_output('1','Offer detail', array('details'=>$detail));
        } else {
            $this->display_output('0','Offer detail not found');
        }
    }
    public function sp_offer_delete(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $offer_id  = $this->clear_input($this->input->post('offer_id'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($offer_id)) {
            $this->display_output('0','Please enter offer id.');
        }
        if($this->Common_model->addEditRecords('offer',array('is_deleted'=>1,'modified'=>date('Y-m-d H:i:s')),array('id'=>$offer_id))){
            $this->display_output('1','Offer deleted successfully.');
        }else{
            $this->display_output('0','Some error occured! Please try again.');
        }
    }
    public function sp_my_bookings(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $offer_id  = $this->clear_input($this->input->post('offer_id'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($offer_id)) {
            $this->display_output('0','Please enter offer id.');
        }
        if($this->Common_model->addEditRecords('offer',array('is_deleted'=>1,'modified'=>date('Y-m-d H:i:s')),array('id'=>$offer_id))){
            $this->display_output('1','Offer deleted successfully.');
        }else{
            $this->display_output('0','Some error occured! Please try again.');
        }
    }


    ////////////////////////////////////Celebrant provider section/////////////////////////////////

    public function celebrant_get_service_provider_details(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $service_provider_id  = $this->clear_input($this->input->post('service_provider_id'));
        $timezone  = $this->clear_input($this->input->post('timezone'));

        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($service_provider_id)) {
            $this->display_output('0','Please enter service provider id.');
        }
        /*if(empty($timezone)){
            $this->display_output('0','Please enter time zone.');
        }*/
        $detail=$this->App_model->cp_get_sp_details($service_provider_id);
        if(!empty($detail)) {
            $local = date("Y-m-d H:i:s");
            $service_offer=$this->App_model->get_servies_offer($service_provider_id,$local);
           //echo $this->db->last_query();exit;
            $service_image=$this->Common_model->getRecords('services','img1',array('is_deleted'=>0,'status'=>'Active','user_id'=>$service_provider_id),'',false);
            
            $response =array('sp_details'=>$detail,'service_offer'=>$service_offer,'slider_image'=>$service_image);
            $this->display_output('1','service provider detail', array('details'=>$response));
        } else {
            $this->display_output('0','service provider details not found');
        }
    }
    public function get_items(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $service_id  = $this->clear_input($this->input->post('service_id'));
        
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($service_id)) {
            $this->display_output('0','Please enter service id.');
        }
        $detail=$this->App_model->get_service_items($service_id);
      
        if(!empty($detail)) {
            $this->display_output('1','Item details', array('item_details'=>$detail));
        } else {
            $this->display_output('0','Items not found');
        }
    }
    public function create_event(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $timezone  = $this->clear_input($this->input->post('timezone'));
        $event_title  = $this->clear_input($this->input->post('event_title'));
        $event_date  = $this->clear_input($this->input->post('event_date'));
        $event_time  = $this->clear_input($this->input->post('event_time'));
        $members  = $this->clear_input($this->input->post('members'));
        $country  = $this->clear_input($this->input->post('country'));
        $state  = $this->clear_input($this->input->post('state'));
        $city  = $this->clear_input($this->input->post('city'));
        $event_address  = $this->clear_input($this->input->post('event_address'));
        $latitude  = $this->clear_input($this->input->post('latitude'));
        $longitude  = $this->clear_input($this->input->post('longitude'));
        $note  = $this->clear_input($this->input->post('note'));
        $items  = $this->clear_input($this->input->post('items'));
        $service_id  = $this->clear_input($this->input->post('service_id'));

        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($timezone)){
            $this->display_output('0','Please enter time zone.');
        }
       /* if(empty($service_id)) {
            $this->display_output('0','Please enter service id.');
        }*/
        if(empty($event_title)) {
            $this->display_output('0','Please enter event name.');
        }if(empty($event_date)) {
            $this->display_output('0','Please enter event date.');
        }if(empty($event_time)) {
            $this->display_output('0','Please enter event time.');
        }if(empty($members)) {
            $this->display_output('0','Please enter members.');
        }if(empty($country)) {
            $this->display_output('0','Please enter country.');
        }if(empty($state)) {
            $this->display_output('0','Please enter state.');
        }if(empty($city)) {
            $this->display_output('0','Please enter city.');
        }
        if(empty($event_address)) {
            $this->display_output('0','Please enter event address.');
        }
        if(empty($items)) {
            $this->display_output('0','Please enter items.');
        }if(empty($service_id)) {
            $this->display_output('0','Please enter service id.');
        }
       
        $offer_where="status='Active' and is_deleted=0 and  service_id = '".$service_id."'  and (start_date <= '".date('Y-m-d H:i:s')."' and end_date >= '".date('Y-m-d H:i:s')."')";
        $offer_details=$this->Common_model->getRecords('offer','discount,offer_name',$offer_where,'',true);
        
        $user_details=$this->Common_model->getRecords('users','fullname,email,mobile,staging_id',array('user_id'=>$user_id),'',true);
        $sp_details=$this->Common_model->getRecords('services','user_id',array('id'=>$service_id),'',true);
        $event_date=convertLocalTimezoneToGMT($event_date.' '.$event_time,$timezone,$showTime=true);
        $event_datetime=date('Y-m-d H:i:s',strtotime($event_date));
        $insert_data=array(
            'user_id'=>$user_id,
            'service_id'=>$service_id,
            'sp_id'=>$sp_details['user_id'],
            'event_title'=>$event_title,
            'event_date'=>$event_datetime,
            'members'=>$members,
            'country_id'=>$country,
            'state_id'=>$state,
            'city_id'=>$city,
            'event_address'=>$event_address,
            'note'=>$note,
            'latitude'=>$latitude,
            'longitude'=>$longitude,
            'created'=>date('Y-m-d H:i:s'),
            );
        $item_arr=json_decode($items);
        $this->db->trans_begin();
        if($last_id = $this->Common_model->addEditRecords('events',$insert_data)){
            $event_number='E'.str_pad($last_id,8,0,STR_PAD_LEFT);
                $this->Common_model->addEditRecords('events',array('event_number'=>$event_number),array('id'=>$last_id));
            $user_data=array('user_id'=>$user_id,
                            'fullname'=>$user_details['fullname'],
                            'email'=>$user_details['email'],
                            'mobile'=>$user_details['mobile'],
                            'event_id'=>$last_id,
                            'event_date_time'=>$event_datetime,
                            'service_id'=>$service_id,
                            'ip_address'=>$this->input->ip_address(),
                            'created'=>date('Y-m-d H:i:s')
                );
            if($order_id = $this->Common_model->addEditRecords('orders',$user_data)){
                 $order_number=str_pad($order_id,8,0,STR_PAD_LEFT);
                 $this->Common_model->addEditRecords('orders',array('order_number'=>$order_number),array('id'=>$order_id));
                $user_data['order_id']=$order_id;
                $user_data['order_number']=$order_number;
                $order_history_id=$this->Common_model->addEditRecords('orders_history',$user_data);
                $subtotal=0;
                $discount=0;
                $discount_title='Discount';
                if(!empty($offer_details['discount'])){
                    $discount=$offer_details['discount'];
                    $discount_title='Discount'.'-'.$offer_details['offer_name'];
                }
                
                for($i=0;$i<count($item_arr);$i++){
                    $total_qty=($item_arr[$i]->qty*$item_arr[$i]->customer_qty);
                    $total_price=number_format($item_arr[$i]->customer_qty*$item_arr[$i]->price,2);
                    $sub_total+=$total_price;
                    $sub_item_total_price=$sub_total;
                    $insert_array=array('item_name' => $item_arr[$i]->item,
                                        'price' =>$item_arr[$i]->price,
                                        'unit' =>$item_arr[$i]->unit,
                                        'qty' =>$item_arr[$i]->qty,
                                        'customer_qty' =>$item_arr[$i]->customer_qty,
                                        'total_qty' =>$total_qty,
                                        'total_price' =>$total_price,
                                        'service_id'=>$service_id,
                                        'order_id'=>$order_id,
                                        'event_id'=>$last_id,
                                        'event_date_time'=>$event_datetime,
                                        'created'=>date('Y-m-d H:i:s'));
                    $this->Common_model->addEditRecords('order_details',$insert_array);
                    $insert_array['order_history_id']=$order_history_id;
                    $this->Common_model->addEditRecords('order_history_details',$insert_array);
                }
                $order_total1= array('order_id'=>$order_id,'code'=>'sub_total','title'=>'Subtotal','value'=>$sub_item_total_price,'sort_order'=>1,'created'=>date('Y-m-d H:i:s'));
                if($discount>0){
                    $deduct_discount=($sub_item_total_price*$discount)/100;
                    $gt=$sub_item_total_price-$deduct_discount;
                    $order_total2= array('order_id'=>$order_id,'code'=>'discount','title'=>$discount_title,'value'=>$deduct_discount,'discount_value'=>$discount,'sort_order'=>2,'created'=>date('Y-m-d H:i:s'));
                }else{
                    $gt=$sub_item_total_price;
                    $order_total2= array('order_id'=>$order_id,'code'=>'discount','title'=>$discount_title,'value'=>0,'discount_value'=>0,'sort_order'=>2,'created'=>date('Y-m-d H:i:s'));
                }
                $order_total3= array('order_id'=>$order_id,'code'=>'total_price','title'=>'Total Price','value'=>$gt,'sort_order'=>3,'created'=>date('Y-m-d H:i:s'));
                $this->Common_model->addEditRecords('order_total',$order_total1);
                $this->Common_model->addEditRecords('order_total',$order_total2);
                $this->Common_model->addEditRecords('order_total',$order_total3);
                $this->Common_model->addEditRecords('orders',array('total_amount'=>$gt),array('id'=>$order_id));
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $this->display_output('0','Something went wrong. Please try again.'); 
        } else {
            $this->db->trans_commit();
            $sp_user_details=getNameEmailAddress($sp_details['user_id']);
            $from_email =getAdminEmail() ;
            $to_email =$sp_user_details['email'];
            $subject = WEBSITE_NAME . ' - #'.$event_number.' New event created';
            $data['name']= $sp_user_details['staging_id'];
            $data['message']= '<b>'.$event_number.'</b>'.' '.ucwords($event_title).' is created by '.ucwords($user_details['fullname']).'. Please give your response.';
            $body = $this->load->view('template/common', $data,TRUE);
            $this->Common_model->sendEmail($to_email,$subject,$body,$from_email);

           
            $from_email =getAdminEmail() ;
            $to_email =$user_details['email'];
            $subject = WEBSITE_NAME . ' - #'.$event_number.' New event created';
            $data['name']= $user_details['staging_id'];
            $data['message']= '<b>'.$event_number.'</b>'.' '.ucwords($event_title).' is created by you. It is pending for service provider\'s approval.';
            $body = $this->load->view('template/common', $data,TRUE);
            $this->Common_model->sendEmail($to_email,$subject,$body,$from_email);

            $title=$event_number.'-'.ucwords($event_title).' is created.';
           // $time=date('h:i A',strtotime($start_time)).' - '.date('h:i A',strtotime($end_time));
            $content='For '.date('M d, Y',strtotime($event_date));
           // .' Time : '.$time;
            $notdata['type']='New Event';
            $this->Common_model->push_notification_send($sp_details['user_id'],$notdata,$last_id,$title,$content,'',$user_id);

            $this->display_output('1','Event created successfully.',array('event_id'=>$last_id));
        }
    }
     public function edit_event(){
        $event_id  = $this->clear_input($this->input->post('event_id'));
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $timezone  = $this->clear_input($this->input->post('timezone'));
        
        $event_date  = $this->clear_input($this->input->post('event_date'));
        $event_time  = $this->clear_input($this->input->post('event_time'));
        
        $items  = $this->clear_input($this->input->post('items'));
        $service_id  = $this->clear_input($this->input->post('service_id'));

        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($timezone)){
            $this->display_output('0','Please enter time zone.');
        }
        if(empty($event_id)) {
            $this->display_output('0','Please enter event id.');
        }
        if(empty($event_date)) {
            $this->display_output('0','Please enter event date.');
        }if(empty($event_time)) {
            $this->display_output('0','Please enter event time.');
        }
        if(empty($items)) {
            $this->display_output('0','Please enter items.');
        }if(empty($service_id)) {
            $this->display_output('0','Please enter service id.');
        }
        $current_date=date('Y-m-d H:i:s');
        $event_details=$this->Common_model->getRecords('events','event_date',array('id'=>$event_id),'',true);
        $event_details['event_date']='2020-01-01 08:30:00';
        $diff=strtotime($event_details['event_date'])-strtotime($current_date);
        if($diff < 172800) {
            $this->display_output('0','Edit time of event is passed away.');
        }
        $order_details=$this->Common_model->getRecords('orders','id,payment_status',array('event_id'=>$event_id),'',true);
        $order_id=$order_details['id'];
        $offer_where="status='Active' and is_deleted=0 and  service_id = '".$service_id."'  and (start_date <= '".date('Y-m-d H:i:s')."' and end_date >= '".date('Y-m-d H:i:s')."')";
        $offer_details=$this->Common_model->getRecords('offer','discount,offer_name',$offer_where,'',true);
        
        $user_details=$this->Common_model->getRecords('users','fullname,email,mobile,staging_id',array('user_id'=>$user_id),'',true);
        $sp_details=$this->Common_model->getRecords('services','user_id',array('id'=>$service_id),'',true);
        $event_date=convertLocalTimezoneToGMT($event_date.' '.$event_time,$timezone,$showTime=true);
        $event_datetime=date('Y-m-d H:i:s',strtotime($event_date));
        $update_data=array(
            'user_id'=>$user_id,
            'service_id'=>$service_id,
            'event_date'=>$event_datetime,
            'modified'=>date('Y-m-d H:i:s'),
            );
        $item_arr=json_decode($items);
        $this->db->trans_begin();
        if($this->Common_model->addEditRecords('events',$update_data,array('id'=>$event_id))){
            $user_data=array('user_id'=>$user_id,
                            'fullname'=>$user_details['fullname'],
                            'email'=>$user_details['email'],
                            'mobile'=>$user_details['mobile'],
                            'event_date_time'=>$event_datetime,
                            'payment_status'=>'Unpaid',
                            'service_id'=>$service_id,
                            'ip_address'=>$this->input->ip_address(),
                            'modified'=>date('Y-m-d H:i:s')
                );
            if($this->Common_model->addEditRecords('orders',$user_data,array('id'=>$event_id))){

                $subtotal=0;
                $discount=0;
                $discount_title='Discount';
                if(!empty($offer_details['discount'])){
                    $discount=$offer_details['discount'];
                    $discount_title='Discount'.'-'.$offer_details['offer_name'];
                }
                $this->Common_model->deleteRecords('order_details',array('order_id'=>$order_id));
                $this->Common_model->addEditRecords('order_history_details',array('status'=>'Inactive','modified'=>date('Y-m-d H:i:s')),array('order_id'=>$order_id));
                $this->Common_model->addEditRecords('order_total',array('status'=>'Inactive','modified'=>date('Y-m-d H:i:s')),array('order_id'=>$order_id));
                for($i=0;$i<count($item_arr);$i++){
                    $total_qty=($item_arr[$i]->qty*$item_arr[$i]->customer_qty);
                    $total_price=number_format($item_arr[$i]->customer_qty*$item_arr[$i]->price,2);
                    $sub_total+=$total_price;
                    $sub_item_total_price=$sub_total;
                    $insert_array=array('item_name' => $item_arr[$i]->item,
                                        'price' =>$item_arr[$i]->price,
                                        'unit' =>$item_arr[$i]->unit,
                                        'qty' =>$item_arr[$i]->qty,
                                        'customer_qty' =>$item_arr[$i]->customer_qty,
                                        'total_qty' =>$total_qty,
                                        'total_price' =>$total_price,
                                        'service_id'=>$service_id,
                                        'order_id'=>$order_id,
                                        'event_id'=>$event_id,
                                        'event_date_time'=>$event_datetime,
                                        'created'=>date('Y-m-d H:i:s'));
                    $this->Common_model->addEditRecords('order_details',$insert_array);
                    $this->Common_model->addEditRecords('order_history_details',$insert_array);
                }

                $order_total1= array('order_id'=>$order_id,'code'=>'sub_total','title'=>'Subtotal','value'=>$sub_item_total_price,'sort_order'=>1,'created'=>date('Y-m-d H:i:s'));
                if($discount>0){
                    $deduct_discount=($sub_item_total_price*$discount)/100;
                    $gt=$sub_item_total_price-$deduct_discount;
                    $order_total2= array('order_id'=>$order_id,'code'=>'discount','title'=>$discount_title,'value'=>$deduct_discount,'discount_value'=>$discount,'sort_order'=>2,'created'=>date('Y-m-d H:i:s'));
                }else{
                    $gt=$sub_item_total_price;
                    $order_total2= array('order_id'=>$order_id,'code'=>'discount','title'=>$discount_title,'value'=>0,'discount_value'=>0,'sort_order'=>2,'created'=>date('Y-m-d H:i:s'));
                }
                $order_total3= array('order_id'=>$order_id,'code'=>'total_price','title'=>'Total Price','value'=>$gt,'sort_order'=>3,'created'=>date('Y-m-d H:i:s'));
                $this->Common_model->addEditRecords('order_total',$order_total1);
                $this->Common_model->addEditRecords('order_total',$order_total2);
                $this->Common_model->addEditRecords('order_total',$order_total3);
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $this->display_output('0','Something went wrong. Please try again.'); 
        } else {
            $this->db->trans_commit();
            $sp_user_details=getNameEmailAddress($sp_details['user_id']);
            $from_email =getAdminEmail() ;
            $to_email =$sp_user_details['email'];
            $subject = WEBSITE_NAME . ' - #'.$event_number.' New event created';
            $data['name']= $sp_user_details['staging_id'];
            $data['message']= '<b>'.$event_number.'</b>'.' '.ucwords($event_title).' is created by '.ucwords($user_details['fullname']).'. Please give your response.';
            $body = $this->load->view('template/common', $data,TRUE);
            $this->Common_model->sendEmail($to_email,$subject,$body,$from_email);

           
            $from_email =getAdminEmail() ;
            $to_email =$user_details['email'];
            $subject = WEBSITE_NAME . ' - #'.$event_number.' New event created';
            $data['name']= $user_details['staging_id'];
            $data['message']= '<b>'.$event_number.'</b>'.' '.ucwords($event_title).' is created by you. It is pending for service provider\'s approval.';
            $body = $this->load->view('template/common', $data,TRUE);
            $this->Common_model->sendEmail($to_email,$subject,$body,$from_email);

            $title=$event_number.'-'.ucwords($event_title).' is created.';
           // $time=date('h:i A',strtotime($start_time)).' - '.date('h:i A',strtotime($end_time));
            $content='For '.date('M d, Y',strtotime($event_date));
           // .' Time : '.$time;
            $notdata['type']='New Event';
            $this->Common_model->push_notification_send($sp_details['user_id'],$notdata,$last_id,$title,$content,'',$user_id);

            $this->display_output('1','Event created successfully.');
        }
    }
    public function my_bookings_list(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $keyword  = $this->clear_input($this->input->post('keyword'));
        $user_type  = $this->clear_input($this->input->post('user_type'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $list=$this->App_model->my_bookings($user_id,$user_type,$keyword);
        //echo $this->db->last_query();exit;
        if(!empty($list)) {
            $this->display_output('1','Event list', array('details'=>$list));
        } else {
            $this->display_output('0','Events not found');
        }
    }
    
    public function celebrant_booking_summary(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $event_id  = $this->clear_input($this->input->post('event_id'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }if(empty($event_id)) {
            $this->display_output('0','Please enter event id.');
        }
        $list=$this->App_model->booking_summary($event_id);
        //echo $this->db->last_query();exit;
        if(!empty($list)) {
            $item_record=$this->Common_model->getRecords('order_details','item_name,price,qty,customer_qty,total_price,unit',array('order_id'=>$list['order_id']),'',false);
            $order_record=$this->Common_model->getRecords('order_total','code,title,value,discount_value',array('order_id'=>$list['order_id'],'status'=>'Active'),'',false);
            $order_total=array();
            if(!empty($order_record)){
                foreach ($order_record as $key => $value) {
                   $order_total[$value['title']]=$value['value'];
                }
            }
           
            $service_offer=$this->App_model->get_order_items($list['order_id']);
           //echo $this->db->last_query();exit;
            $response =array('event_summary'=>$list,'items'=>$item_record,'order_total'=>$order_total);
            $this->display_output('1','Booking summary', array('details'=>$response));

        } else {
            $this->display_output('0','Booking summary not found');
        }
    }
    ///////////////////////////////Celebrant social media Section/////////////////////////////
    public function convert_duration($sec) {
        $sec= (double)$sec;
        $s = $sec%60;
        $m = floor(($sec%3600)/60);
        $h = floor(($sec%86400)/3600);
        // $d = floor(($sec%2592000)/86400);
        // $M = floor($sec/2592000);

        $duration = "$h:$m:$s";
        //echo date("H:i:s",strtotime($duration));
        return date("H:i:s",strtotime($duration));
    }
     public function upload_image_amazon() {

     }
    public function upload_amazon() {

        $user_id = $this->clear_input($this->input->post('user_id'));
        $mobile_auth_token   = $this->clear_input($this->input->post('mobile_auth_token'));    
        $user_name = $this->clear_input($this->input->post('user_name')); 
        $media_type  = $this->clear_input($this->input->post('media_type'));  //1 =video, 2= audio
        $format =  $this->clear_input($this->input->post('format')); 
        // $thumb_image =  $this->clear_input($this->input->post('thumb_image'));    
        // $large_image =  $this->clear_input($this->input->post('large_image'));  
        $media_name =  $this->clear_input($this->input->post('media_name')); 
        $duration =  (double)$this->clear_input($this->input->post('duration'));  
        $mobile_type =  $this->clear_input($this->input->post('mobile_type')); //android
        $rotation =  $this->clear_input($this->input->post('rotation')); //android

        // echo "<pre>";print_r($_POST);exit;

       /* if(empty($user_id)) {
            $err = array('data' =>array('status' => '0', 'msg' => 'Please enter user id'));
            echo json_encode($err); exit;
        } else {
            $this->check_user_status($user_id);
        }

        if(empty($mobile_auth_token)) {
            $err = array('data' =>array('status' => '0', 'msg' => 'Please enter mobile auth token'));
            echo json_encode($err); exit;
        } else {
            $this->check_mobile_auth_token($mobile_auth_token,$user_id);
        }

        if(empty($user_name)) {
            $err = array('data' =>array('status' => '0', 'msg' => 'Please enter your username.'));
            echo json_encode($err); exit;
        }

        if(empty($media_type)) {
            $err = array('data' =>array('status' => '0', 'msg' => 'Please enter media type.'));
            echo json_encode($err); exit;
        }

        if(empty($format)) {
            $err = array('data' =>array('status' => '0', 'msg' => 'Please enter format.'));
            echo json_encode($err); exit;
        }

        if(empty($media_name)) {
            $err = array('data' =>array('status' => '0', 'msg' => 'Please enter media name.'));
            echo json_encode($err); exit;
        }
        if(empty($duration)) {
            $err = array('data' =>array('status' => '0', 'msg' => 'Please enter duration.'));
            echo json_encode($err); exit;
        } 
        if(!isset($mobile_type)) {
            $mobile_type = 'ios';
            $rotation = -1;
        } else if($mobile_type=='android') {
            if(!isset($rotation)) {
                $err = array('data' =>array('status' => '0', 'msg' => 'Please enter rotation.'));
                echo json_encode($err); exit;
            } 
        }*/

        $duration = $this->convert_duration($duration);
        $s3thumbPath='';
        $s3largeimagePath='';
        if($media_type==1) {
            $s3 = new S3(AMAZON_ID, AMAZON_KEY); 
            if(!is_dir(MEDIA_THUMB_TEMP_PATH)){
                mkdir(MEDIA_THUMB_TEMP_PATH);
            }
            $s3dir = $user_name.'/';
            $images = createImage($media_name,MEDIA_THUMB_TEMP_PATH,"800x450",$duration,$mobile_type,$rotation);
           // echo '<pre>';print_r($images);
            $thumb_image = $images[1];
            $large_image = $images[0];
            $thumb_imagePath = MEDIA_THUMB_TEMP_PATH.$thumb_image;
            $large_imagePath = MEDIA_THUMB_TEMP_PATH.$large_image;

            //upload images on the amazon s3
            
            if($thumb_imagePath !='') {
                //store thumb image on the amazon s3 and delete temp file
                $save_path = $s3dir.$thumb_image;
                $s3->putObjectFile($thumb_imagePath, AMAZON_BUCKET, $save_path, S3::ACL_PUBLIC_READ);
                $s3thumbPath=AMAZON_PATH.$save_path;
                if(file_exists($thumb_imagePath)) {
                    unlink($thumb_imagePath);
                }
            }

            if($large_imagePath !='') {
                //store thumb image on the amazon s3 and delete temp file
                $save_path = $s3dir.$large_image;
                $s3->putObjectFile($large_imagePath, AMAZON_BUCKET, $save_path, S3::ACL_PUBLIC_READ);
                $s3largeimagePath=AMAZON_PATH.$save_path;
                if(file_exists($large_imagePath)) {
                    unlink($large_imagePath);
                }
            }
        }
        echo $s3thumbPath;
        echo '<br>';
        echo $s3largeimagePath;
        $this->display_output('0',$s3largeimagePath);
         exit;

        /*$insert_data = array(
            'user_id' => $user_id,
            'format' => $format,
            'thumb_image' => $s3thumbPath,
            'large_image' => $s3largeimagePath,
            'media_name' => $media_name,
            'media_type' => $media_type,
            'status' => 3,
            'duration' => $duration,
            'created' => date("Y-m-d H:i:s")
        );

        if($media_id = $this->common_model->addEditRecords('media',$insert_data)) {
            $err = array('data' =>array('status' => '1', 'msg' => 'uplaoded successfully.','media_id'=>(string)$media_id,'image_path'=>$s3largeimagePath));
            echo json_encode($err); exit;
        } else {
            $err = array('data' =>array('status' => '0', 'msg' => 'Some error occured. Please try again.'));
            echo json_encode($err); exit;
        } */
    }
    public function create_post(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $staging_id  = $this->clear_input($this->input->post('staging_id'));
        $post_content  = $this->clear_input($this->input->post('post_content'));
        $address  = $this->clear_input($this->input->post('address'));
        $latitude  = $this->clear_input($this->input->post('latitude'));
        $longitude  = $this->clear_input($this->input->post('longitude'));
        $mobile_type =  $this->clear_input($this->input->post('mobile_type')); //android
        $rotation =  $this->clear_input($this->input->post('rotation'));
        $all_post=$this->clear_input($this->input->post('all_post'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }if(empty($address)) {
            $this->display_output('0','Please enter address.');
        }
        if(empty($latitude)) {
            $this->display_output('0','Please enter latitude.');
        }
        if(empty($longitude)) {
            $this->display_output('0','Please enter longitude.');
        }
        if(!isset($mobile_type)) {
            $mobile_type = 'ios';
            $rotation = -1;
        } else if($mobile_type=='android') {
            if(!isset($rotation)) {
                $err = array('data' =>array('status' => '0', 'msg' => 'Please enter rotation.'));
                echo json_encode($err); exit;
            } 
        }
        if(empty($post_content) && empty($all_post)){
             $this->display_output('0','Something went wrong.You try to submit blank post.');
        }
        //echo '<pre>';print_r($all_post);

        $insert_data=array(
            'user_id'=>$user_id,
            'post_content'=>$post_content,
            'post_address'=>$post_address,
            'latitude'=>$latitude,
            'longitude'=>$longitude,
            'created'=>date('Y-m-d H:i:s'),
            );
        $s3 = new S3(AMAZON_ID, AMAZON_KEY);
        echo '<pre>';print_r($_FILES);exit;
        $s3imagepath = $staging_id.'/'.$_FILES['image']['name'];
        $s3->putObjectFile($qr_image_path, AMAZON_BUCKET, $s3imagepath, S3::ACL_PUBLIC_READ); 
        unlink($qr_image_path);
        echo AMAZON_PATH.$s3imagepath;exit;
        $all_post_data=json_decode($all_post);
        $this->db->trans_begin();
        if($last_id = $this->Common_model->addEditRecords('post',$insert_data)){
            if(!empty($all_post_data)){
                $s3 = new S3(AMAZON_ID, AMAZON_KEY); 
                for($i=0;$i<count($all_post_data);$i++){
                    $post_data=array(
                        'post_id'=>$last_id,
                        'media_path'=>$all_post_data[$i]->media_name,
                        'media_type'=>$all_post_data[$i]->media_type,
                        'format'=>$all_post_data[$i]->format,
                    );
                    if($all_post_data[$i]->media_type=='video') {
                        $duration = $this->convert_duration($all_post_data[$i]->duration);
                        $post_data['duration']=$duration;
                        $s3thumbPath='';
                        $s3largeimagePath='';
                        if(!is_dir(MEDIA_THUMB_TEMP_PATH)){
                            mkdir(MEDIA_THUMB_TEMP_PATH);
                        }
                        $s3dir = $staging_id.'/';
                       
                        $images = createImage($all_post_data[$i]->media_name,MEDIA_THUMB_TEMP_PATH,"800x450",$all_post_data[$i]->duration,$mobile_type,$rotation);
                        $thumb_image = $images[1];
                        $large_image = $images[0];
                        $thumb_imagePath = MEDIA_THUMB_TEMP_PATH.$thumb_image;
                        $large_imagePath = MEDIA_THUMB_TEMP_PATH.$large_image;
                        //upload images on the amazon s3
                        
                        if($thumb_imagePath !='') {
                            //store thumb image on the amazon s3 and delete temp file
                            $save_path = $s3dir.$thumb_image;
                            $s3->putObjectFile($thumb_imagePath, AMAZON_BUCKET, $save_path, S3::ACL_PUBLIC_READ);
                            $s3thumbPath=AMAZON_PATH.$save_path;
                            if(file_exists($thumb_imagePath)) {
                                unlink($thumb_imagePath);
                            }
                            $post_data['video_thumbnail']=$s3thumbPath;
                        }

                        if($large_imagePath !='') {
                            //store thumb image on the amazon s3 and delete temp file
                            $save_path = $s3dir.$large_image;
                            $s3->putObjectFile($large_imagePath, AMAZON_BUCKET, $save_path, S3::ACL_PUBLIC_READ);
                            $s3largeimagePath=AMAZON_PATH.$save_path;
                            if(file_exists($large_imagePath)) {
                                unlink($large_imagePath);
                            }
                            $post_data['video_large_thumbnail']=$s3largeimagePath;
                        }
                    }
                }
                $this->Common_model->addEditRecords('post_media',$post_data);

            }
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////

    public function display_output($status,$msg,$data=array()){

        $response =  array('status'=>$status,'msg'=>$msg);
        if(!empty($data)){
           $response= array_merge($response,$data);
        }
        echo json_encode($response); 
        exit;
    }

    public function clear_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        //$data = htmlspecialchars($data);
        return $data;
    }

    

} //End controlller class




    
