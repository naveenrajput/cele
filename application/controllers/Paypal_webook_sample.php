<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Paypal_webook_sample extends CI_Controller {
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('paypal_lib');
        $this->load->model(array('admin/Common_model','admin/Order_model'));
        $this->load->helper('common_helper');
        $this->load->library('Ajax_pagination');
        $this->load->library('PHPExcel');
    }
public function payment_status() {

    /*$body = file_get_contents('php: //input');

    $data = json_decode($body, true);*/
    $webhook = new \PayPal\Api\Webhook();

    // Set webhook notification URL
    $webhook->setUrl("https://f615ef32.ngrok.io");

    // Set webhooks to subscribe to
    $webhookEventTypes = array();
    $webhookEventTypes[] = new \PayPal\Api\WebhookEventType(
      '{
        "name":"PAYMENT.SALE.COMPLETED"
      }'
    );

    $webhookEventTypes[] = new \PayPal\Api\WebhookEventType(
      '{
        "name":"PAYMENT.SALE.DENIED"
      }'
    );

    $webhook->setEventTypes($webhookEventTypes);
    echo '<pre>';print_r($webhook);exit;
    //Validate and verify webhook here.

    if ($data['event_type'] == 'PAYMENT.CAPTURE.COMPLETED') {
    $transaction_id = $data['event_type']['resource']['id'];
        if ($transactionData = $this->Common_model->getRecords('transaction_history','id',array('transaction_id'=>$transaction_id),'',true,'','',1)) {
        $id = $transactionData['id'];
                    $updateTransaction = array(
                        'status' => 'Completed',
                        'modified' => date('Y-m-d H:i:s'),
                    );

                $this->Common_model->addEditRecords('transaction_history', $updateTransaction,array('id'=>$id));
        }
    } else if ($data['event_type'] == 'PAYMENT.CAPTURE.DENIED') {
        //Payment capture denied.
        if ($transactionData = $this->Common_model->getRecords('transaction_history','id',array('transaction_id'=>$transaction_id),'',true,'','',1)) {
        $id = $transactionData['id'];
                    $updateTransaction = array(
                        'status' => 'Rejected',
                        'modified' => date('Y-m-d H:i:s'),
                    );
                $this->Common_model->addEditRecords('transaction_history', $updateTransaction,array('id'=>$id));
        }        
    } else if ($data['event_type'] == 'PAYMENT.CAPTURE.PENDING') {
        //Payment capture pending.
        if ($transactionData = $this->Common_model->getRecords('transaction_history','id',array('transaction_id'=>$transaction_id),'',true,'','',1)) {
        $id = $transactionData['id'];
                    $updateTransaction = array(
                        'status' => 'Pending',
                        'modified' => date('Y-m-d H:i:s'),
                    );
                $this->Common_model->addEditRecords('transaction_history', $updateTransaction,array('id'=>$id));
        }        
    } /*else if ($data['event_type'] == 'PAYMENT.CAPTURE.REFUNDED') {
        //Payment capture refunded.
    if ($transactionData = $this->Common_model->getRecords('transaction_history','id',array('transaction_id'=>$transaction_id),'',true,'','',1)) {
    $id = $transactionData['id'];
                $updateTransaction = array(
                    'status' => 'Refunded',
                    'modified' => date('Y-m-d H:i:s'),
                );
            $this->Common_model->addEditRecords('transaction_history', $updateTransaction,array('id'=>$id));
    }        
    } else if ($data['event_type'] == 'PAYMENT.CAPTURE.REVERSED') {
        //Payment capture reversed.
    if ($transactionData = $this->Common_model->getRecords('transaction_history','id',array('transaction_id'=>$transaction_id),'',true,'','',1)) {
    $id = $transactionData['id'];
                $updateTransaction = array(
                    'status' => 'Reversed',
                    'modified' => date('Y-m-d H:i:s'),
                );
            $this->Common_model->addEditRecords('transaction_history', $updateTransaction,array('id'=>$id));
    }        
    }*/
}
}
