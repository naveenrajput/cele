<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Deal extends CI_Controller {

	public function __construct() {
		parent::__construct();
        $this->load->library('paypal_lib');
		$this->load->model(array('admin/Common_model','Front_common_model','front/Booking_model','front/Home_model'));
		$this->load->helper('common_helper');
		$user_type = $this->session->userdata('front_user_type'); 
        
	}
	/////////////////////////////////service providers /////////////////////////////////

    public function manage_deal_list() 
    {
        
        $this->Common_model->check_user_login();
        $data['user_type']=$this->session->userdata('front_user_type');
       /* if($data['user_type']==2 || $data['user_type']==3){
           $doc_status=check_user_document_status();
            if($doc_status!='Accepted'){
                redirect('edit-profile');
            }  
        }*/
        $data['title']= 'Deal' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Deal'." | ".SITE_TITLE;
        $data['meta_desc']= 'Deal'." | ".SITE_TITLE;
        $data['page_title'] = "Deal";
        $data['page_sub_title'] = "Find best service provider's here";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Deals',
            'link' => ''
        );
        
       
        $data['total_records'] = $this->Booking_model->get_deals(0,0);
        //echo $this->db->last_query();exit;

        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/sp/deals_list');
        $this->load->view('front/include/footer');
    }
    public function manage_deal_list_data(){
        $this->Common_model->check_user_login();
        if(isset($_POST) && !empty($_POST)) {
            $page_number    = trim($_POST['page']);
            $item_per_page = FRONT_LIMIT;
            $offset = (($page_number) * $item_per_page);
            $name=isset($_POST['name'])?$_POST['name']:'';
            $status=isset($_POST['event_status'])?$_POST['event_status']:'';
            $data['bookings'] = $this->Booking_model->get_deals($offset,$item_per_page,$name,$status);
            $data['detail_action']='deals-details';
            echo $all_services = $this->load->view('front/sp/deals_list_data',$data,true); exit;
        }
    }
    public function deals_details($id) 
    {
        $this->Common_model->check_user_login();
        $data['user_type']=$this->session->userdata('front_user_type');
        $seg=$this->uri->segment(1);
        /*if($this->session->userdata('front_user_type')=='1')
        {
            redirect('home'); 
        }
        $doc_status=check_user_document_status();
        if($doc_status!='Accepted'){
            redirect('edit-profile');
        }  */
        $event_id=base64_decode($id);
        $data['title']= 'Deal Details' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Deal Details'." | ".SITE_TITLE;
        $data['meta_desc']= 'Deal Details'." | ".SITE_TITLE;
       
        $data['page_sub_title'] = "Find your all events history";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => base_url()
        );
        if($seg=='csp-booking-details'){
            $data['breadcrumbs'][] = array(
                'title' => 'My Bookings',
                'link' => 'csp-my-bookings'
            );
        }else{
            $data['breadcrumbs'][] = array(
                'title' => 'Deals',
                'link' => 'manage-deals'
            );
        }
        
        $data['breadcrumbs'][] = array(
            'title' => 'Deal Details',
            'link' => ''
        );
        
        $user_id= $this->session->userdata('user_id');
        $data['user_id']=$user_id;
        if(!$data['booking_details']=$this->Booking_model->deals_details($event_id,$user_id,'booking')) {
            redirect('pages/page_not_found');
        }
        
        $data['page_title'] = ucfirst($data['booking_details']['event_title']);
        $data['pending']=0;
        $data['accepted']=0;
        $data['rejected']=0;
        if($request_status = $this->Common_model->getRecords('event_attendee','count("status") as count,status',array('event_id'=>$event_id),'',false,'status')){
           foreach($request_status as $row){
                if($row['status']=='Accepted'){
                    $data['accepted']=$row['count'];
                }if($row['status']=='Rejected'){
                    $data['rejected']=$row['count'];
                }
                if($row['status']=='Pending'){
                    $data['pending']=$row['count'];
                }
           }
        }
        $deal_id = $this->Common_model->getFieldValue('events','deal_id',array('id'=>$event_id,'is_deleted'=>0),'',true);
        $event_id=$deal_id;
        $order_id = $this->Common_model->getFieldValue('orders','id',array('event_id'=>$event_id),'',true);
        $data['order_record']=$this->Booking_model->booking_detail_order($order_id);
        $data['items'] = $this->Common_model->getRecords('order_details','item_name,price,qty,unit,customer_qty,total_price,sell_price,tax,discount',array('event_id'=>$event_id,'is_deleted'=>0),'',false);

        $data['order_total']=$this->Common_model->getRecords('order_total','code,title,value,discount_value',array('order_id'=>$order_id,'status'=>'Active'),'',false);
        
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/sp/deals_details');
        $this->load->view('front/include/footer');
    }
    public function deals_filter(){
        $this->Common_model->check_user_login();
       if(isset($_POST) && !empty($_POST)) {
            $name=isset($_POST['name'])?$_POST['name']:'';
            $data['total_records'] = $this->Booking_model->get_deals(0,0,$name);
            echo $all_services =$this->load->view('front/sp/deals_list',$data,true);exit;
        }
    }
        /**************************************Add to deal  *************************/
    public function event_add_to_deal(){
        $this->Common_model->check_user_login();
        if($this->input->post()){
            $user_id= $this->session->userdata('user_id');
            $event_id  = base64_decode($this->input->post('event_id'));
            if(empty($event_id)) {
                display_output('0','Please enter event id.');
            }
            $event_details=$this->Common_model->getRecords('events','id,service_id,sp_id,event_title,event_address,members,country_id,state_id,city_id,latitude,longitude,event_date,event_type,note',array('id'=>$event_id),'',true);
            if($user_id!=$event_details['sp_id']){
                display_output('0','You are not authorize to convert this event in to deal.');
            }
            if($check_deal=$this->Common_model->getRecords('events','deal_id',array('deal_id'=>$event_id,'is_canceled'=>0),'',true)){
                display_output('0','This event already converted in deal.');
            }
            $current_date=convertGMTToLocalTimezone(date('Y-m-d H:i:s'));
            $current_time= convertGMTToLocalTimezone(date('Y-m-d H:i:s'),'',true);
            $curr_d=strtotime($current_date.' '.$current_time);
            $ev_date=convertGMTToLocalTimezone($event_details['event_date']);
            $ev_time= convertGMTToLocalTimezone($event_details['event_date'],'',true);
            $ev=strtotime($ev_date.' '.$ev_time);
            if($curr_d>$ev){ 
                display_output('0','Event date is passed away.');
            }
            if(!empty($event_details)){
                $event_data=array(
                    'deal_id'=>$event_details['id'],
                    'service_id'=>$event_details['service_id'],
                    'sp_id'=>$user_id,
                    'user_id'=>$user_id,
                    'event_title'=>$event_details['event_title'],
                    'event_address'=>$event_details['event_address'],
                    'country_id'=>$event_details['country_id'],
                    'state_id'=>$event_details['state_id'],
                    'city_id'=>$event_details['city_id'],
                    'latitude'=>$event_details['latitude'],
                    'longitude'=>$event_details['longitude'],
                    'event_date'=>$event_details['event_date'],
                    'event_type'=>$event_details['event_type'],
                    'note'=>$event_details['note'],
                    'created' => date('Y-m-d H:i:s'),
                );
                $this->db->trans_begin();
                    $event_id = $this->Common_model->addEditRecords('events',$event_data);
                    $event_number='E'.str_pad($event_id,8,0,STR_PAD_LEFT);
                    $this->Common_model->addEditRecords('events',array('event_number'=>$event_number),array('id'=>$event_id));
                    $frontend_username = getFrontUsername($user_id);
                    $log_msg = getUserType()." ".$frontend_username." convert ".$event_details['event_title']." in to deal.";
                    actionLog('events',$event_id,'add_deal',$log_msg,'User',$user_id); 
                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    display_output('0','Something went wrong. Please try again.'); 
                } else {
                    $this->db->trans_commit();
                    display_output('1','Deals added successfully.');
                }
                
            }else{
                display_output('0','Event not found.');
            }
        }else{
            display_output('0','Some error occured! Please try again.');
        }
    }                                                                                                                    
    public function grab_deal(){
       // echo '<pre>';print_r($_POST);exit;
        $this->Common_model->check_user_login();
        if($this->input->post()) {
            $user_id=$this->session->userdata('user_id');
            $event_id=base64_decode($_POST['event_id']);
            
            if(!$event_details=$this->Common_model->getRecords('events','id,sp_id,is_deal_done,event_date',array('id'=>$event_id),'',true)){
                display_output('0','Event not available.');                                                                                                        
            }
            if($user_id==$event_details['sp_id']){
                display_output('0','You are not authorize to grab your own deal.');
            }
            if($event_details['is_deal_done']==1){
                 display_output('0','This deal already sold out.');
            }
            $current_date=convertGMTToLocalTimezone(date('Y-m-d H:i:s'));
            $current_time= convertGMTToLocalTimezone(date('Y-m-d H:i:s'),'',true);
            $curr_d=strtotime($current_date.' '.$current_time);
            $ev_date=convertGMTToLocalTimezone($event_details['event_date']);
            $ev_time= convertGMTToLocalTimezone($event_details['event_date'],'',true);
            $ev=strtotime($ev_date.' '.$ev_time);
            if($curr_d>$ev){ 
                display_output('0','Deal date is passed away.');
            }
            $update_data=array(
                'is_deal_done'=>1,
                'modified'=>date('Y-m-d H:i:s'),
                );
            $this->db->trans_begin();
            $this->Common_model->addEditRecords('events',$update_data,array('id'=>$event_id));
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                display_output('0','Something went wrong. Please try again.'); 
            } else {
               
                display_output('1',base64_encode($event_id));
            }
        }else{
            display_output('0','Something went wrong. Please try again.');
        }
    }
    function deal_make_payment($event_ids)
    {  
            $event_id=base64_decode($event_ids);
            $user_id=$this->session->userdata('user_id');
            if(!$event_details=$this->Common_model->getRecords('events','id,deal_id,user_id,is_deal_done',array('id'=>$event_id),'',true)){
                $this->session->set_flashdata('error', 'Event not available.');
                redirect('manage-deals');                                                                                      
            }
            /*if(($event_details['user_id']!=$user_id) && $event_details['is_deal_done']==1){
                $this->session->set_flashdata('error', 'Deal is not available.');
                redirect('manage-deals'); 
            }*/
            $order_info=$this->Common_model->getRecords('orders','id,order_number,total_amount',array('event_id'=>$event_details['deal_id']),'',true);
            $sub_item_total_price=$order_info['total_amount'];
            $returnURL = base_url().'deal-notify-payment';
            $cancelURL = base_url().'manage-deals';
            $notifyURL =base_url().'deal-notify-payment';
            $timezone =$this->session->userdata('user_timezone');
            $this->paypal_lib->add_field('return', $returnURL);
            $this->paypal_lib->add_field('cancel_return', $cancelURL);
            $this->paypal_lib->add_field('notify_url', $notifyURL);
            $this->paypal_lib->add_field('item_name', $timezone);
            $this->paypal_lib->add_field('custom', '');
            $this->paypal_lib->add_field('item_number',$event_ids);
            $this->paypal_lib->add_field('amount',  $sub_item_total_price);
            // Render paypal form
            $this->paypal_lib->paypal_auto_form();
        
    }

    function deal_notify_payment(){
        //echo '<pre>';print_r($_SESSION);
        $user_id=$this->session->userdata('user_id');
        $this->load->model('App_model');
        //$timezone =$this->session->userdata('admin_timezone');
        $timezone =$this->session->userdata('user_timezone');
        $paypalInfo = $this->input->post();
        if(!empty($paypalInfo)){
            //echo '<pre>';print_r($paypalInfo);
            if(!empty($paypalInfo["txn_id"])){
                $ipnCheck = $this->paypal_lib->validate_ipn($paypalInfo);
                if($ipnCheck){
                    $event_id=base64_decode($paypalInfo["item_number1"]);
                    //$sp_id=$this->Common_model->getFieldValue('events','sp_id',array('id'=>$event_id),'',true);
                     $update_event_info=array(
                        'is_deal_done'=>0,
                        'modified'=>date('Y-m-d H:i:s'),
                    );
                    $event_info=$this->Common_model->getRecords('events','user_id,sp_id,deal_id,event_title,event_type,service_id,event_date,event_date',array('id'=>$event_id,'status'=>'Active','type'=>'Draft'),'',true);
                    if(empty($event_info)){
                        //echo $this->db->last_query();exit;
                        $this->Common_model->addEditRecords('events',$update_event_info,array('id'=>$event_id));
                        $this->session->set_flashdata('error', 'Payment failed due to some malicious activity.');
                        redirect('service-providers');
                    }
                    if(!isset($_SESSION['user_id']) && empty($_SESSION['user_id'])){
                        $user_id=$event_info['user_id'];
                        if($user_data = $this->Common_model->getRecords('users','user_id,fullname,staging_id,mobile,user_type,email,status',array('user_id'=>$user_id),'user_id DESC',true)) {
                            $login=array(   
                                'user_id'=>$user_data['user_id'],
                                'fullname'=>$user_data['fullname'],
                                'staging_id'=>$user_data['staging_id'],
                                'mobile'=>$user_data['mobile'],
                                'email'=>$user_data['email'],
                                'front_user_type'=>$user_data['user_type'],
                                'status'=>$user_data['status'],
                                );
                            $this->session->set_userdata($login);
                            $this->session->set_userdata('user_timezone',$paypalInfo["item_name1"]);
                        }
                       
                    }
                    $update_event=array('user_id'=>$user_id,'type'=>'Actual','modified'=>date('Y-m-d H:i:s'));
                    $this->db->trans_begin();
                    $this->Common_model->addEditRecords('events',$update_event,array('id'=>$event_id));
                    $order_details=$this->Common_model->getRecords('orders','id,total_amount,redeem_payment,admin_commission_amount',array('event_id'=>$event_info['deal_id']),'',true);
                    $order_data=array('user_id'=>$user_id,
                                'fullname'=>$this->session->userdata('fullname'),
                                'email'=>$this->session->userdata('email'),
                                'mobile'=>$this->session->userdata('mobile'),
                                'event_id'=>$event_id,
                                'event_date_time'=>$event_info['event_date'],
                                'service_id'=>$event_info['service_id'],
                                'type'=>'Actual',
                                'ip_address'=>$this->input->ip_address(),
                                'payment_status'=>'Paid',
                                'payment_date'=>date('Y-m-d H:i:s'),
                                'order_status'=>'Confirmed',
                                'total_amount'=>$order_details['total_amount'],
                                'redeem_payment'=>$order_details['redeem_payment'],
                                'admin_commission_amount'=>$order_details['admin_commission_amount'],
                                'created'=>date('Y-m-d H:i:s')
                    );
                    if($order_id = $this->Common_model->addEditRecords('orders',$order_data)){
                        $order_number=str_pad($order_id,8,0,STR_PAD_LEFT);
                        $this->Common_model->addEditRecords('orders',array('order_number'=>$order_number),array('id'=>$order_id));
                        if($order_items = $this->Common_model->getRecords('order_details','item_name,price,qty,total_qty,unit,customer_qty,total_price,sell_price,tax,discount,offer_name',array('order_id'=>$order_details['id']),'',false)){
                            $max_group_id=$this->Common_model->getRecords('order_status_history','max(`order_batch_id`) as order_group_id','','',true);
                            $order_history_details_max_id=$max_group_id['order_group_id']+1;
                            foreach ($order_items as $key => $item) {
                                $item_arr[]=array(
                                    'order_group_id'=>$order_history_details_max_id,
                                    'service_id'=>$event_info['service_id'],
                                    'order_id'=>$order_id,
                                    'event_id'=>$event_id,
                                    'event_date_time'=>$event_info['event_date'],
                                    'item_name'=>$item['item_name'],
                                    'price'=>$item['price'],
                                    'qty'=>$item['qty'],
                                    'total_qty'=>$item['total_qty'],
                                    'unit'=>$item['unit'],
                                    'customer_qty'=>$item['customer_qty'],
                                    'total_price'=>$item['total_price'],
                                    'sell_price'=>$item['sell_price'],
                                    'tax'=>$item['tax'],
                                    'discount'=>$item['discount'],
                                    'offer_name'=>$item['offer_name'],
                                    'type'=>'Actual',
                                    'created'=>date('Y-m-d H:i:s')
                                    );
                            }
                            $this->db->insert_batch('order_details', $item_arr);
                            $order_status=array(
                                'order_batch_id'=>$order_history_details_max_id,
                                'order_id'=>$order_id,
                                'status'=>'Accepted',
                                'type'=>'Actual',
                                'created'=>date('Y-m-d H:i:s'),
                                'ip_addres'=>$this->input->ip_address(),
                                );
                            $this->Common_model->addEditRecords('order_status_history',$order_status);
                            if($order_amt = $this->Common_model->getRecords('order_total','code,title,value,discount_value,sort_order',array('order_id'=>$order_id),'',false)){
                                foreach ($order_amt as $key => $total) {
                                   $total_arr[]=array(
                                    'order_batch_id'=>$order_history_details_max_id,
                                    'order_id'=>$order_id,
                                    'code'=>$total['code'],
                                    'title'=>$total['title'],
                                    'value'=>$total['value'],
                                    'discount_value'=>$total['discount_value'],
                                    'sort_order'=>$total['sort_order'],
                                    'status'=>'Active',
                                    'type'=>'Actual',
                                    'created'=>date('Y-m-d H:i:s')
                                    );
                                }
                                $this->db->insert_batch('order_total',$total_arr);
                            }
                            $paypal_fees = $paypalInfo["payment_fee"];  // paypal fee
                            $payment_history=array(
                                'order_id'=>$order_id,
                                'transaction_id'=>$paypalInfo["txn_id"],
                                'total_order_amount'=>$order_details['total_amount'],
                                'paid_amount'=>$order_details['total_amount'],
                                'customer_id'=>$user_id,
                                'service_provider_id'=>$event_info['sp_id'],
                                'payment_type'=>'Order',
                                'transfer_type'=>'Customer_To_Admin',
                                'paypal_fee_amount'=>$paypal_fees,
                                'user_will_get'=>($order_details['total_amount']-$paypal_fees),
                                'payment_status'=>$paypalInfo["payment_status"],
                                'created'=>date('Y-m-d H:i:s'),
                                'ip_addres'=>$this->input->ip_address(),
                                );
                            $this->Common_model->addEditRecords('payment_history',$payment_history);

                            }
                            spFriends($user_id,$event_info['sp_id']);
                    }
                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE){
                        $this->db->trans_rollback();
                        display_output('0','Something went wrong. Please try again.'); 
                    } else {
                        $this->db->trans_commit();
                        $this->Front_common_model->send_order_invoice($order_id,'Customer_To_Admin',$timezone);
                        $title= 'New event created.';
                        $content='Thank you for your order purchase your order number is #'.$order_number.'.';
                        $notdata['type']='new_event';
                        $this->Common_model->push_notification_send($user_id,$notdata,$event_id,$title,$content,'',$user_id);
                        $title1= 'New Order.';
                        $content1='A new order is placed for you order number is #'.$order_number.'.';
                        $notdata1['type']='new_order';
                        $this->Common_model->push_notification_send($event_info['sp_id'],$notdata1,$event_id,$title1,$content1,'',$user_id);
                        $frontend_username = getFrontUsername($user_id);
                        $log_msg = getUserType()." ".$frontend_username." has created a new event '".$event_title."'.";
                        actionLog('events',$event_id,'add',$log_msg,'User',$user_id); 

                        $this->session->set_flashdata('success', 'Deal confirmed.');
                        redirect('my-events');
                    }
                }else{
                    $this->Common_model->addEditRecords('events',$update_event_info,array('id'=>$event_id));
                    $this->session->set_flashdata('error', 'Payment failed.');
                    redirect('manage-deals');
                }
            }else{

                $this->Common_model->addEditRecords('events',$update_event_info,array('id'=>$event_id));
                $this->session->set_flashdata('error', 'Payment failed.');
                redirect('manage-deals');
            }
        }else{
            $this->Common_model->addEditRecords('events',$update_event_info,array('id'=>$event_id));
            $this->session->set_flashdata('error', 'Some error occured in payment.');
            redirect('manage-deals');
        }
    }

    
}
