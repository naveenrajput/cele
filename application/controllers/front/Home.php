<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
		parent::__construct();
        error_reporting(E_ALL);
        error_reporting(1);
        ini_set('max_execution_time', 0);
        ini_set('max_input_time', 0);

        // echo ini_get("memory_limit")."\n";
        ini_set("memory_limit","20000M");
        $this->load->library(array('upload','S3'));
		$this->load->model(array('admin/Common_model','Front_common_model', 'front/Home_model'));
		$this->load->helper('common_helper');
        if(!empty($this->session->userdata('front_user_type'))){
    		$user_type = $this->session->userdata('front_user_type');

            if($user_type=='2')
            {
                redirect(base_url());
            }
        }

	}


	public function index()
	{
        //echo phpinfo();exit;    
        $data['title']= 'Home' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Home'." | ".SITE_TITLE;
        $data['meta_desc']= 'Home'." | ".SITE_TITLE;
        $data['page_title'] = "Home";

       	$this->Common_model->check_user_login();
        if($this->session->userdata('front_user_type')=='2')
        {
            redirect('home'); 
        }
        $user_id  = $this->session->userdata('user_id');
        $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);
        $data['top_sp']=$this->Home_model->top_rated_sp($user_address['country'],$user_address['state'],$user_address['city']);

        $data['total_records']=$this->Home_model->get_feeds(0,0,$user_id);
        
        $data['be_my_date']= $this->Home_model->be_my_date_list(3,3,$user_id,'limit');

        $data['service_category']=$this->Common_model->getRecords('service_category','id,title',array('is_deleted'=>0,'status'=>'Active'),'',false);

        //echo $this->db->last_query();exit;
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/slider');
        $this->load->view('front/celebrant/home');
        $this->load->view('front/include/footer');
	}
    public function feeds_data(){
        $user_id  = $this->session->userdata('user_id');
        $data['user_id']=$user_id;
        $this->Common_model->check_user_login();
        if(isset($_POST) && !empty($_POST)) {
            $page_number    = trim($_POST['page']);
            $item_per_page = FRONT_LIMIT;
            $offset = (($page_number) * $item_per_page);
            $user_post_data = $this->Home_model->get_feeds($offset,$item_per_page,$user_id);
            // /echo $this->db->last_query();
            $details=array();
            if(!empty($user_post_data)){ 
            $i=0;
            foreach ($user_post_data as $key => $value) { 
                //echo '<pre>';print_r($value);
                $where="post_id='".$value['post_id']."'";
                $like=$this->Home_model->get_count('like_post',$where);
                $comment=$this->Home_model->get_count('comments',$where);
                $where_shared="shared_post_id='".$value['post_id']."'";
                $share=$this->Home_model->get_count('post',$where_shared);
                $details[$i]['like']=shortNumber($like['count']);
                $details[$i]['comment']=shortNumber($comment['count']);
                $details[$i]['share']=shortNumber($share['count']);
                $details[$i]['request_id']=$value['request_id'];
                $details[$i]['is_like_by_me']=$value['is_like_by_me'];
                $details[$i]['is_report_by_me']= $this->Common_model->getFieldValue('post_report','id',array('user_id'=>$user_id,'post_id'=>$value['post_id']),'',true);
                $details[$i]['is_saved_by_me']= $this->Common_model->getFieldValue('save_post','id',array('user_id'=>$user_id,'post_id'=>$value['post_id']),'',true);
               // $details[$i]['is_event_completed']= $this->Common_model->getFieldValue('events','is_completed',array('id'=>$value['event_id']),'',true);
                if($value['shared_post_id']>0){
                    $details[$i]['is_orignal_post_deleted']= $this->Common_model->getFieldValue('post','is_deleted',array('id'=>$value['shared_post_id']),'',true);
                    $post_media= $this->Common_model->getRecords('post_media','media_path,video_thumbnail,video_large_thumbnail,media_type,format,duration',array('media_is_deleted'=>0,'post_id'=>$value['shared_post_id']),'',false);
                    $details[$i]['post_id']=$value['post_id'];
                    $details[$i]['post_number']=$value['post_number'];
                    $details[$i]['user_id']=$value['user_id'];
                    $details[$i]['sp_id']=$value['sp_id'];
                    $details[$i]['follower_id']=$value['follower_id'];
                    $details[$i]['fullname']=$value['fullname'];
                    $details[$i]['post_type']=$value['post_type'];
                    $details[$i]['event_id']=$value['event_id'];
                    $details[$i]['profile_picture']=$value['profile_picture'];
                    $details[$i]['gender']=$value['gender'];
                    $details[$i]['created']=$value['created'];
                    $details[$i]['is_shared']=1;
                    $details[$i]['shared_post_id']=$value['shared_post_id'];
                    $details[$i]['post_content']=$value['post_content'];
                    $details[$i]['shared_post_content']=$value['shared_post_content'];
                    $details[$i]['post_media']=$post_media;
                    
                }else{
                    $post_media= $this->Common_model->getRecords('post_media','media_path,video_thumbnail,video_large_thumbnail,media_type,format,duration',array('media_is_deleted'=>0,'post_id'=>$value['post_id']),'',false);
                    $details[$i]['post_id']=$value['post_id'];
                    $details[$i]['post_number']=$value['post_number'];
                    $details[$i]['user_id']=$value['user_id'];
                    $details[$i]['sp_id']=$value['sp_id'];
                    $details[$i]['follower_id']=$value['follower_id'];
                    $details[$i]['fullname']=$value['fullname'];
                    $details[$i]['post_type']=$value['post_type'];
                    $details[$i]['event_id']=$value['event_id'];
                    $details[$i]['profile_picture']=$value['profile_picture'];
                    $details[$i]['gender']=$value['gender'];
                    $details[$i]['created']=$value['created'];
                    $details[$i]['is_shared']=0;
                    $details[$i]['shared_post_id']=0;
                    $details[$i]['post_content']=$value['post_content'];
                    $details[$i]['post_media']=$post_media;
                }
              $i++;  
            }
        }
        $data['news_feeds']=$details;
        //echo '<pre>';print_r($data['news_feeds']);exit;
        echo $all_feeds = $this->load->view('front/celebrant/feeds_data',$data,true); exit;
        }
    }
    public function get_post_media($post_id){
        //$this->Common_model->check_user_login();
        $post_id=base64_decode($post_id);
        $data['details']= $this->Common_model->getRecords('post_media','media_path,media_type',array('post_id'=>$post_id),'',false);
        return $this->load->view('front/celebrant/home_slider',$data);
    }
    public function get_post_content($post_number){
        //$this->Common_model->check_user_login();
        $post_number=base64_decode($post_number);
        $data['details']= $this->Common_model->getRecords('post','id,post_number,post_content',array('post_number'=>$post_number),'',true);
        if($data['details']){
            if($data['media']= $this->Common_model->getRecords('post_media','id',array('post_id'=>$data['details']['id']),'',true)){
                $data['media_exist']=1;
            }else{
                $data['media_exist']=0;
            }

        }
        return $this->load->view('front/celebrant/edit_post',$data);
    }

    /* post details   start  */
    public function post_details($post_num) 
    {
        $data['title']= 'Post Details' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Post Details'." | ".SITE_TITLE;
        $data['meta_desc']= 'Post Details'." | ".SITE_TITLE;
        $data['page_title'] = "Post Details";
        $data['page_sub_title'] = "";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => 'home'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Post Details',
            'link' => ''
        );
        $data['post_num']=$post_num;
        $this->Common_model->check_user_login();
        $user_id= $this->session->userdata('user_id');
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/celebrant/post_detail');
        $this->load->view('front/include/footer');
    }
    public function post_details_data(){
        if(isset($_POST) && !empty($_POST)) {
            $data['post_detail_page_type']='1';
            $post_number=base64_decode($_POST['post_num']);
            if($user_post_data= $this->Home_model->post_detail($post_number)) { 
                 $details=array();
                if(!empty($user_post_data)){
                    $i=0;
                    foreach ($user_post_data as $key => $value) { 
                        //echo '<pre>';print_r($value);
                        $where="post_id='".$value['post_id']."'";
                        $like=$this->Home_model->get_count('like_post',$where);
                        $comment=$this->Home_model->get_count('comments',$where);
                        $where_shared="shared_post_id='".$value['post_id']."'";
                        $share=$this->Home_model->get_count('post',$where_shared);
                        $details[$i]['like']=shortNumber($like['count']);
                        $details[$i]['comment']=shortNumber($comment['count']);
                        $details[$i]['share']=shortNumber($share['count']);
                        $details[$i]['request_id']=$value['request_id'];
                        $details[$i]['is_like_by_me']=$value['is_like_by_me'];
                        $details[$i]['is_like_by_me']= $this->Common_model->getFieldValue('like_post','id',array('user_id'=>$user_id,'post_id'=>$value['post_id']),'',true);
                        $details[$i]['is_report_by_me']= $this->Common_model->getFieldValue('post_report','id',array('user_id'=>$user_id,'post_id'=>$value['post_id']),'',true);
                        if($value['shared_post_id']>0){
                            $details[$i]['is_orignal_post_deleted']= $this->Common_model->getFieldValue('post','is_deleted',array('id'=>$value['shared_post_id']),'',true);
                            $post_media= $this->Common_model->getRecords('post_media','media_path,video_thumbnail,video_large_thumbnail,media_type,format,duration',array('media_is_deleted'=>0,'post_id'=>$value['shared_post_id']),'',false);

                            $details[$i]['post_id']=$value['post_id'];
                            $details[$i]['post_number']=$value['post_number'];
                            $details[$i]['user_id']=$value['user_id'];
                            $details[$i]['fullname']=$value['fullname'];
                            $details[$i]['post_type']=$value['post_type'];
                            $details[$i]['event_id']=$value['event_id'];
                            $details[$i]['profile_picture']=$value['profile_picture'];
                            $details[$i]['gender']=$value['gender'];
                            $details[$i]['created']=$value['created'];
                            $details[$i]['is_shared']=1;
                            $details[$i]['shared_post_id']=$value['shared_post_id'];
                            $details[$i]['post_content']=$value['post_content'];
                            $details[$i]['shared_post_content']=$value['shared_post_content'];
                            $details[$i]['post_media']=$post_media;
                        }else{
                            $post_media= $this->Common_model->getRecords('post_media','media_path,video_thumbnail,video_large_thumbnail,media_type,format,duration',array('media_is_deleted'=>0,'post_id'=>$value['post_id']),'',false);
                            $details[$i]['post_id']=$value['post_id'];
                            $details[$i]['post_number']=$value['post_number'];
                            $details[$i]['user_id']=$value['user_id'];
                            $details[$i]['fullname']=$value['fullname'];
                            $details[$i]['post_type']=$value['post_type'];
                            $details[$i]['event_id']=$value['event_id'];
                            $details[$i]['profile_picture']=$value['profile_picture'];
                            $details[$i]['gender']=$value['gender'];
                            $details[$i]['created']=$value['created'];
                            $details[$i]['is_shared']=0;
                            $details[$i]['shared_post_id']=0;
                            $details[$i]['post_content']=$value['post_content'];
                            $details[$i]['post_media']=$post_media;
                        }
                      $i++;  
                    }
                }
            }
            $data['news_feeds']=$details;
            echo $all_feeds = $this->load->view('front/celebrant/feeds_data',$data,true); exit;
        }
    }
    /* post detail   end  */
    /* post submission   start  */
    public function post_content()
    {
        $this->Common_model->check_user_login();
        $user_id  = $this->session->userdata('user_id');
        $staging_id  = $this->session->userdata('staging_id');
        if(!empty($_POST) || !empty($_FILES)){
            $insert_data=array(
            'user_id'=>$user_id,
            'post_content'=>$this->input->post('post_content'),
            'created'=>date('Y-m-d H:i:s'),
            );
           $this->db->trans_begin();
           if($last_id = $this->Common_model->addEditRecords('post',$insert_data)){
            $post_number='Post'.str_pad($last_id,8,0,STR_PAD_LEFT);
            $this->Common_model->addEditRecords('post',array('post_number'=>$post_number),array('id'=>$last_id));
                if(!empty($_FILES)){
                    $s3 = new S3(AMAZON_ID, AMAZON_KEY);
                    if(!is_dir(MEDIA_THUMB_TEMP_PATH)){
                        mkdir(MEDIA_THUMB_TEMP_PATH);
                    } 
                    for($i=0;$i<count($_FILES['file_upload']);$i++){
                        if(!empty($_FILES['file_upload']['name'][$i])){
                            $tmp_name = $_FILES['file_upload']['tmp_name'][$i];
                            $ext = strtolower(substr(strrchr($_FILES['file_upload']['name'][$i], '.'), 1)); 
                            $thumb_image="";
                            $thumb_imagePath="";
                            $large_image="";
                            $large_imagePath="";
                            if($ext=='avi' || $ext=='mov' || $ext=='mp4') {
                                $duration = $this->get_duration($tmp_name);
                                $media_name = 'video'.'_'.$i.time().'_v.mp4';
                                
                                $images = createImage($tmp_name,MEDIA_THUMB_TEMP_PATH,"800x450",$duration);
                                ;
                               
                                $thumb_image = $images[1];
                                $large_image = $images[0];
                                $thumb_imagePath = MEDIA_THUMB_TEMP_PATH.$thumb_image;
                                $large_imagePath = MEDIA_THUMB_TEMP_PATH.$large_image;
                                //Convert and upload video on server
                                $newSource =MEDIA_PATH.$media_name;
                                $command = 'ffmpeg -i ' . $tmp_name . ' -r 21 -vcodec libx264 -b:v 1M -crf 32 -preset veryfast -profile:v baseline -level 3.0 -c:a copy -movflags +faststart -tune zerolatency '.$newSource ;
                                $output = shell_exec($command);
                                $s3dir = $staging_id.'/';
                                $savemediapath = $s3dir.$media_name;
                                if($s3->putObjectFile($newSource, AMAZON_BUCKET, $savemediapath, S3::ACL_PUBLIC_READ)) 
                                {
                                    //upload media on the amazon s3
                                    $s3mediapath =AMAZON_PATH.$savemediapath;
                                    $s3thumbPath='';
                                    $s3largeimagePath='';
                                    if($thumb_imagePath !='') {
                                        //store thumb image on the amazon s3 and delete temp file
                                        $save_path = $s3dir.$thumb_image;
                                        $s3->putObjectFile($thumb_imagePath, AMAZON_BUCKET, $save_path, S3::ACL_PUBLIC_READ);
                                        $s3thumbPath=AMAZON_PATH.$save_path;
                                        if(file_exists($thumb_imagePath)) {
                                            unlink($thumb_imagePath);
                                        }
                                    }
                                    if($large_imagePath !='') {
                                        //store thumb image on the amazon s3 and delete temp file
                                        $save_path = $s3dir.$large_image;
                                        $s3->putObjectFile($large_imagePath, AMAZON_BUCKET, $save_path, S3::ACL_PUBLIC_READ);
                                        $s3largeimagePath=AMAZON_PATH.$save_path;
                                        if(file_exists($large_imagePath)) {
                                            unlink($large_imagePath);
                                        }
                                    }
                                    $insert_array[]=array(
                                        'post_id'=>$last_id,
                                        'user_id'=>$user_id,
                                        'media_path' => $s3mediapath,
                                        'video_thumbnail' => $s3thumbPath,
                                        'video_large_thumbnail' => $s3largeimagePath,
                                        'duration' => $duration,
                                        'media_type' => 'video',
                                        'created' => date('Y-m-d H:i:s'),
                                    );
                                   
                                }
                            }
                            if($ext=='jpg' || $ext=='png' || $ext=='jpeg') {

                                $_FILES['images']['name'] = $_FILES['file_upload']['name'][$i];
                                $_FILES['images']['type'] = $_FILES['file_upload']['type'][$i];
                                $_FILES['images']['tmp_name'] =$_FILES['file_upload']['tmp_name'][$i];
                                $_FILES['images']['error'] = $_FILES['file_upload']['error'][$i];
                                $_FILES['images']['size'] =  $_FILES['file_upload']['size'][$i]; 
                                //Rename image name 
                                $img = time().'_'.rand(); 
                                $config['upload_path'] = MEDIA_PATH;
                                //$config['allowed_types'] = 'jpg|png|mp4|mov|jpeg|JPG|PNG|MP4|MOV|JPEG';
                                $config['allowed_types'] = '*';
                                $config['file_name'] =  $img; 
                                $this->load->library('upload', $config);
                                $this->upload->initialize($config); 
                                if($this->upload->do_upload('images')){
                                    $fileData = $this->upload->data();
                                    $newSource=compress(MEDIA_PATH.$config['file_name'].$fileData['file_ext']);
                                    $media_name = 'image'.'_'.$i.time().'_v.png';
                                    $s3dir = $staging_id.'/';
                                    $savemediapath = $s3dir.$media_name;
                                    if($s3->putObjectFile($newSource, AMAZON_BUCKET, $savemediapath, S3::ACL_PUBLIC_READ)) 
                                    {
                                        $s3mediapath =AMAZON_PATH.$savemediapath;
                                        $insert_array[]=array(
                                            'post_id'=>$last_id,
                                            'user_id'=>$user_id,
                                            'media_path' => $s3mediapath,
                                            'video_thumbnail' =>'',
                                            'video_large_thumbnail' =>'',
                                            'duration' =>'',
                                            'media_type' => 'image',
                                            'created' => date('Y-m-d H:i:s'),
                                        );
                                        if(file_exists($newSource)) {
                                            unlink($newSource);
                                        }
                                    }   
                                }else{
                                    display_output('0','Some error occured in image upload.');
                                } 
                                /////////////////////////////////////////////
                                
                            }
                        }
                    }
                    if(isset($insert_array) && !empty($insert_array)){
                         $this->db->insert_batch('post_media', $insert_array);
                    }
                }
            }
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                display_output('0','Something went wrong. Please try again.'); 
            } else {
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getUserType()." ".$frontend_username." has added a post '".$post_number."'.";
                actionLog('post',$last_id,'Add',$log_msg,'User',$user_id);

                $this->db->trans_commit();
                display_output('1','Post created successfully.');
            }

        }else{
            display_output('0','Something went wrong. Please try again.');
        } 
    }
   
    public function get_duration($srcFile) {
        // Change the path according to your server.
        if(!empty($srcFile)) {
            $command = "ffmpeg -i $srcFile -vstats 2>&1";
            $output = shell_exec($command);
            $regex_duration = "/Duration: ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2}).([0-9]{1,2})/";
            $duration=0;
            if (preg_match($regex_duration, $output, $regs)) {
                $hours = $regs [1] ? $regs [1] : null;
                $mins = $regs [2] ? $regs [2] : null;
                $secs = $regs [3] ? $regs [3] : null;
                $ms = $regs [4] ? $regs [4] : null;
                //$duration = $hours.':'. $mins.':'. $secs.':'. $ms; 
                $duration = ($hours*3600) + ($mins*60) + $secs + ($ms/1000);
                //$duration = gmdate("H:i:s", $duration);
            }
            return $duration;
        }
    }
    /* post submission ends */
    /* update post submission start */
    public function update_post_content()
    {
        $user_id  = $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        if(!empty($_POST)){
        $post_number  = base64_decode($this->input->post('post_num'));
        $post_content  = $this->input->post('post_content');
        if(!$post_owner=$this->Common_model->getRecords('post','id,user_id',array('post_number'=>$post_number,'is_deleted'=>0,'status'=>'Active'),'',true)){
             display_output('0','Post isn\'t available now.');
        }
        $post_id=$post_owner['id'];
        if($post_owner['user_id']!=$user_id){
            display_output('0','You cannot edit others post.');
        }
        $post_id=$post_owner['id'];
        if($media= $this->Common_model->getRecords('post_media','id',array('post_id'=>$post_id),'',true)){
                $media_exist=1;
        }else{
            $media_exist=0;
        }
        if($media_exist==0 && empty($post_content)){
            display_output('0','You cannot be update blank post.'); 
        }

            $update_data=array(
            'user_id'=>$user_id,
            'post_content'=>$post_content,
            'modified'=>date('Y-m-d H:i:s'),
            );
            $this->db->trans_begin();
            $this->Common_model->addEditRecords('post',$update_data,array('id'=>$post_id));
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                display_output('0','Something went wrong. Please try again.'); 
            } else {
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getUserType()." ".$frontend_username." has updated a post '".$post_number."'.";
                actionLog('post',$post_id,'Update',$log_msg,'User',$user_id);
                $this->db->trans_commit();
                display_output('1','Post updated successfully.');
            }
        }else{
            display_output('0','Something went wrong. Please try again.');
        } 
    }
    /* update post submission end */
    /* Share post start */

    public function get_share_post_content($post_number){
        //$this->Common_model->check_user_login();
        $post_number=base64_decode($post_number);
        $data['shared_details']= $this->Common_model->getRecords('post','id,shared_post_id',array('post_number'=>$post_number),'',true);

        if($data['shared_details']){
            if($data['shared_details']['shared_post_id']>0){
                $post_id=$data['shared_details']['shared_post_id'];
            }else{
                $post_id=$data['shared_details']['id'];
            }
            $data['details']= $this->Common_model->getRecords('post','id,post_number,post_content',array('id'=>$post_id),'',true);
            $data['media']= $this->Common_model->getRecords('post_media','media_path,video_thumbnail,media_type',array('post_id'=>$post_id),'',false);
        }
        return $this->load->view('front/celebrant/share_post',$data);
    }
    public function share_post(){
        $user_id  = $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        if(!empty($_POST)){
            //echo "<pre>";print_r($_POST);die;
            $post_number  = base64_decode($this->input->post('post_num'));
            $post_content  = $this->input->post('post_content');
            if(!$post_owner=$this->Common_model->getRecords('post','id,user_id,shared_post_id',array('post_number'=>$post_number,'is_deleted'=>0,'status'=>'Active'),'',true)){
                 display_output('0','Post isn\'t available now.');
            }
            $post_id=$post_owner['id'];
            if($post_owner['shared_post_id']>0){
                $shared_id=$post_owner['shared_post_id'];
            }else{
                $shared_id=$post_id;
            }
            $insert_data=array(
            'user_id'=>$user_id,
            'shared_post_id'=>$shared_id,
            'post_content'=>$post_content,
            'created'=>date('Y-m-d H:i:s'),
            );
            if($last_id=$this->Common_model->addEditRecords('post',$insert_data)){
                $post_number='Post'.str_pad($last_id,8,0,STR_PAD_LEFT);
                $this->Common_model->addEditRecords('post',array('post_number'=>$post_number),array('id'=>$last_id));
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getUserType()." ".$frontend_username." has shared a post '".$post_number."'.";
                actionLog('post',$last_id,'Share Post',$log_msg,'User',$user_id);
                display_output('1','Post shared successfully.');
            }else{
                display_output('0','Something went wrong. Please try again.');
            }
        }else{
            display_output('0','Something went wrong. Please try again.');
        }
    }
    /* Share post end */
    /*Feeds Like*/
    public function post_like(){

        $user_id  = $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        if(empty($_POST)){
            display_output('0','Something went wrong. Please try again.');
        }
        $post_number  = base64_decode($this->input->post('post_num'));
        if(empty($user_id)) {
            display_output('0','Please enter user id.');
        }
        if(empty($post_number)) {
            display_output('0','Please enter post number.');
        }
        if(!$post_owner=$this->Common_model->getRecords('post','id,user_id',array('post_number'=>$post_number,'is_deleted'=>0,'status'=>'Active'),'',true)){
             display_output('0','Post isn\'t available now.');
        }
        $post_id=$post_owner['id'];
        $where=array('post_id'=>$post_id,'user_id'=>$user_id);
        if($this->Common_model->getRecords('like_post','*',$where,'',true)){
            if($this->Common_model->deleteRecords('like_post',$where)){
                 removeNotification($post_owner['user_id'],'Post Like',$post_id); 
            }
            $total=$this->Common_model->getRecords('like_post','count(id) as likes',array('post_id'=>$post_id),'',true);
            $where_post_count="post_id='".$post_id."'";
            $total=$this->Home_model->get_count('like_post',$where_post_count);
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getUserType()." ".$frontend_username." has unlike a post '".$post_number."'.";
            actionLog('post',$post_id,'Unlike',$log_msg,'User',$user_id);
            display_output('1','unlike',array('like_count'=>shortNumber($total['count'])));
        }else{
                $insert_data=array(
                'user_id'=>$user_id,
                'post_id'=>$post_id,
                'created'=>date('Y-m-d H:i:s'),
                );
            if($last_id=$this->Common_model->addEditRecords('like_post',$insert_data)){
                if($post_owner['user_id']!=$user_id){
                    $liked_by_fullname=getUserInfo($user_id,'users','user_id','fullname');
                    $title=ucwords($liked_by_fullname).' likes your post.';
                    $content=ucwords($liked_by_fullname).' likes your post.';
                    $notdata['type']='post_like';
                    $this->Common_model->push_notification_send($post_owner['user_id'],$notdata,$post_id,$title,$content,'',$user_id);
                }
                $where_post_count="post_id='".$post_id."'";
                $total=$this->Home_model->get_count('like_post',$where_post_count);
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getUserType()." ".$frontend_username." has like a post '".$post_number."'.";
                actionLog('post',$post_id,'Like',$log_msg,'User',$user_id);
                display_output('1','like',array('like_count'=>shortNumber($total['count'])));
            }
        }
    }
    /*Feeds Like end*/
    /*Hide post start*/
    public function hide_post(){
        $user_id  = $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        if(empty($_POST)){
            display_output('0','Something went wrong. Please try again.');
        }
        $post_number  = base64_decode($this->input->post('post_num'));
        if(empty($user_id)) {
            display_output('0','Please enter user id.');
        }
        if(empty($post_number)) {
            display_output('0','Please enter post number.');
        }
        $where_post="post_number='".$post_number."' and (is_deleted=0 and status ='Active')";
        if(!$post_owner=$this->Common_model->getRecords('post','id,user_id,status',$where_post,'',true)){
             display_output('0','Post isn\'t available now.');
        }
        $post_id=$post_owner['id'];
        if($post_owner['user_id']==$user_id){
            display_output('0','You cannot hide your own post.');
        }
        if($this->Common_model->getRecords('hide_post','id',array('post_id'=>$post_id,'user_id'=>$user_id),'',true)){
             display_output('0','You aleady hide this post.');
        }

        $insert_data=array(
            'user_id'=>$user_id,
            'post_id'=>$post_id,
            'created'=>date('Y-m-d H:i:s'),
        );
        if($last_id=$this->Common_model->addEditRecords('hide_post',$insert_data)){
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getUserType()." ".$frontend_username." has hided a post '".$post_number."'.";
            actionLog('post',$post_id,'Hide Post',$log_msg,'User',$user_id);
            display_output('1','Post hide successfully.');
        }else{
            display_output('0','Something went wrong. Please try again.');
        }
    }
    /*Hide post end*/
    /*Report post start*/
    public function report_post(){
        $user_id  = $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        if(empty($_POST)){
            display_output('0','Something went wrong. Please try again.');
        }
        $post_number  = base64_decode($this->input->post('post_num'));
        if(empty($user_id)) {
            display_output('0','Please enter user id.');
        }
        if(empty($post_number)) {
            display_output('0','Please enter post number.');
        }
        $comment  = $this->input->post('comment');
        if(!isset($comment) || empty($comment)){
            $comment='';
        }
     
        $where_post="post_number='".$post_number."' and (is_deleted=0 and status ='Active')";
        if(!$post_owner=$this->Common_model->getRecords('post','id,user_id,status',$where_post,'',true)){
             display_output('0','Post isn\'t available now.');
        }
        $post_id=$post_owner['id'];
        if($post_owner['user_id']==$user_id){
            display_output('0','You cannot report of your own post.');
        }
        if($this->Common_model->getRecords('post_report','id',array('post_id'=>$post_id,'user_id'=>$user_id),'',true)){
             display_output('0','You aleady report for this post.');
        }

        $insert_data=array(
            'user_id'=>$user_id,
            'post_id'=>$post_id,
            'comment'=>$comment,
            'created'=>date('Y-m-d H:i:s'),
        );
        if($last_id=$this->Common_model->addEditRecords('post_report',$insert_data)){
            /*$to_email = $this->session->userdata('email'); 
            $from_email =  getNotificationEmail();  
            $subject = WEBSITE_NAME." : Report your post"; 
            $data['name'] = ucwords($this->session->userdata('fullname'));
            $data['message'] = 'Your post is mark as reported.'; 
            $body = $this->load->view('template/common', $data,TRUE);
            $this->Common_model->sendEmail($to_email,$subject,$body,$from_email); */
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getUserType()." ".$frontend_username." has reported a post '".$post_number."'.";
            actionLog('post',$post_id,'Report Post',$log_msg,'User',$user_id);
                    
            display_output('1','Post is mark as reported.');
        }else{
            display_output('0','Something went wrong. Please try again.');
        }
    }
    /*Report post end*/
    /*Save post start*/
    public function save_post(){
        $user_id  = $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        if(empty($_POST)){
            display_output('0','Something went wrong. Please try again.');
        }
        $post_number  = base64_decode($this->input->post('post_num'));
        if(empty($user_id)) {
            display_output('0','Please enter user id.');
        }
        if(empty($post_number)) {
            display_output('0','Please enter post number.');
        }
        $where_post="post_number='".$post_number."' and (is_deleted=0 OR status !='Active')";
        if(!$post_owner=$this->Common_model->getRecords('post','id,status',$where_post,'',true)){
             $this->display_output('0','Post isn\'t available now.');
        }
        $post_id=$post_owner['id'];
        if($this->Common_model->getRecords('save_post','id',array('post_id'=>$post_id,'user_id'=>$user_id),'',true)){
             $this->display_output('0','You aleady saved this post.');
        }
        
        $insert_data=array(
            'user_id'=>$user_id,
            'post_id'=>$post_id,
            'created'=>date('Y-m-d H:i:s'),
        );
        if($last_id=$this->Common_model->addEditRecords('save_post',$insert_data)){
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getUserType()." ".$frontend_username." has saved a post '".$post_number."'.";
            actionLog('post',$post_id,'Saved Post',$log_msg,'User',$user_id);
            display_output('1','Post saved successfully.');
        }else{
            display_output('0','Something went wrong. Please try again.');
        }
    }
    /*Save post end*/
    /*Delete post start*/
    public function delete_post(){
        $user_id  = $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        if(empty($_POST)){
            display_output('0','Something went wrong. Please try again.');
        }
        $post_number  = base64_decode($this->input->post('post_num'));
        if(empty($user_id)) {
            display_output('0','Please enter user id.');
        }
        if(empty($post_number)) {
            display_output('0','Please enter post number.');
        }
        $where_post="post_number='".$post_number."' and (is_deleted=0 and status ='Active')";
        if(!$post_owner=$this->Common_model->getRecords('post','id,user_id,status',$where_post,'',true)){
             display_output('0','Post isn\'t available now.');
        }
        $post_id=$post_owner['id'];
        if($post_owner['user_id']!=$user_id){
            display_output('0','You cannot delete others post.');
        }
        if($post_media_data=$this->Common_model->getRecords('post_media','id,media_path,video_thumbnail,video_large_thumbnail',array('post_id'=>$post_id),'',false)){
            $s3 = new S3(AMAZON_ID, AMAZON_KEY); 
            foreach($post_media_data as $media_data){
                if($media_data['video_thumbnail']!='') {
                    $thumb_image = getDeletePath($media_data['video_thumbnail']);
                    $object = $s3->getObjectInfo(AMAZON_BUCKET,$thumb_image);
                    if($object) {
                        $s3->deleteObject(AMAZON_BUCKET, $thumb_image);
                    } 
                }
                if($media_data['video_large_thumbnail']!='') {
                    $large_image = getDeletePath($media_data['video_large_thumbnail']);
                    $object = $s3->getObjectInfo(AMAZON_BUCKET,$large_image );
                    if($object) {
                        $s3->deleteObject(AMAZON_BUCKET, $large_image);
                    } 
                }
                if($media_data['media_path']!='') {
                    $media_name = getDeletePath($media_data['media_path']);
                    $object = $s3->getObjectInfo(AMAZON_BUCKET,$media_name );
                    if($object) {
                        $s3->deleteObject(AMAZON_BUCKET, $media_name);
                    }
                }
            } 
        }
        $update_data=array(
            'is_deleted'=>1,
            'modified'=>date('Y-m-d H:i:s'),
            );
        if($this->Common_model->addEditRecords('post',$update_data,array('id'=>$post_id))){
            $this->Common_model->deleteRecords('notifications',array('record_id'=>$post_id));
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getUserType()." ".$frontend_username." has deleted a post '".$post_number."'.";
            actionLog('post',$post_id,'Delete',$log_msg,'User',$user_id);
            display_output('1','Post Deleted successfully.');
        }else{
            display_output('0','Something went wrong. Please try again.');
        }
    }
    /*Delete post end*/
    /*post comment start*/
    public function post_comment() {
        
        $user_id  = $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        if(empty($_POST)){
            display_output('0','Something went wrong. Please try again.');
        }
        $post_number  = base64_decode($this->input->post('post_num'));
        $comment = $this->input->post('comment');
        $comment_id = base64_decode($this->input->post('comment_id'));
        if(empty($user_id)) {
            display_output('0','Please enter user id.');
        }
        if(empty($post_number)) {
            display_output('0','Please enter post number.');
        }      
        if(empty($comment)) {
           display_output('0','Please enter comment.');
        }
        if(!$post_owner=$this->Common_model->getRecords('post','id,user_id',array('post_number'=>$post_number,'is_deleted'=>0,'status'=>'Active'),'',true)){
             display_output('0','Post isn\'t available now.');
        }
        $post_id=$post_owner['id'];
        $insert_data = array(
            'user_id' => $user_id,
            'post_id' => $post_id,
            'comment' => $comment,
            'created' => date("Y-m-d H:i:s")
        );
        if(isset($comment_id) && !empty($comment_id)){
            if(!$comment_owner=$this->Common_model->getRecords('comments','user_id',array('id'=>$comment_id,'is_deleted'=>0),'',true)){
                 display_output('0','Parent comment isn\'t available now.');
            }
            $insert_data['parent_id']=$comment_id;
        }
        if($last_id=$this->Common_model->addEditRecords('comments',$insert_data)) {
            $action_by_fullname=getUserInfo($user_id,'users','user_id','fullname');
            if($post_owner['user_id']!=$user_id){
                $title=ucwords($action_by_fullname).' commented on your post.';
                $content=ucwords($action_by_fullname).' commented on your post.';
                $notdata['type']='post_comment';
                $this->Common_model->push_notification_send($post_owner['user_id'],$notdata,$post_id,$title,$content,'',$user_id);


            }
            if(isset($comment_id) && !empty($comment_id)){
                if($comment_owner['user_id']!=$user_id){
                    $title=ucwords($action_by_fullname).' commented on your comment.';
                    $content=ucwords($action_by_fullname).' commented on your comment.';
                    $notdata['type']='post_comment';
                    $this->Common_model->push_notification_send($comment_owner['user_id'],$notdata,$post_id,$title,$content,'',$user_id);
                }
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getUserType()." ".$frontend_username." has added a comment on comment of post '".$post_number."'.";
                actionLog('comments',$last_id,'Add',$log_msg,'User',$user_id);   
            }else{
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getUserType()." ".$frontend_username." has added a comment on post '".$post_number."'.";
                actionLog('comments',$last_id,'Add',$log_msg,'User',$user_id);
            }
            

            $where="post_id='".$post_id."'";
            $comment=$this->Home_model->get_count('comments',$where);
           //return $this->load->view('front/celebrant/comment',$data,true); exit;
            
           display_output('1','Comment posted successfully.',array('comment_total'=>shortNumber($comment['count']),'last_id'=>$last_id));
        } 
        else { 
            display_output('0','Something went wrong. Please try again.');
        }
    }
    /*post comment end*/
    /*post comment list start*/
    public function comment_list($post_num) {
        $limit=COMMENT_LIMIT;
        if(isset($_POST) && !empty($_POST)){
           $data['track_page']=$_POST['page'];
           $offset=$_POST['page']*$limit;
           $post_number=base64_decode($_POST['post_num']);
           
        }else{
            $data['track_page']=0;
            $offset=0;
            $post_number=base64_decode($post_num);
        } 
        $user_id  = $this->session->userdata('user_id');
       // $this->Common_model->check_user_login();
        
        $where_post="post_number='".$post_number."' and (is_deleted=0 and status ='Active')";
        if(!$post_owner=$this->Common_model->getRecords('post','id,user_id,status',$where_post,'',true)){
            echo 'Post isn\'t available now.';exit;
        }
        $post_id=$post_owner['id'];
        $data['comment_total_records']=$this->Front_common_model->getpostcomment(0,0,$post_id,0);
        //echo $this->db->last_query();
        if($data['comments'] = $this->Front_common_model->getpostcomment($limit,$offset,$post_id,0)){
            $data['child_track_page']=0;
            $i=0;
            foreach($data['comments'] as $row){
                $data['comments'][$i]['child_comment'] = $this->Front_common_model->getpostcomment(INNER_CHILD_COMMENT_LIMIT,$offset,$post_id,$row['id']);
                $i++;
            }
        }
        
       
        return $this->load->view('front/celebrant/comment',$data); exit; 
    }
    public function child_comment_list($post_num,$parent_id) {
        $limit=CHILD_COMMENT_LIMIT;
        if(isset($_POST) && !empty($_POST)){
           $data['child_track_page']=$_POST['page'];
           $offset=(($_POST['page']*$limit)-(INNER_CHILD_COMMENT_LIMIT+1));
           $post_number=base64_decode($_POST['post_num']);
          $parent_id=base64_decode($_POST['parent_id']);
           $this->Common_model->check_user_login();
            $where_post="post_number='".$post_number."' and (is_deleted=0 and status ='Active')";
           if(!$post_owner=$this->Common_model->getRecords('post','id',$where_post,'',true)){
                echo 'Post isn\'t available now.';exit;
            }
            $post_id=$post_owner['id'];
            $data['child_comment']=$this->Front_common_model->getpostcomment($limit,$offset,$post_id,$parent_id);
            // echo $this->db->last_query();
            // echo '<pre>';print_r($data['child_comment']);
            $data['total_child_comment']=$this->Front_common_model->getpostcomment(0,0,$post_id,$parent_id);


            return $this->load->view('front/celebrant/child_comment',$data); exit; 
        } 
    }
    /*post comment list end*/
    /*delete comment list start*/
    public function delete_comment(){
        $user_id  = $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        $comment_id  = base64_decode($this->input->post('comment_id'));
        if(empty($comment_id)) {
            display_output('0','Please enter comment id.');
        }
        if(!$comment_owner=$this->Common_model->getRecords('comments','user_id,post_id',array('id'=>$comment_id,'is_deleted'=>0),'',true)){
             display_output('0','Comment isn\'t available now.');
        }
        if($comment_owner['user_id']!=$user_id){
            display_output('0','You are not authorized to delete this comment.');
        }
        //$post_owner_id=$this->Common_model->getFieldValue('post','user_id',array('id'=>$comment_owner['post_id']),'',true);
        $post_owner_detail=$this->Common_model->getRecords('post','user_id,post_number',array('id'=>$comment_owner['post_id']),'',true);
        $post_owner_id=$post_owner_detail['user_id'];
        $post_number=$post_owner_detail['post_number'];
        $records=$this->Common_model->getRecords('comments','*',array('id'=>$comment_id),'',true);
        $post_id=$comment_owner['post_id'];
        //print_r($records);exit;
        if($records['parent_id']>0){
            if($this->Common_model->addEditRecords('comments',array('is_deleted'=>1,'modified'=>date('Y-m-d H:i:s')),array('id'=>$comment_id))){
               // removeNotification($post_owner_id,'post_comment',$comment_owner['post_id']);
                removeNotification($records['user_id'],'post_comment',$post_id,1);
                removeNotification($records['user_id'],'post_comment',$post_id,1);
                $where="post_id='".$post_id."'";
                $comment=$this->Home_model->get_count('comments',$where);
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getUserType()." ".$frontend_username." has deleted comment of comment on post '".$post_number."'.";
                actionLog('comments',$comment_id,'Delete',$log_msg,'User',$user_id);
                display_output('1','Comment deleted successfully.',array('comment_total'=>shortNumber($comment['count']))); 
            }else{
                display_output('0','Something went wrong. Please try again.');
            }
        }else{ 
            if($multi_comment=$this->Common_model->getRecords('comments','id,user_id,post_id',array('parent_id'=>$comment_id,'is_deleted'=>0),'',false))
            {
                $this->db->trans_begin();
                foreach($multi_comment as $row){
                    $this->Common_model->addEditRecords('comments',array('is_deleted'=>1,'modified'=>date('Y-m-d H:i:s')),array('id'=>$row['id']));
                    removeNotification($row['user_id'],'post_comment',$row['post_id'],1);
                }
                $this->Common_model->addEditRecords('comments',array('is_deleted'=>1,'modified'=>date('Y-m-d H:i:s')),array('id'=>$comment_id));
                removeNotification($post_owner_id,'post_comment',$comment_owner['post_id']);
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    display_output('0','Something went wrong. Please try again.'); 
                } else {
                    $this->db->trans_commit();
                    $where="post_id='".$post_id."'";
                    $comment=$this->Home_model->get_count('comments',$where);
                    display_output('1','Comment deleted successfully.',array('comment_total'=>shortNumber($comment['count']))); 
                }
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getUserType()." ".$frontend_username." has deleted comment on post '".$post_number."'.";
                actionLog('comments',$comment_id,'Delete',$log_msg,'User',$user_id);
                display_output('1','Comment deleted successfully.'); 
            }
            else{
                if($this->Common_model->addEditRecords('comments',array('is_deleted'=>1,'modified'=>date('Y-m-d H:i:s')),array('id'=>$comment_id))){
                    removeNotification($post_owner_id,'post_comment',$comment_owner['post_id']);
                    $where="post_id='".$post_id."'";
                    $comment=$this->Home_model->get_count('comments',$where);
                    $frontend_username = getFrontUsername($user_id);
                    $log_msg = getUserType()." ".$frontend_username." has deleted comment on post '".$post_number."'.";
                    actionLog('comments',$comment_id,'Delete',$log_msg,'User',$user_id);
                    display_output('1','Comment deleted successfully.',array('comment_total'=>shortNumber($comment['count']))); 
                }else{
                    display_output('0','Something went wrong. Please try again.');
                }
            }
            
        }        
    }
    /*delete comment list end*/
    /* comment like & dislike start*/
    public function comment_like_dislike(){
        $user_id  = $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        $comment_id  = base64_decode($this->input->post('comment_id'));
        if(empty($comment_id)) {
            display_output('0','Please enter comment id.');
        }
        if(!$comment_owner=$this->Common_model->getRecords('comments','user_id,post_id',array('id'=>$comment_id,'is_deleted'=>0),'',true)){
             display_output('0','Comment isn\'t available now.');
        }
        $post_number=$this->Common_model->getFieldValue('post','post_number',array('id'=>$comment_owner['post_id']),'',true);
        $where=array('comment_id'=>$comment_id,'user_id'=>$user_id);
        if($this->Common_model->getRecords('comment_like','*',$where,'',true)){
            if($this->Common_model->deleteRecords('comment_like',$where)){
                removeNotification($comment_owner['user_id'],'Comment Like',$comment_owner['post_id']); 
            }
            $total=$this->Common_model->getRecords('comment_like','count(id) as likes',array('comment_id'=>$comment_id),'',true);
            $where_post_count="comment_id='".$comment_id."'";
            $total=$this->Home_model->get_count('comment_like',$where_post_count);
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getUserType()." ".$frontend_username." has unlike comment of post '".$post_number."'.";
            actionLog('comment_like',$comment_id,'Unlike Comment',$log_msg,'User',$user_id);
            display_output('1','unlike',array('like_count'=>shortNumber($total['count'])));
        }else{
                $insert_data=array(
                'user_id'=>$user_id,
                'comment_id'=>$comment_id,
                'created'=>date('Y-m-d H:i:s'),
                );
            if($last_id=$this->Common_model->addEditRecords('comment_like',$insert_data)){
                if($comment_owner['user_id']!=$user_id){
                    $liked_by_fullname=getUserInfo($user_id,'users','user_id','fullname');
                    $title=ucwords($liked_by_fullname).' likes your comment.';
                    $content=ucwords($liked_by_fullname).' likes your comment.';
                    $notdata['type']='comment_like';
                    $this->Common_model->push_notification_send($comment_owner['user_id'],$notdata,$comment_owner['post_id'],$title,$content,'',$user_id);
                }
                $where_post_count="comment_id='".$comment_id."'";
                $total=$this->Home_model->get_count('comment_like',$where_post_count);
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getUserType()." ".$frontend_username." has like comment of post '".$post_number."'.";
                actionLog('comment_like',$last_id,'Like Comment',$log_msg,'User',$user_id);
                display_output('1','like',array('like_count'=>shortNumber($total['count'])));
            }
        }
        
    }
    /* comment like & dislike end*/
    /* save post start*/
    public function save_post_list()
    {
        $data['title']= 'Saved Post' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Saved Post'." | ".SITE_TITLE;
        $data['meta_desc']= 'Saved Post'." | ".SITE_TITLE;
        $data['page_title'] = "Saved Post";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Saved Post',
            'link' => ''
        );
        
        $this->Common_model->check_user_login();
        if($this->session->userdata('front_user_type')=='2')
        {
            redirect('home'); 
        }
        $user_id  = $this->session->userdata('user_id');
        $data['total_records']=$this->Home_model->save_post_list(0,0,$user_id);
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/celebrant/saved_post');
        $this->load->view('front/include/footer');
    }
    public function save_post_data(){
        $user_id  = $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        if(isset($_POST) && !empty($_POST)) {
            $data['saved_post']=1;
            $page_number    = trim($_POST['page']);
            $item_per_page = FRONT_LIMIT;
            $offset = (($page_number) * $item_per_page);
            $user_post_data = $this->Home_model->save_post_list($offset,$item_per_page,$user_id);
            $details=array();
            if(!empty($user_post_data)){
            $i=0;
            foreach ($user_post_data as $key => $value) { 
                $where="post_id='".$value['post_id']."'";
                $like=$this->Home_model->get_count('like_post',$where);
                $comment=$this->Home_model->get_count('comments',$where);
                $where_shared="shared_post_id='".$value['post_id']."'";
                $share=$this->Home_model->get_count('post',$where_shared);
                $details[$i]['like']=shortNumber($like['count']);
                $details[$i]['comment']=shortNumber($comment['count']);
                $details[$i]['share']=shortNumber($share['count']);
                $details[$i]['is_like_by_me']=$value['is_like_by_me'];
                $details[$i]['is_report_by_me']= $this->Common_model->getFieldValue('post_report','id',array('user_id'=>$user_id,'post_id'=>$value['post_id']),'',true);
                $details[$i]['is_saved_by_me']= $this->Common_model->getFieldValue('save_post','id',array('user_id'=>$user_id,'post_id'=>$value['post_id']),'',true);
                if($value['shared_post_id']>0){
                    $post_media= $this->Common_model->getRecords('post_media','media_path,video_thumbnail,video_large_thumbnail,media_type,format,duration',array('media_is_deleted'=>0,'post_id'=>$value['shared_post_id']),'',false);
                    $details[$i]['post_id']=$value['post_id'];
                    $details[$i]['save_post_id']=$value['save_post_id'];
                    $details[$i]['post_number']=$value['post_number'];
                    $details[$i]['user_id']=$value['user_id'];
                    $details[$i]['fullname']=$value['fullname'];
                    $details[$i]['profile_picture']=$value['profile_picture'];
                    $details[$i]['gender']=$value['gender'];
                    $details[$i]['created']=$value['created'];
                    $details[$i]['is_shared']=1;
                    $details[$i]['shared_post_id']=$value['shared_post_id'];
                    $details[$i]['post_content']=$value['post_content'];
                    $details[$i]['shared_post_content']=$value['shared_post_content'];
                    $details[$i]['post_media']=$post_media;
                    
                }else{
                    $post_media= $this->Common_model->getRecords('post_media','media_path,video_thumbnail,video_large_thumbnail,media_type,format,duration',array('media_is_deleted'=>0,'post_id'=>$value['post_id']),'',false);
                    $details[$i]['post_id']=$value['post_id'];
                    $details[$i]['save_post_id']=$value['save_post_id'];
                    $details[$i]['post_number']=$value['post_number'];
                    $details[$i]['user_id']=$value['user_id'];
                    $details[$i]['fullname']=$value['fullname'];
                    $details[$i]['profile_picture']=$value['profile_picture'];
                    $details[$i]['gender']=$value['gender'];
                    $details[$i]['created']=$value['created'];
                    $details[$i]['is_shared']=0;
                    $details[$i]['shared_post_id']=0;
                    $details[$i]['post_content']=$value['post_content'];
                    $details[$i]['post_media']=$post_media;
                }
              $i++;  
            }
        }
        $data['news_feeds']=$details;

        //echo '<pre>';print_r($data['news_feeds']);exit;
        echo $all_feeds = $this->load->view('front/celebrant/feeds_data',$data,true); exit;
        }
    }
    /* save post end*/
    /* unsave post start*/
    public function unsave_post(){
        $user_id  = $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        $save_post_id  = base64_decode($this->input->post('save_post_id'));
        if(empty($user_id)) {
            display_output('0','Please enter user id.');
        }
        if(empty($save_post_id)) {
            display_output('0','Please enter save post id.');
        }
        if($save_post=$this->Common_model->getRecords('save_post','id,post_id',array('id'=>$save_post_id,'user_id'=>$user_id),'',true)){
            $post_number=$this->Common_model->getFieldValue('post','post_number',array('id'=>$save_post['post_id']),'',true);
            if($this->Common_model->deleteRecords('save_post',array('id'=>$save_post_id))){
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getUserType()." ".$frontend_username." has unsaved a post '".$post_number."'.";
                actionLog('save_post',$save_post_id,'Unsave Post',$log_msg,'User',$user_id);
                display_output('1','Post unsaved successfully.');
            }else{
                display_output('0','Something went wrong. Please try again.');
            }
        }else{
            //echo $this->db->last_query();exit;
            display_output('0','Post isn\'t available.');
        }
        
    }
    /* unsave post end*/
    /*Get my friends list*/
    public function my_friends_list_bkp()
    {
        if($this->session->userdata('front_user_type')=='2')
        {
            redirect('home');
        }
        $user_id = $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        $data['title']= 'My Friends' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'My Friends'." | ".SITE_TITLE;
        $data['meta_desc']= 'My Friends'." | ".SITE_TITLE;
        $data['page_title'] = "My Friends";
        //$data['page_sub_title'] = "Find your all event history";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Friends',
            'link' => ''
        );

        $this->Common_model->check_user_login();
        if(isset($_POST) && !empty($_POST)){
            //$data['name']=$_POST['name'];
            //$data['event_status']=$_POST['event_stauts'];
        }
        $data['filter']='0';
        $data['total_records'] = $this->Home_model->get_friend_list(0,0);
        $data['request_list']=$this->Home_model->get_friend_request_list(0,5,$user_id,$keyword='','limit');
        $data['total_request']=$this->Home_model->get_friend_request_list(0,0,$user_id,$keyword='');
        //$data['total_request']=$this->Home_model->get_friend_request_list(0,0,$user_id,$keyword='');
        //echo $this->db->last_query();
        //echo "<pre>"; print_r($data['total_request']); die;
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/celebrant/friends');
        $this->load->view('front/include/footer');
    }

    public function my_friends_list_data_bkp()
    {
        $this->Common_model->check_user_login();
        if($this->session->userdata('front_user_type')=='2' )
        {
            redirect('home');
        }
        if(isset($_POST) && !empty($_POST)) {
            $page_number    = trim($_POST['page']);
            $item_per_page = FRONT_LIMIT;
            $offset = (($page_number) * $item_per_page);
            //$name=isset($_POST['name'])?$_POST['name']:'';
            //$status=isset($_POST['event_status'])?$_POST['event_status']:'';
            $data['friends_list'] = $this->Home_model->get_friend_list($offset,$item_per_page);
          /*  echo $this->db->last_query();
            echo "<pre>"; print_r($data['friends_list']); die;*/
            echo $all_services = $this->load->view('front/celebrant/friends_list_data',$data,true); exit;
        }
    }
    public function my_friends_list()
    {   
        // echo 1231313; die;
        if($this->session->userdata('front_user_type')=='2')
        {
            redirect('home');
        }
        $user_id = $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        $data['title']= 'My Friends' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'My Friends'." | ".SITE_TITLE;
        $data['meta_desc']= 'My Friends'." | ".SITE_TITLE;
        $data['page_title'] = "My Friends";
        //$data['page_sub_title'] = "Find your all event history";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Friends',
            'link' => ''
        );

        $this->Common_model->check_user_login();
        if(isset($_POST) && !empty($_POST)){
            //$data['name']=$_POST['name'];
            //$data['event_status']=$_POST['event_stauts'];
        }
        //$user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);
        $data['filter']='0';
        $data['total_records'] = $this->Home_model->get_friend_list(0,0);
        $data['request_list']=$this->Home_model->get_friend_request_list(0,5,$user_id,$keyword='','limit');
        $data['total_request']=$this->Home_model->get_friend_request_list(0,0,$user_id,$keyword='');
        //$data['total_request']=$this->Home_model->get_friend_request_list(0,0,$user_id,$keyword='');
        //echo $this->db->last_query();
        // echo "<pre>"; print_r($data); die;
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/celebrant/friends');
        $this->load->view('front/include/footer');
    }

    public function my_friends_list_data()
    {
        $this->Common_model->check_user_login();
        if($this->session->userdata('front_user_type')=='2' )
        {
            redirect('home');
        }
        $user_id = $this->session->userdata('user_id');
        if(isset($_POST) && !empty($_POST)) {
            $page_number    = trim($_POST['page']);
            $item_per_page = FRONT_LIMIT;
            $offset = (($page_number) * $item_per_page);
            //$name=isset($_POST['name'])?$_POST['name']:'';
            //$status=isset($_POST['event_status'])?$_POST['event_status']:'';
           // $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);
            $data['friends_list'] = $this->Home_model->get_friend_list($offset,$item_per_page);
            /*echo $this->db->last_query();
            echo "<pre>"; print_r($data['friends_list']); die;*/
            echo $all_services = $this->load->view('front/celebrant/friends_list_data',$data,true); exit;
        }
    }
    /*Get my friends list ends*/

    /*Get friend suggestion start*/
    public function my_friend_suggestion()
    {
        if($this->session->userdata('front_user_type')=='2')
        {
            redirect('home');
        }
        $user_id = $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        $data['title']= 'Find Friends' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Find Friends'." | ".SITE_TITLE;
        $data['meta_desc']= 'Find Friends'." | ".SITE_TITLE;
        $data['page_title'] = "Find Friends";
        //$data['page_sub_title'] = "Find your all event history";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Find Friends',
            'link' => ''
        );
        $data['filter_hide']=0;
        $data['gender_hide']=1;
        $data['age_hide']=1;

        $this->Common_model->check_user_login();
        $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);
        $data['total_records']=$this->Home_model->be_my_date_list(0,0,$user_id,'');
        $data['countries']=$this->Common_model->getRecords('countries', 'id,name', array(), 'name', false);
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/celebrant/find_friend_list');
        $this->load->view('front/include/footer');
    }

    public function my_friend_suggestion_data()
    {
        $this->Common_model->check_user_login();
        if($this->session->userdata('front_user_type')=='2' )
        {
            redirect('home');
        }
        $data['filter_hide']=0;
        $data['gender_hide']=1;
        $data['age_hide']=1;
        $user_id = $this->session->userdata('user_id');
        if(isset($_POST) && !empty($_POST)) {
            $page_number    = trim($_POST['page']);
            $item_per_page = FRONT_LIMIT;
            $offset = (($page_number) * $item_per_page);
            $data['date_list']=$this->Home_model->be_my_date_list($offset,$item_per_page,$user_id,'');
            echo $all_services = $this->load->view('front/celebrant/find_friend_list_data',$data,true); exit;
        }
    }
    public function my_friend_suggestion_filter(){
        $this->Common_model->check_user_login();
       if(isset($_POST) && !empty($_POST)) {
            $country_id=isset($_POST['country_id'])?$_POST['country_id']:'';
            $state_id=isset($_POST['state_id'])?$_POST['state_id']:'';
            $item_per_page = FRONT_LIMIT;
            $offset=0;

            if(isset($country_id) && !empty($country_id)){
                $i=0;
                $where='';
                foreach ($country_id as $key => $value) {

                    if($i==0){
                        $where.="country_id=$value";
                    }else{
                        $where.=" OR country_id=$value";
                    }$i++;
                }
                $data['states']=$this->Common_model->getRecords('states', 'id,name', $where, 'name', false);
                //echo $this->db->last_query();

            }
            if(isset($state_id) && !empty($state_id)){
                $i=0;
                $where='';
                foreach ($state_id as $key => $value) {
                    if($i==0){
                        $where.="state_id=$value";
                    }else{
                        $where.=" OR state_id=$value";
                    }$i++;
                }
                $data['cities']=$this->Common_model->getRecords('cities', 'id,name', $where, 'name', false);
            }
            $data['filter_hide']=0;
            $data['gender_hide']=1;
            $data['age_hide']=1;
            $data['countries']=$this->Common_model->getRecords('countries', 'id,name', array(), 'name', false);
            $user_id= $this->session->userdata('user_id');
           
            $data['total_records']=$this->Home_model->be_my_date_list(0,0,$user_id,'');
            //echo "<pre>"; print_r($this->db->last_query()); 
            echo $all_providers = $this->load->view('front/celebrant/find_friend_list',$data,true); exit;
        }

    }
    /*Get friend suggestion end*/
    /*Friends request data*/
    public function my_friend_requests()
    {
        if($this->session->userdata('front_user_type')=='2')
        {
            redirect('home');
        }
        $user_id = $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        $data['title']= 'Friends Request' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Friends Request'." | ".SITE_TITLE;
        $data['meta_desc']= 'Friends Request'." | ".SITE_TITLE;
        $data['page_title'] = "Friends Request";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'All Request',
            'link' => ''
        );

        $this->Common_model->check_user_login();
        if(isset($_POST) && !empty($_POST)){
            $data['name']=$_POST['name'];
            $data['request_list']=$this->Home_model->get_friend_request_list(0,5,$user_id,$keyword='','limit');
        }
        $data['filter']='0';
        $data['total_request']=$this->Home_model->get_friend_request_list(0,0,$user_id,$keyword='');
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/celebrant/request_friend');
        $this->load->view('front/include/footer');
    }

    public function my_friends_request_list_data()
    {
        $this->Common_model->check_user_login();
        if($this->session->userdata('front_user_type')=='2' )
        {
            redirect('home');
        }
        if(isset($_POST) && !empty($_POST)) {
            $page_number    = trim($_POST['page']);
            $page_name    = trim($_POST['page_name']);
            if ($page_name=="Friend List") {
                $item_per_page = 5;
            }else{
                $item_per_page = FRONT_LIMIT;
            }
            $data['page'] = $page_name;
            $offset = (($page_number) * $item_per_page);

            $user_id  = $this->session->userdata('user_id');
            $data['request_list']=$this->Home_model->get_friend_request_list($offset,$item_per_page,$user_id,$keyword='');
           // echo $this->db->last_query();exit;
            echo $all_services = $this->load->view('front/celebrant/friends_request_list_data',$data,true); exit;
        }
    }
    /*Friends request data ends*/


    /* Accept reject request*/
    public function accept_reject_request()
    {
        $this->Common_model->check_user_login();
        if($this->session->userdata('front_user_type')=='2' )
        {
            redirect('home');
        }
        $user_id  = $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        $sent_by  = base64_decode($this->input->post('sent_by'));
        $request_id  = base64_decode($this->input->post('request_id'));
        $status  = $this->input->post('status');

        if($status!='Accepted' &&  $status!='Rejected'){
            display_output('0','Status either Accepted or Rejected.');
        }
        if(!$record=$this->Common_model->getRecords('friends', 'id,status', array('id'=>$request_id), '', true)){
            display_output('0','Friend request not available.');
        }
        if($record['status']=='Accepted' && $status=='Accepted'){
             display_output('0','Friend request already accepted.');
        }
        if($user_id==$sent_by) {
            display_output('0','You cannot perform this action on yourself.');
        }
        $sender_staging_id=getUserInfo($sent_by,'users','user_id','staging_id');
        if($status=='Accepted'){
            $update_data=array('status'=>'Accepted','modified'=>date('Y-m-d H:i:s'));
            if($this->Common_model->addEditRecords('friends',$update_data,array('id'=>$request_id))){
                $user_fullname=getUserInfo($user_id,'users','user_id','fullname');
                $title=ucwords($user_fullname).' accepted your friend request.';
                $content=ucwords($user_fullname).' accepted your friend request.';
                $notdata['type']='friend_request_accepted';
                $this->Common_model->push_notification_send($sent_by,$notdata,$request_id,$title,$content,'',$user_id);
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getUserType()." ".$frontend_username." has accepted friend request of '".$sender_staging_id."'.";
                actionLog('friends',$request_id,'Accept Friend Request',$log_msg,'User',$user_id);
                display_output('1','Friend request accepted.',array('rq_status'=>$status));
            }else{
                display_output('0','Something went wrong. Please try again.');
            }
        }else{
            if($this->Common_model->deleteRecords('friends',array('id' =>$request_id))){
                if($record['status']=='Accepted' && $status=='Rejected'){
                    $frontend_username = getFrontUsername($user_id);
                    $log_msg = getUserType()." ".$frontend_username." unfriend to '".$sender_staging_id."'.";
                    actionLog('friends',$request_id,'Unfriend',$log_msg,'User',$user_id);
                    display_output('1','Connection remove successfully.');
                }else{
                    $frontend_username = getFrontUsername($user_id);
                    $log_msg = getUserType()." ".$frontend_username." has rejected friend request of '".$sender_staging_id."'.";
                    actionLog('friends',$request_id,'Reject Friend Request',$log_msg,'User',$user_id);
                    display_output('1','Friend request rejected.',array('rq_status'=>$status));
                }
            }else{
                display_output('0','Something went wrong. Please try again.');
            }
        }
    }
    /* Accept reject request ends*/


    public function my_date()
    {   
        if($this->session->userdata('front_user_type')=='2')
        {
            redirect('home');
        }
        $user_id = $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        $data['title']= 'Be My Date' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Be My Date'." | ".SITE_TITLE;
        $data['meta_desc']= 'Be My Date'." | ".SITE_TITLE;
        $data['page_title'] = "Be My Date";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Be My Date',
            'link' => ''
        );

        $this->Common_model->check_user_login();
        $user_id= $this->session->userdata('user_id');
        $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);
        $data['filter_hide']=0;
        $data['gender_hide']=1;
        $data['age_hide']=1;
        $data['countries']=$this->Common_model->getRecords('countries', 'id,name', array(), 'name', false);
        $data['total_records']=$this->Home_model->be_my_date_list(0,0,$user_id,'');
        //echo "<pre>"; print_r($this->db->last_query());die;
        // echo "<pre>"; print_r($data); die;
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/celebrant/be-my-date');
        $this->load->view('front/include/footer');
    }

    public function manage_my_date_data(){
        $this->Common_model->check_user_login();
        if(isset($_POST) && !empty($_POST)) {
            $page_number    = trim($_POST['page']);
            $item_per_page = FRONT_LIMIT;
            $offset = (($page_number) * $item_per_page);
            $user_id= $this->session->userdata('user_id');
            $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);
            $data['filter_hide']=0;
            $data['gender_hide']=1;
            $data['age_hide']=1;
            $data['date_list']=$this->Home_model->be_my_date_list($offset,$item_per_page,$user_id,'');
            //echo $this->db->last_query();die;
            // echo "<pre>"; print_r($data['date_list']); die;
            echo $all_providers = $this->load->view('front/celebrant/be-my-date-list',$data,true); exit;
        }
    }

    public function be_my_date_filter(){
        $this->Common_model->check_user_login();
       if(isset($_POST) && !empty($_POST)) {
            $country_id=isset($_POST['country_id'])?$_POST['country_id']:'';
            $state_id=isset($_POST['state_id'])?$_POST['state_id']:'';
            $item_per_page = FRONT_LIMIT;
            $offset=0;

            if(isset($country_id) && !empty($country_id)){
                $i=0;
                $where='';
                foreach ($country_id as $key => $value) {

                    if($i==0){
                        $where.="country_id=$value";
                    }else{
                        $where.=" OR country_id=$value";
                    }$i++;
                }
                $data['states']=$this->Common_model->getRecords('states', 'id,name', $where, 'name', false);
                //echo $this->db->last_query();

            }
            if(isset($state_id) && !empty($state_id)){
                $i=0;
                $where='';
                foreach ($state_id as $key => $value) {
                    if($i==0){
                        $where.="state_id=$value";
                    }else{
                        $where.=" OR state_id=$value";
                    }$i++;
                }
                $data['cities']=$this->Common_model->getRecords('cities', 'id,name', $where, 'name', false);
            }
            $data['filter_hide']=0;
            $data['gender_hide']=1;
            $data['age_hide']=1;
            $data['countries']=$this->Common_model->getRecords('countries', 'id,name', array(), 'name', false);
            $user_id= $this->session->userdata('user_id');
            $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);
            $data['total_records']=$this->Home_model->be_my_date_list(0,0,$user_id,'');
            //echo "<pre>"; print_r($data['total_records']); die;
            echo $all_providers = $this->load->view('front/celebrant/be-my-date',$data,true); exit;
        }

    }

    public function be_my_date_hide(){
        if($this->session->userdata('front_user_type')=='2' )
        {
            redirect('home');
        }
        $user_id  = $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        $hide_to  = base64_decode($this->input->post('hide_to'));
        $status  = $this->input->post('status');
        $sender_staging_id=getUserInfo($user_id,'users','user_id','fullname');
        $hide_to_staging_id=getUserInfo($hide_to,'users','user_id','staging_id');
        $where="user_id='".$hide_to."' and (is_deleted=0 and status ='Active')";
        if(!$hide_user=$this->Common_model->getRecords('users','user_id',$where,'',true)){
           // echo $this->db->last_query();exit;
             display_output('0','User isn\'t available now.');
        }
        if($status=='Hide'){
            if($this->Common_model->getRecords('be_my_date_hide','id',array('hide_by'=>$user_id,'hide_to'=>$hide_to,'status'=>'Hide'),'',true)){
                 display_output('0','You aleady hide this user.');
            }
            $insert_data=array(
                'hide_by'=>$user_id,
                'hide_to'=>$hide_to,
                'status'=>$status,
                'created'=>date('Y-m-d H:i:s'),
            );
            if($last_id=$this->Common_model->addEditRecords('be_my_date_hide',$insert_data)){
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getUserType()." ".$frontend_username." has hided '".$hide_to_staging_id."' from be my date.";
                actionLog('be_my_date_hide',$last_id,'Add',$log_msg,'User',$user_id);
                display_output('1','User hided successfully.');
            }else{
                display_output('0','Something went wrong. Please try again.');
            }
        }elseif($status == 'Pending'){
            if($this->Common_model->getRecords('friends','id',array('sender_id'=>$user_id,'receiver_id'=>$hide_to),'',true)){
                display_output('0','You aleady send request to this user.');
            }
            $insert_data=array(
                'receiver_id'=>$hide_to,
                'sender_id'=>$user_id,
                'status'=>$status,
                'created'=>date('Y-m-d H:i:s'),
            );
            if($last_id=$this->Common_model->addEditRecords('friends',$insert_data)){
                $title=ucwords($sender_staging_id).' sent you friend request.';
                $content=ucwords($sender_staging_id).' sent you friend request.';
                $notdata['type']='friend_request';
                $this->Common_model->push_notification_send($hide_to,$notdata,$last_id,$title,$content,'',$user_id);
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getUserType()." ".$frontend_username." has sent friend request to '".$hide_to_staging_id."'.";
                actionLog('friends',$last_id,'Add',$log_msg,'User',$user_id);
                display_output('Pending','Request has been Sent!');
            }else{
                display_output('0','Something went wrong. Please try again.');
            }
        }elseif($status=='Report'){
            if($this->Common_model->getRecords('be_my_date_hide','id',array('hide_by'=>$user_id,'hide_to'=>$hide_to,'status'=>'Report'),'',true)){
                display_output('0','You aleady report this user.');
            }
            $insert_data=array(
                'hide_by'=>$user_id,
                'hide_to'=>$hide_to,
                'status'=>$status,
                'created'=>date('Y-m-d H:i:s'),
            );
            if($last_id=$this->Common_model->addEditRecords('be_my_date_hide',$insert_data)){
                display_output('1','User mark as reported successfully.');
            }else{
                display_output('0','Something went wrong. Please try again.');
            }
        }
        
    }


    public function be_my_date_data(){ //1
        
        $user_id  = $this->session->userdata('user_id');
        $beMy_date_ID = $this->input->POST('beMyDate_id');

        if($user_id && $beMy_date_ID){ //2

            $bemydate_data = array(
                'user_id'=> $user_id,
                'beMyDate_id'=>$beMy_date_ID,
                'status' => 1,    
            );

            $bemydate_data_id=$this->Common_model->addEditRecords('beMyDate',$bemydate_data);
            if($bemydate_data_id){ // 3

                $insert_data=array(
                    'message'=>"",
                    'is_beMyDate'=>1,
                    'created_by'=>$user_id,
                    'created'=>date('Y-m-d H:i:s'),
                );
                $last_id=$this->Common_model->addEditRecords('chat_message',$insert_data);
                if($last_id){
    
                    $newInserData = array(
                        'recipient_id' => $beMy_date_ID,
                        'message_id' => $last_id,
                        'is_beMyDate'=>1,
                        'created_by' => $user_id    
                    );
                    $Newlast_id=$this->Common_model->addEditRecords('chat_message_recipients',$newInserData);
                    if($Newlast_id){
                        echo'success';
                    }
    
                }else{
                    echo "fail";
                }
            }else{ // 3
                echo 'fail';
            }

        }else{ // 2
            echo "fail";
        }

    } //1
    
}
