<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Explore extends CI_Controller {

	public function __construct() {
		parent::__construct();
        $this->load->library('paypal_lib');
		$this->load->model(array('admin/Common_model','front/Explore_model','Front_common_model','front/Booking_model','front/Home_model'));
		$this->load->helper('common_helper');
		$user_type = $this->session->userdata('front_user_type'); 

        if($user_type=='2')
        {
            redirect('manage-service'); 
        } 
	}
	/////////////////////////////////service providers /////////////////////////////////
	public function manage_service_providers_list($cat_id='') 
	{
        $data['title']= 'Explore' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Explore'." | ".SITE_TITLE;
        $data['meta_desc']= 'Explore'." | ".SITE_TITLE;
        $data['page_title'] = "Explore";
        $data['page_sub_title'] = "Find best service provider's here";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => 'home'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Explore',
            'link' => 'service-providers'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Service Providers',
            'link' => ''
        );
       	$this->Common_model->check_user_login();
        $user_id= $this->session->userdata('user_id');
        $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);
        $data['total_records']=$this->Explore_model->celebrant_get_service_provider_list(0,0,$user_id,$user_address['country'],$user_address['state'],$user_address['city']);
        $data['countries']=$this->Common_model->getRecords('countries', 'id,name', array(), 'name', false);
        $data['upcoming_events'] = $this->Explore_model->get_upcoming_public_list(0,5,$user_id,$user_address['country'],$user_address['state'],$user_address['city'],'limit');
        if(isset($cat_id) && !empty($cat_id)){
            $data['service_category']=$cat_id;
        }else{
             $data['service_category']='';
        }
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/celebrant/service_provider_list');
        $this->load->view('front/include/footer');
	}
    public function manage_service_providers_data(){
        $this->Common_model->check_user_login();
        if(isset($_POST) && !empty($_POST)) {
            $page_number    = trim($_POST['page']);
            $item_per_page = FRONT_LIMIT;
            $offset = (($page_number) * $item_per_page);
            $user_id= $this->session->userdata('user_id');
            $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);
            $data['details'] = $this->Explore_model->celebrant_get_service_provider_list($offset,$item_per_page,$user_id,$user_address['country'],$user_address['state'],$user_address['city']);
            echo $all_providers = $this->load->view('front/celebrant/service_provider_data',$data,true); exit;
        }
    }
    public function service_provider_filter(){
       $this->Common_model->check_user_login();
       if(isset($_POST) && !empty($_POST)) {
            $country_id=isset($_POST['country_id'])?$_POST['country_id']:'';
            $state_id=isset($_POST['state_id'])?$_POST['state_id']:'';
            $data['service_category']=isset($_POST['service_cat'])?$_POST['service_cat']:'';
            $item_per_page = FRONT_LIMIT;
            $offset=0;
            if(isset($country_id) && !empty($country_id)){
                $i=0;
                $where='';
                foreach ($country_id as $key => $value) {

                    if($i==0){
                        $where.="country_id=$value";
                    }else{
                        $where.=" OR country_id=$value";
                    }$i++;
                }
                $data['states']=$this->Common_model->getRecords('states', 'id,name', $where, 'name', false);
            } 
            if(isset($state_id) && !empty($state_id)){
                $i=0;
                $where='';
                foreach ($state_id as $key => $value) {
                    if($i==0){
                        $where.="state_id=$value";
                    }else{
                        $where.=" OR state_id=$value";
                    }$i++;
                }
                $data['cities']=$this->Common_model->getRecords('cities', 'id,name', $where, 'name', false);
            }
            $data['countries']=$this->Common_model->getRecords('countries', 'id,name', array(), 'name', false);
            $user_id= $this->session->userdata('user_id');
            $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);
            $data['total_records']=$this->Explore_model->celebrant_get_service_provider_list(0,0,$user_id,$user_address['country'],$user_address['state'],$user_address['city']);
            echo $all_providers = $this->load->view('front/celebrant/service_provider_list',$data,true); exit;
        }
    }
    public function service_provider_details($id) 
    {

        $service_provider_id=base64_decode($id);
        $data['title']= 'Service Provider Details' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Service Provider Details'." | ".SITE_TITLE;
        $data['meta_desc']= 'Service Provider Details'." | ".SITE_TITLE;
        $data['page_title'] = "Service Provider Details";
        $data['page_sub_title'] = "Find best service provider's here";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => 'home'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Explore',
            'link' => 'service-providers'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Service Details',
            'link' => ''
        );
        $this->Common_model->check_user_login();
        if(!$data['sp_details']=$this->Explore_model->cp_get_sp_details($service_provider_id)) {
            redirect('pages/page_not_found');
        }
        $local = date("Y-m-d H:i:s");
        $data['service_offer']=$this->Explore_model->get_servies_offer($service_provider_id,$local);
        $data['service_image']=$this->Common_model->getRecords('services','img1,img2,img3',array('is_deleted'=>0,'status'=>'Active','user_id'=>$service_provider_id),'',true);
        $user_id= $this->session->userdata('user_id');
        $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);
        $data['upcoming_events'] = $this->Explore_model->get_upcoming_public_list(0,5,$user_id,$user_address['country'],$user_address['state'],$user_address['city'],'limit');
        $data['service_provider_id']=$id;
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/celebrant/service_detail');
        $this->load->view('front/include/footer');
    }
    public function get_items($service_id){
       
        
       // $this->Common_model->check_user_login();
        $data['service_id']=$service_id;
        $data['item_details']=$this->Explore_model->get_service_items($service_id);
        return $this->load->view('front/celebrant/get_items',$data);
    }

    public function book_service()
    {
        $this->Common_model->check_user_login();
        $data['title']= 'Service Provider Details' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Service Provider Details'." | ".SITE_TITLE;
        $data['meta_desc']= 'Service Provider Details'." | ".SITE_TITLE;
        $data['page_title'] = "Service Provider Details";
        $data['page_sub_title'] = "Find best service provider's here";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => 'home'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Explore',
            'link' => 'service-providers'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Service Details',
            'link' => ''
        );
        $user_id=$this->session->userdata('user_id');
        $rand=rand(0,9999);
        $tax=$this->Common_model->getFieldValue('settings','tax',array('id'=>1),'',true);
        if($this->input->post()) 
        {   
            //echo '<pre>';print_r($_POST);exit;
            $customer_qty=$_POST['customer_qty'];
            $item_name=$_POST['item_name'];
            $qty=$_POST['qty'];
            $price=$_POST['price'];
            $unit=$_POST['unit'];
            $service_id=base64_decode($_POST['s_id']);
            if(!$sp_id=$this->Common_model->getRecords('services','user_id',array('id'=>$service_id,'is_deleted'=>0),'',true)){
                $this->session->set_flashdata('error', 'Service provider is not available. Please check again.');
                redirect('service-providers');
            }
            if(!empty($this->session->userdata('temp_user_item'))){
                $this->Common_model->deleteRecords('temp_table',array('temp_id'=>$this->session->userdata('temp_user_item')));
            }
            if(strlen(implode($customer_qty)) == 0) {
               $this->session->set_flashdata('error', 'Please add item.');
               redirect('service-provider-detail/'.base64_encode($sp_id['user_id']));
            }
            $offer_where="status='Active' and is_deleted=0 and  service_id = '".$service_id."'  and (start_date <= '".date('Y-m-d H:i:s')."' and end_date >= '".date('Y-m-d H:i:s')."')";
            $data['offer_details']=$this->Common_model->getRecords('offer','discount,offer_name',$offer_where,'',true);
            $sub_total=0;
            $discount=0;
            $order_item_total=0;
            $sub_item_total_price=0;
            $discount_title='Discount';
            if(!empty($data['offer_details'])){
                $discount=$offer_details['discount'];
                $discount_title='Discount'.'-'.$offer_details['offer_name'];
            }
            foreach ($customer_qty as $key => $value) {
                if(!empty($value)){
                    if(isset($customer_qty[$key]) && isset($price[$key]) && isset($item_name[$key]) && isset($unit[$key]) && isset($qty[$key])){
                        $total_price=number_format($customer_qty[$key]*$price[$key],2,'.','');
                        $order_item_total+=$total_price;
                        $sub_item_total_price=$sub_total;
                        $insert_array[]=array('item_id' => $key,
                                            'item_name' => $item_name[$key],
                                            'price' =>$price[$key],
                                            'unit' =>$unit[$key],
                                            'qty' =>$qty[$key],
                                            'customer_qty' =>$customer_qty[$key],
                                            'total_price' =>$total_price,
                                           // 'sell_price' =>$sell_price,
                                            'tax' =>$tax,
                                            'discount' =>$data['offer_details']['discount'],
                                            'offer_name' =>$data['offer_details']['offer_name'],
                                            'service_id'=>$service_id,
                                            'user_id'=>$user_id,
                                            'temp_id'=>$user_id.$rand,
                                            'created'=>date('Y-m-d H:i:s'));
                    }else{
                        $this->session->set_flashdata('error', 'Something went wrong!!');
                        redirect('service-provider-detail/'.base64_encode($sp_id));
                    }
                }
                
            }
            if($this->db->insert_batch('temp_table', $insert_array)){
                $temp_items=array('temp_user_item'=>$user_id.$rand);
                $this->session->set_userdata($temp_items);
                redirect('book-service');
            }
        }
        if(!empty($this->session->userdata('temp_user_item'))){
            if(!$data['item_details']=$this->Common_model->getRecords('temp_table','*',array('user_id'=>$user_id,'temp_id'=>$this->session->userdata('temp_user_item')),'',false)){
                redirect('service-providers');
            }

            $data['sp_info']=$this->Explore_model->sp_info($data['item_details'][0]['service_id']);
        }
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/celebrant/book_service');
        $this->load->view('front/include/footer');
    }
    public function update_book_service()
    {
        $this->Common_model->check_user_login();
        
        $user_id=$this->session->userdata('user_id');
        if($this->input->post()) 
        {
            $id=base64_decode($this->input->post('temp_id'));
            $qty=$this->input->post('qty');
            if($item_details=$this->Common_model->getRecords('temp_table','temp_id,service_id,price',array('id'=>$id,),'',true)){
                $service_id=$item_details['service_id'];
                $offer_where="status='Active' and is_deleted=0 and  service_id = '".$service_id."'  and (start_date <= '".date('Y-m-d H:i:s')."' and end_date >= '".date('Y-m-d H:i:s')."')";
                $data['offer_details']=$this->Common_model->getRecords('offer','discount,offer_name',$offer_where,'',true);
                $tax=$this->Common_model->getFieldValue('settings','tax',array('id'=>1),'',true);
                $update_data=array('customer_qty'=>$qty,
                                   'total_price'=>$item_details['price']*$qty,
                );
                $update_data2=array('tax'=>$tax,
                                   'offer_name'=>$data['offer_details']['offer_name'],
                                   'discount'=>$data['offer_details']['discount']);
                $this->db->trans_begin();
                $this->Common_model->addEditRecords('temp_table',$update_data, array('id' =>$id));
                $this->Common_model->addEditRecords('temp_table',$update_data2, array('temp_id' =>$item_details['temp_id']));
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    display_output('0','Something went wrong. Please try again.'); 
                     
                } else {
                    $this->db->trans_commit();
                    display_output('1','success.');
                }
            }
        }else{
            display_output('0','Something went wrong. Please try again.'); 
        }
       
    }
    public function boook_event_service() 
    {
        $data['title']= 'Book Service' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Book Service'." | ".SITE_TITLE;
        $data['meta_desc']= 'Book Service'." | ".SITE_TITLE;
        $data['page_title'] = "Book Service";
        $data['page_sub_title'] = "Booking Details";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => 'home'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Explore',
            'link' => 'service-providers'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Book Service',
            'link' => ''
        );
        $this->Common_model->check_user_login();
        $data['countries'] = $this->Common_model->getRecords('countries','*',array('id'=>231));
        $user_id=$this->session->userdata('user_id');
        if(!empty($this->session->userdata('temp_user_item'))){
            if(!$data['item_details']=$this->Common_model->getRecords('temp_table','*',array('user_id'=>$user_id,'temp_id'=>$this->session->userdata('temp_user_item')),'',false)){

                redirect('service-providers');
            }
            $data['total_price'] = array_sum(array_column($data['item_details'],'total_price'));
            $data['sp_info']=$this->Explore_model->sp_info($data['item_details'][0]['service_id']);
        }
        $data['form_action']=site_url('add-event');
        if ($this->input->is_ajax_request()) {
           return $this->load->view('front/celebrant/book_event_service',$data);
        }else{
            $this->load->view('front/include/header',$data);
            $this->load->view('front/include/breadcrumb');
            $this->load->view('front/celebrant/book_event_service');
            $this->load->view('front/include/footer');
        }
    }
    public function checkout(){
        $this->Common_model->check_user_login();
        $user_id=$this->session->userdata('user_id');
        if(!empty($this->session->userdata('temp_user_item'))){
            if(!$item_details=$this->Common_model->getRecords('temp_table','*',array('user_id'=>$user_id,'temp_id'=>$this->session->userdata('temp_user_item')),'',false)){
               display_output('0','Items are not available in cart.');
            }
            $service_id=$item_details[0]['service_id'];
            if(!$service_record=$this->Common_model->getRecords('services','user_id',array('id'=>$service_id,'status'=>'Active','is_deleted'=>0),'',true)){
                display_output('0','Service isn\'t available now.');
            }
            $tax=$this->Common_model->getFieldValue('settings','tax',array('id'=>1),'',true);
            $offer_where="status='Active' and is_deleted=0 and  service_id = '".$service_id."'  and (start_date <= '".date('Y-m-d H:i:s')."' and end_date >= '".date('Y-m-d H:i:s')."')";
            $offer_details=$this->Common_model->getRecords('offer','discount,offer_name',$offer_where,'',true);
            if(!empty($item_details[0]['discount'])){
                if($item_details[0]['discount']!=$offer_details['discount']){
                    display_output('0','Offer isn\'t available now.');
                }
            }
            if($item_details[0]['tax']!=$tax){
                display_output('0','Tax percentage changed now.');
            }
            foreach ($item_details as $key => $value) {
                if(!$item_records=$this->Common_model->getRecords('services_item','item_name,price,qty,unit',array('id'=>$value['item_id'],'status'=>'Active','is_deleted'=>0),'',true)){
                    display_output('0',ucfirst($value['item_name']). ' isn\'t available now.');
                }
                if($item_records['item_name']!=$value['item_name']){
                    display_output('0',ucfirst($value['item_name']). ' name has been changed.');
                }
                if($item_records['price']!=$value['price']){
                    display_output('0',ucfirst($value['item_name']). ' price has been changed.');
                }
                if($item_records['qty']!=$value['qty']){
                    display_output('0',ucfirst($value['item_name']). ' per unit quantity has been changed.');
                }
                $measurement_unit=$this->Common_model->getFieldValue('measurement_unit','title',array('id'=>$item_records['unit']),'',true);
                if($measurement_unit!=$value['unit']){
                    display_output('0',ucfirst($value['item_name']). ' measuring unit has been changed.');
                }
                $current_total_price=number_format($value['customer_qty']*$item_records['price'],2,'.','');
                if($current_total_price!=$value['total_price']){
                    display_output('0',ucfirst($value['item_name']). ' total price has been changed.');
                }
            }
        }else{
            display_output('0','Something went wrong!!.');
        }
        
    }
    public function create_event(){
        if($this->input->post()) {
            
           $user_id=$this->session->userdata('user_id');
            $this->checkout();
            $this->form_validation->set_rules('event_title', 'event title', 'trim|required',array('required'=>'Please enter %s'));
            $this->form_validation->set_rules('event_date', 'event date', 'trim|required',array('required'=>'Please select %s'));
            $this->form_validation->set_rules('event_time', 'event time', 'trim|required',array('required'=>'Please select %s'));
            $this->form_validation->set_rules('members', 'memebers', 'trim|required',array('required'=>'Please enter %s'));
            $this->form_validation->set_rules('country', 'country', 'trim|required',array('required'=>'Please select %s'));
            $this->form_validation->set_rules('state', 'state', 'trim|required',array('required'=>'Please select %s'));
            $this->form_validation->set_rules('city', 'city', 'trim|required',array('required'=>'Please select %s'));
            
            $this->form_validation->set_rules('note', 'note', 'trim',array('required'=>'Please enter %s'));
            if($this->form_validation->run()==FALSE) 
            {
                display_output('0','Please fill all required fields.');
            }
            $latitude=$this->input->post('latitude');
            $longitude=$this->input->post('longitude');
            /*if($latitude=='' && $longitude==''){
                display_output('0','Please enter valid address.'); 
            }*/
            if(!empty($this->session->userdata('temp_user_item'))){
                if(!$item_details=$this->Common_model->getRecords('temp_table','*',array('user_id'=>$user_id,'temp_id'=>$this->session->userdata('temp_user_item')),'',false)){
                    display_output('0','Items are not available in cart.');
                }
            }
            // echo "<pre>";print_r($_POST);exit;
            $event_date=date('Y-m-d',strtotime($this->input->post('event_date')));
            $event_time=date('H:i:s',strtotime($this->input->post('event_time')));
            // $event_time=$this->input->post('event_time');
            if(isset($_POST['event_type'])){
                $event_type='Private';
            }else{$event_type='Public';}
            $service_id=$item_details[0]['service_id'];
            $tax=$this->Common_model->getFieldValue('settings','tax',array('id'=>1),'',true);
            $offer_where="status='Active' and is_deleted=0 and  service_id = '".$service_id."'  and (start_date <= '".date('Y-m-d H:i:s')."' and end_date >= '".date('Y-m-d H:i:s')."')";
            $offer_details=$this->Common_model->getRecords('offer','discount,offer_name',$offer_where,'',true);
            $user_details=$this->Common_model->getRecords('users','fullname,email,mobile,staging_id',array('user_id'=>$user_id),'',true);
            $sp_details=$this->Common_model->getRecords('services','user_id',array('id'=>$service_id),'',true);
            $event_date=convertLocalTimezoneToGMT($event_date.' '.$event_time,$this->session->userdata('user_timezone'),true);
            // $event_date=convertGMTToLocalTimezone($event_date.' '.$event_time,true);
            $event_datetime=date('Y-m-d H:i:s',strtotime($event_date));
            $insert_data=array(
                'user_id'=>$user_id,
                'service_id'=>$service_id,
                'sp_id'=>$sp_details['user_id'],
                'event_title'=>$this->input->post('event_title'),
                'event_date'=>$event_datetime,
                'event_type'=>$event_type,
                'members'=>$this->input->post('members'),
                'country_id'=>$this->input->post('country'),
                'state_id'=>$this->input->post('state'),
                'city_id'=>$this->input->post('city'),
                'event_address'=>$this->input->post('event_address'),
                'note'=>$this->input->post('note'),
                'latitude'=>$latitude,
                'longitude'=>$longitude,
                'status'=>'Active',
                'created'=>date('Y-m-d H:i:s'),
                );
            $this->db->trans_begin();
            if($last_id = $this->Common_model->addEditRecords('events',$insert_data)){
                $event_id=$last_id;
                $event_number='E'.str_pad($last_id,8,0,STR_PAD_LEFT);
                    $this->Common_model->addEditRecords('events',array('event_number'=>$event_number),array('id'=>$last_id));
                $user_data=array('user_id'=>$user_id,
                                'fullname'=>$user_details['fullname'],
                                'email'=>$user_details['email'],
                                'mobile'=>$user_details['mobile'],
                                'event_id'=>$last_id,
                                'event_date_time'=>$event_datetime,
                                'service_id'=>$service_id,
                                'ip_address'=>$this->input->ip_address(),
                                'created'=>date('Y-m-d H:i:s')
                    );
                if($order_id = $this->Common_model->addEditRecords('orders',$user_data)){
                     $order_number=str_pad($order_id,8,0,STR_PAD_LEFT);
                     $this->Common_model->addEditRecords('orders',array('order_number'=>$order_number),array('id'=>$order_id));
                    $sub_total=0;
                    $discount=0;
                    $order_item_total=0;
                    $sub_item_total_price=0;
                    $discount_title='Discount';
                    if(!empty($offer_details['discount'])){
                        $discount=$offer_details['discount'];
                        $discount_title='Discount'.'-'.$offer_details['offer_name'];
                    }
                    $max_group_id=$this->Common_model->getRecords('order_status_history','max(`order_batch_id`) as order_group_id','','',true);
                    $order_history_details_max_id=$max_group_id['order_group_id']+1;
                    foreach ($item_details as $key => $value) {
                        $total_qty=($value['qty']*$value['customer_qty']);
                        $total_price=number_format($value['customer_qty']*$value['price'],2,'.','');
                        if(!empty($discount)){
                             $discounted_sell_price=number_format((($total_price*$discount)/100),2,'.','');
                             $discount_price=$total_price-$discounted_sell_price;
                             if($tax){
                                $tax_sell_price=number_format((($discount_price*$tax)/100),2,'.','');
                                $sell_price=$discount_price+$tax_sell_price;
                             }else{
                                $sell_price=$discount_price;
                             }
                        }else{
                            $sell_price=$total_price;
                            if($tax){
                                $tax_sell_price=number_format((($total_price*$tax)/100),2,'.','');
                                $sell_price=$total_price+$tax_sell_price;
                            }
                        }
                        $order_item_total+=$total_price;
                        $sub_total+=$sell_price;
                        $sub_item_total_price=$sub_total;
                        $insert_array[]=array('item_name' => $value['item_name'],
                                            'price' =>$value['price'],
                                            'unit' =>$value['unit'],
                                            'qty' =>$value['qty'],
                                            'customer_qty' =>$value['customer_qty'],
                                            'total_qty' =>$total_qty,
                                            'total_price' =>$total_price,
                                            'sell_price' =>$sell_price,
                                            'tax' =>$tax,
                                            'discount' =>$discount,
                                            'offer_name' =>$offer_details['offer_name'],
                                            'service_id'=>$service_id,
                                            'order_id'=>$order_id,
                                            'event_id'=>$last_id,
                                            'order_group_id'=>$order_history_details_max_id,
                                            'event_date_time'=>$event_datetime,
                                            'created'=>date('Y-m-d H:i:s'));
                    }
                    $this->db->insert_batch('order_details', $insert_array);
                    $order_status=array(
                        'order_batch_id'=>$order_history_details_max_id,
                        'order_id'=>$order_id,
                        'order_id'=>$order_id,
                        'status'=>'Pending',
                        'created'=>date('Y-m-d H:i:s'),
                        'ip_addres'=>$this->input->ip_address(),
                        );
                    $this->Common_model->addEditRecords('order_status_history',$order_status);
                    $this->Common_model->addEditRecords('orders',array('total_amount'=>$sub_item_total_price),array('id'=>$order_id));

                    $order_total1= array('order_batch_id'=>$order_history_details_max_id,'order_id'=>$order_id,'code'=>'subtotal','title'=>'Subtotal','value'=>$order_item_total,'sort_order'=>1,'created'=>date('Y-m-d H:i:s'));
                    $this->Common_model->addEditRecords('order_total',$order_total1);
                    
                    if($discount>0){
                        $deduct_discount=($sub_item_total_price*$discount)/100;
                        $gt=$sub_item_total_price-$deduct_discount;
                        $order_total2= array('order_batch_id'=>$order_history_details_max_id,'order_id'=>$order_id,'code'=>'discount','title'=>$discount_title,'value'=>$deduct_discount,'discount_value'=>$discount,'sort_order'=>2,'created'=>date('Y-m-d H:i:s'));
                    }else{
                        $gt=$sub_item_total_price;
                        $order_total2= array('order_batch_id'=>$order_history_details_max_id,'order_id'=>$order_id,'code'=>'discount','title'=>$discount_title,'value'=>0,'discount_value'=>0,'sort_order'=>2,'created'=>date('Y-m-d H:i:s'));
                    }
                    $order_total3= array('order_batch_id'=>$order_history_details_max_id,'order_id'=>$order_id,'code'=>'tax','title'=>'Tax','value'=>$tax,'sort_order'=>3,'created'=>date('Y-m-d H:i:s'));
                    $order_total4= array('order_batch_id'=>$order_history_details_max_id,'order_id'=>$order_id,'code'=>'total','title'=>'Total','value'=>$sub_item_total_price,'sort_order'=>4,'created'=>date('Y-m-d H:i:s'));
                    $this->Common_model->addEditRecords('order_total',$order_total2);
                    $this->Common_model->addEditRecords('order_total',$order_total3);
                    $this->Common_model->addEditRecords('order_total',$order_total4);
                }
            }
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                display_output('0','Something went wrong. Please try again.'); 
            } else {
                $this->db->trans_commit();
                if(!empty($this->session->userdata('temp_user_item'))){
                    $this->Common_model->deleteRecords('temp_table',array('temp_id'=>$this->session->userdata('temp_user_item')));
                    unset($_SESSION['temp_user_item']);
                }
                
                display_output('1',base64_encode($event_id));
            }
        }else{
            display_output('0','Something went wrong. Please try again.');
        }
    }
    function make_payment($event_ids)
    {  
            $event_id=base64_decode($event_ids);
            $user_id=$this->session->userdata('user_id');
            $order_info=$this->Common_model->getRecords('orders','id,order_number,total_amount',array('event_id'=>$event_id),'',true);

            $sub_item_total_price=$order_info['total_amount'];
            $returnURL = base_url().'notify-payment';
            $cancelURL = base_url().'book-event-service';
            $notifyURL =base_url().'notify-payment';
            $timezone =$this->session->userdata('user_timezone');
            $this->paypal_lib->add_field('return', $returnURL);
            $this->paypal_lib->add_field('cancel_return', $cancelURL);
            $this->paypal_lib->add_field('notify_url', $notifyURL);
            $this->paypal_lib->add_field('item_name', $timezone);
            $this->paypal_lib->add_field('custom', '');
            $this->paypal_lib->add_field('item_number',$event_ids);
            $this->paypal_lib->add_field('amount',  $sub_item_total_price);
            // Render paypal form
            $this->paypal_lib->paypal_auto_form();
        
    }
    function notify_payment(){
        //echo '<pre>';print_r($_SESSION);
        $user_id=$this->session->userdata('user_id');
        $this->load->model('App_model');
        //$timezone =$this->session->userdata('admin_timezone');
        $timezone =$this->session->userdata('user_timezone');
        $paypalInfo = $this->input->post();
        if(!empty($paypalInfo)){
            
            if(!empty($paypalInfo["txn_id"])){
                $ipnCheck = $this->paypal_lib->validate_ipn($paypalInfo);
                if($ipnCheck){
                     $event_id=base64_decode($paypalInfo["item_number"]);
                    //$sp_id=$this->Common_model->getFieldValue('events','sp_id',array('id'=>$event_id),'',true);

                    $event_info=$this->Common_model->getRecords('events','user_id,sp_id,event_title,event_type,service_id,event_date',array('id'=>$event_id,'status'=>'Active','type'=>'Draft'),'',true);
                   
                    if(empty($event_info)){
                        $this->session->set_flashdata('error', 'Payment failed due to some malicious activity.');
                        redirect('service-providers');
                    }
                    if(!isset($_SESSION['user_id']) && empty($_SESSION['user_id'])){
                        $user_id=$event_info['user_id'];
                        if($user_data = $this->Common_model->getRecords('users','user_id,fullname,staging_id,mobile,user_type,email,status',array('user_id'=>$user_id),'user_id DESC',true)) {
                            $login=array(   
                                'user_id'=>$user_data['user_id'],
                                'fullname'=>$user_data['fullname'],
                                'staging_id'=>$user_data['staging_id'],
                                'mobile'=>$user_data['mobile'],
                                'email'=>$user_data['email'],
                                'front_user_type'=>$user_data['user_type'],
                                'status'=>$user_data['status'],
                                );
                            $this->session->set_userdata($login);
                            $this->session->set_userdata('user_timezone',$paypalInfo["item_name1"]);
                        }
                       
                    }
                    //echo 'user_id'.$user_id;echo '<br>';
                    $sp_id=$event_info['sp_id'];
                    $event_title=$event_info['event_title'];
                    $order_info=$this->Common_model->getRecords('orders','id,order_number,total_amount',array('event_id'=>$event_id),'',true);

                    $order_id=$order_info['id'];
                    if($check_pay=$this->Common_model->getRecords('payment_history','id',array('order_id'=>$order_id,'transaction_id'=>$paypalInfo["txn_id"]),'',true)){
                        $this->session->set_flashdata('success', 'Service Booked successfully.');
                        redirect('my-events');
                    }
                    $sub_item_total_price=$order_info['total_amount'];
                    $order_number=$order_info['order_number'];
                    $this->db->trans_begin();
                        //$paypal_fees = ($sub_item_total_price*2.90/100)+0.30;  // paypal fee
                        $paypal_fees = $paypalInfo["payment_fee"];  // paypal fee
                        $payment_history=array(
                            'order_id'=>$order_id,
                            'transaction_id'=>$paypalInfo["txn_id"],
                            'total_order_amount'=>$sub_item_total_price,
                            'paid_amount'=>$sub_item_total_price,
                            'customer_id'=>$user_id,
                            'service_provider_id'=>$sp_id,
                            'payment_type'=>'Order',
                            'transfer_type'=>'Customer_To_Admin',
                            'paypal_fee_amount'=>$paypal_fees,
                            'user_will_get'=>($sub_item_total_price-$paypal_fees),
                            'payment_status'=>$paypalInfo["payment_status"],
                            'created'=>date('Y-m-d H:i:s'),
                            'ip_addres'=>$this->input->ip_address(),
                            );
                        //echo '<pre>';print_r($payment_history);exit;
                        $this->Common_model->addEditRecords('payment_history',$payment_history);
                        $this->Common_model->addEditRecords('events',array('type'=>'Actual'),array('id'=>$event_id));
                        $this->Common_model->addEditRecords('orders',array('type'=>'Actual','payment_status'=>'Paid'),array('id'=>$order_id));
                        $this->Common_model->addEditRecords('order_details',array('type'=>'Actual'),array('order_id'=>$order_id));
                        $this->Common_model->addEditRecords('order_status_history',array('type'=>'Actual'),array('order_id'=>$order_id));
                        $this->Common_model->addEditRecords('order_total',array('type'=>'Actual'),array('order_id'=>$order_id));
                        if($event_info['event_type']=='Public'){
                            $is_private=0;
                        }else{
                            $is_private=1;
                        }
                        $post_data=array(
                            'user_id'=>$user_id,
                            'post_type'=>2,
                            'event_id'=>$event_id,
                            'sp_id'=>$sp_id,
                            'is_private'=>$is_private,
                            'post_content'=>$event_title.'^'.$event_info['event_date'],
                            'created'=>date('Y-m-d H:i:s'),
                        );
                        if($post_id=$this->Common_model->addEditRecords('post',$post_data)){
                            $post_number='Post'.str_pad($post_id,8,0,STR_PAD_LEFT);
                            $this->Common_model->addEditRecords('post',array('post_number'=>$post_number),array('id'=>$post_id));
                            $post_media=array(
                                'post_id'=>$post_id,
                                'user_id'=>$user_id,
                                'media_path' =>getServiceCategoryImage($event_info['service_id']) ,
                                'video_thumbnail' =>'',
                                'video_large_thumbnail' =>'',
                                'duration' =>'',
                                'media_type' => 'image',
                                'created' => date('Y-m-d H:i:s'),
                            );
                            $this->Common_model->addEditRecords('post_media',$post_media);
                        }
                    $this->db->trans_complete();
                    if ($this->db->trans_status() === FALSE){
                        $this->db->trans_rollback();
                        display_output('0','Something went wrong. Please try again.'); 
                    } else {
                        $this->db->trans_commit();
                        $this->Front_common_model->send_order_invoice($order_id,'Customer_To_Admin',$timezone);
                        $title= 'New event created.';
                        $content='Thank you for your order purchase your order number is #'.$order_number.'.';
                        $notdata['type']='new_event';
                        $this->Common_model->push_notification_send($user_id,$notdata,$event_id,$title,$content,'',$user_id);
                        $title1= 'New Order.';
                        $content1='A new order is placed for you order number is #'.$order_info['order_number'].'.';
                        $notdata1['type']='new_order';
                        $this->Common_model->push_notification_send($sp_id,$notdata1,$event_id,$title1,$content1,'',$user_id);
                        $frontend_username = getFrontUsername($user_id);
                        $log_msg = getUserType()." ".$frontend_username." has created a new event '".$event_title."'.";
                        actionLog('events',$event_id,'add',$log_msg,'User',$user_id); 

                        $this->session->set_flashdata('success', 'Service Booked successfully.');
                        redirect('my-events');
                    }
                }else{
                    $this->session->set_flashdata('error', 'Payment failed.');
                    redirect('service-providers');
                }
            }else{
                $this->session->set_flashdata('error', 'Payment failed.');
                redirect('service-providers');
            }
        }else{
            $this->session->set_flashdata('error', 'Some error occured in payment.');
            redirect('service-providers');
        }
    }

    /////////////////////////////upcoming events///////////////////////////////
    public function manage_upcoming_event_list() 
    {
        $data['title']= 'Explore' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Explore'." | ".SITE_TITLE;
        $data['meta_desc']= 'Explore'." | ".SITE_TITLE;
        $data['page_title'] = "Explore";
        $data['page_sub_title'] = "Find best service provider's here";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => 'home'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Explore',
            'link' => 'service-providers'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Upcoming Events',
            'link' => ''
        ); 
        
        $this->Common_model->check_user_login();
        $user_id= $this->session->userdata('user_id');
        $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);
        //$data['total_records']=$this->Explore_model->get_upcoming_public_list(0,0,$user_id,$user_address['city'],'all');
        $data['total_records']=$this->Explore_model->get_upcoming_public_list(0,0,$user_id,$user_address['country'],$user_address['state'],$user_address['city'],'all');

        $data['countries']=$this->Common_model->getRecords('countries', 'id,name', array(), 'name', false);
        $data['filter_hide']=1;
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/celebrant/upcoming_event_list');
        $this->load->view('front/include/footer');
    }
    public function manage_upcoming_event_data(){
        $this->Common_model->check_user_login();
        if(isset($_POST) && !empty($_POST)) {
            $page_number    = trim($_POST['page']);
            $item_per_page = FRONT_LIMIT;
            $offset = (($page_number) * $item_per_page);
            $user_id= $this->session->userdata('user_id');
            $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);
            //$data['details'] = $this->Explore_model->get_upcoming_public_list($offset,$item_per_page,$user_id,$user_address['city'],'all');
             $data['details'] = $this->Explore_model->get_upcoming_public_list($offset,$item_per_page,$user_id,$user_address['country'],$user_address['state'],$user_address['city'],'all');
             //echo $this->db->last_query();exit;
            $data['filter_hide']=1;
            echo $all_events = $this->load->view('front/celebrant/upcoming_events_data',$data,true); exit;
        }
    }
     public function upcoming_event_filter(){
        $this->Common_model->check_user_login();
       if(isset($_POST) && !empty($_POST)) {
            $country_id=isset($_POST['country_id'])?$_POST['country_id']:'';
            $state_id=isset($_POST['state_id'])?$_POST['state_id']:'';
            $item_per_page = FRONT_LIMIT;
            $offset=0;

            if(isset($country_id) && !empty($country_id)){
                $i=0;
                $where='';
                foreach ($country_id as $key => $value) {

                    if($i==0){
                        $where.="country_id=$value";
                    }else{
                        $where.=" OR country_id=$value";
                    }$i++;
                }
                $data['states']=$this->Common_model->getRecords('states', 'id,name', $where, 'name', false);
               
            } 
            if(isset($state_id) && !empty($state_id)){
                $i=0;
                $where='';
                foreach ($state_id as $key => $value) {
                    if($i==0){
                        $where.="state_id=$value";
                    }else{
                        $where.=" OR state_id=$value";
                    }$i++;
                }
                $data['cities']=$this->Common_model->getRecords('cities', 'id,name', $where, 'name', false);
            }
          
            $data['countries']=$this->Common_model->getRecords('countries', 'id,name', array(), 'name', false);
            $data['filter_hide']=1;
            $user_id= $this->session->userdata('user_id');
            $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);
            $data['total_records']=$this->Explore_model->get_upcoming_public_list(0,0,$user_id,$user_address['country'],$user_address['state'],$user_address['city'],'all');
            //echo '<pre>';print_r($data);exit;
            echo $all_providers = $this->load->view('front/celebrant/upcoming_event_list',$data,true); exit;
        }
    }
    public function upcoming_event_details($id) 
    {

        $event_id=base64_decode($id);
        $data['title']= 'Event Details' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Event Details'." | ".SITE_TITLE;
        $data['meta_desc']= 'Event Details'." | ".SITE_TITLE;
        $data['page_title'] = 'Event Details';
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => 'home'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Explore',
            'link' => 'service-providers'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Upcoming Events',
            'link' => 'upcoming-events'
        ); 
        $data['breadcrumbs'][] = array(
            'title' => 'Details',
            'link' => ''
        );
        $this->Common_model->check_user_login();
        $user_id= $this->session->userdata('user_id');
        if(!$data['booking_details']=$this->Booking_model->booking_details($event_id,$user_id,'event')){
            redirect('pages/page_not_found');
        }
        if($data['booking_details']['user_id']==$user_id){
            redirect('pages/page_not_found');
        }
        $data['page_type']='upcoming';
        
        $data['page_sub_title'] = ucfirst($data['booking_details']['event_title']);
        $data['be_my_date']= $this->Home_model->be_my_date_list(3,3,$user_id,'limit');
        $order_id = $this->Common_model->getFieldValue('orders','id',array('event_id'=>$event_id),'',true);
        $data['order_record']=$this->Booking_model->booking_detail_order($order_id);
        $data['pending']=0;
        $data['accepted']=0;
        $data['rejected']=0;
        $where_request_status="event_id =$event_id and (sender_id=$user_id OR receiver_id =$user_id)";
        $data['my_request_status']=$this->Common_model->getRecords('event_attendee','id as request_id,status,event_owner',$where_request_status,'',true);
        if($request_status = $this->Common_model->getRecords('event_attendee','count("status") as count,status',array('event_id'=>$event_id),'',false,'status')){
           foreach($request_status as $row){
                if($row['status']=='Accepted'){
                    $data['accepted']=$row['count'];
                    if($row['count']>3){
                        $data['total_attendee']=$row['count']-3;
                    }else{
                        $data['total_attendee']=0;
                    }
                }if($row['status']=='Rejected'){
                    $data['rejected']=$row['count'];
                }
                if($row['status']=='Pending'){
                    $data['pending']=$row['count'];
                }
           }
        }
       
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/celebrant/upcoming_event_details');
        $this->load->view('front/include/footer');
    }
    public function public_event_list() 
    {
        $data['title']= 'Public Events' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Public Events'." | ".SITE_TITLE;
        $data['meta_desc']= 'Public Events'." | ".SITE_TITLE;
        $data['page_title'] = "Public Events";
        $data['page_sub_title'] = "Find your all event history";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Public Events',
            'link' => ''
        );
        
        $this->Common_model->check_user_login();
        $user_id= $this->session->userdata('user_id');
        $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);
        $data['total_records']=$this->Explore_model->get_public_events(0,0,$user_id,$user_address['country'],$user_address['state'],$user_address['city']);
        $data['countries']=$this->Common_model->getRecords('countries', 'id,name', array(), 'name', false);
        $data['filter_hide']=1;
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/celebrant/public_event_list');
        $this->load->view('front/include/footer');
    }
    public function public_event_data(){
        $this->Common_model->check_user_login();
        if(isset($_POST) && !empty($_POST)) {
            $page_number    = trim($_POST['page']);
            $item_per_page = FRONT_LIMIT;
            $offset = (($page_number) * $item_per_page);
            $user_id= $this->session->userdata('user_id');
            $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);
            $data['bookings'] = $this->Explore_model->get_public_events($offset,$item_per_page,$user_id,$user_address['country'],$user_address['state'],$user_address['city']);
           // echo $this->db->last_query();
            $data['filter_hide']=1;
            echo $all_events = $this->load->view('front/celebrant/public_event_data',$data,true); exit;
        }
    }
    public function public_event_filter(){
        $this->Common_model->check_user_login();
        if(isset($_POST) && !empty($_POST)) {
            $country_id=isset($_POST['country_id'])?$_POST['country_id']:'';
            $state_id=isset($_POST['state_id'])?$_POST['state_id']:'';
            $item_per_page = FRONT_LIMIT;
            $offset=0;

            if(isset($country_id) && !empty($country_id)){
                $i=0;
                $where='';
                foreach ($country_id as $key => $value) {

                    if($i==0){
                        $where.="country_id=$value";
                    }else{
                        $where.=" OR country_id=$value";
                    }$i++;
                }
                $data['states']=$this->Common_model->getRecords('states', 'id,name', $where, 'name', false);
            } 
            if(isset($state_id) && !empty($state_id)){
                $where='';$i=0;
                foreach ($state_id as $key => $value) {
                    if($i==0){
                       $where.="state_id=$value";
                    }else{
                       $where.=" OR state_id=$value";
                    }$i++;
                }
                
                $data['cities']=$this->Common_model->getRecords('cities', 'id,name', $where, 'name', false);
            }
          
            $data['countries']=$this->Common_model->getRecords('countries', 'id,name', array(), 'name', false);
            $data['filter_hide']=1;
            $user_id= $this->session->userdata('user_id');
            $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);
            $data['total_records']=$this->Explore_model->get_public_events(0,0,$user_id,$user_address['country'],$user_address['state'],$user_address['city']);
            //echo '<pre>';print_r($data);exit;
            echo $all_providers = $this->load->view('front/celebrant/public_event_list',$data,true); exit;
        }
    }
    public function public_event_details($id) 
    {

        $event_id=base64_decode($id);
        $data['title']= 'Event Details' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Event Details'." | ".SITE_TITLE;
        $data['meta_desc']= 'Event Details'." | ".SITE_TITLE;
        $data['page_title'] = 'Event Details';
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => 'home'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Explore',
            'link' => 'service-providers'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Public Events',
            'link' => 'public-events'
        ); 
        $data['breadcrumbs'][] = array(
            'title' => 'Details',
            'link' => ''
        );
        $this->Common_model->check_user_login();
        $user_id= $this->session->userdata('user_id');
        if(!$data['booking_details']=$this->Booking_model->booking_details($event_id,$user_id,'event')){
            redirect('pages/page_not_found');
        }
        if($data['booking_details']['user_id']==$user_id){
            redirect('pages/page_not_found');
        }
        $data['page_type']='public';
        
        $data['page_sub_title'] = ucfirst($data['booking_details']['event_title']);
        $data['be_my_date']= $this->Home_model->be_my_date_list(3,3,$user_id,'limit');
        $order_id = $this->Common_model->getFieldValue('orders','id',array('event_id'=>$event_id),'',true);
        $data['order_record']=$this->Booking_model->booking_detail_order($order_id);
        $data['pending']=0;
        $data['accepted']=0;
        $data['rejected']=0;
        $where_request_status="event_id =$event_id and (sender_id=$user_id OR receiver_id =$user_id)";
        $data['my_request_status']=$this->Common_model->getRecords('event_attendee','id as request_id,status,event_owner',$where_request_status,'',true);
        if($request_status = $this->Common_model->getRecords('event_attendee','count("status") as count,status',array('event_id'=>$event_id),'',false,'status')){
           foreach($request_status as $row){
                if($row['status']=='Accepted'){
                    $data['accepted']=$row['count'];
                    if($row['count']>3){
                        $data['total_attendee']=$row['count']-3;
                    }else{
                        $data['total_attendee']=0;
                    }
                }if($row['status']=='Rejected'){
                    $data['rejected']=$row['count'];
                }
                if($row['status']=='Pending'){
                    $data['pending']=$row['count'];
                }
           }
        }
       
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/celebrant/upcoming_event_details');
        $this->load->view('front/include/footer');
    }
    public function invite_friend(){
        $this->Common_model->check_user_login();
        $event_id  = base64_decode($this->input->post('event_id'));
        $user_id= $this->session->userdata('user_id');
        $request_type  = $this->input->post('request_type');//invite or request_to_join
        if(empty($event_id)) {
            display_output('0','Please enter event id.');
        }
        if(empty($request_type)) {
            display_output('0','Please enter request type.');
        }
        if($request_type!='invite' &&  $request_type!='request_to_join'){
            display_output('0','Request type either invite or request_to_join.');
        }
        if(!$event_record = $this->Common_model->getRecords('events','id,event_title,user_id,is_canceled,event_date,event_type',array('id'=>$event_id,'status'=>'Active','is_deleted'=>0),'',true)){
            display_output('0','Event isn\'t available.');
        }
        if($event_record['is_canceled']==1){
            display_output('0','Event has been canceled.');
        }
        $current_date=date('Y-m-d H:i:s');
        if($current_date > $event_record['event_date']){
            display_output('0','Event time has been passed.');
        }
       //echo $this->db->last_query();exit;
        
       
        if($request_type=='invite'){
            $receiver_id  = base64_decode($this->input->post('receiver_id'));
            if(empty($receiver_id)) {
                display_output('0','Please enter receiver id.');
            }
            if($event_record['user_id']!=$user_id){
                display_output('0','You are not authorized to invite friends for this event.');
            }
            $user_fullname=$this->Common_model->getFieldValue('users','fullname',array('user_id'=>$user_id),'',true);
            $receiver_staging=$this->Common_model->getFieldValue('users','staging_id',array('user_id'=>$receiver_id),'',true);
            $check_invitation="((sender_id='".$user_id."' and receiver_id ='".$receiver_id."' )or (receiver_id ='".$user_id."' and sender_id ='".$receiver_id."')) and event_id ='".$event_id."'";
            $check_invitation_record = $this->Common_model->getRecords('event_attendee','id,status',$check_invitation,'',true);
            if(!empty($check_invitation_record) && $check_invitation_record['status']!='Rejected'){
                display_output('0','You already invited this user.');
            }
            $insert_data=array(
                'event_id'=>$event_id,
                'sender_id'=>$user_id,
                'receiver_id'=>$receiver_id,
                'event_owner'=>'Self',
                'created' =>date('Y-m-d H:i:s')
            );
            
            if($last_id=$this->Common_model->addEditRecords('event_attendee',$insert_data)) {

                $title='Event Invitation.';
                $content=ucfirst($user_fullname).' invites you in '.$event_record['event_title'];
                $notdata['type']='event_invite';
                $this->Common_model->push_notification_send($receiver_id,$notdata,$event_id,$title,$content,'',$user_id);
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getUserType()." ".$frontend_username." has invited '".$receiver_staging."' to join event '".$event_record['event_title']."'.";
                actionLog('event_attendee',$last_id,'Invite User',$log_msg,'User',$user_id);
                display_output('1','Invitation sent successfully.');
            } else {
                display_output('0','Something went wrong. Please try again.');
            }
        }else{
            if($event_record['event_type']=='Private'){
                display_output('0','This is private event.You are not authorized to request on this event.');
            }
            $receiver_id=$event_record['user_id'];
            $user_fullname=$this->Common_model->getFieldValue('users','fullname',array('user_id'=>$user_id),'',true);
            $check_invitation="((sender_id='".$user_id."' and receiver_id ='".$receiver_id."' )or (receiver_id ='".$user_id."' and sender_id ='".$receiver_id."')) and event_id ='".$event_id."'";
            $check_invitation_record = $this->Common_model->getRecords('event_attendee','id,status',$check_invitation,'',true);

            if(!empty($check_invitation_record) && $check_invitation_record['status']!='Rejected'){
                display_output('0','You already requested for join this event.');
            }
            $insert_data=array(
                'event_id'=>$event_id,
                'sender_id'=>$user_id,
                'receiver_id'=>$receiver_id,
                'event_owner'=>'Other',
                'created' =>date('Y-m-d H:i:s')
            );
            if($last_id=$this->Common_model->addEditRecords('event_attendee',$insert_data)) {
                $title='Request to join event.';
                $content=ucfirst($user_fullname).' requested to join '.$event_record['event_title'];
                $notdata['type']='request_to_join';
                $this->Common_model->push_notification_send($receiver_id,$notdata,$event_id,$title,$content,'',$user_id);
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getUserType()." ".$frontend_username." has requested to join event '".$event_record['event_title']."'.";
                actionLog('event_attendee',$last_id,'Request to Join',$log_msg,'User',$user_id);
                display_output('1','Invitation request sent successfully.');
            } else {
                display_output('0','Something went wrong. Please try again.');
            }
        }
        
    }

    /**************************************Follow User *************************/

    public function follow_user(){
        if($this->input->post()){
            $user_id= $this->session->userdata('user_id');
            $user_type= $this->session->userdata('front_user_type');
            if($user_type==2){
                display_output('0','You are not authorized to follow any user.');
            }
            $follow_to  = base64_decode($this->input->post('id'));
            $insert_data = array(
                'follow_to'=>$follow_to,
                'followed_by'=>$user_id,
                'created'=> date("Y-m-d H:i:s"),
            );
            $where=array('follow_to'=>$follow_to,'followed_by'=>$user_id);
            if($check_status= $this->Common_model->getRecords('follow_users','id',$where,'',true)){
                $insert_data['status']=2;
                $this->db->trans_begin();
                    $last_id=$this->Common_model->addEditRecords('follow_user_log', $insert_data);
                    $this->Common_model->deleteRecords('follow_users',$where);
                    $frontend_username = getFrontUsername($user_id);
                    $log_msg = getUserType()." ".$frontend_username." unfollow ".getFrontUsername($follow_to).".";
                    actionLog('follow_user_log',$last_id,'Unfollow',$log_msg,'User',$user_id);
                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    display_output('0','Something went wrong. Please try again.'); 
                } else {
                    $this->db->trans_commit();
                    removeNotification($follow_to,'follow_user',$check_status['id']); 
                    display_output('1','Join');
                } 
            }else{
                $this->db->trans_begin();
                    $f_last_id=$this->Common_model->addEditRecords('follow_users', $insert_data);
                    $insert_data['status']=1;
                    $last_id=$this->Common_model->addEditRecords('follow_user_log', $insert_data);
                    $frontend_username = getFrontUsername($user_id);
                    $log_msg = getUserType()." ".$frontend_username." starting following ".getFrontUsername($follow_to).".";
                    actionLog('follow_user_log',$last_id,'follow',$log_msg,'User',$user_id); 
                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    display_output('0','Something went wrong. Please try again.'); 
                } else {
                    $this->db->trans_commit();
                    $title=ucfirst($frontend_username).' starting following you';
                    $content=ucfirst($frontend_username).' starting following you';
                    $notdata['type']='follow_user';
                    $this->Common_model->push_notification_send($follow_to,$notdata,$f_last_id,$title,$content,'',$user_id);
                    display_output('1','Un-Join');
                } 
            }
        }else{
            display_output('0','Some error occured! Please try again.');
        }
    }

    /**************************************Remove from deal  *************************/
    public function remove_from_deal(){
        $this->Common_model->check_user_login();
        if($this->input->post()){
            $user_id= $this->session->userdata('user_id');
            $event_id  = base64_decode($this->input->post('event_id'));
            if(empty($event_id)) {
                display_output('0','Please enter event id.');
            }
            $event_details=$this->Common_model->getRecords('events','id,service_id,sp_id,event_title,event_address,members,country_id,state_id,city_id,latitude,longitude,event_date,event_type,note',array('id'=>$event_id),'',true);
            if($user_id!=$event_details['sp_id']){
                display_output('0','You are not authorize to remove this event from deal.');
            }
            if(!$check_deal=$this->Common_model->getRecords('events','deal_id',array('deal_id'=>$event_id,'is_canceled'=>0),'',true)){
                display_output('0','This event is not a deal.');
            }
            if(!empty($event_details)){
                $event_data=array(
                    'deal_id'=>$event_details['id'],
                );
                
                $this->db->trans_begin();
                    $event_id = $this->Common_model->addEditRecords('events',$event_data);
                    $event_number='E'.str_pad($event_id,8,0,STR_PAD_LEFT);
                    $this->Common_model->addEditRecords('events',array('event_number'=>$event_number),array('id'=>$event_id));
                    $frontend_username = getFrontUsername($user_id);
                    $log_msg = getUserType()." ".$frontend_username." convert ".$event_details['event_title']." in to deal.";
                    actionLog('events',$event_id,'add_deal',$log_msg,'User',$user_id); 
                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    display_output('0','Something went wrong. Please try again.'); 
                } else {
                    $this->db->trans_commit();
                    display_output('1','Deals added successfully.');
                }
                
            }else{
                display_output('0','Event not found.');
            }
        }else{
            display_output('0','Some error occured! Please try again.');
        }
    }
}
