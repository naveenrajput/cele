<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Booking extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(array('admin/Common_model','front/Booking_model','Front_common_model','front/Home_model'));
		$this->load->helper('common_helper');
		/*$user_type = $this->session->userdata('front_user_type'); 
        if($user_type!='1'){
            $doc_status=check_user_document_status();
            if($doc_status!='Accepted'){
                redirect('edit-profile');
            }
        }*/

        
	}
	
	public function sp_my_booking() 
	{
        
        $data['title']= 'My Booking' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'My Booking'." | ".SITE_TITLE;
        $data['meta_desc']= 'My Booking'." | ".SITE_TITLE;
        $data['page_title'] = "My Booking";
        $data['page_sub_title'] = "Find your all event history";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'My Bookings',
            'link' => ''
        );
        
       	$this->Common_model->check_user_login();
        if($this->session->userdata('front_user_type')!='2')
        {
            redirect('home'); 
        }
        $doc_status=check_user_document_status();
        if($doc_status!='Accepted'){
            redirect('edit-profile');
        } 
        if(isset($_POST) && !empty($_POST)){
            $data['name']=$_POST['name'];
            //$data['event_status']=$_POST['event_stauts'];
            
        }
        $data['filter']='0';
        $data['total_records'] = $this->Booking_model->my_bookings(0,0);
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/sp/my_booking');
        $this->load->view('front/include/footer');
	}
    public function sp_my_booking_data(){
        $this->Common_model->check_user_login();
        if($this->session->userdata('front_user_type')!='2')
        {
            redirect('home'); 
        } 
        if(isset($_POST) && !empty($_POST)) {
            $page_number    = trim($_POST['page']);
            $item_per_page = FRONT_LIMIT;
            $offset = (($page_number) * $item_per_page);
            $name=isset($_POST['name'])?$_POST['name']:'';
            $status=isset($_POST['event_status'])?$_POST['event_status']:'';
            $data['bookings'] = $this->Booking_model->my_bookings($offset,$item_per_page,$name,$status);
           // echo $this->db->last_query();
            echo $all_services = $this->load->view('front/sp/my_booking_data',$data,true); exit;
        }
    }
    public function sp_my_booking_filter(){
        $this->Common_model->check_user_login();
        if($this->session->userdata('front_user_type')!='2')
        {
            redirect('home'); 
        } 
       if(isset($_POST) && !empty($_POST)) {
            $name=isset($_POST['name'])?$_POST['name']:'';
            $status=isset($_POST['event_status'])?$_POST['event_status']:'';
            $item_per_page = FRONT_LIMIT;
            //$offset = (($page_number) * $item_per_page);
            $offset=0;
            $data['filter']='1';
            $data['total_records'] = $this->Booking_model->my_bookings(0,0,$name,$status);
            
            $data['bookings'] = $this->Booking_model->my_bookings($offset,$item_per_page,$name,$status);
           // echo $this->db->last_query();
            echo $all_services =$this->load->view('front/sp/my_booking',$data,true);exit;
            //echo $all_services = $this->load->view('front/sp/my_booking_data',$data,true); exit;
        }
    }

    public function booking_details($id) 
    {
        $this->Common_model->check_user_login();
        $seg=$this->uri->segment(1);
        if($this->session->userdata('front_user_type')=='1')
        {
            redirect('home'); 
        }
        $doc_status=check_user_document_status();
        if($doc_status!='Accepted'){
            redirect('edit-profile');
        }  
        $event_id=base64_decode($id);
        $data['title']= 'Booking Details' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Booking Details'." | ".SITE_TITLE;
        $data['meta_desc']= 'Booking Details'." | ".SITE_TITLE;
       
        $data['page_sub_title'] = "Find your all events history";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => base_url()
        );
        if($seg=='csp-booking-details'){
            $data['breadcrumbs'][] = array(
                'title' => 'My Bookings',
                'link' => 'csp-my-bookings'
            );
        }else{
            $data['breadcrumbs'][] = array(
                'title' => 'My Bookings',
                'link' => 'my-bookings'
            );
        }
        
        $data['breadcrumbs'][] = array(
            'title' => 'Booking Details',
            'link' => ''
        );
        
        $user_id= $this->session->userdata('user_id');
        if(!$data['booking_details']=$this->Booking_model->booking_details($event_id,$user_id,'booking')) {
            redirect('pages/page_not_found');
        }
       // echo '<pre>';print_r($data['booking_details']);
        $data['page_title'] = ucfirst($data['booking_details']['event_title']);
       // $data['order_record']=$this->Booking_model->booking_detail_order($event_id);
        $order_id = $this->Common_model->getFieldValue('orders','id',array('event_id'=>$event_id),'',true);
        $data['order_record']=$this->Booking_model->booking_detail_order($order_id);
       // echo '<pre>';print_r($data['order_record']);exit;
        $data['pending']=0;
        $data['accepted']=0;
        $data['rejected']=0;
        if($request_status = $this->Common_model->getRecords('event_attendee','count("status") as count,status',array('event_id'=>$event_id),'',false,'status')){
           foreach($request_status as $row){
                if($row['status']=='Accepted'){
                    $data['accepted']=$row['count'];
                }if($row['status']=='Rejected'){
                    $data['rejected']=$row['count'];
                }
                if($row['status']=='Pending'){
                    $data['pending']=$row['count'];
                }
           }
        }
        $data['items'] = $this->Common_model->getRecords('order_details','item_name,price,qty,unit,customer_qty,total_price,sell_price,tax,discount',array('event_id'=>$event_id,'is_deleted'=>0,'type'=>'Actual'),'',false);
        $data['order_total']=$this->Common_model->getRecords('order_total','code,title,value,discount_value',array('order_id'=>$data['order_record']['order_id'],'status'=>'Active'),'',false);
        //echo '<pre>';print_r($data);exit;
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/sp/my_booking_details');
        $this->load->view('front/include/footer');
    }
    public function get_event_attendee($status,$id,$user_id) 
    {
        $this->Common_model->check_user_login();
        $data['details']=$this->Booking_model->get_event_attendee(base64_decode($id),$status,base64_decode($user_id));
        $data['total']=count($data['details']);
        return $this->load->view('front/include/attendee_modal',$data);
    }
    public function sp_accept_reject_booking(){
        $this->Common_model->check_user_login();
        if($this->session->userdata('front_user_type')=='1')
        {
            redirect('home'); 
        } 
        
        $doc_status=check_user_document_status();
        if($doc_status!='Accepted'){
            redirect('edit-profile');
        } 
        if(isset($_POST) && !empty($_POST)) {
            $user_id= $this->session->userdata('user_id');
            $event_id  = base64_decode($this->input->post('event_id'));
            $order_status_id  = base64_decode($this->input->post('order_status_id'));
            $status  = $this->input->post('status');
            $sp_order_cancel_date  = $this->input->post('sp_order_cancel_date');
            $this->Booking_model->check_subscription_status();
            $current_date=date('Y-m-d H:i:s');
            if($status!='Accepted' &&  $status!='Rejected'){
                display_output('0','Status either Accepted or Rejected.');
            }
            if(!$event_record = $this->Common_model->getRecords('events','id,event_title,user_id,is_canceled,event_date,sp_id',array('id'=>$event_id,'status'=>'Active','is_deleted'=>0),'',true)){
                display_output('0','Event isn\'t available.');
            }
            $current_date=convertGMTToLocalTimezone(date('Y-m-d H:i:s'));
            $current_time= convertGMTToLocalTimezone(date('Y-m-d H:i:s'),'',true);
            $curr_d=strtotime($current_date.' '.$current_time);
            $ev_date=convertGMTToLocalTimezone($event_record['event_date']);
            $ev_time= convertGMTToLocalTimezone($event_record['event_date'],'',true);
            $ev=strtotime($ev_date.' '.$ev_time);
            if($curr_d>$ev){
                display_output('0','Event time has been passed.');
            }
            if($event_record['is_canceled']==1){
                display_output('0','Event has been canceled.');
            }
            if($event_record['sp_id']!=$user_id){
                display_output('0','You are not authorized to accept this order.');
            }

            $order_record = $this->Common_model->getRecords('orders','id,order_number,order_status',array('event_id'=>$event_id),'id desc',true);
            if($order_record['order_status']!='Pending'){
                display_output('0','Order has been '.ucwords($order_record['order_status']).'.');
            }
            if($status=='Accepted'){
                $update_status='Accepted';
                if(empty($sp_order_cancel_date)) {
                    display_output('0','Please enter order cancel date.');
                }
            }else{$update_status='Rejected';}
            $order_history_record = $this->Common_model->getRecords('order_status_history','status',array('id'=>$order_status_id),'',true);
            if($order_history_record['status']!='Pending'){
                display_output('0','Order already '.ucwords($order_history_record['status']).'.');
            }
            $this->db->trans_begin();
            if($this->Common_model->addEditRecords('order_status_history',array('status'=>$update_status,'modified'=>date('Y-m-d H:i:s')), array('id'=>$order_status_id))) {
                $company_name=getUserInfo($user_id,'users','user_id','fullname');
                if($status=='Accepted'){
                    $this->Common_model->addEditRecords('orders',array('sp_order_cancel_date'=>date('Y-m-d',strtotime($sp_order_cancel_date)),'order_status'=>'Confirmed','modified'=>date('Y-m-d H:i:s')), array('id'=>$order_record['id']));
                    $subscription_data = $this->Common_model->getRecords('user_subscription_history','id,total_remaining_order,plan_orders',array('user_id'=>$user_id),'id desc',true);
                    if($subscription_data['plan_orders']!='Unlimited'){
                       $substract= $subscription_data['total_remaining_order']-1;
                       $this->Common_model->addEditRecords('user_subscription_history',array('total_remaining_order'=>$substract,'modified'=>date('Y-m-d H:i:s')), array('id'=>$subscription_data['id']));
                    }
                    spFriends($event_record['user_id'],$event_record['sp_id']);
                    $title=ucwords($company_name).' accepted your order request.';
                    $content=ucwords($company_name).' accepted your order request '.$order_record['order_number'].' for '.$event_record['event_title'].'.';
                    $notdata['type']='order_request_accepted';
                    $this->Common_model->push_notification_send($event_record['user_id'],$notdata,$order_record['id'],$title,$content,'',$user_id);
                }else{
                    $this->Front_common_model->cancel_by_sp_payment($user_id,$order_record['id']);
                    $title=ucwords($company_name).' rejected your order request.';
                    $content=ucwords($company_name).' rejected your order request'.$order_record['order_number'].' for '.$event_record['event_title'].' Your Payment will be back in your account soon.';
                    $notdata['type']='order_request_rejected';
                    $this->Common_model->push_notification_send($event_record['user_id'],$notdata,$order_record['id'],$title,$content,'',$user_id);
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    display_output('0','Something went wrong. Please try again.'); 
                } else {
                    $frontend_username = getFrontUsername($user_id);
                    $log_msg = getUserType()." ".$frontend_username." has ".lcfirst($status)." order of an event '".$event_record['event_title']."'.";
                    actionLog('events',$event_id,'Event '.lcfirst($status).'',$log_msg,'User',$user_id);
                    $this->db->trans_commit();
                    display_output('1','Order request '.lcfirst($status).'.');
                }
            }else{
                 display_output('0','Something went wrong. Please try again.');
            }

        }else{
             display_output('0','Something went wrong. Please try agains.');
        }
    }
    public function sp_canceled_booking(){
        if($this->session->userdata('front_user_type')=='1')
        {
            redirect('home'); 
        } 
        $this->Common_model->check_user_login();
        $doc_status=check_user_document_status();
        if($doc_status!='Accepted'){
            redirect('edit-profile');
        } 
        if(isset($_POST['order_id']) && !empty($_POST['order_id']) && isset($_POST['cancel_reason']) && !empty($_POST['cancel_reason'])){
            $user_id= $this->session->userdata('user_id');
            $order_id  = base64_decode($this->input->post('order_id'));
            $reason  = $this->input->post('cancel_reason');
            $today_date=date('Y-m-d');
            $this->Booking_model->check_subscription_status();
            $check_order_record = $this->Common_model->getRecords('orders','event_id,order_status',array('id'=>$order_id),'',true);
            if($check_order_record['order_status']=='Canceled'){
                display_output('0','Order status already canceled.');
            }
            $check_event_record = $this->Common_model->getRecords('events',"if(events.is_completed=1,'Completed',if(events.is_canceled=1,'Canceled',if(events.event_date>'".$today_date."','Not started','On Going'))) as event_status,event_title",array('id'=>$check_order_record['event_id']),'',true);

            if($check_event_record['event_status']=='On Going'){
                display_output('0','Event is on going. You cannot be canceled it.');
            }
            if($check_event_record['event_status']=='Canceled'){
                display_output('0','Event already canceled.');
            }
            $result=$this->Front_common_model->cancel_by_sp_payment($user_id,$order_id,$reason);
            if($result==1){
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getUserType()." ".$frontend_username." has canceled order of an event '".$check_event_record['event_title']."'.";
                actionLog('events',$check_order_record['event_id'],'Event Canceled',$log_msg,'User',$user_id);
                display_output('1','Booking canceled successfully.');
            }else{
                display_output('0','Something went wrong. Please try again.');
            }
        }else{
            display_output('0','Something went wrong. Please try again.');
        }
       
    }
    public function sp_order_history() 
    {
        $this->Common_model->check_user_login();
        $doc_status=check_user_document_status();
        if($doc_status!='Accepted'){
            redirect('edit-profile');
        } 
        if($this->session->userdata('front_user_type')!='2')
        {
            redirect('home'); 
        } 
        $data['title']= 'Booking History' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Booking History'." | ".SITE_TITLE;
        $data['meta_desc']= 'Booking History'." | ".SITE_TITLE;
        $data['page_title'] = "Order Received";
        $order_id    = isset($_POST['orderid'])?$_POST['orderid']:'';
        $data['total_records'] = $this->Booking_model->sp_booking_history_list(0,0,$order_id);
        $this->load->view('front/include/header',$data);
        $this->load->view('front/sp/order_history_sp');
        $this->load->view('front/include/footer');
    }
    public function sp_order_history_data(){
        $this->Common_model->check_user_login();
        if($this->session->userdata('front_user_type')!='2')
        {
            redirect('home'); 
        } 
        if(isset($_POST) && !empty($_POST)) {
            $page_number    = trim($_POST['page']);
            $item_per_page = FRONT_LIMIT;
            $offset = (($page_number) * $item_per_page);
            $order_id    = isset($_POST['orderid'])?$_POST['orderid']:'';
            $data['bookings'] = $this->Booking_model->sp_booking_history_list($offset,$item_per_page,$order_id);
            $data['total']=count($data['bookings']);
            echo $all_orders = $this->load->view('front/sp/sp_order_history_data',$data,true); exit;
        }
    }
    public function get_order_items($order_id) 
    {
        //$this->Common_model->check_user_login();
        $order_id=base64_decode($order_id);
        $data['details']= $this->Common_model->getRecords('order_details','item_name,price,qty,total_qty,unit,customer_qty,total_price',array('order_id'=>$order_id,'type'=>'Actual','is_deleted'=>0),'',false);
        $data['order_total']=$this->Common_model->getRecords('order_total','code,title,value,discount_value',array('order_id'=>$order_id,'status'=>'Active'),'',false);
        return $this->load->view('front/include/item_modal',$data);
    }
    //////////////////////////////////////cele & cele sp///////////////////////////
    public function my_events() 
    {
        $user_type=$this->session->userdata('front_user_type');
        if($user_type=='2')
        {
            redirect('my-store'); 
        } 
        $data['title']= 'My Events' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'My Events'." | ".SITE_TITLE;
        $data['meta_desc']= 'My Events'." | ".SITE_TITLE;
        $data['page_title'] = "My Events";
        $data['page_sub_title'] = "Find your all event history";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'My Events',
            'link' => ''
        );
        
        $this->Common_model->check_user_login();
        $is_event='';
        if($user_type==3){
            $is_event=1;
        }
        $data['total_records'] = $this->Booking_model->my_bookings(0,0,'','',$is_event);
       // echo $this->db->last_query();exit;
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/celebrant/my_events_list');
        $this->load->view('front/include/footer');
    }
    public function my_events_data(){
        $this->Common_model->check_user_login();
        if(isset($_POST) && !empty($_POST)) {
            $user_type=$this->session->userdata('front_user_type');
            if($user_type=='2')
            {
                redirect('my-store'); 
            }
            $is_event='';
            if($user_type==3){
                $is_event=1;
            } 
            $page_number    = trim($_POST['page']);
            $item_per_page = FRONT_LIMIT;
            $offset = (($page_number) * $item_per_page);
            $name=isset($_POST['name'])?$_POST['name']:'';
            $status=isset($_POST['event_status'])?$_POST['event_status']:'';
            $data['detail_action']='my-event-details';
            $data['bookings'] = $this->Booking_model->my_bookings($offset,$item_per_page,$name,$status,$is_event);
            echo $all_services = $this->load->view('front/celebrant/my_event_data',$data,true); exit;
        }
    }
    public function my_event_filter(){
        $this->Common_model->check_user_login();
       if(isset($_POST) && !empty($_POST)) {
            $user_type=$this->session->userdata('front_user_type');
            if($user_type=='2')
            {
                redirect('my-store'); 
            }
            $is_event='';
            if($user_type==3){
                $is_event=1;
            } 
            $name=isset($_POST['name'])?$_POST['name']:'';
            $status=isset($_POST['event_status'])?$_POST['event_status']:'';
            $data['total_records'] = $this->Booking_model->my_bookings(0,0,$name,$status,$is_event);
            echo $all_services =$this->load->view('front/celebrant/my_events_list',$data,true);exit;
        }
    }
    public function my_event_details($id) 
    {
        $event_id=base64_decode($id);
        $data['title']= 'Event Details' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Event Details'." | ".SITE_TITLE;
        $data['meta_desc']= 'Event Details'." | ".SITE_TITLE;
        $data['page_title'] = 'Event Details';
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'My Events',
            'link' => 'my-events'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Details',
            'link' => ''
        );
        $this->Common_model->check_user_login();
        $user_id= $this->session->userdata('user_id');
        if(!$data['booking_details']=$this->Booking_model->booking_details($event_id,$user_id,'event')){
            redirect('pages/page_not_found');
        }
        if($data['booking_details']['user_id']!=$user_id){
            redirect('pages/page_not_found');
        }
        $data['page_type']='public';
        
        $data['page_sub_title'] = ucfirst($data['booking_details']['event_title']);
        $data['be_my_date']= $this->Home_model->be_my_date_list(3,3,$user_id,'limit');
        $order_id = $this->Common_model->getFieldValue('orders','id',array('event_id'=>$event_id),'',true);
        $data['order_record']=$this->Booking_model->booking_detail_order($order_id);
        //echo '<pre>';print_r($data['order_record']);exit;
        $data['pending']=0;
        $data['accepted']=0;
        $data['rejected']=0;
        $data['my_request_status']=$this->Common_model->getRecords('event_attendee','status',array('event_id'=>$event_id,'sender_id'=>$user_id),'',true);
        $data['total_records'] = $this->Booking_model->event_request_received_list(0,0,$event_id,$user_id);
        $data['is_provide_rating']=0;
        if($this->Common_model->getRecords('reviews','id',array('event_id'=>$event_id),'',true)){
            $data['is_provide_rating']=1; 
        }
        if($request_status = $this->Common_model->getRecords('event_attendee','count("status") as count,status',array('event_id'=>$event_id),'',false,'status')){
           foreach($request_status as $row){
                if($row['status']=='Accepted'){
                    $data['accepted']=$row['count'];
                    if($row['count']>3){
                        $data['total_attendee']=$row['count']-3;
                    }else{
                        $data['total_attendee']=0;
                    }
                }if($row['status']=='Rejected'){
                    $data['rejected']=$row['count'];
                }
                if($row['status']=='Pending'){
                    $data['pending']=$row['count'];
                }
           }
        }
        $data['form_action']='add-rating-review';
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/celebrant/my_event_details');
        $this->load->view('front/include/footer');
    }
    public function celebrant_completed_booking(){
        $user_id= $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        $event_id  = base64_decode($this->input->post('event_id'));
        $order_id  = base64_decode($this->input->post('order_id'));
        $today_date = date('Y-m-d H:i:s');
        if(empty($user_id)) {
            display_output('0','Please enter user id.');
        }
        if(empty($event_id)) {
            display_output('0','Please enter event id.');
        }
        if(empty($order_id)) {
            display_output('0','Please enter order id.');
        }
        $event_record=$this->Common_model->getRecords('events',"event_title,event_number,user_id,sp_id,if(events.is_completed=1,'Completed',if(events.is_canceled=1,'Canceled',if(events.event_date >'".$today_date."','Not started','On Going'))) as event_status",array('id'=>$event_id),'id DESC',true);
        if($event_record['event_status']!='On Going'){
            display_output('0','Event status is '.$event_record['event_status'].'. You are not authorize to complete this event.');
        }
        if($event_record['user_id']!=$user_id){
             display_output('0','You are not authorize to complete this event.');
        }
        $result=$this->Front_common_model->complete_payment($order_id);
        if($result==1){
            $title='#'.$event_record['event_number'].' '.ucwords($event_record['event_title']).' has been mark as completed.';
            $content='#'.$event_record['event_number'].' '.ucwords($event_record['event_title']).' has been mark as completed. Your payment will be credited in your account soon.';
            $notdata['type']='event_completed';
            $this->Common_model->push_notification_send($event_record['sp_id'],$notdata,$event_id,$title,$content,'',$user_id);
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getUserType()." ".$frontend_username." has marked completed an event '".$event_record['event_title']."'.";
            actionLog('events',$event_id,'Event Completed',$log_msg,'User',$user_id);
            display_output('1','Event completed successfully.');
        }else{
            display_output('0','Something went wrong. Please try again.');
        }
    }
    public function add_rating_review(){
        $user_id= $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        $event_id= base64_decode($this->input->post('event_id'));
        $rating  = trim($this->input->post('rating'));
        $review  = trim($this->input->post('review'));
        if(empty($user_id)) {
            display_output('0','Please enter user id.');
        }
        if(empty($event_id)) {
            display_output('0','Please enter event id.');
        }
        if(empty($rating)) {
            display_output('0','Please enter rating.');
        }
       /* if(empty($review)) {
            display_output('0','Please enter review.');
        }*/
        if($rating>5){
            display_output('0','You try enter invalid rating.');
        }
        if(!$event_details=$this->Common_model->getRecords('events','id,sp_id,is_completed,event_title',array('id'=>$event_id,'is_deleted'=>0,'status'=>'Active','user_id'=>$user_id),'',true)){
            display_output('0','Event isn\'t available.');
        }
        if($event_details['is_completed']==0){
            display_output('0','Event is not completed yet.');
        }
        if($rating_record=$this->Common_model->getRecords('reviews','id',array('event_id'=>$event_id,'is_deleted'=>0,'status'=>'Active','sender_id'=>$user_id,'receiver_id'=>$event_details['sp_id']),'',true)){
            display_output('0','You have already submitted your ratings to this event.');
        }
        $add=array(
            'sender_id'=>$user_id,
            'receiver_id'=>$event_details['sp_id'],
            'event_id'=>$event_id,
            'rating'=>$rating,
            //'review'=>$review,
            'created'=> date("Y-m-d H:i:s")
        );

        if($last_id=$this->Common_model->addEditRecords('reviews',$add))
        {
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getUserType()." ".$frontend_username." added ".$rating." rating for event '".$event_details['event_title']."'.";
            actionLog('reviews',$last_id,'Add',$log_msg,'User',$user_id);
            display_output('1','Rating submitted successfully.');
        }else{
            display_output('0','Something went wrong. Please try again.');
        }
        
    }
    public function celebrant_canceled_event(){
        $this->Common_model->check_user_login();
        $user_id= $this->session->userdata('user_id');
        $event_id  = base64_decode($this->input->post('event_id'));
        $order_id  = base64_decode($this->input->post('order_id'));
        $today_date=date('Y-m-d');
        
        if(empty($order_id)) {
            display_output('0','Please enter order id.');
        }
        if(empty($event_id)) {
            display_output('0','Please enter event id.');
        }
       
        $check_event_record = $this->Common_model->getRecords('events',"if(events.is_completed=1,'Completed',if(events.is_canceled=1,'Canceled',if(events.event_date>'".$today_date."','Not started','On Going'))) as event_status,event_title",array('id'=>$event_id),'',true);
        if($check_event_record['event_status']=='On Going'){
            display_output('0','Event is on going. You cannot be canceled it.');
        }
        if($check_event_record['event_status']=='Completed'){
            display_output('0','Event already completed.');
        }
        if($check_event_record['event_status']=='Canceled'){
            display_output('0','Event already canceled.');
        }
        $check_order_record = $this->Common_model->getRecords('orders','order_status',array('id'=>$order_id),'',true);
        if($check_order_record['order_status']=='Canceled'){
            display_output('0','Order status already canceled.');
        }
        $check_order_status_record = $this->Common_model->getRecords('order_status_history','status',array('order_id'=>$order_id),'',true);
        if($check_order_status_record['status']=='Pending'){
            $result=$this->Front_common_model->cancel_by_sp_payment($user_id,$order_id);
        }else{
            $result=$this->Front_common_model->cancel_by_cele_payment($user_id,$order_id);
        }
        if($result==1){
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getUserType()." ".$frontend_username." has canceled his/her own event '".$check_event_record['event_title']."'.";
            actionLog('events',$event_id,'Event canceled',$log_msg,'User',$user_id);
            display_output('1','Event canceled successfully.');
        }else{
            display_output('0','Something went wrong. Please try again.');
        }
    }
    public function event_request_received(){
        $user_id= $this->session->userdata('user_id');
        if(isset($_POST) && !empty($_POST)) {
            $page_number    = trim($_POST['page']);
            $item_per_page = FRONT_LIMIT;
            $offset = (($page_number) * $item_per_page);
            $event_id=isset($_POST['event_id'])?$_POST['event_id']:'';
            $event_id=base64_decode($event_id);
            $data['request_received'] = $this->Booking_model->event_request_received_list($offset,$item_per_page,$event_id,$user_id);
            echo $all_requests = $this->load->view('front/celebrant/event_request_received',$data,true); exit;
        }
    }
    public function accept_reject_event_request(){
        $this->Common_model->check_user_login();
        $user_id= $this->session->userdata('user_id');
        $request_id  = base64_decode($this->input->post('request_id'));
        $status  = $this->input->post('status');
        if(empty($request_id)) {
            display_output('0','Please enter request id.');
        }if(empty($status)) {
            display_output('0','Please enter status.');
        }
        if($status!='Accepted' &&  $status!='Rejected'){
            display_output('0','Status either Accepted or Rejected.');
        }
        if(!$record=$this->Common_model->getRecords('event_attendee', 'id,status,event_id,sender_id,event_owner', array('id'=>$request_id), '', true)){
            display_output('0','Event request not available.');
        }
        $sent_by=$record['sender_id'];
        $sent_by_staging_id=getUserInfo($sent_by,'users','user_id','staging_id');
        if(!$event_record = $this->Common_model->getRecords('events','id,event_title,user_id,is_canceled,event_date,members',array('id'=>$record['event_id'],'status'=>'Active','is_deleted'=>0),'',true)){
            display_output('0','Event isn\'t available.');
        }
        if($record['status']=='Accepted' && $status=='Accepted'){
             display_output('0','Event request already accepted.');
        }
        if($record['status']=='Rejected' && $status=='Rejected'){
             display_output('0','Event request already rejected.');
        }
        if($status=='Accepted'){
            if($event_record['is_canceled']==1){
                display_output('0','Event has been canceled.');
            }
            $current_date=date('Y-m-d H:i:s');
            if($current_date > $event_record['event_date']){
                display_output('0','Event time has been passed.');
            }
            $accpeted_member=$this->Common_model->getFieldValue('event_attendee','count(id)',array('event_id'=>$record['event_id'],'status'=>'Accepted'),'',true);
            if($event_record['members'] <= $accpeted_member){
                display_output('0','Sorry required members are completed for this event.');
            } 
            $update_data=array('status'=>'Accepted','modified'=>date('Y-m-d H:i:s'));
            if($this->Common_model->addEditRecords('event_attendee',$update_data,array('id'=>$request_id))){
                $user_fullname=getUserInfo($user_id,'users','user_id','fullname');

                $title=ucwords($user_fullname).' accepted your event request for '.$event_record['event_title'].'.';
                $content=ucwords($user_fullname).' accepted your event request for '.$event_record['event_title'].'.';
                if($record['event_owner']=='Self'){
                    $notdata['type']='invite_event_request_accepted';
                }else{
                    $notdata['type']='request_event_request_accepted';
                }
            $this->Common_model->push_notification_send($sent_by,$notdata,$record['event_id'],$title,$content,'',$user_id);
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getUserType()." ".$frontend_username." has accepted event request of '".$sent_by_staging_id."' for event '".$event_record['event_title']."'.";
                actionLog('event_attendee',$request_id,'Event Request Accept',$log_msg,'User',$user_id);
                display_output('1','Event request accepted.');
            }else{
                display_output('0','Something went wrong. Please try again.');
            }
        }else{

            $update_data=array('status'=>'Rejected','modified'=>date('Y-m-d H:i:s'));
            if($this->Common_model->addEditRecords('event_attendee',$update_data,array('id'=>$request_id))){
                $user_fullname=getUserInfo($user_id,'users','user_id','fullname');

                $title=ucwords($user_fullname).' rejected your event request for '.$event_record['event_title'].'.';
                $content=ucwords($user_fullname).' rejected your event request for '.$event_record['event_title'].'.';
                if($record['event_owner']=='Self'){
                    $notdata['type']='invite_event_request_rejected';
                }else{
                    $notdata['type']='request_event_request_rejected';
                }
            $this->Common_model->push_notification_send($sent_by,$notdata,$record['event_id'],$title,$content,'',$user_id);
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getUserType()." ".$frontend_username." has rejeced event request of '".$sent_by_staging_id."' for event '".$event_record['event_title']."'.";
                actionLog('event_attendee',$request_id,'Event Request Reject',$log_msg,'User',$user_id); 

                display_output('1','Event request rejected.');
            }else{
                display_output('0','Something went wrong. Please try again.');
            }
            
        }
    }
    public function invite_friend_list($id) 
    {
        if($this->session->userdata('front_user_type')=='2')
        {
            redirect('home'); 
        } 
        $data['title']= 'Invite Friends' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Invite Friends'." | ".SITE_TITLE;
        $data['meta_desc']= 'Invite Friends'." | ".SITE_TITLE;
        $data['page_title'] = "Invite Friends";
        $data['page_sub_title'] = "Find your all friends";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'My Events',
            'link' => 'my-events'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Event Detail',
            'link' => 'my-event-details/'.$id
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Invite Friends',
            'link' => ''
        );
        $data['event_id']=$id;
        $this->Common_model->check_user_login();
        $data['total_records'] = $this->Booking_model->get_invite_friend_list(0,0,base64_decode($id));
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/celebrant/invite_friend_list');
        $this->load->view('front/include/footer');
    }
    public function invite_friend_list_data(){
        $this->Common_model->check_user_login();
        if(isset($_POST) && !empty($_POST)) {
            $page_number    = trim($_POST['page']);
            $id    = trim($_POST['event_id']);
            $item_per_page = FRONT_LIMIT;
            $offset = (($page_number) * $item_per_page);
            $data['friends'] = $this->Booking_model->get_invite_friend_list($offset,$item_per_page,base64_decode($id));
            //echo '<pre>';print_r($data['friends']);
            echo $all_services = $this->load->view('front/celebrant/invite_friend_list_data',$data,true); exit;
        }
    }

    ///////////////////csp my booking section///////////////////////////
    public function csp_my_booking() 
    {
        
        $this->Common_model->check_user_login();
        if($this->session->userdata('front_user_type')!='3')
        {
            redirect('home'); 
        }
        $doc_status=check_user_document_status();
        if($doc_status!='Accepted'){
            redirect('edit-profile');
        }  
        $data['title']= 'My Bookings' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'My Bookings'." | ".SITE_TITLE;
        $data['meta_desc']= 'My Bookings'." | ".SITE_TITLE;
        $data['page_title'] = "My Bookings";
        $data['page_sub_title'] = "Find your all event history";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'My Bookings',
            'link' => ''
        );
        
       
        $data['total_records'] = $this->Booking_model->my_bookings(0,0);

        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/celebrant/csp_booking_list');
        $this->load->view('front/include/footer');
    }
    public function csp_my_booking_data(){
        $this->Common_model->check_user_login();
        if(isset($_POST) && !empty($_POST)) {
            $page_number    = trim($_POST['page']);
            $item_per_page = FRONT_LIMIT;
            $offset = (($page_number) * $item_per_page);
            $name=isset($_POST['name'])?$_POST['name']:'';
            $status=isset($_POST['event_status'])?$_POST['event_status']:'';
            $data['bookings'] = $this->Booking_model->my_bookings($offset,$item_per_page,$name,$status);
            $data['detail_action']='csp-booking-details';
            echo $all_services = $this->load->view('front/celebrant/my_event_data',$data,true); exit;
        }
    }
    public function csp_my_booking_filter(){
        $this->Common_model->check_user_login();
       if(isset($_POST) && !empty($_POST)) {
            $name=isset($_POST['name'])?$_POST['name']:'';
            $status=isset($_POST['event_status'])?$_POST['event_status']:'';
            $data['total_records'] = $this->Booking_model->my_bookings(0,0,$name,$status);
            echo $all_services =$this->load->view('front/celebrant/csp_booking_list',$data,true);exit;
        }
    }
    public function order_history() 
    {
        
        $data['title']= 'Booking History' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Booking History'." | ".SITE_TITLE;
        $data['meta_desc']= 'Booking History'." | ".SITE_TITLE;
        $data['page_title'] = "Order Received";
        $this->Common_model->check_user_login();
        if($this->session->userdata('front_user_type')!='3')
        {
            redirect('home'); 
        } 
        $doc_status=check_user_document_status();
        if($doc_status!='Accepted'){
            redirect('edit-profile');
        }  
        $order_id    = isset($_POST['orderid'])?$_POST['orderid']:'';
        $data['total_records'] = $this->Booking_model->sp_booking_history_list(0,0,$order_id);
        $this->load->view('front/include/header',$data);
        $this->load->view('front/celebrant/order_received_list');
        $this->load->view('front/include/footer');
    }
    public function order_history_data(){
        if($this->session->userdata('front_user_type')!='3')
        {
            redirect('home'); 
        } 
        if(isset($_POST) && !empty($_POST)) {
            $page_number    = trim($_POST['page']);
            $item_per_page = FRONT_LIMIT;
            $offset = (($page_number) * $item_per_page);
            $order_id    = isset($_POST['orderid'])?$_POST['orderid']:'';
            $data['bookings'] = $this->Booking_model->sp_booking_history_list($offset,$item_per_page,$order_id);

            $data['total']=count($data['bookings']);
            echo $all_orders = $this->load->view('front/sp/sp_order_history_data',$data,true); exit;
        }
    }
    public function order_placed_history() 
    {
        if($this->session->userdata('front_user_type')=='2')
        {
            redirect('home'); 
        } 
        $data['title']= 'Booking History' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Booking History'." | ".SITE_TITLE;
        $data['meta_desc']= 'Booking History'." | ".SITE_TITLE;
        $data['page_title'] = "Order Received";
        $this->Common_model->check_user_login();
        $order_id    = isset($_POST['orderid'])?$_POST['orderid']:'';
        $data['total_records'] = $this->Booking_model->celebrant_order_history_list(0,0,$order_id);
        $this->load->view('front/include/header',$data);
        $this->load->view('front/celebrant/order_placed_list');
        $this->load->view('front/include/footer');
    }
    public function order_placed_history_data(){
        $this->Common_model->check_user_login();
        if($this->session->userdata('front_user_type')=='2')
        {
            redirect('home'); 
        } 
        if(isset($_POST) && !empty($_POST)) {
            $page_number    = trim($_POST['page']);
            $item_per_page = FRONT_LIMIT;
            $offset = (($page_number) * $item_per_page);
            $order_id    = isset($_POST['orderid'])?$_POST['orderid']:'';
            $data['bookings'] = $this->Booking_model->celebrant_order_history_list($offset,$item_per_page,$order_id);
           // echo $this->db->last_query();
            $data['total']=count($data['bookings']);
            echo $all_orders = $this->load->view('front/celebrant/order_placed_data',$data,true); exit;
        }
    }
    public function get_cancel_order_details($order_id) 
    {
        //$this->Common_model->check_user_login();
        $order_id=base64_decode($order_id);
        $data['details']= $this->Common_model->getRecords('payment_history','transaction_id,paid_amount,payment_type,user_will_get,paypal_fee_amount,payment_status',array('order_id'=>$order_id,'payment_type'=>'return','payment_type'=>'return','transfer_type'=>'Admin_To_Customer'),'',true);
        //echo '<pre>';print_r($data['details']);
        //echo $this->db->last_query();
        return $this->load->view('front/celebrant/cancel_order_details',$data);
    }
}
