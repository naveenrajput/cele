<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mystore extends CI_Controller {

	public function __construct() {
		parent::__construct();
        $this->load->library('paypal_lib');
		$this->load->model(array('admin/Common_model','front/Store_model','front/Home_model','Front_common_model'));
		$this->load->helper('common_helper');
        if(!empty($this->session->userdata('front_user_type'))){
            
    		$user_type = $this->session->userdata('front_user_type'); 

            if($user_type=='1')
            {
                redirect('home'); 
            } 
            $doc_status=check_user_document_status();
            if($doc_status!='Accepted'){
                redirect('edit-profile');
            }
        }
	}
	
	public function manage_service_list() 
	{
        $data['title']= 'My Store' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'My Store'." | ".SITE_TITLE;
        $data['meta_desc']= 'My Store'." | ".SITE_TITLE;
        $data['page_title'] = "My Store";
        $data['page_sub_title'] = "Find best service provider's here";
        $data['breadcrumbs'] = array();
        if($this->session->userdata('front_user_type')==3){
            $bd_title='Home';
        }else{
            $bd_title='My Store';
        }

        $data['breadcrumbs'][] = array(
            'title' => $bd_title,
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Manage Service',
            'link' => ''
        );
       // echo '<pre>';print_r($_POST);exit;
       	$this->Common_model->check_user_login();
        $data['type']='service';
        $data['total_records'] = $this->Store_model->get_sp_service_list(0,0);
       // $data['offer_total_records'] = $this->Store_model->get_sp_offer_list(0,0);
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/sp/my_store');
        $this->load->view('front/include/footer');
	}
    public function manage_service_data(){
        $this->Common_model->check_user_login();
        if(isset($_POST) && !empty($_POST)) {
            $page_number    = trim($_POST['page']);
            $item_per_page = FRONT_LIMIT;
            $offset = (($page_number) * $item_per_page);
            $data['services'] = $this->Store_model->get_sp_service_list($offset,$item_per_page);
           // echo '<pre>';print_r($data);exit;
            echo $all_services = $this->load->view('front/sp/service_data',$data,true); exit;
        }
    }
    
    public function add_service() 
    {
        $data['title']= 'Add Service' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Add Service'." | ".SITE_TITLE;
        $data['meta_desc']= 'Add Service'." | ".SITE_TITLE;
        $data['page_title'] = "Add Service";
        $data['page_sub_title'] = "Find best service provider's here";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'My Store',
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Manage Service',
            'link' => 'manage-service'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Add Service',
            'link' => ''
        );
        $this->Common_model->check_user_login();
        $data['category_list']=$this->Common_model->getRecords('service_category', 'id,title', array('status'=>'Active','is_deleted'=>0), 'title', false);
        $data['measurement_unit']=$this->Common_model->getRecords('measurement_unit', 'id,title', array('status'=>'Active','is_deleted'=>0), 'title', false);
        if($this->input->post()) { //echo '<pre>';print_r($_POST);exit;
          
            $this->form_validation->set_rules('service_title', 'service title', 'trim|required',array('required'=>'Please enter %s'));
            $this->form_validation->set_rules('category_id', 'category id', 'trim|required',array('required'=>'Please select %s'));
            $this->form_validation->set_rules('description', 'description', 'trim|required',array('required'=>'Please enter %s'));
            
            if($this->form_validation->run()==TRUE) 
            {
               
                if(!isset($_POST['item_name']) || empty($_POST['item_name']) &&!isset($_POST['price']) || empty($_POST['price']) && !isset($_POST['qty']) || empty($_POST['qty']) && !isset($_POST['unit']) || empty($_POST['unit'])){
                     display_output('0','Please select valid items.');
                }
             
                $user_id= $this->session->userdata('user_id');
                $service_data=array(   
                    'user_id'=>$user_id,
                    'service_title'=>$this->input->post('service_title'),
                    'category_id'=>$this->input->post('category_id'),
                    'description'=>$this->input->post('description'),
                    'created'=>date('Y-m-d H:i:s')
                );
                $width="";
                $height="";
                if(isset($_FILES['img_1']['name']) && $_FILES['img_1']['name']!="" && !empty($_FILES['img_1']['name']))
                {
                    $file_ext_ary= explode(".",$_FILES['img_1']['name']);
                    $file_ext= end($file_ext_ary);
                    $filename = rand().time().'_'.'01'.'.'.$file_ext;
                    $file='img_1';
                    $image_path = SERVICE_BANNER_PATH;
                    $allowed_types = 'jpg|jpeg|png|JPG|JPEG|PNG';
                    $file='img_1';
                    if($_FILES['img_1']['error']==0) {
                        $responce_img_1 = multiImageUpload($image_path,$allowed_types,$file,$width,$height,$filename);
                        if($responce_img_1['status']==0){
                            display_output('0','Some error occured in upload of image 1.');
                        }else{
                            $service_data['img1']=$responce_img_1['image_path'];
                        }
                    }
                }else{
                    display_output('0','Please insert an image.');
                }
                if(isset($_FILES['img_2']['name']) && $_FILES['img_2']['name']!="" && !empty($_FILES['img_2']['name']))
                {
                    $file_ext_ary= explode(".",$_FILES['img_2']['name']);
                    $file_ext= end($file_ext_ary);
                    $filename = rand().time().'_'.'02'.'.'.$file_ext;
                    $file='img_2';
                    $image_path = SERVICE_BANNER_PATH;
                    $allowed_types = 'jpg|jpeg|png|JPG|JPEG|PNG';
                    $file='img_2';
                  
                    if($_FILES['img_2']['error']==0) {
                        $responce_img_2 = multiImageUpload($image_path,$allowed_types,$file,$width,$height,$filename);
                        if($responce_img_2['status']==0){
                             display_output('0','Some error occured in upload of image 2.');  
                        }
                        else{
                            $service_data['img2']=$responce_img_2['image_path'];
                        }
                    }
                }
                if(isset($_FILES['img_3']['name']) &&$_FILES['img_3']['name']!="" && !empty($_FILES['img_3']['name']))
                {
                    $file_ext_ary= explode(".",$_FILES['img_3']['name']);
                    $file_ext= end($file_ext_ary);
                    $filename = rand().time().'_'.'03'.'.'.$file_ext;
                    $file='img_3';
                    $image_path = SERVICE_BANNER_PATH;
                    $allowed_types = 'jpg|jpeg|png|JPG|JPEG|PNG';
                    $file='img_3';
                  
                    if($_FILES['img_3']['error']==0) {
                        $responce_img_3 = multiImageUpload($image_path,$allowed_types,$file,$width,$height,$filename);
                        if($responce_img_3['status']==0){
                            display_output('0','Some error occured in upload of image 3.');
                        }else{
                            $service_data['img3']=$responce_img_3['image_path'];
                        }
                    }
                }
                $this->db->trans_begin();
                if($last_id = $this->Common_model->addEditRecords('services',$service_data)){
                    $insert_array=array();
                    for($i=0;$i<count($_POST['item_name']);$i++){
                            $insert_array[]=array('item_name' =>$_POST['item_name'][$i],
                                                'price' =>$_POST['price'][$i],
                                                'unit' =>$_POST['unit'][$i],
                                                'qty' =>$_POST['qty'][$i],
                                                'service_id'=>$last_id,
                                                'created'=>date('Y-m-d H:i:s'));
                        }
                    $this->db->insert_batch('services_item', $insert_array);
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    display_output('0','Something went wrong. Please try again.'); 
                } else {
                    $user_id = $this->session->userdata('user_id');
                    $frontend_username = getFrontUsername($user_id);
                    $log_msg = getUserType()." ".$frontend_username." has created a new service '".$this->input->post('service_title')."'.";
                    actionLog('services',$last_id,'add',$log_msg,'User',$user_id); 
                    $this->db->trans_commit();
                    display_output('1','Service created successfully.');
                }
                
            }else{
                display_output('0','Please fill all required fields.');
            }
        }
        $data['form_action']=site_url('add-service');
        if ($this->input->is_ajax_request()) {
           return $this->load->view('front/sp/add_service',$data);
        }else{
            $this->load->view('front/include/header',$data);
            $this->load->view('front/include/breadcrumb');
            $this->load->view('front/sp/add_service');
            $this->load->view('front/include/footer');
        }
    }
    public function edit_service($id) 
    {
        //$id=$this->uri->segment(2);
        $service_id=base64_decode($id);
        $data['title']= 'Edit Service' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Edit Service'." | ".SITE_TITLE;
        $data['meta_desc']= 'Edit Service'." | ".SITE_TITLE;
        $data['page_title'] = "Edit Service";
        $data['page_sub_title'] = "Find best service provider's here";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'My Store',
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Manage Service',
            'link' => 'manage-service'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Edit Service',
            'link' => ''
        );
        $this->Common_model->check_user_login();
        if(!$data['service']=$this->Store_model->sp_service_details($service_id)) {
            redirect('pages/page_not_found');
        }
        $data['items']=$this->Store_model->service_item_details($service_id);
        $data['category_list']=$this->Common_model->getRecords('service_category', 'id,title', array('status'=>'Active','is_deleted'=>0), 'title', false);
        $data['measurement_unit']=$this->Common_model->getRecords('measurement_unit', 'id,title', array('status'=>'Active','is_deleted'=>0), 'title', false);
        if($this->input->post()) { //echo '<pre>';print_r($_FILES);exit;
          
            $this->form_validation->set_rules('service_title', 'service title', 'trim|required',array('required'=>'Please enter %s'));
            $this->form_validation->set_rules('category_id', 'category id', 'trim|required',array('required'=>'Please select %s'));
            $this->form_validation->set_rules('description', 'description', 'trim|required',array('required'=>'Please enter %s'));
            
            if($this->form_validation->run()==TRUE) 
            {
                if(!isset($_POST['item_name']) || empty($_POST['item_name']) &&!isset($_POST['price']) || empty($_POST['price']) && !isset($_POST['qty']) || empty($_POST['qty']) && !isset($_POST['unit']) || empty($_POST['unit'])){
                    if(empty($_POST['form_id'])){
                        display_output('0','Please select valid items.');
                    }
                }
             
                $user_id= $this->session->userdata('user_id');
                $service_data=array(   
                    'user_id'=>$user_id,
                    'service_title'=>$this->input->post('service_title'),
                    'category_id'=>$this->input->post('category_id'),
                    'description'=>$this->input->post('description'),
                    'created'=>date('Y-m-d H:i:s')
                );
                $width="";
                $height="";
                if(isset($_FILES['img_1']['name']) && $_FILES['img_1']['name']!="" && !empty($_FILES['img_1']['name']))
                {
                    $file_ext_ary= explode(".",$_FILES['img_1']['name']);
                    $file_ext= end($file_ext_ary);
                    $filename = rand().time().'_'.'01'.'.'.$file_ext;
                    $file='img_1';
                    $image_path = SERVICE_BANNER_PATH;
                    $allowed_types = 'jpg|jpeg|png|JPG|JPEG|PNG';
                    $file='img_1';
                    if($_FILES['img_1']['error']==0) {
                        $responce_img_1 = multiImageUpload($image_path,$allowed_types,$file,$width,$height,$filename);
                        if($responce_img_1['status']==0){
                            display_output('0','Some error occured in upload of image 1.');
                        }else{
                            $service_data['img1']=$responce_img_1['image_path'];
                        }
                    }
                }/*else{
                    display_output('0','Please insert an image.');
                }*/
                if(isset($_FILES['img_2']['name']) && $_FILES['img_2']['name']!="" && !empty($_FILES['img_2']['name']))
                {
                    $file_ext_ary= explode(".",$_FILES['img_2']['name']);
                    $file_ext= end($file_ext_ary);
                    $filename = rand().time().'_'.'02'.'.'.$file_ext;
                    $file='img_2';
                    $image_path = SERVICE_BANNER_PATH;
                    $allowed_types = 'jpg|jpeg|png|JPG|JPEG|PNG';
                    $file='img_2';
                  
                    if($_FILES['img_2']['error']==0) {
                        $responce_img_2 = multiImageUpload($image_path,$allowed_types,$file,$width,$height,$filename);
                        if($responce_img_2['status']==0){
                             display_output('0','Some error occured in upload of image 2.');  
                        }
                        else{
                            $service_data['img2']=$responce_img_2['image_path'];
                        }
                    }
                }
                if(isset($_FILES['img_3']['name']) &&$_FILES['img_3']['name']!="" && !empty($_FILES['img_3']['name']))
                {
                    $file_ext_ary= explode(".",$_FILES['img_3']['name']);
                    $file_ext= end($file_ext_ary);
                    $filename = rand().time().'_'.'03'.'.'.$file_ext;
                    $file='img_3';
                    $image_path = SERVICE_BANNER_PATH;
                    $allowed_types = 'jpg|jpeg|png|JPG|JPEG|PNG';
                    $file='img_3';
                  
                    if($_FILES['img_3']['error']==0) {
                        $responce_img_3 = multiImageUpload($image_path,$allowed_types,$file,$width,$height,$filename);
                        if($responce_img_3['status']==0){
                            display_output('0','Some error occured in upload of image 3.');
                        }else{
                            $service_data['img3']=$responce_img_3['image_path'];
                        }
                    }
                }
                $this->db->trans_begin();
                if($this->Common_model->addEditRecords('services',$service_data,array('id'=>$service_id))){
                    $result = array_diff($_POST['old_form_id'],$_POST['form_id']);
                    if(!empty($result)){
                        foreach($result as $value){
                            $this->Common_model->addEditRecords('services_item',array('is_deleted'=>1,'modified'=>date('Y-m-d H:i:s')),array('id'=>base64_decode($value)));

                        }
                    }
                    if(isset($_POST['item_name']) && !empty($_POST['item_name'])){
                        $insert_array=array();
                        for($i=0;$i<count($_POST['item_name']);$i++){
                                $insert_array[]=array('item_name' =>$_POST['item_name'][$i],
                                                    'price' =>$_POST['price'][$i],
                                                    'unit' =>$_POST['unit'][$i],
                                                    'qty' =>$_POST['qty'][$i],
                                                    'service_id'=>$service_id,
                                                    'created'=>date('Y-m-d H:i:s'));
                        }
                        $this->db->insert_batch('services_item', $insert_array);
                    }
                }
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    display_output('0','Something went wrong. Please try again.'); 
                } else {
                    $user_id = $this->session->userdata('user_id');
                    $frontend_username = getFrontUsername($user_id);
                    $log_msg = getUserType()." ".$frontend_username." has updated a service '".$this->input->post('service_title')."'.";
                    actionLog('services',$service_id,'update',$log_msg,'User',$user_id);
                    $this->db->trans_commit();
                    display_output('1','Service updated successfully.');
                }
                
            }else{
                display_output('0','Please fill all required fields.');
            }
        }
        $data['form_action']=site_url('edit-service/'.$id);
        if ($this->input->is_ajax_request()) {
           return $this->load->view('front/sp/edit_service',$data);
        }else{
            $this->load->view('front/include/header',$data);
            $this->load->view('front/include/breadcrumb');
            $this->load->view('front/sp/edit_service');
            $this->load->view('front/include/footer');
        }
    }
	public function service_details($id) 
    {
        $service_id=base64_decode($id);
        $data['title']= 'Service Details' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Service Details'." | ".SITE_TITLE;
        $data['meta_desc']= 'Service Details'." | ".SITE_TITLE;
        $data['page_title'] = "Service Details";
        $data['page_sub_title'] = "Find best service provider's here";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'My Store',
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Manage Service',
            'link' => 'manage-service'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Service Details',
            'link' => ''
        );
        $this->Common_model->check_user_login();
        if(!$data['service']=$this->Store_model->sp_service_details($service_id)) {
            redirect('pages/page_not_found');
        }
        $data['items']=$this->Store_model->service_item_details($service_id);
        $data['my_bookings']=$this->Store_model->service_details_my_booking();
        //echo $this->db->last_query();
        //echo '<pre>';print_r($data['my_bookings']);exit;
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/sp/manage_service_details');
        $this->load->view('front/include/footer');
    }
    
	public function manage_offer_list() 
    {
        $data['title']= 'My Store' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'My Store'." | ".SITE_TITLE;
        $data['meta_desc']= 'My Store'." | ".SITE_TITLE;
        $data['page_title'] = "My Store";
        $data['page_sub_title'] = "Find best service provider's here";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'My Store',
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Manage offer',
            'link' => ''
        );
        $this->Common_model->check_user_login();
        $data['type']='offer';
        //$data['total_records'] = $this->Store_model->get_sp_service_list(0,0);
        $data['offer_total_records'] = $this->Store_model->get_sp_offer_list(0,0);
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/sp/manage_offer');
        $this->load->view('front/include/footer');
    }
    public function manage_offer_data(){
        $this->Common_model->check_user_login();
        if(isset($_POST) && !empty($_POST)) {
            $page_number    = trim($_POST['page']);
            $item_per_page = FRONT_LIMIT;
            $offset = (($page_number) * $item_per_page);
            $data['offers'] = $this->Store_model->get_sp_offer_list($offset,$item_per_page);
            echo $all_offers = $this->load->view('front/sp/offer_data',$data,true); exit;
        }
    }
    public function add_offer() 
    {
        $data['title']= 'Add Offer' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Add Offer'." | ".SITE_TITLE;
        $data['meta_desc']= 'Add Offer'." | ".SITE_TITLE;
        $data['page_title'] = "Add Offer";
        $data['page_sub_title'] = "Find best service provider's here";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'My Store',
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Manage Offer',
            'link' => 'manage-offers'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Add Offer',
            'link' => ''
        );
        $user_id= $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        $data['services']=$this->Common_model->getRecords('services', 'id,service_title', array('status'=>'Active','is_deleted'=>0,'user_id'=>$user_id), 'service_title', false);
        if($this->input->post()) { //echo '<pre>';print_r($_FILES);exit;
          
            $this->form_validation->set_rules('service_id', 'service id', 'trim|required',array('required'=>'Please enter %s'));
            $this->form_validation->set_rules('description', 'description', 'trim|required',array('required'=>'Please enter %s'));
            $this->form_validation->set_rules('start_date', 'start date', 'trim|required',array('required'=>'Please select %s'));
            $this->form_validation->set_rules('end_date', 'end date', 'trim|required',array('required'=>'Please select %s'));
            $this->form_validation->set_rules('discount', 'discount', 'trim|required',array('required'=>'Please select %s'));
            $timezone=$this->session->userdata('user_timezone');
            if($this->form_validation->run()==TRUE) 
            {
                $start_date=$this->input->post('start_date');
                $end_date=$this->input->post('end_date');
                $service_id=$this->input->post('service_id');
               /* $st_date=convertLocalTimezoneToGMT($start_date,$timezone,$showTime=true);
                $end_date=convertLocalTimezoneToGMT($end_date,$timezone,$showTime=true);*/
                $st_date=$start_date;
                $end_date=$end_date;
                $offer_st_date=date('Y-m-d',strtotime($st_date));
                $offer_end_date= date('Y-m-d',strtotime($end_date));
                if($this->Store_model->check_offer($service_id,$offer_st_date,$offer_end_date)){
                    //msg reflects on vview page also//
                    display_output('0','You already added offer for this time duration.');
                }
                if(!$service_records=$this->Common_model->getRecords('services','id',array('id'=>$service_id,'is_deleted'=>0,'status'=>'Active','user_id'=>$user_id),'',true)){
                    display_output('0','You are not authorize to create offer on this service.');
                }
                $offer_data=array(   
                    'user_id'=>$user_id,
                    'offer_name'=>$this->input->post('offer_name'),
                    'service_id'=>$service_id,
                    'description'=>$this->input->post('description'),
                    'start_date'=>$offer_st_date,
                    'end_date'=>$offer_end_date,
                    'discount'=>$this->input->post('discount'),
                    'created'=>date('Y-m-d H:i:s')
                );
                $width="";
                $height="";
                if(isset($_FILES['img_1']['name']) && $_FILES['img_1']['name']!="" && !empty($_FILES['img_1']['name']))
                {
                    $file_ext_ary= explode(".",$_FILES['img_1']['name']);
                    $file_ext= end($file_ext_ary);
                    $filename = rand().time().'_'.'01'.'.'.$file_ext;
                    $file='img_1';
                    $image_path = OFFER_BANNER_PATH;
                    $allowed_types = 'jpg|jpeg|png|JPG|JPEG|PNG';
                    $file='img_1';
                    if($_FILES['img_1']['error']==0) {
                        $responce_img_1 = multiImageUpload($image_path,$allowed_types,$file,$width,$height,$filename);
                        if($responce_img_1['status']==0){
                            display_output('0','Some error occured in upload of image 1.');
                        }else{
                            $offer_data['img1']=$responce_img_1['image_path'];
                        }
                    }
                }
                if($last_id=$this->Common_model->addEditRecords('offer',$offer_data)){
                    $user_id = $this->session->userdata('user_id');
                    $frontend_username = getFrontUsername($user_id);
                    $log_msg = getUserType()." ".$frontend_username." has created a new offer '".$this->input->post('offer_name')."'.";
                    actionLog('offer',$last_id,'add',$log_msg,'User',$user_id); 
                    display_output('1','Offer created successfully.');
                }else{
                    display_output('0','Something went wrong. Please try again.'); 
                }
            }else{
                display_output('0','Please fill all required fields.');
            }
        }
        $data['form_action']=site_url('add-offer');
        if ($this->input->is_ajax_request()) {
           return $this->load->view('front/sp/add_offer',$data);
        }else{
            $this->load->view('front/include/header',$data);
            $this->load->view('front/include/breadcrumb');
            $this->load->view('front/sp/add_offer');
            $this->load->view('front/include/footer');
        }
    }
    public function edit_offer($id) 
    {
        $offer_id=base64_decode($id);
        $data['title']= 'Edit Offer' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Edit Offer'." | ".SITE_TITLE;
        $data['meta_desc']= 'Edit Offer'." | ".SITE_TITLE;
        $data['page_title'] = "Edit Offer";
        $data['page_sub_title'] = "Find best service provider's here";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'My Store',
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Manage Offer',
            'link' => 'manage-offers'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Edit Offer',
            'link' => ''
        );
        $user_id= $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        if(!$data['offer']=$this->Store_model->sp_offer_details($offer_id)) {
            redirect('pages/page_not_found');
        }
        $data['services']=$this->Common_model->getRecords('services', 'id,service_title', array('status'=>'Active','is_deleted'=>0,'user_id'=>$user_id), 'service_title', false);
        if($this->input->post()) { //echo '<pre>';print_r($_FILES);exit;
          
            $this->form_validation->set_rules('service_id', 'service id', 'trim|required',array('required'=>'Please enter %s'));
            $this->form_validation->set_rules('description', 'description', 'trim|required',array('required'=>'Please enter %s'));
            $this->form_validation->set_rules('start_date', 'start date', 'trim|required',array('required'=>'Please select %s'));
            $this->form_validation->set_rules('end_date', 'end date', 'trim|required',array('required'=>'Please select %s'));
            $this->form_validation->set_rules('discount', 'discount', 'trim|required',array('required'=>'Please select %s'));
            $timezone=$this->session->userdata('user_timezone');
            if($this->form_validation->run()==TRUE) 
            {
                $start_date=$this->input->post('start_date');
                $end_date=$this->input->post('end_date');
                $service_id=$this->input->post('service_id');
                /*$st_date=convertLocalTimezoneToGMT($start_date,$timezone,$showTime=true);
                $end_date=convertLocalTimezoneToGMT($end_date,$timezone,$showTime=true);*/
                $st_date=$start_date;
                $end_date=$end_date;
                $offer_st_date=date('Y-m-d',strtotime($st_date));
                $offer_end_date= date('Y-m-d',strtotime($end_date));
                if($this->Store_model->check_offer($service_id,$offer_st_date,$offer_end_date,$offer_id)){
                    display_output('0','You already added offer for this time duration.');
                }
                if(!$service_records=$this->Common_model->getRecords('services','id',array('id'=>$service_id,'is_deleted'=>0,'status'=>'Active','user_id'=>$user_id),'',true)){
                    display_output('0','You are not authorize to create offer on this service.');
                }
                $offer_data=array(   
                    'user_id'=>$user_id,
                    'offer_name'=>$this->input->post('offer_name'),
                    'service_id'=>$service_id,
                    'description'=>$this->input->post('description'),
                    'start_date'=>$offer_st_date,
                    'end_date'=>$offer_end_date,
                    'discount'=>$this->input->post('discount'),
                    'created'=>date('Y-m-d H:i:s')
                );
                $width="";
                $height="";
                if(isset($_FILES['img_1']['name']) && $_FILES['img_1']['name']!="" && !empty($_FILES['img_1']['name']))
                {
                    $file_ext_ary= explode(".",$_FILES['img_1']['name']);
                    $file_ext= end($file_ext_ary);
                    $filename = rand().time().'_'.'01'.'.'.$file_ext;
                    $file='img_1';
                    $image_path = OFFER_BANNER_PATH;
                    $allowed_types = 'jpg|jpeg|png|JPG|JPEG|PNG';
                    $file='img_1';
                    if($_FILES['img_1']['error']==0) {
                        $responce_img_1 = multiImageUpload($image_path,$allowed_types,$file,$width,$height,$filename);
                        if($responce_img_1['status']==0){
                            display_output('0','Some error occured in upload of image 1.');
                        }else{
                            $offer_data['img1']=$responce_img_1['image_path'];
                        }
                    }
                }
                if($this->Common_model->addEditRecords('offer',$offer_data, array('id' =>$offer_id))){
                    $user_id = $this->session->userdata('user_id');
                    $frontend_username = getFrontUsername($user_id);
                    $log_msg = getUserType()." ".$frontend_username." has updated a offer '".$this->input->post('offer_name')."'.";
                    actionLog('offer',$offer_id,'update',$log_msg,'User',$user_id); 
                    display_output('1','Offer Updated successfully.');
                }else{
                    display_output('0','Something went wrong. Please try again.'); 
                }
            }else{
                display_output('0','Please fill all required fields.');
            }
        }
        $data['form_action']=site_url('edit-offer/'.$id);
        if ($this->input->is_ajax_request()) {
           return $this->load->view('front/sp/edit_offer',$data);
        }else{
            $this->load->view('front/include/header',$data);
            $this->load->view('front/include/breadcrumb');
            $this->load->view('front/sp/edit_offer');
            $this->load->view('front/include/footer');
        }
    }
    public function offer_details($id) 
    {
        $offer_id=base64_decode($id);
        $data['title']= 'Offer Details' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Offer Details'." | ".SITE_TITLE;
        $data['meta_desc']= 'Offer Details'." | ".SITE_TITLE;
        $data['page_title'] = "Offer Details";
        $data['page_sub_title'] = "Find best service provider's here";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'My Store',
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Manage Offer',
            'link' => 'manage-offers'
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Offer Details',
            'link' => ''
        );
        $this->Common_model->check_user_login();
        if(!$data['offer']=$this->Store_model->sp_offer_details($offer_id)) {
            redirect('pages/page_not_found');
        }
        $data['my_bookings']=$this->Store_model->service_details_my_booking();
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/sp/offer_details');
        $this->load->view('front/include/footer');
    }

    public function subscription() 
    {
        $data['title']= 'Subscription' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Subscription'." | ".SITE_TITLE;
        $data['meta_desc']= 'Subscription'." | ".SITE_TITLE;
        $data['page_title'] = "Subscription";
        $data['page_sub_title'] = "Subscribe best plans according your need";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'My Store',
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Subscription',
            'link' => ''
        );
        $this->Common_model->check_user_login();
        
        $data['plan']=$this->Common_model->getRecords('subscriptions','id as plan_id,paypal_plan_id,time_type,time_value,total_days,amount,title,description,orders,',array('is_deleted'=>0,'status'=>'Active'),'sort_order asc',false);
       // echo $this->db->last_query();
        $data['previous_plan']=$this->Common_model->getRecords('user_subscription_history','plan_id,start_date,end_date,start_time,end_time,total_remaining_order,(select title from subscriptions where id=user_subscription_history.plan_id) as title,is_current,subscriptionID',array('status'=>'Completed','user_id'=>$this->session->userdata('user_id')),'id desc',true);
        
        //echo '<pre>';print_r($data['plan']);exit;
        $this->load->view('front/include/header',$data);
        $this->load->view('front/include/breadcrumb');
        $this->load->view('front/sp/subscription');
        $this->load->view('front/include/footer');
    }
    function plan_subscription($id)
    {  
       $this->load->library('paypal_lib');
       $this->Common_model->check_user_login();
       $current=date('Y-m-d');
       $paln_id=base64_decode($id);
       $userID =base64_encode($this->session->userdata('user_id'));
       /*if($previous_plan=$this->Common_model->getRecords('user_subscription_history','plan_id,start_date,end_date,total_remaining_order',array('status'=>'Completed','user_id'=>$this->session->userdata('user_id')),'id desc',true)){
            if($paln_id==$previous_plan['plan_id']){
                if($previous_plan['end_date'] > $current && !empty($previous_plan['total_remaining_order'])){
                   //display_output('0','You cannot subscribe current plan.');
                   $this->session->set_flashdata('error', 'You cannot subscribe current plan.');
                    redirect('subscription');
                }
                
            }
        }*/
       
        if(!$plan=$this->Common_model->getRecords('subscriptions','id as plan_id,amount,title',array('is_deleted'=>0,'status'=>'Active','id'=>$paln_id),'',true)){
            $this->session->set_flashdata('error', 'Plan is expired now !');
            redirect('subscription');
        }
     
        $returnURL = base_url().'subscription-notify-payment';
        $cancelURL = base_url().'subscription';
        $notifyURL =base_url().'subscription-notify-payment';
        
        // Add fields to paypal form
        $this->paypal_lib->add_field('return', $returnURL);
        $this->paypal_lib->add_field('cancel_return', $cancelURL);
        $this->paypal_lib->add_field('notify_url', $notifyURL);
        $this->paypal_lib->add_field('item_name', $plan['title']);
        $this->paypal_lib->add_field('custom', $userID);
        $this->paypal_lib->add_field('item_number',$id);
        $this->paypal_lib->add_field('amount',  $plan['amount']);
        $this->paypal_lib->add_field('sub_type', 'subscription');
        
        // Render paypal form
        $this->paypal_lib->paypal_auto_form();
      
    }
    public function subscription_notify_payment(){ 
        $paypalInfo = $this->input->post();
        if(!empty($paypalInfo)){
            if(!empty($paypalInfo["txn_id"])){
                $ipnCheck = $this->paypal_lib->validate_ipn($paypalInfo);
                // Check whether the transaction is valid
                if($ipnCheck){
                    $plan_id=base64_decode($paypalInfo["item_number"]);
                    if(!$record=$this->Common_model->getRecords('subscriptions','id as plan_id,time_type,time_value,total_days,amount,title,description,orders',array('id'=>$plan_id),'',true)){
                        $this->session->set_flashdata('error', 'Plan isn\'t available.');
                        redirect('subscription');
                    }
                    $user_id=base64_decode($paypalInfo["custom"]);
                    $sp_user_details=getNameEmailAddress($user_id);
                    $start_date=date('Y-m-d');
                    $end_date= date('Y-m-d', strtotime($start_date. ' + '.$record['total_days'].' days'));
                    $current_time=date('H:i');
                    $insert_array=array(
                        'user_id'=>base64_decode($paypalInfo["custom"]),
                        'plan_id'=>$record['plan_id'],
                        'start_date'=>$start_date,
                        'start_time'=>$current_time,
                        'end_date'=>$end_date,
                        'end_time'=>$current_time,
                        'plan_orders'=>$record['orders'],
                        'time_type'=>$record['time_type'],
                        'time_value'=>$record['time_value'],
                        'total_days'=>$record['total_days'],
                        'amount'=>$record['amount'],
                        'title'=>$record['title'],
                        'description'=>$record['description'],
                        'transaction_id'=>$paypalInfo["txn_id"],
                        'transaction_fees'=>$paypalInfo["payment_fee"],
                        'total_used_order'=>0,
                        'status'=>$paypalInfo["payment_status"],
                        'created'=>date('Y-m-d H:i:s'),
                        );
                    $insert_array['previous_remaining_order']=0;
                    $insert_array['total_remaining_order']=$record['orders'];
                    if($record['orders']!='Unlimited'){
                        if($past_subscription=$this->Common_model->getRecords('user_subscription_history','*',array('user_id'=>$user_id),'id desc',true)){
                            $insert_array['previous_remaining_order']=$past_subscription['total_remaining_order'];
                            $insert_array['total_remaining_order']=$record['orders']+$past_subscription['total_remaining_order'];
                        }
                    }
                    if($last_id=$this->Common_model->addEditRecords('user_subscription_history',$insert_array)){
                        $from_email =getNotificationEmail();
                        $to_email =$sp_user_details['email'];
                        $subject =  WEBSITE_NAME.' : Subscription';
                        $data['name']= $sp_user_details['fullname'];
                        $data['message']= 'Congratulations! you are our subscribed member.You purchased '.$record['title'].' Plan in '.ADMIN_CURRENCY.$record['amount'].'.';
                        $body = $this->load->view('template/common', $data,TRUE);
                        $this->Common_model->sendEmail($to_email,$subject,$body,$from_email);
                        $frontend_username = getFrontUsername($user_id);
                        $log_msg = getUserType()." ".$frontend_username." has subscribe a new plan '".$record['title']."'.";
                        actionLog('user_subscription_history',$last_id,'add',$log_msg,'User',$user_id);
                        $this->session->set_flashdata('success', 'Plan subscribed successfully.');
                        redirect('subscription');

                    }else{
                        $this->session->set_flashdata('error', 'Some error occured in payment.');
                        redirect('subscription');
                    }
                                    
                }else{
                    $this->session->set_flashdata('error', 'Payment failed.');
                    redirect('subscription');
                }
            }else{
                $this->session->set_flashdata('error', 'Payment failed.');
                redirect('subscription');
            }

        }else{
            $this->session->set_flashdata('error', 'Some error occured in payment.');
            redirect('subscription');
        }
    }
    public function paypal_subscription_payment(){ 
        if($this->input->post()){
            $user_id=$this->session->userdata('user_id');
            $paypal_plan_id=$_POST['plan_id'];
            $order_id=$_POST['order_id'];
            $payment_id=$_POST['payment_id'];
            $billing_token=$_POST['billing_token'];
            $subscription_id=$_POST['subscription_id'];
            if(!$record=$this->Common_model->getRecords('subscriptions','id as plan_id,time_type,time_value,total_days,amount,title,description,orders',array('paypal_plan_id'=>$paypal_plan_id),'',true)){
                $this->session->set_flashdata('error', 'Plan isn\'t available.');
                redirect('subscription');
            }
            $sp_user_details=getNameEmailAddress($user_id);
            $start_date=date('Y-m-d');
            $end_date= date('Y-m-d', strtotime($start_date. ' + '.$record['total_days'].' days'));
            $current_time=date('H:i');
            $insert_array=array(
                'user_id'=>$user_id,
                'plan_id'=>$record['plan_id'],
                'start_date'=>$start_date,
                'start_time'=>$current_time,
                'end_date'=>$end_date,
                'end_time'=>$current_time,
                'plan_orders'=>$record['orders'],
                'time_type'=>$record['time_type'],
                'time_value'=>$record['time_value'],
                'total_days'=>$record['total_days'],
                'amount'=>$record['amount'],
                'title'=>$record['title'],
                'description'=>$record['description'],
                'transaction_id'=>"",
                'transaction_fees'=>"",
                'total_used_order'=>0,
                'status'=>'Completed',
                'paypal_plan_id'=>$paypal_plan_id,
                'orderID'=>$order_id,
                'billingToken'=>$billing_token,
                'subscriptionID'=>$subscription_id,
                'paymentID'=>$payment_id,
                'created'=>date('Y-m-d H:i:s'),
                );
            $insert_array['previous_remaining_order']=0;
            $insert_array['total_remaining_order']=$record['orders'];
            if($record['orders']!='Unlimited'){
                if($past_subscription=$this->Common_model->getRecords('user_subscription_history','*',array('user_id'=>$user_id),'id desc',true)){
                    $insert_array['previous_remaining_order']=$past_subscription['total_remaining_order'];
                    $insert_array['total_remaining_order']=$record['orders']+$past_subscription['total_remaining_order'];
                }
            }
            if($last_id=$this->Common_model->addEditRecords('user_subscription_history',$insert_array)){
                $from_email =getNotificationEmail();
                $to_email =$sp_user_details['email'];
                $subject =  WEBSITE_NAME.' : Subscription';
                $data['name']= $sp_user_details['fullname'];
                $data['message']= 'Congratulations! you are our subscribed member.You purchased '.$record['title'].' Plan in '.ADMIN_CURRENCY.$record['amount'].'.';
                $body = $this->load->view('template/common', $data,TRUE);
                $this->Common_model->sendEmail($to_email,$subject,$body,$from_email);
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getUserType()." ".$frontend_username." has subscribe a new plan '".$record['title']."'.";
                actionLog('user_subscription_history',$last_id,'add',$log_msg,'User',$user_id);
                display_output('1','Plan subscribed successfully.');

            }else{
                display_output('0','Some error occured in payment.');
            }
        }
    }
    public function cancel_subscription_plan($subscription_id){
        $subscription_id=base64_decode($subscription_id);
        $result=$this->cancel_subscription($subscription_id);
        if(empty($result)){
            $update_data=array(
            'is_current'=>0,
            'modified'=>date('Y-m-d H:i:s')
            );
            $this->Common_model->addEditRecords('user_subscription_history',$update_data,array('subscriptionID'=>$subscription_id));
            $this->session->set_flashdata('success', 'Subscription canceled successfully.');
            redirect('subscription');
        }else{
            $this->session->set_flashdata('error', 'Some error occured in canceled subscription.');
            redirect('subscription');
        }
        

    }
    public function cancel_subscription($subscription_id){
        
        $ch = curl_init();
       
        curl_setopt($ch, CURLOPT_URL, "https://api.sandbox.paypal.com/v1/oauth2/token");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_USERPWD, 'AYXHJUrvsNQIJC-ZqNvma69H9S7a9_2lCrFM3d0odWweGGmoOa-zPL38H_9LoA0mOLoTL3y5sAjgLtjV' . ":" . 'EG8YWp8pbQpaqSi9uD3Mk2l9upRvHwoc-1zWLSs3Z9e12DOkZSkzs89-Btei78FPuHOV4z25J6HfFESK');

        $headers = array();
        $headers[] = "Accept: application/json";
        $headers[] = "Accept-Language: en_US";
        $headers[] = "Content-Type: application/x-www-form-urlencoded";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $results = curl_exec($ch);
        $getresult = json_decode($results);
        $paypal_access_token=$getresult->access_token;
        $data='{
           "reason": "Customer canceled subscription"
         
        }';

        curl_setopt($ch, CURLOPT_URL, "https://api.sandbox.paypal.com/v1/billing/subscriptions/$subscription_id/cancel");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_POST, 1);

        $headers = array();
        $headers[] = "Accept: application/json";
        $headers[] = "Accept-Language: en_US";
        $headers_data[] = "Authorization: Bearer $paypal_access_token";
       // $headers[] = "Prefer: return=representation";
        $headers[] = "PayPal-Request-Id: PROD-XYAB12ABSB7868432";
     
        $headers[] = "Content-Type: application/json";
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $results1 = curl_exec($ch);
        return $getresult1 = json_decode($results1);
    }

}
