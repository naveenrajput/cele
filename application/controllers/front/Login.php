<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(array('admin/Common_model','Front_common_model','front/Home_model'));
		$this->load->helper('common_helper');
		error_reporting(0);

	}
	public function show404page(){
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'class'=>'breadcrumb-item',
            'title' => 'Home',
            'link' => base_url()
        );
        $this->load->view('front/include/header',$data);
        $this->load->view('errors/html/error_404');
        $this->load->view('front/include/footer');
    }
	
	public function index() 
	{ 
        $data['title']= 'Login' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Login'." | ".SITE_TITLE;
        $data['meta_desc']= 'Login'." | ".SITE_TITLE;

        $data['page_title'] = "Login";
        $user_id= $this->session->userdata('user_id');
        if(!empty($user_id))
        {
        	$user_type= $this->session->userdata('front_user_type');
        	if($user_type==2){
        		redirect('manage-service');
        	}else{
        		redirect('home');
        	}
            redirect(base_url());
        }
        $this->load->view('front/include/header',$data);
        $this->load->view('front/login');
        $this->load->view('front/include/footer');
	}
    public function login_ajax() 
    {
       if($this->input->post()) {
            $staging_id = trim($this->input->post('staging_id'));
            $mobile = trim($this->input->post('mobile'));
            $password = trim($this->input->post('password'));

	        if(empty($mobile) && empty($staging_id)) {
	            display_output('0','Please enter mobile or login ID.');
	        }
	        if(!empty($staging_id)){
	            if(empty($password)) {
	                display_output('0','Please enter your password.');
	            }
	        }
	        $password=base64_encode($password);
	        $login_by='';
	        if(!empty($staging_id)){
	        	$login_by='s_id';
	            $where="staging_id='".addslashes($staging_id)."'";
	        }else{
	        	$login_by='mobile';
	            $where="mobile='".addslashes($mobile)."'";
	        }
	            if($user_data = $this->Common_model->getRecords('users','user_id,fullname,staging_id,mobile,user_type,status,email,password,is_deleted,mobile_auth_token',$where,'user_id DESC',true)) {
	           
	            if($user_data['status']=='Inactive')
	            { 
	                display_output('0','Your account has been deactivated! Please contact administrator for more details.');
	            }
	            
	            if($user_data['is_deleted']==1)
	            { 
	                display_output('0','Your account has been deleted ! Please contact administrator for more details.');
	            }
	            if(!empty($staging_id)){
	                if($user_data['password']==$password){
	                    if($user_data['status']=='Unverified')
	                    { 
	                        display_output('2','Your account is not verified.');
	                    }
	                    if($user_data['is_deleted']==0 && $user_data['status']=='Active'){
	                        
	                        $login=array(   
	                            'user_id'=>$user_data['user_id'],
	                            'fullname'=>$user_data['fullname'],
	                            'staging_id'=>$user_data['staging_id'],
	                            'mobile'=>$user_data['mobile'],
	                            'email'=>$user_data['email'],
	                            'front_user_type'=>$user_data['user_type'],
	                            'status'=>$user_data['status'],
	                            );
	                        $this->session->set_userdata($login);
	                        $user_id = $this->session->userdata('user_id');
							$frontend_username = getFrontUsername($user_id);
			            	$log_msg = getUserType()." ".$frontend_username." logged in.";
			            	actionLog('users',$user_id,'Login',$log_msg,'User',$user_id);
	                        display_output('1','Logged in successfully.',array('details'=>$user_data['user_type'],'login_by'=>$login_by));
	                    }
	                    else
	                    {
	                        display_output('0','Your account has been deactivated!  Please contact administrator for details.');
	                    }
	                }else{
	                    display_output('0','Incorrect Password.');
	                }
	            }else{
	                if($user_data['is_deleted']==0){
	                    $otp = mt_rand(1000,9999);
	                    $otp ='1234';
	                    $formData = array('otp' =>$otp,'otp_expired'=>date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))+900)); 
	                    $this->Common_model->addEditRecords('users',$formData,array('user_id'=>$user_data['user_id']));  
	                    display_output('1','Verification OTP sent on your registered mobile number, Please check it.',array('details'=>$user_data['user_type'],'login_by'=>$login_by));
	                }
	                else
	                {
	                    display_output('0','Your account has been deactivated!  Please contact administrator for details.');
	                }
	               
	            }
	        }
        	else
	        {
	        	if($login_by=='s_id'){
	        		display_output('0','Login ID does not match.');
	        	}else{
	        		display_output('0','Mobile number does not match.');
	        	}
	            
	        }
        }
    }
    public function sign_up() 
	{
        $user_id= $this->session->userdata('user_id');
        if(!empty($user_id))
        {
        	$user_type= $this->session->userdata('front_user_type');
        	if($user_type==2){
        		redirect('manage-service');
        	}else{
        		redirect('home');
        	}
            redirect(base_url());
        }
        $data['title']= 'Sign Up' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Sign Up'." | ".SITE_TITLE;
        $data['meta_desc']= 'Sign Up'." | ".SITE_TITLE;
        $data['page_title'] = "Sign Up";

       	$data['countries'] = $this->Common_model->getRecords('countries','*',array('id'=>231));
       	if($this->input->post()) {
			$this->form_validation->set_rules('fullname', 'full name', 'trim|required|max_length[100]',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('staging_id', 'Login ID', 'trim|required|max_length[20]',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('mobile', 'mobile','trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('email', 'email','trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('password', 'password', 'trim|required|min_length[6]|max_length[15]',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('user_type', 'user type', 'trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('country', 'country', 'trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('state', 'state', 'trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('city', 'city', 'trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('address', 'address', 'trim|required',array('required'=>'Please enter %s'));
			$latitude=$this->input->post('latitude');
			$longitude=$this->input->post('longitude');
			if($latitude=='' && $longitude==''){
				display_output('0','Please enter valid address.'); 
			}
			$user_type=$this->input->post('user_type');
			if($user_type=='2' || $user_type=='3'){
				$this->form_validation->set_rules('company_name', 'company name', 'trim|required|max_length[100]',array('required'=>'Please enter %s'));
				$this->form_validation->set_rules('business_no', 'business_no', 'trim|max_length[50]',array('required'=>'Please enter %s'));
				$this->form_validation->set_rules('establishment_year', 'establishment year', 'trim|required',array('required'=>'Please enter %s'));
				$this->form_validation->set_rules('document_type', 'document type', 'trim|required',array('required'=>'Please enter %s'));
			}
			$this->form_validation->set_error_delimiters('<p class="inputerror">', '</p>');

			if($this->form_validation->run()==TRUE) 
			{
				front_check_server_unique('users', 'email',$this->input->post('email'),'','','Email');
				front_check_server_unique('users', 'staging_id',$this->input->post('staging_id'),'','','Login ID');
				front_check_server_unique('users', 'mobile',$this->input->post('mobile'),'','','Mobile');
				$password=base64_encode($this->input->post('password'));
				$otp = mt_rand(1000,9999);
		        $otp ='1234';
				$signup_data = array(
					'fullname'=> $this->input->post('fullname'),
					'staging_id'=> $this->input->post('staging_id'),
					'mobile'=> $this->input->post('mobile'),
					'email'=> $this->input->post('email'),
					'password'=> $password,
					'user_type'=> $this->input->post('user_type'),
					'status'=> 'Unverified',
					'otp' =>$otp,
		            'otp_expired'=>date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))+900),
					'created'=> date("Y-m-d H:i:s"),
				);
		 		if($user_type==2 || $user_type==3){
		           
		            $filepath="";
		            $filerror="";
		            $width="";$height="";
		            if(isset($_FILES['document_proof']) && !empty($_FILES['document_proof']['name'])){
		                if($_FILES['document_proof']['error']==0) {
		                    $image_path = USER_DOCUMNET_PATH;
		                    $allowed_types = '*';
		                    $file='document_proof';
		                    $height = 150;
		                    $width = 150;
		                    $responce = commonImageUpload($image_path,$allowed_types,$file,$width,$height);

		                    if($responce['status']==0){
		                        $upload_error = $responce['msg'];   
		                        $filerror="1";
		                    } else {
		                        $filepath=$responce['image_path'];
		                    }
		                }
		            }else{
		                display_output('0','Please upload document.');
		            }
		            if($filerror!=''){
		                display_output('0',strip_tags($upload_error));
		            }
		            if(!empty($filepath)){
		                $signup_data['document_proof']=$filepath;
		            }
		            $signup_data['company_name']=$this->input->post('company_name');
		            $signup_data['establishment_year']=$this->input->post('establishment_year');
		            $signup_data['business_no']=$this->input->post('business_no');
		            $signup_data['document_type']=$this->input->post('document_type');
		            $signup_data['document_status']='Unverified';
		        }
				$this->db->trans_begin();
		        if($last_id = $this->Common_model->addEditRecords('users',$signup_data)){
		            $where  = array('user_id' => $last_id);
		            $user_number=str_pad($last_id,8,0,STR_PAD_LEFT);
		            $this->Common_model->addEditRecords('users',array('user_number'=>$user_number),$where);
		            $address_data = array(
		                'user_id'=>$last_id, 
		                'country'=>$this->input->post('country'),
		                'state'=>$this->input->post('state'),
		                'city'=>$this->input->post('city'),
		                'latitude'=>$this->input->post('latitude'),
		                'longitude'=>$this->input->post('longitude'),
		                'address'=>$this->input->post('address'),
		                'status'=>'Primary',
		                'created'=> date("Y-m-d H:i:s"),
		            );
		            $this->Common_model->addEditRecords('user_address',$address_data);
		            if(APP_PUBLISH){
		                if($user_type==2 || $user_type==3){
		                    $this->Front_common_model->free_trial_subscription($last_id);
		                }
		            }
		            $this->db->trans_complete();
		            if ($this->db->trans_status() === FALSE){
		                $this->db->trans_rollback();
		                display_output('0','Some error occured! Please try again.'); 
		            } else {
		                $this->db->trans_commit();

		                $user_record=$this->Common_model->getRecords('users','user_id,mobile',$where,'',true);
		                if($user_type==2 || $user_type==3){
			            	
		                    $message='Registration completed & verification OTP sent on your registered mobile number, Please check it. We\'re reviewing your information. Your account should be verified shortly.' ;
		                }else{
		                	
		                     $message='Registration completed & verification OTP sent on your registered mobile number, Please check it.';
		                }
		                $log_msg = getUserType($user_type)." ".$this->input->post('staging_id')." registered as a new user.";
			            	actionLog('users',$last_id,'Sign Up',$log_msg,'User',$last_id);
		                display_output('1',$message,array('mobile'=>$user_record['mobile']));
		            }
		        }else{
		            display_output('0','Some error occured! Please try again.');  
		        }
			} else {
				display_output('0','Please fill all required fields.');   
			}
		}

       	$data['form_action']=site_url('sign-up');
        $this->load->view('front/include/header',$data);
        $this->load->view('front/sign_up');
        $this->load->view('front/include/footer');
	}
 	public function verify_otp(){
 		$user_id= $this->session->userdata('user_id');
        if(!empty($user_id))
        {
        	$user_type= $this->session->userdata('front_user_type');
        	if($user_type==2){
        		redirect('manage-service');
        	}else{
        		redirect('home');
        	}
            redirect(base_url());
        }
        $mobile  = trim($this->input->post('mobile'));
        $otp  = trim($this->input->post('otp'));
        $otp_type  = trim($this->input->post('otp_type'));
        if(empty($mobile)) {
            display_output('0','Please enter mobile number.');
        }
        if(empty($otp)) {
            display_output('0','Please enter otp.');
        }
       
        if($user_data = $this->Common_model->getRecords('users','user_id,fullname,staging_id,mobile,user_type,status,email,otp,otp_expired',array('mobile'=>$mobile,'otp'=>$otp,'is_deleted'=>0),'user_id DESC',true)){
            $current_date = new DateTime("now", new DateTimeZone('UTC'));
            if(strtotime($user_data['otp_expired']) < strtotime($current_date->format('Y-m-d H:i:s'))){
                display_output('0','Your OTP has been expired.'); 
            }
            if($user_data['otp']==$otp){
                $otp_data = array( 
                    'otp'=>'',
                    'otp_expired'=>'',
                    'status'=>'Active',
                    'modified'=>date('Y-m-d H:i:s')
                );
                if($this->Common_model->addEditRecords('users',$otp_data,array('mobile'=>$mobile))){
                	if($otp_type=='login'){
                   	  $login=array(   
	                        'user_id'=>$user_data['user_id'],
	                        'fullname'=>$user_data['fullname'],
	                        'staging_id'=>$user_data['staging_id'],
	                        'mobile'=>$user_data['mobile'],
	                        'email'=>$user_data['email'],
	                        'front_user_type'=>$user_data['user_type'],
	                        'status'=>$user_data['status'],
	                        );
	                    $this->session->set_userdata($login);
	                    $log_msg = getUserType($user_data['user_type'])." ".$user_data['staging_id']." logged in.";
	            		actionLog('users',$user_data['user_id'],'Login',$log_msg,'User',$user_data['user_id']);
                	}else{
                		$log_msg = getUserType($user_data['user_type'])." ".$user_data['staging_id']." verified account.";
	            		actionLog('users',$user_data['user_id'],'Verified account',$log_msg,'User',$user_data['user_id']);
                	}
                    display_output('1','OTP verified successfully.',array('details'=>$user_data['user_type'])); 
                }else{
                    display_output('0','Some error occured! Please try again.'); 
                }
            }else{
                display_output('0','Wrong OTP ! Please enter correct OTP.');   
            }

        }else{
            display_output('0','Invalid OTP.'); 
        }
    }
    public function resend_otp(){
    	$user_id= $this->session->userdata('user_id');
        if(!empty($user_id))
        {
        	$user_type= $this->session->userdata('front_user_type');
        	if($user_type==2){
        		redirect('manage-service');
        	}else{
        		redirect('home');
        	}
            redirect(base_url());
        }
        $mobile= trim($this->input->post('mobile'));
        if(empty($mobile)) {
            display_output('0','Please enter mobile no.');
        }
        if(!$user_data= $this->Common_model->getRecords('users','user_id,staging_id,user_type,status,is_deleted,mobile',array('mobile'=>$mobile),'user_id DESC',true)) { 
            display_output('0','Please enter correct mobile no.'); 
        }
        if($user_data['is_deleted']==1) {
            display_output('0','Your account has been deleted, Please contact us for more details.'); 
        } 
        if($user_data['status']=='Inactive') {
            display_output('0','Your account has been inactive, Please contact us for more details.');
        } 
        $otp = mt_rand(1000,9999);
        $otp ='1234';
        //$this->otp_send($mobile,$otp);
        $otp_data = array( 
            'otp'=>$otp,
            'otp_expired'=>date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))+900),
            'modified'=>date('Y-m-d H:i:s')
        );
        if($this->Common_model->addEditRecords('users',$otp_data,array('mobile'=>$mobile))){
        	$log_msg = getUserType($user_data['user_type'])." ".$user_data['staging_id']." requested to resend OTP";
            actionLog('users',$user_data['user_id'],'Resend OTP',$log_msg,'User',$user_data['user_id']);
            display_output('1','OTP sent to your registered mobile no.'); 
        }else{
            display_output('0','Some error occured! Please try again.'); 
        }

    } 
    public function forgot_password()
    {   
    	$user_id= $this->session->userdata('user_id');
        if(!empty($user_id))
        {
        	$user_type= $this->session->userdata('front_user_type');
        	if($user_type==2){
        		redirect('manage-service');
        	}else{
        		redirect('home');
        	}
            redirect(base_url());
        }
    	$data['title']= 'Forgot Password' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Forgot Password'." | ".SITE_TITLE;
        $data['meta_desc']= 'Forgot Password'." | ".SITE_TITLE;
        $data['page_title'] = "Forgot Password";
    	if($this->input->post()) {
    		$email=$this->input->post('email');
    		$this->form_validation->set_rules('email', 'email/Staging/ID', 'trim|required',array('required'=>'Please enter %s'));
	        if($this->form_validation->run()==TRUE) 
			{
		        if(!$user_data= $this->Common_model->getRecords('users','user_id,staging_id,user_type,fullname,status,is_deleted,email,mobile_auth_token',array('email'=>$email),'user_id DESC',true)) {  
		            if(!$user_data= $this->Common_model->getRecords('users','user_id,staging_id,fullname,status,is_deleted,email, mobile_auth_token',array('staging_id'=>$email),'user_id DESC',true)) {  
		                display_output('0','Please enter registered email/login ID.');
		            }
		        }
		        if($user_data) {
		            if($user_data['is_deleted']==1) {
		                display_output('0','Your account has been deleted, Please contact us for more details.');
		            } 
		            if($user_data['status']=='Inactive') {
		                display_output('0','Your account has been inactive, Please contact us for more details.');
		            } 
		            if($user_data['status']=='Unverified') {
		                display_output('2','Your account is not verified.');
		            } 
		            $token = md5(uniqid(rand(), true));
		            $reset_token_date = date("Y-m-d H:i:s");
		            $to_email = $user_data['email']; 
		            $from_email =  getNotificationEmail();  
		            $subject = WEBSITE_NAME." : Reset Password"; 
		            $data['reset_password_url'] = base_url().'reset-password?token='.$token;
		            $data['name'] = ucwords($user_data['fullname']); 
		            
		            $data['reset_token'] = $token;
		            $body = $this->load->view('template/forgot_password_admin', $data,TRUE);


		            if($this->Common_model->sendEmail($to_email,$subject,$body,$from_email)) 
		            {
		                $reset_token_date = date("Y-m-d H:i:s");
		                $where = array('user_id'=> $user_data['user_id']); 
		                $update_data = array(
		                    'reset_token'=>$token, 
		                    'reset_token_date'=>$reset_token_date
		                    );
		                $this->Common_model->addEditRecords('users', $update_data, $where);
		                display_output('1','Reset password link sent on your email address, Please check your inbox and spam.');
		            } else {
		                display_output('0','Some error occurred! Please try again.');
		            } 
		        }
		    }else{
		    	display_output('0','Please enter email/login ID.');
		    }
	    }
	    $data['form_action']=site_url('forgot-password');
		$data['back_action']=site_url('login');
	    $this->load->view('front/include/header',$data);
        $this->load->view('front/forgot_password');
        $this->load->view('front/include/footer');
            
    }
    public function reset_password()
	{
		$user_id= $this->session->userdata('user_id');
        if(!empty($user_id))
        {
        	$user_type= $this->session->userdata('front_user_type');
        	if($user_type==2){
        		redirect('manage-service');
        	}else{
        		redirect('home');
        	}
            redirect(base_url());
        }
		$data['title']= 'Reset Password' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Reset Password'." | ".SITE_TITLE;
        $data['meta_desc']= 'Reset Password'." | ".SITE_TITLE;
        $data['page_title'] = "Reset Password";
		if($this->input->get('token')) 
		{
			$data['token'] = $this->input->get('token');
			if($user_data = $this->Common_model->getRecords('users','user_id,staging_id,user_type,reset_token_date,password',array('reset_token'=> $this->input->get('token')),'',true)) 
			{
				if($this->input->post()) 
				{
					$this->form_validation->set_rules('new_password', 'New Password', 'trim|required|min_length[6]|max_length[15]');
					$this->form_validation->set_rules('confirm_password', 'Confirm Password', 'trim|required|matches[new_password]');

					if ($this->form_validation->run() == FALSE) {	
						$this->form_validation->set_error_delimiters('<div class="parsley-errors-list">', '</div>');
						$this->load->view('front/include/header',$data);
				        $this->load->view('front/reset_password',$data);
				        $this->load->view('front/include/footer');
					} 
					else {

						if($user_data['password'] != base64_encode($this->input->post('new_password')) )
						{
							$where = array('user_id' => $user_data['user_id']);
							$update_data = array(
								'password' => base64_encode($this->input->post('new_password')), 
								'reset_token' =>'',
								'reset_token_date' =>''
								);
							if(!$this->Common_model->addEditRecords('users', $update_data, $where)) {
								$this->session->set_flashdata('error', 'Some error occurred. Please try again.');
								redirect('reset_password');
							} else {
								$log_msg = getUserType($user_data['user_type'])." ".$user_data['staging_id']." reset password.";
            					actionLog('users',$user_data['user_id'],'Reset Password',$log_msg,'User',$user_data['user_id']);
								$this->session->set_flashdata('success', 'Password Changed Successfully.');
								redirect('login');
							}
						} else {
							$this->session->set_flashdata('error', "Password Can't be same as old password !!");
							redirect($_SERVER['HTTP_REFERER']);
						}
					}
				} else {
					$token_date = strtotime($user_data['reset_token_date']);
					$current_date=strtotime(date("Y-m-d H:i:s"));
					$diff=$current_date-$token_date;
					if($diff > 86400) {
						$this->session->set_flashdata('error', 'Reset password link has been Experied !!');
						redirect('forgot-password');
					} else {
						 	$this->load->view('front/include/header',$data);
					        $this->load->view('front/reset_password',$data);
					        $this->load->view('front/include/footer');
					}
				}
			} else {
				$this->session->set_flashdata('error', 'Invalid reset password link !!');
				redirect('forgot-password');
			}	
		} else {
			$this->session->set_flashdata('error', 'Invalid reset password link !!');
			redirect('forgot-password');
		}
		
	}

	public function logout()
	{
		if(!empty($this->session->userdata('temp_user_item'))){
            $this->Common_model->deleteRecords('temp_table',array('temp_id'=>$this->session->userdata('temp_user_item')));
        }
        $user_id = $this->session->userdata('user_id');
		$frontend_username = getFrontUsername($user_id);
    	$log_msg = getUserType()." ".$frontend_username." logout.";
    	actionLog('users',$user_id,'',$log_msg,'User',$user_id);
		$this->session->sess_destroy();
		redirect("login");
	}
	

	public function change_password()
	{
		$this->Common_model->check_user_login();
    	$data['title']= 'Change Password' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Change Password'." | ".SITE_TITLE;
        $data['meta_desc']= 'Change Password'." | ".SITE_TITLE;
        $data['page_title'] = "Change Password";
		$user_id = $this->session->userdata('user_id');
		
		$current_password = $this->Common_model->getFieldValue('users', 'password', array('user_id'=>$user_id));
		$data['current_password'] =  base64_decode($current_password);
		if($this->input->post()) 
		{	
			$this->form_validation->set_rules('old_password','Old Password','required|trim');
			$this->form_validation->set_rules('new_password','New Password','trim|required|min_length[6]|max_length[15]');
			$this->form_validation->set_rules('confirm_password','Confirm Password','trim|required|matches[new_password]');

			if ($this->form_validation->run() == FALSE)
			{	
				$this->form_validation->set_error_delimiters('<div class="parsley-errors-list">', '</div>');
			} 
			else 
			{ 
				if($current_password == base64_encode($this->input->post('old_password'))) {
					$new_password = base64_encode($this->input->post('new_password'));
					$where = array('user_id'=>$user_id);
					$date = date("Y-m-d H:i:s");
					$update_data = array(
						'password' => $new_password,
						'modified'=>$date
						);

					if(!$this->Common_model->addEditRecords('users', $update_data, $where)) {
						$this->session->set_flashdata('error', 'Some error occurred! Please try again.');
						
					} else {
						$user_id = $this->session->userdata('user_id');
						$frontend_username = getFrontUsername($user_id);
		            	$log_msg = getUserType()." ".$frontend_username." changed his/her own password.";
		            	actionLog('users',$user_id,'update',$log_msg,'User',$user_id);
						$this->session->set_flashdata('success', 'Password changed successfully.');
						redirect('change-password');
					}
				} else {
					$this->session->set_flashdata('error', 'Old password is incorrect.');
					redirect('change-password');
				}
				redirect('change-password');
			}
		} 

		$data['form_action']=site_url('change-password');
	    $this->load->view('front/include/header',$data);
        $this->load->view('front/change_password');
        $this->load->view('front/include/footer');
	}
	public function profile() 
	{
		$data['title']= 'Profile' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Profile'." | ".SITE_TITLE;
        $data['meta_desc']= 'Profile'." | ".SITE_TITLE;
        $data['page_title'] = "Profile";
        $data['page_sub_title'] = "";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => base_url()
        );
        $data['breadcrumbs'][] = array(
            'title' => 'Profile',
            'link' => ''
        );
        if($this->uri->segment(2)){
        	$user_id=base64_decode($this->uri->segment(2));
        	$data['edit_icon_hide']=1;
        }else{
        	$user_id= $this->session->userdata('user_id');
        	$data['edit_icon_hide']=0;
        $user_type= $this->session->userdata('front_user_type');
        }
        if($user_type==2){
    		redirect('pages/page_not_found');
    	}
	    $this->Common_model->check_user_login();
	    $data['countries'] = $this->Common_model->getRecords('countries','*',array('id'=>231));
		if(!$data['user_data'] = $this->Front_common_model->get_user_by_id($user_id,$user_type)){
			redirect('pages/page_not_found');
		}
		$data['total_records']=$this->Home_model->my_post(0,0,$user_id);
		//echo '<pre>';print_r($data['user_data']);exit;
	  	$this->load->view('front/include/header',$data);
		$this->load->view('front/include/breadcrumb');
    	$this->load->view('front/profile');
		$this->load->view('front/include/footer');
	}
	 public function my_post_data(){
        $user_id  = $this->session->userdata('user_id');
        $this->Common_model->check_user_login();
        if(isset($_POST) && !empty($_POST)) {
        	if(isset($_POST['get_id']) && !empty($_POST['get_id'])){
        		$user_id =trim(base64_decode($_POST['get_id']));
        	}
            $page_number    = trim($_POST['page']);
            $item_per_page = FRONT_LIMIT;
            $offset = (($page_number) * $item_per_page);
            $user_post_data = $this->Home_model->my_post($offset,$item_per_page,$user_id);
            $details=array();
            if(!empty($user_post_data)){
            $i=0;
            foreach ($user_post_data as $key => $value) { 
                $where="post_id='".$value['post_id']."'";
                $like=$this->Home_model->get_count('like_post',$where);
                $comment=$this->Home_model->get_count('comments',$where);
                $where_shared="shared_post_id='".$value['post_id']."'";
                $share=$this->Home_model->get_count('post',$where_shared);
                $details[$i]['like']=shortNumber($like['count']);
                $details[$i]['comment']=shortNumber($comment['count']);
                $details[$i]['share']=shortNumber($share['count']);
                $details[$i]['is_like_by_me']=$value['is_like_by_me'];
                $details[$i]['is_report_by_me']= $this->Common_model->getFieldValue('post_report','id',array('user_id'=>$user_id,'post_id'=>$value['post_id']),'',true);
                $details[$i]['is_saved_by_me']= $this->Common_model->getFieldValue('save_post','id',array('user_id'=>$user_id,'post_id'=>$value['post_id']),'',true);
                if($value['shared_post_id']>0){
                	 $details[$i]['is_orignal_post_deleted']= $this->Common_model->getFieldValue('post','is_deleted',array('id'=>$value['shared_post_id']),'',true);
                    $post_media= $this->Common_model->getRecords('post_media','media_path,video_thumbnail,video_large_thumbnail,media_type,format,duration',array('media_is_deleted'=>0,'post_id'=>$value['shared_post_id']),'',false);
                    $details[$i]['post_id']=$value['post_id'];
                    $details[$i]['post_number']=$value['post_number'];
                    $details[$i]['user_id']=$value['user_id'];
                    $details[$i]['fullname']=$value['fullname'];
                    $details[$i]['profile_picture']=$value['profile_picture'];
                    $details[$i]['gender']=$value['gender'];
                    $details[$i]['created']=$value['created'];
                    $details[$i]['is_shared']=1;
                    $details[$i]['shared_post_id']=$value['shared_post_id'];
                    $details[$i]['post_content']=$value['post_content'];
                    $details[$i]['shared_post_content']=$value['shared_post_content'];
                    $details[$i]['post_media']=$post_media;
                    
                }else{
                    $post_media= $this->Common_model->getRecords('post_media','media_path,video_thumbnail,video_large_thumbnail,media_type,format,duration',array('media_is_deleted'=>0,'post_id'=>$value['post_id']),'',false);
                    $details[$i]['post_id']=$value['post_id'];
                    $details[$i]['post_number']=$value['post_number'];
                    $details[$i]['user_id']=$value['user_id'];
                    $details[$i]['fullname']=$value['fullname'];
                    $details[$i]['profile_picture']=$value['profile_picture'];
                    $details[$i]['gender']=$value['gender'];
                    $details[$i]['created']=$value['created'];
                    $details[$i]['is_shared']=0;
                    $details[$i]['shared_post_id']=0;
                    $details[$i]['post_content']=$value['post_content'];
                    $details[$i]['post_media']=$post_media;
                }
              $i++;  
            }
        }
        $data['news_feeds']=$details;

        //echo '<pre>';print_r($data['news_feeds']);exit;
        echo $all_feeds = $this->load->view('front/celebrant/feeds_data',$data,true); exit;
        }
    }
	public function edit_profile() 
	{
		
		$user_id= $this->session->userdata('user_id');
        $user_type= $this->session->userdata('front_user_type');
		$data['title']= 'Edit Profile' . " | ".SITE_TITLE;
        $data['meta_keywords']= 'Edit Profile'." | ".SITE_TITLE;
        $data['meta_desc']= 'Edit Profile'." | ".SITE_TITLE;
        $data['page_title'] = "Edit Profile";
        $data['page_sub_title'] = "Find best service provider's here";
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'title' => 'Home',
            'link' => base_url()
        );
        $data['show_sp_data']='';
        $seg=$this->uri->segment(1);
        if($seg=='upgrade-profile'){
        	$data['show_sp_data']='1';
        	if($user_type!=1){
        		redirect('home');
        	}
        	$data['form_action']=site_url('upgrade-profile');
        }else{
        	$data['form_action']=site_url('edit-profile');
        }
        if($user_type!=2){
	         $data['breadcrumbs'][] = array(
	            'title' => 'Profile',
	            'link' => base_url('profile')
	        );
     	}
        $data['breadcrumbs'][] = array(
            'title' => 'Edit Profile',
            'link' => ''
        );
        
	    $this->Common_model->check_user_login();
	    $data['countries'] = $this->Common_model->getRecords('countries','*',array('id'=>231));
		if(!$data['user_data'] = $this->Front_common_model->get_user_by_id($user_id,$user_type)){
			redirect('pages/page_not_found');
		}
		if($this->input->post()) 
		{
			$this->form_validation->set_rules('fullname', 'full name', 'trim|required|max_length[100]',array('required'=>'Please enter %s'));
			
			$this->form_validation->set_rules('email', 'email','trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('country', 'country', 'trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('state', 'state', 'trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('city', 'city', 'trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('address', 'address', 'trim|required',array('required'=>'Please enter %s'));
			$this->form_validation->set_rules('paypal_id', 'Paypal ID', 'trim|required',array('required'=>'Please enter %s'));
			$latitude=$this->input->post('latitude');
			$longitude=$this->input->post('longitude');
			if($latitude=='' && $longitude==''){
				display_output('0','Please enter valid address.'); 
			}
			if($user_type=='2' || $user_type=='3' || $data['show_sp_data']==1){
				$this->form_validation->set_rules('company_name', 'company name', 'trim|required|max_length[100]',array('required'=>'Please enter %s'));
				$this->form_validation->set_rules('business_no', 'business_no', 'trim|max_length[50]',array('required'=>'Please enter %s'));
				$this->form_validation->set_rules('establishment_year', 'establishment year', 'trim|required',array('required'=>'Please enter %s'));
				$this->form_validation->set_rules('document_type', 'document type', 'trim|required',array('required'=>'Please enter %s'));
				$document_type=$this->input->post('document_type');
			}

			if($this->form_validation->run()==TRUE) 
			{
				front_check_server_unique('users', 'email',$this->input->post('email'),'user_id',$user_id,'Email');
				
				$dob=$this->input->post('dob');
				$signup_data = array(
					'fullname'=> $this->input->post('fullname'),
					'email'=> $this->input->post('email'),
				 	'dob'=>$dob?date('Y-m-d',strtotime($dob)):'',
		            'gender'=>$this->input->post('gender'),
		            'interests'=>$this->input->post('interests'),
		            'about_description'=>$this->input->post('about_description'),
		            'hobbies'=>$this->input->post('hobbies'),
		            'hometown'=>$this->input->post('hometown'),
		            'relationship_status'=>$this->input->post('relationship_status'),
		            'education'=>$this->input->post('education'),
		            'paypal_id'=>$this->input->post('paypal_id'),
					'modified'=> date("Y-m-d H:i:s"),
				);
		 		if($user_type==2 || $user_type==3 || $data['show_sp_data']==1){
                    $filepath="";
		            $filerror="";
		            $width="";$height="";
		            if(isset($_FILES['document_proof']) && !empty($_FILES['document_proof']['name'])){
		                if($_FILES['document_proof']['error']==0) {
		                    $image_path = USER_DOCUMNET_PATH;
		                    $allowed_types = 'jpg|jpeg|png|doc|docx|pdf|JPG|JPEG|PNG|DOC|DOCX';
		                    $file='document_proof';
		                    $height = 150;
		                    $width = 150;
		                    $responce = commonImageUpload($image_path,$allowed_types,$file,$width,$height);
		                    if($responce['status']==0){
		                        $upload_error = $responce['msg'];   
		                        $filerror="1";
		                    } else {
		                        $filepath=$responce['image_path'];
		                    }
		                }
		            }
		            if($filerror!=''){
		                display_output('0',strip_tags($upload_error));
		            }
		            if(!empty($filepath)){
		                $signup_data['document_proof']=$filepath;
		                $signup_data['document_status']='Unverified';
		                if($data['show_sp_data']==1){
		                	$signup_data['user_type']='3';
		                	$this->session->set_userdata('front_user_type','3');
		                }
		            }
		            $signup_data['company_name']=$this->input->post('company_name');
		            $signup_data['establishment_year']=$this->input->post('establishment_year');
		            $signup_data['business_no']=$this->input->post('business_no');
		            $past_doc=$this->Common_model->getFieldValue('users','document_type',array('user_id'=>$user_id),'',true);
		            if($past_doc!=$document_type){
		                $signup_data['document_type']=$document_type;
		                $signup_data['document_status']='Unverified';
		            }
		        }
		        $imgfilepath="";
	            $imgfilerror="";
	            if(isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']['name'])){
	                if($_FILES['profile_picture']['error']==0) {
	                    $image_path = USER_PROFILE_PATH;
	                    $allowed_types = 'jpg|jpeg|png|JPG|JPEG|PNG';
	                    $file='profile_picture';
	                    $height = 150;
	                    $width = 150;
	                    $imgresponce = commonImageUpload($image_path,$allowed_types,$file,$width,$height);

	                    if($imgresponce['status']==0){
	                        $upload_error = $imgresponce['msg'];   
	                        $imgfilerror="1";
	                    } else {
	                        $imgfilepath=$imgresponce['image_path'];
	                    }
	                }
	            }
	            if($imgfilerror!=''){
	                display_output('0',strip_tags($upload_error));
	            }
	            if(!empty($imgfilepath)){
	                $signup_data['profile_picture']=$imgfilepath;
	            }
				$this->db->trans_begin();
		        if($this->Common_model->addEditRecords('users',$signup_data,array('user_id'=>$user_id))){
		            $address_data = array(
		                'country'=>$this->input->post('country'),
		                'state'=>$this->input->post('state'),
		                'city'=>$this->input->post('city'),
		                'latitude'=>$this->input->post('latitude'),
		                'longitude'=>$this->input->post('longitude'),
		                'address'=>$this->input->post('address'),
		                'status'=>'Primary',
		                'modified'=> date("Y-m-d H:i:s"),
		            );
		            $this->Common_model->addEditRecords('user_address',$address_data,array('user_id'=>$user_id,'status'=>'Primary'));
		            $this->db->trans_complete();
		            if ($this->db->trans_status() === FALSE){
		                $this->db->trans_rollback();
		                display_output('0','Some error occured! Please try again.'); 
		            } else {
		            	$user_id = $this->session->userdata('user_id');
						$frontend_username = getFrontUsername($user_id);
						if($data['show_sp_data']==1){
							$log_msg = getUserType()." ".$frontend_username." updated his/her own profile as celebrant SP.";
						}else{
		            		$log_msg = getUserType()." ".$frontend_username." updated his/her own profile.";
						}
		            	actionLog('users',$user_id,'update',$log_msg,'User',$user_id); 
		                $this->db->trans_commit();
		                display_output('1','Profile updated successfully.');
		            }
		        }else{
		            display_output('0','Some error occured! Please try again.');  
		        }
			} else {
				display_output('0','Please fill all required fields.');   
			}
		}
		if ($this->input->is_ajax_request()) {
		   return $this->load->view('front/edit_profile',$data);
		}else{
		  	$this->load->view('front/include/header',$data);
    		$this->load->view('front/include/breadcrumb');
        	$this->load->view('front/edit_profile');
    		$this->load->view('front/include/footer');
		}		
	}
	public function upgrade_sp_as_csp(){
		$user_id= $this->session->userdata('user_id');
        $user_type= $this->session->userdata('front_user_type');
        if($user_type!=2){
        	display_output('0','You are not service provider.');
        }
        $update_data = array(
            'user_type'=>3,
            'modified'=> date("Y-m-d H:i:s"),
        );
        if($this->Common_model->addEditRecords('users',$update_data,array('user_id'=>$user_id))){
        	$this->session->set_userdata('front_user_type', 3);
        	$user_id = $this->session->userdata('user_id');
			$frontend_username = getFrontUsername($user_id);
			$log_msg = getUserType()." ".$frontend_username." updated his/her own profile as celebrant SP.";
        	actionLog('users',$user_id,'update',$log_msg,'User',$user_id); 
        	display_output('1','User updated successfully.');
        }else{
        	display_output('0','Some error occured! Please try again.');
        }
	}
	//////////////////////////////////////////////SMS Configuration///////////////////////////////////
	public function otp_send($mobile,$otp)
    {

        $message = SITE_TITLE.'- Verification OTP : '.$otp;

        $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.msg91.com/api/sendhttp.php?campaign=attendance&response=json&mobiles=$mobile&authkey=284459AmmGJoNl5d246f3e&route=4&sender=TESTIN&message=$message&country=91",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
           
            return $response;
           
        }
    }

    public function send_sms($mobile,$msg='')
    {

        $message = SITE_TITLE.' - '.$msg. 'Regards,'.SITE_TITLE. 'Team';
        $curl = curl_init();
        curl_setopt_array($curl, array(
           CURLOPT_URL => "https://api.msg91.com/api/sendhttp.php?campaign=attendance&response=json&mobiles=$mobile&authkey=284459AmmGJoNl5d246f3e&route=4&sender=TESTIN&message=$message&country=91",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
           
            return $response;
        }
    }
	
    public function notification_list() {
        $limit=FRONT_LIMIT;
        $user_id  = $this->session->userdata('user_id');
        //$this->Common_model->check_user_login();
        if(isset($_POST) && !empty($_POST)){
           $data['track_page']=$_POST['page'];
           $offset=$_POST['page']*$limit;
        }else{
            $data['track_page']=0;
            $offset=0;
        } 
        $data['notification_list']=$this->Home_model->notification_list($offset,$limit);
        echo $all_services = $this->load->view('front/include/notification',$data,true); exit;
    }
    public function update_notification() {
        $this->Common_model->check_user_login();
        if(isset($_POST) && !empty($_POST)){
           $type=$_POST['type'];
           $record_id=$_POST['record_id'];
           $notification_id=base64_decode($_POST['notification_id']);
           $this->Common_model->addEditRecords('notifications',array('is_read'=>1),array('notification_id'=>$notification_id));
           	switch ($type) {
            case 'new_event': //cele get notification when book service
                echo $link='my-event-details/'.$record_id; 
                break; 
            case 'event_canceled':  //attendee get notifcation for event cancel
                echo $link='public-event-details/'.$record_id;
                break;
            case 'request_to_join':
                echo $link='my-event-details/'.$record_id;
                break;
            case 'event_invite':
                echo $link='public-event-details/'.$record_id;
                break;
            case 'event_canceled_by_sp':
                echo $link='my-event-details/'.$record_id;
                break;
            case 'event_canceled_by_celebrant': //celebrant cancel event
                echo $link='my-booking-details/'.$record_id;
                break;
            case 'event_completed': //event completed by celebrant
                echo $link='my-booking-details/'.$record_id;
                break;
            case 'invite_event_request_accepted': //when invited user accept event rq
                echo $link='my-event-details/'.$record_id;
                break;
            case 'request_event_request_accepted': //when request to join & cele accept event request
                echo $link='public-event-details/'.$record_id;
                break;
            case 'invite_event_request_rejected': //when invited user reject event rq
                echo $link='my-event-details/'.$record_id;
                break;
            case 'request_event_request_rejected': //when request to join & cele reject event request
                echo $link='public-event-details/'.$record_id;
                break;
            //////////////////order/////////////////////
            case 'new_order': //sp get notification when cele book service
                echo $link='my-booking-details/'.$record_id; 
                break;
            case 'order_request_accepted': //order accepted by sp
            case 'order_request_rejected': //order rejected by sp
                echo $link='my-event-details/'.$record_id; 
                break;
            ///////////////////Social media//////////////
            case 'post_like': //post like by others
            case 'post_comment': //comment by others
            case 'comment_like': //comment like by others
            	$post_num=base64_decode($record_id);
            	$post_number='Post'.str_pad($post_num,8,0,STR_PAD_LEFT);
       	 		echo $link='post-details/'.base64_encode($post_number); 
                break;  
            case 'friend_request': //friend request sent
         		echo $link='friends-request-list'; 
                break;
            case 'friend_request_accepted': //friend request accepted by other
                echo $link='friends-list'; 
                break; 
            ///////admin sent notification//////////
            case 'document_status_update': //document status updated by admin
                echo $link='edit-profile'; 
                break; 

       }
        
        
    }
    
}

}
