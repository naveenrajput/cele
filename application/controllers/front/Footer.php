<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Footer extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('admin/Common_model','Front_common_model','front/Home_model'));
		$this->load->helper('common_helper');
		
	}

	public function about_us()
	{
		$data['title']= 'About Us' . " | ".SITE_TITLE;
	    $data['page_title'] = "About Us";
	    $data['page_detail']=$this->Common_model->getRecords('pages','*',array('page_id'=> '5'),'',true);
	    $this->load->view('front/include/header',$data);
	    $this->load->view('front/about-us');
	    $this->load->view('front/include/footer');
	}

	public function privacy_policy()
	{
		$data['title']= 'Privacy Policy' . " | ".SITE_TITLE;
	    $data['page_title'] = "Privacy Policy";
	    $data['page_detail']=$this->Common_model->getRecords('pages','*',array('page_id'=> '2'),'',true);
	    $this->load->view('front/include/header',$data);
	    $this->load->view('front/privacy-policy');
	    $this->load->view('front/include/footer');
	}

	public function terms_and_conditions()
	{
		$data['title']= 'Terms & Condition' . " | ".SITE_TITLE;
	    $data['page_title'] = "Terms & Condition";
	    $data['page_detail']=$this->Common_model->getRecords('pages','*',array('page_id'=> 1),'',true);
	    $this->load->view('front/include/header',$data);
	    $this->load->view('front/terms-and-conditions');
	    $this->load->view('front/include/footer');
	}
	public function cancellation_refund_policy()
	{
		$data['title']= 'Cancellation & Refund Policy' . " | ".SITE_TITLE;
	    $data['page_title'] = "Cancellation & Refund Policy";
	    $data['page_detail']=$this->Common_model->getRecords('pages','*',array('page_id'=> '4'),'',true);
	    $this->load->view('front/include/header',$data);
	    $this->load->view('front/cancellation_refund_policy');
	    $this->load->view('front/include/footer');
	}
	public function contact_us()
	{
		$data['title']= 'Contact Us' . " | ".SITE_TITLE;
	    $data['page_title'] = "Contact Us";
	    $data['page_detail']=$this->Common_model->getRecords('pages','*',array('page_id'=> '3'),'',true);
	     if($this->input->post()) { 
            $this->form_validation->set_rules('name', 'name', 'trim|required|max_length[50]',array('required'=>'Please enter %s'));
            $this->form_validation->set_rules('email', 'email', 'trim|required',array('required'=>'Please enter %s'));
            $this->form_validation->set_rules('subject', 'subject', 'trim|required|max_length[150]',array('required'=>'Please select %s'));
            $this->form_validation->set_rules('message', 'message', 'trim|required|max_length[500]',array('required'=>'Please select %s'));
            if($this->form_validation->run()==TRUE) 
            {
                $insert_data=array(   
                    'name'=>$this->input->post('name'),
                    'email'=>$this->input->post('email'),
                    'subject'=>$this->input->post('subject'),
                    'message'=>$this->input->post('message'),
                    'created'=>date('Y-m-d H:i:s')
                );
                if($last_id=$this->Common_model->addEditRecords('contact',$insert_data)){
                	$ticket_number='TID'.str_pad($last_id,8,0,STR_PAD_LEFT);
                	$this->Common_model->addEditRecords('contact',array('ticket_id'=>$ticket_number),array('contact_id'=>$last_id));
                    display_output('1','We will get back to you soon.');
                }else{
                    display_output('0','Something went wrong. Please try again.'); 
                }
            }else{
                display_output('0','Please fill all required fields.');
            }
        }
	    $data['form_action']=site_url('contact-us');
	    $this->load->view('front/include/header',$data);
	    $this->load->view('front/contact_us');
	    $this->load->view('front/include/footer');
	}
}