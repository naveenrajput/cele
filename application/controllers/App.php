<?php defined('BASEPATH') OR exit('No direct script access allowed');
class App extends CI_Controller
{
    function __construct() 
    {
      
        parent::__construct();
        $this->load->model(array('admin/Common_model','App_model','Front_common_model'));
        $this->load->helper('common_helper'); 
        $this->load->helper('string');
       // $this->load->library(array('encrypt','PHPExcel'));
        $this->load->library(array('upload','S3'));
       // ini_set('display_errors', 0);
       // error_reporting(0);

        $h_key= getallheaders();
        if(isset($h_key['Apikey']))
        {
            $h_key['Apikey']=$h_key['Apikey'];
        }
        if(isset($h_key['apikey']))
        {
            $h_key['Apikey']=$h_key['apikey'];
        }
        if(APP_KEY !== $h_key['Apikey'])  //check header key for authorizetion 
        {
            echo json_encode(array('data'=> array('status' =>'0' ,'msg'=>"Error Invalid Api Key")));
            header('HTTP/1.0 401 Unauthorized');
            die;
        }
    }
    public function otp_send($mobile,$otp)
    {

        $message = SITE_TITLE.'- Verification OTP : '.$otp;

        $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.msg91.com/api/sendhttp.php?campaign=attendance&response=json&mobiles=$mobile&authkey=284459AmmGJoNl5d246f3e&route=4&sender=TESTIN&message=$message&country=91",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
           
            return $response;
           
        }
    }

    public function send_sms($mobile,$msg='')
    {

        $message = SITE_TITLE.' - '.$msg. 'Regards,'.SITE_TITLE. 'Team';
        $curl = curl_init();
        curl_setopt_array($curl, array(
           CURLOPT_URL => "https://api.msg91.com/api/sendhttp.php?campaign=attendance&response=json&mobiles=$mobile&authkey=284459AmmGJoNl5d246f3e&route=4&sender=TESTIN&message=$message&country=91",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_SSL_VERIFYHOST => 0,
          CURLOPT_SSL_VERIFYPEER => 0,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
        } else {
           
            return $response;
        }
    }
    public function signup() 
    {
        $fullname               =   $this->clear_input($this->input->post('fullname'));
        $staging_id             =   $this->clear_input($this->input->post('staging_id'));
        $email                  =   $this->clear_input($this->input->post('email'));
        $mobile                 =   $this->clear_input($this->input->post('mobile'));
        $user_type              =   $this->clear_input($this->input->post('user_type'));
        $password               =   $this->clear_input($this->input->post('password'));
        $device_id              =   $this->clear_input($this->input->post('device_id'));
        $device_type            =   $this->clear_input($this->input->post('device_type'));
        $company_name           =   $this->clear_input($this->input->post('company_name'));
        $country                =   $this->clear_input($this->input->post('country'));
        $state                  =   $this->clear_input($this->input->post('state'));
        $city                  =   $this->clear_input($this->input->post('city'));
        $address                =   $this->clear_input($this->input->post('address'));
        $business_no       =   $this->clear_input($this->input->post('business_no'));
        $establishment_year     =   $this->clear_input($this->input->post('establishment_year'));
        $document_type          =   $this->clear_input($this->input->post('document_type'));
        $latitude          =   $this->clear_input($this->input->post('latitude'));
        $longitude          =   $this->clear_input($this->input->post('longitude'));

        $mobile_auth_token      =   $this->App_model->generateRandomString();
        $tableName ="users";
        if(empty($fullname)){
            $this->display_output('0','Please enter fullname.');
        }
        if(empty($user_type)){
            $this->display_output('0','Please enter user type.');
        } 
        if(empty($password)) {
            $this->display_output('0','Please enter password.');
        }
        if(strlen($password) < 6) {
            $this->display_output('0','Password length must be minimum of 6 characters.');
        }
        if(empty($staging_id)){
            $this->display_output('0','Please enter login ID.');
        }
        if(strlen($staging_id) < 6 && strlen($staging_id) > 15) {
            $this->display_output('0','Login ID length must be minimum of 6 characters and maximum of 15 characters long.');
        } 
        if(!empty($staging_id)){
            $where1 = array('staging_id' => $staging_id,'is_deleted'=>'0');
            if($this->Common_model->getRecords($tableName,'user_id',$where1,'',true)) {
                $this->display_output('0','Login ID already used.');
            }
        }
        if(empty($email)){
            $this->display_output('0','Please enter email.');
        }
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            $this->display_output('0','Please enter a valid email.');
        }
        if(!empty($email)){
            $where2 = array('email' => $email,'is_deleted'=>'0');
            if($this->Common_model->getRecords($tableName,'user_id',$where2,'',true)) {
                $this->display_output('0','Email already used.');
            }
        }
        if(empty($mobile)) {
            $this->display_output('0','Please enter mobile number.');
        }
        if(!empty($mobile)){
            $where = array('mobile' => $mobile,'is_deleted'=>'0');
            if($this->Common_model->getRecords($tableName,'user_id',$where,'',true)) {
                $this->display_output('0','Mobile number already used.');
            }
        }
        if(empty($country)){
            $this->display_output('0','Please select country.');
        }if(empty($state)){
            $this->display_output('0','Please select state.');
        }
        if(empty($city)){
            $this->display_output('0','Please select city.');
        }
        if(empty($address)){
            $this->display_output('0','Please enter address.');
        }
        if(empty($device_id)) {
            $this->display_output('0','Please enter device id.');
        }
        if(empty($device_type)) {
            $this->display_output('0','Please enter device type.');
        } else if($device_type !='android' && $device_type !='ios' ){
            $this->display_output('0','Device type must be either android or ios.');
        }
        $password= base64_encode($password);
        $otp = mt_rand(1000,9999);
        $otp ='1234';
        $signup_data = array( 
            'fullname'=>$fullname,
            'staging_id'=>$staging_id,
            'email'=>$email,
            'user_type'=>$user_type,
            'mobile'=>$mobile,
            'status'=>'Unverified',
            'password'=> $password,
            'otp' =>$otp,
            'otp_expired'=>date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))+900),
            'device_type'=>$device_type,
            'device_id'=>$device_id,
            'mobile_auth_token'=> $mobile_auth_token,
            'created'=> date("Y-m-d H:i:s"),
        );
        if($user_type==2 || $user_type==3){
            if(empty($company_name)){
                $this->display_output('0','Please enter company name.');
            }if(empty($address)){
                $this->display_output('0','Please enter address.');
            }if(empty($latitude)){
                $this->display_output('0','Please enter latitude.');
            }if(empty($longitude)){
                $this->display_output('0','Please enter longitude.');
            }if(empty($establishment_year)){
                $this->display_output('0','Please enter establishment year.');
            }if(empty($document_type)){
                $this->display_output('0','Please select document.');
            }
            $filepath="";
            $filerror="";
            $width="";$height="";
            if(isset($_FILES['document_proof']) && !empty($_FILES['document_proof']['name'])){
                if($_FILES['document_proof']['error']==0) {
                    $image_path = USER_DOCUMNET_PATH;
                    $allowed_types = '*';
                    $file='document_proof';
                    $height = 150;
                    $width = 150;
                    $responce = commonImageUpload($image_path,$allowed_types,$file,$width,$height);

                    if($responce['status']==0){
                        $upload_error = $responce['msg'];   
                        $filerror="1";
                    } else {
                        $filepath=$responce['image_path'];
                    }
                }
            }else{
                $this->display_output('0','Please upload document.');
            }
            if($filerror!=''){
                $this->display_output('0',strip_tags($upload_error));
            }
            if(!empty($filepath)){
                $signup_data['document_proof']=$filepath;
            }
            $signup_data['company_name']=$company_name;
           /* $signup_data['country']=$country;
            $signup_data['state']=$state;
            $signup_data['address']=$address;*/
            $signup_data['establishment_year']=$establishment_year;
            $signup_data['business_no']=$business_no;
            $signup_data['document_type']=$document_type;
            $signup_data['document_status']='Unverified';
        }
        $this->db->trans_begin();
        if($last_id = $this->Common_model->addEditRecords('users',$signup_data)){
            $where  = array('user_id' => $last_id);
            $user_number=str_pad($last_id,8,0,STR_PAD_LEFT);
            $this->Common_model->addEditRecords('users',array('user_number'=>$user_number),$where);
            $address_data = array(
                'user_id'=>$last_id, 
                'country'=>$country,
                'state'=>$state,
                'city'=>$city,
                'latitude'=>$latitude,
                'longitude'=>$longitude,
                'address'=>$address,
                'status'=>'Primary',
                'created'=> date("Y-m-d H:i:s"),
            );
            $this->Common_model->addEditRecords('user_address',$address_data);
            if(APP_PUBLISH){
                if($user_type==2 || $user_type==3){
                    $this->Front_common_model->free_trial_subscription($last_id);
                }
            }
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $this->display_output('0','Some error occured! Please try again.'); 
            } else {
                $this->db->trans_commit();

                $user_record=$this->Common_model->getRecords('users','user_id,fullname,staging_id,mobile,user_type,status,email,company_name,establishment_year,business_no,document_type,document_proof,document_status',$where,'',true);
                if($user_type==2 || $user_type==3){
                    //$message='Registration completed & verification OTP sent on your registered mobile number, Please check it. We\'re reviewing your information. Your account should be verified shortly.' ;
                    $message='We\'re reviewing your information. Your account should be verified shortly.' ;
                }else{
                    $message='Registration completed & verification OTP sent on your registered mobile number, Please check it.';
                }
                $log_msg = getUserType($user_type)." ".$staging_id." registered as a new user.";
                actionLog('users',$last_id,'Sign Up',$log_msg,'User',$last_id);
                $this->display_output('1',$message,array('details'=>$user_record));
            }
            /*$user_record=$this->Common_model->getRecords('users','user_id,fullname,staging_id,mobile,user_type,status,email,company_name,establishment_year,business_no,document_type,document_proof,document_status',$where,'',true);
            if($user_type==2 || $user_type==3){
                $message='Registration completed & verification OTP sent on your registered mobile number, Please check it. We\'re reviewing your information. Your account should be verified shortly.' ;
            }else{
                 $message='Registration completed & verification OTP sent on your registered mobile number, Please check it.';
            }
            $this->display_output('1',$message,array('details'=>$user_record));*/
        }else{
            $this->display_output('0','Some error occured! Please try again.');  
        }
    }
    public function verify_otp(){
        $mobile  = $this->clear_input($this->input->post('mobile'));
        $otp  = $this->clear_input($this->input->post('otp'));
        if(empty($mobile)) {
            $this->display_output('0','Please enter mobile number.');
        }
        if(empty($otp)) {
            $this->display_output('0','Please enter otp.');
        }
       
        if($user_data = $this->Common_model->getRecords('users','user_id,fullname,staging_id,mobile,user_type,status,email,company_name,establishment_year,business_no,document_type,document_proof,document_status,otp,otp_expired',array('mobile'=>$mobile,'otp'=>$otp,'is_deleted'=>0),'user_id DESC',true)){
            $current_date = new DateTime("now", new DateTimeZone('UTC'));
            if(strtotime($user_data['otp_expired']) < strtotime($current_date->format('Y-m-d H:i:s'))){
                $this->display_output('0','Your OTP has been expired.'); 
            }
            if($user_data['otp']==$otp){
                $mobile_auth_token= $this->App_model->generateRandomString();
                $otp_data = array( 
                    'otp'=>'',
                    'otp_expired'=>'',
                    'mobile_auth_token'=>$mobile_auth_token,
                    'status'=>'Active',
                    'modified'=>date('Y-m-d H:i:s')
                );
                if($user_data['status']=='Unverified'){
                    $log_msg = getUserType($user_data['user_type'])." ".$user_data['staging_id']." verified account.";
                    actionLog('users',$user_data['user_id'],'Verified account',$log_msg,'User',$user_data['user_id']);
                }else{
                    $log_msg = getUserType($user_data['user_type'])." ".$user_data['staging_id']." logged in.";
                    actionLog('users',$user_data['user_id'],'Login',$log_msg,'User',$user_data['user_id']);
                }
                if($this->Common_model->addEditRecords('users',$otp_data,array('mobile'=>$mobile))){
                    $login=array(   
                        'user_id'=>$user_data['user_id'],
                        'fullname'=>$user_data['fullname'],
                        'staging_id'=>$user_data['staging_id'],
                        'mobile'=>$user_data['mobile'],
                        'email'=>$user_data['email'],
                        'user_type'=>$user_data['user_type'],
                        'status'=>'Active',
                        'company_name'=>$user_data['company_name'],
                       /* 'country'=>$user_data['country'],
                        'state'=>$user_data['state'],
                        'address'=>$user_data['address'],*/
                        'establishment_year'=>$user_data['establishment_year'],
                        'business_no'=>$user_data['business_no'],
                        'document_type'=>$user_data['document_type'],
                        'document_proof'=>$user_data['document_proof'],
                        'document_status'=>$user_data['document_status'],
                        'mobile_auth_token'=>$mobile_auth_token,
                        );
                    $this->display_output('1','OTP verified successfully.',array('details'=>$login)); 
                }else{
                    $this->display_output('0','Some error occured! Please try again.'); 
                }
            }else{
                $this->display_output('0','Wrong OTP ! Please enter correct OTP.');   
            }

        }else{
            $this->display_output('0','Invalid OTP.'); 
        }
    }
    public function login() 
    {
        $mobile       =  $this->clear_input($this->input->post('mobile'));
        $staging_id   =  $this->clear_input($this->input->post('staging_id'));
        $password     =  $this->clear_input($this->input->post('password'));
        $device_id    =  $this->clear_input($this->input->post('device_id'));
        $device_type  =  $this->clear_input($this->input->post('device_type')); 

        if(empty($device_type)) {
            $this->display_output('0','Please enter device type');
        } else if($device_type !='android' && $device_type !='ios') {
            $this->display_output('0','Device type must be either andriod or ios.');
        }
        
        if(empty($device_id)) {
            $this->display_output('0','Device required.');
        }

        if(empty($mobile) && empty($staging_id)) {
            $this->display_output('0','Please enter mobile or login ID.');
        }
        if(!empty($staging_id)){
            if(empty($password)) {
                $this->display_output('0','Please enter your password.');
            }
        }


        $device_type = strtolower($device_type);
        $password=base64_encode($password);
        if(!empty($staging_id)){
            $where="staging_id='".addslashes($staging_id)."'";
        }else{
            $where="mobile='".addslashes($mobile)."'";
        }
        
        if($user_data = $this->Common_model->getRecords('users','user_id,fullname,staging_id,mobile,user_type,status,email,password,company_name,establishment_year,business_no,document_type,document_proof,document_status,is_deleted,mobile_auth_token',$where,'user_id DESC',true)) {
           
            if($user_data['status']=='Inactive')
            { 
                $this->display_output('0','Your account has been deactivated! Please contact administrator for more details.');
            }
            
            if($user_data['is_deleted']==1)
            { 
                $this->display_output('0','Your account has been deleted ! Please contact administrator for more details.');
            }
            if(!empty($staging_id)){
                if($user_data['password']==$password){
                    if($user_data['status']=='Unverified')
                    { 
                        $this->display_output('2','Your account is not verified.');
                    }
                    if($user_data['is_deleted']==0 && $user_data['status']=='Active'){
                        
                        $mobile_auth_token= $this->App_model->generateRandomString();
                        $formData = array('device_id' => $device_id,'device_type' => $device_type,'mobile_auth_token' => $mobile_auth_token); 
                        $this->Common_model->addEditRecords('users',$formData,array('user_id'=>$user_data['user_id']));  

                        $login=array(   
                            'user_id'=>$user_data['user_id'],
                            'fullname'=>$user_data['fullname'],
                            'staging_id'=>$user_data['staging_id'],
                            'mobile'=>$user_data['mobile'],
                            'email'=>$user_data['email'],
                            'user_type'=>$user_data['user_type'],
                            'status'=>$user_data['status'],
                            'company_name'=>$user_data['company_name'],
                            /*'country'=>$user_data['country'],
                            'state'=>$user_data['state'],
                            'address'=>$user_data['address'],*/
                            'establishment_year'=>$user_data['establishment_year'],
                            'business_no'=>$user_data['business_no'],
                            'document_type'=>$user_data['document_type'],
                            'document_proof'=>$user_data['document_proof'],
                            'document_status'=>$user_data['document_status'],
                            'mobile_auth_token'=>$mobile_auth_token,
                            );
                        $log_msg = getUserType($user_data['user_type'])." ".$user_data['staging_id']." logged in.";
                        actionLog('users',$user_data['user_id'],'Login',$log_msg,'User',$user_data['user_id']);
                       
                        $this->display_output('1','Logged in successfully.',array('details'=>$login));
                    }
                    else
                    {
                        $this->display_output('0','Your account has been deactivated!  Please contact administrator for details.');
                    }
                }else{
                    $this->display_output('0','Incorrect Password.');
                }
            }else{
                if($user_data['is_deleted']==0){
                    $otp = mt_rand(1000,9999);
                    $otp ='1234';
                    $mobile_auth_token= $this->App_model->generateRandomString();
                    //$this->otp_send($mobile,$otp); 
                    $formData = array('device_id' => $device_id,'device_type' => $device_type,'mobile_auth_token' => $mobile_auth_token,'otp' =>$otp,'otp_expired'=>date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))+900)); 
                    $this->Common_model->addEditRecords('users',$formData,array('user_id'=>$user_data['user_id']));  
                    $login=array(   
                        'user_id'=>$user_data['user_id'],
                        'fullname'=>$user_data['fullname'],
                        'staging_id'=>$user_data['staging_id'],
                        'mobile'=>$user_data['mobile'],
                        'email'=>$user_data['email'],
                        'user_type'=>$user_data['user_type'],
                        'status'=>$user_data['status'],
                        'company_name'=>$user_data['company_name'],
                       /* 'country'=>$user_data['country'],
                        'state'=>$user_data['state'],
                        'address'=>$user_data['address'],*/
                        'establishment_year'=>$user_data['establishment_year'],
                        'business_no'=>$user_data['business_no'],
                        'document_type'=>$user_data['document_type'],
                        'document_proof'=>$user_data['document_proof'],
                        'document_status'=>$user_data['document_status'],
                        'mobile_auth_token'=>$mobile_auth_token,
                        );
                   
                    $this->display_output('1','Verification OTP sent on your registered mobile number, Please check it.',array('details'=>$login));
                }
                else
                {
                    $this->display_output('0','Your account has been deactivated!  Please contact administrator for details.');
                }
               
            }
            
        }
        else
        {
            if(!empty($staging_id)){
                $this->display_output('0','Login ID does not match.');
            }else{
                $this->display_output('0','Mobile number does not match.');
            }
        }
    }
    public function resend_otp(){
        $mobile= $this->clear_input($this->input->post('mobile'));
      
        if(empty($mobile)) {
            $this->display_output('0','Please enter mobile no.');
        }
        if(!$user_data= $this->Common_model->getRecords('users','user_id,staging_id,user_type,status,is_deleted,mobile,mobile_auth_token',array('mobile'=>$mobile),'user_id DESC',true)) { 
            $this->display_output('0','Please enter correct mobile no.'); 
        }
        if($user_data['is_deleted']==1) {
            $this->display_output('0','Your account has been deleted, Please contact us for more details.'); 
        } 
        if($user_data['status']=='Inactive') {
            $this->display_output('0','Your account has been inactive, Please contact us for more details.');
        } 
        $otp = mt_rand(1000,9999);
        $otp ='1234';
        //$this->otp_send($mobile,$otp);
        $otp_data = array( 
            'otp'=>$otp,
            'otp_expired'=>date('Y-m-d H:i:s',strtotime(date('Y-m-d H:i:s'))+900),
            'modified'=>date('Y-m-d H:i:s')
        );
        if($this->Common_model->addEditRecords('users',$otp_data,array('mobile'=>$mobile))){
            $log_msg = getUserType($user_data['user_type'])." ".$user_data['staging_id']." requested to resend OTP";
            actionLog('users',$user_data['user_id'],'Resend OTP',$log_msg,'User',$user_data['user_id']);
            $this->display_output('1','OTP sent to your registered mobile no.'); 
        }else{
            $this->display_output('0','Some error occured! Please try again.'); 
        }

    } 
    public function logout() 
    {
        $user_id            =   $this->clear_input($this->input->post('user_id'));
        $mobile_auth_token  =   $this->clear_input($this->input->post('mobile_auth_token'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $update = array(
            'mobile_auth_token'=>'',
            'device_id'=>'',
            'device_type'=>''
        );
        $this->Common_model->addEditRecords('users',$update,array('user_id' => $user_id));
        $user_data=$this->Common_model->getRecords('users','user_id,staging_id,user_type',array('user_id' => $user_id),'',true);
        $user_id = $user_data['user_id'];
        $frontend_username = getFrontUsername($user_id);
        $log_msg = getUserType($user_data['user_type'])." ".$frontend_username." logout.";
        actionLog('users',$user_id,'',$log_msg,'User',$user_id);
        $this->display_output('1','Logout successfully.');
    }

    public function change_password() 
    {
        $user_id            = $this->clear_input($this->input->post('user_id')); 
        $current_password   = $this->clear_input($this->input->post('current_password'));
        $new_password       = $this->clear_input($this->input->post('new_password'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }  
        if(empty($current_password)){
            $this->display_output('0','Please enter current password.');
        }
        if(empty($new_password)){
            $this->display_output('0','Please enter new password.');
        }  
        $this->App_model->check_user_status();

        $old_password = $this->Common_model->getFieldValue('users', 'password', array('user_id'=>$user_id));

        if($old_password == base64_encode($current_password)) {
            $new_password = base64_encode($new_password);
            $where = array('user_id'=>$user_id);
            $update_data = array(
                'password' => $new_password,
                'modified'=>date("Y-m-d H:i:s")
                );

            if(!$this->Common_model->addEditRecords('users', $update_data, $where)) {
                $this->display_output('0','Some error occurred! Please try again.');
            } else {
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getFrontUsertype($user_id)." ".$frontend_username." changed his/her own password.";
                actionLog('users',$user_id,'update',$log_msg,'User',$user_id);
                $this->display_output('1','Password changed successfully.');
            }
        } else {
            $this->display_output('0','Current password is incorrect.');
        }
    }
 
    public function forgot_password()
    {   
        $email    =   $this->clear_input($this->input->post('email'));  
        if(empty($email)){
            $this->display_output('0','Please enter email/login ID.');
        } 
       
        if(!$user_data= $this->Common_model->getRecords('users','user_id,staging_id,status,is_deleted,email,mobile_auth_token',array('email'=>$email),'user_id DESC',true)) {  
            if(!$user_data= $this->Common_model->getRecords('users','user_id,staging_id,status,is_deleted,email, mobile_auth_token',array('staging_id'=>$email),'user_id DESC',true)) {  
                $this->display_output('0','Please enter registered email/login ID.');
            }
        }
        if($user_data) {
            if($user_data['is_deleted']==1) {
                $this->display_output('0','Your account has been deleted, Please contact us for more details.');
            } 
            if($user_data['status']=='Inactive') {
                $this->display_output('0','Your account has been inactive, Please contact us for more details.');
            } 
            if($user_data['status']=='Unverified') {
                $this->display_output('2','Your account is not verified.');
            } 
            $token = mt_rand(1000,9999);
            $reset_token_date = date("Y-m-d H:i:s");
            $to_email = $user_data['email']; 
            $from_email =  getNotificationEmail();  
            $subject = WEBSITE_NAME." : Reset Password"; 
            
            $data['name'] = ucwords($user_data['staging_id']); 
            
            $data['reset_token'] = $token;
            $body = $this->load->view('template/forgot_password', $data,TRUE);


            if($this->Common_model->sendEmail($to_email,$subject,$body,$from_email)) 
            {
                $reset_token_date = date("Y-m-d H:i:s");
                $where = array('user_id'=> $user_data['user_id']); 
                $update_data = array(
                    'reset_token'=>$token, 
                    'reset_token_date'=>$reset_token_date
                    );
                $this->Common_model->addEditRecords('users', $update_data, $where);
                $this->display_output('1','Reset password OTP sent on your email address, Please check your inbox and spam.');
            } else {
                $this->display_output('0','Some error occurred! Please try again.');
            } 
        }
            
    }

    public function reset_password() 
    {
        $email  =   $this->clear_input($this->input->post('email'));  
        $otp    =   $this->clear_input($this->input->post('reset_token'));  
        $new_password    =   $this->clear_input($this->input->post('new_password'));  

        if(empty($email)){
            $this->display_output('0','Please enter email/login ID.');
        }
        if(empty($otp)){
            $this->display_output('0','Please enter reset password token.');
        }  
        if(empty($new_password)){
            $this->display_output('0','Please enter new password.');
        }  
        if(!$user_data= $this->Common_model->getRecords('users','user_id,user_type,staging_id,status,is_deleted,mobile_auth_token,reset_token,reset_token_date,password',array('email'=>$email),'user_id DESC',true)) {  
            if(!$user_data= $this->Common_model->getRecords('users','user_id,status,is_deleted, mobile_auth_token,reset_token,reset_token_date,password',array('staging_id'=>$email),'user_id DESC',true)) {  
                $this->display_output('0','Please enter registered email/login ID.');
            }
        }

        if($user_data){
            if($user_data['reset_token'] != trim($otp)) {
                $this->display_output('0','Invalid reset password token.');
            }
            if($user_data['is_deleted']==0 && $user_data['status']=='Active'){

                $token_date = strtotime($user_data['reset_token_date']);
                $current_date = new DateTime("now", new DateTimeZone('UTC'));
                $current_date = $current_date->format('Y-m-d H:i:s');
                $diff=strtotime($current_date)-$token_date;
                if($diff > 3600) {
                    $this->display_output('0','Reset password token has been expired !!');
                } else {
                    if($user_data['password'] != base64_encode($new_password) )
                    {
                        $where = array('user_id'=> $user_data['user_id']); 
                        $update_data = array(
                                'password'=>base64_encode($new_password),
                                'reset_token'=>'', 
                                'reset_token_date'=>'',
                                'modified'=>date('Y-m-d H:i:s')
                            );

                        $this->Common_model->addEditRecords('users', $update_data, $where);
                        $log_msg = getUserType($user_data['user_type'])." ".$user_data['staging_id']." reset password.";
                        actionLog('users',$user_data['user_id'],'Reset Password',$log_msg,'User',$user_data['user_id']);
                        $this->display_output('1','Password reset successfully.');
                        
                    }else{
                        $this->display_output('0',"Password Can't be same as old password !!");
                    } 
                } 
            }else{
                $this->display_output('0','Your account has been deactivated!  Please contact administrator for details.');
            }
        }

    }

    public function get_user_detail() 
    {

        $user_id            = $this->clear_input($this->input->post('user_id')); 
        $other_user_id            = $this->clear_input($this->input->post('other_user_id')); 
        $user_type            = $this->clear_input($this->input->post('user_type')); 
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($user_type)) {
            $this->display_output('0','Please enter user type.');
        }
        if(!empty($other_user_id)){
            
            $user_id=$other_user_id;
        }else{
            $user_id=$user_id;
        }
     
        $details=$this->App_model->get_user_by_id($user_id,$user_type);
        if($user_type==2 || $user_type==3){
            $subscription=$this->App_model->get_current_subscription($user_id);
            $details['subscription_plan']=$subscription['subscription_plan'];
        }
        
        //echo $this->db->last_query();exit;
        if($details){
            $this->display_output('1','User Detail',array('details'=>$details));
        }else{
            $this->display_output('0','No detail found.');  
        }
    }


    public function update_user_profile() 
    {
        $user_id                = $this->clear_input($this->input->post('user_id')); 
        $user_type              = $this->clear_input($this->input->post('user_type')); 
        $fullname               =   $this->clear_input($this->input->post('fullname'));
        $email                  =   $this->clear_input($this->input->post('email'));
        $company_name           =   $this->clear_input($this->input->post('company_name'));
        $country                =   $this->clear_input($this->input->post('country'));
        $state                  =   $this->clear_input($this->input->post('state'));
        $city                   =   $this->clear_input($this->input->post('city'));
        $address                =   $this->clear_input($this->input->post('address'));
        $business_no            =   $this->clear_input($this->input->post('business_no'));
        $establishment_year     =   $this->clear_input($this->input->post('establishment_year'));
        $document_type          =   $this->clear_input($this->input->post('document_type'));
        $latitude               =   $this->clear_input($this->input->post('latitude'));
        $longitude              =   $this->clear_input($this->input->post('longitude'));

        $dob                    =   $this->clear_input($this->input->post('dob'));
        $gender                 =   $this->clear_input($this->input->post('gender'));
        $interests              =   $this->clear_input($this->input->post('interests'));
        $about_description      =   $this->clear_input($this->input->post('about_description'));
        $hobbies                =   $this->clear_input($this->input->post('hobbies'));
        $hometown               =   $this->clear_input($this->input->post('hometown'));
        $relationship_status    =   $this->clear_input($this->input->post('relationship_status'));
        $education    =   $this->clear_input($this->input->post('education'));
        $paypal_id    =   $this->clear_input($this->input->post('paypal_id'));
        $upgrade_sp    =   $this->clear_input($this->input->post('upgrade_sp'));
        if(!isset($upgrade_sp)){
            $upgrade_sp=0;
        }
        $this->App_model->check_user_status();
        $tableName ="users";
        if(empty($fullname)){
            $this->display_output('0','Please enter fullname.');
        }
        if(empty($user_type)){
            $this->display_output('0','Please enter user type.');
        } 
        if(empty($email)){
            $this->display_output('0','Please enter email.');
        }
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            $this->display_output('0','Please enter a valid email.');
        }

        if(!empty($email)){
            $where2 = array('email' => $email,'is_deleted'=>'0','user_id!='=>$user_id);
            if($this->Common_model->getRecords($tableName,'user_id',$where2,'',true)) {
                $this->display_output('0','Email already used.');
            }
        }
        if(empty($country)){
            $this->display_output('0','Please select country.');
        }if(empty($state)){
            $this->display_output('0','Please select state.');
        }
        if(empty($city)){
            $this->display_output('0','Please select city.');
        }
        if(empty($address)){
            $this->display_output('0','Please enter address.');
        } if(empty($paypal_id)){
            $this->display_output('0','Please enter paypal_id.');
        }
        if (filter_var($paypal_id, FILTER_VALIDATE_EMAIL) === false) {
            $this->display_output('0','Please enter a valid paypal id.');
        }
        $signup_data = array( 
            'fullname'=>$fullname,
            'email'=>$email,
            'dob'=>$dob,
            'gender'=>$gender,
            'interests'=>$interests,
            'about_description'=>$about_description,
            'hobbies'=>$hobbies,
            'hometown'=>$hometown,
            'relationship_status'=>$relationship_status,
            'education'=>$education,
            'paypal_id'=>$paypal_id,
            'modified'=> date("Y-m-d H:i:s"),
        );
        if($user_type==2 || $user_type==3 || $upgrade_sp==1){
            if(empty($company_name)){
                $this->display_output('0','Please enter company name.');
            }if(empty($address)){
                $this->display_output('0','Please enter address.');
            }if(empty($latitude)){
                $this->display_output('0','Please enter latitude.');
            }if(empty($longitude)){
                $this->display_output('0','Please enter longitude.');
            }if(empty($establishment_year)){
                $this->display_output('0','Please enter establishment year.');
            }if(empty($document_type)){
                $this->display_output('0','Please enter document type.');
            }
            $filepath="";
            $filerror="";
            $width="";$height="";
            if(isset($_FILES['document_proof']) && !empty($_FILES['document_proof']['name'])){
                if($_FILES['document_proof']['error']==0) {
                    $image_path = USER_DOCUMNET_PATH;
                    $allowed_types = '*';
                    $file='document_proof';
                    $height = 150;
                    $width = 150;
                    $responce = commonImageUpload($image_path,$allowed_types,$file,$width,$height);

                    if($responce['status']==0){
                        $upload_error = $responce['msg'];   
                        $filerror="1";
                    } else {
                        $filepath=$responce['image_path'];
                    }
                }
            }
            if($filerror!=''){
                $this->display_output('0',strip_tags($upload_error));
            }
            if(!empty($filepath)){
                $signup_data['document_proof']=$filepath;
                $signup_data['document_status']='Unverified';
            }
            
            $signup_data['company_name']=$company_name;
            $signup_data['establishment_year']=$establishment_year;
            $signup_data['business_no']=$business_no;
            $past_doc=$this->Common_model->getFieldValue('users','document_type',array('user_id'=>$user_id),'',true);
            if($past_doc!=$document_type){
                $signup_data['document_type']=$document_type;
                $signup_data['document_status']='Unverified';
            }
        }if($upgrade_sp==1 && $user_type==1){
            $signup_data['user_type']='3';
        }
        $imgfilepath="";
        $imgfilerror="";
        if(isset($_FILES['profile_picture']) && !empty($_FILES['profile_picture']['name'])){
            if($_FILES['profile_picture']['error']==0) {
                $image_path = USER_PROFILE_PATH;
                $allowed_types = '*';
                $file='profile_picture';
                $height = 150;
                $width = 150;
                $imgresponce = commonImageUpload($image_path,$allowed_types,$file,$width,$height);

                if($imgresponce['status']==0){
                    $upload_error = $imgresponce['msg'];   
                    $imgfilerror="1";
                } else {
                    $imgfilepath=$imgresponce['image_path'];
                }
            }
        }
        if($imgfilerror!=''){
            $this->display_output('0',strip_tags($upload_error));
        }
        if(!empty($imgfilepath)){
            $signup_data['profile_picture']=$imgfilepath;
        }
        $this->db->trans_begin();
        if($this->Common_model->addEditRecords('users',$signup_data,array('user_id'=>$user_id))){
            $address_data = array(
                'country'=>$country,
                'state'=>$state,
                'city'=>$city,
                'address'=>$address,
                'latitude'=>$latitude,
                'longitude'=>$longitude,
                'status'=>'Primary',
                'modified'=> date("Y-m-d H:i:s"),
            );
            $this->Common_model->addEditRecords('user_address',$address_data,array('user_id'=>$user_id,'status'=>'Primary'));
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $this->display_output('0','Some error occured! Please try again.'); 
            } else {
                $frontend_username = getFrontUsername($user_id);
                if($upgrade_sp==1){
                    $log_msg = getFrontUsertype($user_id)." ".$frontend_username." updated his/her own profile as celebrant SP.";
                }else{
                    $log_msg = getFrontUsertype($user_id)." ".$frontend_username." changed his/her own profile.";
                }
                
                actionLog('users',$user_id,'update',$log_msg,'User',$user_id);
                $this->db->trans_commit();
                $this->display_output('1','Profile updated successfully.');
            }
        }else{
            $this->display_output('0','Some error occured! Please try again.');  
        }
    }

    public function get_countries() 
    {
        //$this->App_model->check_user_status();
        $list=$this->Common_model->getRecords('countries', 'id,name', array('id'=>231), 'name', false);
        if($list){
            $this->display_output('1','Country list',array('list'=>$list));
        }else{
            $this->display_output('0','No data available at this time.');  
        }

    }
    public function get_states() 
    {
       // $this->App_model->check_user_status();
        $country_id   = $this->clear_input($this->input->post('country_id'));
        if(empty($country_id)){
            $this->display_output('0','Please enter country id.');  
        }
        $list=$this->Common_model->getRecords('states', 'id,name', array('country_id'=>$country_id), 'name', false);
        if($list){
            $this->display_output('1','State list',array('list'=>$list));
        }else{
            $this->display_output('0','No data available at this time.');  
        }

    }
    public function get_cities() 
    {
       // $this->App_model->check_user_status();
        $state_id   = $this->clear_input($this->input->post('state_id'));
        if(empty($state_id)){
            $this->display_output('0','Please select state id.');  
        }
        $list=$this->Common_model->getRecords('cities', 'id,name', array('state_id'=>$state_id), 'name', false);
        if($list){
            $this->display_output('1','City list',array('list'=>$list));
        }else{
            $this->display_output('0','No data available at this time.');  
        }

    }
    public function get_service_category_list() 
    {
        $this->App_model->check_user_status();
        $this->App_model->check_user_document_status();
        $list=$this->Common_model->getRecords('service_category', 'id,title', array('status'=>'Active','is_deleted'=>0), 'title', false);
        if($list){
            $this->display_output('1','Service category list',array('list'=>$list));
        }else{
            $this->display_output('0','No data available at this time.');  
        }

    }
    public function get_measurement_unit_list() 
    {
        $this->App_model->check_user_status();
        $list=$this->Common_model->getRecords('measurement_unit', 'id,title', array('status'=>'Active','is_deleted'=>0), 'title', false);
        if($list){
            $this->display_output('1','Measurement unit list',array('list'=>$list));
        }else{
            $this->display_output('0','No data available at this time.');  
        }

    }
    ////////////////////////////////////service provider section/////////////////////////////////
    public function create_services() 
    {
        $user_id            = $this->clear_input($this->input->post('user_id')); 
        $service_title            = $this->clear_input($this->input->post('service_title')); 
        $category_id        = $this->clear_input($this->input->post('category_id')); 
        $description        = $this->clear_input($this->input->post('description'));
       /* $address            = $this->clear_input($this->input->post('address'));//after discussion remove it & all the location added in profile
        $latitude         = $this->clear_input($this->input->post('latitude'));
        $longitude        = $this->clear_input($this->input->post('longitude'));*/
        $item        = $this->clear_input($this->input->post('item'));
        $this->App_model->check_user_status();
        $this->App_model->check_user_document_status();
        $item_arr=json_decode($item);
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        } 
        if(empty($service_title)) {
            $this->display_output('0','Please enter service title.');
        } 
        if(empty($category_id)){
            $this->display_output('0','Please enter category id.');
        } 
        if(empty($description)){
            $this->display_output('0','Please enter description.');
        }
        if(empty($item)){
            $this->display_output('0','Please enter item.');
        }
       /* if(empty($address)){
            $this->display_output('0','Please enter address.');
        }
        if(empty($latitude)){
            $this->display_output('0','Please enter correct latitude.');
        }
        if(empty($longitude)){
            $this->display_output('0','Please enter correct longitude.');
        }*/
        /*$where2 = array('user_id' => $user_id,'category_id'=>$category_id,'is_deleted'=>'0');
        if($this->Common_model->getRecords('services','id',$where2,'',true)) {
            $this->display_output('0','You already created a service for this category.');
        }*/
       
        
        $service_data=array(   
            'user_id'=>$user_id,
            'service_title'=>$service_title,
            'category_id'=>$category_id,
            'description'=>$description,
            'created'=>date('Y-m-d H:i:s')
        );
        $width="";
        $height="";
        if(isset($_FILES['img_1']['name']) && $_FILES['img_1']['name']!="" && !empty($_FILES['img_1']['name']))
        {
            $file_ext_ary= explode(".",$_FILES['img_1']['name']);
            $file_ext= end($file_ext_ary);
            $filename = rand().time().'_'.'01'.'.'.$file_ext;
            $file='img_1';
            $image_path = SERVICE_BANNER_PATH;
            $allowed_types = '*';
            $file='img_1';
            if($_FILES['img_1']['error']==0) {
                $responce_img_1 = multiImageUpload($image_path,$allowed_types,$file,$width,$height,$filename);
                if($responce_img_1['status']==0){
                    $this->display_output('0','Some error occured in upload of image 1.');
                }else{
                    $service_data['img1']=$responce_img_1['image_path'];
                }
            }
        }else{
            $this->display_output('0','Please insert an image.');
        }
        if(isset($_FILES['img_2']['name']) && $_FILES['img_2']['name']!="" && !empty($_FILES['img_2']['name']))
        {
            $file_ext_ary= explode(".",$_FILES['img_2']['name']);
            $file_ext= end($file_ext_ary);
            $filename = rand().time().'_'.'02'.'.'.$file_ext;
            $file='img_2';
            $image_path = SERVICE_BANNER_PATH;
            $allowed_types = '*';
            $file='img_2';
          
            if($_FILES['img_2']['error']==0) {
                $responce_img_2 = multiImageUpload($image_path,$allowed_types,$file,$width,$height,$filename);
                if($responce_img_2['status']==0){
                     $this->display_output('0','Some error occured in upload of image 2.');  
                }
                else{
                    $service_data['img2']=$responce_img_2['image_path'];
                }
            }
        }
        if(isset($_FILES['img_3']['name']) &&$_FILES['img_3']['name']!="" && !empty($_FILES['img_3']['name']))
        {
            $file_ext_ary= explode(".",$_FILES['img_3']['name']);
            $file_ext= end($file_ext_ary);
            $filename = rand().time().'_'.'03'.'.'.$file_ext;
            $file='img_3';
            $image_path = SERVICE_BANNER_PATH;
            $allowed_types = '*';
            $file='img_3';
          
            if($_FILES['img_3']['error']==0) {
                $responce_img_3 = multiImageUpload($image_path,$allowed_types,$file,$width,$height,$filename);
                if($responce_img_3['status']==0){
                    $this->display_output('0','Some error occured in upload of image 3.');
                }else{
                    $service_data['img3']=$responce_img_3['image_path'];
                }
            }
        }
        $this->db->trans_begin();
        if($last_id = $this->Common_model->addEditRecords('services',$service_data)){
            for($i=0;$i<count($item_arr);$i++){
                $insert_array=array('item_name' => $item_arr[$i]->item,
                                    'price' =>$item_arr[$i]->price,
                                    'unit' =>$item_arr[$i]->unit,
                                    'qty' =>$item_arr[$i]->qty,
                                    'service_id'=>$last_id,
                                    'created'=>date('Y-m-d H:i:s'));
                 $this->Common_model->addEditRecords('services_item',$insert_array);
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $this->display_output('0','Something went wrong. Please try again.'); 
        } else {
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getFrontUsertype($user_id)." ".$frontend_username."  has created a new service '".$service_title."'.";
            actionLog('services',$last_id,'add',$log_msg,'User',$user_id);
            $this->db->trans_commit();
            $this->display_output('1','Service created successfully.');
        }
    }
    public function edit_services() 
    {
        $service_id         = $this->clear_input($this->input->post('service_id')); 
        $user_id            = $this->clear_input($this->input->post('user_id')); 
        $service_title      = $this->clear_input($this->input->post('service_title')); 
        $category_id        = $this->clear_input($this->input->post('category_id')); 
        $description        = $this->clear_input($this->input->post('description'));
       /* $address            = $this->clear_input($this->input->post('address'));//after discussion remove it & all the location added in profile
        $latitude         = $this->clear_input($this->input->post('latitude'));
        $longitude        = $this->clear_input($this->input->post('longitude'));*/
        $item        = $this->clear_input($this->input->post('item'));
        $item_arr=json_decode($item);
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        } 
        if(empty($service_id)) {
            $this->display_output('0','Please enter service id.');
        } 
        if(empty($service_title)) {
            $this->display_output('0','Please enter service title.');
        } 
        if(empty($category_id)){
            $this->display_output('0','Please enter category id.');
        } 
        if(empty($description)){
            $this->display_output('0','Please enter description.');
        }
        /*if(empty($item)){
            $this->display_output('0','Please enter item.');
        }*/
       /* if(empty($address)){
            $this->display_output('0','Please enter address.');
        }
        if(empty($latitude)){
            $this->display_output('0','Please enter correct latitude.');
        }
        if(empty($longitude)){
            $this->display_output('0','Please enter correct longitude.');
        }*/
        /*$where2 = array('user_id' => $user_id,'category_id'=>$category_id,'is_deleted'=>'0');
        if($this->Common_model->getRecords('services','id',$where2,'',true)) {
            $this->display_output('0','You already created a service for this category.');
        }*/
       
        $this->App_model->check_user_status();
        $this->App_model->check_user_document_status();
       
        $service_data=array(   
           // 'user_id'=>$user_id,
            'service_title'=>$service_title,
            'category_id'=>$category_id,
            'description'=>$description,
            'modified'=>date('Y-m-d H:i:s')
        );
        $width="";$height="";
        if(isset($_FILES['img_1']['name']) && $_FILES['img_1']['name']!="" && !empty($_FILES['img_1']['name']))
        {
            $file_ext_ary= explode(".",$_FILES['img_1']['name']);
            $file_ext= end($file_ext_ary);
            $filename = rand().time().'_'.'01'.'.'.$file_ext;
            $file='img_1';
            $image_path = SERVICE_BANNER_PATH;
            $allowed_types = '*';
            $file='img_1';
            if($_FILES['img_1']['error']==0) {
                $responce_img_1 = multiImageUpload($image_path,$allowed_types,$file,$width,$height,$filename);
                if($responce_img_1['status']==0){
                    $this->display_output('0','Some error occured in upload of image 1.');
                }else{
                    $service_data['img1']=$responce_img_1['image_path'];
                }
            }
        }
        if(isset($_FILES['img_2']['name']) && $_FILES['img_2']['name']!="" && !empty($_FILES['img_2']['name']))
        {
            $file_ext_ary= explode(".",$_FILES['img_2']['name']);
            $file_ext= end($file_ext_ary);
            $filename = rand().time().'_'.'02'.'.'.$file_ext;
            $file='img_2';
            $image_path = SERVICE_BANNER_PATH;
            $allowed_types = '*';
            $file='img_2';
          
            if($_FILES['img_2']['error']==0) {
                $responce_img_2 = multiImageUpload($image_path,$allowed_types,$file,$width,$height,$filename);
                if($responce_img_2['status']==0){
                     $this->display_output('0','Some error occured in upload of image 2.');  
                }
                else{
                    $service_data['img2']=$responce_img_2['image_path'];
                }
            }
        }
        if(isset($_FILES['img_3']['name']) && $_FILES['img_3']['name']!="" && !empty($_FILES['img_3']['name']))
        {
            $file_ext_ary= explode(".",$_FILES['img_3']['name']);
            $file_ext= end($file_ext_ary);
            $filename = rand().time().'_'.'03'.'.'.$file_ext;
            $file='img_3';
            $image_path = SERVICE_BANNER_PATH;
            $allowed_types = '*';
            $file='img_3';
          
            if($_FILES['img_3']['error']==0) {
                $responce_img_3 = multiImageUpload($image_path,$allowed_types,$file,$width,$height,$filename);
                if($responce_img_3['status']==0){
                    $this->display_output('0','Some error occured in upload of image 3.');
                }else{
                    $service_data['img3']=$responce_img_3['image_path'];
                }
            }
        }
        $this->db->trans_begin();
        if($last_id = $this->Common_model->addEditRecords('services',$service_data,array('id'=>$service_id))){
            if(isset($item_arr) && !empty($item_arr)){
                 $this->Common_model->addEditRecords('services_item',array('is_deleted'=>1,'modified'=>date('Y-m-d H:i:s')),array('service_id'=>$service_id));
                for($i=0;$i<count($item_arr);$i++){
                    $insert_array=array('item_name' => $item_arr[$i]->item,
                                        'price' =>$item_arr[$i]->price,
                                        'unit' =>$item_arr[$i]->unit,
                                        'qty' =>$item_arr[$i]->qty,
                                        'service_id'=>$service_id,
                                        'created'=>date('Y-m-d H:i:s'));
                     $this->Common_model->addEditRecords('services_item',$insert_array);
                }
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $this->display_output('0','Something went wrong. Please try again.'); 
        } else {
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getFrontUsertype($user_id)." ".$frontend_username."  has updated a service '".$service_title."'.";
            actionLog('services',$service_id,'update',$log_msg,'User',$user_id);
            $this->db->trans_commit();
            $this->display_output('1','Service Updated successfully.');
        }
    }
    
    public function sp_service_list(){
        $this->App_model->check_user_status();
        $this->App_model->check_user_document_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $other_user_id  = $this->clear_input($this->input->post('other_user_id'));
        if(!empty($other_user_id)){
            $user_id = $other_user_id;
        }
        $list=$this->App_model->get_sp_service_list($user_id);
        if(!empty($list)) {
            $this->display_output('1','service list', array('details'=>$list));
        } else {
            $this->display_output('0','Service not found');
        }
    }
    public function sp_service_details(){
        $this->App_model->check_user_status();
        $this->App_model->check_user_document_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $service_id  = $this->clear_input($this->input->post('service_id'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($service_id)) {
            $this->display_output('0','Please enter service id.');
        }
        $detail=$this->App_model->sp_service_details($service_id);
        if(!empty($detail)) {
            $detail['is_follow']=checkFollowStatus($detail['sp_id'],$user_id);
            $items=$this->App_model->service_item_details($service_id);
            $response = array('service_details'=>$detail,'service_item'=>$items);
            $this->display_output('1','service detail', array('details'=>$response));
        } else {
            $this->display_output('0','Service detail not found');
        }
    }
    public function sp_service_delete(){
        $this->App_model->check_user_status();
        $this->App_model->check_user_document_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $service_id  = $this->clear_input($this->input->post('service_id'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($service_id)) {
            $this->display_output('0','Please enter service id.');
        }
        $service_record=$this->Common_model->getRecords('services', 'id,user_id,service_title', array('id'=>$service_id), '', true);
        if($service_record['user_id']!=$user_id){
            $this->display_output('0','You are not authorize to delete this service.');
        }
        if($order_record=$this->Common_model->getRecords('orders', 'id,order_status', array('service_id'=>$service_id), '', true)){
            if($order_record['order_status']=='Pending' || $order_record['order_status']=='Confirmed'){
                $this->display_output('0','This service is associated with running order.');
            }
        }

        $update_array=array('is_deleted' => 1,'modified' =>date('Y-m-d H:i:s'));
        $this->db->trans_begin();
        $this->Common_model->addEditRecords('services',$update_array, array('id'=>$service_id));
        $this->Common_model->addEditRecords('services_item',$update_array, array('service_id'=>$service_id));
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $this->display_output('0','Some error occured! Please try again.'); 
        } else {
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getFrontUsertype($user_id)." ".$frontend_username." deleted service '".$service_record['service_title']."'.";
            actionLog('services',$service_id,'delete',$log_msg,'User',$user_id);
            $this->db->trans_commit();
            $this->display_output('1','Service deleted successfully.'); 
        }

    }

    public function get_offer_service_list() 
    {
        $user_id            = $this->clear_input($this->input->post('user_id'));
        $this->App_model->check_user_status();
        $this->App_model->check_user_document_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $list=$this->Common_model->getRecords('services', 'id,service_title', array('status'=>'Active','is_deleted'=>0,'user_id'=>$user_id), 'service_title', false);
        if($list){
            $this->display_output('1','Service category list',array('list'=>$list));
        }else{
            $this->display_output('0','No data available at this time.');  
        }

    }
    public function create_offers() 
    {
        $user_id            = $this->clear_input($this->input->post('user_id')); 
        $offer_name         = $this->clear_input($this->input->post('offer_name')); 
        $service_id         = $this->clear_input($this->input->post('service_id')); 
        $description        = $this->clear_input($this->input->post('description'));
        $start_date         = $this->clear_input($this->input->post('start_date'));
        $end_date           = $this->clear_input($this->input->post('end_date'));
        $discount           = $this->clear_input($this->input->post('discount'));
        $timezone           = $this->clear_input($this->input->post('timezone'));
        $this->App_model->check_user_status();
        $this->App_model->check_user_document_status();
        
        
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($offer_name)){
            $this->display_output('0','Please enter offer name.');
        }  
        if(empty($service_id)){
            $this->display_output('0','Please enter service id.');
        } 
        /*if(empty($description)){
            $this->display_output('0','Please enter description.');
        }*/
        if(empty($start_date)){
            $this->display_output('0','Please enter start date.');
        }
        if(empty($end_date)){
            $this->display_output('0','Please enter end date.');
        }if(empty($discount)){
            $this->display_output('0','Please enter discount.');
        }if(empty($timezone)){
            $this->display_output('0','Please enter time zone.');
        }
        /*$st_date=convertLocalTimezoneToGMT($start_date,$timezone,$showTime=true);
        $end_date=convertLocalTimezoneToGMT($end_date,$timezone,$showTime=true);*/
        $st_date=$start_date;
        $end_date=$end_date;
        $offer_st_date=date('Y-m-d H:i:s',strtotime($st_date));
        $offer_end_date= date('Y-m-d H:i:s',strtotime($end_date));
        if(strtotime($st_date)>strtotime($end_date)){
            $this->display_output('0','Offer end date must be greater than start date.');
        }
        if($this->App_model->check_offer($service_id,$offer_st_date,$offer_end_date)){
            $this->display_output('0','You already added offer for this time duration.');
        }
        if(!$service_records=$this->Common_model->getRecords('services','id',array('id'=>$service_id,'is_deleted'=>0,'status'=>'Active','user_id'=>$user_id),'',true)){
            $this->display_output('0','You are not authorize to create offer on this service.');
        }
        $offer_data=array(   
            'user_id'=>$user_id,
            'offer_name'=>$offer_name,
            'service_id'=>$service_id,
            'description'=>$description,
            'start_date'=>$offer_st_date,
            'end_date'=>$offer_end_date,
            'discount'=>$discount,
            //'img1'=>$responce_img_1['image_path'],
            'created'=>date('Y-m-d H:i:s')
        );
        $width="";$height="";
        if(isset($_FILES['img_1']['name']) && $_FILES['img_1']['name']!="" && !empty($_FILES['img_1']['name']))
        {
            $file_ext_ary= explode(".",$_FILES['img_1']['name']);
            $file_ext= end($file_ext_ary);
            $filename = rand().time().'_'.'01'.'.'.$file_ext;
            $file='img_1';
            $image_path = OFFER_BANNER_PATH;
            $allowed_types = '*';
            $file='img_1';
            if($_FILES['img_1']['error']==0) {
                $responce_img_1 = multiImageUpload($image_path,$allowed_types,$file,$width,$height,$filename);
                 $offer_data['img1']=$responce_img_1['image_path'];
                //unlink($image_path);
                if($responce_img_1['status']==0){
                    $this->display_output('0','Some error occured in upload of image.');
                }
            }
        }/*else{
            $this->display_output('0','Please insert an image.');
        }*/
        
        
        if($last_id = $this->Common_model->addEditRecords('offer',$offer_data)){
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getFrontUsertype($user_id)." ".$frontend_username."  has created a new offer '".$offer_name."'.";
            actionLog('offer',$last_id,'add',$log_msg,'User',$user_id);
            $this->display_output('1','Offer created successfully.');
        }else{
            $this->display_output('0','Something went wrong. Please try again.'); 
        }
        
    }
    public function edit_offers() 
    {
        $offer_id           = $this->clear_input($this->input->post('offer_id')); 
        $user_id            = $this->clear_input($this->input->post('user_id')); 
        $offer_name         = $this->clear_input($this->input->post('offer_name')); 
        $service_id         = $this->clear_input($this->input->post('service_id')); 
        $description        = $this->clear_input($this->input->post('description'));
        $start_date         = $this->clear_input($this->input->post('start_date'));
        $end_date           = $this->clear_input($this->input->post('end_date'));
        $discount           = $this->clear_input($this->input->post('discount'));
        $timezone           = $this->clear_input($this->input->post('timezone'));
        

        if(empty($offer_id)) {
            $this->display_output('0','Please enter offer id.');
        }
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($offer_name)){
            $this->display_output('0','Please enter offer name.');
        }  
        if(empty($service_id)){
            $this->display_output('0','Please enter service id.');
        } 
        /*if(empty($description)){
            $this->display_output('0','Please enter description.');
        }*/
        if(empty($start_date)){
            $this->display_output('0','Please enter start date.');
        }
        if(empty($end_date)){
            $this->display_output('0','Please enter end date.');
        }if(empty($discount)){
            $this->display_output('0','Please enter discount.');
        }if(empty($timezone)){
            $this->display_output('0','Please enter time zone.');
        }
        $this->App_model->check_user_status();
        /*$st_date=convertLocalTimezoneToGMT($start_date,$timezone,$showTime=true);
        $end_date=convertLocalTimezoneToGMT($end_date,$timezone,$showTime=true);*/
        $st_date=$start_date;
        $end_date=$end_date;
        $offer_st_date=date('Y-m-d H:i:s',strtotime($st_date));
        $offer_end_date= date('Y-m-d H:i:s',strtotime($end_date));
        if(strtotime($st_date)>strtotime($end_date)){
            $this->display_output('0','Offer end date must be greater than start date.');
        }
        if(!$service_records=$this->Common_model->getRecords('services','id',array('id'=>$service_id,'is_deleted'=>0,'status'=>'Active','user_id'=>$user_id),'',true)){
            $this->display_output('0','You are not authorize to edit offer on this service.');
        }
        if($this->App_model->check_offer($service_id,$offer_st_date,$offer_end_date,$offer_id)){
            $this->display_output('0','You already added offer for this time duration.');
        }
        $offer_data=array(   
            'user_id'=>$user_id,
            'offer_name'=>$offer_name,
            'service_id'=>$service_id,
            'description'=>$description,
            'start_date'=>$offer_st_date,
            'end_date'=>$offer_end_date,
            'discount'=>$discount,
            'modified'=>date('Y-m-d H:i:s')
        );
        $width="";$height="";
        if(isset($_FILES['img_1']['name']) && $_FILES['img_1']['name']!="" && !empty($_FILES['img_1']['name']))
        {
            $file_ext_ary= explode(".",$_FILES['img_1']['name']);
            $file_ext= end($file_ext_ary);
            $filename = rand().time().'_'.'01'.'.'.$file_ext;
            $file='img_1';
            $image_path = OFFER_BANNER_PATH;
            $allowed_types = '*';
            $file='img_1';
            if($_FILES['img_1']['error']==0) {
                $responce_img_1 = multiImageUpload($image_path,$allowed_types,$file,$width,$height,$filename);
                if($responce_img_1['status']==0){
                    $this->display_output('0','Some error occured in upload of image.');
                }else{
                    $offer_data['img1']=$responce_img_1['image_path'];
                    $record=$this->Common_model->getRecords('offer', 'img1', array('id'=>$offer_id), '', true);
                }
            }
        }
        
        if($last_id = $this->Common_model->addEditRecords('offer',$offer_data,array('id'=>$offer_id))){
            if(isset($record['img1']) && !empty($record['img1'])){
                if(file_exists($record['img1'])){
                   unlink($record['img1']);
                }
            }
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getFrontUsertype($user_id)." ".$frontend_username."  has updated a offer '".$offer_name."'.";
            actionLog('offer',$offer_id,'update',$log_msg,'User',$user_id);
            $this->display_output('1','Offer updated successfully.');
        }else{
            $this->display_output('0','Something went wrong. Please try again.'); 
        }
        
    }
    public function sp_offers_list(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $this->App_model->check_user_status();
        $this->App_model->check_user_document_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $list=$this->App_model->get_sp_offer_list($user_id);
        if(!empty($list)) {
            $this->display_output('1','Offer list', array('details'=>$list));
        } else {
            $this->display_output('0','Offers not found');
        }
    }
    public function sp_offer_details(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $offer_id  = $this->clear_input($this->input->post('offer_id'));
        $this->App_model->check_user_status();
        $this->App_model->check_user_document_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($offer_id)) {
            $this->display_output('0','Please enter offer id.');
        }
        $detail=$this->App_model->sp_offer_details($offer_id);
        if(!empty($detail)) {
            $this->display_output('1','Offer detail', array('details'=>$detail));
        } else {
            $this->display_output('0','Offer detail not found');
        }
    }
    public function sp_offer_delete(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $offer_id  = $this->clear_input($this->input->post('offer_id'));
        $this->App_model->check_user_status();
        $this->App_model->check_user_document_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($offer_id)) {
            $this->display_output('0','Please enter offer id.');
        }
        $offer_record=$this->Common_model->getRecords('offer', 'offer_name', array('id'=>$offer_id), '', true);
        if($this->Common_model->addEditRecords('offer',array('is_deleted'=>1,'modified'=>date('Y-m-d H:i:s')),array('id'=>$offer_id))){
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getFrontUsertype($user_id)." ".$frontend_username." deleted offer '".$offer_record['offer_name']."'.";
            actionLog('offer',$offer_id,'delete',$log_msg,'User',$user_id);
            $this->display_output('1','Offer deleted successfully.');
        }else{
            $this->display_output('0','Some error occured! Please try again.');
        }
    }
    
    public function sp_accept_reject_booking(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $event_id  = $this->clear_input($this->input->post('event_id'));
        $order_status_id  = $this->clear_input($this->input->post('order_status_id'));
        $status  = $this->clear_input($this->input->post('status'));
        $sp_order_cancel_date  = $this->clear_input($this->input->post('order_cancel_date'));
        $this->App_model->check_user_status();
        $this->App_model->check_user_document_status();
        $this->App_model->check_subscription_status();

        $current_date=date('Y-m-d H:i:s');
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($event_id)) {
            $this->display_output('0','Please enter event id.');
        }
        if(empty($order_status_id)) {
            $this->display_output('0','Please enter order status id.');
        }
        if(empty($status)) {
            $this->display_output('0','Please enter status.');
        }
        if($status!='Accepted' &&  $status!='Rejected'){
            $this->display_output('0','Status either Accepted or Rejected.');
        }
        if(!$event_record = $this->Common_model->getRecords('events','id,event_title,user_id,is_canceled,event_date,sp_id',array('id'=>$event_id,'status'=>'Active','is_deleted'=>0),'',true)){
            $this->display_output('0','Event isn\'t available.');
        }
        if($event_record['event_date']<$current_date){
            $this->display_output('0','Event time has been passed.');
        }
        if($event_record['is_canceled']==1){
            $this->display_output('0','Event has been canceled.');
        }
        if($event_record['sp_id']!=$user_id){
            $this->display_output('0','You are not authorized to accept this order.');
        }

        $order_record = $this->Common_model->getRecords('orders','id,order_number,order_status',array('event_id'=>$event_id),'id desc',true);
        if($order_record['order_status']!='Pending'){
            $this->display_output('0','Order has been '.ucwords($order_record['order_status']).'.');
        }
        if($status=='Accepted'){
            $update_status='Accepted';
            if(empty($sp_order_cancel_date)) {
                $this->display_output('0','Please enter order cancel date.');
            }
        }else{$update_status='Rejected';}
        $order_history_record = $this->Common_model->getRecords('order_status_history','status',array('id'=>$order_status_id),'',true);
        if($order_history_record['status']!='Pending'){
            $this->display_output('0','Order already '.ucwords($order_history_record['status']).'.');
        }
        $this->db->trans_begin();
        if($this->Common_model->addEditRecords('order_status_history',array('status'=>$update_status,'modified'=>date('Y-m-d H:i:s')), array('id'=>$order_status_id))) {
            $company_name=getUserInfo($user_id,'users','user_id','fullname');
            if($status=='Accepted'){
                $this->Common_model->addEditRecords('orders',array('sp_order_cancel_date'=>$sp_order_cancel_date,'order_status'=>'Confirmed','modified'=>date('Y-m-d H:i:s')), array('id'=>$order_record['id']));
                $subscription_data = $this->Common_model->getRecords('user_subscription_history','id,total_remaining_order,plan_orders',array('user_id'=>$user_id),'id desc',true);
                if($subscription_data['plan_orders']!='Unlimited'){
                   $substract= $subscription_data['total_remaining_order']-1;
                   $this->Common_model->addEditRecords('user_subscription_history',array('total_remaining_order'=>$substract,'modified'=>date('Y-m-d H:i:s')), array('id'=>$subscription_data['id']));
                }
                spFriends($event_record['user_id'],$event_record['sp_id']);
                $title=ucwords($company_name).' accepted your order request.';
                $content=ucwords($company_name).' accepted your order request '.$order_record['order_number'].' for '.$event_record['event_title'].'.';
                $notdata['type']='order_request_accepted';
                $this->Common_model->push_notification_send($event_record['user_id'],$notdata,$order_record['id'],$title,$content,'',$user_id);
            }else{
                $this->Front_common_model->cancel_by_sp_payment($user_id,$order_record['id']);
                $title=ucwords($company_name).' rejected your order request.';
                $content=ucwords($company_name).' rejected your order request'.$order_record['order_number'].' for '.$event_record['event_title'].' Your Payment will be back in your account soon.';
                $notdata['type']='order_request_rejected';
                $this->Common_model->push_notification_send($event_record['user_id'],$notdata,$order_record['id'],$title,$content,'',$user_id);
            }
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $this->display_output('0','Something went wrong. Please try again.'); 
            } else {
                $this->db->trans_commit();
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has ".lcfirst($status)." order of an event '".$event_record['event_title']."'.";
                actionLog('events',$event_id,'Event '.lcfirst($status).'',$log_msg,'User',$user_id);
                $this->display_output('1','Order request '.lcfirst($status).'.');
            }
        }else{
             $this->display_output('0','Something went wrong. Please try again.');
        }
    }
    public function sp_canceled_booking(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $order_id  = $this->clear_input($this->input->post('order_id'));
        $today_date=date('Y-m-d');
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($order_id)) {
            $this->display_output('0','Please enter order id.');
        }

        $this->App_model->check_user_status();
        $this->App_model->check_user_document_status();
        $this->App_model->check_subscription_status();
        $check_order_record = $this->Common_model->getRecords('orders','event_id,order_status',array('id'=>$order_id),'',true);
        if($check_order_record['order_status']=='Canceled'){
            $this->display_output('0','Order status already canceled.');
        }
        $check_event_record = $this->Common_model->getRecords('events',"if(events.is_completed=1,'Completed',if(events.is_canceled=1,'Canceled',if(events.event_date>'".$today_date."','Not started','On Going'))) as event_status,event_title",array('id'=>$check_order_record['event_id']),'',true);

        if($check_event_record['event_status']=='On Going'){
            $this->display_output('0','Event is on going. You cannot be canceled it.');
        }
        if($check_event_record['event_status']=='Canceled'){
            $this->display_output('0','Event already canceled.');
        }
        $result=$this->Front_common_model->cancel_by_sp_payment($user_id,$order_id);
        if($result==1){
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has canceled order of an event '".$check_event_record['event_title']."'.";
            actionLog('events',$check_order_record['event_id'],'Event Canceled',$log_msg,'User',$user_id);            
            $this->display_output('1','Booking canceled successfully.');
        }else{
            $this->display_output('0','Something went wrong. Please try again.');
        }
    }
    public function sp_booking_history_list(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $status  = $this->clear_input($this->input->post('status'));
        $this->App_model->check_user_status();
        $this->App_model->check_user_document_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $list=$this->App_model->sp_booking_history_list($user_id);
        //echo $this->db->last_query();exit;
        if(!empty($list)) {
            $this->display_output('1','Booking history list', array('details'=>$list));
        } else {
            $this->display_output('0','Booking history not found.');
        }
        
    }
    public function sp_booking_history_details(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $user_type  = $this->clear_input($this->input->post('user_type'));
        $booking_history_id  = $this->clear_input($this->input->post('booking_history_id'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($user_type)) {
            $this->display_output('0','Please enter user type.');
        }
        if(empty($booking_history_id)) {
            $this->display_output('0','Please enter booking history id.');
        }
        $this->App_model->check_user_status();
        $this->App_model->check_user_document_status();
        $details=$this->App_model->sp_booking_history_details($user_id,$user_type,$booking_history_id);
        $items = $this->Common_model->getRecords('order_details','item_name,price,qty,unit,customer_qty,total_price,sell_price,tax,discount',array('order_id'=>$details['order_id'],'is_deleted'=>0),'',false);
        $response=array('booking_details'=>$details,'items'=>$items,'selling_total'=>$details['total_amount']);
        //echo $this->db->last_query();
        if(!empty($details)) {
            $this->display_output('1','Booking history detail', array('details'=>$response));
        } else {
            $this->display_output('0','Booking history detail not found.');
        }
        
    }
    ////////////////////////////////////Service  provider subscription/////////////////////////////////
    public function sp_subscription_list(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $this->App_model->check_user_status();
        $this->App_model->check_user_document_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $list=$this->Common_model->getRecords('subscriptions','id as plan_id,time_type,time_value,total_days,amount,title,description,orders',array('is_deleted'=>0,'status'=>'Active'),'',false);
        if(!empty($list)) {
            $this->display_output('1','Subscription list', array('details'=>$list));
        } else {
            $this->display_output('0','Subscription not found.');
        }
    }

    public function sp_subscribe_plan(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $plan_id  = $this->clear_input($this->input->post('plan_id'));
        $transaction_id  = $this->clear_input($this->input->post('transaction_id'));
        //$transaction_fees  = $this->clear_input($this->input->post('transaction_fees'));
        $sp_user_details=getNameEmailAddress($user_id);
        if($sp_user_details['user_type']==1){
            $this->display_output('0','You are not authorize to take subscription plan.');
        }
        $this->App_model->check_user_status();
        $this->App_model->check_user_document_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($plan_id)) {
            $this->display_output('0','Please enter plan id.');
        }
        if(empty($transaction_id)) {
            $this->display_output('0','Please enter transaction id.');
        }
        /*if(empty($transaction_fees)) {
            $this->display_output('0','Please enter transaction fees.');
        }*/
        if(!$record=$this->Common_model->getRecords('subscriptions','id as plan_id,time_type,time_value,total_days,amount,title,description,orders',array('id'=>$plan_id),'',true)){
            $this->display_output('0','Plan isn\'t available.');
        }
        $start_date=date('Y-m-d');
        $end_date= date('Y-m-d', strtotime($start_date. ' + '.$record['total_days'].' days'));
        $current_time=date('H:i');
        $insert_array=array(
            'user_id'=>$user_id,
            'plan_id'=>$record['plan_id'],
            'start_date'=>$start_date,
            'start_time'=>$current_time,    
            'end_date'=>$end_date,
            'end_time'=>$current_time,
            'plan_orders'=>$record['orders'],
            'time_type'=>$record['time_type'],
            'time_value'=>$record['time_value'],
            'total_days'=>$record['total_days'],
            'amount'=>$record['amount'],
            'title'=>$record['title'],
            'description'=>$record['description'],
            'transaction_id'=>$transaction_id,
            //'transaction_fees'=>$transaction_fees,
            'status'=>'Completed',
            'total_used_order'=>0,
            'created'=>date('Y-m-d H:i:s'),
            );
        $insert_array['previous_remaining_order']=0;
        $insert_array['total_remaining_order']=$record['orders'];
        if($record['orders']!='Unlimited'){
            if($past_subscription=$this->Common_model->getRecords('user_subscription_history','*',array('user_id'=>$user_id),'id desc',true)){
                $insert_array['previous_remaining_order']=$past_subscription['total_remaining_order'];
                $insert_array['total_remaining_order']=$record['orders']+$past_subscription['total_remaining_order'];
            }
        }
        if($last_id=$this->Common_model->addEditRecords('user_subscription_history',$insert_array)){
            $from_email =getNotificationEmail();
            $to_email =$sp_user_details['email'];
            $subject =  WEBSITE_NAME.' : Subscription';
            $data['name']= $sp_user_details['fullname'];
            $data['message']= 'Congratulations! you are our subscribed member.You purchased '.ucfirst($record['title']).' Plan in'.ADMIN_CURRENCY.$record['amount'].'.';
            $body = $this->load->view('template/common', $data,TRUE);
            $this->Common_model->sendEmail($to_email,$subject,$body,$from_email);

            /*$title='Subscription';
            $content=$data['message'];
            $notdata['type']='subscription';
            $this->Common_model->push_notification_send($user_id,$notdata,$last_id,$title,$content,'',$user_id);*/
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has subscribe a new plan '".$record['title']."'.";
            actionLog('user_subscription_history',$last_id,'add',$log_msg,'User',$user_id);
            $this->display_output('1','Plan subscribed successfully.');

        }else{
            $this->display_output('0','Some error occured! Please try again.');
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////Celebrant section/////////////////////////////////

    public function celebrant_get_service_provider_list(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $keyword  = $this->clear_input($this->input->post('keyword'));
       
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);

        $list=$this->App_model->celebrant_get_service_provider_list($user_id,$user_address['country'],$user_address['state'],$user_address['city']);
        //echo $this->db->last_query();exit;
        if(!empty($list)) {
            $this->display_output('1','Celebrant\'s service provider list', array('list'=>$list));
        } else {
            $this->display_output('0','Service providers are not found.');
        }
        //echo $this->db->last_query();exit;


    }
    public function celebrant_get_service_provider_list_bkp(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $keyword  = $this->clear_input($this->input->post('keyword'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);

        $list=$this->App_model->celebrant_get_service_provider_list($user_id,$user_address['country'],$user_address['state'],$user_address['city']);
        //echo $this->db->last_query();exit;
         if(!empty($list)) {
            $this->display_output('1','Celebrant\'s service provider list', array('list'=>$list));
        } else {
            $this->display_output('0','Service providers are not found.');
        }
        //echo $this->db->last_query();exit;


    }
    public function celebrant_get_service_provider_details(){
        
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $service_provider_id  = $this->clear_input($this->input->post('service_provider_id'));
        $timezone  = $this->clear_input($this->input->post('timezone'));

        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($service_provider_id)) {
            $this->display_output('0','Please enter service provider id.');
        }
        /*if(empty($timezone)){
            $this->display_output('0','Please enter time zone.');
        }*/
        $detail=$this->App_model->cp_get_sp_details($service_provider_id);
        if(!empty($detail)) {
            $detail['is_follow']=checkFollowStatus($service_provider_id,$user_id);
            $local = date("Y-m-d H:i:s");
            $service_offer=$this->App_model->get_servies_offer($service_provider_id,$local);
           //echo $this->db->last_query();exit;
            $service_image=$this->Common_model->getRecords('services','img1',array('is_deleted'=>0,'status'=>'Active','user_id'=>$service_provider_id),'',false);
            
            $response =array('sp_details'=>$detail,'service_offer'=>$service_offer,'slider_image'=>$service_image);
            $this->display_output('1','service provider detail', array('details'=>$response));
        } else {
            $this->display_output('0','service provider details not found');
        }
    }
    public function get_items(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $service_id  = $this->clear_input($this->input->post('service_id'));
        
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($service_id)) {
            $this->display_output('0','Please enter service id.');
        }
        $detail=$this->App_model->get_service_items($service_id);
      
        if(!empty($detail)) {
            $this->display_output('1','Item details', array('item_details'=>$detail));
        } else {
            $this->display_output('0','Items not found');
        }
    }
    public function create_event_bkp(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $timezone  = $this->clear_input($this->input->post('timezone'));
        $event_title  = $this->clear_input($this->input->post('event_title'));
        $event_date  = $this->clear_input($this->input->post('event_date'));
        $event_time  = $this->clear_input($this->input->post('event_time'));
        $members  = $this->clear_input($this->input->post('members'));
        $country  = $this->clear_input($this->input->post('country'));
        $state  = $this->clear_input($this->input->post('state'));
        $city  = $this->clear_input($this->input->post('city'));
        $event_address  = $this->clear_input($this->input->post('event_address'));
        $latitude  = $this->clear_input($this->input->post('latitude'));
        $longitude  = $this->clear_input($this->input->post('longitude'));
        $note  = $this->clear_input($this->input->post('note'));
        $items  = $this->clear_input($this->input->post('items'));
        $service_id  = $this->clear_input($this->input->post('service_id'));

        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($timezone)){
            $this->display_output('0','Please enter time zone.');
        }
       /* if(empty($service_id)) {
            $this->display_output('0','Please enter service id.');
        }*/
        if(empty($event_title)) {
            $this->display_output('0','Please enter event name.');
        }if(empty($event_date)) {
            $this->display_output('0','Please enter event date.');
        }if(empty($event_time)) {
            $this->display_output('0','Please enter event time.');
        }if(empty($members)) {
            $this->display_output('0','Please enter members.');
        }if(empty($country)) {
            $this->display_output('0','Please select country.');
        }if(empty($state)) {
            $this->display_output('0','Please select state.');
        }if(empty($city)) {
            $this->display_output('0','Please select city.');
        }
        if(empty($event_address)) {
            $this->display_output('0','Please enter event address.');
        }
        if(empty($items)) {
            $this->display_output('0','Please enter items.');
        }if(empty($service_id)) {
            $this->display_output('0','Please enter service id.');
        }
       
        $offer_where="status='Active' and is_deleted=0 and  service_id = '".$service_id."'  and (start_date <= '".date('Y-m-d H:i:s')."' and end_date >= '".date('Y-m-d H:i:s')."')";
        $offer_details=$this->Common_model->getRecords('offer','discount,offer_name',$offer_where,'',true);
        
        $user_details=$this->Common_model->getRecords('users','fullname,email,mobile,staging_id',array('user_id'=>$user_id),'',true);
        $sp_details=$this->Common_model->getRecords('services','user_id',array('id'=>$service_id),'',true);
        $event_date=convertLocalTimezoneToGMT($event_date.' '.$event_time,$timezone,$showTime=true);
        $event_datetime=date('Y-m-d H:i:s',strtotime($event_date));
        $insert_data=array(
            'user_id'=>$user_id,
            'service_id'=>$service_id,
            'sp_id'=>$sp_details['user_id'],
            'event_title'=>$event_title,
            'event_date'=>$event_datetime,
            'members'=>$members,
            'country_id'=>$country,
            'state_id'=>$state,
            'city_id'=>$city,
            'event_address'=>$event_address,
            'note'=>$note,
            'latitude'=>$latitude,
            'longitude'=>$longitude,
            'created'=>date('Y-m-d H:i:s'),
            );
        $item_arr=json_decode($items);
        $this->db->trans_begin();
        if($last_id = $this->Common_model->addEditRecords('events',$insert_data)){
            $event_number='E'.str_pad($last_id,8,0,STR_PAD_LEFT);
                $this->Common_model->addEditRecords('events',array('event_number'=>$event_number),array('id'=>$last_id));
            $user_data=array('user_id'=>$user_id,
                            'fullname'=>$user_details['fullname'],
                            'email'=>$user_details['email'],
                            'mobile'=>$user_details['mobile'],
                            'event_id'=>$last_id,
                            'event_date_time'=>$event_datetime,
                            'service_id'=>$service_id,
                            'ip_address'=>$this->input->ip_address(),
                            'created'=>date('Y-m-d H:i:s')
                );
            if($order_id = $this->Common_model->addEditRecords('orders',$user_data)){
                 $order_number=str_pad($order_id,8,0,STR_PAD_LEFT);
                 $this->Common_model->addEditRecords('orders',array('order_number'=>$order_number),array('id'=>$order_id));
                $user_data['order_id']=$order_id;
                $user_data['order_number']=$order_number;
                $order_history_id=$this->Common_model->addEditRecords('orders_history',$user_data);
                $subtotal=0;
                $discount=0;
                $discount_title='Discount';
                if(!empty($offer_details['discount'])){
                    $discount=$offer_details['discount'];
                    $discount_title='Discount'.'-'.$offer_details['offer_name'];
                }
                
                for($i=0;$i<count($item_arr);$i++){
                    $total_qty=($item_arr[$i]->qty*$item_arr[$i]->customer_qty);
                    $total_price=number_format($item_arr[$i]->customer_qty*$item_arr[$i]->price,2,'.','');
                    $sub_total+=$total_price;
                    $sub_item_total_price=$sub_total;
                    $insert_array=array('item_name' => $item_arr[$i]->item,
                                        'price' =>$item_arr[$i]->price,
                                        'unit' =>$item_arr[$i]->unit,
                                        'qty' =>$item_arr[$i]->qty,
                                        'customer_qty' =>$item_arr[$i]->customer_qty,
                                        'total_qty' =>$total_qty,
                                        'total_price' =>$total_price,
                                        'service_id'=>$service_id,
                                        'order_id'=>$order_id,
                                        'event_id'=>$last_id,
                                        'event_date_time'=>$event_datetime,
                                        'created'=>date('Y-m-d H:i:s'));
                    $this->Common_model->addEditRecords('order_details',$insert_array);
                    $insert_array['order_history_id']=$order_history_id;
                    $this->Common_model->addEditRecords('order_history_details',$insert_array);
                }
                $order_total1= array('order_id'=>$order_id,'code'=>'sub_total','title'=>'Subtotal','value'=>$sub_item_total_price,'sort_order'=>1,'created'=>date('Y-m-d H:i:s'));
                if($discount>0){
                    $deduct_discount=($sub_item_total_price*$discount)/100;
                    $gt=$sub_item_total_price-$deduct_discount;
                    $order_total2= array('order_id'=>$order_id,'code'=>'discount','title'=>$discount_title,'value'=>$deduct_discount,'discount_value'=>$discount,'sort_order'=>2,'created'=>date('Y-m-d H:i:s'));
                }else{
                    $gt=$sub_item_total_price;
                    $order_total2= array('order_id'=>$order_id,'code'=>'discount','title'=>$discount_title,'value'=>0,'discount_value'=>0,'sort_order'=>2,'created'=>date('Y-m-d H:i:s'));
                }
                $order_total3= array('order_id'=>$order_id,'code'=>'total_price','title'=>'Total Price','value'=>$gt,'sort_order'=>3,'created'=>date('Y-m-d H:i:s'));
                $this->Common_model->addEditRecords('order_total',$order_total1);
                $this->Common_model->addEditRecords('order_total',$order_total2);
                $this->Common_model->addEditRecords('order_total',$order_total3);
                $this->Common_model->addEditRecords('orders',array('total_amount'=>$gt),array('id'=>$order_id));
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $this->display_output('0','Something went wrong. Please try again.'); 
        } else {
            $this->db->trans_commit();
            $sp_user_details=getNameEmailAddress($sp_details['user_id']);
            $from_email =getNotificationEmail();
            $to_email =$sp_user_details['email'];
            $subject =  WEBSITE_NAME.' :  #'.$event_number.' New event created';
            $data['name']= $sp_user_details['staging_id'];
            $data['message']= '<b>'.$event_number.'</b>'.' '.ucwords($event_title).' is created by '.ucwords($user_details['fullname']).'. Please give your response.';
            $body = $this->load->view('template/common', $data,TRUE);
            $this->Common_model->sendEmail($to_email,$subject,$body,$from_email);

           
            $from_email =getNotificationEmail();
            $to_email =$user_details['email'];
            $subject = WEBSITE_NAME.' :  #'.$event_number.' New event created';
            $data['name']= ucwords($user_details['fullname']);
            $data['message']= '<b>'.$event_number.'</b>'.' '.ucwords($event_title).' is created by you. It is pending for service provider\'s approval.';
            $body = $this->load->view('template/common', $data,TRUE);
            $this->Common_model->sendEmail($to_email,$subject,$body,$from_email);

            $title=$event_number.'-'.ucwords($event_title).' is created.';
           // $time=date('h:i A',strtotime($start_time)).' - '.date('h:i A',strtotime($end_time));
            $content='For '.date('M d, Y',strtotime($event_date));
           // .' Time : '.$time;
            $notdata['type']='new_event';
            $this->Common_model->push_notification_send($sp_details['user_id'],$notdata,$last_id,$title,$content,'',$user_id);

            $this->display_output('1','Event created successfully.',array('event_id'=>$last_id));
        }
    }
    public function create_event_bkp2(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $timezone  = $this->clear_input($this->input->post('timezone'));
        $event_title  = $this->clear_input($this->input->post('event_title'));
        $event_date  = $this->clear_input($this->input->post('event_date'));
        $event_time  = $this->clear_input($this->input->post('event_time'));
        $members  = $this->clear_input($this->input->post('members'));
        $country  = $this->clear_input($this->input->post('country'));
        $state  = $this->clear_input($this->input->post('state'));
        $city  = $this->clear_input($this->input->post('city'));
        $event_address  = $this->clear_input($this->input->post('event_address'));
        $latitude  = $this->clear_input($this->input->post('latitude'));
        $longitude  = $this->clear_input($this->input->post('longitude'));
        $note  = $this->clear_input($this->input->post('note'));
        $items  = $this->clear_input($this->input->post('items'));
        $service_id  = $this->clear_input($this->input->post('service_id'));
        $event_type  = $this->clear_input($this->input->post('event_type'));

        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($timezone)){
            $this->display_output('0','Please enter time zone.');
        }
       /* if(empty($service_id)) {
            $this->display_output('0','Please enter service id.');
        }*/
        if(empty($event_title)) {
            $this->display_output('0','Please enter event name.');
        }if(empty($event_date)) {
            $this->display_output('0','Please enter event date.');
        }if(empty($event_time)) {
            $this->display_output('0','Please enter event time.');
        }if(empty($members)) {
            $this->display_output('0','Please enter members.');
        }if(empty($country)) {
            $this->display_output('0','Please enter country.');
        }if(empty($state)) {
            $this->display_output('0','Please enter state.');
        }if(empty($city)) {
            $this->display_output('0','Please enter city.');
        }
        if(empty($event_address)) {
            $this->display_output('0','Please enter event address.');
        }
        if(empty($items)) {
            $this->display_output('0','Please enter items.');
        }if(empty($service_id)) {
            $this->display_output('0','Please enter service id.');
        }if(empty($event_type)) {
            $this->display_output('0','Please enter event type.');
        }
        $tax=$this->Common_model->getFieldValue('settings','tax',array('id'=>1),'',true);
        $offer_where="status='Active' and is_deleted=0 and  service_id = '".$service_id."'  and (start_date <= '".date('Y-m-d H:i:s')."' and end_date >= '".date('Y-m-d H:i:s')."')";
        $offer_details=$this->Common_model->getRecords('offer','discount,offer_name',$offer_where,'',true);
        $user_details=$this->Common_model->getRecords('users','fullname,email,mobile,staging_id',array('user_id'=>$user_id),'',true);
        $sp_details=$this->Common_model->getRecords('services','user_id',array('id'=>$service_id),'',true);
        $event_date=convertLocalTimezoneToGMT($event_date.' '.$event_time,$timezone,$showTime=true);
        $event_datetime=date('Y-m-d H:i:s',strtotime($event_date));
        $insert_data=array(
            'user_id'=>$user_id,
            'service_id'=>$service_id,
            'sp_id'=>$sp_details['user_id'],
            'event_title'=>$event_title,
            'event_date'=>$event_datetime,
            'event_type'=>$event_type,
            'members'=>$members,
            'country_id'=>$country,
            'state_id'=>$state,
            'city_id'=>$city,
            'event_address'=>$event_address,
            'note'=>$note,
            'latitude'=>$latitude,
            'longitude'=>$longitude,
            'created'=>date('Y-m-d H:i:s'),
            );
        $item_arr=json_decode($items);
        $this->db->trans_begin();
        if($last_id = $this->Common_model->addEditRecords('events',$insert_data)){
            $event_number='E'.str_pad($last_id,8,0,STR_PAD_LEFT);
                $this->Common_model->addEditRecords('events',array('event_number'=>$event_number),array('id'=>$last_id));
            $user_data=array('user_id'=>$user_id,
                            'fullname'=>$user_details['fullname'],
                            'email'=>$user_details['email'],
                            'mobile'=>$user_details['mobile'],
                            'event_id'=>$last_id,
                            'event_date_time'=>$event_datetime,
                            'service_id'=>$service_id,
                            'ip_address'=>$this->input->ip_address(),
                            'created'=>date('Y-m-d H:i:s')
                );
            if($order_id = $this->Common_model->addEditRecords('orders',$user_data)){
                 $order_number=str_pad($order_id,8,0,STR_PAD_LEFT);
                 $this->Common_model->addEditRecords('orders',array('order_number'=>$order_number),array('id'=>$order_id));
               // $user_data['order_id']=$order_id;
                //$user_data['order_number']=$order_number;
               // $order_history_id=$this->Common_model->addEditRecords('orders_history',$user_data);
                $sub_total=0;
                $discount=0;
                $discount_title='Discount';
                if(!empty($offer_details['discount'])){
                    $discount=$offer_details['discount'];
                    $discount_title='Discount'.'-'.$offer_details['offer_name'];
                }
                $max_group_id=$this->Common_model->getRecords('order_history_details','max(`order_group_id`) as order_group_id','','',true);
                $order_history_details_max_id=$max_group_id['order_group_id']+1;
                for($i=0;$i<count($item_arr);$i++){
                    $total_qty=($item_arr[$i]->qty*$item_arr[$i]->customer_qty);
                    //$total_price=bcdiv($item_arr[$i]->customer_qty*$item_arr[$i]->price,1,2);
                    $total_price=number_format($item_arr[$i]->customer_qty*$item_arr[$i]->price,2,'.','');
                    if(!empty($discount)){
                         $discounted_sell_price=number_format((($total_price*$discount)/100),2,'.','');
                         $discount_price=$total_price-$discounted_sell_price;
                         if($tax){
                            $tax_sell_price=number_format((($discount_price*$tax)/100),2,'.','');
                            $sell_price=$discount_price+$tax_sell_price;
                         }else{
                            $sell_price=$discount_price;
                         }
                    }else{
                        $sell_price=$total_price;
                        if($tax){
                            $tax_sell_price=number_format((($total_price*$tax)/100),2,'.','');
                            $sell_price=$total_price+$tax_sell_price;
                        }
                    }
                    $sub_total+=$sell_price;
                    $sub_item_total_price=$sub_total;
                    $insert_array=array('item_name' => $item_arr[$i]->item,
                                        'price' =>$item_arr[$i]->price,
                                        'unit' =>$item_arr[$i]->unit,
                                        'qty' =>$item_arr[$i]->qty,
                                        'customer_qty' =>$item_arr[$i]->customer_qty,
                                        'total_qty' =>$total_qty,
                                        'total_price' =>$total_price,
                                        'sell_price' =>$sell_price,
                                        'tax' =>$tax,
                                        'discount' =>$discount,
                                        'offer_name' =>$offer_details['offer_name'],
                                        'service_id'=>$service_id,
                                        'order_id'=>$order_id,
                                        'event_id'=>$last_id,
                                        'order_group_id'=>$order_history_details_max_id,
                                        'event_date_time'=>$event_datetime,
                                        'created'=>date('Y-m-d H:i:s'));
                    $this->Common_model->addEditRecords('order_details',$insert_array);
                    //$this->Common_model->addEditRecords('order_history_details',$insert_array);
                }
                $order_status=array(
                    'order_batch_id'=>$order_history_details_max_id,
                    'order_id'=>$order_id,
                    'order_id'=>$order_id,
                    'status'=>'Pending',
                    'created'=>date('Y-m-d H:i:s'),
                    'ip_addres'=>$this->input->ip_address(),
                    );
                $this->Common_model->addEditRecords('order_status_history',$order_status);
                $order_total1= array('order_batch_id'=>$order_history_details_max_id,'order_id'=>$order_id,'code'=>'total','title'=>'Total','value'=>$sub_item_total_price,'sort_order'=>1,'created'=>date('Y-m-d H:i:s'));
                $this->Common_model->addEditRecords('order_total',$order_total1);
                $this->Common_model->addEditRecords('orders',array('total_amount'=>$sub_item_total_price),array('id'=>$order_id));
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $this->display_output('0','Something went wrong. Please try again.'); 
        } else {
            $this->db->trans_commit();
            $sp_user_details=getNameEmailAddress($sp_details['user_id']);
            $from_email =getNotificationEmail();
            $to_email =$sp_user_details['email'];
            $subject =  WEBSITE_NAME.' :  #'.$event_number.' New event created';
            $data['name']= $sp_user_details['staging_id'];
            $data['message']= '<b>'.$event_number.'</b>'.' '.ucwords($event_title).' is created by '.ucwords($user_details['fullname']).'. Please give your response.';
            $body = $this->load->view('template/common', $data,TRUE);
            $this->Common_model->sendEmail($to_email,$subject,$body,$from_email);

           
            $from_email =getNotificationEmail();
            $to_email =$user_details['email'];
            $subject = WEBSITE_NAME.' :  #'.$event_number.' New event created';
            $data['name']= ucwords($user_details['fullname']);
            $data['message']= '<b>'.$event_number.'</b>'.' '.ucwords($event_title).' is created by you. It is pending for service provider\'s approval.';
            $body = $this->load->view('template/common', $data,TRUE);
            $this->Common_model->sendEmail($to_email,$subject,$body,$from_email);

            $title=$event_number.'-'.ucwords($event_title).' is created.';
           // $time=date('h:i A',strtotime($start_time)).' - '.date('h:i A',strtotime($end_time));
            $content='For '.date('M d, Y',strtotime($event_date));
           // .' Time : '.$time;
            $notdata['type']='new_event';
            $this->Common_model->push_notification_send($sp_details['user_id'],$notdata,$last_id,$title,$content,'',$user_id);

            $this->display_output('1','Event created successfully.',array('event_id'=>$last_id));
        }
    }
    public function create_event(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $timezone  = $this->clear_input($this->input->post('timezone'));
        $event_title  = $this->clear_input($this->input->post('event_title'));
        $event_date  = $this->clear_input($this->input->post('event_date'));
        $event_time  = $this->clear_input($this->input->post('event_time'));
        $members  = $this->clear_input($this->input->post('members'));
        $country  = $this->clear_input($this->input->post('country'));
        $state  = $this->clear_input($this->input->post('state'));
        $city  = $this->clear_input($this->input->post('city'));
        $event_address  = $this->clear_input($this->input->post('event_address'));
        $latitude  = $this->clear_input($this->input->post('latitude'));
        $longitude  = $this->clear_input($this->input->post('longitude'));
        $note  = $this->clear_input($this->input->post('note'));
        $items  = $this->clear_input($this->input->post('items'));
        $service_id  = $this->clear_input($this->input->post('service_id'));
        $event_type  = $this->clear_input($this->input->post('event_type'));

        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($timezone)){
            $this->display_output('0','Please enter time zone.');
        }
        if(empty($event_title)) {
            $this->display_output('0','Please enter event name.');
        }if(empty($event_date)) {
            $this->display_output('0','Please enter event date.');
        }if(empty($event_time)) {
            $this->display_output('0','Please enter event time.');
        }if(empty($members)) {
            $this->display_output('0','Please enter members.');
        }if(empty($country)) {
            $this->display_output('0','Please select country.');
        }if(empty($state)) {
            $this->display_output('0','Please select state.');
        }if(empty($city)) {
            $this->display_output('0','Please select city.');
        }
        if(empty($event_address)) {
            $this->display_output('0','Please enter event address.');
        }
        if(empty($items)) {
            $this->display_output('0','Please enter items.');
        }if(empty($service_id)) {
            $this->display_output('0','Please enter service id.');
        }if(empty($event_type)) {
            $this->display_output('0','Please enter event type.');
        }
        $tax=$this->Common_model->getFieldValue('settings','tax',array('id'=>1),'',true);
        $offer_where="status='Active' and is_deleted=0 and  service_id = '".$service_id."'  and (start_date <= '".date('Y-m-d H:i:s')."' and end_date >= '".date('Y-m-d H:i:s')."')";
        $offer_details=$this->Common_model->getRecords('offer','discount,offer_name',$offer_where,'',true);
        //echo $this->db->last_query();exit;
        $user_details=$this->Common_model->getRecords('users','fullname,email,mobile,staging_id',array('user_id'=>$user_id),'',true);
        $sp_details=$this->Common_model->getRecords('services','user_id',array('id'=>$service_id),'',true);
        $event_date=convertLocalTimezoneToGMT($event_date.' '.$event_time,$timezone,$showTime=true);
        $event_datetime=date('Y-m-d H:i:s',strtotime($event_date));
        $insert_data=array(
            'user_id'=>$user_id,
            'service_id'=>$service_id,
            'sp_id'=>$sp_details['user_id'],
            'event_title'=>$event_title,
            'event_date'=>$event_datetime,
            'event_type'=>$event_type,
            'members'=>$members,
            'country_id'=>$country,
            'state_id'=>$state,
            'city_id'=>$city,
            'event_address'=>$event_address,
            'note'=>$note,
            'latitude'=>$latitude,
            'longitude'=>$longitude,
            'status'=>'Active',
            'created'=>date('Y-m-d H:i:s'),
            );
        $item_arr=json_decode($items);
        $this->db->trans_begin();
        if($last_id = $this->Common_model->addEditRecords('events',$insert_data)){
            $event_number='E'.str_pad($last_id,8,0,STR_PAD_LEFT);
                $this->Common_model->addEditRecords('events',array('event_number'=>$event_number),array('id'=>$last_id));
            $user_data=array('user_id'=>$user_id,
                            'fullname'=>$user_details['fullname'],
                            'email'=>$user_details['email'],
                            'mobile'=>$user_details['mobile'],
                            'event_id'=>$last_id,
                            'event_date_time'=>$event_datetime,
                            'service_id'=>$service_id,
                            'ip_address'=>$this->input->ip_address(),
                            'created'=>date('Y-m-d H:i:s')
                );
            if($order_id = $this->Common_model->addEditRecords('orders',$user_data)){
                 $order_number=str_pad($order_id,8,0,STR_PAD_LEFT);
                 $this->Common_model->addEditRecords('orders',array('order_number'=>$order_number),array('id'=>$order_id));
                $sub_total=0;
                $discount=0;
                $order_item_total=0;
                $sub_item_total_price=0;
                $discount_title='Discount';
                if(!empty($offer_details['discount'])){
                    $discount=$offer_details['discount'];
                    $discount_title='Discount'.'-'.$offer_details['offer_name'];
                }
                $max_group_id=$this->Common_model->getRecords('order_status_history','max(`order_batch_id`) as order_group_id','','',true);
                $order_history_details_max_id=$max_group_id['order_group_id']+1;
                for($i=0;$i<count($item_arr);$i++){
                    $total_qty=($item_arr[$i]->qty*$item_arr[$i]->customer_qty);
                    $total_price=number_format($item_arr[$i]->customer_qty*$item_arr[$i]->price,2,'.','');
                    if(!empty($discount)){
                         $discounted_sell_price=number_format((($total_price*$discount)/100),2,'.','');
                         $discount_price=$total_price-$discounted_sell_price;
                         if($tax){
                            $tax_sell_price=number_format((($discount_price*$tax)/100),2,'.','');
                            $sell_price=$discount_price+$tax_sell_price;
                         }else{
                            $sell_price=$discount_price;
                         }
                    }else{
                        $sell_price=$total_price;
                        if($tax){
                            $tax_sell_price=number_format((($total_price*$tax)/100),2,'.','');
                            $sell_price=$total_price+$tax_sell_price;
                        }
                    }
                    $order_item_total+=$total_price;
                    $sub_total+=$sell_price;
                    $sub_item_total_price=$sub_total;
                    $insert_array=array('item_name' => $item_arr[$i]->item,
                                        'price' =>$item_arr[$i]->price,
                                        'unit' =>$item_arr[$i]->unit,
                                        'qty' =>$item_arr[$i]->qty,
                                        'customer_qty' =>$item_arr[$i]->customer_qty,
                                        'total_qty' =>$total_qty,
                                        'total_price' =>$total_price,
                                        'sell_price' =>$sell_price,
                                        'tax' =>$tax,
                                        'discount' =>$discount,
                                        'offer_name' =>$offer_details['offer_name'],
                                        'service_id'=>$service_id,
                                        'order_id'=>$order_id,
                                        'event_id'=>$last_id,
                                        'order_group_id'=>$order_history_details_max_id,
                                        'event_date_time'=>$event_datetime,
                                        'created'=>date('Y-m-d H:i:s'));
                    $this->Common_model->addEditRecords('order_details',$insert_array);
                }//echo $order_item_total;exit;
                $order_status=array(
                    'order_batch_id'=>$order_history_details_max_id,
                    'order_id'=>$order_id,
                    'order_id'=>$order_id,
                    'status'=>'Pending',
                    'created'=>date('Y-m-d H:i:s'),
                    'ip_addres'=>$this->input->ip_address(),
                    );
                $this->Common_model->addEditRecords('order_status_history',$order_status);
                $this->Common_model->addEditRecords('orders',array('total_amount'=>$sub_item_total_price),array('id'=>$order_id));

                $order_total1= array('order_batch_id'=>$order_history_details_max_id,'order_id'=>$order_id,'code'=>'subtotal','title'=>'Subtotal','value'=>$order_item_total,'sort_order'=>1,'created'=>date('Y-m-d H:i:s'));
                $this->Common_model->addEditRecords('order_total',$order_total1);
                
                if($discount>0){
                    $deduct_discount=($order_item_total*$discount)/100;
                    $gt=$sub_item_total_price-$deduct_discount;
                    $order_total2= array('order_batch_id'=>$order_history_details_max_id,'order_id'=>$order_id,'code'=>'discount','title'=>$discount_title,'value'=>$deduct_discount,'discount_value'=>$discount,'sort_order'=>2,'created'=>date('Y-m-d H:i:s'));
                }else{
                    $gt=$sub_item_total_price;
                    $order_total2= array('order_batch_id'=>$order_history_details_max_id,'order_id'=>$order_id,'code'=>'discount','title'=>$discount_title,'value'=>0,'discount_value'=>0,'sort_order'=>2,'created'=>date('Y-m-d H:i:s'));
                }
                $order_total3= array('order_batch_id'=>$order_history_details_max_id,'order_id'=>$order_id,'code'=>'tax','title'=>'Tax','value'=>$tax,'sort_order'=>3,'created'=>date('Y-m-d H:i:s'));
                $order_total4= array('order_batch_id'=>$order_history_details_max_id,'order_id'=>$order_id,'code'=>'total','title'=>'Total','value'=>$sub_item_total_price,'sort_order'=>4,'created'=>date('Y-m-d H:i:s'));
                $this->Common_model->addEditRecords('order_total',$order_total2);
                $this->Common_model->addEditRecords('order_total',$order_total3);
                $this->Common_model->addEditRecords('order_total',$order_total4);
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $this->display_output('0','Something went wrong. Please try again.'); 
        } else {
            $this->db->trans_commit();
            $this->display_output('1','Event created successfully.',array('event_id'=>$last_id));
        }
    }
    public function checkout(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $items  = $this->clear_input($this->input->post('items'));
        $service_id  = $this->clear_input($this->input->post('service_id'));
        $event_id  = $this->clear_input($this->input->post('event_id'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($items)) {
            $this->display_output('0','Please enter items.');
        }if(empty($service_id)) {
            $this->display_output('0','Please enter service id.');
        }if(empty($event_id)) {
            $this->display_output('0','Please enter event id.');
        }
       
        $order_info=$this->Common_model->getRecords('orders','id',array('event_id'=>$event_id),'',true);
        $order_id=$order_info['id'];
        if(!$service_record=$this->Common_model->getRecords('services','user_id',array('id'=>$service_id,'status'=>'Active','is_deleted'=>0),'',true)){
            $this->display_output('0','Service isn\'t available now.');
        }
        $order_total_record=$this->Common_model->getRecords('order_total','id,code,title,value,discount_value',array('order_id'=>$order_id),'',false);
        $tax=$this->Common_model->getFieldValue('settings','tax',array('id'=>1),'',true);

        $offer_where="status='Active' and is_deleted=0 and  service_id = '".$service_id."'  and (start_date <= '".date('Y-m-d H:i:s')."' and end_date >= '".date('Y-m-d H:i:s')."')";
        $offer_details=$this->Common_model->getRecords('offer','discount,offer_name',$offer_where,'',true);
        $item_arr=json_decode($items);
        $sub_total=0;
        $discount=0;
        $order_item_total=0;
        $sub_item_total_price=0;
        $discount_title='Discount';
                if(!empty($offer_details['discount'])){
                    $discount=$offer_details['discount'];
                    $discount_title='Discount'.'-'.$offer_details['offer_name'];
                }
        for($i=0;$i<count($item_arr);$i++){
            if(!$item_records=$this->Common_model->getRecords('services_item','item_name,price,qty,unit',array('id'=>$item_arr[$i]->item_id,'status'=>'Active','is_deleted'=>0),'',true)){
                $this->display_output('0',ucfirst($item_arr[$i]->item). ' isn\'t available now.');
            }
            if($item_records['item_name']!=$item_arr[$i]->item){
                $this->display_output('0',ucfirst($item_arr[$i]->item). ' name has been changed.');
            }
            if($item_records['price']!=$item_arr[$i]->price){
                $this->display_output('0',ucfirst($item_arr[$i]->item). ' price has been changed.');
            }
            if($item_records['qty']!=$item_arr[$i]->qty){
                $this->display_output('0',ucfirst($item_arr[$i]->item). ' per unit quantity has been changed.');
            }
            $measurement_unit=$this->Common_model->getFieldValue('measurement_unit','title',array('id'=>$item_records['unit']),'',true);
            if($measurement_unit!=$item_arr[$i]->unit){
                $this->display_output('0',ucfirst($item_arr[$i]->item). ' measuring unit has been changed.');
            }
            $total_qty=($item_arr[$i]->qty*$item_arr[$i]->customer_qty);
            $total_price=number_format($item_arr[$i]->customer_qty*$item_arr[$i]->price,2,'.','');
            if(!empty($discount)){
                 $discounted_sell_price=number_format((($total_price*$discount)/100),2,'.','');
                 $discount_price=$total_price-$discounted_sell_price;
                 if($tax){
                    $tax_sell_price=number_format((($discount_price*$tax)/100),2,'.','');
                    $sell_price=$discount_price+$tax_sell_price;
                 }else{
                    $sell_price=$discount_price;
                 }
            }else{
                $sell_price=$total_price;
                if($tax){
                    $tax_sell_price=number_format((($total_price*$tax)/100),2,'.','');
                    $sell_price=$total_price+$tax_sell_price;
                }
            }

            $order_item_total+=$total_price;
            $sub_total+=$sell_price;
            $sub_item_total_price=$sub_total;
               
        }
        if(!empty($order_total_record)){
            foreach ($order_total_record as $key => $value) {
                if($value['code']=='discount'){
                    if($value['value']!='0.00'){
                        if(empty($offer_details['discount']) || ($value['discount_value']!=$offer_details['discount'])){
                            $this->display_output('0',ucfirst($value['title']).' isn\'t available now.');
                        }
                    }
                }
                if($value['code']=='tax'){
                    if($value['value']!=$tax){
                        $this->display_output('0','Tax value has been changed.');
                    }
                }
                if($value['code']=='subtotal'){
                    if($value['value']!=$order_item_total){
                        $this->display_output('0','Order items total price has been changed.');
                    }
                }
                if($value['code']=='total'){
                   // echo $sub_item_total_price;echo '<br>';
                   // echo number_format($sub_item_total_price,2,'.','');
                    if($value['value']!=number_format($sub_item_total_price,2,'.','')){
                        $this->display_output('0','Order items sell price has been changed.');
                    }
                }
            }

        }
        $this->display_output('1','You can move forward to payment.',array('order_id'=>$order_id,'event_id'=>$event_id));
    }
    public function make_payment(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $event_id  = $this->clear_input($this->input->post('event_id'));
        $transaction_id  = $this->clear_input($this->input->post('transaction_id'));
        $timezone  = $this->clear_input($this->input->post('timezone'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($event_id)) {
            $this->display_output('0','Please enter event id.');
        }if(empty($transaction_id)) {
            $this->display_output('0','Please enter transaction id.');
        }if(empty($timezone)) {
            $this->display_output('0','Please enter timezone.');
        }
        //$sp_id=$this->Common_model->getFieldValue('events','sp_id',array('id'=>$event_id),'',true);
        $event_info=$this->Common_model->getRecords('events','sp_id,event_title,event_type,service_id,event_date',array('id'=>$event_id),'',true);
        $sp_id=$event_info['sp_id'];
        $event_title=$event_info['event_title'];
        $order_info=$this->Common_model->getRecords('orders','id,order_number,total_amount',array('event_id'=>$event_id),'',true);

        $order_id=$order_info['id'];
        $sub_item_total_price=$order_info['total_amount'];
        $order_number=$order_info['order_number'];
        $this->db->trans_begin();
            $paypal_fees = ($sub_item_total_price*2.90/100)+0.30;  // paypal fee
            $payment_history=array(
                'order_id'=>$order_id,
                'transaction_id'=>$transaction_id,
                'total_order_amount'=>$sub_item_total_price,
                'paid_amount'=>$sub_item_total_price,
                'customer_id'=>$user_id,
                'service_provider_id'=>$sp_id,
                'payment_type'=>'Order',
                'transfer_type'=>'Customer_To_Admin',
                'paypal_fee_amount'=>$paypal_fees,
                'user_will_get'=>($sub_item_total_price-$paypal_fees),
                'payment_status'=>'Completed',
                'created'=>date('Y-m-d H:i:s'),
                'ip_addres'=>$this->input->ip_address(),
                );
            $this->Common_model->addEditRecords('payment_history',$payment_history);
            $this->Common_model->addEditRecords('events',array('type'=>'Actual'),array('id'=>$event_id));
            $this->Common_model->addEditRecords('orders',array('type'=>'Actual','payment_status'=>'Paid'),array('id'=>$order_id));
            $this->Common_model->addEditRecords('order_details',array('type'=>'Actual'),array('order_id'=>$order_id));
            $this->Common_model->addEditRecords('order_status_history',array('type'=>'Actual'),array('order_id'=>$order_id));
            $this->Common_model->addEditRecords('order_total',array('type'=>'Actual'),array('order_id'=>$order_id));
            if($event_info['event_type']=='Public'){
                $is_private=0;
            }else{
                $is_private=1;
            }
            $post_data=array(
                'user_id'=>$user_id,
                'post_type'=>2,
                'event_id'=>$event_id,
                'sp_id'=>$sp_id,
                'is_private'=>$is_private,
                'post_content'=>$event_title.'^'.$event_info['event_date'],
                'created'=>date('Y-m-d H:i:s'),
            );
            if($post_id=$this->Common_model->addEditRecords('post',$post_data)){
                $post_number='Post'.str_pad($post_id,8,0,STR_PAD_LEFT);
                $this->Common_model->addEditRecords('post',array('post_number'=>$post_number),array('id'=>$post_id));
                $post_media=array(
                    'post_id'=>$post_id,
                    'user_id'=>$user_id,
                    'media_path' =>getServiceCategoryImage($event_info['service_id']) ,
                    'video_thumbnail' =>'',
                    'video_large_thumbnail' =>'',
                    'duration' =>'',
                    'media_type' => 'image',
                    'created' => date('Y-m-d H:i:s'),
                );
                $this->Common_model->addEditRecords('post_media',$post_media);
            }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $this->display_output('0','Something went wrong. Please try again.'); 
        } else {
            $this->db->trans_commit();
            $this->Front_common_model->send_order_invoice($order_id,'Customer_To_Admin',$timezone);
            $title= 'New event created.';
            $content='Thank you for your order purchase your order number is #'.$order_info['order_number'].'.';
            $notdata['type']='new_event';
            $this->Common_model->push_notification_send($user_id,$notdata,$event_id,$title,$content,'',$user_id);
            $title1= 'New Order.';
            $content1='A new order is placed for you order number is #'.$order_info['order_number'].'.';
            $notdata1['type']='new_order';
            $this->Common_model->push_notification_send($sp_id,$notdata1,$event_id,$title1,$content1,'',$user_id);
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has created a new event '".$event_title."'.";
            actionLog('events',$event_id,'add',$log_msg,'User',$user_id); 
            $this->display_output('1','Event created successfully.',array('order_id'=>$order_id,'event_id'=>$event_id));
        }
    }
    public function edit_event_bkp(){
        $event_id  = $this->clear_input($this->input->post('event_id'));
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $timezone  = $this->clear_input($this->input->post('timezone'));
        
        $event_date  = $this->clear_input($this->input->post('event_date'));
        $event_time  = $this->clear_input($this->input->post('event_time'));
        
        $items  = $this->clear_input($this->input->post('items'));
        $remove_items  = $this->clear_input($this->input->post('remove_items'));
        $service_id  = $this->clear_input($this->input->post('service_id'));
        $event_type  = $this->clear_input($this->input->post('event_type'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($timezone)){
            $this->display_output('0','Please enter time zone.');
        }
        if(empty($event_id)) {
            $this->display_output('0','Please enter event id.');
        }
        if(empty($event_date)) {
            $this->display_output('0','Please enter event date.');
        }if(empty($event_time)) {
            $this->display_output('0','Please enter event time.');
        }
        if(empty($service_id)) {
            $this->display_output('0','Please enter service id.');
        }if(empty($event_type)) {
            $this->display_output('0','Please enter event type.');
        }
        if(empty($items)) {
            $this->display_output('0','Please enter items.');
        }
        $removed_item_sell_price=0;
        $remove_item_ids=explode(',', $remove_items);
        $all_remove_item=array();
        if(!empty($remove_item_ids)){
            for($k=0;$k<count($remove_item_ids);$k++){
                 $removed_item_record=$this->Common_model->getRecords('order_details','service_id,order_id,event_id,item_name,price,qty,total_qty,unit,customer_qty,total_price,sell_price',array('id'=>$remove_item_ids[$k]),'',true);
                $removed_item_sell_price+=$removed_item_record['sell_price'];
                $all_remove_item[]=$removed_item_record;
            }
        }
        $current_date=date('Y-m-d H:i:s');
        if(!$event_details=$this->Common_model->getRecords('events','event_date',array('id'=>$event_id,'is_deleted'=>0),'',true)){
            $this->display_output('0','Event isn\'t available.');
        }
        //$event_details['event_date']='2020-01-01 08:30:00';
        $diff=strtotime($event_details['event_date'])-strtotime($current_date);
        if($diff < 172800) {
            $this->display_output('0','Edit time of event is passed away.');
        }
        $order_details=$this->Common_model->getRecords('orders','id,payment_status,total_price',array('event_id'=>$event_id),'',true);
        $order_id=$order_details['id'];
        $offer_where="status='Active' and is_deleted=0 and  service_id = '".$service_id."'  and (start_date <= '".date('Y-m-d H:i:s')."' and end_date >= '".date('Y-m-d H:i:s')."')";
        $offer_details=$this->Common_model->getRecords('offer','discount,offer_name',$offer_where,'',true);
        
        $user_details=$this->Common_model->getRecords('users','fullname,email,mobile,staging_id',array('user_id'=>$user_id),'',true);
        $sp_details=$this->Common_model->getRecords('services','user_id',array('id'=>$service_id),'',true);
        $event_date=convertLocalTimezoneToGMT($event_date.' '.$event_time,$timezone,$showTime=true);
        $event_datetime=date('Y-m-d H:i:s',strtotime($event_date));
        $update_data=array(
            'user_id'=>$user_id,
            'service_id'=>$service_id,
            'event_date'=>$event_datetime,
            'modified'=>date('Y-m-d H:i:s'),
            );
        $item_arr=json_decode($items);
        $this->db->trans_begin();
        if($this->Common_model->addEditRecords('events',$update_data,array('id'=>$event_id))){
            $user_data=array('user_id'=>$user_id,
                            'fullname'=>$user_details['fullname'],
                            'email'=>$user_details['email'],
                            'mobile'=>$user_details['mobile'],
                            'event_date_time'=>$event_datetime,
                            'payment_status'=>'Unpaid',
                            'service_id'=>$service_id,
                            'ip_address'=>$this->input->ip_address(),
                            'modified'=>date('Y-m-d H:i:s'),
                            'event_date'=>$event_datetime,
                );
            if($this->Common_model->addEditRecords('orders',$user_data,array('event_id'=>$event_id))){
                $new_sub_total=0;
                $subtotal=0;
                $discount=0;
                $discount_title='Discount';
                if(!empty($offer_details['discount'])){
                    $discount=$offer_details['discount'];
                    $discount_title='Discount'.'-'.$offer_details['offer_name'];
                }
                $this->Common_model->deleteRecords('order_details',array('order_id'=>$order_id));
                $this->Common_model->addEditRecords('order_history_details',array('status'=>'Inactive','modified'=>date('Y-m-d H:i:s')),array('order_id'=>$order_id));
                $this->Common_model->addEditRecords('order_total',array('status'=>'Inactive','modified'=>date('Y-m-d H:i:s')),array('order_id'=>$order_id));
                $max_group_id=$this->Common_model->getRecords('order_history_details','max(`order_group_id`) as order_group_id','','',true);
                $order_history_details_max_id=$max_group_id['order_group_id']+1;
                for($i=0;$i<count($item_arr);$i++){
                    $total_qty=($item_arr[$i]->qty*$item_arr[$i]->customer_qty);
                    $total_price=number_format($item_arr[$i]->customer_qty*$item_arr[$i]->price,2,'.','');
                    if(!empty($discount)){
                         $discounted_sell_price=number_format((($total_price*$discount)/100),2,'.','');
                         $discount_price=$total_price-$discounted_sell_price;
                         if($tax){
                            $tax_sell_price=number_format((($discount_price*$tax)/100),2,'.','');
                            $sell_price=$discount_price-$tax_sell_price;
                         }else{
                            $sell_price=$discount_price;
                         }
                    }else{
                        $sell_price=$total_price;
                        if($tax){
                            $tax_sell_price=number_format((($total_price*$tax)/100),2,'.','');
                            $sell_price=$total_price-$tax_sell_price;
                         }
                    }
                    $sub_total+=$total_price;
                    $sub_item_total_price=$sub_total;
                    if(empty($item_arr[$i]->order_details_id)){
                        $new_total_qty=($item_arr[$i]->qty*$item_arr[$i]->customer_qty);
                        $new_total_price=number_format($item_arr[$i]->customer_qty*$item_arr[$i]->price,2,'.','');
                        $new_sub_total+=$new_total_price;
                    }
                    $insert_array=array('item_name' => $item_arr[$i]->item,
                                        'price' =>$item_arr[$i]->price,
                                        'unit' =>$item_arr[$i]->unit,
                                        'qty' =>$item_arr[$i]->qty,
                                        'customer_qty' =>$item_arr[$i]->customer_qty,
                                        'total_qty' =>$total_qty,
                                        'total_price' =>$total_price,
                                        'sell_price' =>$sell_price,
                                        'tax' =>$tax,
                                        'service_id'=>$service_id,
                                        'order_id'=>$order_id,
                                        'order_group_id'=>$order_history_details_max_id,
                                        'event_id'=>$event_id,
                                        'event_date_time'=>$event_datetime,
                                        'created'=>date('Y-m-d H:i:s'));
                    $this->Common_model->addEditRecords('order_details',$insert_array);
                    $this->Common_model->addEditRecords('order_history_details',$insert_array);
                }
                if(!empty($remove_item_ids)){
                    foreach($all_remove_item as $remove_insert_array){
                        $remove_insert_array['order_group_id']=$order_history_details_max_id;
                        $remove_insert_array['event_date_time']=$event_datetime;
                        $remove_insert_array['action']='Remove';
                        $this->Common_model->addEditRecords('order_history_details',$remove_insert_array);
                    }
                }
                $order_status=array(
                    'order_batch_id'=>$order_history_details_max_id,
                    'order_id'=>$order_id,
                    'order_id'=>$order_id,
                    'status'=>'Pending',
                    'created'=>date('Y-m-d H:i:s'),
                    'ip_addres'=>$this->input->ip_address(),
                    );
                $this->Common_model->addEditRecords('order_status_history',$order_status);
                $order_total1= array('order_batch_id'=>$order_history_details_max_id,'order_id'=>$order_id,'code'=>'sub_total','title'=>'Subtotal','value'=>$sub_item_total_price,'sort_order'=>1,'created'=>date('Y-m-d H:i:s'));
                if($discount>0){
                    $deduct_discount=($sub_item_total_price*$discount)/100;
                    $gt=$sub_item_total_price-$deduct_discount;
                    $new_deduct_discount=($new_sub_total*$discount)/100;
                    $new_item_gt=$new_sub_total-$new_deduct_discount;
                    $order_total2= array('order_batch_id'=>$order_history_details_max_id,'order_id'=>$order_id,'code'=>'discount','title'=>$discount_title,'value'=>$deduct_discount,'discount_value'=>$discount,'sort_order'=>2,'created'=>date('Y-m-d H:i:s'));
                }else{
                    $gt=$sub_item_total_price;
                    $new_item_gt=$new_sub_total;
                    $order_total2= array('order_batch_id'=>$order_history_details_max_id,'order_id'=>$order_id,'code'=>'discount','title'=>$discount_title,'value'=>0,'discount_value'=>0,'sort_order'=>2,'created'=>date('Y-m-d H:i:s'));
                }
                if($tax=$this->Common_model->getFieldValue('settings','tax',array('id'=>1),'',true)){
                    $applied_tax=number_format((($gt*$tax)/100),2,'.','');
                    $gt=$gt+$applied_tax;
                }
                $order_total3= array('order_batch_id'=>$order_history_details_max_id,'order_id'=>$order_id,'code'=>'total_price','title'=>'Total Price','value'=>$gt,'sort_order'=>6,'created'=>date('Y-m-d H:i:s'));
                
                $order_total4= array('order_batch_id'=>$order_history_details_max_id,'order_id'=>$order_id,'code'=>'tax','title'=>'Tax','value'=>$tax,'sort_order'=>3,'created'=>date('Y-m-d H:i:s'));
                $this->Common_model->addEditRecords('order_total',$order_total1);
                $this->Common_model->addEditRecords('order_total',$order_total2);
                $this->Common_model->addEditRecords('order_total',$order_total4);
                
                $this->Common_model->addEditRecords('order_total',$order_total3);
                $final_order_update=array(
                    'total_amount'=>$gt,
                    );
                if($order_details['payment_status']=='Unpaid'){
                    $final_order_update['redeem_payment']=0;
                }else{
                    if($removed_item_sell_price!=0){
                        $order_total5= array('order_batch_id'=>$order_history_details_max_id,'order_id'=>$order_id,'code'=>'return_item_value','title'=>'Credit','value'=>$removed_item_sell_price,'sort_order'=>4,'created'=>date('Y-m-d H:i:s'));
                        $this->Common_model->addEditRecords('order_total',$order_total5);
                    }
                    if($new_item_gt!=0){
                        $order_total6= array('order_batch_id'=>$order_history_details_max_id,'order_id'=>$order_id,'code'=>'add_item_value','title'=>'Debit','value'=>$new_item_gt,'sort_order'=>5,'created'=>date('Y-m-d H:i:s'));
                        $this->Common_model->addEditRecords('order_total',$order_total6);
                    }
                    $final_order_update['redeem_payment']=$new_item_gt-$removed_item_sell_price;
                    if($final_order_update['redeem_payment']>0){
                        $payment_status='credit_refund';
                    }elseif($final_order_update['redeem_payment']<0){
                        $payment_status='debit_refund';
                    }else{
                        $payment_status='Paid';
                        $final_order_update['redeem_payment']=0;
                    }
                    $final_order_update['payment_status']=$payment_status;
                }
                $this->Common_model->addEditRecords('orders',$final_order_update,array('id'=>$order_id));
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $this->display_output('0','Something went wrong. Please try again.'); 
        } else {
            $this->db->trans_commit();
            $sp_user_details=getNameEmailAddress($sp_details['user_id']);
            $from_email =getNotificationEmail();
            $to_email =$sp_user_details['email'];
            $subject = WEBSITE_NAME.' :  #'.$event_number.'event Updated';
            $data['name']= $sp_user_details['staging_id'];
            $data['message']= '<b>'.$event_number.'</b>'.' '.ucwords($event_title).' is updated by '.ucwords($user_details['fullname']).'. Please give your response.';
            $body = $this->load->view('template/common', $data,TRUE);
            $this->Common_model->sendEmail($to_email,$subject,$body,$from_email);
           
            $from_email =getNotificationEmail();
            $to_email =$user_details['email'];
            $subject = WEBSITE_NAME.' :  #'.$event_number.' New event created';
            $data['name']= ucwords($user_details['fullname']);
            $data['message']= '<b>'.$event_number.'</b>'.' '.ucwords($event_title).' is updated by you. It is pending for service provider\'s approval.';
            $body = $this->load->view('template/common', $data,TRUE);
            $this->Common_model->sendEmail($to_email,$subject,$body,$from_email);

            $title=$event_number.'-'.ucwords($event_title).' is updated.';
           // $time=date('h:i A',strtotime($start_time)).' - '.date('h:i A',strtotime($end_time));
            $content='For '.date('M d, Y',strtotime($event_date));
           // .' Time : '.$time;
            $notdata['type']='event_update';
            $this->Common_model->push_notification_send($sp_details['user_id'],$notdata,$event_id,$title,$content,'',$user_id);

            $this->display_output('1','Event updated successfully.');
        }
    }
    public function edit_event(){
        $event_id  = $this->clear_input($this->input->post('event_id'));
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $timezone  = $this->clear_input($this->input->post('timezone'));
        
        $event_date  = $this->clear_input($this->input->post('event_date'));
        $event_time  = $this->clear_input($this->input->post('event_time'));
        $items  = $this->clear_input($this->input->post('items'));
        $remove_items  = $this->clear_input($this->input->post('remove_items'));
        $service_id  = $this->clear_input($this->input->post('service_id'));
        $event_type  = $this->clear_input($this->input->post('event_type'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($timezone)){
            $this->display_output('0','Please enter time zone.');
        }
        if(empty($event_id)) {
            $this->display_output('0','Please enter event id.');
        }
        if(empty($event_date)) {
            $this->display_output('0','Please enter event date.');
        }if(empty($event_time)) {
            $this->display_output('0','Please enter event time.');
        }
        if(empty($service_id)) {
            $this->display_output('0','Please enter service id.');
        }if(empty($event_type)) {
            $this->display_output('0','Please enter event type.');
        }
        if(empty($items)) {
            $this->display_output('0','Please enter items.');
        }
        $tax=$this->Common_model->getFieldValue('settings','tax',array('id'=>1),'',true);
        $current_date=date('Y-m-d H:i:s');
        if(!$event_details=$this->Common_model->getRecords('events','event_date,event_number',array('id'=>$event_id,'is_deleted'=>0),'',true)){
            $this->display_output('0','Event isn\'t available.');
        }
        //$event_details['event_date']='2020-01-01 08:30:00';
        $diff=strtotime($event_details['event_date'])-strtotime($current_date);
        if($diff < 172800) {
            $this->display_output('0','Edit time of event is passed away.');
        }
        $item_arr=json_decode($items);
        $removed_item_sell_price=0;
        $remove_item_ids=explode(',', $remove_items);
        $all_remove_item=array();
        if(isset($remove_items) && !empty($remove_items) && !empty($remove_item_ids)){
            for($k=0;$k<count($remove_item_ids);$k++){
                 $removed_item_record=$this->Common_model->getRecords('order_details','service_id,order_id,event_id,item_name,price,qty,total_qty,unit,customer_qty,total_price,sell_price,tax,discount,offer_name',array('id'=>$remove_item_ids[$k]),'',true);
                $removed_item_sell_price+=$removed_item_record['sell_price'];
                $all_remove_item[]=$removed_item_record;
            }
        }
       
        $order_details=$this->Common_model->getRecords('orders','id,payment_status,total_amount,redeem_payment',array('event_id'=>$event_id),'',true);
        $order_id=$order_details['id'];
        $offer_where="status='Active' and is_deleted=0 and  service_id = '".$service_id."'  and (start_date <= '".date('Y-m-d H:i:s')."' and end_date >= '".date('Y-m-d H:i:s')."')";
        $offer_details=$this->Common_model->getRecords('offer','discount,offer_name',$offer_where,'',true);
        
        $user_details=$this->Common_model->getRecords('users','fullname,email,mobile,staging_id',array('user_id'=>$user_id),'',true);
        $sp_details=$this->Common_model->getRecords('services','user_id',array('id'=>$service_id),'',true);
        $event_date=convertLocalTimezoneToGMT($event_date.' '.$event_time,$timezone,$showTime=true);
        $event_datetime=date('Y-m-d H:i:s',strtotime($event_date));
        $update_data=array(
            'user_id'=>$user_id,
            'service_id'=>$service_id,
            'event_date'=>$event_datetime,
            'modified'=>date('Y-m-d H:i:s'),
            );
        $item_arr=json_decode($items);
        $this->db->trans_begin();
        if($this->Common_model->addEditRecords('events',$update_data,array('id'=>$event_id))){
            $user_data=array('user_id'=>$user_id,
                            'fullname'=>$user_details['fullname'],
                            'email'=>$user_details['email'],
                            'mobile'=>$user_details['mobile'],
                            'event_date_time'=>$event_datetime,
                            'payment_status'=>'Unpaid',
                            'service_id'=>$service_id,
                            'ip_address'=>$this->input->ip_address(),
                            'modified'=>date('Y-m-d H:i:s'),
                            'event_date'=>$event_datetime,
                );
            if($this->Common_model->addEditRecords('orders',$user_data,array('event_id'=>$event_id))){
                $subtotal=0;
                $discount=0;
                $discount_title='Discount';
                if(!empty($offer_details['discount'])){
                    $discount=$offer_details['discount'];
                }
                $this->Common_model->addEditRecords('order_details',array('status'=>'Inactive'),array('order_id'=>$order_id));
                $this->Common_model->addEditRecords('order_history_details',array('status'=>'Inactive','modified'=>date('Y-m-d H:i:s')),array('order_id'=>$order_id));
                $this->Common_model->addEditRecords('order_total',array('status'=>'Inactive','modified'=>date('Y-m-d H:i:s')),array('order_id'=>$order_id));
                $max_group_id=$this->Common_model->getRecords('order_history_details','max(`order_group_id`) as order_group_id','','',true);
                $order_history_details_max_id=$max_group_id['order_group_id']+1;
                $old_item_sell_price=0;
                $old_items_ids = array_map('current', $item_arr);
                //$old_items=array();
                if(!empty($old_items_ids)){
                    $j=0;
                    foreach ($old_items_ids as $key => $value) {
                        if(!empty($value)){
                            $old_items_record=$this->Common_model->getRecords('order_details','service_id,order_id,event_id,item_name,price,qty,total_qty,unit,customer_qty,total_price,sell_price,tax,discount,offer_name',array('id'=>$value),'',true);
                          
                            $old_item_sell_price+=$old_items_record['sell_price'];
                            $old_items_record['order_group_id']=$order_history_details_max_id;
                            $old_items_record['event_date_time']=$event_datetime;
                            $old_items_record['created']=date('Y-m-d H:i:s');
                           
                            $this->Common_model->addEditRecords('order_details',$old_items_record);
                            //echo $this->db->last_query();exit;
                            $this->Common_model->addEditRecords('order_history_details',$old_items_record);
                            $j++;

                        }
                    }
                } 
                $this->Common_model->deleteRecords('order_details',array('order_id'=>$order_id,'status'=>'Inactive'));
                for($i=0;$i<count($item_arr);$i++){
                    if(empty($item_arr[$i]->order_details_id)){
                        $total_qty=($item_arr[$i]->qty*$item_arr[$i]->customer_qty);
                        $total_price=number_format($item_arr[$i]->customer_qty*$item_arr[$i]->price,2,'.','');
                        if(!empty($discount)){
                             $discounted_sell_price=number_format((($total_price*$discount)/100),2,'.','');
                             $discount_price=$total_price-$discounted_sell_price;
                             if($tax){
                                $tax_sell_price=number_format((($discount_price*$tax)/100),2,'.','');
                                $sell_price=$discount_price+$tax_sell_price;
                             }else{
                                $sell_price=$discount_price;
                             }
                        }else{
                            $sell_price=$total_price;
                            if($tax){
                                $tax_sell_price=number_format((($total_price*$tax)/100),2,'.','');
                                $sell_price=$total_price+$tax_sell_price;
                             }
                        }
                        $sub_total+=$sell_price;
                        $sub_item_total_price=$sub_total;
                    
                        $insert_array=array('item_name' => $item_arr[$i]->item,
                                            'price' =>$item_arr[$i]->price,
                                            'unit' =>$item_arr[$i]->unit,
                                            'qty' =>$item_arr[$i]->qty,
                                            'customer_qty' =>$item_arr[$i]->customer_qty,
                                            'total_qty' =>$total_qty,
                                            'total_price' =>$total_price,
                                            'sell_price' =>$sell_price,
                                            'tax' =>$tax,
                                            'discount' =>$discount,
                                            'offer_name' =>$offer_details['offer_name'],
                                            'service_id'=>$service_id,
                                            'order_id'=>$order_id,
                                            'order_group_id'=>$order_history_details_max_id,
                                            'event_id'=>$event_id,
                                            'event_date_time'=>$event_datetime,
                                            'created'=>date('Y-m-d H:i:s'));
                        $this->Common_model->addEditRecords('order_details',$insert_array);
                        $this->Common_model->addEditRecords('order_history_details',$insert_array);
                    }
                }
                if(isset($remove_items) && !empty($remove_items) &&!empty($remove_item_ids)){
                    foreach($all_remove_item as $remove_insert_array){
                        $remove_insert_array['order_group_id']=$order_history_details_max_id;
                        $remove_insert_array['event_date_time']=$event_datetime;
                        $remove_insert_array['action']='Remove';
                        $this->Common_model->addEditRecords('order_history_details',$remove_insert_array);
                    }
                }
                $order_status=array(
                    'order_batch_id'=>$order_history_details_max_id,
                    'order_id'=>$order_id,
                    'order_id'=>$order_id,
                    'status'=>'Pending',
                    'created'=>date('Y-m-d H:i:s'),
                    'ip_addres'=>$this->input->ip_address(),
                    );
                $this->Common_model->addEditRecords('order_status_history',$order_status);
                $removed_item_sell_price;
                $sub_item_total_price;//////add_item_price
                $old_item_sell_price;

                $gt= $sub_item_total_price+$old_item_sell_price;
                $order_total1= array('order_batch_id'=>$order_history_details_max_id,'order_id'=>$order_id,'code'=>'total','title'=>'Total','value'=>$gt,'sort_order'=>1,'created'=>date('Y-m-d H:i:s'));
               
                $this->Common_model->addEditRecords('order_total',$order_total1);
                $order_status=$order_details['payment_status'];
               switch ($order_status) {
                    case 'Unpaid':
                        $this->Common_model->addEditRecords('orders',array('total_amount'=>$gt,'modified'=>date('Y-m-d H:i:s')),array('id'=>$order_id));
                        //echo $this->db->last_query();exit;
                        break;
                    case 'Paid':
                            $order_update=array('total_amount'=>$gt,'modified'=>date('Y-m-d H:i:s'));
                            if($sub_item_total_price >0 && $removed_item_sell_price==0){
                                $order_update['payment_status']='debit_refund';
                                $order_update['redeem_payment']=$sub_item_total_price;
                            }
                            if($sub_item_total_price ==0 && $removed_item_sell_price>0){
                                $order_update['payment_status']='credit_refund';
                                $order_update['redeem_payment']=$removed_item_sell_price;
                            }
                            if($sub_item_total_price > 0 && $removed_item_sell_price >0){
                                $unpaid_result=$sub_item_total_price-$removed_item_sell_price;
                                if($unpaid_result>0){
                                    $order_update['payment_status']='debit_refund';
                                    $order_update['redeem_payment']=$unpaid_result;
                                }elseif($unpaid_result<0){
                                    $order_update['payment_status']='credit_refund';
                                    $order_update['redeem_payment']=$unpaid_result;
                                }
                                elseif($unpaid_result==0){
                                    $order_update['payment_status']='Paid';
                                    $order_update['redeem_payment']=$unpaid_result;
                                }
                            }
                            $this->Common_model->addEditRecords('orders',$order_update,array('id'=>$order_id));
                        break;
                    case 'credit_refund':
                            $order_update=array('total_amount'=>$gt,'modified'=>date('Y-m-d H:i:s'));
                            if($sub_item_total_price >0 && $removed_item_sell_price==0){
                                $credit_result=($order_details['redeem_payment']-$sub_item_total_price);
                                $order_update['redeem_payment']=$credit_result;
                                if($credit_result < 0){
                                    $order_update['payment_status']='debit_refund';
                                }if($credit_result== 0){
                                    $order_update['payment_status']='Paid';
                                }
                                $order_update['redeem_payment']=$credit_result;
                            }
                            if($sub_item_total_price ==0 && $removed_item_sell_price > 0){
                                $credit_result=($order_details['redeem_payment']+$removed_item_sell_price);
                                $order_update['payment_status']='credit_refund';
                                $order_update['redeem_payment']=$credit_result;
                            }
                            if($sub_item_total_price > 0 && $removed_item_sell_price >0){
                                $credit_result=(($sub_item_total_price-$removed_item_sell_price)-$order_details['redeem_payment']);

                                if($credit_result < 0){
                                    $order_update['payment_status']='debit_refund';
                                    $order_update['redeem_payment']=$credit_result;
                                }
                                elseif($credit_result > 0){
                                    $order_update['payment_status']='credit_refund';
                                    $order_update['redeem_payment']=$credit_result;
                                }elseif($credit_result==0){
                                    $order_update['payment_status']='Paid';
                                    $order_update['redeem_payment']=0;
                                }


                            }
                            $this->Common_model->addEditRecords('orders',$order_update,array('id'=>$order_id));
                        break;
                    case 'debit_refund':
                            $order_update=array('total_amount'=>$gt,'modified'=>date('Y-m-d H:i:s'));
                            if($sub_item_total_price >0 && $removed_item_sell_price==0){
                                $debit_result=($sub_item_total_price+$order_details['redeem_payment']);
                                $order_update['redeem_payment']=$debit_result;
                                $order_update['payment_status']='debit_refund';
                            }
                            if($sub_item_total_price ==0 && $removed_item_sell_price > 0){
                                $debit_result=($order_details['redeem_payment']-$removed_item_sell_price);
                                if($debit_result < 0){
                                    $order_update['payment_status']='credit_refund';
                                    $order_update['redeem_payment']=$debit_result;
                                }elseif($debit_result >0){
                                    $order_update['payment_status']='debit_refund';
                                    $order_update['redeem_payment']=$debit_result;
                                }
                                
                            }
                            if($sub_item_total_price > 0 && $removed_item_sell_price >0){
                                $debit_result=($removed_item_sell_price-$order_details['redeem_payment']);

                                if($debit_result < 0){
                                    $amt=$order_details['redeem_payment']+$sub_item_total_price;
                                    $order_update['redeem_payment']=$amt;
                                }else{
                                    $amt=$sub_item_total_price-$order_details['redeem_payment'];
                                    $order_update['redeem_payment']=$amt;
                                }
                                $order_update['payment_status']='debit_refund';
                                
                                /*$debit_result=($sub_item_total_price-$removed_item_sell_price);
                                if($debit_result > 0){
                                    $amt=$order_details['redeem_payment']+$debit_result;
                                    $order_update['payment_status']='debit_refund';
                                    $order_update['redeem_payment']=$amt;
                                }else{
                                    $amt=$order_details['redeem_payment']-$debit_result;
                                    if($amt< 0){
                                        $order_update['payment_status']='credit_refund';
                                        $order_update['redeem_payment']=$amt;
                                    }
                                }*/
                            }
                            $this->Common_model->addEditRecords('orders',$order_update,array('id'=>$order_id));
                        break;
                    default:
                    break;
                }
                
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $this->display_output('0','Something went wrong. Please try again.'); 
        } else {
            $this->db->trans_commit();
            $event_number=$event_details['event_number'];
            $sp_user_details=getNameEmailAddress($sp_details['user_id']);
            $from_email =getNotificationEmail();
            $to_email =$sp_user_details['email'];
            $subject = WEBSITE_NAME.' :  #'.$event_number.'event Updated';
            $data['name']= $sp_user_details['staging_id'];
            $data['message']= '<b>'.$event_number.'</b>'.' '.ucwords($event_title).' is updated by '.ucwords($user_details['fullname']).'. Please give your response.';
            $body = $this->load->view('template/common', $data,TRUE);
            $this->Common_model->sendEmail($to_email,$subject,$body,$from_email);
           
            $from_email =getNotificationEmail();
            $to_email =$user_details['email'];
            $subject = WEBSITE_NAME.' :  #'.$event_number.' event updated';
            $data['name']= ucwords($user_details['fullname']);
            $data['message']= '<b>'.$event_number.'</b>'.' '.ucwords($event_title).' is updated by you. It is pending for service provider\'s approval.';
            $body = $this->load->view('template/common', $data,TRUE);
            $this->Common_model->sendEmail($to_email,$subject,$body,$from_email);

            $title=$event_number.'-'.ucwords($event_title).' is updated.';
           // $time=date('h:i A',strtotime($start_time)).' - '.date('h:i A',strtotime($end_time));
            $content='For '.date('M d, Y',strtotime($event_date));
           // .' Time : '.$time;
            $notdata['type']='event_update';
            $this->Common_model->push_notification_send($sp_details['user_id'],$notdata,$event_id,$title,$content,'',$user_id);

            $this->display_output('1','Event updated successfully.');
        }
    }
    public function event_details(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $event_id  = $this->clear_input($this->input->post('event_id'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($event_id)) {
            $this->display_output('0','Please enter event id.');
        }

        $detail=$this->App_model->booking_details($event_id,$user_id,'event');
        $detail['is_provide_rating']=0;
        if($this->Common_model->getRecords('reviews','id',array('event_id'=>$event_id),'',true)){
            $detail['is_provide_rating']=1; 
        }
        $order_id = $this->Common_model->getFieldValue('orders','id',array('event_id'=>$event_id),'',true);
        $order_record=$this->App_model->booking_detail_order($order_id);
        $detail['order_id']=$order_record['order_id'];
        //echo $this->db->last_query();
       // echo '<pre>';print_r($detail);exit;
        $detail['pending']="0";
        $detail['accepted']="0";
        $detail['rejected']="0";
        $detail['total_attendee']="0";
        $where_request_status="event_id =$event_id and (sender_id=$user_id OR receiver_id =$user_id)";
        if($my_request_status=$this->Common_model->getRecords('event_attendee','id as request_id,status,event_owner',$where_request_status,'',true)){

            $detail['my_request_status']=$my_request_status['status'];
        }else{
            $detail['my_request_status']="";
        }
        if($request_status = $this->Common_model->getRecords('event_attendee','count("status") as count,status',array('event_id'=>$event_id),'',false,'status')){
           foreach($request_status as $row){
                if($row['status']=='Accepted'){
                    $detail['accepted']=$row['count'];
                    if($row['count']>3){
                        $detail['total_attendee']=$row['count']-3;
                    }else{
                        $detail['total_attendee']="0";
                    }
                    
                }if($row['status']=='Rejected'){
                    $detail['rejected']=$row['count'];
                }
                if($row['status']=='Pending'){
                    $detail['pending']=$row['count'];
                }
           }
        }
        
       // echo $this->db->last_query();exit;
        if(!empty($detail)) {
            $response=array('event_details'=>$detail);
            if(!empty($detail['user_id'])){
                $attendee_detail=$this->App_model->get_event_attendee($detail['user_id'],$event_id,'limit');
                $response['attendee_detail']=$attendee_detail;
            }
            $this->display_output('1','Event details', array('details'=>$response));
        } else {
            $this->display_output('0','Event details not found');
        }
    }
    public function celebrant_canceled_event(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $event_id  = $this->clear_input($this->input->post('event_id'));
        $order_id  = $this->clear_input($this->input->post('order_id'));
        $today_date=date('Y-m-d');
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($order_id)) {
            $this->display_output('0','Please enter order id.');
        }
        if(empty($event_id)) {
            $this->display_output('0','Please enter event id.');
        }
       
        $check_event_record = $this->Common_model->getRecords('events',"if(events.is_completed=1,'Completed',if(events.is_canceled=1,'Canceled',if(events.event_date>'".$today_date."','Not started','On Going'))) as event_status,event_title",array('id'=>$event_id),'',true);
        if($check_event_record['event_status']=='On Going'){
            $this->display_output('0','Event is on going. You cannot be canceled it.');
        }
        if($check_event_record['event_status']=='Completed'){
            $this->display_output('0','Event already completed.');
        }
        if($check_event_record['event_status']=='Canceled'){
            $this->display_output('0','Event already canceled.');
        }
        $check_order_record = $this->Common_model->getRecords('orders','order_status',array('id'=>$order_id),'',true);
        if($check_order_record['order_status']=='Canceled'){
            $this->display_output('0','Order status already canceled.');
        }
        $check_order_status_record = $this->Common_model->getRecords('order_status_history','status',array('order_id'=>$order_id),'',true);
        if($check_order_status_record['status']=='Pending'){
            $result=$this->Front_common_model->cancel_by_sp_payment($user_id,$order_id);
        }else{
            $result=$this->Front_common_model->cancel_by_cele_payment($user_id,$order_id);
        }
        if($result==1){
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has canceled his/her own event '".$check_event_record['event_title']."'.";
            actionLog('events',$event_id,'Event canceled',$log_msg,'User',$user_id);
            $this->display_output('1','Event canceled successfully.');
        }else{
            $this->display_output('0','Something went wrong. Please try again.');
        }
    }
    public function celebrant_completed_booking(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $event_id  = $this->clear_input($this->input->post('event_id'));
        $order_id  = $this->clear_input($this->input->post('order_id'));
        $today_date = date('Y-m-d H:i:s');
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($event_id)) {
            $this->display_output('0','Please enter event id.');
        }
        if(empty($order_id)) {
            $this->display_output('0','Please enter order id.');
        }
        $event_record=$this->Common_model->getRecords('events',"event_title,event_number,user_id,sp_id,if(events.is_completed=1,'Completed',if(events.is_canceled=1,'Canceled',if(events.event_date>'".$today_date."','Not started','On Going'))) as event_status",array('id'=>$event_id),'id DESC',true);
        if($event_record['event_status']!='On Going'){
            $this->display_output('0','Event status is '.$event_record['event_status'].'. You are not authorize to complete this event.');
        }
        if($event_record['user_id']!=$user_id){
             $this->display_output('0','You are not authorize to complete this event.');
        }
        $result=$this->Front_common_model->complete_payment($order_id);
        if($result==1){
            $title='#'.$event_record['event_number'].' '.ucwords($event_record['event_title']).' has been mark as completed.';
            $content='#'.$event_record['event_number'].' '.ucwords($event_record['event_title']).' has been mark as completed. Your payment will be credited in your account soon.';
            $notdata['type']='event_completed';
            $this->Common_model->push_notification_send($event_record['sp_id'],$notdata,$event_id,$title,$content,'',$user_id);
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has marked completed an event '".$event_record['event_title']."'.";
            actionLog('events',$event_id,'Event Completed',$log_msg,'User',$user_id);
            $this->display_output('1','Event completed successfully.');
        }else{
            $this->display_output('0','Something went wrong. Please try again.');
        }
    }
    public function celebrant_order_history_list(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $status  = $this->clear_input($this->input->post('status'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $this->App_model->check_user_status();
        $list=$this->App_model->celebrant_order_history_list($user_id);
        //echo $this->db->last_query();exit;
        if(!empty($list)) {
            $this->display_output('1','Order history list', array('details'=>$list));
        } else {
            $this->display_output('0','Order history not found.');
        }
        
    }
    public function celebrant_order_history_details(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $booking_history_id  = $this->clear_input($this->input->post('booking_history_id'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($booking_history_id)) {
            $this->display_output('0','Please enter booking history id.');
        }
        $this->App_model->check_user_status();
       
        if($details=$this->App_model->celebrant_order_history_details($user_id,$booking_history_id)){
            
            $items = $this->Common_model->getRecords('order_details','item_name,price,qty,unit,customer_qty,total_price,sell_price,tax,discount',array('order_id'=>$details['order_id'],'is_deleted'=>0),'',false);
            $response=array('booking_details'=>$details,'items'=>$items);
            if($details['order_status']=='Canceled'){
                $canceled_order = $this->Common_model->getRecords('payment_history','transaction_id,total_order_amount,paid_amount,payment_type,user_will_get,paypal_fee_amount,payment_status',array('transfer_type'=>'Admin_To_Customer','order_id'=>$details['order_id']),'',true);
                $response['cancel_order_details']=$canceled_order;
            }
            $order_total = $this->Common_model->getRecords('order_total','code,title,value,discount_value',array('order_id'=>$details['order_id'],'type'=>'Actual'),'',false);
            $response['order_total']=$order_total;

            //echo $this->db->last_query();
            if(!empty($details)) {
                $this->display_output('1','Order history detail', array('details'=>$response));
            } else {
                $this->display_output('0','Order history detail not found.');
            }
        }else{
            $this->display_output('0','Something went wrong. Please try again.');
        }
    }
    public function csp_order_history_list(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $status  = $this->clear_input($this->input->post('status'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $this->App_model->check_user_status();
        $list=$this->App_model->csp_order_history_list($user_id);
        // /echo $this->db->last_query();exit;
        if(!empty($list)) {
            $this->display_output('1','Order history list', array('details'=>$list));
        } else {
            $this->display_output('0','Order history not found.');
        }
        
    }
    public function csp_order_history_details(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $booking_history_id  = $this->clear_input($this->input->post('booking_history_id'));
        $created_by_me  = $this->clear_input($this->input->post('created_by_me'));
        //created by me 1 for my created event
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($booking_history_id)) {
            $this->display_output('0','Please enter booking history id.');
        }
        $this->App_model->check_user_status();
       
        if($details=$this->App_model->csp_order_history_details($user_id,$booking_history_id,$created_by_me)){
            
            $items = $this->Common_model->getRecords('order_details','item_name,price,qty,unit,customer_qty,total_price,sell_price,tax,discount',array('order_id'=>$details['order_id'],'is_deleted'=>0),'',false);
            $response=array('booking_details'=>$details,'items'=>$items);
            if($details['order_status']=='Canceled'){
                $canceled_order = $this->Common_model->getRecords('payment_history','transaction_id,total_order_amount,paid_amount,payment_type,user_will_get,paypal_fee_amount,payment_status',array('transfer_type'=>'Admin_To_Customer','order_id'=>$details['order_id']),'',true);
                $response['cancel_order_details']=$canceled_order;
            }
            $order_total = $this->Common_model->getRecords('order_total','code,title,value,discount_value',array('order_id'=>$details['order_id'],'type'=>'Actual'),'',false);
            $response['order_total']=$order_total;

            //echo $this->db->last_query();
            if(!empty($details)) {
                $this->display_output('1','Order history detail', array('details'=>$response));
            } else {
                $this->display_output('0','Order history detail not found.');
            }
        }else{
            $this->display_output('0','Something went wrong. Please try again.');
        }
    }

    public function get_event_attendee(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $event_id  = $this->clear_input($this->input->post('event_id'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($event_id)) {
            $this->display_output('0','Please enter event id.');
        }
        $attendee_detail=$this->App_model->get_event_attendee($user_id,$event_id,'all');
        if(!empty($attendee_detail)) {
            $this->display_output('1','Event attendee details', array('details'=>$attendee_detail));
        } else {
            $this->display_output('0','Event attendee details not found');
        }
    }
    public function my_bookings_list(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $keyword  = $this->clear_input($this->input->post('keyword'));
        $status  = $this->clear_input($this->input->post('status'));
        $user_type  = $this->clear_input($this->input->post('user_type'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($user_type)) {
            $this->display_output('0','Please enter user type.');
        }
        $list=$this->App_model->my_bookings($user_id,$user_type,$keyword,$status);
        // echo $this->db->last_query();exit;
        if(!empty($list)) {
            $this->display_output('1','Booking list', array('details'=>$list));
        } else {
            $this->display_output('0','Bookings not found');
        }
    }
     public function csp_my_event_list(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $keyword  = $this->clear_input($this->input->post('keyword'));
        $status  = $this->clear_input($this->input->post('status'));
        $user_type  = $this->clear_input($this->input->post('user_type'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($user_type)) {
            $this->display_output('0','Please enter user type.');
        }
        $list=$this->App_model->my_bookings($user_id,$user_type,$keyword,$status,1);
        // echo $this->db->last_query();exit;
        if(!empty($list)) {
            $this->display_output('1','Booking list', array('details'=>$list));
        } else {
            $this->display_output('0','Bookings not found');
        }
    }
   

    public function my_bookings_details(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $event_id  = $this->clear_input($this->input->post('event_id'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($event_id)) {
            $this->display_output('0','Please enter event id.');
        }
        
        $detail=$this->App_model->booking_details($event_id,$user_id,'booking');
        //echo $this->db->last_query();
        $order_id = $this->Common_model->getFieldValue('orders','id',array('event_id'=>$event_id),'',true);
        $order_record=$this->App_model->booking_detail_order($order_id);

        $detail['show_deal_btn']=checkDealStatus($event_id,1);
        //echo $this->db->last_query();exit;
        //$detail['canceled_by']=$detail['canceled_by'];
        $detail['order_status']=$order_record['status'];
        $detail['order_id']=$order_record['order_id'];
        $detail['order_number']=$order_record['order_number'];
        $detail['order_status_id']=$order_record['order_status_id'];
        $detail['pending']="0";
        $detail['accepted']="0";
        $detail['rejected']="0";
        if($request_status = $this->Common_model->getRecords('event_attendee','count("status") as count,status',array('event_id'=>$event_id),'',false,'status')){
           foreach($request_status as $row){
                if($row['status']=='Accepted'){
                    $detail['accepted']=$row['count'];
                }if($row['status']=='Rejected'){
                    $detail['rejected']=$row['count'];
                }
                if($row['status']=='Pending'){
                    $detail['pending']=$row['count'];
                }
           }
        }
        $items = $this->Common_model->getRecords('order_details','item_name,price,qty,unit,customer_qty,total_price,sell_price,tax,discount',array('event_id'=>$event_id,'is_deleted'=>0),'',false);
        if(!empty($items)){
            $sell_total=0;
            foreach($items as $data){
                $sell_total=$data['sell_price']+$sell_total;
            }
        }
        if(!empty($detail)) {
            $response=array('event_details'=>$detail,'items'=>$items,'selling_total'=>$sell_total);
            $this->display_output('1','Booking details', array('details'=>$response));
        } else {
            $this->display_output('0','Booking details not found');
        }
    }
    
    public function celebrant_booking_summary(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $event_id  = $this->clear_input($this->input->post('event_id'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }if(empty($event_id)) {
            $this->display_output('0','Please enter event id.');
        }
        $list=$this->App_model->booking_summary($event_id);
        //echo $this->db->last_query();exit;
        if(!empty($list)) {
            $item_record=$this->Common_model->getRecords('order_details','id as order_details_id,item_name,price,qty,customer_qty,total_price,unit',array('order_id'=>$list['order_id']),'',false);
            $order_record=$this->Common_model->getRecords('order_total','code,title,value,discount_value',array('order_id'=>$list['order_id'],'status'=>'Active'),'',false);
            $order_total=array();
            if(!empty($order_record)){
                foreach ($order_record as $key => $value) {
                    if($value['code']=='discount'){
                        $order_total['discount']=$value['value'];
                        if($value['value']!='0.00'){
                            $discount_offer_name= explode('-',$value['title']);
                            $order_total['discount_name']=$discount_offer_name[1];
                        }
                        
                    }else{
                        $order_total[$value['title']]=$value['value'];   
                    }
                   
                }
            }
           
            $service_offer=$this->App_model->get_order_items($list['order_id']);
           //echo $this->db->last_query();exit;
            $response =array('event_summary'=>$list,'items'=>$item_record,'order_total'=>$order_total);
            $this->display_output('1','Booking summary', array('details'=>$response));

        } else {
            $this->display_output('0','Booking summary not found');
        }
    }

    //////////////////////////////////celebrant Events request////////////////////////////
    public function invite_friend_list(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $keyword  = $this->clear_input($this->input->post('keyword'));
        $event_id  = $this->clear_input($this->input->post('event_id'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($event_id)) {
            $this->display_output('0','Please enter event id.');
        }
        $list=$this->App_model->get_invite_friend_list($user_id,$event_id,$keyword='');
        //echo $this->db->last_query();exit;
        if(!empty($list)) {
            $this->display_output('1','Invite friend list', array('details'=>$list));
        } else {
            $this->display_output('0','Friends not found.');
        }
    }
    public function invite_friend(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $event_id  = $this->clear_input($this->input->post('event_id'));
        $receiver_id  = $this->clear_input($this->input->post('receiver_id'));
        $request_type  = $this->clear_input($this->input->post('request_type'));//invite or request_to_join
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($event_id)) {
            $this->display_output('0','Please enter event id.');
        }
        if(empty($receiver_id)) {
            $this->display_output('0','Please enter receiver id.');
        }
        if(empty($request_type)) {
            $this->display_output('0','Please enter request type.');
        }
        if($request_type!='invite' &&  $request_type!='request_to_join'){
            $this->display_output('0','Request type either invite or request_to_join.');
        }
        if(!$event_record = $this->Common_model->getRecords('events','id,event_title,user_id,is_canceled,event_date',array('id'=>$event_id,'status'=>'Active','is_deleted'=>0),'',true)){
            $this->display_output('0','Event isn\'t available.');
        }
        if($event_record['is_canceled']==1){
            $this->display_output('0','Event has been canceled.');
        }
        $current_date=date('Y-m-d H:i:s');
        if($current_date > $event_record['event_date']){
            $this->display_output('0','Event time has been passed.');
        }
        $check_invitation="((sender_id='".$user_id."' and receiver_id ='".$receiver_id."' )or (receiver_id ='".$user_id."' and sender_id ='".$receiver_id."')) and event_id ='".$event_id."'";
        $check_invitation_record = $this->Common_model->getRecords('event_attendee','id,status',$check_invitation,'',true);
       //echo $this->db->last_query();exit;
        $insert_data=array(
            'event_id'=>$event_id,
            'sender_id'=>$user_id,
            'receiver_id'=>$receiver_id,
            'created' =>date('Y-m-d H:i:s')
        );
        $user_fullname=$this->Common_model->getFieldValue('users','fullname',array('user_id'=>$user_id),'',true);
        $receiver_staging=$this->Common_model->getFieldValue('users','staging_id',array('user_id'=>$receiver_id),'',true);
        if($request_type=='invite'){
            if($event_record['user_id']!=$user_id){
                $this->display_output('0','You are not authorized to invite friends for this event.');
            }
            if(!empty($check_invitation_record) && $check_invitation_record['status']!='Rejected'){
                $this->display_output('0','You already invited this user.');
            }
            $insert_data['event_owner']='Self';
            if($last_id=$this->Common_model->addEditRecords('event_attendee',$insert_data)) {

                $title='Event Invitation.';
                $content=ucfirst($user_fullname).' invites you in '.$event_record['event_title'];
                $notdata['type']='event_invite';
                $this->Common_model->push_notification_send($receiver_id,$notdata,$event_id,$title,$content,'',$user_id);
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has invited '".$receiver_staging."' to join event '".$event_record['event_title']."'.";
                actionLog('event_attendee',$last_id,'Invite User',$log_msg,'User',$user_id);
                $this->display_output('1','Invitation sent successfully.');
            } else {
                $this->display_output('0','Something went wrong. Please try again.');
            }
        }else{
            if(!empty($check_invitation_record) && $check_invitation_record['status']!='Rejected'){
                $this->display_output('0','You already requested for join this event.');
            }
            $insert_data['event_owner']='Other';
            if($last_id=$this->Common_model->addEditRecords('event_attendee',$insert_data)) {
                $title='Request to join event.';
                $content=ucfirst($user_fullname).' requested to join '.$event_record['event_title'];
                $notdata['type']='request_to_join';
                $this->Common_model->push_notification_send($receiver_id,$notdata,$event_id,$title,$content,'',$user_id);
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has requested to join event '".$event_record['event_title']."'.";
                actionLog('event_attendee',$last_id,'Request to Join',$log_msg,'User',$user_id);
                $this->display_output('1','Invitation request sent successfully.');
            } else {
                $this->display_output('0','Something went wrong. Please try again.');
            }
        }
        
    }
    ////All event request shown mine or others both, on event owner other not show both btns////////////
    public function event_request_received_list(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $keyword  = $this->clear_input($this->input->post('keyword'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $list=$this->App_model->event_request_received_list($user_id,$keyword='');
        //echo $this->db->last_query();exit;
        if(!empty($list)) {
            $this->display_output('1','Event request list', array('details'=>$list));
        } else {
            $this->display_output('0','Event request not found.');
        }
    }
    public function accept_reject_event_request(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $request_id  = $this->clear_input($this->input->post('request_id'));
        $status  = $this->clear_input($this->input->post('status'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($request_id)) {
            $this->display_output('0','Please enter request id.');
        }if(empty($status)) {
            $this->display_output('0','Please enter status.');
        }
        if($status!='Accepted' &&  $status!='Rejected'){
            $this->display_output('0','Status either Accepted or Rejected.');
        }
        if(!$record=$this->Common_model->getRecords('event_attendee', 'id,status,event_id,sender_id,event_owner', array('id'=>$request_id), '', true)){
            $this->display_output('0','Event request not available.');
        }
        $sent_by=$record['sender_id'];
        $sent_by_staging_id=getUserInfo($sent_by,'users','user_id','staging_id');
        if(!$event_record = $this->Common_model->getRecords('events','id,event_title,user_id,is_canceled,event_date,members',array('id'=>$record['event_id'],'status'=>'Active','is_deleted'=>0),'',true)){
            $this->display_output('0','Event isn\'t available.');
        }
        if($record['status']=='Accepted' && $status=='Accepted'){
             $this->display_output('0','Event request already accepted.');
        }
        if($record['status']=='Rejected' && $status=='Rejected'){
             $this->display_output('0','Event request already rejected.');
        }
        if($status=='Accepted'){
            if($event_record['is_canceled']==1){
                $this->display_output('0','Event has been canceled.');
            }
            $current_date=date('Y-m-d H:i:s');
            if($current_date > $event_record['event_date']){
                $this->display_output('0','Event time has been passed.');
            }
            $accpeted_member=$this->Common_model->getFieldValue('event_attendee','count(id)',array('event_id'=>$record['event_id'],'status'=>'Accepted'),'',true);
            if($event_record['members'] <= $accpeted_member){
                $this->display_output('0','Sorry required members are completed for this event.');
            } 
            $update_data=array('status'=>'Accepted','modified'=>date('Y-m-d H:i:s'));
            if($this->Common_model->addEditRecords('event_attendee',$update_data,array('id'=>$request_id))){
                $user_fullname=getUserInfo($user_id,'users','user_id','fullname');

                $title=ucwords($user_fullname).' accepted your event request for '.$event_record['event_title'].'.';
                $content=ucwords($user_fullname).' accepted your event request for '.$event_record['event_title'].'.';
                if($record['event_owner']=='Self'){
                    $notdata['type']='invite_event_request_accepted';
                }else{
                    $notdata['type']='request_event_request_accepted';
                }
            $this->Common_model->push_notification_send($sent_by,$notdata,$record['event_id'],$title,$content,'',$user_id);
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has accepted event request of '".$sent_by_staging_id."' for event '".$event_record['event_title']."'.";
                actionLog('event_attendee',$request_id,'Event Request Accept',$log_msg,'User',$user_id);
                $this->display_output('1','Event request accepted.');
            }else{
                $this->display_output('0','Something went wrong. Please try again.');
            }
        }else{

            $update_data=array('status'=>'Rejected','modified'=>date('Y-m-d H:i:s'));
            if($this->Common_model->addEditRecords('event_attendee',$update_data,array('id'=>$request_id))){
                $user_fullname=getUserInfo($user_id,'users','user_id','fullname');

                $title=ucwords($user_fullname).' rejected your event request for '.$event_record['event_title'].'.';
                $content=ucwords($user_fullname).' rejected your event request for '.$event_record['event_title'].'.';
                if($record['event_owner']=='Self'){
                    $notdata['type']='invite_event_request_rejected';
                }else{
                    $notdata['type']='request_event_request_rejected';
                }
            $this->Common_model->push_notification_send($sent_by,$notdata,$record['event_id'],$title,$content,'',$user_id);
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has rejeced event request of '".$sent_by_staging_id."' for event '".$event_record['event_title']."'.";
                actionLog('event_attendee',$request_id,'Event Request Reject',$log_msg,'User',$user_id); 
                $this->display_output('1','Event request rejected.');
            }else{
                $this->display_output('0','Something went wrong. Please try again.');
            }
            
        }
    }
    public function upcoming_public_event_list(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $keyword  = $this->clear_input($this->input->post('keyword'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $list=$this->App_model->get_upcoming_public_list($user_id);
       // echo $this->db->last_query();
        if(!empty($list)) {
            $this->display_output('1','Event list', array('details'=>$list));
        } else {
            $this->display_output('0','Events not found');
        }
    }
    public function public_event_list(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);
        $list=$this->App_model->get_public_events($user_id,$user_address['country'],$user_address['state'],$user_address['city']);
       // echo $this->db->last_query();
        if(!empty($list)) {
            $this->display_output('1','Public event list', array('details'=>$list));
        } else {
            $this->display_output('0','Public events not found');
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////Celebrant social media Section/////////////////////////////
    public function dashboard_search(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $keyword  = $this->clear_input($this->input->post('keyword'));
        $page  = $this->clear_input($this->input->post('page'));
        $limit  = $this->clear_input($this->input->post('limit'));
        $offset=$limit*$page;
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(!$limit){
            $limit=10;
        }
        if(empty($keyword)) {
            $this->display_output('0','Please enter keyword.');
        }
        //$where="(users.fullname like '%".$keyword."%' OR users.staging_id like '%".$keyword."%') and users.is_deleted=0 and users.status ='Active'";
        $where="(users.fullname like '%".$keyword."%') and users.is_deleted=0 and users.status ='Active'";
        if($user_data=$this->Common_model->getRecords('users','user_id,fullname,profile_picture,gender',$where,'',false,'',$offset,$limit)){
            $this->display_output('1','Users list', array('details'=>$user_data));
        }else{
            $this->display_output('0','No result found.');
        }



    }
    public function feeds(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }

        $other_user_id  = $this->clear_input($this->input->post('other_user_id'));
        if(!empty($other_user_id)){
            $user_id = $other_user_id;
        }
        $details=array();
        //$user_city_id=$this->Common_model->getRecords('user_address','city',array('user_id'=>$user_id,'status'=>'primary'),'',true);
       $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);
        $banner=$this->App_model->get_banner($user_address['country'],$user_address['state'],$user_address['city']);
        //echo $this->db->last_query();exit;
        $user_post_data=$this->App_model->get_feeds($user_id);
        //echo $this->db->last_query();exit;
        if(!empty($user_post_data)){
            $i=0;
            foreach ($user_post_data as $key => $value) {
                $where="post_id='".$value['post_id']."'";
                $like=$this->App_model->get_count('like_post',$where);
                $comment=$this->App_model->get_count('comments',$where);
                $where_shared="shared_post_id='".$value['post_id']."'";
                $share=$this->App_model->get_count('post',$where_shared);
                $details[$i]['like']=shortNumber($like['count']);
                $details[$i]['comment']=shortNumber($comment['count']);
                $details[$i]['share']=shortNumber($share['count']);
                $details[$i]['request_id']=$value['request_id']?$value['request_id']:0;
                $details[$i]['is_report_by_me']=0; 
                if($this->Common_model->getFieldValue('post_report','id',array('user_id'=>$user_id,'post_id'=>$value['post_id']),'',true)){
                    $details[$i]['is_report_by_me']=1; 
                }
                $details[$i]['is_saved_by_me']=0; 
                if($this->Common_model->getFieldValue('save_post','id',array('user_id'=>$user_id,'post_id'=>$value['post_id']),'',true)){
                    $details[$i]['is_saved_by_me']=1; 
                }
                if($value['shared_post_id']>0){
                    $details[$i]['is_orignal_post_deleted']= $this->Common_model->getFieldValue('post','is_deleted',array('id'=>$value['shared_post_id']),'',true);
                    $post_media= $this->Common_model->getRecords('post_media','media_path,video_thumbnail,video_large_thumbnail,media_type,format,duration',array('media_is_deleted'=>0,'post_id'=>$value['shared_post_id']),'',false);

                    $user_type = $this->Common_model->getFieldValue('users','user_type',array('user_id'=>$value['user_id']));

                    $details[$i]['is_like_by_me']=!empty($value['is_like_by_me'])?1:0;
                    $details[$i]['post_id']=$value['post_id'];
                    $details[$i]['user_id']=$value['user_id'];
                    $details[$i]['user_type']=$user_type;
                    $details[$i]['fullname']=$value['fullname'];
                    $details[$i]['post_type']=$value['post_type'];
                    $details[$i]['event_id']=$value['event_id'];
                    $details[$i]['sp_id']=$value['sp_id'];
                    $details[$i]['follower_id']=$value['follower_id'];
                    $details[$i]['profile_picture']=$value['profile_picture'];
                    $details[$i]['gender']=$value['gender'];
                    $details[$i]['created']=$value['created'];
                    $details[$i]['is_shared']=1;
                    $details[$i]['post_content']=$value['post_content'];
                    $details[$i]['shared_post_content']=$value['shared_post_content'];
                    $details[$i]['is_post_content_symbol']=strpos($value['post_content'],"^")?1:0;
                    $details[$i]['is_shared_post_content_symbol']=strpos($value['shared_post_content'],"^")?1:0;
                    $details[$i]['post_media']=$post_media;
        
                    
                }else{
                    $post_media= $this->Common_model->getRecords('post_media','media_path,video_thumbnail,video_large_thumbnail,media_type,format,duration',array('media_is_deleted'=>0,'post_id'=>$value['post_id']),'',false);

                    $user_type = $this->Common_model->getFieldValue('users','user_type',array('user_id'=>$value['user_id']));
                    $details[$i]['is_like_by_me']=!empty($value['is_like_by_me'])?1:0;
                    $details[$i]['post_id']=$value['post_id'];
                    $details[$i]['user_id']=$value['user_id'];
                    $details[$i]['user_type']=$user_type;
                    $details[$i]['fullname']=$value['fullname'];
                    $details[$i]['post_type']=$value['post_type'];
                    $details[$i]['event_id']=$value['event_id'];
                    $details[$i]['sp_id']=$value['sp_id'];
                    $details[$i]['follower_id']=$value['follower_id'];
                    $details[$i]['profile_picture']=$value['profile_picture'];
                    $details[$i]['gender']=$value['gender'];
                    $details[$i]['created']=$value['created'];
                    $details[$i]['is_shared']=0;
                    $details[$i]['post_content']=$value['post_content'];
                    $details[$i]['is_post_content_symbol']=strpos($value['post_content'],"^")?1:0;
                    $details[$i]['post_media']=$post_media;
                   
                }
              $i++;  
            }
        }
        if(!empty($details) || !empty($banner)) {
            $response =array('banner'=>$banner,'newsfeeds'=>$details);
            $this->display_output('1','News feeds', array('details'=>$response));

        } else {
            $this->display_output('0','Feeds not found');
        }
      
    }

    public function get_top_sp(){

        $user_id  = $this->clear_input($this->input->post('user_id'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $details=array();
        $user_address=$this->Common_model->getRecords('user_address','country,state,city',array('is_deleted'=>0,'status'=>'Primary','user_id'=>$user_id),'',true);

        $details=$this->App_model->top_rated_sp($user_address['country'],$user_address['state'],$user_address['city']);
        
        
        if(!empty($details)) {
            
            $this->display_output('1','Top Rated SP', array('list'=>$details));

        } else {
            $this->display_output('0','Data not found');
        }
    }
    public function my_post(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $details=array();
        
        //echo $this->db->last_query();exit;
        $user_post_data=$this->App_model->my_post($user_id);
        if(!empty($user_post_data)){
            $i=0;
            foreach ($user_post_data as $key => $value) {
                $where="post_id='".$value['post_id']."'";
                $like=$this->App_model->get_count('like_post',$where);
                $comment=$this->App_model->get_count('comments',$where);
                $where_shared="shared_post_id='".$value['post_id']."'";
                $share=$this->App_model->get_count('post',$where_shared);
                $details[$i]['like']=$like['count'];
                $details[$i]['comment']=$comment['count'];
                $details[$i]['share']=$share['count'];
               
                if($value['shared_post_id']>0){
                    $details[$i]['is_orignal_post_deleted']= $this->Common_model->getFieldValue('post','is_deleted',array('id'=>$value['shared_post_id']),'',true);
                    $post_media= $this->Common_model->getRecords('post_media','media_path,video_thumbnail,video_large_thumbnail,media_type,format,duration',array('media_is_deleted'=>0,'post_id'=>$value['shared_post_id']),'',false);

                    $details[$i]['post_id']=$value['post_id'];
                    $details[$i]['user_id']=$value['user_id'];
                    $details[$i]['fullname']=$value['fullname'];
                    $details[$i]['post_type']=$value['post_type'];
                    $details[$i]['event_id']=$value['event_id'];
                    $details[$i]['profile_picture']=$value['profile_picture'];
                    $details[$i]['gender']=$value['gender'];
                    $details[$i]['created']=$value['created'];
                    $details[$i]['is_shared']=1;
                    $details[$i]['post_content']=$value['post_content'];
                    $details[$i]['shared_post_content']=$value['shared_post_content'];
                    $details[$i]['post_media']=$post_media;
                    
                }else{
                    $post_media= $this->Common_model->getRecords('post_media','media_path,video_thumbnail,video_large_thumbnail,media_type,format,duration',array('media_is_deleted'=>0,'post_id'=>$value['post_id']),'',false);
                    $details[$i]['post_id']=$value['post_id'];
                    $details[$i]['user_id']=$value['user_id'];
                    $details[$i]['fullname']=$value['fullname'];
                    $details[$i]['post_type']=$value['post_type'];
                    $details[$i]['event_id']=$value['event_id'];
                    $details[$i]['profile_picture']=$value['profile_picture'];
                    $details[$i]['gender']=$value['gender'];
                    $details[$i]['created']=$value['created'];
                    $details[$i]['is_shared']=0;
                    $details[$i]['post_content']=$value['post_content'];
                    $details[$i]['post_media']=$post_media;
                }
              $i++;  
            }
        }
        if(!empty($details)) {
            $response =array('newsfeeds'=>$details);
            $this->display_output('1','My post', array('details'=>$response));

        } else {
            $this->display_output('0','Post not found');
        }
      
    }
    public function convert_duration($sec) {
        $sec= (double)$sec;
        $s = $sec%60;
        $m = floor(($sec%3600)/60);
        $h = floor(($sec%86400)/3600);
        // $d = floor(($sec%2592000)/86400);
        // $M = floor($sec/2592000);

        $duration = "$h:$m:$s";
        //echo date("H:i:s",strtotime($duration));
        return date("H:i:s",strtotime($duration));
    }
    public function create_post(){
        error_reporting(0);
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $staging_id  = $this->clear_input($this->input->post('staging_id'));
        $post_content  = $this->clear_input($this->input->post('post_content'));
        $address  = $this->clear_input($this->input->post('address'));
        $latitude  = $this->clear_input($this->input->post('latitude'));
        $longitude  = $this->clear_input($this->input->post('longitude'));
        $mobile_type =  $this->clear_input($this->input->post('mobile_type')); //android
        $rotation =  $this->clear_input($this->input->post('rotation'));
        $all_post=$this->clear_input($this->input->post('all_post'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($staging_id)) {
            $this->display_output('0','Please enter login ID.');
        }
       /* if(empty($address)) {
            $this->display_output('0','Please enter address.');
        }*/
        if(empty($latitude)) {
            $this->display_output('0','Please enter latitude.');
        }
        if(empty($longitude)) {
            $this->display_output('0','Please enter longitude.');
        }
        if(!isset($mobile_type)) {
            $mobile_type = 'ios';
            $rotation = -1;
        } else if($mobile_type=='android') {
            if(!isset($rotation)) {
                $err = array('data' =>array('status' => '0', 'msg' => 'Please enter rotation.'));
                echo json_encode($err); exit;
            } 
        }
        if(empty($post_content) && empty($all_post)){
             $this->display_output('0','Something went wrong.You try to submit blank post.');
        }
        //echo '<pre>';print_r($all_post);

        $insert_data=array(
            'user_id'=>$user_id,
            'post_content'=>$post_content,
            'post_address'=>$address,
            'latitude'=>$latitude,
            'longitude'=>$longitude,
            'created'=>date('Y-m-d H:i:s'),
            );
        if(isset($all_post) && !empty($all_post)){
            $all_post_data=json_decode($all_post);
        }
        $this->db->trans_begin();
        if($last_id = $this->Common_model->addEditRecords('post',$insert_data)){
            $post_number='Post'.str_pad($last_id,8,0,STR_PAD_LEFT);
            $this->Common_model->addEditRecords('post',array('post_number'=>$post_number),array('id'=>$last_id));
            if(!empty($all_post_data) && isset($all_post_data)){
                $s3 = new S3(AMAZON_ID, AMAZON_KEY); 
                for($i=0;$i<count($all_post_data);$i++){
                    $post_data=array(
                        'post_id'=>$last_id,
                        'user_id'=>$user_id,
                        'media_path'=>$all_post_data[$i]->media_name,
                        'media_type'=>$all_post_data[$i]->media_type,
                        'format'=>$all_post_data[$i]->format,
                        'created'=>date('Y-m-d H:i:s'),
                    );
                    if($all_post_data[$i]->media_type=='video') {
                        $video_duration=$all_post_data[$i]->duration;
                        $duration= $this->convert_duration($video_duration);
                        $post_data['duration']=$duration;
                        $s3thumbPath='';
                        $s3largeimagePath='';
                        if(!is_dir(MEDIA_THUMB_TEMP_PATH)){
                            mkdir(MEDIA_THUMB_TEMP_PATH);
                        }
                        $s3dir = $staging_id.'/';
                       
                        $images = createImage($all_post_data[$i]->media_name,MEDIA_THUMB_TEMP_PATH,"800x450",$duration,$mobile_type,$rotation);
                       
                        $thumb_image = $images[1];
                        $large_image = $images[0];
                        $thumb_imagePath = MEDIA_THUMB_TEMP_PATH.$thumb_image;
                        $large_imagePath = MEDIA_THUMB_TEMP_PATH.$large_image;
                        //upload images on the amazon s3
                        
                        if($thumb_imagePath !='') {
                            //store thumb image on the amazon s3 and delete temp file
                            $save_path = $s3dir.$thumb_image;
                            $s3->putObjectFile($thumb_imagePath, AMAZON_BUCKET, $save_path, S3::ACL_PUBLIC_READ);
                            $s3thumbPath=AMAZON_PATH.$save_path;
                            if(file_exists($thumb_imagePath)) {
                                unlink($thumb_imagePath);
                            }
                            $post_data['video_thumbnail']=$s3thumbPath;
                        }

                        if($large_imagePath !='') {
                            //store thumb image on the amazon s3 and delete temp file
                            $save_path = $s3dir.$large_image;
                            $s3->putObjectFile($large_imagePath, AMAZON_BUCKET, $save_path, S3::ACL_PUBLIC_READ);
                            $s3largeimagePath=AMAZON_PATH.$save_path;
                            if(file_exists($large_imagePath)) {
                                unlink($large_imagePath);
                            }
                            $post_data['video_large_thumbnail']=$s3largeimagePath;
                        }
                    }
                    $this->Common_model->addEditRecords('post_media',$post_data);
                }
            }
            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $this->display_output('0','Something went wrong. Please try again.'); 
            } else {
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has added a post '".$post_number."'.";
                actionLog('post',$last_id,'Add',$log_msg,'User',$user_id);
                $this->db->trans_commit();
                $this->display_output('1','Post created successfully.');
            }
        }
    }
    public function edit_post(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $post_id  = $this->clear_input($this->input->post('post_id'));
        $post_content  = $this->clear_input($this->input->post('post_content'));
        //$address  = $this->clear_input($this->input->post('address'));
        $latitude  = $this->clear_input($this->input->post('latitude'));
        $longitude  = $this->clear_input($this->input->post('longitude'));
        
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($post_id)) {
            $this->display_output('0','Please enter post id.');
        }
       /* if(empty($address)) {
            $this->display_output('0','Please enter address.');
        }*/
        if(empty($latitude)) {
            $this->display_output('0','Please enter latitude.');
        }
        if(empty($longitude)) {
            $this->display_output('0','Please enter longitude.');
        }
       
        if(empty($post_content)){
             $this->display_output('0','Please enter post content.');
        }
        //echo '<pre>';print_r($all_post);
        if(!$post_info = $this->Common_model->getRecords('post', 'post_type,post_number', array('id'=>$post_id),'',true)){
            $this->display_output('0','Post not available.');
        }
        if($post_info['post_type']==2){
             $this->display_output('0','You can\'t update event type post.');
        }

        $update_data=array(
            'user_id'=>$user_id,
            'post_content'=>$post_content,
            //'post_address'=>$address,
            'latitude'=>$latitude,
            'longitude'=>$longitude,
            'modified'=>date('Y-m-d H:i:s'),
            );
        if($this->Common_model->addEditRecords('post',$update_data,array('id'=>$post_id))){
            $post_number=$post_info['post_number'];
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has updated a post '".$post_number."'.";
            actionLog('post',$post_id,'Update',$log_msg,'User',$user_id);
            $this->display_output('1','Post updated successfully.');
        }else{
            $this->display_output('0','Something went wrong. Please try again.');
        }
        
    }
    public function delete_post(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $post_id  = $this->clear_input($this->input->post('post_id'));
        
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }if(empty($post_id)) {
            $this->display_output('0','Please enter post id.');
        }
        $where_post="id='".$post_id."' and (is_deleted=0 and status ='Active')";
        if(!$post_owner=$this->Common_model->getRecords('post','user_id,status,post_number',$where_post,'',true)){
             $this->display_output('0','Post isn\'t available now.');
        }
        if($post_media_data=$this->Common_model->getRecords('post_media','id,media_path,video_thumbnail,video_large_thumbnail',array('post_id'=>$post_id),'',false)){
            $s3 = new S3(AMAZON_ID, AMAZON_KEY); 
            foreach($post_media_data as $media_data){
                if($media_data['video_thumbnail']!='') {
                    $thumb_image = getDeletePath($media_data['video_thumbnail']);
                    $object = $s3->getObjectInfo(AMAZON_BUCKET,$thumb_image);
                    if($object) {
                        $s3->deleteObject(AMAZON_BUCKET, $thumb_image);
                    } 
                }
                if($media_data['video_large_thumbnail']!='') {
                    $large_image = getDeletePath($media_data['video_large_thumbnail']);
                    $object = $s3->getObjectInfo(AMAZON_BUCKET,$large_image );
                    if($object) {
                        $s3->deleteObject(AMAZON_BUCKET, $large_image);
                    } 
                }
                if($media_data['media_path']!='') {
                    $media_name = getDeletePath($media_data['media_path']);
                    $object = $s3->getObjectInfo(AMAZON_BUCKET,$media_name );
                    if($object) {
                        $s3->deleteObject(AMAZON_BUCKET, $media_name);
                    }
                }
            } 
        }
        $update_data=array(
            'is_deleted'=>1,
            'modified'=>date('Y-m-d H:i:s'),
            );
        if($this->Common_model->addEditRecords('post',$update_data,array('id'=>$post_id))){
            $this->Common_model->deleteRecords('notifications',array('record_id'=>$post_id));
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has deleted a post '".$post_owner['post_number']."'.";
            actionLog('post',$post_id,'Delete',$log_msg,'User',$user_id);
            $this->display_output('1','Post Deleted successfully.');
        }else{
            $this->display_output('0','Something went wrong. Please try again.');
        }
        
    }
    public function get_post_by_id(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $post_id  = $this->clear_input($this->input->post('post_id'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }if(empty($post_id)) {
            $this->display_output('0','Please enter post id.');
        }
        if($post_content= $this->App_model->post_detail($post_id,$user_id)) { 
                $where="post_id='".$post_id."'";
                $like=$this->App_model->get_count('like_post',$where);
                $comment=$this->App_model->get_count('comments',$where);
                $where_shared="shared_post_id='".$post_id."'";
                $share=$this->App_model->get_count('post',$where_shared);
                $post_content['like']=shortNumber($like['count']);
                $post_content['comment']=shortNumber($comment['count']);
                $post_content['share']=shortNumber($share['count']);
                if($post_content['shared_post_id']>0){
                    $post_media= $this->Common_model->getRecords('post_media','media_path,video_thumbnail,video_large_thumbnail,media_type,format,duration',array('media_is_deleted'=>0,'post_id'=>$post_content['shared_post_id']),'',false);
                    $post_content['is_shared']=1;
                    
                }else{
                    $post_media= $this->Common_model->getRecords('post_media','media_path,video_thumbnail,video_large_thumbnail,media_type,format,duration',array('media_is_deleted'=>0,'post_id'=>$post_content['post_id']),'',false);
                    $post_content['is_shared']=0;
                }
        
         $response = array('post_content'=>$post_content,'post_media'=>$post_media);
            $this->display_output('1','Post detail', array('details'=>$response));
        }else{
            $this->display_output('0','Something went wrong. Please try again.');
        }
        
    }
    public function share_post(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $post_id  = $this->clear_input($this->input->post('post_id'));
        $post_content  = $this->clear_input($this->input->post('post_content'));
        $address  = $this->clear_input($this->input->post('address'));
        $latitude  = $this->clear_input($this->input->post('latitude'));
        $longitude  = $this->clear_input($this->input->post('longitude'));
        
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($post_id)) {
            $this->display_output('0','Please enter post id.');
        }
       /* if(empty($address)) {
            $this->display_output('0','Please enter address.');
        }*/
        if(empty($latitude)) {
            $this->display_output('0','Please enter latitude.');
        }
        if(empty($longitude)) {
            $this->display_output('0','Please enter longitude.');
        }
         $where_post="id='".$post_id."' and (is_deleted=0 and status ='Active')";
        if(!$post_owner=$this->Common_model->getRecords('post','user_id,status,shared_post_id',$where_post,'',true)){
             $this->display_output('0','Post isn\'t available now.');
        }
        if($post_owner['shared_post_id']>0){
            $shared_id=$post_owner['shared_post_id'];
        }else{
            $shared_id=$post_id;
        }
        $insert_data=array(
            'user_id'=>$user_id,
            'shared_post_id'=>$shared_id,
            'post_content'=>$post_content,
            'post_address'=>$address,
            'latitude'=>$latitude,
            'longitude'=>$longitude,
            'created'=>date('Y-m-d H:i:s'),
            );
        if($last_id=$this->Common_model->addEditRecords('post',$insert_data)){
            $post_number='Post'.str_pad($last_id,8,0,STR_PAD_LEFT);
            $this->Common_model->addEditRecords('post',array('post_number'=>$post_number),array('id'=>$last_id));
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has shared a post '".$post_number."'.";
            actionLog('post',$last_id,'Share Post',$log_msg,'User',$user_id);
            $this->display_output('1','Post shared successfully.');
        }else{
            $this->display_output('0','Something went wrong. Please try again.');
        }
        
    }
    public function post_like(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $post_id  = $this->clear_input($this->input->post('post_id'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($post_id)) {
            $this->display_output('0','Please enter post id.');
        }
        if(!$post_owner=$this->Common_model->getRecords('post','user_id,post_number',array('id'=>$post_id,'is_deleted'=>0,'status'=>'Active'),'',true)){
             $this->display_output('0','Post isn\'t available now.');
        }
        $where=array('post_id'=>$post_id,'user_id'=>$user_id);
        if($this->Common_model->getRecords('like_post','*',$where,'',true)){
            if($this->Common_model->deleteRecords('like_post',$where)){
                 removeNotification($post_owner['user_id'],'Post Like',$post_id); 
            }
            //$total=$this->Common_model->getRecords('like_post','count(id) as likes',array('post_id'=>$post_id),'',true);
            $where_post_count="post_id='".$post_id."'";
            $total=$this->App_model->get_count('like_post',$where_post_count);
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has unlike a post '".$post_owner['post_number']."'.";
            actionLog('post',$post_id,'Unlike',$log_msg,'User',$user_id);
            $this->display_output('1','Post unlike successfully.',array('like_count'=>shortNumber($total['count'])));
        }else{
                $insert_data=array(
                'user_id'=>$user_id,
                'post_id'=>$post_id,
                'created'=>date('Y-m-d H:i:s'),
                );
            if($last_id=$this->Common_model->addEditRecords('like_post',$insert_data)){
                if($post_owner['user_id']!=$user_id){
                    $liked_by_fullname=getUserInfo($user_id,'users','user_id','fullname');
                    $title=ucwords($liked_by_fullname).' likes your post.';
                    $content=ucwords($liked_by_fullname).' likes your post.';
                    $notdata['type']='post_like';
                    $this->Common_model->push_notification_send($post_owner['user_id'],$notdata,$post_id,$title,$content,'',$user_id);
                }
                $where_post_count="post_id='".$post_id."'";
                $total=$this->App_model->get_count('like_post',$where_post_count);
               // $this->display_output('1','Post like successfully.',array('like_count'=>$total['likes']));
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has like a post '".$post_owner['post_number']."'.";
                actionLog('post',$post_id,'Like',$log_msg,'User',$user_id);
                $this->display_output('1','Post like successfully.',array('like_count'=>shortNumber($total['count'])));
            }
        }
        
    }

    public function like_list(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $post_id  = $this->clear_input($this->input->post('post_id')); // for like pass post_id , for comment pass comment_id
        $type  = $this->clear_input($this->input->post('type'));   //like, comment
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($post_id)) {
            $this->display_output('0','Please enter post id.');
        }
        if(empty($type)) {
            $this->display_output('0','Please enter type.');
        }
        if($type=='like'){
            $where="post_id='".$post_id."'";
            $table='like_post';
        }
        else{
            $where="comment_id='".$post_id."'";
            $table='comment_like';
        }
        if($user_details=$this->App_model->get_like_users($table,$where)){
            $this->display_output('1','Users list.',array('details'=>$user_details));
        }else{
            $this->display_output('0','Users list not found.');
        }
    }
    public function save_post(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $post_id  = $this->clear_input($this->input->post('post_id'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($post_id)) {
            $this->display_output('0','Please enter post id.');
        }
        $where_post="id='".$post_id."' and (is_deleted=1 OR status !='Active')";
        if($post_owner=$this->Common_model->getRecords('post','status',$where_post,'',true)){
             $this->display_output('0','Post isn\'t available now.');
        }
        if($this->Common_model->getRecords('save_post','id',array('post_id'=>$post_id,'user_id'=>$user_id),'',true)){
             $this->display_output('0','You already saved this post.');
        }

        $insert_data=array(
            'user_id'=>$user_id,
            'post_id'=>$post_id,
            'created'=>date('Y-m-d H:i:s'),
        );
        if($last_id=$this->Common_model->addEditRecords('save_post',$insert_data)){
            $post_number=$this->Common_model->getFieldValue('post','post_number',array('id'=>$post_id),'',true);
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has saved a post '".$post_number."'.";
            actionLog('post',$post_id,'Saved Post',$log_msg,'User',$user_id);
            $this->display_output('1','Post saved successfully.');
        }else{
            $this->display_output('0','Something went wrong. Please try again.');
        }
    }
    public function all_save_post(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $details=array();
        if($post_content=$this->App_model->save_post_list($user_id)){
            $i=0;
            foreach ($post_content as $key => $value) {
                $where="post_id='".$value['post_id']."'";
                $like=$this->App_model->get_count('like_post',$where);
                $comment=$this->App_model->get_count('comments',$where);
                $where_shared="shared_post_id='".$value['post_id']."'";
                $share=$this->App_model->get_count('post',$where_shared);
                $details[$i]['like']=shortNumber($like['count']);
                $details[$i]['comment']=shortNumber($comment['count']);
                $details[$i]['share']=shortNumber($share['count']);
                $details[$i]['fullname']=$value['fullname'];
                $details[$i]['profile_picture']=$value['profile_picture'];
                $details[$i]['created']=$value['created'];
                $details[$i]['save_post_time']=$value['save_post_time'];
                if($value['shared_post_id']>0){
                    $post_media= $this->Common_model->getRecords('post_media','media_path,video_thumbnail,video_large_thumbnail,media_type,format,duration',array('media_is_deleted'=>0,'post_id'=>$value['shared_post_id']),'',false);

                    $details[$i]['is_like_by_me']=!empty($value['is_like_by_me'])?1:0;
                    $details[$i]['post_id']=$value['post_id'];
                    $details[$i]['user_id']=$value['user_id'];
                    $details[$i]['fullname']=$value['fullname'];
                    $details[$i]['profile_picture']=$value['profile_picture'];
                    $details[$i]['gender']=$value['gender'];
                    $details[$i]['created']=$value['created'];
                    $details[$i]['is_shared']=1;
                    $details[$i]['post_content']=$value['post_content'];
                    $details[$i]['shared_post_content']=$value['shared_post_content'];
                    $details[$i]['post_media']=$post_media;

                    
                }else{
                    $post_media= $this->Common_model->getRecords('post_media','media_path,video_thumbnail,video_large_thumbnail,media_type,format,duration',array('media_is_deleted'=>0,'post_id'=>$value['post_id']),'',false);
                    $details[$i]['is_like_by_me']=!empty($value['is_like_by_me'])?1:0;
                    $details[$i]['post_id']=$value['post_id'];
                    $details[$i]['user_id']=$value['user_id'];
                    $details[$i]['fullname']=$value['fullname'];
                    $details[$i]['profile_picture']=$value['profile_picture'];
                    $details[$i]['gender']=$value['gender'];
                    $details[$i]['created']=$value['created'];
                    $details[$i]['is_shared']=0;
                    $details[$i]['post_content']=$value['post_content'];
                    $details[$i]['post_media']=$post_media;

                }
                $i++;
            }
            $this->display_output('1','Save post details.', array('details'=>$details));
        }else{
            $this->display_output('0','Saved post not available.');
        }
        
    }
    public function delete_save_post(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $save_post_id  = $this->clear_input($this->input->post('save_post_id'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($save_post_id)) {
            $this->display_output('0','Please enter save post id.');
        }
        if($save_post=$this->Common_model->getRecords('save_post','id,post_id',array('id'=>$save_post_id,'user_id'=>$user_id),'',true)){
            $post_number=$this->Common_model->getFieldValue('post','post_number',array('id'=>$save_post['post_id']),'',true);
            if($this->Common_model->deleteRecords('save_post',array('id'=>$save_post_id))){
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has unsaved a post '".$post_number."'.";
                actionLog('save_post',$save_post_id,'Unsave Post',$log_msg,'User',$user_id);
                $this->display_output('1','Post deleted successfully.');
            }else{
                $this->display_output('0','Something went wrong. Please try again.');
            }
        }else{
            $this->display_output('0','Post isn\'t available.');
        }
        
    }
    public function hide_post(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $post_id  = $this->clear_input($this->input->post('post_id'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($post_id)) {
            $this->display_output('0','Please enter post id.');
        }
        $where_post="id='".$post_id."' and (is_deleted=0 and status ='Active')";
        if(!$post_owner=$this->Common_model->getRecords('post','user_id,status,post_number',$where_post,'',true)){
             $this->display_output('0','Post isn\'t available now.');
        }
        if($post_owner['user_id']==$user_id){
            $this->display_output('0','You cannot hide your own post.');
        }
        if($this->Common_model->getRecords('hide_post','id',array('post_id'=>$post_id,'user_id'=>$user_id),'',true)){
             $this->display_output('0','You already hide this post.');
        }

        $insert_data=array(
            'user_id'=>$user_id,
            'post_id'=>$post_id,
            'created'=>date('Y-m-d H:i:s'),
        );
        if($last_id=$this->Common_model->addEditRecords('hide_post',$insert_data)){
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has hided a post '".$post_owner['post_number']."'.";
            actionLog('post',$post_id,'Hide Post',$log_msg,'User',$user_id);
            $this->display_output('1','Post hide successfully.');
        }else{
            $this->display_output('0','Something went wrong. Please try again.');
        }
    }
    public function report_post(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $post_id  = $this->clear_input($this->input->post('post_id'));
        $comment  = $this->clear_input($this->input->post('comment'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($post_id)) {
            $this->display_output('0','Please enter post id.');
        }
        /*$where_post="id='".$post_id."' and (is_deleted=1 OR status !='Active')";
        if($post_owner=$this->Common_model->getRecords('post','user_id,status',$where_post,'',true)){
             $this->display_output('0','Post isn\'t available now.');
        }*/
        $where_post="id='".$post_id."' and is_deleted=0 and status ='Active'";
        if(!$post_owner=$this->Common_model->getRecords('post','user_id,status,post_number',$where_post,'',true)){
             $this->display_output('0','Post isn\'t available now.');
        }
        if($post_owner['user_id']==$user_id){
            $this->display_output('0','You cannot report of your own post.');
        }
        if($this->Common_model->getRecords('post_report','id',array('post_id'=>$post_id,'user_id'=>$user_id),'',true)){
             $this->display_output('0','You already report for this post.');
        }

        $insert_data=array(
            'user_id'=>$user_id,
            'post_id'=>$post_id,
            'comment'=>$comment,
            'created'=>date('Y-m-d H:i:s'),
        );
        if($last_id=$this->Common_model->addEditRecords('post_report',$insert_data)){
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has reported a post '".$post_owner['post_number']."'.";
            actionLog('post',$post_id,'Report Post',$log_msg,'User',$user_id);
            $this->display_output('1','Post is mark as reported.');
        }else{
            $this->display_output('0','Something went wrong. Please try again.');
        }
    }
    public function be_my_date_list(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $page  = $this->clear_input($this->input->post('page'));
        $limit  = $this->clear_input($this->input->post('limit'));
        $name  = $this->clear_input($this->input->post('name'));
        $age  = $this->clear_input($this->input->post('age'));
        $address  = $this->clear_input($this->input->post('address'));
        $gender  = $this->clear_input($this->input->post('gender'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $user_data=$this->App_model->be_my_date_list($user_id,$name,$age,$address,$gender);
        //echo $this->db->last_query();exit;
        if(!empty($user_data)){
            $this->display_output('1','Be my date list', array('details'=>$user_data));
        }else{
            $this->display_output('0','No result found.');
        }

    }
    public function be_my_date_hide(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $hide_to  = $this->clear_input($this->input->post('hide_to'));
        $status  = $this->clear_input($this->input->post('status'));
        $hide_to_staging_id=getUserInfo($hide_to,'users','user_id','staging_id');
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($hide_to)) {
            $this->display_output('0','Please enter hide to.');
        }
        if(empty($status)){
             $this->display_output('0','Please enter status.');
        }
        $where="user_id='".$hide_to."' and (is_deleted=0 and status ='Active')";
        if(!$hide_user=$this->Common_model->getRecords('users','user_id',$where,'',true)){
             $this->display_output('0','User isn\'t available now.');
        }
        if($status=='Hide'){
            if($this->Common_model->getRecords('be_my_date_hide','id',array('hide_by'=>$user_id,'hide_to'=>$hide_to,'status'=>'Hide'),'',true)){
                 $this->display_output('0','You already hide this user.');
            }
            $insert_data=array(
                'hide_by'=>$user_id,
                'hide_to'=>$hide_to,
                'status'=>$status,
                'created'=>date('Y-m-d H:i:s'),
            );
            if($last_id=$this->Common_model->addEditRecords('be_my_date_hide',$insert_data)){
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has hided '".$hide_to_staging_id."' from be my date.";
                actionLog('be_my_date_hide',$last_id,'Add',$log_msg,'User',$user_id);
                $this->display_output('1','User hided successfully.');
            }else{
                $this->display_output('0','Something went wrong. Please try again.');
            }
        }else{
            if($this->Common_model->getRecords('be_my_date_hide','id',array('hide_by'=>$user_id,'hide_to'=>$hide_to,'status'=>'Report'),'',true)){
                 $this->display_output('0','You already report this user.');
            }
            $insert_data=array(
                'hide_by'=>$user_id,
                'hide_to'=>$hide_to,
                'status'=>$status,
                'created'=>date('Y-m-d H:i:s'),
            );
            if($last_id=$this->Common_model->addEditRecords('be_my_date_hide',$insert_data)){
                $this->display_output('1','User mark as reported successfully.');
            }else{
                $this->display_output('0','Something went wrong. Please try again.');
            }
        }
        
    }
   /* public function blocke_user(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $blocked_to  = $this->clear_input($this->input->post('blocked_to'));
       
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($blocked_to)) {
            $this->display_output('0','Please enter blocked user id.');
        }
        if(!$blocked_user_detail=$this->Common_model->getRecords('users','id',array('status'=>'Active','user_id'=>$blocked_to,'is_deleted'=>0),'',true)){
            $this->display_output('0','User isn\'t available.');
        }

        if($blocked_record=$this->Common_model->getRecords('blocked_user','id',array('blocked_by'=>$user_id,'blocked_to'=>$blocked_to,'is_deleted'=>0),'',true)){
            $this->display_output('0','You already blocked this user.');
        }
        
        $insert_data=array(
            'blocked_by'=>$user_id,
            'blocked_to'=>$post_id,
            'created'=>date('Y-m-d H:i:s'),
        );
        if($last_id=$this->Common_model->addEditRecords('post_report',$insert_data)){
            $this->display_output('1','Post is mark as reported.');
        }else{
            $this->display_output('0','Something went wrong. Please try again.');
        }
    }*/
    public function post_comment() {
        $this->App_model->check_user_status();
        $user_id = $this->clear_input($this->input->post('user_id'));
        $post_id      = $this->clear_input($this->input->post('post_id'));
        $comment = $this->clear_input($this->input->post('comment'));
        $comment_id = $this->clear_input($this->input->post('comment_id'));      
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($post_id)) {
            $this->display_output('0','Please enter post id.');
        }
        if(empty($comment)) {
            $this->display_output('0','Please enter comment.');
        }
        if(!$post_owner=$this->Common_model->getRecords('post','user_id,post_number',array('id'=>$post_id,'is_deleted'=>0,'status'=>'Active'),'',true)){
             $this->display_output('0','Post isn\'t available now.');
        }
     
        $insert_data = array(
            'user_id' => $user_id,
            'post_id' => $post_id,
            'comment' => $comment,
            'created' => date("Y-m-d H:i:s")
        );
        if(isset($comment_id) && !empty($comment_id)){
            if(!$comment_owner=$this->Common_model->getRecords('comments','user_id',array('id'=>$comment_id,'is_deleted'=>0),'',true)){
                 $this->display_output('0','Parent comment isn\'t available now.');
            }
            $insert_data['parent_id']=$comment_id;
        }
        if($last_id=$this->Common_model->addEditRecords('comments',$insert_data)) {
            $action_by_fullname=getUserInfo($user_id,'users','user_id','fullname');
            if($post_owner['user_id']!=$user_id){
                $title=ucwords($action_by_fullname).' commented on your post.';
                $content=ucwords($action_by_fullname).' commented on your post.';
                $notdata['type']='post_comment';
                $this->Common_model->push_notification_send($post_owner['user_id'],$notdata,$post_id,$title,$content,'',$user_id);
            }
            if(isset($comment_id) && !empty($comment_id)){
                if($comment_owner['user_id']!=$user_id){
                    $title=ucwords($action_by_fullname).' commented on your comment.';
                    $content=ucwords($action_by_fullname).' commented on your comment.';
                    $notdata['type']='post_comment';
                    $this->Common_model->push_notification_send($comment_owner['user_id'],$notdata,$post_id,$title,$content,'',$user_id);
                }
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has added a comment on comment of post '".$post_owner['post_number']."'.";
                actionLog('comments',$last_id,'Add',$log_msg,'User',$user_id); 
            }else{
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has added a comment on post '".$post_owner['post_number']."'.";
                actionLog('comments',$last_id,'Add',$log_msg,'User',$user_id);   

            }    
            
           // $total_comments = $this->Common_model->getNumRecords('comments', '', array('post_id'=>$post_id,'is_deleted'=>0));
            $where="post_id='".$post_id."'";
            $comment=$this->App_model->get_count('comments',$where);
            $this->display_output('1','Comment posted successfully.',array('comment_total'=>shortNumber($comment['count'])));
        } 
        else { 
            $this->display_output('0','Something went wrong. Please try again.');
        }
    }
    public function comment_list() {
        $this->App_model->check_user_status();
        $user_id = $this->clear_input($this->input->post('user_id'));
        $post_id      = $this->clear_input($this->input->post('post_id'));
        $array=array();

        $comments = $this->n_level_comment($post_id,0,$array);
        $count=count($comments);
        if(!empty($comments)){

            $this->display_output('1','Comment list.',array('count'=>$count,'details'=>$comments));
        }else{
            $this->display_output('0','Comment list not found.');
        }
    }
    function n_level_comment($post_id,$parent_id=0,&$array){
        $record = $this->App_model->getpostcomment($post_id,$parent_id);
        // echo $this->db->last_query();die;
        if(!empty($record)){ 
            foreach ($record as $key => $list) {  
                $array[] = $list;
                
                 $this->n_level_comment($post_id,$list['id'],$array); 

            }
        } 
        return $array;
    }

    public function comment_like_dislike(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $comment_id  = $this->clear_input($this->input->post('comment_id'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($comment_id)) {
            $this->display_output('0','Please enter comment id.');
        }
        if(!$comment_owner=$this->Common_model->getRecords('comments','user_id,post_id',array('id'=>$comment_id,'is_deleted'=>0),'',true)){
             $this->display_output('0','Comment isn\'t available now.');
        }
        $post_number=$this->Common_model->getFieldValue('post','post_number',array('id'=>$comment_owner['post_id']),'',true);
        $where=array('comment_id'=>$comment_id,'user_id'=>$user_id);
        if($this->Common_model->getRecords('comment_like','*',$where,'',true)){
            if($this->Common_model->deleteRecords('comment_like',$where)){
                removeNotification($comment_owner['user_id'],'Comment Like',$comment_owner['post_id']); 
            }
            $total=$this->Common_model->getRecords('comment_like','count(id) as likes',array('comment_id'=>$comment_id),'',true);
            //$this->display_output('1','Comment unlike successfully.',array('like_count'=>$total['likes']));
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has unlike comment of post '".$post_number."'.";
            actionLog('comment_like',$comment_id,'Unlike Comment',$log_msg,'User',$user_id);
            $this->display_output('1','Comment unlike successfully.');
        }else{
                $insert_data=array(
                'user_id'=>$user_id,
                'comment_id'=>$comment_id,
                'created'=>date('Y-m-d H:i:s'),
                );
            if($last_id=$this->Common_model->addEditRecords('comment_like',$insert_data)){
                if($comment_owner['user_id']!=$user_id){
                    $liked_by_fullname=getUserInfo($user_id,'users','user_id','fullname');
                    $title=ucwords($liked_by_fullname).' likes your comment.';
                    $content=ucwords($liked_by_fullname).' likes your comment.';
                    $notdata['type']='comment_like';
                    $this->Common_model->push_notification_send($comment_owner['user_id'],$notdata,$comment_owner['post_id'],$title,$content,'',$user_id);
                }
                $total=$this->Common_model->getRecords('comment_like','count(id) as likes',array('comment_id'=>$comment_id),'',true);
                //$this->display_output('1','Comment like successfully.',array('like_count'=>$total['likes']));
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has like comment of post '".$post_number."'.";
                actionLog('comment_like',$last_id,'Like Comment',$log_msg,'User',$user_id);
                $this->display_output('1','Comment like successfully.');
            }
        }
        
    }
    public function delete_comment(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $comment_id  = $this->clear_input($this->input->post('comment_id'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($comment_id)) {
            $this->display_output('0','Please enter comment id.');
        }
        if(!$comment_owner=$this->Common_model->getRecords('comments','user_id,post_id',array('id'=>$comment_id,'is_deleted'=>0),'',true)){
             $this->display_output('0','Comment isn\'t available now.');
        }
        if($comment_owner['user_id']!=$user_id){
            $this->display_output('0','You are not authorized to delete this comment.');
        }
        //$post_owner_id=$this->Common_model->getFieldValue('post','user_id',array('id'=>$comment_owner['post_id']),'',true);
        $post_owner_detail=$this->Common_model->getRecords('post','user_id,post_number',array('id'=>$comment_owner['post_id']),'',true);
        $post_owner_id=$post_owner_detail['user_id'];
        $post_number=$post_owner_detail['post_number'];
        $records=$this->Common_model->getRecords('comments','*',array('id'=>$comment_id),'',true);
        //print_r($records);exit;
        if($records['parent_id']>0){
            if($this->Common_model->addEditRecords('comments',array('is_deleted'=>1,'modified'=>date('Y-m-d H:i:s')),array('id'=>$comment_id))){
                removeNotification($post_owner_id,'Post Comment',$comment_owner['post_id'],1);
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has deleted comment of comment on post '".$post_number."'.";
                actionLog('comments',$comment_id,'Delete',$log_msg,'User',$user_id);
                $this->display_output('1','Comment deleted successfully.'); 
            }else{
                $this->display_output('0','Something went wrong. Please try again.');
            }
        }else{ 
            if($multi_comment=$this->Common_model->getRecords('comments','id,user_id,post_id',array('parent_id'=>$comment_id),'',false))
            {
                $this->db->trans_begin();
                foreach($multi_comment as $row){
                    $this->Common_model->addEditRecords('comments',array('is_deleted'=>1,'modified'=>date('Y-m-d H:i:s')),array('id'=>$row['id']));
                    removeNotification($row['user_id'],'post_comment',$row['post_id'],1);
                }
                $this->Common_model->addEditRecords('comments',array('is_deleted'=>1,'modified'=>date('Y-m-d H:i:s')),array('id'=>$comment_id));
                removeNotification($post_owner_id,'post_comment',$comment_owner['post_id']);
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    $this->display_output('0','Something went wrong. Please try again.'); 
                } else {
                    $frontend_username = getFrontUsername($user_id);
                    $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has deleted comment of post '".$post_number."'.";
                    actionLog('comments',$comment_id,'Delete',$log_msg,'User',$user_id);
                    $this->db->trans_commit();
                    $this->display_output('1','Comment deleted successfully.');
                }
                $this->display_output('1','Comment deleted successfully.'); 
            }
            else{
                if($this->Common_model->addEditRecords('comments',array('is_deleted'=>1,'modified'=>date('Y-m-d H:i:s')),array('id'=>$comment_id))){
                    removeNotification($post_owner_id,'post_comment',$comment_owner['post_id']);
                    $frontend_username = getFrontUsername($user_id);
                    $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has deleted comment of post '".$post_number."'.";
                    actionLog('comments',$comment_id,'Delete',$log_msg,'User',$user_id);
                    $this->display_output('1','Comment deleted successfully.');  
                }else{
                    $this->display_output('0','Something went wrong. Please try again.');
                }
            }
        }        
    }
    ///////////////////////////////friends section//////////////////////
    public function sent_friend_request(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $receiver_id  = $this->clear_input($this->input->post('receiver_id'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }if(empty($receiver_id)) {
            $this->display_output('0','Please enter receiver id.');
        }
        $user_details=getNameEmailAddress($receiver_id);
        if($user_details['status']!='Active' || $user_details['is_deleted']==1){
            $this->display_output('0','Receiver\'s account has been deactivated.');
        }
        $where = "(sender_id = '".$user_id."' and receiver_id = '".$receiver_id."' OR sender_id = '".$receiver_id."' and receiver_id = '".$user_id."')";
        if($already_requested= $this->Common_model->getRecords('friends','id',$where,'',true)) { 
            $this->display_output('0','Already friend request is sent.');
        }
        $insert_data=array('sender_id'=>$user_id,
            'receiver_id'=>$receiver_id,
            'status'=>'Pending',
            'created'=>date('Y-m-d H:i:s'),
            );
        if($last_id = $this->Common_model->addEditRecords('friends',$insert_data)){
            $sender_staging_id=getUserInfo($user_id,'users','user_id','fullname');
            $receive_staging_id=getUserInfo($receiver_id,'users','user_id','staging_id');
           /* $from_email =getNotificationEmail() ;
            $to_email =$user_details['email'];
            $subject = WEBSITE_NAME.' : Friend Request';
            $data['name']= ucwords($user_details['fullname']);
            $data['message']= ucwords($sender_staging_id).' sent you friend request.';
            $body = $this->load->view('template/common', $data,TRUE);
            $this->Common_model->sendEmail($to_email,$subject,$body,$from_email)*/;

            $title=ucwords($sender_staging_id).' sent you friend request.';
            $content=ucwords($sender_staging_id).' sent you friend request.';
            $notdata['type']='friend_request';
            $this->Common_model->push_notification_send($receiver_id,$notdata,$last_id,$title,$content,'',$user_id);
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has sent friend request to '".$receive_staging_id."'.";
            actionLog('friends',$last_id,'Add',$log_msg,'User',$user_id);
            $this->display_output('1','Request sent successfully.');
        }else{
            $this->display_output('0','Something went wrong. Please try again.');
        }
    }
    public function friend_request_list(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $keyword  = $this->clear_input($this->input->post('keyword'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $list=$this->App_model->get_friend_request_list($user_id,$keyword='');
        if(!empty($list)) {
            $this->display_output('1','Friend list', array('details'=>$list));
        } else {
            $this->display_output('0','Friend request not found.');
        }
    }
    public function accept_reject_request(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $sent_by  = $this->clear_input($this->input->post('sent_by'));
        $request_id  = $this->clear_input($this->input->post('request_id'));
        $status  = $this->clear_input($this->input->post('status'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($request_id)) {
            $this->display_output('0','Please enter request id.');
        }if(empty($status)) {
            $this->display_output('0','Please enter status.');
        }
        if(empty($sent_by)) {
            $this->display_output('0','Please enter sent by.');
        }
        if($status!='Accepted' &&  $status!='Rejected'){
            $this->display_output('0','Status either Accepted or Rejected.');
        }
        if(!$record=$this->Common_model->getRecords('friends', 'id,status', array('id'=>$request_id), '', true)){
            $this->display_output('0','Friend request not available.');
        }
        if($record['status']=='Accepted' && $status=='Accepted'){
             $this->display_output('0','Friend request already accepted.');
        }
        if($user_id==$sent_by) {
            $this->display_output('0','You cannot perform this action on yourself.');
        }
        $sender_staging_id=getUserInfo($sent_by,'users','user_id','staging_id');
        if($status=='Accepted'){
            $update_data=array('status'=>'Accepted','modified'=>date('Y-m-d H:i:s'));
            if($this->Common_model->addEditRecords('friends',$update_data,array('id'=>$request_id))){
                $user_fullname=getUserInfo($user_id,'users','user_id','fullname');
                /*$user_details=getNameEmailAddress($sent_by);
                $from_email =getNotificationEmail() ;
                $to_email =$user_details['email'];
                $subject = WEBSITE_NAME.' : Friend request accepted';
                $data['name']= ucwords($user_details['fullname']);
                $data['message']= ucwords($user_fullname).' accepted your friend request.';
                $body = $this->load->view('template/common', $data,TRUE);
                $this->Common_model->sendEmail($to_email,$subject,$body,$from_email);*/

                $title=ucwords($user_fullname).' accepted your friend request.';
                $content=ucwords($user_fullname).' accepted your friend request.';
                $notdata['type']='friend_request_accepted';
                $this->Common_model->push_notification_send($sent_by,$notdata,$request_id,$title,$content,'',$user_id);
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has accepted friend request of '".$sender_staging_id."'.";
                actionLog('friends',$request_id,'Accept Friend Request',$log_msg,'User',$user_id);
                $this->display_output('1','Friend request accepted.');
            }else{
                $this->display_output('0','Something went wrong. Please try again.');
            }
        }else{
            if($this->Common_model->deleteRecords('friends',array('id' =>$request_id))){
                if($record['status']=='Accepted' && $status=='Rejected'){
                    $frontend_username = getFrontUsername($user_id);
                    $log_msg = getFrontUsertype($user_id)." ".$frontend_username." unfriend to '".$sender_staging_id."'.";
                    actionLog('friends',$request_id,'Unfriend',$log_msg,'User',$user_id);
                    $this->display_output('1','Connection remove successfully.');
                }else{
                    $frontend_username = getFrontUsername($user_id);
                    $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has rejected friend request of '".$sender_staging_id."'.";
                    actionLog('friends',$request_id,'Reject Friend Request',$log_msg,'User',$user_id);
                    $this->display_output('1','Friend request rejected.');
                }
            }else{
                $this->display_output('0','Something went wrong. Please try again.');
            }
        }
    }
    public function friend_list(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $keyword  = $this->clear_input($this->input->post('keyword'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        $list=$this->App_model->get_friend_list($user_id,$keyword='');
        if(!empty($list)) {
            $this->display_output('1','Friend list', array('details'=>$list));
        } else {
            $this->display_output('0','Friends not found.');
        }
    }
    ///////////////////////////////////////////////////////////////////////

    ///////////////////////////////////Add rating review///////////////////////////////////
    public function add_rating_review(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $event_id  = $this->clear_input($this->input->post('event_id'));
        $rating  = $this->clear_input($this->input->post('rating'));
        //$review  = $this->clear_input($this->input->post('review'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($event_id)) {
            $this->display_output('0','Please enter event id.');
        }
        if(empty($rating)) {
            $this->display_output('0','Please enter rating.');
        }
        /*if(empty($review)) {
            $this->display_output('0','Please enter review.');
        }*/
        if(!$event_details=$this->Common_model->getRecords('events','id,sp_id,is_completed,event_title',array('id'=>$event_id,'is_deleted'=>0,'status'=>'Active','user_id'=>$user_id),'',true)){
            $this->display_output('0','Event isn\'t available.');
        }
        if($event_details['is_completed']==0){
            $this->display_output('0','Event is not completed yet.');
        }
        if($rating_record=$this->Common_model->getRecords('reviews','id',array('event_id'=>$event_id,'is_deleted'=>0,'status'=>'Active','sender_id'=>$user_id,'receiver_id'=>$event_details['sp_id']),'',true)){
            $this->display_output('0','You have already submitted your ratings to this event.');
        }
        $add=array(
            'sender_id'=>$user_id,
            'receiver_id'=>$event_details['sp_id'],
            'event_id'=>$event_id,
            'rating'=>$rating,
            //'review'=>$review,
            'created'=> date("Y-m-d H:i:s")
        );

        if($last_id=$this->Common_model->addEditRecords('reviews',$add))
        {   
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getFrontUsertype($user_id)." ".$frontend_username." added ".$rating." rating for event '".$event_details['event_title']."'.";
            actionLog('reviews',$last_id,'Add',$log_msg,'User',$user_id);
            $this->display_output('1','Rating submitted successfully.');
        }else{
            $this->display_output('0','Something went wrong. Please try again.');
        }
        
    }
    //////////////////////////////////////////////////////////////////////////////////////////
    public function notification_setting(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $action  = $this->clear_input($this->input->post('action'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if($action=="") {
            $this->display_output('0','Please enter action.');
        }
        if($this->Common_model->addEditRecords('users',array('send_notification'=>$action,'modified'=>date('Y-m-d H:i:s')),array('user_id'=>$user_id))) {
            //$this->Common_model->addEditRecords('users',$update,array('user_id' => $user_id));

            //echo $this->db->last_query();exit;
            $this->display_output('1','Notification setting updated.');
        } else {
            $this->display_output('0','Something went wrong. Please try again.');
        }
    }
    public function notification_list(){

        $user_id  =  $this->clear_input($this->input->post('user_id'));
        $page =  $this->clear_input($this->input->post('page')); 
        $limit  =   $this->clear_input($this->input->post('limit')); 
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }  
        if(!$page){
            $page=0;
        }
        if(!$limit){
            $limit=10;
        }
        $start = $limit*$page; 
        $this->App_model->check_user_status();
       // $this->Common_model->addEditRecords('users',array('badge_count'=>'0'),array('user_id' => $user_id));
        $notifications= $this->Common_model->getRecords('notifications','notification_id,user_id,notification_title,notification_description,creation_datetime,notification_type,record_id',array('user_id'=>$user_id),'notification_id DESC',false,'',$start,$limit); 
        if(!empty($notifications)) {
            $this->display_output('1','Notification list.',array('notification_list'=>$notifications));
        } else {
            $this->display_output('0','Notification list not found..');
        }
    }
    /************************************Follow User ***********************************/
    public function follow_user(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $follow_to  =   $this->clear_input($this->input->post('follow_to'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        } 
        if(empty($follow_to)){
            $this->display_output('0','Please enter follow to.');
        }
        if($user_id==$follow_to){
            $this->display_output('0','you can\'t follow yourself.');
        }
        $insert_data = array(
            'follow_to'=>$follow_to,
            'followed_by'=>$user_id,
            'created'=> date("Y-m-d H:i:s"),
        );

        $where=array('follow_to'=>$follow_to,'followed_by'=>$user_id);
        if($check_status= $this->Common_model->getRecords('follow_users','id',$where,'',true)){
            $insert_data['status']=2;
            $this->db->trans_begin();
                $last_id=$this->Common_model->addEditRecords('follow_user_log', $insert_data);
                $this->Common_model->deleteRecords('follow_users',$where);
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getFrontUsertype($user_id)." ".$frontend_username." unfollow ".getFrontUsername($follow_to).".";
                actionLog('follow_user_log',$last_id,'Unfollow',$log_msg,'User',$user_id);
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                display_output('0','Something went wrong. Please try again.'); 
            } else {
                $this->db->trans_commit();
                removeNotification($follow_to,'follow_user',$check_status['id']); 
                display_output('1','Un-join user successfully.',array('text'=>'Join'));
            } 
        }else{
            $this->db->trans_begin();
                $f_last_id=$this->Common_model->addEditRecords('follow_users', $insert_data);
                $insert_data['status']=1;
                $last_id=$this->Common_model->addEditRecords('follow_user_log', $insert_data);
                $frontend_username = getFrontUsername($user_id);
                $log_msg = getFrontUsertype($user_id)." ".$frontend_username." start follow ".getFrontUsername($follow_to).".";
                actionLog('follow_user_log',$last_id,'follow',$log_msg,'User',$user_id); 
            if($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                display_output('0','Something went wrong. Please try again.'); 
            } else {
                $this->db->trans_commit();
                $title=ucfirst($frontend_username).' starting following you';
                $content=ucfirst($frontend_username).' starting following you';
                $notdata['type']='follow_user';
                $this->Common_model->push_notification_send($follow_to,$notdata,$f_last_id,$title,$content,'',$user_id);
                display_output('1','Join user successfully.',array('text'=>'Joined'));
            } 
        }
    }
    /*************************************Deal *****************************************/
    public function my_deal_list(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $keyword  = $this->clear_input($this->input->post('keyword'));
        $user_type  = $this->clear_input($this->input->post('user_type'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($user_type)) {
            $this->display_output('0','Please enter user type.');
        }
        $list=$this->App_model->get_deals($user_id,$user_type,$keyword);
        // echo $this->db->last_query();exit;
        if(!empty($list)) {
            $this->display_output('1','Deal list', array('details'=>$list));
        } else {
            $this->display_output('0','Deal not found');
        }
    }

    public function deals_details(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $event_id  = $this->clear_input($this->input->post('event_id'));
        $user_type  = $this->clear_input($this->input->post('user_type'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($event_id)) {
            $this->display_output('0','Please enter event id.');
        }
        if(empty($user_type)) {
            $this->display_output('0','Please enter user type.');
        }
        
        $detail=$this->App_model->deals_details($event_id,$user_id,'booking');
       
        $detail['pending']="0";
        $detail['accepted']="0";
        $detail['rejected']="0";
        if($request_status = $this->Common_model->getRecords('event_attendee','count("status") as count,status',array('event_id'=>$event_id),'',false,'status')){
           foreach($request_status as $row){
                if($row['status']=='Accepted'){
                    $detail['accepted']=$row['count'];
                }if($row['status']=='Rejected'){
                    $detail['rejected']=$row['count'];
                }
                if($row['status']=='Pending'){
                    $detail['pending']=$row['count'];
                }
           }
        }
        $deal_id = $this->Common_model->getFieldValue('events','deal_id',array('id'=>$event_id,'is_deleted'=>0),'',false);
        $event_id=$deal_id;
        $order_id = $this->Common_model->getFieldValue('orders','id',array('event_id'=>$event_id),'',true);
        $order_record=$this->App_model->booking_detail_order($order_id);
        //echo $this->db->last_query();exit;
        $detail['order_status']=$order_record['status'];
        $detail['order_id']=$order_record['order_id'];
        $detail['order_number']=$order_record['order_number'];
        $detail['order_status_id']=$order_record['order_status_id'];
        $items = $this->Common_model->getRecords('order_details','item_name,price,qty,unit,customer_qty,total_price,sell_price,tax,discount',array('event_id'=>$event_id,'is_deleted'=>0),'',false);
        $sell_total=0;
        if(!empty($items)){
            $sell_total=0;
            foreach($items as $data){
                $sell_total=$data['sell_price']+$sell_total;
            }
        }
        if(!empty($detail)) {
            $response=array('event_details'=>$detail,'items'=>$items,'selling_total'=>$sell_total);
            $this->display_output('1','Deal details', array('details'=>$response));
        } else {
            $this->display_output('0','Deal details not found');
        }
    }

    public function event_add_to_deal(){
       
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $event_id  = $this->clear_input($this->input->post('event_id'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($event_id)) {
            $this->display_output('0','Please enter event id.');
        }
        $event_details=$this->Common_model->getRecords('events','id,service_id,sp_id,event_title,event_address,members,country_id,state_id,city_id,latitude,longitude,event_date,event_type,note,is_canceled',array('id'=>$event_id),'',true);
        if($user_id!=$event_details['sp_id']){
            $this->display_output('0','You are not authorize to convert this event in to deal.');
        }
        if($check_deal=$this->Common_model->getRecords('events','deal_id',array('deal_id'=>$event_id,'is_canceled'=>0),'',true)){
            $this->display_output('0','This event already converted in deal.');
        }
        if($event_details['is_canceled']==0){
            $this->display_output('0','You can\'t add this event as deal. It is not canceled yet.');
        }
        $current_date=convertGMTToLocalTimezone(date('Y-m-d H:i:s'));
        $current_time= convertGMTToLocalTimezone(date('Y-m-d H:i:s'),'',true);
        $curr_d=strtotime($current_date.' '.$current_time);
        $ev_date=convertGMTToLocalTimezone($event_details['event_date']);
        $ev_time= convertGMTToLocalTimezone($event_details['event_date'],'',true);
        $ev=strtotime($ev_date.' '.$ev_time);
        if($curr_d>$ev){ 
            $this->display_output('0','Event date is passed away.');
        }
        if(!empty($event_details)){
            $event_data=array(
                'deal_id'=>$event_details['id'],
                'service_id'=>$event_details['service_id'],
                'sp_id'=>$user_id,
                'user_id'=>$user_id,
                'event_title'=>$event_details['event_title'],
                'event_address'=>$event_details['event_address'],
                'country_id'=>$event_details['country_id'],
                'state_id'=>$event_details['state_id'],
                'city_id'=>$event_details['city_id'],
                'latitude'=>$event_details['latitude'],
                'longitude'=>$event_details['longitude'],
                'event_date'=>$event_details['event_date'],
                'event_type'=>$event_details['event_type'],
                'note'=>$event_details['note'],
                'created' => date('Y-m-d H:i:s'),
            );
            $this->db->trans_begin();
                $event_id = $this->Common_model->addEditRecords('events',$event_data);
                $event_number='E'.str_pad($event_id,8,0,STR_PAD_LEFT);
                $this->Common_model->addEditRecords('events',array('event_number'=>$event_number),array('id'=>$event_id));

                $frontend_username = getFrontUsername($user_id);
                $log_msg = getFrontUsertype($user_id)." ".$frontend_username." convert ".$event_details['event_title']." in to deal.";
                actionLog('events',$event_id,'add_deal',$log_msg,'User',$user_id); 
            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $this->display_output('0','Something went wrong. Please try again.'); 
            } else {
                $this->db->trans_commit();
                $this->display_output('1','Deals added successfully.');
            }
            
        }else{
            $this->display_output('0','Event not found.');
        }
        
    } 
    public function deal_checkout(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $event_id  = $this->clear_input($this->input->post('event_id'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->$this->display_output('0','Please enter user id.');
        }
        if(empty($event_id)) {
            $this->$this->display_output('0','Please enter event id.');
        }
        
        if(!$event_details=$this->Common_model->getRecords('events','id,sp_id,is_deal_done,event_date',array('id'=>$event_id),'',true)){
            $this->display_output('0','Event not available.');                                                                                                        
        }
        if($user_id==$event_details['sp_id']){
            $this->display_output('0','You are not authorize to grab your own deal.');
        }
        if($event_details['is_deal_done']==1){
             $this->display_output('0','This deal already sold out.');
        }
        $current_date=convertGMTToLocalTimezone(date('Y-m-d H:i:s'));
        $current_time= convertGMTToLocalTimezone(date('Y-m-d H:i:s'),'',true);
        $curr_d=strtotime($current_date.' '.$current_time);
        $ev_date=convertGMTToLocalTimezone($event_details['event_date']);
        $ev_time= convertGMTToLocalTimezone($event_details['event_date'],'',true);
        $ev=strtotime($ev_date.' '.$ev_time);
        if($curr_d>$ev){ 
            $this->display_output('0','Deal date is passed away.');
        }
        $update_data=array(
            'is_deal_done'=>1,
            'modified'=>date('Y-m-d H:i:s'),
            );
        $this->db->trans_begin();
        $this->Common_model->addEditRecords('events',$update_data,array('id'=>$event_id));
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $this->display_output('0','Something went wrong. Please try again.'); 
        } else {
           
           $this->display_output('1','You can move forward to payment.',array('event_id'=>$event_id));
        }
    }                                                                                                               

    public function deal_notify_payment(){
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $event_id  = $this->clear_input($this->input->post('event_id'));
        $transaction_id  = $this->clear_input($this->input->post('transaction_id'));
        $timezone  = $this->clear_input($this->input->post('timezone'));
        $payment_status  = $this->clear_input($this->input->post('payment_status'));
        $this->App_model->check_user_status();
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($event_id)) {
            $this->display_output('0','Please enter event id.');
        }if(empty($transaction_id)) {
            $this->display_output('0','Please enter transaction id.');
        }if(empty($timezone)) {
            $this->display_output('0','Please enter timezone.');
        }
        if(empty($payment_status)) {
           $payment_status='Completed';
        }

        $update_event_info=array(
            'is_deal_done'=>0,
            'modified'=>date('Y-m-d H:i:s'),
        );
        $event_info=$this->Common_model->getRecords('events','user_id,sp_id,deal_id,event_title,event_type,service_id,event_date,event_date',array('id'=>$event_id,'status'=>'Active','type'=>'Draft'),'',true);
        if(empty($event_info)){
            $this->Common_model->addEditRecords('events',$update_event_info,array('id'=>$event_id));
            $this->display_output('0','Payment failed due to some malicious activity.');
        }
        $user_data = $this->Common_model->getRecords('users','user_id,fullname,staging_id,mobile,user_type,email,status',array('user_id'=>$user_id),'user_id DESC',true);
        $update_event=array('user_id'=>$user_id,'type'=>'Actual','modified'=>date('Y-m-d H:i:s'));
        $this->db->trans_begin();
        $this->Common_model->addEditRecords('events',$update_event,array('id'=>$event_id));
        $order_details=$this->Common_model->getRecords('orders','id,total_amount,redeem_payment,admin_commission_amount',array('event_id'=>$event_info['deal_id']),'',true);
        $order_data=array('user_id'=>$user_id,
                    'fullname'=>$user_data['fullname'],
                    'email'=>$user_data['email'],
                    'mobile'=>$user_data['mobile'],
                    'event_id'=>$event_id,
                    'event_date_time'=>$event_info['event_date'],
                    'service_id'=>$event_info['service_id'],
                    'type'=>'Actual',
                    'ip_address'=>$this->input->ip_address(),
                    'payment_status'=>'Paid',
                    'payment_date'=>date('Y-m-d H:i:s'),
                    'order_status'=>$payment_status,
                    'total_amount'=>$order_details['total_amount'],
                    'redeem_payment'=>$order_details['redeem_payment'],
                    'admin_commission_amount'=>$order_details['admin_commission_amount'],
                    'created'=>date('Y-m-d H:i:s')
        );
        if($order_id = $this->Common_model->addEditRecords('orders',$order_data)){
            $order_number=str_pad($order_id,8,0,STR_PAD_LEFT);
            $this->Common_model->addEditRecords('orders',array('order_number'=>$order_number),array('id'=>$order_id));
            if($order_items = $this->Common_model->getRecords('order_details','item_name,price,qty,total_qty,unit,customer_qty,total_price,sell_price,tax,discount,offer_name',array('order_id'=>$order_details['id']),'',false)){
                $max_group_id=$this->Common_model->getRecords('order_status_history','max(`order_batch_id`) as order_group_id','','',true);
                $order_history_details_max_id=$max_group_id['order_group_id']+1;
                foreach ($order_items as $key => $item) {
                    $item_arr[]=array(
                        'order_group_id'=>$order_history_details_max_id,
                        'service_id'=>$event_info['service_id'],
                        'order_id'=>$order_id,
                        'event_id'=>$event_id,
                        'event_date_time'=>$event_info['event_date'],
                        'item_name'=>$item['item_name'],
                        'price'=>$item['price'],
                        'qty'=>$item['qty'],
                        'total_qty'=>$item['total_qty'],
                        'unit'=>$item['unit'],
                        'customer_qty'=>$item['customer_qty'],
                        'total_price'=>$item['total_price'],
                        'sell_price'=>$item['sell_price'],
                        'tax'=>$item['tax'],
                        'discount'=>$item['discount'],
                        'offer_name'=>$item['offer_name'],
                        'type'=>'Actual',
                        'created'=>date('Y-m-d H:i:s')
                        );
                }
                $this->db->insert_batch('order_details', $item_arr);
                $order_status=array(
                    'order_batch_id'=>$order_history_details_max_id,
                    'order_id'=>$order_id,
                    'status'=>'Accepted',
                    'type'=>'Actual',
                    'created'=>date('Y-m-d H:i:s'),
                    'ip_addres'=>$this->input->ip_address(),
                    );
                $this->Common_model->addEditRecords('order_status_history',$order_status);
                if($order_amt = $this->Common_model->getRecords('order_total','code,title,value,discount_value,sort_order',array('order_id'=>$order_id),'',false)){
                    foreach ($order_amt as $key => $total) {
                       $total_arr[]=array(
                        'order_batch_id'=>$order_history_details_max_id,
                        'order_id'=>$order_id,
                        'code'=>$total['code'],
                        'title'=>$total['title'],
                        'value'=>$total['value'],
                        'discount_value'=>$total['discount_value'],
                        'sort_order'=>$total['sort_order'],
                        'status'=>'Active',
                        'type'=>'Actual',
                        'created'=>date('Y-m-d H:i:s')
                        );
                    }
                    $this->db->insert_batch('order_total',$total_arr);
                }
              
                $sub_item_total_price=$order_details['total_amount'];
               
                $paypal_fees = ($sub_item_total_price*2.90/100)+0.30;  // paypal fee
                $payment_history=array(
                    'order_id'=>$order_id,
                    'transaction_id'=>$transaction_id,
                    'total_order_amount'=>$order_details['total_amount'],
                    'paid_amount'=>$order_details['total_amount'],
                    'customer_id'=>$user_id,
                    'service_provider_id'=>$event_info['sp_id'],
                    'payment_type'=>'Order',
                    'transfer_type'=>'Customer_To_Admin',
                    'paypal_fee_amount'=>$paypal_fees,
                    'user_will_get'=>($order_details['total_amount']-$paypal_fees),
                    'payment_status'=>$payment_status,
                    'created'=>date('Y-m-d H:i:s'),
                    'ip_addres'=>$this->input->ip_address(),
                    );
                $this->Common_model->addEditRecords('payment_history',$payment_history);
                spFriends($user_id,$event_info['sp_id']);
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            display_output('0','Something went wrong. Please try again.'); 
        } else {
            $this->db->trans_commit();
            $this->Front_common_model->send_order_invoice($order_id,'Customer_To_Admin',$timezone);
            $title= 'New event created.';
            $content='Thank you for your order purchase your order number is #'.$order_number.'.';
            $notdata['type']='new_event';
            $this->Common_model->push_notification_send($user_id,$notdata,$event_id,$title,$content,'',$user_id);
            $title1= 'New Order.';
            $content1='A new order is placed for you order number is #'.$order_number.'.';
            $notdata1['type']='new_order';
            $this->Common_model->push_notification_send($event_info['sp_id'],$notdata1,$event_id,$title1,$content1,'',$user_id);
            $frontend_username = getFrontUsername($user_id);
            $log_msg = getFrontUsertype($user_id)." ".$frontend_username." has created a new event '".$event_info['event_title']."'.";
            actionLog('events',$event_id,'add',$log_msg,'User',$user_id); 
            display_output('1','Deal confirmed.');
        }
      
    }

    public function system_search(){
        $this->App_model->check_user_status();
        $user_id  = $this->clear_input($this->input->post('user_id'));
        $type  = $this->clear_input($this->input->post('type'));
        $keyword  = $this->clear_input($this->input->post('keyword'));
        if(empty($user_id)) {
            $this->display_output('0','Please enter user id.');
        }
        if(empty($type)) {
            $this->display_output('0','Please enter type.');
        }

        if($type != "User" && $type != "Event" && $type != "Deal"){
           $this->display_output('0','Type must be User, Event or Deal.');
        }

        //Search User
        if($type == "User"){
            $user_list = $this->App_model->system_user_search($keyword);
            if(!empty($user_list)) {
            $this->display_output('1','User List', array('list'=>$user_list));
            } else {
                $this->display_output('0','User not found.');
            }

        //Search Deal
        }elseif($type == "Deal"){
            $deal_list = $this->App_model->system_deal_search($keyword);
            if(!empty($deal_list)) {
            $this->display_output('1','Deal List', array('list'=>$deal_list));
            } else {
                $this->display_output('0','Deal not found.');
            }

        //Search Event
        }elseif($type == "Event"){
            $event_list = $this->App_model->system_event_search($keyword);
            if(!empty($event_list)) {
            $this->display_output('1','Event List', array('list'=>$event_list));
            } else {
                $this->display_output('0','Event not found.');
            }
        }

    }

    public function display_output($status,$msg,$data=array()){

        $response =  array('status'=>$status,'msg'=>$msg);
        if(!empty($data)){
           $response= array_merge($response,$data);
        }
        echo json_encode($response); 
        exit;
    }

    public function clear_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        //$data = htmlspecialchars($data);
        return $data;
    }

    /***************************************test notification ***********************/
    public function testNotis()
    {
        $notdata['type']='post_like';
        $this->Common_model->push_notification_send('121',$notdata,'98','adf','asdf','','121');        

    }

    public function testNoti()
    {
        $iosarray = array(
            'alert' => 'Hello :- Test 2',
            // 'title' => $alert_message.$extra_msg_for_title,
            'type'  => 'Test',
            // 'user_id' => $user_id,
            // 'record_id' => $record_id,
            // 'badge' =>  $badge_count,
            'sound' => 'default',
            // 'msg' => $description_message
        );
        
        $deviceToken = '3de9c1bbc2ab9894d67ad8252a6cbbd7dc6c2bc1f2f945e43216a0d39cee4dcc';
        $passphrase = '1234';
        $ctx = stream_context_create();
   
        $pem = 'pem/pushcert.pem';
        $url = 'ssl://gateway.sandbox.push.apple.com:2195';

        stream_context_set_option($ctx, 'ssl', 'local_cert', $pem);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        // Open a connection to the APNS server
        $fp = stream_socket_client($url, $err, $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp){
           return false;
        }
        // Create the payload body
        $body['aps'] = $iosarray;

        $payload = json_encode($body);

        // Build the binary notification
        $msg = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(' ', '', $deviceToken)) . chr(0) . chr(strlen($payload)) . $payload;
        $result = fwrite($fp, $msg, strlen($msg)); 

        // Send it to the server
        if (!$result) {
            return false;
        } else {
            fclose($fp);
            return true;
        }

    }

} //End controlller class  
