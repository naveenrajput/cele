<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Static_pages extends CI_Controller {

	public function __construct() {
		parent::__construct();
		error_reporting(0);
		$this->data ='';
		$this->load->model('admin/Common_model');
	}
	
	// App Pages
	public function terms_and_conditions() {  
		$data['details'] = $this->Common_model->getRecords('pages','*',array('page_id'=>1),'',true); 
		$this->load->view('app/terms_and_conditions_mobile',$data); 
	}	
	public function about_us() {  
		$data['details'] = $this->Common_model->getRecords('pages','*',array('page_id'=>5),'',true); 
		$this->load->view('app/about_us',$data); 
	}
	
	public function privacy_policy() {  
		$data['details'] = $this->Common_model->getRecords('pages','*',array('page_id'=>2),'',true); 
		$this->load->view('app/privacy_policy_mobile',$data); 
	}
	public function contact_us() {  
		$data['details'] = $this->Common_model->getRecords('pages','*',array('page_id'=>3),'',true); 
		$this->load->view('app/contact_us',$data); 
	}
	public function cancellation_and_refund_policy() {  
		$data['details'] = $this->Common_model->getRecords('pages','*',array('page_id'=>4),'',true); 
		$this->load->view('app/cancellation_and_refund_policy',$data); 
	}	
}

/* End of file welcome.php */
/* Location: ./application/controllers/login.php */
