 // Add slideDown animation to Bootstrap dropdown when expanding.
//  $(document).ready(function () {
//   $('.dropdown').hover(function () {
//           $(this).find('.dropdown-menu').first().stop(true, true).slideDown(150);
//       }, function () {
//           $(this).find('.dropdown-menu').first().stop(true, true).slideUp(105)
//       });
//   });
 $('.dropdown').on('show.bs.dropdown', function() {
    $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
  });

  // Add slideUp animation to Bootstrap dropdown when collapsing.
  $('.dropdown').on('hide.bs.dropdown', function() {
    $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
  });

 $('.dropdown-menu').click(function(e) {
      e.stopPropagation();
  });

  /********* rating slider *********/
 
  $('.qty').hide();
$('.btn-add').click(function(){
     var $this= $(this);
     $this.hide();
     $this.next('.qty').show();
 });


    /****** incerement quantity ***********/
function incrementQty_bkp() {
  var value = document.querySelector('input[name="qty"]').value;
  var cardQty = document.querySelector(".cart-qty");
  value = isNaN(value) ? 1 : value;
  value++;
  document.querySelector('input[name="qty"]').value = value;
  cardQty.innerHTML = value;
  cardQty.classList.add("rotate-x");
}
function incrementQty(id) {
  var value = document.querySelector('#qty_'+id).value;
  value = isNaN(value) ? 1 : value;
  value++;
  document.querySelector('#qty_'+id).value = value;
}
function decrementQty(id) {
  var value = document.querySelector('#qty_'+id).value;
  value = isNaN(value) ? 1 : value;
  value > 0 ? value-- : value;
  document.querySelector('#qty_'+id).value = value;
  if(value==0){
    $('#qty_'+id).val('');
    $('#qty_'+id).parent().hide();
    $('#qty_'+id).parent().prev('button').css('display','block');
  }
}

function removeAnimation(e) {
  e.target.classList.remove("rotate-x");
}

/*const counter = document.querySelector(".cart-qty");
counter.addEventListener("animationend", removeAnimation);

$(window).on('scroll', function(event) {
  var scrollValue = $(window).scrollTop();
  if (scrollValue == settings.scrollTopPx || scrollValue > 70) {
       $('.sidebar').addClass('fixed-top');
  } 
});*/


/**** sign up form jquery for relavent form ****/
function show1(){

  //document.getElementById("celebrant_sign_up_form").reset(); 
  $("#celebrant_sign_up_form").parsley().reset();
  $("#celebrant_sign_up_form")[0].reset();
  $("#celebrant_sign_up_form #un_error_title").hide();
  $("#celebrant_sign_up_form #access_email_error").hide();
  $("#celebrant_sign_up_form #access_mobile_error").hide();
  document.getElementById('celebrant').style.display ='block';
  //document.getElementById('celebrantSP').style.display = 'none';
  document.getElementById('onlySP').style.display = 'none';
}

function show3(type){
/*  document.getElementById("sp_sign_up_form").reset(); 
  document.getElementById("sp_sign_up_form")[0].parsley().reset();*/
    $("#sp_sign_up_form").parsley().reset();
    $("#sp_sign_up_form")[0].reset();
    $("#sp_sign_up_form #un_error_title").hide();
    $("#sp_sign_up_form #access_email_error").hide();
    $("#sp_sign_up_form #access_mobile_error").hide();
  document.getElementById('celebrant').style.display ='none';
  //document.getElementById('celebrantSP').style.display = 'none';
  document.getElementById('onlySP').style.display = 'block';
}


/******   subscription div *******/

function monthly(){
  document.getElementById('monthly').style.display ='block';
  document.getElementById('quarterly').style.display = 'none';
  document.getElementById('yearly').style.display = 'none';
}
function quarterly(){
  document.getElementById('monthly').style.display ='none';
  document.getElementById('quarterly').style.display = 'block';
  document.getElementById('yearly').style.display = 'none';
}
function yearly(){
  document.getElementById('monthly').style.display ='none';
  document.getElementById('quarterly').style.display = 'none';
  document.getElementById('yearly').style.display = 'block';
}

