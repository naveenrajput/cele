const mysql = require('mysql');

// const connection = mysql.createConnection({
//   host: process.env.DB_HOST,
//   user: process.env.DB_USERNAME,
//   password: process.env.DB_PASSWORD,
//   database: process.env.DB_DATABASE,
// });

const connection  = mysql.createPool({
	connectionLimit : 5,
	host            : process.env.DB_HOST,
	user            : process.env.DB_USERNAME,
	password        : process.env.DB_PASSWORD,
	database        : process.env.DB_DATABASE
});

this.getRecord=function(table,fields,conditions='',orderby='',single_row=false,groupby='',limit='',offset=''){  

	return promise = new Promise(function(resolve,reject){ 

		var	where = '';  
		if(conditions!=''){ 
			condition =[]; 
			if(conditions!=''){
				condition.push(conditions);
			}

		 	for(let i of condition){  
				colum_name = Object.keys(i);
			 	values = Object.keys(i).map(key => i[key]);  
			} 
			where += ' where '; 

			for (var i = 0; i < colum_name.length; i++) {
				
				if(i==0){
					where += '`'+colum_name[i]+'` = \''+values[i]+'\'';		 
				}else{
					where += ' AND `'+colum_name[i]+'`  = \''+values[i]+'\'';		 
				}
			}  
		} 
		if(orderby!=''){
			orderby = ' Order By ' +orderby;
		}
		if(groupby!=''){

			groupby = 'group by ' +groupby;
		}
		if(limit!=''){
			limit = ' limit '+offset+' ,'+limit;
		}

		var sql = 'SELECT '+fields+' FROM '+table+' '+where+' '+groupby+' '+orderby +' '+limit;
		
		connection.query(sql, (err,rows,fields) => {
			console.log(err,rows,'user_sql');
		if(err){
		 	myObj = { status: '0',  msg: err.sqlMessage};    
			resolve(myObj);  return
		}else{ 
		  if(single_row==true){  
		  	rows = rows[0];    
		  	myObj = {  status: '1',  msg: rows};     
				resolve(myObj);  return 
		  }else{ 
		  	myObj = {  status: '1',  msg: rows};     
				resolve(myObj);  return   
		  } 
		} 
		});

	})  	 
} 

this.addEditRecord =function(table,datas,wheres=''){ 
	return promise = new Promise(function(resolve,reject){ 
		data = [];
		if(Object.keys(datas).length > 0){
			data.push(datas);
		} 
		where = [];
		if(Object.keys(wheres).length > 0){
			where.push(wheres);
		}
	 	// console.log(where);
		if(where==''){

				if(data!=''){ 

				 	for(let i of data){  
						colum_name = Object.keys(i);
					 	values = Object.keys(i).map(key => i[key]);  
					} 
		 	
					col_name= '';
					values_name= '';
					console.log(values);
					for (var i = 0; i < colum_name.length; i++) {
						
						if(i==0){
							col_name += '`'+colum_name[i]+'`';		 
							values_name += '\''+values[i]+'\'';		 
						}else{
							col_name += ', `'+colum_name[i]+'`';		 
							values_name += ', \''+values[i]+'\'';		 
						}
					}  
				} 
				console.log(values_name);
			 	var sql = 'Insert into '+table+' ('+col_name+') values ('+values_name+') ';
		 		console.log(sql,'insert_chat')
				connection.query(sql, (err,result) => {
					if(err){  
					 	myObj = {  status: '0',  msg: err.sqlMessage};    
					 	console.log(err.sqlMessage); 
						resolve(myObj);return
					}else{ 
					 	myObj = {  status: '1',  msg: result.insertId};     
						resolve(myObj);return 
						    
					}
				});  
		}else{
			if(where!=''){ 

			 	for(let i of where){  
					colum_name = Object.keys(i);
				 	values = Object.keys(i).map(key => i[key]);  
				} 
				wheres = ' '; 

				for (var i = 0; i < colum_name.length; i++) {
					
					if(i==0){
						wheres += '`'+colum_name[i]+'` = \''+values[i]+'\'';		 
					}else{
						wheres += ' AND `'+colum_name[i]+'`  = \''+values[i]+'\'';		 
					}
				}
 
				for(let i of data){  
					colum_name = Object.keys(i);
				 	values = Object.keys(i).map(key => i[key]);  
				} 
				datas = ' '; 

				for (var i = 0; i < colum_name.length; i++) {
					
					if(i==0){
						datas += '`'+colum_name[i]+'` = \''+values[i]+'\'';		 
					}else{
						datas += ' , `'+colum_name[i]+'`  = \''+values[i]+'\'';		 
					}
				}  

			}  

				var sql = 'UPDATE '+table+' SET '+datas+' where ('+wheres+') ';
		 		// console.log(sql);
				connection.query(sql, (err,result) => {
					if(err){  
						 myObj = {  status: '0',  msg: err.sqlMessage};     
						 console.log(err.sqlMessage)
						resolve(myObj);return
					}else{ 
						 myObj = {  status: '1',  msg: 1};     
						resolve(myObj);return 
						 return 	 
					}
				});  
		}
		 
	})
}


/*this.get_user_profile = function(user_id){

	return promise = new Promise(function(resolve,reject){

		var	sql = "SELECTd IFNULL(AVG(r.rating), 0) as rating, IFNULL(COUNT(r.rating), 0) as total_review, `u`.*, `co`.`name` as `country`,`s`.`name` as `state`, `c`.`name` as `city` FROM `users` `u`JOIN `countries` `co` ON `co`.`id`=`u`.`country_id` JOIN `states` `s` ON `s`.`id`=`u`.`state_id` JOIN `cities` `c` ON `c`.`id`=`u`.`city_id` LEFT JOIN `reviews` `r` ON `r`.`receiver_id`=`u`.`user_id` and `r`.`is_deleted`=0 and `r`.`status` ='Active' WHERE `u`.`is_deleted` =0 AND `u`.`user_id` = "+user_id +' limit 1';

			// console.log(sql);
			connection.query(sql, (err,result) => {
				if(err){  
					 myObj = {  status: '0',  msg: err.sqlMessage};     
					resolve(myObj);return
				}else{ 
					if(result.length > 0){
					 myObj = {  status: '1',  msg: result[0]};  
					 resolve(myObj);return 
					}else{
					 	myObj = {  status: '1',  msg: 1};  
						resolve(myObj);return 
					 	return
					} 		
					 
				}
			})
 
	})
}

*/


this.findRoomId= function(sender_id,receiver_id,job_id=0){
	return new Promise(function(resolve,reject){ 
		var	sql = "select * FROM `chat_room` where (`first_user`='"+sender_id+"' and `second_user`='"+receiver_id+"' and `job_id`='"+job_id+"') or (`second_user`='"+sender_id+"' and `first_user`='"+receiver_id+"' and `job_id`='"+job_id+"') ";
			console.log(sql);
			connection.query(sql, (err,result) => {
			if(err){  
				console.log(err.sqlMessage);
				 myObj = {  status: '0',  msg: err.sqlMessage};
				resolve(myObj);
			}else{  
				
				if(result.length > 0){ 
				 	resolve(result[0]); 
				}else{ 
					resolve(0);    
				} 		 	 
			}
		}) 
	})  
}
this.getUserIdFromChat= function(socket_id){

	return new Promise(function(resolve,reject){ 
		var	sql = "select if(first_user_chat_id='"+socket_id+"',first_user,second_user) as user_id FROM `chat_room` where (`first_user_chat_id`='"+socket_id+"' OR `second_user_chat_id`='"+socket_id+"'  ) limit 1";
				// console.log(sql);
			connection.query(sql, (err,result) => {
			if(err){  
				console.log(err.sqlMessage);
				 myObj = {  status: '0',  msg: err.sqlMessage};
				resolve(myObj);
			}else{   
				if(result.length > 0){ 
				 	resolve(result[0]); 
				}else{ 
					resolve(0);    
				} 		
				 
			}
		}) 
	})  
}


this.getChatList = function(user_id,action){
	// console.log('user_id'+user_id);
	return new Promise(function(resolve,reject){  
		var where = '';
			if(action=='Normal'){
				var where = ' AND if(chat_room.first_user="'+user_id+'",chat_room.first_user_archive=0,chat_room.second_user_archive=0)';
			}else{
				var where = ' AND if(chat_room.first_user="'+user_id+'",chat_room.first_user_archive=1,chat_room.second_user_archive=1)';
			} 

			 where += ' AND if(chat_room.first_user="'+user_id+'",chat_room.first_user_delete=0,chat_room.second_user_delete=0)';
			 
			//  where += ' Having message!=\'\' ';

			// var	sql = "SELECT  users.chat_status, if(chat_room.first_user="+user_id+",chat_room.second_user,chat_room.first_user) as other_user_id,if(chat_room.first_user="+user_id+",chat_room.second_user,chat_room.first_user) as other_user_id,chat_room.first_user as sender_id,chat_room.second_user as receiver_id, CONCAT(users.first_name,' ',users.last_name) as user_name,if(users.profile_pic='','resources/images/profile/default_image.png',users.profile_pic) as profile_pic,IFNULL(job.job_title,'') as job_title,chat_room.id as room_id, chat_room.job_id,(SELECT COUNT(chat.is_read) as unread FROM chat WHERE chat.room_id = chat_room.id and chat.receiver_id ="+user_id+" and chat.is_read=1 ORDER by chat.id DESC LIMIT 1) as unread,(SELECT chat.message FROM chat WHERE chat.room_id = chat_room.id ORDER by chat.id DESC LIMIT 1) as message,(SELECT chat.created FROM chat WHERE chat.room_id = chat_room.id ORDER by chat.id DESC LIMIT 1) as created FROM chat_room  join `users` on users.user_id = if(chat_room.first_user="+user_id+",chat_room.second_user,chat_room.first_user) LEFT JOIN job on job.id = chat_room.job_id WHERE (chat_room.first_user ="+user_id+" OR chat_room.second_user="+user_id+") "+where+" ";
			var	sql = `SELECT  users.chat_status, 
								if(chat_room.first_user=${user_id},chat_room.second_user,chat_room.first_user) as other_user_id,
								if(chat_room.first_user=${user_id},chat_room.second_user,chat_room.first_user) as other_user_id,
								chat_room.first_user as sender_id,
								chat_room.second_user as receiver_id, 
								users.fullname as user_name,
								if(users.profile_pic='','resources/images/profile/default_image.png',users.profile_pic) as profile_pic,
								chat_room.id as room_id,
								(SELECT COUNT(chat.is_read) as unread FROM chat WHERE chat.room_id = chat_room.id and chat.receiver_id =${user_id} and chat.is_read=1 ORDER by chat.id DESC LIMIT 1) as unread,
								(SELECT chat.message FROM chat WHERE chat.room_id = chat_room.id ORDER by chat.id DESC LIMIT 1) as message,
								(SELECT chat.created FROM chat WHERE chat.room_id = chat_room.id ORDER by chat.id DESC LIMIT 1) as created 
								FROM chat_room  
								join users on users.user_id = if(chat_room.first_user=${user_id},chat_room.second_user,chat_room.first_user) 
								WHERE (chat_room.first_user =${user_id} OR chat_room.second_user=${user_id}) 
								${where} `;
			
			// var sql = `SELECT  	users.chat_status,
			// 					request_by as other_user_id,
			// 					users.fullname as user_name,
			// 					if(users.profile_pic='','resources/images/profile/default_image.png',users.profile_pic) as profile_pic,
			// 					cr.course_id as room_id,
			// 					(SELECT COUNT(chat.is_read) as unread FROM chat WHERE chat.room_id = cr.course_id and chat.receiver_id =${user_id} and chat.is_read=1 ORDER by chat.id DESC LIMIT 1) as unread,
			// 					(SELECT chat.message FROM chat WHERE chat.room_id = cr.course_id ORDER by chat.id DESC LIMIT 1) as message,
			// 					(SELECT chat.created FROM chat WHERE chat.room_id = cr.course_id ORDER by chat.id DESC LIMIT 1) as created 
			// 					FROM course_request AS cr
			// 					join users on users.user_id = cr.request_by AND users.chat_status = 'Online'
			// 					WHERE ((cr.request_by =${user_id} AND cr.request = 'Accept') 
			// 					OR cr.mentor_id=${user_id}) 
			// 					AND cr.course_id = ${course_id}`
			console.log(sql);
			connection.query(sql, (err,result) => {
			if(err){ 
				 myObj = {  status: '0', msg: err.sqlMessage};
				resolve(myObj);
			}else{   
				if(result.length > 0){  
				 	resolve(result); 
				}else{ 
					resolve(0);    
				} 		
				 
			} 
		})
	}) 
}

this.getChatHistory = function(user_id,room_id,page,limit){
	
	return new Promise(function(resolve,reject){  
			var	sql = "SELECT chat.original_file_name,chat.id,chat.file_format,chat.sender_id,chat.receiver_id,sender.fullname as user_name,if(sender.profile_pic='','resources/images/profile/default_image.png',sender.profile_pic) as profile_pic,chat.room_id,chat.type,chat.job_id,chat.message,chat.created FROM `chat` join `users`sender on sender.user_id = chat.sender_id   WHERE chat.room_id="+room_id+" AND chat.message!='' And (if(chat.sender_id="+user_id+",chat.sender_id_delete=0,chat.receiver_id_delete=0)) ORDER BY chat.id DESC limit "+page+" ,"+limit;
			console.log(sql);
			connection.query(sql, (err,result) => {
			if(err){  
				console.log(err.sqlMessage);
				 myObj = {  status: '0', msg: err.sqlMessage};
				resolve(myObj);
			}else{   
				if(result.length > 0){  
				 	resolve(result); 
				}else{ 
					resolve('0');    
				} 		 	 
			} 
		})
	}) 
}
