const model = require('./model.js');
var confiq = require('./confiq.js'); 
const mysql = require('mysql');
const connection = mysql.createConnection({
  host: '192.168.2.196',
  user: 'root',
  password: 'root',
  database: 'munka'
});
var strtotime = require('strtotime');
var dateFormat = require('dateformat');
this.login = function login(datas){
 
	return	promise = new Promise(function(resolves,reject){  
 		(async()=>{ 
			data = datas.body;
			var email = data.email; 
		 	var password = data.password;
			if(email=='' || email==undefined){ 
				var myObj = {  status: '0',msg: 'Please enter email.'};  
				 	resolves(myObj);  
			}
			if(password=='' || password==undefined){ 
				var myObj = {  status: '0',msg: 'Please enter password.'};  
					resolves(myObj);   
				  
			}

			var password_encoded = Buffer.from(password).toString('base64') 
		 
			var my_object = { email: email,password:password_encoded}; 

			var basicArray =[]; 
			  
			basicArray.push(my_object); 
		 	 
	 		var	result = await model.getRecord('users','*',basicArray,'user_id DESC',true,'user_id','','');
 					if(result.status==0){ 
 						resolves(result); 
 					}else{
 						var myObj = {   status: '1',  msg: 'Login Successfully.','details':result.msg};   
 					
		 				resolves(myObj);
 					}
		 	})()
		}) 

 
}



this.signup = async(datas)=>{

	return	promise = new Promise(function(resolve,reject){ 

				var	data = datas.body;
				var file = datas.files;
			 	var user_type =data.user_type;
				if(user_type=='' || user_type==undefined){ 
					var myObj = {   status: '0',  msg: 'Please enter user type.'};  
				 	resolve(myObj); 
				 	return
				}else{

					if(user_type!='Individual' && user_type!='Professional'){
						var myObj = {   status: '0',  msg: 'User type must be either Individual or Professional.'};  
				 		resolve(myObj);  
				 		return
					}

				}

				var first_name =data.first_name;
				if(first_name=='' || first_name==undefined){ 
					var myObj = {   status: '0',  msg: 'Please enter first name.'};  
				 	resolve(myObj); 
				 	return
				}

				var last_name =data.last_name;
				if(last_name=='' || last_name==undefined){ 
					var myObj = {   status: '0',  msg: 'Please enter last name.'};  
				 	resolve(myObj);
				 	return 
				}

				var email = data.email; 
				if(email=='' || email==undefined){ 
					var myObj = {   status: '0',  msg: 'Please enter email.'}; 
					 
				 	resolve(myObj); 
				 	return
				}

			 	var password = data.password;
				if(password=='' || password==undefined){ 
					var myObj = {   status: '0',  msg: 'Please enter password.'}; 
				 	resolve(myObj); 
				 	return
				}
			 
			 	var ssn_number = data.ssn_number;
			 	if(ssn_number==undefined){
			 		ssn_number = '';
			 	}
			 	var llc_number = data.llc_number;
			 	if(llc_number==undefined){
			 		llc_number = '';
			 	}

			 	var country_id = data.country_id;  
				if(country_id=='' || country_id==undefined){ 
					var myObj = {   status: '0',  msg: 'Please enter country name.'}; 
				 	resolve(myObj); 
				 	return
				}

			 	var state_id = data.state_id;  
				if(state_id=='' || state_id==undefined){ 
					var myObj = {   status: '0',  msg: 'Please enter state name.'}; 
				 	resolve(myObj); 
				 	return
				}

			 	var city_id = data.city_id;  
				if(city_id=='' || city_id==undefined){ 
					var myObj = {   status: '0',  msg: 'Please enter city name.'}; 
				 	resolve(myObj); 
				 	return
				}

			 	var address = data.address;  
				if(address=='' || address==undefined){ 
					var myObj = {   status: '0',  msg: 'Please enter address name.'}; 
				 	resolve(myObj); 
				 	return
				}

			 	var latitude = data.latitude;  
				if(latitude=='' || latitude==undefined){ 
					var myObj = {   status: '0',  msg: 'Please enter latitude.'}; 
				 	resolve(myObj); 
				 	return
				}
			 
			 	var longitude = data.longitude;  
				if(longitude=='' || longitude==undefined){ 
					var myObj = {   status: '0',  msg: 'Please enter longitude.'}; 
				 	resolve(myObj); 
				 	return
				}
			    
				 
					if(file.resume!='' &&  file.resume!== undefined){
						resume_file = file.resume[0].path;
					}else{
						resume_file = '';
					}
					if(file.profile_pic!='' &&  file.profile_pic!== undefined){
						profile_pic_file = file.profile_pic[0].path;
					}else{
						profile_pic_file = '';
					}
				 
			 


				var password_encoded = Buffer.from(password).toString('base64') 
			 
				var my_object = {
					 user_type:user_type,
					 first_name:first_name,
					 last_name:last_name, 
					 email: email,
					 password:password_encoded,
					 ssn_number:ssn_number,
					 // llc_number:llc_number,
					 country_id:country_id,
					 state_id:state_id,
					 city_id:city_id,
					 address:address,
					 latitude:latitude,
					 longitude:longitude, 
					 profile_pic:profile_pic_file, 
					 resume:resume_file, 
					 access_pin:'', 
					 country_code:'', 
					 mobile:'', 
					 fb_id:'', 
					 google_id:'', 
					 business_name:'', 
					 tax_id:'', 
					 document:'', 
					 is_professional:1, 
					 professional_proof:1, 
					 freelancer_plus:1, 
					 verified_freelancer:1, 
					 reset_token:1, 
					 reset_token_date:'', 
					}; 

				var basicArray =[]; 
				  
				basicArray.push(my_object);  
				(async()=>{
					result = await model.addEditRecord('users',basicArray,'');
						
					if(result.status==0){
						resolve(result);
					}else{
						if(result.msg!=undefined){
							var where = []
							where.push({ user_id:result.msg});
							(async()=>{

								user_result = await	model.getRecord('users','*',where,'user_id DESC',true,'user_id','','');
								if(user_result.status==0){
									resolve(user_result); 
								}else{
									var myObj = {  status: '1',  msg: 'User Added Successfully.','details':user_result.msg};    
						 			resolve(myObj); 		 
								}
							})() 
						}


					}
 
			})()

		})


}


this.account_verification = async(datas)=>{

	return promise = new Promise(function(resolve,reject){
		data = datas.body;
 		var email = data.email; 
		if(email=='' || email==undefined){ 
			var myObj = {   status: '0',  msg: 'Please enter email.'};   
		 	resolve(myObj); 
		 	return
		}
		var type = data.type; 
		if(type=='' || type==undefined){
			var myObj = {   status: '0',  msg: 'Please enter type.'};   
		 	resolve(myObj); 
		 	return
		}else{ 
			if(type!='Email' && type!='Mobile'){
				var myObj = {   status: '0',  msg: 'Verification type must be either Email or Mobile.'};  
		 		resolve(myObj);  
		 		return
			} 
		}

		connection.beginTransaction(function(err) {

			var verification_code = data.verification_code; 
			if(verification_code=='' || verification_code==undefined){ 
				var myObj = {   status: '0',  msg: 'Please enter verification code.'};   
			 	resolve(myObj); 
			 	return
			}
 			var my_object = {email: email,is_deleted:'0'}; 

			var basicArray =[]; 
			  
			basicArray.push(my_object); 

			(async()=>{  

				var	results = await model.getRecord('users','user_id,email_verification_token,email_verification_token_date,mobile_verified,email_verified, mobile_verification_token,mobile_verification_token_date',basicArray,'',true);
					if(results.status==0){ 
						resolves(results); 
					}else{
						 
						if(results.msg=='' || results.msg==undefined){
							 
							var myObj = {   status: '0',  msg: 'Please enter registred email.'};   
				 			resolve(myObj); 
						}else{

							if(type=='Email'){

								result = results.msg; 
								email_verification_token = result.email_verification_token;

								if(email_verification_token=='' || email_verification_token==undefined){
									var myObj = {   status: '0',  msg: 'Already Verfied.'};   
								 	resolve(myObj); 
								 	return
								}

								if(email_verification_token!=verification_code){
									var myObj = {   status: '0',  msg: 'Invalid verification code.'};   
								 	resolve(myObj); 
								 	return
								}else{
									var now = new Date(); 
									var token_date = strtotime(dateFormat(result.email_verification_token_date,'isoDate'));  
									var current_date = strtotime(dateFormat(now,'isoDate'));  
											 // console.log(token_date);
									var diff=current_date-token_date;
									// console.log(diff);
									if(diff > 50000000){
										var myObj = {   status: '0',  msg: 'This email verification code is no longer Valid.'};   
									 	resolve(myObj); 
									 	return
									}else{
										array = [];
									    if (results.mobile_verified==1) { 
									    	var object_data = {
									    		status:'Active',
									    		email_verification_token:'',
									    		email_verification_token_date:'',
									    		email_verified:1, 
									    	};  
			                            }else{ 
			                                var object_data = { 
									    		email_verification_token:'',
									    		email_verification_token_date:'',
									    		email_verified:1, 
									    	};  
			                            } 
		                            	array.push(object_data);  
		                            	(async()=>{   
	                    					updated = await model.addEditRecord('users',array,basicArray);    	 
	                    					var user_id = result.user_id;
	                    					user_record = await model.get_user_profile(user_id); 
	                    					// console.log(user_record);
	                					

						 					  connection.commit(function(err) {
										        if (err) { 
										          connection.rollback(function() {
										          	var myObj = {  status: '0',  msg: 'Some Error Occored.'};    
						 							resolve(myObj); 
										          });
										        }
										       	var myObj = {  status: '1',  msg: 'Email verification completed successfully.','details':user_record};    
						 						resolve(myObj); 
										      });


		                            	})()  
									} 
								}
							}else{

								result = results.msg; 
								
								  if(result.mobile_verification_token==''  || result.mobile_verification_token==undefined) { 
					                    var myObj = {   status: '0',  msg: 'Verification code already used.'};   
									 	resolve(myObj); 
									 	return 
					                } else {

					                	if(result.mobile_verification_token != verification_code) { 
				                         	var myObj = {   status: '0',  msg: 'Invalid verification code.'};   
										 	resolve(myObj); 
										 	return 
					                    } else { 
					                    	
				                    		var now = new Date(); 
											var token_date = strtotime(dateFormat(result.mobile_verification_token,'isoDate'));  
											var current_date = strtotime(dateFormat(now,'isoDate'));  
													 // console.log(token_date);
											var diff=current_date-token_date;
											console.log(diff);
											if(diff > 500000000000){
												var myObj = { status: '0',  msg: 'This email verification code is no longer Valid.'};   
											 	resolve(myObj); 
											 	return
											}else{
												array = [];
											    if (results.email_verified==1) { 
											    	var object_data = {
											    		status:'Active',
											    		sms_notification:'1',
											    		mobile_verification_token:'',
											    		mobile_verification_token_date:'',
											    		mobile_verified:1,  
											    	};  
					                            }else{ 
					                                var object_data = { 
											    		mobile_verification_token:'',
											    		mobile_verification_token_date:'',
											    		mobile_verified:1, 
											    	};  
					                            } 
				                            	array.push(object_data);   
				                            	(async()=>{   
			                    					updated = await model.addEditRecord('users',array,basicArray);
			                    					var user_id = result.user_id;
			                    					user_record = await model.get_user_profile(user_id); 

		                    					  	connection.commit(function(err) {
										        	if (err) { 
											          connection.rollback(function() {
											          	var myObj = {  status: '0',  msg: 'Some Error Occored.'};    
							 							resolve(myObj); 
											          });
											        }
											        if(user_record.status==0){
											        	  connection.rollback(function() {  
												        	var myObj = {  status: '0',  msg: user_record.msg};    
								 							resolve(myObj); 
							 							 });

											        }else{
											        	var myObj = {  status: '1',  msg: 'Mobile verification completed successfully.','details':user_record};    
								 						resolve(myObj); 
											        }
											      	}); 
											       	
 
				                            	})()
											} 
					                    }
					                }
							}

						}
					} 
			})()  

		})


});
 	




}