var express = require('express');
var ucfirst = require('ucfirst');
require('dotenv').config()
var app     = express();  
var bodyParser = require('body-parser');
var multer  = require('multer'); // File upload
var model = require('./model.js');
var path = require('path');
var fs = require('fs');
var document_path = 'chat_file/';


const http  = require('http').Server(app); 
const io    = require('socket.io')(http);
const request = require('request');
const mysql = require('mysql');
const SocketIOFile = require('socket.io-file');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));  
app.use("/static", express.static('./static/'));
app.use("/chat_file", express.static('./chat_file/'));


// const connection = mysql.createConnection({
//     host: process.env.DB_HOST,
//     user: process.env.DB_USERNAME,
//     password: process.env.DB_PASSWORD,
//     database: process.env.DB_DATABASE,
//     dateStrings:true,
// });

const connection  = mysql.createPool({
	connectionLimit : 5,
	host            : process.env.DB_HOST,
	user            : process.env.DB_USERNAME,
	password        : process.env.DB_PASSWORD,
	database        : process.env.DB_DATABASE
});

const storage = multer.diskStorage({
    destination: (req, file, cb) => { 
        cb(null, 'chat_file/')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now()+path.extname(file.originalname))
    }
});
var upload = multer({storage: storage});
      

/*app.all('/',function(req,res){ 
  res.render('../view/chat_window.ejs');  
}) */
app.all('/',function(req,res){ 
  res.render('../view/chat.ejs');  
}) 
app.get('/socket.io.js', (req, res, next) => {
    return res.sendFile(__dirname + '/node_modules/socket.io-client/dist/socket.io.js');
});
 
app.get('/socket.io-file-client.js', (req, res, next) => {
    return res.sendFile(__dirname + '/node_modules/socket.io-file-client/socket.io-file-client.js');
});


/*app.all('/chat2',function(req,res){ 
  res.render('../view/index2.ejs');  
}) */

const notificationUrl = process.env.BASE_PATH+'send_notification';
user_list = [];

io.sockets.on('connection', function(socket) {    
        console.log('Connect');
        // console.log('socket_id'+socket_id);
  
		socket.on('send_message',function(data){  
            socket_id = socket.id;
            // io.emit()online_offline
			(async()=>{ 
                console.log(data)
			 	ss = {status:'Online',user_id:data.sender_id} //for web
	        	io.emit('online_offline',ss); 
	        	await model.addEditRecord('users',{ chat_status:'Online'},{user_id:data.sender_id});    
				sender_id  = data.sender_id;
                receiver_id = data.receiver_id;
                message = data.message ;
                type = data.type;
                file_type = data.file_type ;
                job_id = data.job_id;  
                console.log('Sending Data'); 
                console.log(data);
			 	(async()=>{  
                    var read =0;
		 		  	var sender_image = await model.getRecord('users',"IF(profile_pic='','resources/images/profile/default_image.png',profile_pic) as profile_pic",{user_id:sender_id},'',true);   
		 		  
	 		   		image = sender_image.msg.profile_pic;
		 		  
		 		  	var record_sender_data = {
		 		  		message:message,
		 		  		profile_pic:image,
		 		  		original_file_name:data.original_file_name,
		 		  		type:data.type,
		 		  		sender_id:data.sender_id,
		 		  		receiver_id:data.receiver_id,
		 		  	}
		 		  	 // console.log('Data Return');
		 		  	 // console.log(record_sender_data);
		            room_record = await  model.findRoomId(sender_id,receiver_id,job_id);   
                    console.log(room_record, 'Room record data',Object.keys(room_record).length==0)
                    if(Object.keys(room_record).length==0){  

                    	var my_object = {  
                            first_user:sender_id,
                            second_user:receiver_id, 
                            first_user_chat_id:socket_id, 
                            job_id:job_id, 
                        };  
                        record = await model.addEditRecord('chat_room',my_object); 
                        console.log(record,'record')
                        room_id = record.msg;
                        other_user_socket_id = '';
                        noti_user = receiver_id;

	            	}else{ 
                        room_id = room_record.id; 
                        // console.log('dd'+room_record.first_user);

                        if(room_record.first_user==sender_id){ 
                            // console.log('send'+sender_id);
                            other_user_socket_id = room_record.second_user_chat_id;
                            noti_user = room_record.second_user;
                            var my_object = {first_user_chat_id:socket_id}

                        }else{
                            noti_user = room_record.first_user;
                            other_user_socket_id = room_record.first_user_chat_id;
                            var my_object = {second_user_chat_id:socket_id}
                        } 

                         await model.addEditRecord('chat_room',my_object,{id:room_id}); 

                    }    
                    
                   console.log('receiving response');
                       
                    var message_name = message;
                    if(other_user_socket_id=='' && message_name!=''){  
                        var getUnreadMsg = await model.getRecord('chat',"COUNT(`is_read`) as read_count ",{sender_id:sender_id,receiver_id:receiver_id,is_read:1},'',true);   
                        // ;

                        io.emit('read_count',{count:getUnreadMsg.msg.read_count,user_id:sender_id}); 
                        read = 1;
                        console.log('ffff'+noti_user);
                        sendNotification(noti_user,'You received message.');
                    }

                    // File Upload 
                    /*if(type!='Text' && type!=''){
                        var message_name = document_path+ranDomNumber()+'.'+file_type;
                        console.log(message_name);
                        require("fs").writeFile(message_name,message,'binary',function(err) {
                            console.log(err);
                        }); 
                    }else{
                    }*/
                     
                    console.log(message_name!='' && type=='Text');
                  
                    if(message_name!='' && type=='Text'){        
      			        var my_object = { 
          					room_id: room_id,
          					sender_id:sender_id,
          					receiver_id:receiver_id,
                            message:message_name, 
                            job_id:job_id, 
                            type:type,  
                            is_read:read, 
          					created:getCurrentTime(), 
                          };  
                          
        				(async()=>{ 
                            console.log(my_object,'insert meessage') 
        					insert_message = await model.addEditRecord('chat',my_object);   
                            console.log(insert_message);
        					await model.addEditRecord('chat_room',{first_user_archive:0,second_user_archive:0},{id:room_id}); 
    					  	console.log('Data Send Socket'); 
        					console.log(record_sender_data);
        				})()    	
                    } 

                    
                    if(message_name!=''){
                    	// console.log('Fire')
                    	// console.log(record_sender_data)
						io.to(`${other_user_socket_id}`).emit('get_message',record_sender_data); 
					}
		        })(); 
 
			})(); 
 
		});

   	    socket.on('disconnect', function(data) { 
       	    socket_id = socket.id;
        	console.log('disconnect');
    		(async()=>{   
    			record = await model.getUserIdFromChat(socket_id);    
		 	 	ss = {status:'Offline',user_id:record.user_id}
    			io.emit('online_offline',ss); //for web 
    			disconnect = await model.addEditRecord('chat_room',{ first_user_chat_id:''},{ first_user_chat_id:socket_id});   
    			disconnect = await model.addEditRecord('chat_room',{ second_user_chat_id:''},{ second_user_chat_id:socket_id});   
    			disconnect = await model.addEditRecord('users',{ chat_status:'Offline'},{user_id:record.user_id});   
    		})() 
              
})
  
}); 


app.post('/chat_list',upload.single('uploaded_image'),function(req,res){ 
    var user_id = req.body.user_id; 
    var action = req.body.action; 
    // console.log(user_id);
    if(user_id=='' || user_id ==undefined){ 
        var myObj = {  status: '0',msg: 'Please enter user id.'};  
        return res.send(myObj);    
    }   
    if(action=='' || action ==undefined){ 
        var myObj = {  status: '0',msg: 'Please enter action.'};  
        return res.send(myObj);    
    }else{
    	if(action!='Normal' && action!='Archive'){
    		var myObj = {  status: '0',msg: 'Action Must be Normal or Archive.'};  
        return res.send(myObj);
    	}
    }  
    (async()=>{ 

	 		var sender_image = await model.getRecord('users',"IF(profile_pic='','resources/images/profile/default_image.png',profile_pic) as profile_pic",{user_id:user_id},'',true);   
            console.log(sender_image,'senderData')
	   		user_image = sender_image.msg.profile_pic;
           chating_list = await model.getChatList(user_id,action); 
           console.log(chating_list)   
           if(chating_list!=0){
           		// console.log('chating_list');
           		console.log(chating_list);
                var myObj = {  status: '1',msg: 'data found',details:chating_list,'user_image':user_image};  
                return res.send(myObj);    
           }else{
                var myObj = {  status: '0',msg: 'No Record Found.'};  
                return res.send(myObj);   
           } 
    })()   
}) 
 
app.post('/file_upload',upload.single('message'),function(req,res){ 
    var sender_id = req.body.sender_id;
    var receiver_id = req.body.receiver_id;
    var job_id = req.body.job_id; 
    var type = req.body.type; 
    var file_format = req.body.file_format; 
      var file = req.file;
    // console.log(file_format);
    console.log(file);
    console.log(req);
    if(sender_id=='' || sender_id ==undefined){ 
        var myObj = {  status: '0',msg: 'Please enter sender id.'};  
        return res.send(myObj);    

    }  
    if(receiver_id=='' || receiver_id ==undefined){ 
        var myObj = {  status: '0',msg: 'Please enter receiver id.'};  
        return res.send(myObj);    
    }  
    if(type=='' || type ==undefined){ 
        var myObj = {  status: '0',msg: 'Please enter type.'};  
        return res.send(myObj);    
    }else{
        if(type!='Video' && type!='Image' && type!='Document'){
            var myObj = {  status: '0',msg: 'Type must be Video,Image Or document.'};  
            return res.send(myObj);    
        }
    }      
    if(file_format=='' || file_format ==undefined){ 
        var myObj = {  status: '0',msg: 'Please enter file format.'};  
        return res.send(myObj);    
    }     
    var file = req.file;
    if(file!='' &&  file!== undefined){
        upload_file = file.path;
        originalname = file.originalname;
        // console.log(file);
    }else{
        var myObj = {  status: '0',msg: 'Please upload file.'};  
        return res.send(myObj);    
    } 

    // console.log(file); 
    (async()=>{  
        room_record = await  model.findRoomId(sender_id,receiver_id,job_id);   
        if(Object.keys(room_record).length==0){   
             var my_object = {  
                    first_user:sender_id,
                    second_user:receiver_id, 
                    first_user_chat_id:socket_id, 
                    job_id:job_id, 
                };  
            record = await model.addEditRecord('chat_room',my_object);  
            room_id = record.msg; 
        }else{ 
            room_id = room_record.id; 
        }   
        var my_object = { 
            room_id: room_id,
            sender_id:sender_id,
            receiver_id:receiver_id,
            message:upload_file, 
            job_id:job_id, 
            file_format:file_format, 
            original_file_name:originalname, 
            type:type, 
            created:getCurrentTime()
        };    
        insert_message = await model.addEditRecord('chat',my_object);     
        record = await model.getRecord('chat','*',{room_id:room_id},'id DESC',true);   
        var myObj = {  status: '1',msg: 'data found',details:record.msg};  
        return res.send(myObj);      
    })();        
 
})
  



app.post('/chat_histroy',upload.single('uploaded_image'),function(req,res){ 
    // var room_id = req.body.room_id; 
    var limit = req.body.limit; 
    var page = req.body.page; 
    var sender_id = req.body.sender_id; 
    var receiver_id = req.body.receiver_id; 
    var job_id = req.body.job_id; 
    console.log(limit);
    console.log(page);
    console.log(sender_id);
    console.log(receiver_id);
    console.log(job_id);
    if(sender_id=='' || sender_id ==undefined){ 
        var myObj = {  status: '0',msg: 'Please enter sender id.'};  
        return res.send(myObj);    
    }
    if(receiver_id=='' || receiver_id ==undefined){ 
        var myObj = {  status: '0',msg: 'Please enter receiver id.'};  
        return res.send(myObj);    
    } 
  
    // if(room_id=='' || room_id ==undefined){ 
    //      room_id =0;
    // } 
    if(limit=='' || limit ==undefined){ 
        var myObj = {  status: '0',msg: 'Please enter limit.'};  
        return res.send(myObj);    
    }  
    if(page=='' || page ==undefined){ 
        var myObj = {  status: '0',msg: 'Please enter page.'};  
        return res.send(myObj);    
    }
    var page = ((page-1)*limit);  
        (async()=>{  

            await model.addEditRecord('chat',{is_read:0},{receiver_id:sender_id,sender_id:receiver_id,job_id:job_id});

    	 	room_record = await  model.findRoomId(sender_id,receiver_id,job_id);   
            console.log(room_record.id);
           	chating_list = await model.getChatHistory(sender_id,room_record.id,page,limit);     
            if(chating_list!=0){
                var myObj = {  status: '1',msg: 'data found',details:chating_list};  
                return res.send(myObj);    
            }else{
                var myObj = {  status: '0',msg: 'No Record Found.'};  
                return res.send(myObj);   
            } 
        })()   
})

app.post('/chat_archive',upload.single('uploaded_image'),function(req,res){ 
    // var room_id = req.body.room_id; 
 
    var sender_id = req.body.sender_id; 
    var room_id = req.body.room_id; 
    var action = req.body.action; 
   
    if(sender_id=='' || sender_id ==undefined){ 
        var myObj = {  status: '0',msg: 'Please enter sender id.'};  
        return res.send(myObj);    
    }
    if(room_id=='' || room_id ==undefined){ 
        var myObj = {  status: '0',msg: 'Please enter room id.'};  
        return res.send(myObj);    
    } 
   if(action=='' || action ==undefined){ 
        var myObj = {  status: '0',msg: 'Please enter action.'};  
        return res.send(myObj);    
    }else{
    	if(action!='Unarchive' && action!='Archive'){
    		var myObj = {  status: '0',msg: 'Action Must be Unarchive or Archive.'};  
        return res.send(myObj);
    	}
    }  
   
        (async()=>{ 
        point =0;  
	  		var ids = room_id.split(",");
	  		for (var i = 0; i < ids.length; i++) {
	  			if(action=='Unarchive'){
	  				point = 0;
	  			}else{
	  				point = 1;
	  			}
  				await model.addEditRecord('chat_room',{ first_user_archive:point},{ first_user:sender_id,id:ids[i]});  
    	   		await model.addEditRecord('chat_room',{ second_user_archive:point},{ second_user:sender_id,id:ids[i]});  
	  		}  
  		  	var myObj = {  status: '1',msg: 'Chat '+action+' Successfully'};  
            return res.send(myObj);   
        })()   
})
  

app.post('/chat_delete',upload.single('uploaded_image'),function(req,res){ 
    // var room_id = req.body.room_id; 
 
    var sender_id = req.body.sender_id; 
    var room_id = req.body.room_id; 
    // var action = req.body.action; 
   
    if(sender_id=='' || sender_id ==undefined){ 
        var myObj = {  status: '0',msg: 'Please enter sender id.'};  
        return res.send(myObj);    
    }
    if(room_id=='' || room_id ==undefined){ 
        var myObj = {  status: '0',msg: 'Please enter room id.'};  
        return res.send(myObj);    
    }  
   
        (async()=>{   
	  		var ids = room_id.split(",");
	  		for (var i = 0; i < ids.length; i++) {
	  			 
  				await model.addEditRecord('chat_room',{first_user_delete:1},{first_user:sender_id,id:ids[i]});  
    	   		await model.addEditRecord('chat_room',{second_user_delete:1},{second_user:sender_id,id:ids[i]});  

  				await model.addEditRecord('chat',{ sender_id_delete:1},{ sender_id:sender_id,room_id:ids[i]});   
  				await model.addEditRecord('chat',{ receiver_id_delete:1},{ receiver_id:sender_id,room_id:ids[i]});  	

	  		}  
  		  	var myObj = {  status: '1',msg: 'Chat Deleted Successfully'};  
            return res.send(myObj);   
        })()   
})
  


var sendNotification = function(receiver_id,$msg){ 
    request.post({
          headers: {'content-type' : 'application/x-www-form-urlencoded'},
          url:     notificationUrl,
          body:    "receiver_id="+receiver_id+"&msg="+$msg,
        });
 }
 

// Web Chat 
var getOnlineUi =  function(user){ 

  return new Promise(function(resolve,reject){ 
     var users = '';
         if(user.length > 0){
          for (var i = 0; i < user.length; i++) {  
            if(user[i].length > 0){

              users += '<li><strong>' +user[i]+'</strong></li>';
            }
          }
         }  
          resolve(users); 
         
    })    
 }


 var ranDomNumber = function(){
    return  Math.floor(Math.random() * (+1500000000000000000 - +1)) + +1; 
 }




var getCurrentTime =function(){

	let date_ob = new Date(); 
	// current date
	// adjust 0 before single digit date
	let date = ("0" + date_ob.getDate()).slice(-2);

	// current month
	let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);

	// current year
	let year = date_ob.getFullYear();

	// current hours
	let hours = date_ob.getHours();

	// current minutes
	let minutes = date_ob.getMinutes();

	// current seconds
	let seconds = date_ob.getSeconds();

	// prints date in YYYY-MM-DD format
	// console.log(year + "-" + month + "-" + date);

	// prints date & time in YYYY-MM-DD HH:MM:SS format
	return (year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds);

	// prints time in HH:MM format
	// console.log(hours + ":" + minutes);
}

const port = process.env.PORT || 3800;
console.log(port)
const server = http.listen(port, function() {
    console.log('listening on *:'+port);
});
// model.getRecord('job');
